﻿function CriarSolicitacaoEfetivacaoDistrato(distratarFilhos) {
    if (RevisarInformacoesFornecidasParaDistrato()) {
        var resultado = ChamarWebService(distratarFilhos);
        if (resultado.ReturnValue.Mrv.Sucesso) {
            alert('Distrato Efetuado.');
            window.document.location.reload();
        }
        else {
            //verificar se a mensagem de erro contém o código para distratar filhos.
            var erro = resultado.ReturnValue.Mrv.Mensagem.toString();
            var codigoDistratarFilhos = '149209221519';
            if (erro.indexOf(codigoDistratarFilhos, 0) == -1) {
                alert(erro);
            }
            else {
                var tmp = erro.split('*');
                if (window.confirm('Este contrato contém aditivos filhos.\n' + tmp[1] + 'Deseja distratá-los automaticamente?')) {
                    var retorno = window.showModalDialog('/' + ORG_UNIQUE_NAME + '/MRVWEB/DistratoRenegociacao/distratarfilhos.aspx?distratoid=' + crmForm.ObjectId, '', 'dialogHeight: 400px;dialogWidth:700px');
                    if (typeof (retorno) != 'undefined') {
                        alert(retorno.returValue);
                        if (retorno.returValue == true) {
                            DisplayActionMsg('Aguarde, processo demorado.', 400, 150);
                            setTimeout('CriarSolicitacaoEfetivacaoDistrato(false)', 1000);
                        }
                    } else {
                        alert('Ação não finalizada.');
                    }
                }
                else {
                    alert('Distrato não efetivado pois possuir aditivos filhos.');
                }

            }
        }
    }
}