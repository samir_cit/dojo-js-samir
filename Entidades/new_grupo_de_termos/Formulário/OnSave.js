﻿var querystring = location.search.substring(1, location.search.length);
var args = querystring.split('&');
var grupoTermoID = "";
for (var i = 0; i < args.length; i++) {
    var pair = args[i].split('=');
    if (pair[0] == 'id') {
        grupoTermoID = CrmEncodeDecode.CrmUrlDecode(pair[1]);
    }
}

grupoTermoID = grupoTermoID.toUpperCase().replace('{', '').replace('}', '');

var cmd = new RemoteCommand("MrvService", "ObterDadosGrupoDeTermos", "/MRVCustomizations/");
cmd.SetParameter("codigoTermo", crmForm.all.new_cod_termo.DataValue.toString());
cmd.SetParameter("planoFinanciamento", crmForm.all.new_planodefinanciamento.DataValue.toString());
cmd.SetParameter("idTermo", grupoTermoID);
cmd.SetParameter("tipoAlteracao", crmForm.FormType.toString());
var oResult = cmd.Execute();

if (oResult.ReturnValue.Mrv.existeRegistro != null) {
    if (oResult.ReturnValue.Mrv.existeRegistro.toString() == 'true') {
        alert("Já existe Grupo de Termo com o " + oResult.ReturnValue.Mrv.tipoValidacao + " informado.");
        if (oResult.ReturnValue.Mrv.tipoValidacao.toString() == 'Código do Termo') {
            crmForm.all.new_cod_termo.DataValue = null;
        }
        else {
            crmForm.all.new_planodefinanciamento.DataValue = null;
        }
        event.returnValue = false;
    }
}