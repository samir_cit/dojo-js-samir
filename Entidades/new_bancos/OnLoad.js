﻿formularioValido = true;
try {

    function loadScript() {
        var oScript = document.createElement('script');
        oScript.type = "text/javascript";
        oScript.src = "/_static/Base.js";
        var oHead = document.getElementsByTagName('head')[0];
        oHead.appendChild(oScript);

        oScript.onreadystatechange = function() {
            if (this.readyState == 'complete' || this.readyState == 'loaded')
                Load();
        }
    }

    loadScript();

    function Load() {
		if (crmForm.UsuarioPertenceEquipe(Equipe.ELEGIBILIDADE_EMPREENDIMENTO)) {
           crmForm.all.new_banco_desligamento.Disabled = false;
       } else {
           crmForm.all.new_banco_desligamento.Disabled = true;
       }
    }
    
} catch (erro) {
    formularioValido = false;
    alert("Ocorreu um erro no formulário.\n" + erro.description);
}