﻿formularioValido = true;
TypeCode = {
    OCORRENCIA: 112
}
try {
    /* Usuários da equipe 'Bloqueio Criação de Atividades' só podem criar atividades a partir de uma ocorrência. */
    function VerificaSeUsuarioTemPermissaoParaCriarAtividades() {
        if (crmForm.FormType == TypeCreate &&
            crmForm.UsuarioPertenceEquipe("Bloqueio Criação de Atividades") &&
            (window.opener == null ||
                window.opener.crmForm == undefined ||
                window.opener.crmForm == null ||
                (window.opener.crmForm && window.opener.crmForm.ObjectTypeCode != TypeCode.OCORRENCIA))) {
            alert('Você não possui permissão para criar atividades que não são vinculadas à uma ocorrência.');
            window.close();
        }
    }

    function Load() {
        VerificaSeUsuarioTemPermissaoParaCriarAtividades();
        if (crmForm.FormType != TypeCreate)
            crmForm.all.description.Disabled = true;

        crmForm.SomenteProprietario("_MIonActionMenuClickdelete4204;_MIchangeStatedeactivate42045;_MSsubconvertActivity;_MBSaveAsCompleted;convertActivity");
    }

    function loadScript() {
        var oHead = document.getElementsByTagName('head')[0];
        var oScript = document.createElement('script');
        oScript.type = "text/javascript";
        oScript.src = "/ISV/Tridea/JavaScript/Base.js";

        oScript.onreadystatechange = function() {
            if (this.readyState == 'complete' || this.readyState == 'loaded') {
                Load();
            }
        }
        oHead.appendChild(oScript);
    }

    loadScript();

} catch (error) {
    formularioValido = false;
    alert("Ocorreu um erro no formulário.\n" + error.description);
}