﻿try {
    if (crmForm.all.new_promooid.DataValue == null) {
        mensagemErro = "É necessário informar a promoção do lote";
        formularioValido = false;
    }

    if (!formularioValido) {
        alert(mensagemErro);
        event.returnValue = false;
        return false;
    }
    
    crmForm.all.new_name.DataValue = DefinirNomeLote();
    crmForm.all.new_name.ForceSubmit = true;
} 
catch (error) {
    alert("Ocorreu um erro no formulário.\n" + error.description);
    event.returnValue = false;
    return false;
}
