﻿formularioValido = true;
mensagemErro = "";
try {
    function loadScript() {
        var oScript = document.createElement('script');
        oScript.type = "text/javascript";
        oScript.src = "/_static/Base.js";

        var oHead = document.getElementsByTagName('head')[0];
        oHead.appendChild(oScript);

        oScript.onreadystatechange = function() {
            if (this.readyState == 'complete' || this.readyState == 'loaded')
                Load();
        }
    }
    loadScript();

    function Load() {
        DefinirNomeLote = function() {
            var dataAtual = new Date();
            return "LOTE: " +
                   crmForm.all.new_promooid.DataValue[0].name +
                   " - " +
                   dataAtual.getFullYear().toString() +
                   AdicionaZeroEsquerda(dataAtual.getMonth()+1) +
                   AdicionaZeroEsquerda(dataAtual.getDate()) +
                   " - " +
                   AdicionaZeroEsquerda(dataAtual.getHours()) +
                   AdicionaZeroEsquerda(dataAtual.getMinutes());
        }
        
        AdicionaZeroEsquerda = function(numero) {
            if (numero.toString().length < 2) {
                numero = "0" + numero;
                return numero;
            }
            else return numero;
        }

        //desabilita o formulário quando o lote estiver travado
        if (crmForm.all.new_travarlote.DataValue != null) {
            var camposBloqueados = ["new_empreendimentoid", "new_travarlote"];
            if (crmForm.all.new_travarlote.DataValue == true) 
                crmForm.DesabilitarCampos(camposBloqueados.join(";"), true);
            else
                crmForm.DesabilitarCampos(camposBloqueados.join(";"), false);

            crmForm.DesabilitarCampos("new_datacompra", crmForm.all.new_datacompra.DataValue != null || crmForm.all.new_travarlote.DataValue == false);
        }
        else crmForm.DesabilitarCampos("new_datacompra", true);

        //define o filter lookup do campo de assunto principal
        //crmForm.FilterLookup(crmForm.all.new_assuntoprincipalid,
        //    10092,
        //    "/MRVWeb/FilterLookup/FilterLookup.aspx", 
        //    "objectTypeCode=10092001&filterDefault=false&attributesearch=new_assunto_principal");
    }
}
catch (error) {
    formularioValido = false;
    alert("Ocorreu um erro no formulário.\n" + error.description);
}
