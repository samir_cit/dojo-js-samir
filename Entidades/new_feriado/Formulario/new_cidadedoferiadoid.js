﻿if (crmForm.all.new_cidadedoferiadoid.DataValue != null) {
    var attributes = "new_uf";
    var cmd = new RemoteCommand("MrvService", "GetEntity", "/MRVCustomizations/");

    cmd.SetParameter("entity", "new_cidade");
    cmd.SetParameter("primaryField", "new_cidadeid");
    cmd.SetParameter("entityId", crmForm.all.new_cidadedoferiadoid.DataValue[0].id);
    cmd.SetParameter("fieldList", attributes);

    var oResult = cmd.Execute();
    if (oResult.Success) {

        if (!oResult.ReturnValue.Mrv.Found) {
            alert('A cidade ' + crmForm.all.new_cidadedoferiadoid.DataValue[0].name + ' não possui UF. Favor cadastrar.');
            event.returnValue = false;
            return false;
        }
        
        SetUF(oResult.ReturnValue.Mrv.new_uf, crmForm.all.new_uf);
    }
    else
        alert("Erro na consulta ao WebService.\nContate o administrador do sistema.");
}
else {
    crmForm.all.new_uf.DataValue = null;
}

function SetUF(nome, picklistField) {
    for (i = 0; i < picklistField.Options.length; i++) {
        if (picklistField.Options[i].Text.toUpperCase() == nome.toUpperCase())
            picklistField.SelectedIndex = i;
    }
}

