﻿if (crmForm.all.new_tipo.DataValue != null)
{
    if (crmForm.all.new_tipo.DataValue == 1) // Nacional
    {
        crmForm.all.new_cidadedoferiadoid_c.style.visibility = 'hidden';
        crmForm.all.new_cidadedoferiadoid_d.style.visibility = 'hidden';
        crmForm.all.new_cidadedoferiadoid.DataValue = null;        
        crmForm.SetFieldReqLevel('new_cidadedoferiadoid', false);

        crmForm.all.new_uf_c.style.visibility = 'hidden';
        crmForm.all.new_uf_d.style.visibility = 'hidden';
        crmForm.all.new_uf.DataValue = null;
        crmForm.SetFieldReqLevel('new_uf', false);
    } 
    else if (crmForm.all.new_tipo.DataValue == 2) // Estadual
    {
        crmForm.all.new_cidadedoferiadoid_c.style.visibility = 'hidden';
        crmForm.all.new_cidadedoferiadoid_d.style.visibility = 'hidden';
        crmForm.all.new_cidadedoferiadoid.DataValue = null;        
        crmForm.SetFieldReqLevel('new_cidadedoferiadoid', false);
        
        crmForm.all.new_uf_c.style.visibility = 'visible';
        crmForm.all.new_uf_d.style.visibility = 'visible';
        crmForm.SetFieldReqLevel('new_uf', true);
        crmForm.all.new_uf.Disabled = false;
    }
    else // Municipal
    {
        crmForm.all.new_cidadedoferiadoid_c.style.visibility = 'visible';
        crmForm.all.new_cidadedoferiadoid_d.style.visibility = 'visible';
        crmForm.SetFieldReqLevel('new_cidadedoferiadoid', true);
        crmForm.all.new_uf_c.style.visibility = 'visible';
        crmForm.all.new_uf_d.style.visibility = 'visible';
        crmForm.SetFieldReqLevel('new_uf', true);
        crmForm.all.new_uf.Disabled = true;
    }
}
