﻿try {
    if (!formularioValido) {
        alert("Ocorreu um erro no formulário. Impossível salvar.");
        event.returnValue = false;
        return false;
    }
    
    //Deixar maiusculo UF
    crmForm.all.new_uf.DataValue = (crmForm.all.new_uf.DataValue != null) ? crmForm.all.new_uf.DataValue.toUpperCase() : null;
    
    // Verifica se teve alguma alteração    
    if (!crmForm.IsDirty)
        return false;
        
    //Habilitando campos
    crmForm.all.new_uf.Disabled = false;
    
} catch (error) {
    alert("Ocorreu um erro no formulário.\n" + error.description);
    event.returnValue = false;
    return false;
}