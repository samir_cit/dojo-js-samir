﻿/* Se a atividade está vinculada a uma ocorrência, o campo Remente só podem ser alterado para o usuário logado. */
if (crmForm.all.from.DataValue != null && 
    atividadeCriadaApartirDeUmaOcorrencia &&
    !crmForm.ComparaIdComUsuarioLogado(crmForm.all.from.DataValue[0].id)) {

    crmForm.all.from.DataValue = null;
    alert('Usuário sem permissão para editar o campo De.');
}