﻿formularioValido = true;
try {

    TipoEmail = {
        CLIENTE: "2",
        INTERNO: "1"
    }

    crmForm.ConfiguraExibicaoDepartamentos = function() {
        if (crmForm.all.new_tipo.DataValue == TipoEmail.INTERNO) {
            crmForm.SetFieldReqLevel("new_departamentos", true);
            crmForm.EscondeCampo("new_departamentos", false);
        }
        else {
            crmForm.SetFieldReqLevel("new_departamentos", false);
            crmForm.EscondeCampo("new_departamentos", true);
        }
    }

    atividadeCriadaApartirDeUmaOcorrencia = false;
    usuarioId = null;
    
    function ObterUsuarioLogado() {
        var cmd = new RemoteCommand('MrvService', 'ObterUsuarioLogado', '/MRVCustomizations/');
        var result = cmd.Execute();
        if (result.Success) {
            if (result.ReturnValue.Mrv.Erro) {
                alert(result.ReturnValue.Mrv.Result);
                DisabledAllFields();
            }
            else {
                usuarioId = result.ReturnValue.Mrv.UsuarioId;
            }
        }
        else {
            alert('Erro na comunicação com o MrvService. Contate o administrador do sistema.');
            DisabledAllFields();
        }
    }
    
    /* Ocorrências criadas a partir de uma conversão de um email, são criadas indevidamentes. */
    function DesabilitarCampoConverterAtividadeEmOcorrencia() {
        crmForm.RemoveBotaoPadraoCRM("convertActivity");
        crmForm.RemoveBotaoPadraoCRM("_MSsubconvertActivity");
    }

    /* Preencher o remetente como Relacionamento Cliente */
    function PreencherRemetenteRelacionamentoCliente() {
        var cmd = new RemoteCommand("MrvService", "ObterPorLogin", "/MrvCustomizations/");
        cmd.SetParameter("login", "relacionamento");
        var resultado = cmd.Execute();
        if (resultado.Success) {
            crmForm.AddValueLookup(crmForm.all.from, resultado.ReturnValue.Mrv.Id, 8, resultado.ReturnValue.Mrv.Nome.toUpperCase());
        }
        else {
            alert(resultado.ReturnValue.Mrv.Error);
            formularioValido = false;
        }
    }

    /* Usuários da equipe 'Bloqueio Criação de Atividades' só podem criar atividades a partir de uma ocorrência. */
    function VerificaSeUsuarioTemPermissaoParaCriarAtividades() {
        if (crmForm.FormType == TypeCreate && crmForm.UsuarioPertenceEquipe("Bloqueio Criação de Atividades")) {
            alert('Você não possui permissão para criar atividades que não são vinculadas à uma ocorrência.');
            window.close();
        }
    }

    crmForm.ComparaIdComUsuarioLogado = function(id) {
        if (usuarioId == null)
            ObterUsuarioLogado();

        return usuarioId != null && (id.toUpperCase() == usuarioId.toUpperCase());
    }

    function Load() {
        /*  [ Seja atividade criada a partir de uma ocorrência ]
        - Bloquear o campo Remetente.
        - Limpa o campo Para.
        - Preenche o campo Remetente como Relacionamento Cliente {default}.
        - Adiciona o item no campo Assunto
        */
        if (crmForm.FormType == TypeCreate && window.opener && window.opener.opener && window.opener.crmForm.ObjectTypeCode == 112 && window.opener.crmForm) {
            atividadeCriadaApartirDeUmaOcorrencia = true;
            crmForm.all.to.DataValue = null;
            PreencherRemetenteRelacionamentoCliente();
            var numeroOcorrencia = window.opener.crmForm.ticketnumber.DataValue;
            crmForm.all.subject.DataValue = "Ocorrência: " + numeroOcorrencia;
        }
        else
            VerificaSeUsuarioTemPermissaoParaCriarAtividades();

        DesabilitarCampoConverterAtividadeEmOcorrencia();
        crmForm.SomenteProprietario("_MIonActionMenuClickdelete4202;_MIsend;_MIlocAddObjTo1001;_MBsend");
        crmForm.ConfiguraExibicaoDepartamentos();        
    }

    function loadScript() {
        var oHead = document.getElementsByTagName('head')[0];
        var oScript = document.createElement('script');
        oScript.type = "text/javascript";
        oScript.src = "/ISV/Tridea/JavaScript/Base.js";

        oScript.onreadystatechange = function() {
            if (this.readyState == 'complete' || this.readyState == 'loaded') {
                Load();
            }
        }
        oHead.appendChild(oScript);
    }

    loadScript();
}
catch (error) {
    formularioValido = false;
    alert("Ocorreu um erro no formulário.\n" + error.description);
}
