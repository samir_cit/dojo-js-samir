﻿formularioValido = true;
numeroDias = "";

try {
    function loadScript() {
        var oScript = document.createElement('script');
        oScript.type = "text/javascript";
        oScript.src = "/_static/Base.js";

        var oHead = document.getElementsByTagName('head')[0];
        oHead.appendChild(oScript);

        oScript.onreadystatechange = function() {
            if (this.readyState == 'complete' || this.readyState == 'loaded')
                Load();
        }
    }
    loadScript();

    function Load() {
        //Usuário Adm
        Administrador = null;
        crmForm.UsuarioAdministrador = function() {
            if (Administrador == null) {
                Administrador = crmForm.VerificaFuncao("Administradores");
            }
            return Administrador;
        }

        crmForm.sicaq = crmForm.all.new_prazo_desligamento_sicaq.value;
        crmForm.sacbb = crmForm.all.new_prazo_desligamento_sacbb.value;
        crmForm.intermedium = crmForm.all.new_prazo_desligamento_intermedium.value;
        crmForm.politica = crmForm.all.new_prazo_desligamento_politica_credito.value;
        crmForm.idRegistro = crmForm.ObjectId;
        
        
        crmForm.VerificaFuncao = function(nomeFuncao) {
            var cmd = new RemoteCommand("MrvService", "UserContainsTeams", "/MrvCustomizations/");
            cmd.SetParameter("teams", nomeFuncao);
            var resultado = cmd.Execute();
            if (resultado.Success) {
                return resultado.ReturnValue.Mrv.Found;
            } else {
                alert("Erro na consulta do método UserContainsTeams");
                formularioValido = false;
                //return false;
            }
        }

        crmForm.AbrirConsultaLog = function() {
            if (crmForm.UsuarioAdministrador()) {
                window.open("/mrvweb/consultalog/default.aspx", "ConsultaLog", "height=500,width=999,scrollbars=no,status=no,location=no,toolbar=no,menubar=yes");
            }
        }

        crmForm.UsarDataLimite = function() {
            var usarData = (crmForm.all.new_usar_data_limite.DataValue != null) ? crmForm.all.new_usar_data_limite.DataValue : false;
            if (usarData) {
                //Liberar campo e tornar obrigatório.
                crmForm.all.new_data_limite.Disabled = false;
                crmForm.SetFieldReqLevel("new_data_limite", true);
            } else {
                crmForm.all.new_data_limite.Disabled = true;
                crmForm.SetFieldReqLevel("new_data_limite", true);
            }
        }

        /// Desabilita ou Habilitar o formulário de acordo com o parâmetro
        crmForm.DesabilitarFormulario = function(valor) {
            var todosAtributos = document.getElementsByTagName("label");
            for (var i = 0; i < todosAtributos.length; i++) {
                if (todosAtributos[i].htmlFor != "") {
                    var atributo = eval("crmForm.all." + todosAtributos[i].htmlFor.replace("_ledit", ""));
                    if (atributo != undefined && atributo != null)
                        atributo.Disabled = valor;
                }
            }
        }

        if (crmForm.all.new_dias_oportunidade_perdida != null &&
			crmForm.all.new_dias_oportunidade_perdida.DataValue != null) {
            numeroDias = crmForm.all.new_dias_oportunidade_perdida.DataValue.toString();
        }

        if (crmForm.all.new_prazo_limite_fechamento.DataValue == null) {

            //padrão de 4 dias para limite de fechamento
            crmForm.all.new_prazo_limite_fechamento.DataValue = 4;
        }
        if (crmForm.UsuarioPertenceEquipe("Administradores")) {
            crmForm.all.new_validar_serasa.Disabled = false;
            crmForm.all.new_validar_spc.Disabled = false;
        } else {
            crmForm.all.new_validar_serasa.Disabled = true;
            crmForm.all.new_validar_spc.Disabled = true;
        }

        if (crmForm.UsuarioPertenceEquipe(Equipe.CR_PARAMETRO_COMERCIAL_DATABASE)) {
            crmForm.all.new_prazo_limite_fechamento.Disabled = false;
            crmForm.all.new_percentual_primeira_parcela.Disabled = false;
            crmForm.all.new_comprometimento_renda_cliente.Disabled = false;
        } else {
            crmForm.all.new_prazo_limite_fechamento.Disabled = true;
            crmForm.all.new_percentual_primeira_parcela.Disabled = true;
            crmForm.all.new_comprometimento_renda_cliente.Disabled = true;
        }

        if (crmForm.UsuarioPertenceEquipe(Equipe.CR_PARAMETRO_COMERCIAL_DATABASE)) {
            crmForm.all.new_percentual_comprometimento_maximo_fiador.Disabled = false;
            crmForm.all.new_percentual_comprometimento_minimo_fiador.Disabled = false;
        } else {
            crmForm.all.new_percentual_comprometimento_maximo_fiador.Disabled = true;
            crmForm.all.new_percentual_comprometimento_minimo_fiador.Disabled = true;
        }

        if (!crmForm.UsuarioAdministrador()) {
            crmForm.all.tab0Tab.style.display = "none";
            crmForm.all.tab1Tab.style.display = "none";
            crmForm.all.tab2Tab.style.display = "none";
            crmForm.all.tab3Tab.style.display = "none";
            crmForm.all.tab4Tab.style.display = "none";
            crmForm.all.tab5Tab.style.display = "none";
            crmForm.all.tab6Tab.style.display = "none";
            crmForm.all.tab7Tab.style.display = "none";

            crmForm.all.tab0.style.display = "none";
            crmForm.all.tab7.style.display = "none";

            crmForm.all.new_desconto_indicado.Disabled = true;
            crmForm.all.new_valor_indicacao1.Disabled = true;
            crmForm.all.new_valor_indicacao2.Disabled = true;
            crmForm.all.new_valor_indicacao3.Disabled = true;
            crmForm.all.new_valor_indicacao4.Disabled = true;

            if (crmForm.VerificaFuncao("Marketing")) {
                crmForm.all.tab6Tab.style.display = "inline";
                crmForm.all.tab6.style.display = "inline";
                crmForm.all.new_dias_oportunidade_perdida.Disabled = false;
            }


            var camposComissao = "new_prazo_desligamento_sicaq;new_prazo_desligamento_sacbb;new_prazo_desligamento_intermedium;new_prazo_desligamento_politica_credito"
            if (crmForm.UsuarioPertenceEquipe("CO_PARCELAMENTO_COMISSAO")) {
                crmForm.all.tab4Tab.style.display = "inline";
                crmForm.all.tab4.style.display = "inline";
                crmForm.DesabilitarFormulario(true);
                crmForm.DesabilitarCampos(camposComissao, false)
            }
            else {
                crmForm.DesabilitarCampos(camposComissao, true)
            }

            var camposDataBase = "new_tipo_delta_renda_cima;new_valor_delta_renda_cima;new_tipo_delta_renda_baixo;new_valor_delta_renda_baixo;new_tipo_delta_fgts_cima;new_valor_delta_fgts_cima;new_tipo_delta_fgts_baixo;new_valor_delta_fgts_baixo;new_tipo_delta_financ_cima;new_valor_delta_financ_cima;new_tipo_delta_financ_baixo;new_valor_delta_financ_baixo;new_prazo_limite_fechamento;new_percentual_primeira_parcela;new_comprometimento_renda_cliente;new_percentual_comprometimento_maximo_fiador;new_percentual_comprometimento_minimo_fiador";
            if (crmForm.UsuarioPertenceEquipe(Equipe.CR_PARAMETRO_COMERCIAL_DATABASE)) {
                crmForm.all.tab0Tab.style.display = "inline";
                crmForm.all.tab0.style.display = "inline";
                
                crmForm.DesabilitarFormulario(true);
                crmForm.DesabilitarCampos(camposDataBase, false)
            }
            else {
                crmForm.DesabilitarCampos(camposDataBase, true)
            }
        } else {
            //Chamar onChange do campo usar data limite
            new_usar_data_limite_onchange0();
        }
        
    }
} catch (error) {
    formularioValido = false;
    alert("Ocorreu um erro no formulário.\n" + error.description);
}
