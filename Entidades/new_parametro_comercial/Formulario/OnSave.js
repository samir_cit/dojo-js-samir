﻿try {
    if (!formularioValido) {
        alert("Ocorreu um erro no formulário. Impossível salvar.");
        event.returnValue = false;
        return false;
    }
    
    // Transformar caracteres em maiusculos------------------------------------//
    var elm = document.getElementsByTagName("input");
    for (i = 0; i< elm.length; i++)
    {
        if (elm[i].type == "text")
            if (elm[i].DataValue != null)
            { 
                 try
                 { 
                     if(elm[i] != crmForm.all.new_localpagamento_unibanco && elm[i] != crmForm.all.new_localpagamento_itau && elm[i] != crmForm.all.new_enderecocedente_itau)
                        elm[i].DataValue = elm[i].DataValue.toUpperCase(); 
                  }
                  catch (e) {}
             }
    }    
    
    //Testa se houve alteração no período de vigência    
    if (crmForm.all.new_dias_oportunidade_perdida != null &&
        crmForm.all.new_dias_oportunidade_perdida.DataValue != null &&
        numeroDias != crmForm.all.new_dias_oportunidade_perdida.DataValue.toString())
    {
	    Informacoes = new InformacoesDoUsuario();
		
	    var today = new Date();
	    var strDataAtual = "{0}/{1}/{2}".format(today.getDate(), today.getMonth()+1, today.getFullYear());
	
	    var texto = crmForm.all.new_historico_dias_oportunidade_perdida.DataValue == null ? "" : crmForm.all.new_historico_dias_oportunidade_perdida.DataValue;
	    texto += texto == "" ? "" : "\r\n===============================\r\n";
	    texto += "Valor anterior: {0}\r\n".format(numeroDias);
	    texto += "Alterado para: {0}\r\n".format(crmForm.all.new_dias_oportunidade_perdida.DataValue.toString());
	    texto += "Alterado por: {0}\r\n".format(Informacoes.Fullname);
	    texto += "Em: {0}\r\n".format(strDataAtual);
	    crmForm.all.new_historico_dias_oportunidade_perdida.DataValue = texto;
	    crmForm.all.new_historico_dias_oportunidade_perdida.ForceSubmit = true;
    }
    if (crmForm.all.new_prazo_desligamento_sicaq.IsDirty || crmForm.all.new_prazo_desligamento_sacbb.IsDirty || crmForm.all.new_prazo_desligamento_intermedium.IsDirty ||crmForm.all.new_prazo_desligamento_politica_credito.IsDirty ) 
    {
        if(crmForm.all.new_prazo_desligamento_sicaq.IsDirty){
            var cmd = new RemoteCommand("MrvService", "GerarLogAltercaoPrazosDesligamentoElegibilidade", "/MRVCustomizations/");
                            cmd.SetParameter("id", crmForm.idRegistro);
                            cmd.SetParameter("valorAnterior", crmForm.sicaq);
                            cmd.SetParameter("novoValor", crmForm.all.new_prazo_desligamento_sicaq.value);
                            cmd.SetParameter("campo", "Prazo de desligamento com o banco Sicaq");
                            cmd.Execute();                            
                            }
        if(crmForm.all.new_prazo_desligamento_sacbb.IsDirty){
            var cmd = new RemoteCommand("MrvService", "GerarLogAltercaoPrazosDesligamentoElegibilidade", "/MRVCustomizations/");
                            cmd.SetParameter("id", crmForm.idRegistro);
                            cmd.SetParameter("valorAnterior", crmForm.sacbb);
                            cmd.SetParameter("novoValor", crmForm.all.new_prazo_desligamento_sacbb.value);
                            cmd.SetParameter("campo", "Prazo de desligamento com o banco SacBB");
                            cmd.Execute();                            
                            }
        if(crmForm.all.new_prazo_desligamento_intermedium.IsDirty){
                            var cmd = new RemoteCommand("MrvService", "GerarLogAltercaoPrazosDesligamentoElegibilidade", "/MRVCustomizations/");
                            cmd.SetParameter("id", crmForm.idRegistro);
                            cmd.SetParameter("valorAnterior", crmForm.intermedium);
                            cmd.SetParameter("novoValor", crmForm.all.new_prazo_desligamento_intermedium.value);
                            cmd.SetParameter("campo", "Prazo de desligamento com o banco Intermedium");
                            cmd.Execute();                            
                            }
        if(crmForm.all.new_prazo_desligamento_politica_credito.IsDirty){
            var cmd = new RemoteCommand("MrvService", "GerarLogAltercaoPrazosDesligamentoElegibilidade", "/MRVCustomizations/");
                            cmd.SetParameter("id", crmForm.idRegistro);
                            cmd.SetParameter("valorAnterior", crmForm.politica);
                            cmd.SetParameter("novoValor", crmForm.all.new_prazo_desligamento_politica_credito.value);
                            cmd.SetParameter("campo", "Prazo de desligamento com o banco Politica de crédito");
                            var resultado = cmd.Execute();
                            if(!resultado.Success){
                                
                            }
                            }
                            
        }
} 
catch (error) {
    alert("Ocorreu um erro no formulário.\n" + error.description);
    event.returnValue = false;
    return false;
}