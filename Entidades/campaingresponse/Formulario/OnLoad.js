﻿formularioValido = true;
try {
    /* Usuários da equipe 'Bloqueio Criação de Atividades' só podem criar atividades a partir de uma ocorrência. */
    function VerificaSeUsuarioTemPermissaoParaCriarAtividades() {
        if (crmForm.FormType == TypeCreate && crmForm.UsuarioPertenceEquipe("Bloqueio Criação de Atividades")) {
            alert('Você não possui permissão para criar atividades que não são vinculadas à uma ocorrência.');
            window.close();
        }
    }

    function Load() {
        if (crmForm.FormType == TypeCreate && window.opener && window.opener.opener && window.opener.crmForm.ObjectTypeCode == 112 && window.opener.crmForm) {
            if (window.opener.crmForm.casetypecode.DataValue != null && window.opener.crmForm.casetypecode.DataValue == 200003) {
                var elm = document.getElementsByTagName('*');
                for (var i = 0; i < elm.length; i++) {
                    if (elm[i].IsDirty) {
                        elm[i].DataValue = null;
                    }
                }
                alert("Não é possível criar resposta da campanha para uma ocorrência de Assistência Técnica.");
                window.close();
            }
        }
        else
            VerificaSeUsuarioTemPermissaoParaCriarAtividades();

        if (crmForm.FormType != TypeCreate)
            crmForm.all.description.Disabled = true;

        crmForm.SomenteProprietario("_MIonActionMenuClickdelete4401;_MIConvertResponse;_MBConvertResponse");
    }

    function loadScript() {
        var oHead = document.getElementsByTagName('head')[0];
        var oScript = document.createElement('script');
        oScript.type = "text/javascript";
        oScript.src = "/_static/Base.js";

        oScript.onreadystatechange = function() {
            if (this.readyState == 'complete' || this.readyState == 'loaded') {
                Load();
            }
        }
        oHead.appendChild(oScript);
    }

    loadScript();
     
} catch (error) {
    formularioValido = false;
	alert("Ocorreu um erro no formulário.\n" + error.description);
}