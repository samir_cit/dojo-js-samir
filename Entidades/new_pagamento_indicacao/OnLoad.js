﻿try {
    if (window.opener.parent.crmForm.all.new_enviado_sodexo.DataValue == true)
        crmForm.all.new_remover_lote.Disabled = true;
    else
        crmForm.all.new_remover_lote.Disabled = false;
}
catch (error) {
    crmForm.all.new_remover_lote.Disabled = true;
}

//Permite bloquear/desbloquear o pagamento somente nos status: 
//Pendente (1), Calculado (3), Inadimplente (6) e Bloqueado (7)
crmForm.all.new_bloquearpagamento.Disabled = (
    crmForm.all.statuscode.DataValue.toString() != "1" &&
    crmForm.all.statuscode.DataValue.toString() != "3" &&
    crmForm.all.statuscode.DataValue.toString() != "6" &&
    crmForm.all.statuscode.DataValue.toString() != "7");
    