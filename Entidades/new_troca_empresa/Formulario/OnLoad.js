﻿formularioValido = true;
try {
    //Adiciona Script para ser usado no click do botão
    var elm = document.createElement("script");
    elm.src = "/_static/_grid/cmds/util.js";
    document.appendChild(elm);

    AdicionaNotificacao = function(message, tipo) {
        var imagem = "";
        if (tipo == "erro")
            imagem = "/_imgs/error/notif_icn_crit16.png";
        else if (tipo == "alerta")
            imagem = "/_imgs/error/notif_icn_warn16.png";
        else if (tipo == "sucesso")
            imagem = "/_imgs/ico/16_L_check.gif";

        var notificationHTML = '<DIV class="Notification"><TABLE cellSpacing="0" cellPadding="0"><TBODY><TR><TD vAlign="top"><IMG class="ms-crm-Lookup-Item" alt="" src="' + imagem + '" /></TD><TD><SPAN>' + message + '</SPAN></TD></TR></TBODY></TABLE></DIV>';
        var notificationsArea = document.getElementById('Notifications');
        if (notificationsArea == null)
            return;
        notificationsArea.innerHTML = notificationHTML;
        notificationsArea.style.display = 'block';
    }

    //se status igual a enviado, desabilitar tudo
    if (crmForm.all.statuscode.DataValue == 3 || crmForm.all.statuscode.DataValue == 4) {
        var todosAtributos = document.getElementsByTagName("label");
        for (var i = 0; i < todosAtributos.length; i++) {
            if (todosAtributos[i].htmlFor != "") {
                var object = eval("crmForm.all." + todosAtributos[i].htmlFor.replace("_ledit", ""))
                object.Disabled = true;
            }
        }
        if (crmForm.all.statuscode.DataValue == 3)
            AdicionaNotificacao("Enviado com sucesso", "sucesso");
    }

    crmForm.VerificaFuncao = function(nomeFuncao) {
        var cmd = new RemoteCommand("MrvService", "UserContainsTeams", "/MrvCustomizations/");
        cmd.SetParameter("teams", nomeFuncao);
        var resultado = cmd.Execute();
        if (resultado.Success) {
            return resultado.ReturnValue.Mrv.Found;
        } else {
            alert("Erro na consulta do método UserContainsTeams");
            AdicionaNotificacao("Erro na consulta do método UserContainsTeams", "erro");
            formularioValido = false;
            return false;
        }
    }

    crmForm.MontaNome = function() {
        crmForm.all.new_name.DataValue = crmForm.all.new_empreendimentoid.DataValue[0].name;
    }

    crmForm.CancelaTroca = function() {
        if (crmForm.all.statuscode.DataValue == 3) {
            AdicionaNotificacao("Registro já foi enviado. Impossível cancelar.", "alerta");
            return false;
        }
        if (crmForm.all.statuscode.DataValue == 4) {
            AdicionaNotificacao("Registro já foi cancelado.", "alerta");
            return false;
        }

        crmForm.all.statuscode.Disabled = false;
        crmForm.all.statuscode.DataValue = 4;
        crmForm.Save();
    }

    function ExecutaRemoteCommand(nomeMetodo) {
        var cmd = new RemoteCommand('MrvService', nomeMetodo, '/MRVCustomizations/');
        cmd.SetParameter('trocaEmpresaId', crmForm.ObjectId);
        var resultado = cmd.Execute();
        if (!resultado.Success) {
            HideActionMsg();
            AdicionaNotificacao("Erro ao acessar o WebService. Tente novamente mais tarde.", "erro");
            return false;
        } else if (resultado.ReturnValue.Mrv.Error) {
            HideActionMsg();
            AdicionaNotificacao(resultado.ReturnValue.Mrv.Error, "erro");
            return false;
        } else if (resultado.ReturnValue.Mrv.ErrorSoap) {
            HideActionMsg();
            AdicionaNotificacao(resultado.ReturnValue.Mrv.ErrorSoap, "erro");
            return false;
        } else
            return true;
    }

    function MovimentarEstoque() {
        if (ExecutaRemoteCommand("MovimentarEstoque"))
            window.location.reload(true);
    }

    function ReenviarProdutos() {
        if (ExecutaRemoteCommand("ReenviarProdutos")) {
            //Chama passo 4
            //var passo4 = new MovimentarEstoque();
            DisplayActionMsg("Movimentando Estoque.", 400, 150);
            window.setTimeout(MovimentarEstoque, 1000);
        }
    }

    function ValidaProdutosExecutaBackup() {
        if (ExecutaRemoteCommand("ValidaProdutosExecutaBackup")) {
            //Chama passo 3
            DisplayActionMsg("Reenviando Produtos.", 400, 150);
            window.setTimeout(ReenviarProdutos, 1000);
        }
    }

    function ValidacoesIniciais() {
        var objeto = this;
        var Sucesso = true;
        var MensagemRetorno = "";
        var Imagem = "";

        objeto.Executa = function() {
            if (crmForm.all.statuscode.DataValue == 3) {
                MensagemRetorno = "Registro já foi enviado.";
                Imagem = "alerta";
                Sucesso = false;
            }
            if (crmForm.all.statuscode.DataValue == 4) {
                MensagemRetorno = "Registro foi cancelado.";
                Imagem = "alerta";
                Sucesso = false;
            }

            if (!crmForm.IsValid()) {
                MensagemRetorno = "Validações do Formulário.";
                Imagem = "alerta";
                Sucesso = false;
            }
            if (crmForm.IsDirty) {
                MensagemRetorno = "Salve antes de enviar.";
                Imagem = "alerta";
                Sucesso = false;
            }

            if (!crmForm.VerificaFuncao("Administradores")) {
                MensagemRetorno = "Usuário não possui privilégio para executar está função.";
                Imagem = "erro";
                Sucesso = false;
            }

            if (!Sucesso) {
                AdicionaNotificacao(MensagemRetorno, Imagem);
                HideActionMsg();
                return false;
            }

            DisplayActionMsg("Validando Produtos e Efetuando BackUp.", 400, 150);
            window.setTimeout(ValidaProdutosExecutaBackup, 1000);
        }
    }

    crmForm.TrocaEmpresa = function() {
        var passo1 = new ValidacoesIniciais();
        DisplayActionMsg("Executando Validações.", 400, 150);
        window.setTimeout(passo1.Executa, 1000);
    }

    crmForm.BloqueioEmpresa = function() {
        var passo1 = new ValidacoesIniciais();
        DisplayActionMsg("Executando bloqueio/desbloqueio.", 400, 150);
        window.setTimeout(passo1.Executa, 1000);
    }


    crmForm.EscondeCampo = function(nomeCampo, esconde) {
        var tipo = "";
        if (esconde)
            tipo = "none";

        var atributo_c = eval("crmForm.all." + nomeCampo + "_c");
        var atributo_d = eval("crmForm.all." + nomeCampo + "_d");
        atributo_c.style.display = tipo;
        atributo_d.style.display = tipo;
    }

    //Esconder campos da seção administrativa
    crmForm.EscondeCampo("new_name", true);
    //Monta o nome do registro
    crmForm.MontaNome();

} catch (error) {
    formularioValido = false;
    alert("Ocorreu um erro no formulário.\n" + error.description);
}