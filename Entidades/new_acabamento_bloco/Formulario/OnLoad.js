﻿try {
    function loadScript() {
        var oHead = document.getElementsByTagName('head')[0];
        var oScript = document.createElement('script');
        oScript.type = "text/javascript";
        oScript.src = "/_static/Base.js";

        oScript.onreadystatechange = function() {
            if (this.readyState == 'complete' || this.readyState == 'loaded') {
                Load();
            }
        }
        oHead.appendChild(oScript);
    }

    loadScript();

    function Load() {

        TipoBloco = {
            BLOCO: 1,
            TORRE: 2,
            CASA: 3,
            QUADRA: 4
        }
        
        crmForm.ObtemTipoBloco = function() {
            var atributos = "new_tipo";
            var cmd = new RemoteCommand("MrvService", "GetEntity", "/MRVCustomizations/");
            cmd.SetParameter("entity", "new_blocoviabilidade");
            cmd.SetParameter("primaryField", "new_blocoviabilidadeid");
            cmd.SetParameter("entityId", crmForm.new_bloco_viabilidade_id.DataValue[0].id);
            cmd.SetParameter("fieldList", atributos);
            var oResult = cmd.Execute();
            if (oResult.Success) {
                if (oResult.ReturnValue.Mrv.new_tipo != null)
                    return oResult.ReturnValue.Mrv.new_tipo;
            }
            return null;
        }
        
        /// Desabilita todos os campos e habilita os campos de acordo com a equipe do usuário
        crmForm.HabilitaCamposPorEquipe = function() {
            /// Esconde aba Anotações
            crmForm.EscondeAba("tab1Tab", true);

            crmForm.DesabilitarFormulario(true);

            var tipoBloco = crmForm.ObtemTipoBloco();

            // Tipo do bloco igual a casa
            if (tipoBloco != null && tipoBloco == TipoBloco.CASA) {
                crmForm.EscondeCampo("new_tipo_acabamento", true);
            }
            
            var UsuarioAcabamentoBloco = crmForm.UsuarioPertenceEquipe(Equipe.CO_ACABAMENTO_BLOCO);
            var UsuarioDI = crmForm.UsuarioPertenceEquipe(Equipe.DESENVOLVIMENTO_IMOBILIARIO);
            var UsuarioAdministrador = crmForm.UsuarioPertenceEquipe(Equipe.ADMINISTRADORES);
            
            if (UsuarioAcabamentoBloco)
                crmForm.DesabilitarCampos("new_tipo_acabamento", false);
            
            if (UsuarioDI)
                crmForm.DesabilitarCampos("new_nome", false);
            
            if (UsuarioAdministrador)
                crmForm.DesabilitarFormulario(false);
        }

        crmForm.HabilitaCamposPorEquipe();

        crmForm.ObtemViabilidadeId = function() {
            var atributos = "new_viabilidadeid";
            var cmd = new RemoteCommand("MrvService", "GetEntity", "/MRVCustomizations/");
            cmd.SetParameter("entity", "new_blocoviabilidade");
            cmd.SetParameter("primaryField", "new_blocoviabilidadeid");
            cmd.SetParameter("entityId", crmForm.new_bloco_viabilidade_id.DataValue[0].id);
            cmd.SetParameter("fieldList", atributos);
            var oResult = cmd.Execute();
            if (crmForm.TratarRetornoRemoteCommand(oResult, "GetEntity")) {
                if (oResult.ReturnValue.Mrv.new_viabilidadeid != null)
                    return oResult.ReturnValue.Mrv.new_viabilidadeid;
            }
            return null;
        }

        crmForm.ViabilidadeGerada = function(viabilidadeId) {
            var atributos = "new_viabilidadegerada";
            var cmd = new RemoteCommand("MrvService", "GetEntity", "/MRVCustomizations/");
            cmd.SetParameter("entity", "new_viabilidade");
            cmd.SetParameter("primaryField", "new_viabilidadeid");
            cmd.SetParameter("entityId", viabilidadeId);
            cmd.SetParameter("fieldList", atributos);
            var oResult = cmd.Execute();

            if (crmForm.TratarRetornoRemoteCommand(oResult, "GetEntity")) {
                if (oResult.ReturnValue.Mrv.new_viabilidadegerada != null) {
                    return oResult.ReturnValue.Mrv.new_viabilidadegerada;
                }
            }
            return false;
        }

        crmForm.ValidaViabilidadeGerada = function() {
            if ((crmForm.FormType == TypeCreate || crmForm.FormType == TypeUpdate) && (crmForm.new_bloco_viabilidade_id.DataValue != null)) {
            
                var viabilidadeId = crmForm.ObtemViabilidadeId();

                if (viabilidadeId != null) {
                    if (crmForm.ViabilidadeGerada(viabilidadeId)) {
                        crmForm.DesabilitarFormulario(true);
                    }
                }
            }
        }

        crmForm.ValidaViabilidadeGerada();
    }

} catch (error) {
    formularioValido = false;
    alert("Ocorreu um erro no formulário.\n" + error.description);
}
