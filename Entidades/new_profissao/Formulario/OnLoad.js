﻿formularioValido = true;
try {
    crmForm.VerificaFuncao = function(nomeFuncao) {
        var cmd = new RemoteCommand("MrvService", "UserContainsTeams", "/MrvCustomizations/");
        cmd.SetParameter("teams", nomeFuncao);
        var resultado = cmd.Execute();
        if (resultado.Success) {
            return resultado.ReturnValue.Mrv.Found;
        } else {
            alert("Erro na consulta do método UserContainsTeams");
            formularioValido = false;
            return false;
        }
    }

    //Verificar se o usuário é ADM, se não for, bloquear nome e codigo.
    if (!crmForm.VerificaFuncao("Administradores")) {
        crmForm.all.new_name.Disabled = true;
        crmForm.all.new_codigo_sap6.Disabled = true;
    }
} catch (error) {
    formularioValido = false;
	alert("Ocorreu um erro no formulário.\n" + error.description);
}