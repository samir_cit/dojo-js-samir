﻿if (crmForm.all.new_name.IsDirty || crmForm.all.new_tipodeunidade.IsDirty)
{
  //Limita o nome do bloco a 30 caracteres
  var tamNomeBloco = crmForm.all.new_name.DataValue.length;
  if(tamNomeBloco > 30)
  {
    alert("O nome do bloco não pode ultrapassar 30 caracteres.\n Tamanho atual: " + tamNomeBloco + "caracteres");
    crmForm.all.new_name.SetFocus();
    event.returnValue = false;
  }

  //Força o usuário a usar bloco no nome do bloco
  var nomeBloco = crmForm.all.new_name.DataValue.toUpperCase();
  var new_tipodeunidade = parseFloat(crmForm.all.new_tipodeunidade.DataValue);
  var palavra = "BLOCO";
  switch (new_tipodeunidade) {
      case 1:
      case 2:
          palavra = "BLOCO"; break;
      case 3:
          palavra = "BLOCO"; break;
      case 4:
          palavra = "QUADRA";
          crmForm.all.new_tipo.DataValue = 4;
       break;
  }

  if (!(nomeBloco.match(palavra))) {
          alert("O nome do bloco deve conter a palavra " + palavra);
          crmForm.all.new_name.SetFocus();
          event.returnValue = false;
      }
  }

//

// Transformar caracteres em maiusculos------------------------------------//
var elm = document.getElementsByTagName("input");
for (i = 0; i< elm.length; i++)
{
    if (elm[i].type == "text")
        if (elm[i].DataValue != null)
        { 
             try{
              elm[i].DataValue = elm[i].DataValue.toUpperCase(); }
              catch (e) {}
         }
}

 if(crmForm.all.new_repeticoes.DataValue == null || crmForm.all.new_repeticoes.DataValue == 0)
    {
        crmForm.all.new_repeticoes.DataValue = 1;
    }