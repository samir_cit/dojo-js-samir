﻿formularioValido = true;
try {

    function loadScript() {
        var oHead = document.getElementsByTagName('head')[0];
        var oScript = document.createElement('script');
        oScript.type = "text/javascript";
        oScript.src = "/_static/Base.js";

        oScript.onreadystatechange = function() {
            if (this.readyState == 'complete' || this.readyState == 'loaded') {
                Load();
            }
        }
        oHead.appendChild(oScript);
    }

    loadScript();
    
    function Load() {

        StatusAprovacao = {
            SIM: 1,
            NAO: 2
        }

        StatusViabilidade = {
            INATIVA: 1,
            FECHADA: 2,
            ATIVA: 3
        }
        
        TipoEmpreendimento = {
	        Parque: 1,
	        Village: 2,
	        Spazio: 3
	    }

	    TipoBloco = {
	        Bloco: 1,
	        Torre: 2,
	        Casa: 3,
	        Quadra: 4
	    }

        /* Valores Defauts - Solicitação da Área */
        if (crmForm.FormType == TypeCreate) {
            crmForm.all.new_indiceaptocob.DataValue = 1.2;
            crmForm.all.new_indiceterracocob.DataValue = 2;
            crmForm.all.new_indiceprecovendacob.DataValue = 1.4;
            crmForm.all.new_indicevarandaequiv.DataValue = 0.5;
            crmForm.all.new_indiceterracoequiv.DataValue = 0.16;
        }
        //Se for update e existir viabilidade associada
        if ((crmForm.FormType == TypeUpdate) && (crmForm.new_viabilidadeid.DataValue != null)) {
            var attributes = "new_statusaprovacao;statuscode;new_viabilidadegerada";
            var cmd = new RemoteCommand("MrvService", "GetEntity", "/MRVCustomizations/");
            cmd.SetParameter("entity", "new_viabilidade");
            cmd.SetParameter("primaryField", "new_viabilidadeid");
            cmd.SetParameter("entityId", crmForm.new_viabilidadeid.DataValue[0].id);
            cmd.SetParameter("fieldList", attributes);
            var oResult = cmd.Execute();
            if (oResult.Success) {
                if (oResult.ReturnValue.Mrv.new_statusaprovacao != null && oResult.ReturnValue.Mrv.statuscode != null) {
                    //Caso a viabilidade tenha sido gerada (consequentemente tem que estar ativa) os campos
                    //"Tipo de unidade a ser gerada", "Identificação Bloco" e "Tipo" serão bloqueados para edição
                    if (oResult.ReturnValue.Mrv.new_statusaprovacao == StatusAprovacao.SIM && oResult.ReturnValue.Mrv.statuscode == StatusViabilidade.ATIVA) {
                        crmForm.all.new_tipodeunidade.Disabled = true;
                        crmForm.all.new_name.Disabled = true;
                        crmForm.all.new_tipo.Disabled = true;
                    }
                }
                // Caso a viabilidade está gerada será bloqueada o campo "Repetições"
                if (oResult.ReturnValue.Mrv.new_viabilidadegerada != null) {
                    if (oResult.ReturnValue.Mrv.new_viabilidadegerada)
                        crmForm.all.new_repeticoes.Disabled = true;
                }
            }
        }
        crmForm.VerificaFuncao = function(nomeFuncao) {
            var cmd = new RemoteCommand("MrvService", "UserContainsTeams", "/MrvCustomizations/");
            cmd.SetParameter("teams", nomeFuncao);
            var resultado = cmd.Execute();
            if (resultado.Success) {
                return resultado.ReturnValue.Mrv.Found;
            } else {
                alert("Erro na consulta do método UserContainsTeams");
                formularioValido = false;
                return false;
            }
        }

        crmForm.ObtemEmpreendimentoId = function() {
            var attributes = "new_viabilidadeid1";
            var cmd = new RemoteCommand("MrvService", "GetEntity", "/MRVCustomizations/");
            cmd.SetParameter("entity", "new_viabilidade");
            cmd.SetParameter("primaryField", "new_viabilidadeid");
            cmd.SetParameter("entityId", crmForm.new_viabilidadeid.DataValue[0].id);
            cmd.SetParameter("fieldList", attributes);

            var resultado = cmd.Execute();

            if (crmForm.TratarRetornoRemoteCommand(resultado, "GetEntity")) {
                return resultado.ReturnValue.Mrv.new_viabilidadeid1;
            }

            return null;
        }

        crmForm.ObtemTipoEmpreendimento = function(empreendimentoId) {
            var attributes = "new_tipo";
            var cmd = new RemoteCommand("MrvService", "GetEntity", "/MRVCustomizations/");
            cmd.SetParameter("entity", "new_empreendimento");
            cmd.SetParameter("primaryField", "new_empreendimentoid");
            cmd.SetParameter("entityId", empreendimentoId);
            cmd.SetParameter("fieldList", attributes);

            var resultado = cmd.Execute();

            if (crmForm.TratarRetornoRemoteCommand(resultado, "GetEntity")) {
                return resultado.ReturnValue.Mrv.new_tipo;
            }

            return null;
        }

        crmForm.BloqueiaTipoBlocoVillage = function() {
            var empreendimentoid = crmForm.ObtemEmpreendimentoId();
            if (empreendimentoid != null) {
                var tipoEmpreendimento = crmForm.ObtemTipoEmpreendimento(empreendimentoid);

                if ((tipoEmpreendimento != null) && (tipoEmpreendimento == TipoEmpreendimento.Village)) {
                    crmForm.all.new_tipo.DataValue = TipoBloco.Casa;
                    crmForm.all.new_tipo.Disabled = true;
                    crmForm.all.new_tipo.ForceSubmit = true;
                }
            }
        }

        crmForm.BloqueiaTipoBlocoVillage();

        function DisabledField(Fields) {
            var tmp = Fields.split(";");
            for (index = 0; index < tmp.length; index++) {
                var object = eval("crmForm.all." + tmp[index])
                object.Disabled = true;
            }
        }

        //Caso o usuário pertença a equipe Viabilidade engenharia, ele não poderá alterar nenhum campo da viabilidade
        if (crmForm.VerificaFuncao("Viabilidade Engenharia")) {
            if (crmForm.FormType == TypeUpdate) {
                var todosAtributos = document.getElementsByTagName("label");
                for (var i = 0; i < todosAtributos.length; i++) {
                    if (todosAtributos[i].htmlFor != "")
                        DisabledField(todosAtributos[i].htmlFor.replace("_ledit", ""));
                }
            }
        }

        crmForm.ConfiguraLoockupCurvaVendas = function() {
            var oParam = "objectTypeCode=10257&filterDefault=false&attributesearch=new_name&_tipo=2";
            var typeCode = "10257";
            if (ORG_UNIQUE_NAME == "QAS")
                typeCode = "10264";
            crmForm.FilterLookup(crmForm.all.new_curvadevendasid, typeCode, "/MRVWeb/FilterLookup/FilterLookup.aspx", oParam);
        }
        
        crmForm.ConfiguraLoockupCurvaVendas();
    }
}
catch (error) {
    formularioValido = false;
    alert("Ocorreu um erro no formulário.\n" + error.description);
}
