﻿formularioValido = true;
try {
    function ContratoDevolvido() {
        debugger;
        this.Checado = function() {
            return (crmForm.all.new_contrato_devolvido.DataValue != null) && crmForm.all.new_contrato_devolvido.DataValue == 1;
        }
        this.Limpar = function() {
            crmForm.all.new_data_devolucao.DataValue = null;
            crmForm.all.new_motivo_devolucao.DataValue = null;
        }
        this.Habilitar = function(valor) {
            crmForm.all.new_data_devolucao.Disabled = valor;
            crmForm.all.new_motivo_devolucao.Disabled = valor;
        }
    }

    function DesabilitarSePreenchido(atributo) {
        var atributoCRM = eval('crmForm.all.' + atributo);
        if (atributoCRM.DataValue != null)
            atributoCRM.Disabled = true;
    }

    crmForm.RecuperarInformacoesOportunidade = function() {
        if (crmForm.all.new_oportunidadeid.DataValue != null) {
            var cmd = new RemoteCommand("MrvService", "RecuperarInformacoesOportunidade", "/MRVCustomizations/");
            cmd.SetParameter("oportunidadeId", crmForm.all.new_oportunidadeid.DataValue[0].id);
            var resultado = cmd.Execute();
            if (crmForm.TratarRetornoRemoteCommand(resultado, "RecuperarInformacoesOportunidade")) {
                //Contrato
                if (resultado.ReturnValue.Mrv.ContratoSAP) {
                    crmForm.all.new_codigo_contrato.DataValue = resultado.ReturnValue.Mrv.ContratoSAP.toString();
                    crmForm.all.new_codigo_contrato.ForceSubmit = true;
                }
                else
                    crmForm.all.new_codigo_contrato.DataValue = null;

                //Produto
                if (crmForm.all.new_oportunidadeid.IsDirty && (resultado.ReturnValue.Mrv.ProdutoId && resultado.ReturnValue.Mrv.ProdutoNome)) {
                    crmForm.AddValueLookup(crmForm.all.new_produtoid, resultado.ReturnValue.Mrv.ProdutoId, "1024", resultado.ReturnValue.Mrv.ProdutoNome)
                    crmForm.all.new_produtoid.ForceSubmit = true;
                }
            }
        }
    }

    crmForm.AlteraStatus = function() {
        if (crmForm.all.new_data_recebimento.DataValue != null)
            crmForm.all.statuscode.DataValue = 4;
        if (crmForm.all.new_contrato_devolvido.DataValue != null && crmForm.all.new_contrato_devolvido.DataValue == 1)
            crmForm.all.statuscode.DataValue = 3;
        crmForm.all.statuscode.ForceSubmit = true;
    }

    function Load() {
        contratoDevolvido = new ContratoDevolvido();
        crmForm.all.new_clienteid.FireOnChange();
        crmForm.all.new_contrato_devolvido.FireOnChange();
        //Desabilitar campos que estejam preenchidos.
        if (crmForm.FormType != TypeCreate) {
            DesabilitarSePreenchido("new_clienteid");
            DesabilitarSePreenchido("new_oportunidadeid");
            DesabilitarSePreenchido("new_data_envio");
            DesabilitarSePreenchido("new_observacao");
            DesabilitarSePreenchido("new_data_recebimento");
            DesabilitarSePreenchido("new_recebido_por");
            DesabilitarSePreenchido("new_data_devolucao");
            DesabilitarSePreenchido("new_motivo_devolucao");

            var status = document.getElementById("EntityStatusText").innerText;
            document.getElementById("EntityStatusText").innerText = status + " - " + crmForm.all.statuscode.SelectedText;
        }
    }

    function loadScript() {
        var oHead = document.getElementsByTagName('head')[0];
        var oScript = document.createElement('script');
        oScript.type = "text/javascript";
        oScript.src = "/_static/Base.js";

        oScript.onreadystatechange = function() {
            if (this.readyState == 'complete' || this.readyState == 'loaded') {
                Load();
            }
        }
        oHead.appendChild(oScript);
    }

    loadScript();
} catch (error) {
    formularioValido = false;
    alert("Ocorreu um erro no formulário.\n" + error.description);
}