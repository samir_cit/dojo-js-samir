﻿formularioValido = true;
try {
    //Adiciona Script para ser usado no click do botão
    var elm = document.createElement("script");
    elm.src = "/_static/_grid/cmds/util.js";
    document.appendChild(elm);

    var oScript = document.createElement('script');
    oScript.type = "text/javascript";
    oScript.src = "/_static/Base.js";
    var oHead = document.getElementsByTagName('head')[0];
    oHead.appendChild(oScript);
    
    //-------Faz o cálculo da duração real (dias)-----------------------//
    crmForm.AjustaDuracaoReal = function() {
        if (crmForm.all.new_data_real_inicio.DataValue != null && crmForm.all.new_data_real_conclusao.DataValue != null) {
            var dtIni = new Date(crmForm.all.new_data_real_inicio.DataValue);
            var dtFim = new Date(crmForm.all.new_data_real_conclusao.DataValue);
            if (dtIni <= dtFim) {
                var dtAux = new Date(dtFim - dtIni);
                //getTime() retorna milisegundos, faço a conversão para dias.
                crmForm.all.new_duracao_real.DataValue = (dtAux.getTime() / 86400000);
            }
            else {
                alert('A data real de conclusão não pode ser menor que a data real de início.');
                crmForm.all.new_duracao_real.DataValue = null;
                crmForm.all.new_data_real_conclusao.DataValue = null;
            }
        }
        else
            crmForm.all.new_duracao_real.DataValue = null;
    }

    //---Caso estejam nulos, prrenche os campos de duração com 0 (zero) ---------------------------------//
    crmForm.AjustaCamposDuracaoNulos = function() {
        if (crmForm.all.new_duracao_atualizada.DataValue == null)
            crmForm.all.new_duracao_atualizada.DataValue = 0;

        if (crmForm.all.new_duracao_dias.DataValue == null)
            crmForm.all.new_duracao_dias.DataValue = 0;
    }
    crmForm.AjustaCamposDuracaoNulos();

} catch (error) {
    formularioValido = false;
    alert("Ocorreu um erro no formulário.\n" + error.description);
}