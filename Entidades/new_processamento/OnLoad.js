﻿var formularioValido = true;
try {

    //Adiciona Script para ser usado no click do botão
    var elm = document.createElement("script");
    elm.src = "/_static/_grid/cmds/util.js";
    document.appendChild(elm);

    TipoProcessamento =
        {
            NIVEL_RETENCAO: "9",
            COMERCIAL_CSG: "10",
			CRONOGRAMA_OBRA: "17",
			PREVISAO_TERMINO: "18"
        }

        Equipe = 
        {
            EMPREENDIMENTO_ESTRATEGIA_NEGOCIACAO: "Empreendimento - Estratégia de Negociação",
            CO_CSG: "CO_CSG",
			ENGENHARIA_PRAZOS: "Engenharia - Prazos"
        }

    function Load() {
        crmForm.all.new_tipo.Disabled = crmForm.FormType != TypeCreate;
        crmForm.ValidarPermissao(crmForm.all.new_tipo.DataValue);
    }

    crmForm.ValidarPermissao = function(value) {
        var obj = document.getElementById("IFRAME_importas");
        var desabilitarCampos = false;
        var tipoProcessamento = value;

        //seleciona a equipe
        if (value == TipoProcessamento.NIVEL_RETENCAO) {
            desabilitarCampos = !crmForm.UsuarioPertenceEquipe(Equipe.EMPREENDIMENTO_ESTRATEGIA_NEGOCIACAO);
            tipoProcessamento = TipoProcessamento.NIVEL_RETENCAO;
        } else if (value == TipoProcessamento.COMERCIAL_CSG) {
            desabilitarCampos = !crmForm.UsuarioPertenceEquipe(Equipe.CO_CSG);
            tipoProcessamento = TipoProcessamento.COMERCIAL_CSG;
        } else if (value == TipoProcessamento.CRONOGRAMA_OBRA || value == TipoProcessamento.PREVISAO_TERMINO) {
            desabilitarCampos = !crmForm.UsuarioPertenceEquipe(Equipe.ENGENHARIA_PRAZOS);
        }

        if (obj != null) {
            if (crmForm.FormType == TypeCreate) {
                var url = "/MrvWeb/ImportarArquivoCarga/Default.aspx?entidade=new_processamento&desabilitarCampos=" + desabilitarCampos.toString();
                obj.src = url;
            } else {
                obj.src = obj.src + "&TipoProcessamento=" + tipoProcessamento;
            }
        }
    }

    crmForm.Recarregar = function() {
        crmForm.SubmitCrmForm(1, true, true, false);
    }

    
    function loadScript() {
        var oHead = document.getElementsByTagName('head')[0];
        var oScript = document.createElement('script');
        oScript.type = "text/javascript";
        oScript.src = "/_static/Base.js";

        oScript.onreadystatechange = function() {
            if (this.readyState == 'complete' || this.readyState == 'loaded') {
                Load();
            }
        }
        oHead.appendChild(oScript);
    }

    loadScript();

} catch (error) {
    formularioValido = false;
    alert("Ocorreu um erro no formulário.\n" + error.description);
}