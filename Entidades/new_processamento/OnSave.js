﻿var tipoProcessamento = crmForm.all.new_tipo.SelectedText;
var nomeEquipe = tipoProcessamento;
if (tipoProcessamento == "Nível de Retenção") {
	nomeEquipe = Equipe.EMPREENDIMENTO_ESTRATEGIA_NEGOCIACAO;
} else if (tipoProcessamento == "Comercial CSG") {
	nomeEquipe = Equipe.CO_CSG;
} else if (tipoProcessamento == "Cronograma de Obra" || tipoProcessamento == "Previsão de Término") {
	nomeEquipe = Equipe.ENGENHARIA_PRAZOS;
}
if (!crmForm.UsuarioPertenceEquipe(nomeEquipe)) {
   alert("Você deve pertercer à equipe " + nomeEquipe + " para poder salvar esse registro. Solicite permissão.");
   crmForm.all.new_tipo.DataValue = null;
   event.returnValue = false;
   return false;
}
var elm = document.getElementsByTagName("input");
for (i = 0; i < elm.length; i++) {
        if (elm[i].type == "text")
            if (elm[i].DataValue != null) {
            try {
                elm[i].DataValue = elm[i].DataValue.toUpperCase();
            }
            catch (e) { }
        }
}