﻿crmForm.OportunidadeEstaFechada = function() {
    return crmForm.statuscode.DataValue != 1;
}

crmForm.Totaliza = function() {
    var valor = 0;

    if (crmForm.all.new_quantidadeparcelas1.DataValue != null && crmForm.all.new_valorparcela1.DataValue != null)
        valor += crmForm.all.new_quantidadeparcelas1.DataValue * crmForm.all.new_valorparcela1.DataValue;

    if (crmForm.all.new_quantidadeparcelas2.DataValue != null && crmForm.all.new_valorparcela2.DataValue != null)
        valor += crmForm.all.new_quantidadeparcelas2.DataValue * crmForm.all.new_valorparcela2.DataValue;

    if (crmForm.all.new_quantidadeparcelas3.DataValue != null && crmForm.all.new_valorparcela3.DataValue != null)
        valor += crmForm.all.new_quantidadeparcelas3.DataValue * crmForm.all.new_valorparcela3.DataValue;

    crmForm.all.new_valortotalcontrato.DataValue = valor;
}


function DesabilitaFormulario() {

    for (var index = 0; index < crmForm.all.length; index++)
        if (crmForm.all[index].Disabled != null)
        crmForm.all[index].Disabled = true;
}

if (crmForm.OportunidadeEstaFechada(crmForm.ObjectId)) {
    DesabilitaFormulario();
  
}