﻿crmForm.all.new_valorempreendimento.DataValue = 0;
if (crmForm.all.new_empreendimentoid.DataValue != null) {
    var cmd = new RemoteCommand("MrvService", "ObterValorEmpreendimento", "/MRVCustomizations/");
    cmd.SetParameter("idEmpreendimento", crmForm.all.new_empreendimentoid.DataValue[0].id);
    var oResult = cmd.Execute();
    if (oResult.Success) 
    {
        if (oResult.ReturnValue.Mrv.valor)
        {
         crmForm.all.new_valorempreendimento.DataValue = parseFloat(oResult.ReturnValue.Mrv.valor);
        }   
    }
}