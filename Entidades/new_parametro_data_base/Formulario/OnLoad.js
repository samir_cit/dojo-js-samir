﻿formularioValido = true;
var IframeMaster = "IFRAME_Plano_Financiamento";
try {
    function loadScript() {
        var oHead = document.getElementsByTagName('head')[0];
        var oScript = document.createElement('script');
        oScript.type = "text/javascript";
        oScript.src = "/_static/Base.js";

        oScript.onreadystatechange = function() {
            if (this.readyState == 'complete' || this.readyState == 'loaded') {
                Load();
            }
        }
        oHead.appendChild(oScript);
    }

    loadScript();

    function Load() {
    
        MesDataBase = ["JANEIRO", "FEVEREIRO", "MARÇO", "ABRIL", "MAIO", "JUNHO", "JULHO", "AGOSTO",
                           "SETEMBRO", "OUTUBRO", "NOVEMBRO", "DEZEMBRO"];

        DATA_BASE_DIA_PERMITIDO = 5;
        MENSAGEM_EXISTE_PARAMETRO_DATA_BASE = "Existe um parametro criado para esta data base. Favor informar nova data base.";
        MENSAGEM_DATA_BASE_INCORRETA = "A Data Base informada está incorreta, apenas dia " + DATA_BASE_DIA_PERMITIDO + " do mês é permitido";
        MENSAGEM_NOVA_COTA_CSG = "Não é possível atualizar a Cota CSG abaixo do Número Total Vendas CSG";
        
        crmForm.ObterDataBaseAtual = function() {
            if (crmForm.all.new_data_base.DataValue == null) {
                crmForm.all.new_data_base.DataValue = new Date().setDate(DATA_BASE_DIA_PERMITIDO);
                new_data_base_onchange0();
            }
        }

        crmForm.ValidarDataBase = function() {
            if (crmForm.all.new_data_base.DataValue != null) {
                if (crmForm.all.new_data_base.DataValue.getDate() != DATA_BASE_DIA_PERMITIDO) {
                    alert(MENSAGEM_DATA_BASE_INCORRETA);
                    crmForm.all.new_data_base.DataValue = null;
                }

                if ((crmForm.FormType == TypeCreate || crmForm.FormType == TypeUpdate) && (crmForm.all.new_data_base.DataValue != null)) {

                    var cmd = new RemoteCommand("MrvService", "VerificarDataBaseParametro", "/MrvCustomizations/");

                    cmd.SetParameter("parametroDataBaseId", crmForm.ObjectId != null ? crmForm.ObjectId : "");
                    cmd.SetParameter("dataBase", crmForm.all.new_data_base.DataValue.toLocaleDateString());

                    var resultado = cmd.Execute();

                    if (crmForm.TratarRetornoRemoteCommand(resultado, "VerificarDataBaseParametro")) {
                        if (resultado.ReturnValue.Mrv.ExisteParametro) {
                            alert(MENSAGEM_EXISTE_PARAMETRO_DATA_BASE);
                            crmForm.all.new_data_base.DataValue = null;
                        }
                    }
                }
            }
        }

        crmForm.ObterDataBaseAtual();

        function OnReadyStateChange() {
            if (document.frames(IframeMaster).frameElement.readyState == 'complete') {
                if ((crmForm.GetElementsByClassName("ms-crm-MenuBar-Left", "ul", document.frames(IframeMaster).document) != null) &&
			    (crmForm.GetElementsByClassName("ms-crm-MenuBar-Left", "ul", document.frames(IframeMaster).document)).length != 0) {
                    crmForm.GetElementsByClassName("ms-crm-MenuBar-Left", "ul", document.frames(IframeMaster).document)[0].children[0].attachEvent("onclick", OnclickChange);
                }
            }
        }

        function OnclickChange() {
            if (document.frames(IframeMaster).frameElement.readyState == 'complete') {
                eval('crmForm.all.' + IframeMaster).src = ObterEnderecoIframe();
                AjustandoIframe(IframeMaster);
            }
        }

        function ObterEnderecoIframe() {
            if (crmForm.ObjectId != null) {

                var oId = crmForm.ObjectId;
                var oType = crmForm.ObjectTypeCode;
                var security = crmFormSubmit.crmFormSubmitSecurity.value;

                return "/" + ORG_UNIQUE_NAME + "/userdefined/areas.aspx?oId=" + oId + "&oType=" + oType + "&security=" + security + "&roleOrd=1&tabSet=areanew_new_parametro_data_base_new_plano";
            }
            else {
                return "about:blank";
            }
        }

        function AjustandoIframe(iframeID) {
            if (document.getElementById(iframeID).contentWindow.document.readyState != "complete") {
                window.setTimeout(function() { AjustandoIframe(iframeID) }, 10);
            }
            else {
                // Alterar a cor do fundo
                document.getElementById(iframeID).contentWindow.document.body.style.backgroundColor = "#eef0f6";
                // Remova a borda esquerda
                document.getElementById(iframeID).contentWindow.document.body.all(0).style.borderLeftStyle = "none";
                // Remove padding
                document.getElementById(iframeID).contentWindow.document.body.all(0).all(0).all(0).all(0).style.padding = "0px";
                // Deixa a célula com toda a largura do IFRAME
                document.getElementById(iframeID).contentWindow.document.body.all(0).style.width = "100%"

                // mostra o IFrame
                document.getElementById(iframeID).style.display = "block";
            }
        }
    
        document.frames(IframeMaster).frameElement.attachEvent("onreadystatechange", OnReadyStateChange);

        if (crmForm.FormType != TypeCreate) {
            eval('crmForm.all.' + IframeMaster).src = ObterEnderecoIframe();
            AjustandoIframe(IframeMaster);
        }
        else {

            document.getElementById(IframeMaster + "_d").parentNode.parentNode.style.display = "none";
        } if (crmForm.FormType != TypeCreate) {
            eval('crmForm.all.' + IframeMaster).src = ObterEnderecoIframe();
            AjustandoIframe(IframeMaster);
        }
        else {

            document.getElementById(IframeMaster + "_d").parentNode.parentNode.style.display = "none";
        }

        crmForm.ValidarNovaQuantidadeCotaCSG = function() {
            if (crmForm.all.new_cota_csg.DataValue != null &&
                crmForm.all.new_total_vendas_csg.DataValue != null) {
                novaCotaCSG = crmForm.all.new_cota_csg.DataValue;
                totalVendasCSG = crmForm.all.new_total_vendas_csg.DataValue;

                if (totalVendasCSG > novaCotaCSG) {
                    alert(MENSAGEM_NOVA_COTA_CSG);
                    event.returnValue = false;
                    return false;
                }
            }
        }
    }
    
} catch (error) {
    formularioValido = false;
    alert("Ocorreu um erro no formulário.\n" + error.description);
}