﻿try {
    if (!formularioValido) {
        alert("Ocorreu um erro no formulário. Impossível salvar.");
        event.returnValue = false;
        return false;
    }
    
    if (crmForm.all.new_data_base.DataValue != null)
    {
        mes = crmForm.all.new_data_base.DataValue.getMonth();
        ano = crmForm.all.new_data_base.DataValue.getYear();
        crmForm.all.new_nome.DataValue = MesDataBase[mes] + " / " + ano;
        crmForm.all.new_nome.ForceSubmit = true;
    }
	
	if (crmForm.IsDirty)
	{
        crmForm.ValidarNovaQuantidadeCotaCSG();
	}
            
} catch (error) {
    alert("Ocorreu um erro no formulário.\n" + error.description);
    event.returnValue = false;
    return false;
}