﻿if (crmForm.all.new_imprensa.DataValue) {
    crmForm.EscondeCampo("new_qual_imprensa", false);
    crmForm.SetFieldReqLevel("new_qual_imprensa", true);
}
else {
    crmForm.EscondeCampo("new_qual_imprensa", true);
    crmForm.all.new_qual_imprensa.DataValue = null;
    crmForm.SetFieldReqLevel("new_qual_imprensa", false);

    crmForm.EscondeCampo("new_outros_imprensa", true);
    crmForm.all.new_outros_imprensa.DataValue = null;
    crmForm.SetFieldReqLevel("new_outros_imprensa", false);
}