﻿function regIsNumber(valor) {
    var reg = new RegExp("^[-]?[0-9]+[\.]?[0-9]+$");
    return reg.test(valor);
}

if (crmForm.all.new_idreclameaqui.DataValue != null) {
    var completeNumber = "";
    var numberIDreclameAqui = crmForm.all.new_idreclameaqui.DataValue.toString();

    if (numberIDreclameAqui.length <= 9) {
        for (var i = numberIDreclameAqui.length; i < 9; i++) {
            completeNumber += "0";
        }
        crmForm.all.new_idreclameaqui.DataValue = completeNumber + crmForm.all.new_idreclameaqui.DataValue;
    }

    if (!regIsNumber(crmForm.all.new_idreclameaqui.DataValue)) {
        crmForm.all.new_idreclameaqui.DataValue = null;
        alert('O campo Id Reclame Aqui só permite números.');

    }
    else {
        var cmd = new RemoteCommand("MrvService", "ObterDadosOcorrenciaReclameAqui", "/MRVCustomizations/");
        cmd.SetParameter("reclameAquiID", crmForm.all.new_idreclameaqui.DataValue.toString());
        var oResult = cmd.Execute();

        if (oResult.Success) {
            var querystring = location.search.substring(1, location.search.length);
            var args = querystring.split('&');
            var ocorrenciaID = undefined;
            for (var i = 0; i < args.length; i++) {
                var pair = args[i].split('=');
                if (pair[0] == 'id') {
                    ocorrenciaID = CrmEncodeDecode.CrmUrlDecode(pair[1]);
                }
            }

            if (oResult.ReturnValue.Mrv.IDReclameAqui != undefined && oResult.ReturnValue.Mrv.IDReclameAqui == crmForm.all.new_idreclameaqui.DataValue) {
                if (ocorrenciaID != undefined) {
                    ocorrenciaID = ocorrenciaID.toUpperCase().replace('{', '').replace('}', '');
                }
                var ocorrenciaExistente = oResult.ReturnValue.Mrv.OcorrenciaID.toUpperCase();
                if (ocorrenciaExistente != ocorrenciaID) {
                    alert('Já existe ocorrência com o ID Reclame Aqui informado.');
                    crmForm.all.new_idreclameaqui.DataValue = "";
                    crmForm.all.new_idreclameaqui.SetFocus();
                }
            }
        }
    }
}