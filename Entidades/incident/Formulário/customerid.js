﻿if (crmForm.all.customerid.DataValue != null) {

    var type = crmForm.all.customerid.DataValue[0].typename;

    switch (type) {
        case 'account':
            crmForm.processaCliente(); break;
        case 'contact':
            crmForm.processaContato(); break;
    }
}
else {
    //Desabilitando o campo oportunidade
    crmForm.all.new_opportunityid.DataValue = null;
    crmForm.SetFieldReqLevel('new_opportunityid', false);
    crmForm.all.new_opportunityid.Disabled = true;
    crmForm.all.productid.DataValue = null;
}