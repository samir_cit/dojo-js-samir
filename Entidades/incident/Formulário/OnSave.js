﻿try {
    var origemSistemaCredito = 200015;
	
    /* Não permite salvar se o campo 'Recebido Por' estiver marcado a opção 'Sistema de Crédito' e o form for do tipo Create.*/
    if((crmForm.FormType == TypeCreate && crmForm.all.caseorigincode.DataValue == origemSistemaCredito) || 
    (crmForm.FormType == TypeUpdate && crmForm.all.caseorigincode.IsDirty && crmForm.all.caseorigincode.DataValue == origemSistemaCredito))
	{
		alert("Não é permitido criar a ocorrência com informação do campo 'Recebido Por' igual a 'Sistema de Crédito'");
		event.returnValue = false;
		return false;
	}
	
    //se a ocorrência for resgate de promoção
    if (crmForm.all.subjectid.DataValue[0].name.toLowerCase().indexOf('solicitação de prêmio') == 0)
    {
        if (crmForm.all.casetypecode.DataValue == null ||
              (crmForm.all.casetypecode.DataValue != null && 
               crmForm.all.casetypecode.DataValue != 1 && 
               crmForm.all.casetypecode.DataValue != 2))
        {
            if (crmForm.all.casetypecode.DataValue == 200001)
                alert('Para os casos de informação sobre status do prêmio solicitado, registrar ocorrência com o assunto Status solicitação de prêmio.');
            else
                alert('Para ocorrências de resgate de promoção o tipo de chamado deve ser Requisição ou Reclamação.');
            event.returnValue = false;
            return false;
        }
    
        //alerta para inclusão de participantes da promoção
        if (event.Mode != EVENT_MODE_ONSAVE_INCIDENT_CANCEL && numPromocoes <= 0) {
            if (event.Mode == EVENT_MODE_ONSAVE_SAVE_AND_CLOSE || event.Mode == EVENT_MODE_ONSAVE_SAVE_AND_NEW) {
                event.returnValue = false;
                crmForm.Save();
                return false;
            }
            else alert('É necessário informar as promoções a resgatar no menu Promoções.');
        }
    }
    
    if (crmForm.all.casetypecode.DataValue != null &&
        crmForm.all.casetypecode.DataValue == RecebidoPor.INFORMACAO && 
        crmForm.UsuarioPertenceEquipe("Núcleo - AeC")) {
        alert('ATENÇÃO ATENDIMENTO RECEPTIVO: Ocorrência criada como Informação deve ser concluída caso tenha atendido a demanda do cliente. Favor validar se esta ocorrência pode ser fechada e realize a conclusão.');
    }
    
    /// Reativação
    if (event.Mode == EVENT_MODE_ONSAVE_REACTIVATE) {
        if (!crmForm.UsuarioPertenceEquipe("Reativar Ocorrências")) {
            alert('Usuário não possui permissão para realizar esta operação.');
            event.returnValue = false;
            return false;
        }
    }

    /// Habilitar campos
    crmForm.all.contractid.Disabled = false;
    crmForm.all.productid.Disabled = false;
    crmForm.all.title.Disabled = false;
    document.getElementById("new_ocorrencia_at").disabled = false;    

    /* Ação quando uma ocorrência é 'Resolvida' */
    if (event.Mode == EVENT_MODE_ONSAVE_DEACTIVATE && crmForm.all.new_assuntoprincipalid.DataValue != null) {

        /* Verifica se o assunto selecionado possui fechamento customizado */
        var cmd = new RemoteCommand("MrvService", "PossuiMotivoFechamento", "/MRVCustomizations/");
        cmd.SetParameter("assuntoId", crmForm.all.new_assuntoprincipalid.DataValue[0].id);
        var oResult = cmd.Execute();
        
        if (crmForm.TratarRetornoRemoteCommand(oResult, "PossuiMotivoFechamento")) {
            if (oResult.ReturnValue.Mrv.Possui) {
                var retorno = openStdDlg("/MRVWEB/Ocorrencia/MotivoFechamento.aspx?assuntoId=" + crmForm.all.new_assuntoprincipalid.DataValue[0].id, null, 500, 200);
                if(retorno == null || retorno == undefined)
                {
                    alert("Operação cancelada.");
                    event.returnValue = false;
                    return false;
                }
                var type = 10170;
                crmForm.AddValueLookup(crmForm.all.new_motivo_fechamentoid, retorno.Guid, type, retorno.Name);
                crmForm.all.new_motivo_fechamentoid.ForceSubmit = true;
            }
        }
    }

    if (event.Mode == EVENT_MODE_ONSAVE_INCIDENT_CANCEL) {
        var cmd = new RemoteCommand("MrvService", "ValidarTarefasAbertas", "/MRVCustomizations/");
         cmd.SetParameter('ocorrenciaId', crmForm.ObjectId);
         var result = cmd.Execute();
         if (crmForm.TratarRetornoRemoteCommand(result, "ValidarTarefasAbertas")) {
			if (result.ReturnValue.Mrv.PossuiTarafasAberas) {
			    alert("Ainda há atividades em aberto associadas a esta ocorrência, elas devem ser encerradas para que a ocorrência possa ser fechada.");
				event.returnValue = false;
			}
         }
    }

    if(crmForm.FormType == TypeCreate || crmForm.FormType == TypeUpdate)
    {
        /// Deixar Maiusculo o Título
        crmForm.all.title.DataValue = (crmForm.all.title.DataValue != null) ? crmForm.all.title.DataValue.toUpperCase() : "";
        crmForm.all.new_regionalid.ForceSubmit = true;
    }

} catch (erro) {
    alert(erro.message);
    alert("Ocorreu um erro no formulário. Por favor capture a imagem da tela e envie para o Administrador do CRM.\n" + erro.description);
    event.returnValue = false;
    return false;
}
