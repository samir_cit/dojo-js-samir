﻿//script para ocultar Outros imprensa------------------------------------------//
if (crmForm.all.new_qual_imprensa.DataValue == 6) {
    crmForm.EscondeCampo("new_outros_imprensa", false);
    crmForm.SetFieldReqLevel("new_outros_imprensa", true);
}
else {
    crmForm.EscondeCampo("new_outros_imprensa", true);
    crmForm.all.new_outros_imprensa.DataValue = null;
    crmForm.SetFieldReqLevel("new_outros_imprensa", false);
}

//script para Mostrar / Ocultar IdReclameAqui ------------------------------------------//
if (crmForm.all.new_qual_imprensa.DataValue == 1) {
    crmForm.EscondeCampo("new_idreclameaqui", false);
    crmForm.SetFieldReqLevel("new_idreclameaqui", true);
}
else {
    //Esconde a estratégia de contato e remove o valor do picklist caso seja diferente de reclame aqui e a ocorrência já tenha sido salva.
    crmForm.EscondeCampo("new_idreclameaqui", true);
    crmForm.all.new_idreclameaqui.DataValue = null;
    crmForm.all.new_estrategia_de_contato.DataValue = null;
    crmForm.SetFieldReqLevel("new_idreclameaqui", false);
    crmForm.EscondeCampo("new_estrategia_de_contato", true);
    crmForm.SetFieldReqLevel("new_estrategia_de_contato", false);
}