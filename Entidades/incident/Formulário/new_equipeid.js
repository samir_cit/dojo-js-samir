﻿var cmd = new RemoteCommand("MrvService", "GetValuesByEntity", "/MRVCustomizations/");
cmd.SetParameter("entityName", "opportunity");
cmd.SetParameter("fieldForSearch", "new_produtoid");
cmd.SetParameter("fieldValue", crmForm.all.new_produtoid.DataValue[0].id);
cmd.SetParameter("returnFields", "new_empreendimentoid");
var oResult = cmd.Execute();


if (oResult.Success) {
    if (oResult.ReturnValue.Mrv.Found) {
        cmd = null;
        cmd = new RemoteCommand("MrvService", "GetEntity", "/MRVCustomizations/");
        cmd.SetParameter("entity", "new_empreendimento");
        cmd.SetParameter("primaryField", "new_empreendimentoid");
        cmd.SetParameter("entityId", oResult.ReturnValue.Mrv.new_empreendimentoid);
        cmd.SetParameter("fieldList", "new_name");
        oResult = null;
        oResult = cmd.Execute();
        if (oResult.Success) {
            if (oResult.ReturnValue.Mrv.Found) {
                crmForm.AddValueLookup(crmForm.all.new_empreendimentoid, oResult.ReturnValue.Mrv.new_empreendimentoid, "10001", oResult.ReturnValue.Mrv.new_name)
            }
            else
                alert("Nome do empreendimento não encontrado.");
        }
        else
            alert("Erro na consulta do nome do empreendimento.\nContate o administrador do sistema.");
    }
    else
        alert("Unidade sem Empreendimento.");
}
else {
    alert("Erro na consulta do Empreendimento da Unidade.\nContate o administrador do sistema.");
}