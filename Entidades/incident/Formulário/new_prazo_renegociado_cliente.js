﻿if (crmForm.all.new_prazo_renegociado_cliente.DataValue == null)
{
    crmForm.all.new_justificativa_prazo_renegociado.DataValue = null;
    crmForm.SetFieldReqLevel('new_justificativa_prazo_renegociado', false);    
}
else 
{
    if (crmForm.all.new_prazo_renegociado_cliente.DataValue <= crmForm.all.new_prazo_solucao_cliente.DataValue) {
        alert ("O prazo renegociado deve ser maior que o prazo de solução para cliente.");
        crmForm.all.new_prazo_renegociado_cliente.DataValue = null;
        crmForm.all.new_justificativa_prazo_renegociado.DataValue = null;
        crmForm.SetFieldReqLevel('new_justificativa_prazo_renegociado', false);    
    }
    else
        crmForm.SetFieldReqLevel('new_justificativa_prazo_renegociado', true);
}
