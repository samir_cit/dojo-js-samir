﻿if ((crmForm.all.caseorigincode.DataValue == RecebidoPor.IMPRENSA) || (crmForm.all.caseorigincode.DataValue == RecebidoPor.IMPRENSA_INTERNET)) {
    crmForm.RemoverTodosItensCampoQual();
    crmForm.EscondeCampo("new_qual_imprensa", false);
    crmForm.SetFieldReqLevel("new_qual_imprensa", true);
    crmForm.AdicionaValoCampoQual(crmForm.all.caseorigincode.DataValue);
}
else if (crmForm.all.caseorigincode.DataValue == RecebidoPor.SOCIAIS) {
    crmForm.RemoverTodosItensCampoQual();
    crmForm.EscondeCampo("new_qual_imprensa", false);
    crmForm.SetFieldReqLevel("new_qual_imprensa", true);
    crmForm.AdicionaValoCampoQual(crmForm.all.caseorigincode.DataValue);
}
else {
    crmForm.EscondeCampo("new_qual_imprensa", true);
    crmForm.SetFieldReqLevel("new_qual_imprensa", false);
    crmForm.all.new_qual_imprensa.DataValue = null;

    crmForm.EscondeCampo("new_outros_imprensa", true);
    crmForm.SetFieldReqLevel("new_outros_imprensa", false);
    crmForm.all.new_outros_imprensa.DataValue = null;
}

if (crmForm.all.caseorigincode.DataValue != RecebidoPor.SOCIAIS) {
    crmForm.EscondeCampo("new_idreclameaqui", true);
    crmForm.EscondeCampo("new_estrategia_de_contato", true);
    crmForm.SetFieldReqLevel("new_estrategia_de_contato", false);
    crmForm.SetFieldReqLevel("new_idreclameaqui", false);
    crmForm.all.new_idreclameaqui.DataValue = null;
    crmForm.all.new_estrategia_de_contato.DataValue = null;

}