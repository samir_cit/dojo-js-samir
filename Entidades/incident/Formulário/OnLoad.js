﻿formularioValido = true;
numPromocoes = 0;
try {

    RecebidoPor = {
        INFORMACAO: "200001",
        IMPRENSA: "200019",
        IMPRENSA_INTERNET: "200020",
        SOCIAIS: "200022",
        CREDITO: "200015"
    }

    ItensQual = {
        RECLAMEAQUI: "1",
        FOLHASAOPAULO: "2",
        ESTADAO: "3",
        ESTADOMINAS: "4",
        GLOBO: "5",
        OUTROS: "6",
        TWITTER: "7",
        ORKUT: "8",
        FACEBOOK: "9",
        BLOGS: "10",
        REPLICA_RECLAMEAQUI: "11"
    }

    UsuarioId = null;
    AssuntosPermitidos = null;
    EquipesDoUsuario = null;
    /* 
    Funções Locais
    */

    crmForm.RemoverTodosItensCampoQual = function () {
        var opt = crmForm.all.new_qual_imprensa;
        for (i = opt.length - 1; i >= 0; i--) {
            opt.remove(i);
        }
    }

    crmForm.AdicionaValoCampoQual = function (tipo) {
        if (tipo == RecebidoPor.IMPRENSA || tipo == RecebidoPor.IMPRENSA_INTERNET) {
            crmForm.all.new_qual_imprensa.AddOption("Estado de Minas", ItensQual.ESTADOMINAS);
            crmForm.all.new_qual_imprensa.AddOption("Folha de São Paulo", ItensQual.FOLHASAOPAULO);
            crmForm.all.new_qual_imprensa.AddOption("O Estadão", ItensQual.ESTADAO);
            crmForm.all.new_qual_imprensa.AddOption("O Globo", ItensQual.GLOBO);
            crmForm.all.new_qual_imprensa.AddOption("Outros", ItensQual.OUTROS);
        } else if (tipo == RecebidoPor.SOCIAIS) {
            crmForm.all.new_qual_imprensa.AddOption("Blogs", ItensQual.BLOGS);
            crmForm.all.new_qual_imprensa.AddOption("Facebook", ItensQual.FACEBOOK);
            crmForm.all.new_qual_imprensa.AddOption("Orkut", ItensQual.ORKUT);
            crmForm.all.new_qual_imprensa.AddOption("Reclame aqui", ItensQual.RECLAMEAQUI);
            crmForm.all.new_qual_imprensa.AddOption("Réplica reclame aqui", ItensQual.REPLICA_RECLAMEAQUI);
            crmForm.all.new_qual_imprensa.AddOption("Twitter", ItensQual.TWITTER);
            crmForm.all.new_qual_imprensa.AddOption("Outros", ItensQual.OUTROS);
        }
    }

    crmForm.AumentarTamanhoArtigo = function () {
        if (document.getElementById("showKBViewer")) {
            var checkBox = document.getElementById("showKBViewer");
            if (checkBox.checked) {
                if (document.getElementById('kbviewer')) {
                    document.getElementById('kbviewer').style.height = "300px";
                }
            }
        }
    }
    //Função para utilizar a variável EquipesDoUsuario e evitar a consulta no Servidor.
    crmForm.VerificarEquipe = function (equipe) {
        if (EquipesDoUsuario == null)
            return crmForm.UsuarioPertenceEquipe(equipe);
        else {
            var temp = EquipesDoUsuario.split(';');
            for (var i = 0; i < temp.length; i++) {
                if (temp[i] == equipe)
                    return true;
            }
        }
        return false;
    }

    crmForm.ConfiguraOcorrenciaRelacionada = function () {
        if (crmForm.all.customerid.DataValue != null) {
            var idCliente = crmForm.all.customerid.DataValue[0].id;
            var oParam = "objectTypeCode=9993&filterDefault=false&attributesearch=accountid&_accountid=" + idCliente;
            crmForm.FilterLookup(crmForm.all.new_ocorrencia_adicional, 112, "/MRVWeb/FilterLookup/FilterLookup.aspx", oParam);
        }
    }

    //Melhorias kit - Configurar lookup de ocorrencia relacionada
    crmForm.ConfiguraOcorrenciaRelacionada = function () {
        if (crmForm.all.customerid.DataValue != null) {
            var idCliente = crmForm.all.customerid.DataValue[0].id;
            var oParam = "objectTypeCode=9993&filterDefault=false&attributesearch=accountid&_accountid=" + idCliente;
            crmForm.FilterLookup(crmForm.all.new_ocorrencia_adicional, 112, "/MRVWeb/FilterLookup/FilterLookup.aspx", oParam);
        }
    }
    //obter campos que foram escolhidos para o assunto
    crmForm.ObterCamposAdicionaisPorAssunto = function () {
        if (crmForm.all.subjectid.DataValue != null) {
            var cmd = new RemoteCommand("MrvService", "ObterCamposAdicionaisPorAssunto", "/MRVCustomizations/");
            cmd.SetParameter("subjectId", crmForm.all.subjectid.DataValue[0].id);
            var resultado = cmd.Execute();
            if (crmForm.TratarRetornoRemoteCommand(resultado, "ObterCamposAdicionaisPorAssunto")) {
                var tmpAtributos = resultado.ReturnValue.Mrv.Atributos.toString().split(';');
                for (var index = 0; index < tmpAtributos.length; index++) {
                    var tmp = tmpAtributos[index].split('|');
                    if (document.getElementById(tmp[0]) != null && document.getElementById(tmp[0]) != "") {
                        crmForm.EscondeCampo(tmp[0].toLowerCase(), false);
                        if (tmp[1] == "true")
                            crmForm.SetFieldReqLevel(tmp[0].toLowerCase(), true);
                        //Projeto Kit - Configura lookup de campo ocorrencia adicional
                        if (tmp[0].toLowerCase() == "new_ocorrencia_adicional")
                            crmForm.ConfiguraOcorrenciaRelacionada();
                    }
                }
            }
            else {
                formularioValido = false;
                window.close();
            }
        }
    }

    ObterConfiguracaoUsuarioOcorrencia = function () {
        var cmd = new RemoteCommand("MrvService", "ObterConfiguracaoParaCriarOcorrencia", "/MRVCustomizations/");
        var result = cmd.Execute();
        if (crmForm.TratarRetornoRemoteCommand(result, "ObterConfiguracaoParaCriarOcorrencia")) {
            //Configura para novas ocorrências
            if (crmForm.FormType == TypeCreate)
                ConfigurarMeiosDeComunicacao(result.ReturnValue.Mrv.MeiosDeComunicacao);

            //Ou se o usuário é o proprietário e irá editar
            var isOwner = result.ReturnValue.Mrv.UsuarioId.toString().toUpperCase() == crmForm.all.ownerid.DataValue[0].id.toString().toUpperCase();
            if (crmForm.FormType == TypeUpdate) {
                var recebidoPor = crmForm.all.caseorigincode.DataValue;
                //nao permite editar caseorigincode qdo recebido pelo sistema de credito                
                if (recebidoPor == RecebidoPor.CREDITO) {
                    crmForm.all.caseorigincode.Disabled = true;
                }
                else {
                    if (isOwner) {
                        ConfigurarMeiosDeComunicacao(result.ReturnValue.Mrv.MeiosDeComunicacao);
                        for (var i = 0; i < crmForm.all.caseorigincode.Options.length; i++) {
                            if (crmForm.all.caseorigincode.Options[i].DataValue == recebidoPor)
                                crmForm.all.caseorigincode.DataValue = recebidoPor;
                        }
                    }
                }
            }
            //Caso contrário, notifica usuário e desabilita todos os campos
            if (crmForm.FormType == TypeUpdate && !isOwner)
                NotificarUsuarioNaoProprietario(result.ReturnValue.UsuarioId);

            //Carregar dados do usuario para utilizar global
            UsuarioId = result.ReturnValue.Mrv.UsuarioId;
            AssuntosPermitidos = result.ReturnValue.Mrv.AssuntosPermitidos;
            EquipesDoUsuario = result.ReturnValue.Mrv.EquipesDoUsuario;
        }
        else
            formularioValido = false;
    }

    ConfigurarMeiosDeComunicacao = function (meiosDeComunicacao) {
        while (crmForm.caseorigincode.Options.length > 0)
            crmForm.all.caseorigincode.DeleteOption(crmForm.all.caseorigincode.Options[0].DataValue);

        crmForm.all.caseorigincode.AddOption("", "");
        if (meiosDeComunicacao == "null" || meiosDeComunicacao.length == 0) {
            alert('Para criar/atualizar ocorrência é necessário fazer parte de pelo menos uma equipe de Relacionamento.');
            window.close();
        }
        else {
            var meiosTmp = meiosDeComunicacao.split(';');
            for (var index = 0; index < meiosTmp.length; index++) {
                if (meiosTmp[index].toString() != "null") {
                    var temp = meiosTmp[index].split('*');
                    crmForm.all.caseorigincode.AddOption(temp[0], temp[1]);
                }
            }
        }
    }

    crmForm.AssuntoPermitido = function (idAssunto) {
        if (AssuntosPermitidos == null || AssuntosPermitidos.length == 0)
            return false;
        else {
            var tmp = AssuntosPermitidos.split(';');
            for (var index = 0; index < tmp.length; index++) {
                if (idAssunto == tmp[index])
                    return true;
            }
        }
        return false;
    }

    NotificarUsuarioNaoProprietario = function (usuarioId) {
        Notificacao.Mostra('Somente o proprietário da ocorrência pode alterá-la.', Notificacao.ALERTA);
        crmForm.DesabilitarFormulario(true);
        crmForm.all.ownerid.Disabled = false;
        // Ocultando menu Ações
        crmForm.RemoveBotaoPadraoCRM("_MIonActionMenuClickdelete112");
        crmForm.RemoveBotaoPadraoCRM("_MIresolve");
        crmForm.RemoveBotaoPadraoCRM("_MIcancel");
        //remover botões de salvar e fechar.
        crmForm.RemoveBotaoPadraoCRM("_MBcrmFormSave");
        crmForm.RemoveBotaoPadraoCRM("_MBcrmFormSaveAndClose");
        crmForm.RemoveBotaoPadraoCRM("_MBcrmFormSubmitCrmForm59truetruefalse");
    }

    CamposMapeados = "";
    //obter todos os campos que foram mapeados para serem usados na funcionalidade Campo adicionais.
    function ObterAtributosMapeadosCamposAdicionais() {
        if (CamposMapeados == "") {
            var cmd = new RemoteCommand("MrvService", "ObterCamposMapeados", "/MRVCustomizations/");
            var resultado = cmd.Execute();
            if (crmForm.TratarRetornoRemoteCommand(resultado, "ObterCamposMapeados")) {
                CamposMapeados = resultado.ReturnValue.Mrv.Atributos.toString();
            } else {
                window.close();
                formularioValido = false;
            }
        }
        return CamposMapeados;
    }

    crmForm.EsconderAtributosCamposAdicionais = function (limpar) {
        var atributos = ObterAtributosMapeadosCamposAdicionais();
        var tmp = atributos.split(';');
        for (var index = 0; index < tmp.length; index++) {
            if (document.getElementById(tmp[index]) != null && document.getElementById(tmp[index]) != "") {
                crmForm.EscondeCampo(tmp[index], true);
                crmForm.SetFieldReqLevel(tmp[index], false);
                if (limpar)
                    eval("crmForm.all." + tmp[index]).DataValue = null;
            }
        }
    }
    crmForm.MostraCampoOcorrenciaAT = function () {
        var mostra = true;
        if (crmForm.all.new_departamentoid.DataValue == null) {
            crmForm.EscondeCampo("new_ocorrencia_at", mostra);
            return false;
        }

        //Nao exibe campo Ocorrencia At para departamento é Assistencia Tecnica
        if ("{3aa23b6e-e9db-dd11-b2a6-001b78b9d994}" == crmForm.all.new_departamentoid.DataValue[0].id.toLowerCase())
            mostra = false;
        crmForm.EscondeCampo("new_ocorrencia_at", mostra);
        if (mostra)
            document.getElementById("new_ocorrencia_at").value = "";
    }

    crmForm.LocalizaOcorrenciaAT = function () {
        if (crmForm.all.customerid.DataValue == null) {
            alert("Selecione o cliente.");
            return false;
        }
        var oDlgParams = new Object();
        oDlgParams.accountid = crmForm.all.customerid.DataValue[0].id;
        oDlgParams.name = crmForm.all.customerid.DataValue[0].name;

        var retorno = openStdDlg("/MRVWEB/Ocorrencia/ListarAssistenciaTecnica.aspx", oDlgParams, 830, 450);
        if (!IsNull(retorno)) {
            document.getElementById("new_ocorrencia_at").value = retorno.codigo;
        }
    }

    crmForm.AdicionaBotaoLookup = function (campo) {
        //Passar o nome do atributo como parâmetro
        var actnButton = new BotaoAjuda("new_ocorrencia_at");
        //Mensagem
        actnButton.Image.Title = "Clique aqui para selecionar uma Ocorrência AT.";
        //Tamanho e Altura
        actnButton.Image.Width = 21;
        actnButton.Image.Height = 18;
        //Cursor
        actnButton.Image.Cursor = "hand";
        //Imagens Over e Out
        actnButton.Image.MouseOver = "/_imgs/lookupOn.gif";
        actnButton.Image.MouseOut = "/_imgs/lookupOff.gif";
        //Função que o clique deve chamar
        actnButton.Click = crmForm.LocalizaOcorrenciaAT;
        //Adiciona actnButton imagem ao lado do campo
        actnButton.Paint();
    }

    crmForm.ExibirStatus = function () {
        //Mostra status e razão do status
        var status = document.getElementById("EntityStatusText").innerText;
        document.getElementById("EntityStatusText").innerText = status + " - " + crmForm.all.statuscode.SelectedText;
    }

    //função que configura a tela para ocorrência de assistênia técnica
    crmForm.ConfiguraAssistenciaTecnica = function () {
        crmForm.EscondeSecao("subjectid", true);
        crmForm.EscondeSecao("kbarticleid", true);
        crmForm.EscondeCampo("new_opportunityid", true);
        crmForm.DesabilitarFormulario(true);
        crmForm.RemoveBotaoPadraoCRM("action");
        crmForm.RemoveBotaoPadraoCRM("OnDemandWorkflowForm");
        crmForm.RemoveBotaoPadraoCRM("_MBlocAddFileTo5");
        crmForm.RemoveBotao("FichadoCliente");

        crmForm.ExibirStatus();
    }

    //função que configura a tela para outros tipos de ocorrência.
    crmForm.ConfiguraOutrasOcorrencias = function () {
        if (crmForm.FormType != TypeDisabled)
            crmForm.all.new_ocorrencia_at.ForceSubmit = true;

        if (crmForm.all.customerid.DataValue != null) {
            var oParam = "objectTypeCode=3&filterDefault=false&attributesearch=new_produtoidname&_customerid=" + crmForm.all.customerid.DataValue[0].id + "&isUnion=true";
            crmForm.FilterLookup(crmForm.all.new_opportunityid, 3, "/MRVWeb/FilterLookup/FilterLookup.aspx", oParam);
        }

        crmForm.all.new_ocorrencia_at.Disabled = true;
        //Se departamento igual a assistência técnica então mostra campo de ocorrência AT
        crmForm.MostraCampoOcorrenciaAT();
        crmForm.AdicionaBotaoLookup("new_ocorrencia_at");
        //Ocultar guia de reclamações
        crmForm.EscondeAba("tab4Tab", true);
        //ocultar seção assistência técninca
        crmForm.EscondeSecao("new_data_manutencao", true);
        //Removendo campos do empreendimento e bloco
        crmForm.EscondeCampo("new_empreendimentoid", true);
        crmForm.EscondeCampo("new_blocosid", true);
        //Campos obrigatórios
        crmForm.SetFieldReqLevel("new_assuntoprincipalid", 1);
        crmForm.SetFieldReqLevel("new_departamentoid", 1);

        if (crmForm.QueryString("_CreateFromType") == "1" && crmForm.FormType == TypeCreate && crmForm.all.customerid.DataValue != null) {
            //Configurando o lookup da oportunidade
            var oParam = "objectTypeCode=3&filterDefault=false&attributesearch=new_produtoidname&_customerid=" + crmForm.all.customerid.DataValue[0].id + "&isUnion=true";
            crmForm.FilterLookup(crmForm.all.new_opportunityid, 3, "/MRVWeb/FilterLookup/FilterLookup.aspx", oParam);
            //Ativando o campo oportunidade
            crmForm.all.new_opportunityid.Disabled = false;
            crmForm.TrataOportunidade();
        }

        crmForm.all.productid.Disabled = true;
        crmForm.all.contractid.Disabled = true;

        if (crmForm.all.kbarticleid.DataValue != null) {
            //Marcar exibir artigo como true
            document.getElementById("showKBViewer").checked = true;
            //Dispara onclick do checkbox Exibir artigo
            document.getElementById("showKBViewer").fireEvent('onClick');
            //Marcar exibir artigo como true
            document.getElementById("showKBViewer").checked = true;
        }

        //Retirando a obrigatoriedade do contrato.
        crmForm.SetFieldReqLevel("contractdetailid", 0);
        crmForm.SetFieldReqLevel("contractid", 0);

        // A descrição ficará habilitada somente no momento da criação da Ocorrência
        if (crmForm.FormType != TypeCreate)
            crmForm.all.new_descricaodasolucao.Disabled = true;

        if (crmForm.all.new_prazo_renegociado_cliente.DataValue == null)
            crmForm.SetFieldReqLevel('new_justificativa_prazo_renegociado', false);
        else
            crmForm.SetFieldReqLevel('new_justificativa_prazo_renegociado', true);

        ObterConfiguracaoUsuarioOcorrencia();
        crmForm.AumentarTamanhoArtigo();
        crmForm.ExibirStatus();
        crmForm.EscondeBotao('ExibirOM', true);
    }

    // Função para obrigar a informar a oportunidade se o cliente não for prospect
    crmForm.TrataOportunidade = function () {
        crmForm.SetFieldReqLevel('new_opportunityid', false);
        if (crmForm.all.customerid.DataValue != null) {
            var cmd = new RemoteCommand("MrvService", "ClienteProspect", "/MRVCustomizations/");
            cmd.SetParameter("clienteId", crmForm.all.customerid.DataValue[0].id);
            var resultado = cmd.Execute();
            if (crmForm.TratarRetornoRemoteCommand(resultado, "ClienteProspect")) {
                if (resultado.ReturnValue.Mrv.OportunidadeObrigatoria.toString() == "true")
                    crmForm.SetFieldReqLevel('new_opportunityid', true);
            }
        }
    }

    function AbrirJanelaArvoreAssuntos() {
        var attribute = crmForm.all.subjectid;
        retorno = openStdDlg("/MRVWeb/Assunto/ArvoreAssunto.aspx", null, 600, 450);

        if (typeof (retorno) != "undefined" && retorno != undefined) {
            var lookupData = new Array();
            lookupItem = new Object();
            lookupItem.id = retorno.Id;
            lookupItem.type = 129;
            lookupItem.name = retorno.Name;
            lookupData[0] = lookupItem;
            attribute.DataValue = lookupData;
            attribute.FireOnChange();
        }
    }

    crmForm.AbrirNovoArvoreAssuntos = function () {
        var attribute = crmForm.all.subjectid;
        oImg = eval('attribute' + '.parentNode.childNodes[0]');
        oImg.onclick = AbrirJanelaArvoreAssuntos;
    }
    crmForm.AbrirNovoArvoreAssuntos();

    //Adicionando obrigatoriedade nos campos assunto principal e departamento
    crmForm.AbrirLookupAssunto = function () {
        if (window.confirm("O que você deseja fazer é abrir o assunto?")) {
            AbrirJanelaArvoreAssuntos();
            //crmForm.all.subjectid.Lookup(true);
        }
    }
    crmForm.DesabilitarFormularioErro = function (mensagemErro) {
        alert(mensagemErro);
        crmForm.DesabilitarFormulario(true);
    }
    // Método acionado caso o 'Assunto' seja alterado após a criação da ocorrência */
    crmForm.AbrirPopUpMotivoReClassificacao = function () {
        var popUpReclassificacao = openStdDlg("/MRVWEB/Ocorrencia/Reclassificacao.aspx", null, 500, 340);

        if (popUpReclassificacao != undefined) {
            crmForm.all.new_descricao_reclassificacao.DataValue = popUpReclassificacao.MotivoReclassificacao + "|" + popUpReclassificacao.Descricao;
            return true;
        }
        else if (assunto != null) {
            crmForm.all.subjectid.DataValue = assunto;
            return false;
        }
    }

    crmForm.RecuperarInformacoesOportunidade = function () {
        if (crmForm.all.new_opportunityid.DataValue != null) {
            var cmd = new RemoteCommand("MrvService", "RecuperarInformacoesOportunidade", "/MRVCustomizations/");
            cmd.SetParameter("oportunidadeId", crmForm.all.new_opportunityid.DataValue[0].id);
            var resultado = cmd.Execute();
            if (crmForm.TratarRetornoRemoteCommand(resultado, "RecuperarInformacoesOportunidade")) {
                //Contrato
                if (resultado.ReturnValue.Mrv.ContratoSAP)
                    crmForm.all.new_contratosap.DataValue = resultado.ReturnValue.Mrv.ContratoSAP.toString();
                else
                    crmForm.all.new_contratosap.DataValue = null;

                if (resultado.ReturnValue.Mrv.ContratoCotacao)
                    crmForm.all.new_numero_contrato_cotacao.DataValue = resultado.ReturnValue.Mrv.ContratoCotacao.toString();
                else
                    crmForm.all.new_numero_contrato_cotacao.DataValue = null;
                crmForm.all.new_numero_contrato_cotacao.ForceSubmit = true;

                //Produto
                if (crmForm.all.new_opportunityid.IsDirty && (resultado.ReturnValue.Mrv.ProdutoId && resultado.ReturnValue.Mrv.ProdutoNome)) {
                    crmForm.AddValueLookup(crmForm.all.productid, resultado.ReturnValue.Mrv.ProdutoId, "1024", resultado.ReturnValue.Mrv.ProdutoNome)
                }

                //Regional
                if (resultado.ReturnValue.Mrv.RegionalId && resultado.ReturnValue.Mrv.RegionalName) {
                    var typeCode = "10141";
                    crmForm.AddValueLookup(crmForm.all.new_regionalid, resultado.ReturnValue.Mrv.RegionalId, typeCode, resultado.ReturnValue.Mrv.RegionalName)
                }
            }
        }
    }

    crmForm.EmpreendimentoCritico = function () {
        if (crmForm.all.new_opportunityid.DataValue != null &&
            crmForm.all.new_assuntoprincipalid.DataValue != null) {

            var cmd = new RemoteCommand("MrvService", "EmpreendimentoCritico", "/MRVCustomizations/");
            cmd.SetParameter("oportunidadeId", crmForm.all.new_opportunityid.DataValue[0].id);
            cmd.SetParameter("assuntoPrincipalId", crmForm.all.new_assuntoprincipalid.DataValue[0].id);
            cmd.SetParameter("assuntoId", crmForm.all.subjectid.DataValue[0].id);

            var resultado = cmd.Execute();

            if (crmForm.TratarRetornoRemoteCommand(resultado, "EmpreendimentoCritico")) {

                if (resultado.ReturnValue.Mrv.EmpreendimentoCritico.toString() == "true" && resultado.ReturnValue.Mrv.AssuntoPrincipalCritico.toString() == "true")
                    crmForm.all.new_empreendimento_critico.DataValue = 1
                else if (resultado.ReturnValue.Mrv.EmpreendimentoCritico.toString() == "false" || resultado.ReturnValue.Mrv.AssuntoPrincipalCritico.toString() == "false")
                    crmForm.all.new_empreendimento_critico.DataValue = 2;
                else if (resultado.ReturnValue.Mrv.EmpreendimentoCritico.toString() == "" && resultado.ReturnValue.Mrv.AssuntoPrincipalCritico.toString() == "")
                    crmForm.all.new_empreendimento_critico.DataValue = 3;
                var typeCode = "10217";
                /// Informações Críticas
                if (resultado.ReturnValue.Mrv.InformacaoCritica != "null" &&
                    resultado.ReturnValue.Mrv.InformacaoCritica != "false" &&
                    resultado.ReturnValue.Mrv.InformacaoCritica != false) {
                    window.open("http://" + window.location.host + "/userdefined/edit.aspx?id=" + resultado.ReturnValue.Mrv.InformacaoCritica + "&etc=" + typeCode, "InformacaoCritica", "toolbar=no,location=no,directories=no,status=no,menubar=no,resizable=no,width=800px,height=500px");
                }
            }
        }
    }

    crmForm.ObterTodasInformacoesCriticasAprovadas = function () {
        if (crmForm.all.new_opportunityid.DataValue != null &&
            crmForm.all.subjectid.DataValue != null) {
            window.open("/mrvweb/Ocorrencia/InformacoesCriticas.aspx?oportunidadeId=" + crmForm.all.new_opportunityid.DataValue[0].id + "&assuntoId=" + crmForm.all.subjectid.DataValue[0].id, "InformacaoCritica", "toolbar=no,location=no,directories=no,status=no,menubar=no,resizable=no,width=600px,height=400px");
        }
        else
            alert('É necessário preencher a oportunidade e o assunto para pesquisar as informações críticas.');
    }

    crmForm.processaContato = function () {

        crmForm.SetFieldReqLevel('new_opportunityid', false);

        crmForm.all.new_opportunityid.DataValue = null;
        crmForm.all.productid.DataValue = null;
        crmForm.all.new_empreendimentoid.DataValue = null;

        crmForm.EscondeCampo("new_opportunityid", true);
        crmForm.EscondeCampo("productid", true);
        crmForm.EscondeCampo("new_empreendimentoid", false);
    }

    crmForm.processaCliente = function () {
        //Configurando o lookup da oportunidade
        var oParam = "objectTypeCode=3&filterDefault=false&attributesearch=new_produtoidname&_customerid=" + crmForm.all.customerid.DataValue[0].id + "&isUnion=true";
        crmForm.FilterLookup(crmForm.all.new_opportunityid, 3, "/MRVWeb/FilterLookup/FilterLookup.aspx", oParam);

        crmForm.TrataOportunidade();

        //Ativando o campo oportunidade
        crmForm.all.new_opportunityid.Disabled = false;
        crmForm.all.new_opportunityid.DataValue = null;
        crmForm.all.productid.DataValue = null;
        crmForm.all.new_contratosap.DataValue = null;
        crmForm.all.new_regionalid.DataValue = null;
        crmForm.all.new_empreendimentoid.DataValue = null;

        crmForm.EscondeCampo("new_opportunityid", false);
        crmForm.EscondeCampo("productid", false);
        crmForm.EscondeCampo("new_empreendimentoid", true);
    }

    /* 
    Fim Funções Locais
    */

    function Load() {


        if (crmForm.FormType == TypeUpdate) {
            numPromocoes = 1;
        } else

            if (crmForm.FormType != TypeUpdate && crmForm.FormType != TypeRead && crmForm.FormType != TypeDisabled) {
                crmForm.ExcluiEntregaChaves();
            } else {
                if (crmForm.FormType != TypeUpdate && crmForm.FormType != TypeRead && crmForm.FormType != TypeDisabled) {
                    crmForm.ExcluiEntregaChaves();
                }
        }

        crmForm.ValidarExibicaoBotaoEntregaChaves();

        /// Campos Referentes a Informação Crítica
        crmForm.EscondeCampo("new_descricao_informacao_critica", true);
        crmForm.EscondeCampo("new_informacao_critica_repassada", true);

        if (crmForm.FormType != TypeCreate && crmForm.all.casetypecode.DataValue != null && crmForm.all.casetypecode.DataValue == 200003) {
            crmForm.all.ownerid.Disabled = true;
            crmForm.ConfiguraAssistenciaTecnica();
            ObterConfiguracaoUsuarioOcorrencia();
        }
        else {
            if (crmForm.FormType != TypeCreate && crmForm.all.casetypecode.DataValue != null && crmForm.all.casetypecode.DataValue == 200007) {
                crmForm.ConfiguraVistoriaEntregaChaves();
            }
            else {
            crmForm.ConfiguraOutrasOcorrencias();
            }
        }


        if (!crmForm.VerificarEquipe("Fechamento Automático Ocorrência")) {
            crmForm.EscondeSecao("new_dias_isencao", true);
            crmForm.all.new_dias_isencao.FireOnChange();
        }
        // Customizações referentes a equipes do Usuário */
        if (!crmForm.VerificarEquipe("Contato Ativo"))
            crmForm.all.casetypecode.DeleteOption(200005);

        if (!crmForm.VerificarEquipe("Telecobrança"))
            crmForm.all.casetypecode.DeleteOption(200004);

        if (crmForm.VerificarEquipe("Relacionamento - SLA Cliente")) {
            crmForm.all.new_prazo_renegociado_cliente.Disabled = false;
            crmForm.all.new_justificativa_prazo_renegociado.Disabled = false;
        }
        else {
            crmForm.all.new_prazo_renegociado_cliente.Disabled = true;
            crmForm.all.new_justificativa_prazo_renegociado.Disabled = true;
        }

        //Adicionar o botao note
        var objToolbar = new InlineToolbar("actualserviceunits");
        objToolbar.AddButton("btnAbrir", "Nova", "15%", AddNote, "/_imgs/ico_16_5.gif");

        assunto = null;
        if (crmForm.FormType == TypeUpdate && crmForm.all.subjectid.DataValue != null)
            assunto = crmForm.all.subjectid.DataValue;

        // Ocultar campos */
        crmForm.EscondeCampo("new_empreendimento_critico", true);
        crmForm.EscondeCampo("new_descricao_reclassificacao", true);

        // Campos não devem ser editáveis. */
        crmForm.all.title.Disabled = true;
        crmForm.all.new_contratosap.Disabled = true;

        //se o campo tipo de chamado for null ou diferente de "Assistência Técnica", remover o tipo.
        //Remover link dos itens de serviço
        if (crmForm.all.casetypecode.DataValue == null || (crmForm.all.casetypecode.DataValue != null && crmForm.all.casetypecode.DataValue != 200003)) {
            crmForm.all.casetypecode.DeleteOption(200003);
            //Removendo link
            if (document.getElementById("nav_new_incident_new_item_servico"))
                document.getElementById("nav_new_incident_new_item_servico").style.display = "none";
        }

        if (crmForm.all.casetypecode.DataValue == null || (crmForm.all.casetypecode.DataValue != null && crmForm.all.casetypecode.DataValue != 200007)) {
            crmForm.all.casetypecode.DeleteOption(200007);
            //Removendo link
            if (document.getElementById("nav_new_incident_new_item_servico"))
                document.getElementById("nav_new_incident_new_item_servico").style.display = "none";
        }

        //removendo o tipo de notificação do tipo de chamado.
        if (crmForm.all.casetypecode.DataValue == null || (crmForm.all.casetypecode.DataValue != null && crmForm.all.casetypecode.DataValue != 200002))
            crmForm.all.casetypecode.DeleteOption(200002);

        //ocultando campos na seção Informações do Contrato e do Produto na guia Administração
        crmForm.EscondeSecao("contractdetailid", true);
        if (crmForm.FormType == TypeCreate) {
            //ocultando o campo new_qual_imprensa
            crmForm.EscondeCampo("new_qual_imprensa", true);
            //ocultando o campo new_outros_imprensa
            crmForm.EscondeCampo("new_outros_imprensa", true);
            //ocultando o campo new_idreclameaqui
            crmForm.EscondeCampo("new_idreclameaqui", true);
        }
        else if (crmForm.all.caseorigincode.SelectedText.toLowerCase().indexOf("imprensa") > -1 || crmForm.all.caseorigincode.SelectedText.toLowerCase().indexOf("sociais") > -1) {
            crmForm.EscondeCampo("new_qual_imprensa", false);
            crmForm.SetFieldReqLevel("new_qual_imprensa", true);
            crmForm.all.new_qual_imprensa.FireOnChange();
        }
        else {
            //ocultando o campo new_qual_imprensa
            crmForm.EscondeCampo("new_qual_imprensa", true);
            //ocultando o campo new_outros_imprensa
            crmForm.EscondeCampo("new_outros_imprensa", true);
            //ocultando o campo new_idreclameaqui
            crmForm.EscondeCampo("new_idreclameaqui", true);
        }

        //Se Ocorrência já tiver sido salva o campo Estratégia de Contato é mostrado para preenchimento. No caso se o ID do Reclame aqui estiver preenchido, significa que a ocorrência já foi criada e consequentemente já foi salva.
        if (crmForm.all.new_idreclameaqui.DataValue != null) {
            crmForm.EscondeCampo("new_estrategia_de_contato", false);
            crmForm.SetFieldReqLevel("new_estrategia_de_contato", true);

        }
        else {
            crmForm.EscondeCampo("new_estrategia_de_contato", true);
        }

        //Abrir o lookup do assunto
        crmForm.all.new_assuntoprincipalid.onclick = function () {
            crmForm.AbrirLookupAssunto();
        }
        crmForm.all.new_departamentoid.onclick = function () {
            crmForm.AbrirLookupAssunto();
        }

        crmForm.EsconderAtributosCamposAdicionais(false);
        crmForm.ObterCamposAdicionaisPorAssunto();
        crmForm.EscondeCampo("new_motivo_fechamentoid", crmForm.all.new_motivo_fechamentoid.DataValue == null);

        crmForm.ValidaPermissaoBotoesDistratoEOportunidadeRenegociacao();
        crmForm.VerificaAssuntoReclassificacao();

        if (crmForm.FormType == TypeUpdate && crmForm.all.casetypecode.DataValue == 200006)
            crmForm.DesabilitarCampos("casetypecode", true);

        if (crmForm.FormType == TypeUpdate) {
            if (crmForm.all.customerid.DataValue != null) {
                if (crmForm.all.customerid.DataValue[0].typename == 'account') {
                    crmForm.EscondeCampo("new_opportunityid", false);
                    crmForm.EscondeCampo("productid", false);
                    crmForm.EscondeCampo("new_empreendimentoid", true);
                }
                else if (crmForm.all.customerid.DataValue[0].typename == 'contact') {
                    crmForm.EscondeCampo("new_opportunityid", true);
                    crmForm.EscondeCampo("productid", true);
                    crmForm.EscondeCampo("new_empreendimentoid", false);
                }
            }
        }
		
        crmForm.ValidarOcorrenciaCriadaPortalFaleConosco = function() {
            crmForm.DesabilitarCampos("new_meio_retorno", true);
            crmForm.DesabilitarCampos("new_assunto_portal", true);

            if (crmForm.all.new_criado_portal && crmForm.all.new_criado_portal.DataValue) {
                crmForm.EscondeSecao("new_assunto_portal", false);
                crmForm.DesabilitarCampos("caseorigincode", true);
            }
            else {
                crmForm.EscondeSecao("new_assunto_portal", true);
                if (crmForm.all.caseorigincode.DataValue != RecebidoPor.CREDITO && crmForm.all.caseorigincode.DataValue != RecebidoPor.PEC) {
                    if (crmForm.FormType == TypeCreate || crmForm.all.casetypecode.DataValue == null || crmForm.all.casetypecode.DataValue != 200007) {
                        crmForm.DesabilitarCampos("caseorigincode", false);
                    }
                }
            }
        }
		
		crmForm.ValidarOcorrenciaCriadaPortalFaleConosco();
		crmForm.ConfiguraDesbloqueioOM();
    } //Fim Load

    crmForm.VerificaAssuntoReclassificacao = function () {

        if (crmForm.all.subjectid.DataValue == null) {
            crmForm.DesabilitarCampos("subjectid;new_assuntoprincipalid;new_departamentoid", false);
            return;
        }

        var cmdDados = new RemoteCommand("MrvService", "verificaAssuntoReclassificacao", "/MRVCustomizations/");
        cmdDados.SetParameter("assuntoId", crmForm.all.subjectid.DataValue[0].id);
        var oResultado = cmdDados.Execute();

        if (crmForm.TratarRetornoRemoteCommand(oResultado, "verificaAssuntoReclassificacao")) {

            var permiteAssuntoReclassificacao = oResultado.ReturnValue.Mrv.reclassificacao;

            crmForm.DesabilitarCampos("subjectid;new_assuntoprincipalid;new_departamentoid", !permiteAssuntoReclassificacao);
        }
    }

    crmForm.VerificaAssuntoInterno = function () {

        if (crmForm.all.subjectid.DataValue == null) {
            crmForm.all.casetypecode.DataValue = "";
            crmForm.DesabilitarCampos("casetypecode", false);
            return;
        }

        var cmdDados = new RemoteCommand("MrvService", "verificaAssuntoInterno", "/MRVCustomizations/");
        cmdDados.SetParameter("assuntoId", crmForm.all.subjectid.DataValue[0].id);
        var oResultado = cmdDados.Execute();

        if (crmForm.TratarRetornoRemoteCommand(oResultado, "verificaAssuntoInterno")) {
            var permiteAssuntoInterno = oResultado.ReturnValue.Mrv.interno;

            if (permiteAssuntoInterno)
                crmForm.all.casetypecode.DataValue = 200006;

            crmForm.DesabilitarCampos("casetypecode", permiteAssuntoInterno);
        }
    }

    crmForm.EscondeBotao = function (nomeBotao, esconde) {

        var lista = document.getElementsByTagName("LI");
        for (i = 0; i < lista.length; i++) {
            if (lista[i].id.indexOf(nomeBotao) >= 0) {
                var o = lista[i].parentElement;
                lista[i].style.display = (esconde ? "none" : "block");
                lista[i].style.visibility = (esconde ? "hidden" : "visible");
                break;
            }
        }
    }

    crmForm.ValidaPermissaoBotoesDistratoEOportunidadeRenegociacao = function () {

        //DETS 487055: Melhorias no módulo de renegociação e distrato: Item 2.3.1.1
        //Se o campo Assunto (subjectid) estiver preenchido validar
        if (crmForm.all.subjectid.DataValue != null && crmForm.all.new_assuntoprincipalid.DataValue != null) {

            var cmd = new RemoteCommand("MrvService", "VerificaPermissaoDeCriacaoDistratoOuOportunidadeRenegociacao", "/MRVCustomizations/");
            cmd.SetParameter("assuntoprincipalid", crmForm.all.new_assuntoprincipalid.DataValue[0].id);
            var resultado = cmd.Execute();
            if (crmForm.TratarRetornoRemoteCommand(resultado, "VerificaPermissaoDeCriacaoDistratoOuOportunidadeRenegociacao")) {
                //Se o campo new_assunto_principal.new_permite_criacao_de_distrado  está marcado com “Sim”, exibe menu “Novo Distrato”.
                if (resultado.ReturnValue.Mrv.Distrato)
                    crmForm.EscondeBotao('NovoDistrato', false);
                else
                    crmForm.EscondeBotao('NovoDistrato', true);

                //Se o campo new_assunto_principal. new_permite_criacao_de_renegociacao  está marcado com “Sim”, exibe menu “Nova Oportunidade Renegociação”.
                if (resultado.ReturnValue.Mrv.OportunidadeRenegociacao)
                    crmForm.EscondeBotao('NovaOportunidadedeRenegociao', false);
                else
                    crmForm.EscondeBotao('NovaOportunidadedeRenegociao', true);
            }
        }
        else {
            crmForm.EscondeBotao('NovoDistrato', true);
            crmForm.EscondeBotao('NovaOportunidadedeRenegociao', true);
        }
    }
    crmForm.ExibirDadosOM = function () {
        openStdDlg("/MRVWEB/Ocorrencia/ExibirOrdemManutencao.aspx?id=" + crmForm.ObjectId, null, 900, 500);
    }

    crmForm.ConfiguraDesbloqueioOM = function () {
        if (crmForm.FormType == TypeUpdate &&
         crmForm.all.casetypecode.DataValue == 200003 &&
         crmForm.all.new_desbloqueio_ordem.DataValue != 1 &&
         crmForm.VerificarEquipe("AT_DESBLOQUEIO_OM")) {
            crmForm.EscondeBotao('DesbloquearOM', false);
        }
        else
            crmForm.EscondeBotao('DesbloquearOM', true);
    }
    
    crmForm.DesbloquearOM = function () {
        var popUpDesbloqueio = openStdDlg("/MRVWEB/Ocorrencia/MotivoDesbloqueioOM.aspx", null, 500, 340);

        if (popUpDesbloqueio != undefined && popUpDesbloqueio != null) {
        if (window.confirm("A Ordem de manutenção será desbloqueada no SAP. Confirma?")) {
            var numOcorrencia = crmForm.all.ticketnumber.DataValue;

            var cmd = new RemoteCommand("MrvService", "DesbloquearOM", "/MRVCustomizations/");
                cmd.SetParameter("numeroOcorrencia", numOcorrencia);
                cmd.SetParameter("motivoDesbloqueio", popUpDesbloqueio.DropText);
                cmd.SetParameter("motivoDesbloqueioIndex", popUpDesbloqueio.DropIndex);
                cmd.SetParameter("observacaoDesbloqueio", popUpDesbloqueio.Text);

            var resultado = cmd.Execute();
            if (resultado.ReturnValue.Mrv.Mensagem != null) {
                alert(resultado.ReturnValue.Mrv.Mensagem);
            }
                if (!resultado.ReturnValue.Mrv.Error) {
               crmForm.EscondeBotao('DesbloquearOM', true);
        }
                else {
                    alert("Não foi possível Desbloquear OM!");
                }
            }
    }
    }

    crmForm.ExcluiEntregaChaves = function () {
        crmForm.all.casetypecode.DeleteOption(200007);
    }

    crmForm.ConfiguraVistoriaEntregaChaves = function () {
        crmForm.DesabilitarFormulario(true);
    }

    crmForm.ValidarExibicaoBotaoEntregaChaves = function () {
        if (crmForm.FormType == TypeCreate || crmForm.all.casetypecode.DataValue == null || crmForm.all.casetypecode.DataValue != 200007) {
            crmForm.EscondeBotao('HistricoEC', true);
            crmForm.EscondeBotao('CheckList', true);
            return;
        }
        if (crmForm.all.statecode.DataValue != 0 && crmForm.all.statecode.DataValue != 1) {
            crmForm.EscondeBotao('HistricoEC', true);
            crmForm.EscondeBotao('CheckList', true);
            return;
        }
    }

    crmForm.ObterUrlParametroComercial = function (nomeUrl) {
        var cmd = new RemoteCommand("MrvService", "ObterUrlEntregaChaves", "/MRVCustomizations/");
        cmd.SetParameter("tipoUrl", nomeUrl);
        var resultado = cmd.Execute();
        if (crmForm.TratarRetornoRemoteCommand(resultado, "ObterUrlEntregaChaves")) {
            if (resultado.ReturnValue.Mrv.urlEntregaChaves != null) {
                var contratoId = crmForm.ObterIdContrato();
                if (contratoId != null) {
                    var parameter = "&contractid=" + crmForm.ObterIdContrato();
                    return resultado.ReturnValue.Mrv.urlEntregaChaves + parameter;
                }
            } else {
                if (resultado.ReturnValue.Mrv.Mensagem != null)
                    alert(resultado.ReturnValue.Mrv.Mensagem);
            }
        }
        return null;
    }

    crmForm.ObterIdContrato = function () {
        var cmd = new RemoteCommand("MrvService", "ObterIdContrato", "/MRVCustomizations/");
        cmd.SetParameter("codContrato", crmForm.all.new_contratosap.DataValue);
        var resultado = cmd.Execute();
        if (crmForm.TratarRetornoRemoteCommand(resultado, "ObterIdContrato")) {
            if (resultado.ReturnValue.Mrv.idContrato != null) {
                return resultado.ReturnValue.Mrv.idContrato;
            } else {
                if (resultado.ReturnValue.Mrv.Mensagem != null)
                    alert(resultado.ReturnValue.Mrv.Mensagem);
            }
        }
        return null;
    }

    crmForm.ExibirRelatorioEntragaChaves = function () {
        var url = crmForm.ObterUrlParametroComercial("URL_HISTORICO");
        if (url == null) {
            return;
        }
        var retorno = openStdDlg(url, null, 800, 600);
    }

    crmForm.ExibirCheckListEntragaChaves = function () {
        var url = crmForm.ObterUrlParametroComercial("URL_CHECK_LIST");
        if (url == null) {
            return;
        }
        var retorno = openStdDlg(url, null, 800, 600);
    }

    function loadScript() {
        var oHead = document.getElementsByTagName('head')[0];
        var oScript = document.createElement('script');
        oScript.type = "text/javascript";
        oScript.src = "/_static/Base.js";

        oScript.onreadystatechange = function () {
            if (this.readyState == 'complete' || this.readyState == 'loaded') {
                Load();
            }
        }
        oHead.appendChild(oScript);
    }

    loadScript();

}
catch (erro) {
    alert("Ocorreu um erro no formulário.\n" + erro.description);
}
