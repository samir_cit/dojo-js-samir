﻿function MostraBaseConhecimento() {
    var _kbview = document.getElementById("kbviewer");
    _kbview.style.display = (document.getElementById("showKBViewer").checked == true) ? 'inline' : 'none';
    crmForm.AumentarTamanhoArtigo();
}

function LimpaCampos() {
    document.getElementById("showKBViewer").checked = false;
    document.getElementById("KBViewer").src = "about:blank";
    MostraBaseConhecimento();

    crmForm.all.kbarticleid.DataValue = null;
    crmForm.all.new_assuntoprincipalid.DataValue = null;
    crmForm.all.new_departamentoid.DataValue = null;
    crmForm.all.title.DataValue = null;
}

if (crmForm.all.subjectid.DataValue != null) {
    crmForm.EsconderAtributosCamposAdicionais(true);
    if (crmForm.AssuntoPermitido(crmForm.all.subjectid.DataValue[0].id)) {
        LimpaCampos();
        var subjectid = crmForm.all.subjectid.DataValue[0].id;
        var productid = "";
        if(crmForm.all.productid.DataValue != null)
            productid = crmForm.all.productid.DataValue[0].id;
        var cmd = new RemoteCommand("MrvService", "ObterDadosOcorrencia", "/MRVCustomizations/");
        cmd.SetParameter("assuntoId", subjectid);
        cmd.SetParameter("produtoId", productid);
        var result = cmd.Execute();
        if (crmForm.TratarRetornoRemoteCommand(result, "ObterDadosOcorrencia")) {
            if (crmForm.FormType == 2) {
                if (!crmForm.AbrirPopUpMotivoReClassificacao()) 
                    return;
            }
            if (result.ReturnValue.Mrv.NomeArtigo && typeof (result.ReturnValue.Mrv.IdArtigo) != "undefined") {
                crmForm.AddValueLookup(crmForm.all.kbarticleid, result.ReturnValue.Mrv.IdArtigo, 127, result.ReturnValue.Mrv.NomeArtigo);

                //Marcar exibir artigo como true
                document.getElementById("showKBViewer").checked = true;
                MostraBaseConhecimento();
                document.getElementById("KBViewer").src = '/CS/articles/viewer/default.aspx?IsRestrictedMode=true&id=' + result.ReturnValue.Mrv.IdArtigo;
            }
            crmForm.AddValueLookup(crmForm.all.new_assuntoprincipalid, result.ReturnValue.Mrv.IdAssuntoPrincipal, 10092, result.ReturnValue.Mrv.NomeAssuntoPrincipal);
            crmForm.all.new_assuntoprincipalid.FireOnChange();
            crmForm.AddValueLookup(crmForm.all.new_departamentoid, result.ReturnValue.Mrv.IdDepartamento, 10091, result.ReturnValue.Mrv.NomeDepartamento);
            crmForm.all.title.DataValue = crmForm.all.subjectid.DataValue[0].name;
            crmForm.ObterCamposAdicionaisPorAssunto();
        }
        else {
            LimpaCampos();
            crmForm.all.subjectid.DataValue = null;
        }
    }
    else {
        LimpaCampos();
        crmForm.all.subjectid.DataValue = null;
        alert("Usuário sem permissão para criar ocorrências com assunto selecionado.");
    }
}
else {
    LimpaCampos();
    crmForm.EsconderAtributosCamposAdicionais(true);
}

crmForm.MostraCampoOcorrenciaAT();
crmForm.ValidaPermissaoBotoesDistratoEOportunidadeRenegociacao();
crmForm.VerificaAssuntoInterno();
