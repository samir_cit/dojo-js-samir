﻿function ordenarNumeros(a,b)
{
	return a-b;
}

if (crmForm.all.new_predecessora.DataValue != null)
{
	var arrayCodigos = crmForm.all.new_predecessora.DataValue;
	arrayCodigos = arrayCodigos.split(',');
	var stringCodigos = arrayCodigos.sort(ordenarNumeros).toString();
	crmForm.all.new_predecessora.DataValue = stringCodigos;
	crmForm.PredecessorasValidas(stringCodigos.split(','));
}

crmForm.AjustaCamposDuracaoNulos(arrayCodigos);

/*
var listapred1 = "";
var listapred2 = "01";
var listapred3 = "02";
var listapred4 = "01-02-03";
var listapred5 = "03-04";
var listapred6 = "01-03-04-05";
var listapred7 = "01-03-04-05-06-10-11-12-13";
var listapred8 = "01-02-03-04-05-06-10-11-12-13";
var listapred9 = "01-03-04-05-06-10-11-12-13";
var listapred10 = "03-04-05";
var listapred11 = "04-05-10";
var listapred12 = "01-02-03-04-05-10-11";
var listapred13 = "01-02-03-04-05-10-11-12";
var listapred14 = "02-03-04-05-07-08-09-10-11-12";
var listapred15 = "02-03-04-05-07-08-09-10-11-12";
var listapred16 = "04-05-07-08-09-10-11-12";
var listapred17 = "03-04-05-07-08-09-10-11-12";
var listapred18 = "04-05-07-08-09-10-11-12";
var listapred19 = "02-04-05-07-08-09-10-11-12";
var listapred20 = "04-05-07-08-09-10-11-12";
var listapred21 = "03-04-05-07-08-09-10-11-12";
var listapred22 = "04-05-07-08-09-10-11-12";
var listapred23 = "04-05-07-08-09-10-11-12";
var listapred24 = "04-05-07-08-09-10-11-12";
var listapred25 = "04-05-07-08-09-10-11-12";


if (crmForm.all.new_predecessora.DataValue != null)
{
//------Condição que limita a 3 predecessoras no maximo---------------------------
//var sTmp = crmForm.all.new_predecessora.DataValue.replace(/[^0-9]/g, ""); 
//if (sTmp.length != 2 && sTmp.length != 4 && sTmp.length != 6)
//{
//crmForm.all.new_predecessora.DataValue = null;
//alert("Campo fora da faixa de dígitos sequenciais! Ex: 01, 01-02, 01-02-03");
//}
//else
//{

     var codigo = crmForm.all.new_codigo_etapa.DataValue;
     var listaquepode = eval("listapred" + codigo).split("-");

     var lista = crmForm.all.new_predecessora.DataValue.split("-");
     for(i = 0; i < lista.length; i++)
     {    var ok= false;
           for(i2 = 0; i2< listaquepode.length; i2++) 
           {
                  ok = (lista[i] == listaquepode[i2]);
                  if (ok) 
                        break;
           }
           if (!ok) 
           { 
                 alert("Esta etapa não pode ter como predecessora a etapa " +  lista[i]);
                 crmForm.all.new_predecessora.DataValue = null;
                 break;
            }
     }
if (ok)
{
    var cmd = new RemoteCommand("MrvService", "RetornaMaiorDataPredecessoras", "/MRVCustomizations/");
    cmd.SetParameter("empreendimentoId", crmForm.all.new_empreendimentoid.DataValue[0].id);
    cmd.SetParameter("predecessora", crmForm.all.new_predecessora.DataValue);
    var result = cmd.Execute();
    if (result.Success)
   {
         if (!result.ReturnValue.Nulo) {
           var data = new Date(result.ReturnValue.Mrv.Ano, result.ReturnValue.Mrv.Mes - 1, result.ReturnValue.Mrv.Dia);
            if (crmForm.all.new_data_prevista_conclusao.DataValue) {
                 crmForm.all.new_data_prevista_inicio_atualizada.DataValue = data;
                 crmForm.all.new_data_prevista_conclusao_atualizada.DataValue = data;
                 crmForm.all.new_data_prevista_inicio_atualizada.FireOnChange();
            }
            else  {
                 crmForm.all.new_dataprevistainicio.DataValue = data;
                 crmForm.all.new_data_prevista_conclusao.DataValue = data;
                 crmForm.all.new_dataprevistainicio.FireOnChange();
            }
         }
    }
}

//---fecha o if que limita a predecessora a 3------------------}
}
*/