﻿try {
    if (!formularioValido) {
        alert("Ocorreu um erro no formulário. Impossível salvar.");
        event.returnValue = false;
        return false;
    }

    ////Validação das data de início e conclusão. Início deve ser sempre menor que conclusão//////
    if ((crmForm.all.new_dataprevistainicio.DataValue != null) && (crmForm.all.new_data_prevista_conclusao.DataValue != null)) {
        if (crmForm.all.new_dataprevistainicio.DataValue > crmForm.all.new_data_prevista_conclusao.DataValue) {
            alert('Aviso: A data de início deve ser menor que a data de conclusão.');
            event.returnValue = false;
            return false;
        }
    }

    if ((crmForm.all.new_data_prevista_inicio_atualizada.DataValue != null) && (crmForm.all.new_data_prevista_conclusao_atualizada.DataValue != null)) {
        if (crmForm.all.new_data_prevista_inicio_atualizada.DataValue > crmForm.all.new_data_prevista_conclusao_atualizada.DataValue) {
            alert('Aviso: A data de início deve ser menor que a data de conclusão.');
            event.returnValue = false;
            return false;
        }
    }

    if ((crmForm.all.new_data_real_inicio.DataValue != null) && (crmForm.all.new_data_real_conclusao.DataValue != null)) {
        if (crmForm.all.new_data_real_inicio.DataValue > crmForm.all.new_data_real_conclusao.DataValue) {
            alert('Aviso: A data de início deve ser menor que a data de conclusão.');
            event.returnValue = false;
            return false;
        }
    }
    if (crmForm.FormType == 2) {
        var cmd = new RemoteCommand("MrvService", "DatasDIDataProtocoloRIValidas", "/MRVCustomizations/");

        var dataPrevistaInicio = crmForm.all.new_dataprevistainicio.DataValue;
        var dataPrevistaInicioAtualizada = crmForm.all.new_data_prevista_inicio_atualizada.DataValue;
        var dataRealInicio = crmForm.all.new_data_real_inicio.DataValue;

        cmd.SetParameter('etapaID', crmForm.ObjectId);
        cmd.SetParameter('dataPrevIni', dataPrevistaInicio != null ? dataPrevistaInicio.toLocaleDateString() : '');
        cmd.SetParameter('dataPrevIniAtualizada', dataPrevistaInicioAtualizada != null ? dataPrevistaInicioAtualizada.toLocaleDateString() : '');
        cmd.SetParameter('dataRealIni', dataRealInicio != null ? dataRealInicio.toLocaleDateString() : '');

        var oResult = cmd.Execute();
        if (oResult.Success) {
            if (!(oResult.ReturnValue)) {
                alert('As datas de início da etapa não podem ser maiores que a data do protocolo de RI do empreendimento.');
                event.returnValue = false;
                return false;
            }
        }
        else
            alert('Falha no envio das informações. Contate o administrador do sistema.');
    }

    // Transformar caracteres em maiusculos------------------------------------//
    var elm = document.getElementsByTagName("input");
    for (i = 0; i < elm.length; i++) {
        if (elm[i].type == "text")
            if (elm[i].DataValue != null) {
            try {
                elm[i].DataValue = elm[i].DataValue.toUpperCase();
            }
            catch (e) { }
        }
    }
    crmForm.all.statuscode.DataValue = 1;
    //------ Gera o código da nova etapa que não seja uma das 25 default do empreendimento ------//
    if (crmForm.FormType == 1) {
        if (crmForm.all.new_codigo_etapa.DataValue == null) {
            if (crmForm.all.new_empreendimentoid.DataValue != null) {
                var cmdEtapa = new RemoteCommand('MrvService', 'ObterMaiorCodigoEtapa', '/MRVCustomizations/');
                cmdEtapa.SetParameter('idEmpreendimento', crmForm.all.new_empreendimentoid.DataValue[0].id);
                var result = cmdEtapa.Execute();
                if (result.Success) {
                    if (result.ReturnValue.Mrv.Erro) {
                        alert(result.ReturnValue.Mrv.Result);
                        event.returnValue = false;
                        return false;
                    }
                    else {
                        //Incrementa 1 (um) ao maior código de etapa recuperado pelo sistema para assim gerar o próximo código
                        var novoCodigoEtapa = (result.ReturnValue.Mrv.Result) + 1;
                        crmForm.all.new_codigo_etapa.DataValue = novoCodigoEtapa;
                    }
                }
                else
                    alert('Erro na comunicação com o MrvService. Contate o administrador do sistema.');
            }
            else {
                alert('O campo empreendimento esta vazio. Favor selecionar um empreendimento.');
                event.returnValue = false;
                return false;
            }
            crmForm.CompoeNomeEtapa();
        }
    }
    //---------------- Calcula a data de início da etapa em função, caso possua, de suas predecessoras.--------//
    function AjustaData(data)
    {
     if(data != "0")
     {
         var dataArray = data.toString().split("-");
         var novaData = new Date(dataArray[0], dataArray[1] - 1, dataArray[2]);
         return novaData;
        }
    }
    
    function AjustaDuracaoAtualizada()
    {
        //Só preenche o campo da duração atualizada se a flag "alterado manualmente" estiver nula ou com valor false.
        if(crmForm.all.new_duracao_atualizada_alterada_manualmente.DataValue == null || crmForm.all.new_duracao_atualizada_alterada_manualmente.DataValue == 0)
        {
            crmForm.all.new_duracao_atualizada_alterada_manualmente.DataValue = false;
            crmForm.all.new_duracao_atualizada.DataValue = crmForm.all.new_duracao_dias.DataValue;
        }
    }

    if (crmForm.FormType == 2) 
    {
        var dataAux = null;
      
        var cmdDataEtapas = new RemoteCommand('MrvService', 'AjustaDataEtapas', '/MRVCustomizations/');
        cmdDataEtapas.SetParameter('empreendimentoID', (crmForm.all.new_empreendimentoid.DataValue != null) ? crmForm.all.new_empreendimentoid.DataValue[0].id : "x");
        cmdDataEtapas.SetParameter('etapaID', crmForm.ObjectId);
        cmdDataEtapas.SetParameter('codigoEtapa', (crmForm.all.new_codigo_etapa.DataValue != null) ? crmForm.all.new_codigo_etapa.DataValue : "x");
        cmdDataEtapas.SetParameter('predecessoras', (crmForm.all.new_predecessora.DataValue != null) ? crmForm.all.new_predecessora.DataValue : "x");
        cmdDataEtapas.SetParameter('duracao_dias', (crmForm.all.new_duracao_dias.DataValue != null) ? crmForm.all.new_duracao_dias.DataValue : "x");
        cmdDataEtapas.SetParameter('duracao_atualizada', (crmForm.all.new_duracao_atualizada.DataValue != null) ? crmForm.all.new_duracao_atualizada.DataValue : "x");

        if (crmForm.all.new_dataprevistainicio.DataValue != null) 
        {
            dataAux = crmForm.all.new_dataprevistainicio.DataValue;
            cmdDataEtapas.SetParameter('dataprevistainicio', dataAux.getFullYear() + "-" + (dataAux.getMonth()+1)  + "-" + dataAux.getDate());
        }
        else
            cmdDataEtapas.SetParameter('dataprevistainicio', "x");

        if (crmForm.all.new_data_prevista_inicio_atualizada.DataValue != null)
        {
            dataAux = crmForm.all.new_data_prevista_inicio_atualizada.DataValue;
            cmdDataEtapas.SetParameter('data_prevista_inicio_atualizada', dataAux.getFullYear() + "-" + (dataAux.getMonth()+1) + "-" + dataAux.getDate());
        }
        else
            cmdDataEtapas.SetParameter('data_prevista_inicio_atualizada', "x");

        crmForm.all.new_data_prevista_conclusao_atualizada.ForceSubmit = true;
        debugger;
        var resultEtapas = cmdDataEtapas.Execute();
        resultEtapas = cmdDataEtapas.Execute();

        if (resultEtapas.Success)
        {
            if (resultEtapas.ReturnValue.Mrv.Erro)
            {
                alert("Ocorreu um erro no MrvService: " + resultEtapas.ReturnValue.Mrv.Result + 
                " Contate o administrador do sistema se necessário.");
                event.returnValue = false;
                return false;
            }
            else
           {
                if (resultEtapas.ReturnValue.Mrv.PreencherDatasAtualizadas)
                {
                    if(crmForm.all.new_predecessora.DataValue != null)
                    {
                        crmForm.all.new_data_prevista_inicio_atualizada.DataValue = AjustaData(resultEtapas.ReturnValue.Mrv.DataInicio.toString());
                        AjustaDuracaoAtualizada();
                        crmForm.all.new_data_prevista_inicio_atualizada.FireOnChange();
                    }
                }
                else
                {
                   crmForm.all.new_dataprevistainicio.DataValue = AjustaData(resultEtapas.ReturnValue.Mrv.DataInicio.toString());
                   crmForm.all.new_dataprevistainicio.FireOnChange();
                }
            }
        }
    }
    crmForm.all.new_name.Disabled = false;
} catch (error) {
    alert("Ocorreu um erro no formulário.\n" + error.description);
    event.returnValue = false;
    return false;
}