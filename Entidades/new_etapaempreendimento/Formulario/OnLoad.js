﻿formularioValido = true;
try {
	
	function Load() {
		CodigoEtapa =
		{
		    Etapa1: 1,
			Etapa2: 2,
			Etapa4: 4,
			Etapa7: 7,
			Etapa8: 8,
			Etapa25: 25
		}
		
		//---Campo que indica se a duração atualizada foi alterada manualmente. Sempre aculto.
		crmForm.all.new_duracao_atualizada_alterada_manualmente_c.style.visibility = 'hidden';
		crmForm.all.new_duracao_atualizada_alterada_manualmente_d.style.visibility = 'hidden';
		
		crmForm.all.new_name.Disabled = true;
		crmForm.all.new_empreendimentoid.Disabled = true;
		crmForm.all.statuscode_c.style.display = 'none';
		crmForm.all.statuscode_d.style.display = 'none';

		//--------- Ocultar campos das versões e arquitetas ----------------------------------------
		if ((crmForm.all.new_codigo_etapa.DataValue == CodigoEtapa.Etapa2) ||
		   (crmForm.all.new_codigo_etapa.DataValue == CodigoEtapa.Etapa4))
		{
			crmForm.all.new_versaoatual_c.style.visibility = 'visible';
			crmForm.all.new_versaoatual_d.style.visibility = 'visible';
			crmForm.all.new_arquiteta_c.style.visibility = 'visible';
			crmForm.all.new_arquiteta_d.style.visibility = 'visible';
		}
		else
		{
			crmForm.all.new_versaoatual_c.style.visibility = 'hidden';
			crmForm.all.new_versaoatual_d.style.visibility = 'hidden';
			crmForm.all.new_arquiteta_c.style.visibility = 'hidden';
			crmForm.all.new_arquiteta_d.style.visibility = 'hidden';
		}
		//----Função para ocultar a entidade de itens da etapa------------------------------------------------//
		//----Ele dá problema ao exibir pelo modo de visualização, mas por dentro da tabela fica ok-----//
		if (crmForm.all.new_codigo_etapa.DataValue != CodigoEtapa.Etapa7)
		{
			if (document.getElementById("nav_new_new_etapaempreendimento_new_item_etapa") != null)
			{
				document.getElementById("nav_new_new_etapaempreendimento_new_item_etapa").style.display = 'none';
			}
		}
		//---Ocultar a entidade Controle de Versao para etapas diferente de 2 e 4-----------------------//
		if (crmForm.all.new_codigo_etapa.DataValue != CodigoEtapa.Etapa2 && crmForm.all.new_codigo_etapa.DataValue != CodigoEtapa.Etapa4)
		{
			if(document.getElementById("nav_new_new_etapaempreendimento_new_controle_vers") != null)
			{
				document.getElementById("nav_new_new_etapaempreendimento_new_controle_vers").style.display = 'none';
			}
		}
		//--------------------------------------Compõe o nome da Etapa-----------------------------//
		crmForm.CompoeNomeEtapa = function()
		{
			if(crmForm.all.new_tipo_etapa_legalizacao_projetoid.DataValue != null)
				crmForm.all.new_name.DataValue = "Etapa " + crmForm.all.new_codigo_etapa.DataValue + " - " + crmForm.all.new_tipo_etapa_legalizacao_projetoid.DataValue[0].name;
		}
		function EscondeLookUpTipoEtapa()
		{
			//Oculta o Tipo de Etapa para as 25 default
			crmForm.all.new_tipo_etapa_legalizacao_projetoid_c.style.display = 'none';
			crmForm.all.new_tipo_etapa_legalizacao_projetoid_d.style.display = 'none';
			//Desabilita o Tipo de Etapa para as 25 default
			crmForm.all.new_tipo_etapa_legalizacao_projetoid.Disabled = true;
			 
		}
		//---Desabilita tipo da etapa e predecessora em função das 25 etapas default ------------------------//
		crmForm.all.new_predecessora.Disabled = true;
		if(crmForm.all.new_codigo_etapa.DataValue != null)
		{
			crmForm.all.new_predecessora.Disabled = (!(crmForm.all.new_codigo_etapa.DataValue.length != 0));
			if(crmForm.all.new_codigo_etapa.DataValue.length != 0) 
			{
			    var codigoEtapa = crmForm.all.new_codigo_etapa.DataValue;
				if((codigoEtapa >= CodigoEtapa.Etapa1) && (codigoEtapa <= CodigoEtapa.Etapa25))
					EscondeLookUpTipoEtapa();
				else
					crmForm.CompoeNomeEtapa();
			}
		}

		//---Caso estejam nulos, prrenche os campos de duração com 0 (zero) ---------------------------------//
		crmForm.AjustaCamposDuracaoNulos = function ()
		{
		   if(crmForm.all.new_duracao_atualizada.DataValue == null)
			  crmForm.all.new_duracao_atualizada.DataValue = 0;
				
			if(crmForm.all.new_duracao_dias.DataValue == null)
			  crmForm.all.new_duracao_dias.DataValue = 0;
		 }
		 crmForm.AjustaCamposDuracaoNulos();

		 //-------Faz o cálculo da duração real (dias)-----------------------//
		 crmForm.AjustaDuracaoReal = function() {     
			 if (crmForm.all.new_data_real_inicio.DataValue != null && crmForm.all.new_data_real_conclusao.DataValue != null) {
				 var dtIni = new Date(crmForm.all.new_data_real_inicio.DataValue);
				 var dtFim = new Date(crmForm.all.new_data_real_conclusao.DataValue);
				 if (dtIni <= dtFim) {
					 var dtAux = new Date(dtFim - dtIni);
					 crmForm.all.new_duracao_real.DataValue = (dtAux.getTime() / 86400000);
				 }
				 else {
					 alert('A data real de conclusão não pode ser menor que a data real de início.');
					 crmForm.all.new_duracao_real.DataValue = null;
					 crmForm.all.new_data_real_conclusao.DataValue = null;
				 }
			 }
			 else
				 crmForm.all.new_duracao_real.DataValue = null;
		 }
		 
		 //-------Faz as validações sobre os códigos das predecessoras-------//
		 crmForm.PredecessorasValidas = function(arrayPredecessoras) {
			 if (crmForm.all.new_codigo_etapa.DataValue != null) {
				 var aux = 0;
				 var codigoEtapaAtual = crmForm.all.new_codigo_etapa.DataValue;
				 for (var i = 0; i <= arrayPredecessoras.length; i++) {
					 //Validação para impedir que usuário coloque a etapa como predecessora dela mesma
					 if (arrayPredecessoras[i] == codigoEtapaAtual) {
						 alert('Atenção. A etapa ' + codigoEtapaAtual + ' não pode ser predecessora dela mesma.');
						 crmForm.all.new_predecessora.DataValue = '';
						 event.returnValue = false;
						 return false;
					 }
					 //Validação pra impedir que o usuário repita códigos de predecessoras
					 if (arrayPredecessoras[i] == aux) {
						 alert('Detectada repetição de códigos de etapas no campo de predecessoras. Os códigos devem ser distintos.');
						 crmForm.all.new_predecessora.DataValue = '';
						 event.returnValue = false;
						 return false;
					 }
					 else
						 aux = arrayPredecessoras[i];
				 }
			 }
		 }

		 crmForm.DesabilitarCamposEtapa = function() {
		     if (crmForm.all.new_codigo_etapa.DataValue && (crmForm.all.new_codigo_etapa.DataValue == CodigoEtapa.Etapa8 ||
		     crmForm.all.new_codigo_etapa.DataValue == CodigoEtapa.Etapa7)) {
		         crmForm.DesabilitarCampos("new_dataprevistainicio", true);
		         crmForm.DesabilitarCampos("new_duracao_dias", true);
		         crmForm.all.new_duracao_dias.DataValue = null;
		         crmForm.all.new_dataprevistainicio.DataValue = null;
		     }
		 }

		 crmForm.DesabilitarCamposEtapa();
	}
	
	function loadScript() {
		var oHead = document.getElementsByTagName('head')[0];
		var oScript = document.createElement('script');
		oScript.type = "text/javascript";
		oScript.src = "/_static/Base.js";

		oScript.onreadystatechange = function() {
			if (this.readyState == 'complete' || this.readyState == 'loaded') {
				Load();
			}
		}
		oHead.appendChild(oScript);
	}

    loadScript();
}
catch (error)
{
    formularioValido = false;
    alert("Ocorreu um erro no formulário.\n" + error.description);
}