﻿// Transformar caracteres em maiusculos------------------------------------//
var elm = document.getElementsByTagName("input");
for (i = 0; i < elm.length; i++) {
    if (elm[i].type == "text")
        if (elm[i].DataValue != null) {
        try {
            elm[i].DataValue = elm[i].DataValue.toUpperCase();
        }
        catch (e) { }
    }
}

//desabilitar campo da área líquida para gravação no banco---------------------------//
crmForm.all.new_areaterrenoliquida.Disabled = false;

// colocar o campo  area terreno (liquida) para somente leitura------------------------//
crmForm.all.new_areaterrenoliquida.ReadOnly = false;