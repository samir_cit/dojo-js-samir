﻿
//valida se status da viabilidade esta ativo para aprovar
if (crmForm.all.statuscode.DataValue == 3) {
    //Verifica se a viabilidade ja foi gerada para confirmar a desaprovação    
    if (crmForm.all.new_viabilidadegerada.DataValue == 1 && crmForm.all.new_statusaprovacao.DataValue == 2) { // Status aprovação -> 1 = Sim e 2 = Não
        alert("Para prosseguir com a desaprovação pressione 'Salvar'. Esta operação pode demorar alguns minutos.");
    }
}
else {
    alert("Para aprovar a viabilidade, é necessário antes ativa-la");
    crmForm.all.new_statusaprovacao.DataValue = crmForm.all.new_statusaprovacao.DefaultValue;
}