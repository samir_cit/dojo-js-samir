﻿formularioValido = true;

try {

    //Adiciona Script para ser usado no click do botão
    var elm = document.createElement("script");
    elm.src = "/_static/_grid/cmds/util.js";
    document.appendChild(elm);

    // colocar o campo  area terreno (liquida) para somente leitura------------------------//
    crmForm.all.new_areaterrenoliquida.ReadOnly = true;

    crmForm.all.new_impostos.DataValue = 0.0673;
    crmForm.all.new_despesas_comerciais.DataValue = 0.07;

    crmForm.BotaoRevisar_Click = function() {
        DisplayActionMsg('Aguarde...', 400, 150);
        setTimeout('crmForm.Revisar()', 1000);
    }

    crmForm.Revisar = function() {
        var cmd;
        var resultado;

        if (crmForm.IsDirty) {
            DisplayActionMsg('');
            alert('O Estudo de Viabilidade não foi salvo. Salve antes de continuar.');
        }

        cmd = new RemoteCommand('MrvService', 'RevisarViabilidade', '/MRVCustomizations/');

        cmd.SetParameter('viabilidadeId', crmForm.ObjectId);
        cmd.SetParameter('statusAtual', crmForm.all.statuscode.DataValue);

        resultado = cmd.Execute();

        if (resultado.ReturnValue.Mrv.Erro) {
            alert(resultado.ReturnValue.Mrv.Mensagem);
        }
        else {
            alert('Revisão criada com sucesso.');
            window.close();
        }
    }

    crmForm.VerificaFuncao = function(nomeFuncao) {
        var cmd = new RemoteCommand("MrvService", "UserContainsTeams", "/MrvCustomizations/");
        cmd.SetParameter("teams", nomeFuncao);
        var resultado = cmd.Execute();
        if (resultado.Success) {
            return resultado.ReturnValue.Mrv.Found;
        } else {
            alert("Erro na consulta do método UserContainsTeams");
            formularioValido = false;
            return false;
        }
    }

    function DisabledField(Fields) {
        var tmp = Fields.split(";");
        for (index = 0; index < tmp.length; index++) {
            var object = eval("crmForm.all." + tmp[index])
            object.Disabled = true;
        }
    }

    //Caso o usuário pertença a equipe Viabilidade engenharia, ele não poderá alterar nenhum campo da viabilidade
    if (crmForm.VerificaFuncao("Viabilidade Engenharia")) {
        if (crmForm.FormType == 2) {
            var todosAtributos = document.getElementsByTagName("label");
            for (var i = 0; i < todosAtributos.length; i++) {
                if (todosAtributos[i].htmlFor != "")
                    DisabledField(todosAtributos[i].htmlFor.replace("_ledit", ""));
            }
        }
    }
}
catch (erro) {
    alert("Ocorreu um erro no formulário.\n" + erro.description);
}