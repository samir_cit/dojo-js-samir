﻿formularioValido = true;
try {

    TipoTelefonema = {
        LIGACAO: "1",
        SMS: "2",
        INTERNO: "3"
    }

    Direcao = {
        ENTRADA: "0",
        SAIDA: "1"
    }

    TypeCode = {
        OCORRENCIA: 112
    }

    function desabilitarBotoes() {
        //Create Form = 1
        if (crmForm.FormType == 1) {
            document.getElementById("_MBcrmFormSave").style.display = "none";
            document.getElementById("_MBcrmFormSaveAndClose").style.display = "none";
            document.getElementById("_MBcrmFormSubmitCrmForm59truetruefalse").style.display = "none";
        }
    }
    desabilitarBotoes();

    crmForm.ConfiguraExibicaoStatusDirecao = function() {
        if (crmForm.all.directioncode.DataValue == Direcao.SAIDA) {
            crmForm.SetFieldReqLevel("new_status_saida_direcao", true);
            crmForm.EscondeCampo("new_status_saida_direcao", false);
        }
        else {
            crmForm.SetFieldReqLevel("new_status_saida_direcao", false);
            crmForm.EscondeCampo("new_status_saida_direcao", true);
        }
    }

    /* Usuários da equipe 'Bloqueio Criação de Atividades' só podem criar atividades a partir de uma ocorrência. */
    function VerificaSeUsuarioTemPermissaoParaCriarAtividades() {
        if (crmForm.FormType == TypeCreate &&
            crmForm.UsuarioPertenceEquipe("Bloqueio Criação de Atividades") &&
            (window.opener == null ||
                window.opener.crmForm == undefined ||
                window.opener.crmForm == null ||
                (window.opener.crmForm && window.opener.crmForm.ObjectTypeCode != TypeCode.OCORRENCIA))) {
            alert('Você não possui permissão para criar atividades que não são vinculadas à uma ocorrência.');
            window.close();
        }
    }

    function Load() {
        VerificaSeUsuarioTemPermissaoParaCriarAtividades();
        
        if (crmForm.FormType != TypeCreate)
            crmForm.all.description.Disabled = true;

        crmForm.SomenteProprietario("convertActivity;_MBSaveAsCompleted;_MSsubconvertActivity;_MIchangeStatedeactivate42105;_MIonActionMenuClickdelete4210");
        crmForm.EscondeCampo("new_departamentos", true);
        crmForm.ConfiguraExibicaoStatusDirecao();

        if (crmForm.FormType == TypeCreate)
            crmForm.all.directioncode.DataValue = null;

    }

    function loadScript() {
        var oHead = document.getElementsByTagName('head')[0];
        var oScript = document.createElement('script');
        oScript.type = "text/javascript";
        oScript.src = "/ISV/Tridea/JavaScript/Base.js";

        oScript.onreadystatechange = function() {
            if (this.readyState == 'complete' || this.readyState == 'loaded') {
                Load();
            }
        }
        oHead.appendChild(oScript);
    }

    loadScript();

} catch (error) {
    formularioValido = false;
    alert("Ocorreu um erro no formulário.\n" + error.description);
}
