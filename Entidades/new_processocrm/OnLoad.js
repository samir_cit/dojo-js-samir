﻿//Adiciona Script para ser usado no click do botão
var elm = document.createElement("script");
elm.src = "/_static/_grid/cmds/util.js";
document.appendChild(elm);

var IsEquipeAdmin = null;
crmForm.EquipeAdministrador = function() {
if (IsEquipeAdmin == null) {
    IsEquipeAdmin = crmForm.VerificaFuncao("administradores");
    }
    return IsEquipeAdmin;
}

crmForm.VerificaFuncao = function(nomeFuncao) {
    var cmd = new RemoteCommand("MrvService", "UserContainsTeams", "/MrvCustomizations/");
    cmd.SetParameter("teams", nomeFuncao);
    var resultado = cmd.Execute();
    if (resultado.Success) {
        return resultado.ReturnValue.Mrv.Found;
    } else {
        alert("Erro na consulta do método UserContainsTeams");
        formularioValido = false;
        return false;
    }
}

crmForm.RemoveBotao = function(nome) {
    var lista = document.getElementsByTagName("LI");
    for (i = 0; i < lista.length; i++) {
        if (lista[i].id.indexOf(nome) >= 0) {
            var o = lista[i].parentElement;
            o.removeChild(lista[i]);
            break;
        }
    }
}

//se distrato foi resolvido remover botão Efetuar distrato
if (crmForm.all.statuscode.DataValue != 1 || !crmForm.EquipeAdministrador())
    crmForm.RemoveBotao("EfetivarProcesso");