﻿formularioValido = true;
try {

    crmForm.VerificaInsercaoDeNovaCurvaDeVendas = function() {
        var rCmd = new RemoteCommand("MrvService", "VerificaInsercaoDeNovaCurvaDeVendas", "/MRVCustomizations/");
        rCmd.SetParameter("numeroDaCurva", crmForm.all.new_numero_da_curva.DataValue);
        rCmd.SetParameter("periodoAcelerado", crmForm.all.new_periodo_acelerado_meses.DataValue);
        rCmd.SetParameter("periodoPosTermino", crmForm.all.new_periodo_pos_termino_meses.DataValue);
        rCmd.SetParameter("limiteVendas", crmForm.all.new_limite_de_vendas_durantea_obra.DataValue);
        rCmd.SetParameter("percentualVenda", crmForm.all.new_percentual_da_venda_acelerada.DataValue);
        rCmd.SetParameter("percentualObra", crmForm.all.new_percentual_obra.DataValue);
        rCmd.SetParameter("percentualPosChave", crmForm.all.new_percentual_pos_chave.DataValue);
        rCmd.SetParameter("tipo", crmForm.all.new_tipo_curva.DataValue);

        var retorno = rCmd.Execute();
        var existe = false;
        if (crmForm.TratarRetornoRemoteCommand(retorno, "VerificaInsercaoDeNovaCurvaDeVendas"))
            existe = retorno.ReturnValue.Mrv.Existe;

        return existe;
    }

    /// Desabilita campo no formulário
    crmForm.DesabilitarCampos = function(campos, tipo) {
        var tmp = campos.toString().split(";");

        for (var i = 0; i < tmp.length; i++) {
            var atributo = eval("crmForm.all." + tmp[i]);
            if (atributo != null)
                atributo.Disabled = tipo;
        }
    }

    function Load() {
        //desabilita nome da entidade
        crmForm.all.new_name.Disabled = true;
        if (crmForm.FormType == TypeCreate)
            crmForm.all.new_name.DataValue = "CURVA DE VENDAS";
        else if (crmForm.FormType == TypeUpdate) {
            var CamposDesabilitadosNaEdicao = "new_numero_da_curva;new_periodo_acelerado_meses;new_periodo_pos_termino_meses;new_limite_de_vendas_durantea_obra;new_percentual_da_venda_acelerada;new_percentual_obra;new_percentual_pos_chave";
            crmForm.DesabilitarCampos(CamposDesabilitadosNaEdicao, true);
        }
    }

    function loadScript() {
        var oScript = document.createElement('script');
        oScript.type = "text/javascript";
        oScript.src = "/_static/Base.js";

        var oHead = document.getElementsByTagName('head')[0];
        oHead.appendChild(oScript);

        oScript.onreadystatechange = function() {
            if (this.readyState == 'complete' || this.readyState == 'loaded')
                Load();
        }
    }

    loadScript();

}
catch (error) {
    formularioValido = false;
    alert("Ocorreu um erro no formulário.\n" + error.description);
}