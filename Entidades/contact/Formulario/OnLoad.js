﻿formularioValido = true;
try {
	function loadScript() {
		TypeCreate = 1;
		var oScript = document.createElement('script');
		oScript.type = "text/javascript";
		oScript.src = "/_static/Base.js";

		var oHead = document.getElementsByTagName('head')[0];
		oHead.appendChild(oScript);

		oScript.onreadystatechange = function () {
			if (this.readyState == 'complete' || this.readyState == 'loaded')
				Load();
		}
	}
	
	TipoContato =
        {
            Interno: "1",
			Externo: "2",
			Departamento: "3",
			Engenheiro: "4",
			Coordenador: "5",
			Gestor: "6",
			Condominio: "7",
			Diretor:"8"
        }
        
	loadScript();
	

	crmForm.ValidarCnpj = function(cnpj) {
	    if (cnpj != null) {
	        var valido = true;
	        var strNum = cnpj.replace(/[^0-9]/g, "");
	        var i = 0; var l = 0; var strMul = "6543298765432";
	        var character = ""; var iSoma = 0; var strNum_base = ""; var iLenNum_base = 0;
	        var iLenMul = 0; var iSoma = 0; var strNum_base = 0; var iLenNum_base = 0;

	        if (strNum.length != 14) {
	            valido = false;
	        }
	        else {
	            strNum_base = strNum.substring(0, 12);
	            iLenNum_base = strNum_base.length - 1;
	            iLenMul = strMul.length - 1;
	            for (i = 0; i < 12; i++)
	                iSoma = iSoma + parseInt(strNum_base.substring((iLenNum_base - i), (iLenNum_base - i) + 1), 10) * parseInt(strMul.substring((iLenMul - i), (iLenMul - i) + 1), 10);
	            iSoma = 11 - (iSoma - Math.floor(iSoma / 11) * 11);
	            if (iSoma == 11 || iSoma == 10)
	                iSoma = 0;
	            strNum_base = strNum_base + iSoma;
	            iSoma = 0;
	            iLenNum_base = strNum_base.length - 1
	            for (i = 0; i < 13; i++)
	                iSoma = iSoma + parseInt(strNum_base.substring((iLenNum_base - i), (iLenNum_base - i) + 1), 10) * parseInt(strMul.substring((iLenMul - i), (iLenMul - i) + 1), 10);
	            iSoma = 11 - (iSoma - Math.floor(iSoma / 11) * 11);
	            if (iSoma == 11 || iSoma == 10)
	                iSoma = 0;
	            strNum_base = strNum_base + iSoma;
	            if (strNum != strNum_base)
	                valido = false;

	            return valido;
	        }
	    }
	}

	crmForm.AplicarRegrasResponsavelObra = function() {
	    crmForm.AplicarRegrasResponsavelObraCampoParceria();
	    crmForm.AplicarRegrasResponsavelObraCampoEmail();
	    crmForm.AplicarRegrasResponsavelObraCampoLoginAd();
	    crmForm.AplicarRegrasResponsavelObraCampoDiretor();
	}

	crmForm.AplicarRegrasResponsavelObraCampoParceria = function() {
	    if (!(crmForm.all.new_tipodecontato.DataValue == TipoContato.Coordenador)) {
	        crmForm.LimpaCampos("new_parceria");
	        crmForm.DesabilitarCampos("new_parceria", true);
	    } else {
	    crmForm.DesabilitarCampos("new_parceria", false);
	    }
	}

	crmForm.AplicarRegrasResponsavelObraCampoEmail = function() {
	    if (crmForm.all.new_tipodecontato.DataValue == TipoContato.Gestor || crmForm.all.new_tipodecontato.DataValue == TipoContato.Diretor
            || crmForm.all.new_tipodecontato.DataValue == TipoContato.Engenheiro || crmForm.all.new_tipodecontato.DataValue == TipoContato.Coordenador) {
	        crmForm.SetFieldReqLevel("emailaddress1", false);
	    } 
	}

	crmForm.AplicarRegrasResponsavelObraCampoLoginAd = function () {
	    if (crmForm.all.new_tipodecontato.DataValue == TipoContato.Gestor || crmForm.all.new_tipodecontato.DataValue == TipoContato.Diretor
            || crmForm.all.new_tipodecontato.DataValue == TipoContato.Engenheiro || crmForm.all.new_tipodecontato.DataValue == TipoContato.Coordenador) {
	        crmForm.SetFieldReqLevel("new_login_ad", false);
	    }
	}

	crmForm.AplicarRegrasResponsavelObraCampoDiretor = function() {
	    if (crmForm.all.new_tipodecontato.DataValue == TipoContato.Gestor) {
	        crmForm.EscondeCampo("new_diretorid", false);
	        crmForm.SetFieldReqLevel("new_diretorid", true);
	    }
	    else {
	        crmForm.LimpaCampos("new_diretorid");
	        crmForm.EscondeCampo("new_diretorid", true);
	        crmForm.SetFieldReqLevel("new_diretorid", false);
	    }
	}

	function Load() {
		var filterDefault = "false";

        var urlFilter = "/MRVWeb/FilterLookup/FilterLookup.aspx"
        var oParam = "objectTypeCode=1014&filterDefault=" + filterDefault + "&attributesearch=fullname";
        crmForm.FilterLookup(crmForm.all.new_diretorid, 2, urlFilter, oParam);


	    crmForm.AplicarRegrasResponsavelObra();
	}

} catch (error) {
	formularioValido = false;
	alert("Ocorreu um erro no formulário.\n" + error.description);
}