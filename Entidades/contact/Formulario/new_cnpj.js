﻿var cnpj = crmForm.all.new_cnpj.DataValue;
if (cnpj != null && cnpj != undefined && cnpj != "") {
    var strNum = cnpj.replace(/[^0-9]/g, "");
    var valido = crmForm.ValidarCnpj(cnpj);
    if (!valido) {
        alert('Digite um CNPJ válido.');
        crmForm.all.new_cnpj.DataValue = null;
    }
    else {
        crmForm.all.new_cnpj.DataValue = strNum.substr(0, 2) + "." + strNum.substr(2, 3) + "." + strNum.substr(5, 3) + "/" + strNum.substr(8, 4) + "-" + strNum.substr(12, 2);
    }
}