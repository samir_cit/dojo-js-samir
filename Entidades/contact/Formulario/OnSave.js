﻿try {
	if (!formularioValido) {
		alert("Ocorreu um erro no formulário. Impossível salvar.");
		event.returnValue = false;
		return false;
	}

	// Transformar caracteres em maiusculos------------------------------------//
	var elm = document.getElementsByTagName("input");
	for (i = 0; i < elm.length; i++) {
		if (elm[i].type == "text")
			if (elm[i].DataValue != null) {
				try {
					elm[i].DataValue = elm[i].DataValue.toUpperCase();
				} catch (e) {}
			}
	}
	
	if(crmForm.all.new_cnpj.DataValue != null && crmForm.all.new_cnpj.DataValue != "" && crmForm.all.new_cnpj.DataValue != undefined)
	{
       var oCmd = new RemoteCommand("MrvService", "ValidCnpjOrCpf", "/MRVCustomizations/");
       
       oCmd.SetParameter("pNumber", crmForm.all.new_cnpj.DataValue);
       
       oCmd.SetParameter("pType","0");

       var oResult = oCmd.Execute();
       
       if(oResult.Success)
       {
          if(!oResult.ReturnValue.Mrv.Valid)
          {
              alert(oResult.ReturnValue.Mrv.Mensagem);
          }
          return oResult.ReturnValue.Mrv.Valid;
       }
       else
       {
          alert("Erro. Contate o administrador.");
          return false;
       }
   }

} catch (error) {
	alert("Ocorreu um erro no formulário.\n" + error.description);
	event.returnValue = false;
	return false;
}