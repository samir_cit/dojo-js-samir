﻿var erro = '';

function AddMascara(cpf)
{
    if (typeof(cpf) != "undefined" && cpf != null) 
    { 
        var sTmp = cpf.replace(/[^0-9]/g, ""); 
        
        if (sTmp.length == 11)
        {
            crmForm.all.new_cpf.DataValue =  sTmp.substr(0,3) + "." + sTmp.substr(3,3) + "." + sTmp.substr(6,3) + "-" + sTmp.substr(9,2); 
            cpf = '';
            return true;
        }
        else
        {
            crmForm.all.new_cpf.DataValue = null;
            alert("Campo fora da faixa de dígitos sequenciais!");
            crmForm.all.new_cpf.SetFocus();
            return false;
        }
    }
}

function RemoteCPF(cpf, changeCPF)
{
   var oCmd = new RemoteCommand("MrvService", "ValidCnpjOrCpf", "/MRVCustomizations/");
   
   oCmd.SetParameter("pNumber", cpf.toString());
   
   if(changeCPF)
      oCmd.SetParameter("pType","1");
   else
      oCmd.SetParameter("pType","0");

   var oResult = oCmd.Execute();
   
   if(oResult.Success)
   {
      if(!oResult.ReturnValue.Mrv.Valid)
      {
          alert(oResult.ReturnValue.Mrv.Mensagem);
      }
      return oResult.ReturnValue.Mrv.Valid;
   }
   else
   {
      alert("Erro. Contate o administrador.");
      return false;
   }
}

if(crmForm.all.new_cpf.DataValue != null) {

    var Number = crmForm.all.new_cpf.DataValue;

    if (AddMascara(crmForm.all.new_cpf.DataValue))
    {
        if (!RemoteCPF(crmForm.all.new_cpf.DataValue, crmForm.all.new_cpf.IsDirty)) {
            crmForm.all.new_cpf.DataValue = null;
            crmForm.all.new_cpf.SetFocus();
        }
    }
}