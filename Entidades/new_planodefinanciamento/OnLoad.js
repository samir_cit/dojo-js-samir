﻿formularioValido = true;
try {
    function loadScript() {
        var oScript = document.createElement('script');
        oScript.type = "text/javascript";
        oScript.src = "/_static/Base.js";

        var oHead = document.getElementsByTagName('head')[0];
        oHead.appendChild(oScript);

        oScript.onreadystatechange = function() {
            if (this.readyState == 'complete' || this.readyState == 'loaded')
                Load();
        }
    }

    loadScript();

    function Load() {
        if (crmForm.UsuarioPertenceEquipe(Equipe.ADMINISTRADORES)) {
            crmForm.DesabilitarCampos("new_nome_plano_sistema_credito; new_grupo_financiamento; new_obrigatoriedade_taxa_despachante", false);
        }
        else {
            crmForm.DesabilitarCampos("new_nome_plano_sistema_credito; new_grupo_financiamento; new_obrigatoriedade_taxa_despachante", true);
        }
        //desabilitando o campo nome do formulário----------------------------------------
        crmForm.all.new_name_c.style.fontSize = "13px";
    }
}
catch (error) {
    formularioValido = false;
    alert("Ocorreu um erro no formulário.\n" + error.description);
}