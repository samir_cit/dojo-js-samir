﻿if (crmForm.all.new_dataprevistadeterminodeobra.DataValue == null) 
{
    alert("Você deve fornecer um valor para Data Prevista de Término de Obra.");
    crmForm.all.new_dataprevistadeterminodeobra.DataValue = DataTerminoObra;
    crmForm.all.new_dataprevistadeterminodeobra.SetFocus();
}
else 
{
    DataTerminoObra = crmForm.all.new_dataprevistadeterminodeobra.DataValue;
    crmForm.CalculaDatasEngenharia();
}
