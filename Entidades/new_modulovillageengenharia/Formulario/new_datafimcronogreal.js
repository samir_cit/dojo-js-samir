﻿//calculo do mês----------------------------------------------------------------------------------//
if (crmForm.all.new_datafimcronogreal.DataValue != null) {
    var DataAtual = new Date();
    var cronograma = crmForm.all.new_datafimcronogreal.DataValue;

    if (cronograma > DataAtual) {
        alert("Data Fim do Cronograma Real (Físico) superior ao permitido.");
        crmForm.all.new_datafimcronogreal.DataValue = null;
        crmForm.all.new_data_entrega_chaves.DataValue = null;
		crmForm.all.new_data_aceite_at.DataValue = null;
    }
}

crmForm.CalculaDatasEngenharia();

//----------------------------------------------------------------------------------------------
//Se este campo preenchido habilita campo Data Aceite
crmForm.CompararDataAt(crmForm.all.new_datafimcronogreal,crmForm.all.new_data_aceite_at, crmForm.all.new_data_entrega_chaves, "A data informada no campo Data de Aceite da AT deve ser maior ou igual a  data do campo Data Real do Fim do Cronograma");

if (!crmForm.UsuarioPertenceEquipe(Equipe.AT_PRAZOS_GERENCIAL) && !crmForm.CompararMeses(crmForm.all.new_datafimcronogreal.DataValue)) {
    alert("O Campo Data Fim Cronog. Real (Físico) - 1ª Vistoria AT deve ser preenchido com data do mês corrente e menor ou igual que data atual, favor corrigir a informação.")
    crmForm.all.new_datafimcronogreal.DataValue = null;
    crmForm.all.new_data_entrega_chaves.DataValue = null;
    crmForm.all.new_data_aceite_at.DataValue = null;
}

//Desabilita campo Entrega de chaves caso o campo Data Aceite At estiver vazio e Desabilita campo Data Aceite caso o campo Data Fim Cronog.Real estiver vazio
if (crmForm.UsuarioPertenceEquipe("AT_PRAZOS") || crmForm.UsuarioPertenceEquipe(Equipe.AT_PRAZOS_GERENCIAL))
{     
	 crmForm.all.new_data_entrega_chaves.Disabled = (crmForm.all.new_data_aceite_at.DataValue == null);
	 crmForm.all.new_data_aceite_at.Disabled = (crmForm.all.new_datafimcronogreal.DataValue == null);
}