﻿// Transformar caracteres em maiusculos------------------------------------//
var elm = document.getElementsByTagName("input");
for (i = 0; i < elm.length; i++) {
    if (elm[i].type == "text")
        if (elm[i].DataValue != null) {
        try {
            elm[i].DataValue = elm[i].DataValue.toUpperCase();
        }
        catch (e) { }
    }
}

crmForm.HouveAlteracaoDasDataPrevistasDoCronograma(true);

function HabilitarCamposSalvar() {
    var atributos = new Array(
                              "new_usuario_1_alteracao", "new_usuario_2_alteracao", "new_usuario_3_alteracao", 
                              "new_usuario_4_alteracao", "new_usuario_5_alteracao", "new_usuario_6_alteracao", 
                              "new_usuario_7_alteracao", "new_usuario_8_alteracao", "new_usuario_9_alteracao", "new_usuario_10_alteracao",
                              "new_dataprevistadeemissocnd", "new_data_prev_protocolo_atu_cnd", "new_data_prev_emissao_atu_cnd",
                              "new_dt_prev_entrega_material", "new_dataprevistaentradaprotocolo", "new_dataprevistadeaverbao",
                              "new_data_prev_protocolo_atu_averb", "new_data_prev_averbacao_atu", "new_dataprevistainicio",
                              "new_dataprevistadeconcluso", "new_data_prev_inicio_proc_atu", "new_data_prev_conclusao_proc_atu",
                              "new_dataprevistadeterminodeobra", "new_atua_dt_protocolo_atualizada_averb",
                              "new_atua_dt_averbacao_atualizada", "new_cancela_lancamento", "new_tipo", "new_dataprevistadoprotocolo",
                              "new_dataprevistadeemissoiss", "new_dataprevistaprotocolocb", "new_dataprevistadeemissocb",
                              "new_dataprevistaprotocolohab", "new_dataprevistadeemissohab", "new_data_prev_protocolo_atu_hab",
                              "new_data_prev_emissao_atu_hab", "new_dataprevistaprotocolocnd",
                              "new_data_limite_aquisicao_kit_acabamento", "new_datainiciocronogreal", "new_data_real_de_alvenaria");
    for (var index = 0; index < atributos.length; index++) {
        eval("crmForm.all." + atributos[index]).Disabled = false;
    }
}

HabilitarCamposSalvar();

if (crmForm.UsuarioPertenceEquipe(Equipe.AT_PRAZOS_GERENCIAL)) {
    crmForm.all.new_datafimcronogreal.Disabled = false;
    crmForm.all.new_data_aceite_at.Disabled = false;
    crmForm.all.new_data_entrega_chaves.Disabled = false;
} else {
    if (crmForm.all.new_datafimcronogreal.IsDirty) {
        crmForm.all.new_datafimcronogreal.Disabled = false;
    }
    if (crmForm.all.new_data_aceite_at.IsDirty) {
        crmForm.all.new_data_aceite_at.Disabled = false;
    }
    if (crmForm.all.new_data_entrega_chaves.IsDirty) {
        crmForm.all.new_data_entrega_chaves.Disabled = false;
    }
}
if (!crmForm.AtualizarDataPrevistaEntregaChaves()) {
	event.returnValue = false;
	return false;
}
