﻿if (crmForm.all.new_data_prev_protocolo_atu_hab.DataValue == null) 
{
    alert("Você deve fornecer um valor para Data Prevista de Protocolo Atualizada.");
    crmForm.all.new_data_prev_protocolo_atu_hab.DataValue = DataPrevProtocoloAtuHab;
    crmForm.all.new_data_prev_protocolo_atu_hab.SetFocus();
}
else 
{
    if (crmForm.DifDtProtocoloDtEmissao()) 
    {
        DataPrevProtocoloAtuHab = crmForm.all.new_data_prev_protocolo_atu_hab.DataValue;
        crmForm.CalculaDatasEngenharia();
    }    
    else
        crmForm.all.new_data_prev_protocolo_atu_hab.DataValue = DataPrevProtocoloAtuHab;
}    
