﻿if ((crmForm.all.new_datafimcronog.DataValue != null) &&
    (crmForm.all.new_datainiciocronog.DataValue != null)) 
{
    crmForm.all.new_duracaodaobra.DataValue = crmForm.CalculaMeses(crmForm.all.new_datainiciocronog.DataValue, crmForm.all.new_datafimcronog.DataValue);
}
else 
    crmForm.all.new_duracaodaobra.DataValue = null;

//---------------------------------------------------------------------------------------------------------------//

if ((crmForm.all.new_datafimcronog.DataValue != null) &&
    (crmForm.all.new_dataprimeirocontratocliente.DataValue != null) &&
    (crmForm.all.new_datafimcronog.DataValue > crmForm.all.new_dataprimeirocontratocliente.DataValue)) 
{
    crmForm.all.new_atraso.DataValue = crmForm.CalculaMeses(crmForm.all.new_dataprimeirocontratocliente.DataValue, crmForm.all.new_datafimcronog.DataValue);
    crmForm.all.new_atraso_c.style.visibility = 'visible';
    crmForm.all.new_atraso_d.style.visibility = 'visible';
}
else 
{
    crmForm.all.new_atraso.DataValue = null;
    crmForm.all.new_atraso_c.style.visibility = 'hidden';
    crmForm.all.new_atraso_d.style.visibility = 'hidden';
}