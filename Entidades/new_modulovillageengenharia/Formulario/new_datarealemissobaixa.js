﻿// Se não informou Data Real Emissão, desabilita Número do Protocolo e o 
// automático/manual das datas atualizadas da Averbação

if (crmForm.all.new_datarealemissobaixa.DataValue == null)
{
    crmForm.all.new_num_protocolo_habitese_c.style.visibility = 'hidden';
    crmForm.all.new_num_protocolo_habitese_d.style.visibility = 'hidden';
    crmForm.all.new_num_protocolo_habitese.DataValue = null;
    crmForm.SetFieldReqLevel('new_num_protocolo_habitese', false);
    
    crmForm.all.new_atua_dt_protocolo_atualizada_averb.Disabled = true;
    crmForm.all.new_atua_dt_protocolo_atualizada_averb.DataValue = true;
    crmForm.all.new_data_prev_protocolo_atu_averb.Disabled = true;
    
    crmForm.all.new_atua_dt_averbacao_atualizada.Disabled = true;
    crmForm.all.new_atua_dt_averbacao_atualizada.DataValue = true;
    crmForm.all.new_data_prev_averbacao_atu.Disabled = true;
}
else 
{
    crmForm.all.new_num_protocolo_habitese_c.style.visibility = 'visible';
    crmForm.all.new_num_protocolo_habitese_d.style.visibility = 'visible';
    crmForm.SetFieldReqLevel('new_num_protocolo_habitese', true);
    
    new_atua_dt_averbacao_atualizada.Disabled = false;
    new_atua_dt_protocolo_atualizada_averb.Disabled = false;
}

if (AtualizaDatas)
    crmForm.CalculaDatasEngenharia();
