﻿if (crmForm.all.new_atua_dt_termino_obra.DataValue != null && crmForm.all.new_atua_dt_termino_obra.DataValue == true) 
{
    // Atualização Automática
    crmForm.all.new_dataprevistadeterminodeobra.Disabled = true;
    
    var Data;
    if (crmForm.all.new_datafimcronogreal.DataValue != null)
        Data = crmForm.all.new_datafimcronogreal.DataValue;
    else if (crmForm.all.new_datafimcronog.DataValue != null)
        Data = crmForm.all.new_datafimcronog.DataValue;
    else if (crmForm.all.new_dt_fim_1cronog_previsto.DataValue != null)
        Data = crmForm.all.new_dt_fim_1cronog_previsto.DataValue;
    
    crmForm.all.new_dataprevistadeterminodeobra.DataValue = Data;
    DataTerminoObra = crmForm.all.new_dataprevistadeterminodeobra.DataValue;
    
    if (AtualizaDatas) 
        crmForm.CalculaDatasEngenharia();
}
else 
{
    // Atualização Manual
    crmForm.all.new_dataprevistadeterminodeobra.Disabled = false
}
