﻿crmForm.CompararDataAt(crmForm.all.new_datafimcronogreal,crmForm.all.new_data_aceite_at, crmForm.all.new_data_entrega_chaves, 
					   "A data informada no campo Lib. Entrega de Chaves AT deve ser maior ou igual a  data do campo Data Aceite da AT",
					   "A data informada no campo Lib. Entrega de chaves AT deve ser maior ou igual a  data do campo Data de Aceite da AT");
if (!crmForm.UsuarioPertenceEquipe(Equipe.AT_PRAZOS_GERENCIAL) && !crmForm.CompararMeses(crmForm.all.new_data_entrega_chaves.DataValue)) {
    alert("O Campo Lib. Entrega de chaves AT deve ser preenchido com data do mês corrente e menor ou igual que data atual, favor corrigir a informação.")
    crmForm.all.new_data_entrega_chaves.DataValue = null;
}