﻿formularioValido = true;
changedAlteracao = false;
nomeUsuarioLogado = "";
idUsuarioLogado = "";

TipoEntidade = {
    BLOCO: 1,
    MODULO_VILLAGE: 2
}
TipoLogCampos = {
    ELEGIBILIDADE: 1,
    DATA_PREVISTA_ENTREGA_CHAVES: 2
}

try {
    Equipe = {
        ENG_KIT_ACABAMENTO: "ENG_Kit_Acabamento",
        ADMINISTRADORES: "Administradores",
        NUCLEO_NOTIFICACAO: "Núcleo - Notificação",
        NUCLEO_NIAC: "Núcleo - NIAC",
        NUCLEO_ENCANTE: "Núcleo - Encante",
        NUCLEO_CONTROLE_ESTRATEGICO: "Núcleo - Controle Estratégico",
        DESENV_IMOBILIARIO: "Desenv.Imobiliário",
        ENGENHARIA_PLANEJAMENTO: "Engenharia - Planejamento",
        COORDENAÇÃO_KIT_ACABAMENTO: "Coordenação - Kit Acabamento",
        ENGENHARIA_PRAZOS: "Engenharia - Prazos",
        BLOCO_VALOR_GLOBAL_DE_VENDAS: "Bloco - Valor Global de Vendas",
        AT_PRAZOS: "AT_Prazos",
        AT_PRAZOS_GERENCIAL: "AT_Prazos_Gerencial"

    }
    //Adiciona Script para ser usado no click do botão
    var elm = document.createElement("script");
    elm.src = "/_static/_grid/cmds/util.js";
    document.appendChild(elm);

    crmForm.DataModificacao = function(campo, atributos) {
        var tmp = atributos.split(";");
        var tipo = "";

        if (eval(campo).DataValue == true)
            tipo = "";
        else
            tipo = "none";

        for (var index = 0; index < tmp.length; index++) {
            document.getElementById(tmp[index] + "_c").style.display = tipo;
            document.getElementById(tmp[index] + "_d").style.display = tipo;
        }
    }
    //grava o usuário no respectivo numero do campo de alteração
    crmForm.GravaUsuario = function(campoAlteracao, campoUsuario) {
        if (campoAlteracao.DataValue && changedAlteracao)
            crmForm.GravarUsuarioLogado(campoUsuario);
    }

    crmForm.EscondeCampo = function(nomeCampo, esconde) {
        var tipo = "";
        if (esconde)
            tipo = "none";

        var atributo_c = eval("crmForm.all." + nomeCampo + "_c");
        var atributo_d = eval("crmForm.all." + nomeCampo + "_d");
        atributo_c.style.display = tipo;
        atributo_d.style.display = tipo;
    }

    crmForm.CompararMeses = function(Data) {
        if (Data == null)
            return true;
        var year = Data.getFullYear();
        var month = Data.getMonth();
        var dia = Data.getDate();
        var today = new Date();
        if (month == today.getMonth() && year == today.getFullYear() && dia <= today.getDate()) {
            return true;
        } else {
            return false;
        }
    }

    var atributosDataLog = new Array("new_data_1_alteracao_log", "new_data_2_alteracao_log", "new_data_3_alteracao_log", "new_data_4_alteracao_log", "new_data_5_alteracao_log", "new_data_6_alteracao_log", "new_data_7_alteracao_log", "new_data_8_alteracao_log", "new_data_9_alteracao_log", "new_data_10_alteracao_log");
    var atributosDatasAlteracoes = new Array("new_1alterao", "new_2alterao", "new_3alterao", "new_4alterao", "new_5alterao", "new_6alterao", "new_7alterao", "new_8alterao", "new_9alterao", "new_10alterao");
    crmForm.Log = function(atributo) {
        //achar posição no array
        var posicao = -1;
        for (var index = 0; index < atributosDatasAlteracoes.length; index++) {
            if (atributo == atributosDatasAlteracoes[index]) {
                posicao = index;
                break;
            }
        }

        if (eval("crmForm.all." + atributosDatasAlteracoes[posicao]).IsDirty) {
            eval("crmForm.all." + atributosDataLog[posicao]).DataValue = new Date();
            eval("crmForm.all." + atributosDataLog[posicao]).ForceSubmit = true;
        }
    }

    // Se for atualização automática, atualiza a Data Prevista de Término da Obra
    crmForm.AtualizaDataTerminoObra = function() {
        if (!crmForm.all.new_atua_dt_termino_obra.DataValue)
            return;

        var Data = crmForm.all.new_10alterao.DataValue != null
			 ? crmForm.all.new_10alterao.DataValue
			 : crmForm.all.new_9alterao.DataValue != null
			 ? crmForm.all.new_9alterao.DataValue
			 : crmForm.all.new_8alterao.DataValue != null
			 ? crmForm.all.new_8alterao.DataValue
			 : crmForm.all.new_7alterao.DataValue != null
			 ? crmForm.all.new_7alterao.DataValue
			 : crmForm.all.new_6alterao.DataValue != null
			 ? crmForm.all.new_6alterao.DataValue
			 : crmForm.all.new_5alterao.DataValue != null
			 ? crmForm.all.new_5alterao.DataValue
			 : crmForm.all.new_4alterao.DataValue != null
			 ? crmForm.all.new_4alterao.DataValue
			 : crmForm.all.new_3alterao.DataValue != null
			 ? crmForm.all.new_3alterao.DataValue
			 : crmForm.all.new_2alterao.DataValue != null
			 ? crmForm.all.new_2alterao.DataValue
			 : crmForm.all.new_1alterao.DataValue != null
			 ? crmForm.all.new_1alterao.DataValue
			 : crmForm.all.new_dataprimeirocontratocliente.DataValue;

        DataTerminoObra
			 = crmForm.all.new_dataprevistadeterminodeobra.DataValue
			 = Data;
    }

    crmForm.VerificaDuracaoObra = function() {
        if ((crmForm.all.new_data_aceite_at.DataValue != null) &&
			(crmForm.all.new_datainiciocronog.DataValue != null)) {
            crmForm.all.new_duracaodaobra.DataValue = crmForm.CalculaMeses(crmForm.all.new_datainiciocronog.DataValue, crmForm.all.new_data_aceite_at.DataValue);
        } else
            crmForm.all.new_duracaodaobra.DataValue = null;
    }

    crmForm.CalculaMeses = function(dtInicio, dtFim) {
        dif = dtFim.getTime() - dtInicio.getTime();
        dia = 1000 * 60 * 60 * 24;
        diaDif = dif / dia;
        return Math.round(Math.round(diaDif) / 30);
    }

    crmForm.VerificaAtrasoObra = function() {
        if ((crmForm.all.new_datafimcronog.DataValue != null) &&
			(crmForm.all.new_dataprimeirocontratocliente.DataValue != null) &&
			(crmForm.all.new_datafimcronog.DataValue > crmForm.all.new_dataprimeirocontratocliente.DataValue)) {
            crmForm.all.new_atraso.DataValue = crmForm.CalculaMeses(crmForm.all.new_dataprimeirocontratocliente.DataValue, crmForm.all.new_datafimcronog.DataValue);
            crmForm.all.new_atraso_c.style.visibility = 'visible';
            crmForm.all.new_atraso_d.style.visibility = 'visible';
        } else {
            crmForm.all.new_atraso.DataValue = null;
            crmForm.all.new_atraso_c.style.visibility = 'hidden';
            crmForm.all.new_atraso_d.style.visibility = 'hidden';
        }
    }

    //GUIA 1================================================

    //SEÇÃO 01 - ocultando campo nome-----------------------
    crmForm.all.new_name_c.style.visibility = 'hidden';
    crmForm.all.new_name_d.style.visibility = 'hidden';

    //SEÇÃO 02 - desabilitando campos--------------------------
    crmForm.all.new_datadetrminodaobra.Disabled = true;

    //tornando o preenchimento do campo obrigatorio--------------//
    if (crmForm.all.new_pilotis.DataValue == 1) {
        crmForm.all.new_tipodepilotis_c.style.visibility = 'visible';
        crmForm.all.new_tipodepilotis_d.style.visibility = 'visible';
        crmForm.SetFieldReqLevel("new_tipodepilotis", true);
    } else {
        crmForm.SetFieldReqLevel("new_tipodepilotis", false);
        crmForm.all.new_tipodepilotis_c.style.visibility = 'hidden';
        crmForm.all.new_tipodepilotis_d.style.visibility = 'hidden';
        crmForm.all.new_tipodepilotis.DataValue = null;
    }

    //tornando o preenchimento do campo obrigatorio
    if (crmForm.all.new_elevador.DataValue == 1) {
        crmForm.all.new_quantidadeelevadores_c.style.visibility = 'visible';
        crmForm.all.new_quantidadeelevadores_d.style.visibility = 'visible';
        crmForm.SetFieldReqLevel("new_quantidadeelevadores", true);
    } else {
        crmForm.SetFieldReqLevel("new_quantidadeelevadores", false);
        crmForm.all.new_quantidadeelevadores_c.style.visibility = 'hidden';
        crmForm.all.new_quantidadeelevadores_d.style.visibility = 'hidden';
        crmForm.all.new_quantidadeelevadores.DataValue = null;
    }

    //GUIA 02===============================================

    //SEÇÃO 03 -  SCRIPT TABELA DE PRAZOS------------------
    crmForm.all.new_dataprimeirocontratocliente.FireOnChange();

    changedAlteracao = false;

    //ALTERAÇÃO HABILITAR CAMPOS ----------------------------------------
    crmForm.all.new_existealterao.FireOnChange();
    crmForm.all.new_2alteracao.FireOnChange();
    crmForm.all.new_3alteracao.FireOnChange();
    crmForm.all.new_4alteracao.FireOnChange();
    crmForm.all.new_5alteracao.FireOnChange();
    crmForm.all.new_6alteracao.FireOnChange();
    crmForm.all.new_7alteracao.FireOnChange();
    crmForm.all.new_8alteracao.FireOnChange();
    crmForm.all.new_9alteracao.FireOnChange();
    crmForm.all.new_10alteracao.FireOnChange();

    changedAlteracao = true;

    //GUIA 03 ==HABITE-SE=======================================

    //Desabiltar seção ------ISS------------------------------------------------------------------

    crmForm.all.new_possuiiss.FireOnChange();

    //Desabiltar seção -----ELEVADO GUIA -03-------------------------------------------------

    if (crmForm.all.new_elevador.DataValue == false) {
        document.getElementById("new_laudotecnicodeinstalaoeletrica_d").parentNode.parentNode.style.display = "none";
        document.getElementById("new_obselevador_d").parentNode.parentNode.style.display = "none";
    } else {
        document.getElementById("new_laudotecnicodeinstalaoeletrica_d").parentNode.parentNode.style.display = "inline";
        document.getElementById("new_obselevador_d").parentNode.parentNode.style.display = "inline";
    }

    //Desabiltar seção -----STATUS DO HABITE-SE-------------------------------------------

    if (crmForm.all.new_statushabitese.DataValue == 2) {
        document.getElementById("new_novadatadevistoria_d").parentNode.parentNode.style.display = "inline";
    } else {
        document.getElementById("new_novadatadevistoria_d").parentNode.parentNode.style.display = "none";
    }

    // Atualiza o campo 'Atualização da Data do Término da Obra' como 'Automática'.
    if (crmForm.all.new_atua_dt_termino_obra.DataValue == null) {
        crmForm.all.new_atua_dt_termino_obra.DataValue = true;
    }

    // Atualiza o campo 'Atualização Dt Prev Protocolo Atualizada' como 'Automática'.
    if (crmForm.all.new_atua_dt_atualizada_habitese.DataValue == null) {
        crmForm.all.new_atua_dt_atualizada_habitese.DataValue = true;
    }

    // Atualiza o campo 'Atualização Dt Prev Emissão Atualizada' como 'Automática'.
    if (crmForm.all.new_atua_dt_emis_atualizada_hab.DataValue == null) {
        crmForm.all.new_atua_dt_emis_atualizada_hab.DataValue = true;
    }

    // Atualiza o campo 'Atualização Dt Prev Protocolo Atualizada da Averbação' como 'Automática'.
    if (crmForm.all.new_atua_dt_protocolo_atualizada_averb.DataValue == null) {
        crmForm.all.new_atua_dt_protocolo_atualizada_averb.DataValue = true;
    }

    // Atualiza o campo 'Atualização Dt de Averbação Atualizada' como 'Automática'.
    if (crmForm.all.new_atua_dt_averbacao_atualizada.DataValue == null) {
        crmForm.all.new_atua_dt_averbacao_atualizada.DataValue = true;
    }

    AtualizaDatas = false;
    crmForm.all.new_datarealemissobaixa.FireOnChange();
    crmForm.all.new_atua_dt_termino_obra.FireOnChange();
    crmForm.all.new_atua_dt_atualizada_habitese.FireOnChange();
    crmForm.all.new_atua_dt_emis_atualizada_hab.FireOnChange();
    crmForm.all.new_atua_dt_protocolo_atualizada_averb.FireOnChange();
    crmForm.all.new_atua_dt_averbacao_atualizada.FireOnChange();
    AtualizaDatas = true;

    //Desabiltar seção ---INSS - PENDECIA ---------------------------------------------------

    if (crmForm.all.new_statusdapesquisa.DataValue == 1) {
        document.getElementById("new_obsinss_d").parentNode.parentNode.style.display = "inline";
    } else {
        document.getElementById("new_obsinss_d").parentNode.parentNode.style.display = "none";
    }

    //script para aparecer a data de entrega d material, cas a empresa esteja contratada----
    if (crmForm.all.new_empresacontratada.DataValue == false) {
        crmForm.all.new_dataentregadomaterial_c.style.visibility = 'hidden';
        crmForm.all.new_dataentregadomaterial_d.style.visibility = 'hidden';
    } else {
        crmForm.all.new_dataentregadomaterial_c.style.visibility = 'visible';
        crmForm.all.new_dataentregadomaterial_d.style.visibility = 'visible';
    }

    //script para mostrar status do -- Doc Elevador------------------------------------

    crmForm.all.new_laudotecnicodeinstalaoeletrica.FireOnChange();

    //script para mostrar status do doc INSS------------------------------------

    if ((crmForm.all.new_copiaautenticadadohabitese.DataValue == false) ||
		(crmForm.all.new_guiaautenticadagps.DataValue == false) ||
		(crmForm.all.new_copiadocei.DataValue == false) ||
		(crmForm.all.new_pesquisaderestries.DataValue == false) ||
		(crmForm.all.new_statusdapesquisa.DataValue == 1)) {
        crmForm.all.new_statusdocinss.DataValue = 1;
    } else {
        crmForm.all.new_statusdocinss.DataValue = 2;
    }

    //==DESABILITAR CAMPOS=====================================
    //crmForm.all.new_statusgeral.Disabled = true;
    crmForm.all.new_cancela_lancamento.Disabled = true;

    // GUIA02 - DURAÇÃO DE OBRA-o--------------------
    crmForm.all.new_duracaodaobra.readOnly = true;

    //GUIA 03 - ISS------------------------------------------
    crmForm.all.new_reamdiadasunidadesm.readOnly = true;
    crmForm.all.new_reaaregularizarm.readOnly = true;
    crmForm.all.new_total.readOnly = true;
    crmForm.all.new_issdomunicpio01.readOnly = true;
    crmForm.all.new_porcentagem5.readOnly = true;
    crmForm.all.new_total2.readOnly = true;
    crmForm.all.new_totalapagar.readOnly = true;

    //GUIA 03 - BOMBEIRO--------------------------------
    crmForm.all.new_areado_modulo_village.readOnly = true;

    //GUIA 03 - STATUS
    crmForm.all.new_statusdoc.Disabled = true;
    crmForm.all.new_statusart.Disabled = true;
    crmForm.all.new_statusdocinss.Disabled = true;

    //Retirar opções do picklist e deixar uma como default----//
    crmForm.all.new_tipo.DeleteOption(1);
    crmForm.all.new_tipo.DeleteOption(2);
    crmForm.all.new_tipo.DataValue = 3;

    //apagar o campo de cálculo de atraso na entrega de obra(ebgenharia), caso exista------------------//
    if (crmForm.all.new_atraso.DataValue != null) {
        crmForm.all.new_atraso_c.style.visibility = 'visible';
        crmForm.all.new_atraso_d.style.visibility = 'visible';
    } else {
        crmForm.all.new_atraso_c.style.visibility = 'hidden';
        crmForm.all.new_atraso_d.style.visibility = 'hidden';
    }

    //função para desbloquear alteração do tipo do bloco-------------------------------------------------//
    crmForm.all.new_tipo.DataValue = 3;
    crmForm.all.new_tipo.Disabled = true;

    //Guarda a data de início 1 cronograma
    if (crmForm.all.new_dt_inicio_1cronog_previsto.DataValue != null)
        crmForm.all.new_data_inicio_primeiro_cronog.DataValue = crmForm.all.new_dt_inicio_1cronog_previsto.DataValue;

    if (crmForm.all.new_dt_fim_1cronog_previsto.DataValue != null)
        crmForm.all.new_data_fim_primeiro_cronog.DataValue = crmForm.all.new_dt_fim_1cronog_previsto.DataValue;

    //Esconde campo.
    crmForm.all.new_data_inicio_primeiro_cronog_c.style.visibility = "hidden";
    crmForm.all.new_data_inicio_primeiro_cronog_d.style.visibility = "hidden";
    crmForm.all.new_data_fim_primeiro_cronog_c.style.visibility = "hidden";
    crmForm.all.new_data_fim_primeiro_cronog_d.style.visibility = "hidden";

    crmForm.UsuarioPertenceEquipe = function(nomeEquipe) {
        var cmd = new RemoteCommand("MrvService", "UserContainsTeams", "/MRVCustomizations/");
        cmd.SetParameter("teams", nomeEquipe);
        var resultado = cmd.Execute();
        return resultado.Success ? resultado.ReturnValue.Mrv.Found : false;
    }

    crmForm.GravarUsuarioLogado = function(objeto) {

        if (nomeUsuarioLogado == "" && idUsuarioLogado == "") {
            var cmd = new RemoteCommand("MrvService", "ObterDadosUsuario", "/MRVCustomizations/");

            var resultado = cmd.Execute();
            if (resultado.Success) {
                nomeUsuarioLogado = resultado.ReturnValue.Mrv.Nome;
                idUsuarioLogado = resultado.ReturnValue.Mrv.Id;

            }
        }

        objeto.DataValue = nomeUsuarioLogado;
    }

    //Desabilitar o campo da tabela de prazo para quem nao pertence a equipe de Engenharia-Planejamento
    if (!crmForm.UsuarioPertenceEquipe("Engenharia - Planejamento")) {
        crmForm.all.new_statusgeral.Disabled = true;
        crmForm.all.new_dt_inicio_1cronog_previsto.Disabled = true;
        crmForm.all.new_entrega_chaves_pos_assinatura.Disabled = true;
        crmForm.all.new_dt_fim_1cronog_previsto.Disabled = true;
        crmForm.all.new_entrega_chaves_pos_assinatura.Disabled = true;
        crmForm.all.new_duracao_obra_1cronog.Disabled = true;
        crmForm.all.new_datainiciocronog.Disabled = true;
        crmForm.all.new_datafimcronog.Disabled = true;
        crmForm.all.new_duracaodaobra.Disabled = true;
        crmForm.all.new_datafimcronogreal.Disabled = true;
        crmForm.all.new_duracaodaobrareal.Disabled = true;
        crmForm.all.new_observaodaobra.Disabled = true;
        crmForm.all.new_dataprimeirocontratocliente.Disabled = true;
        crmForm.all.new_atraso.Disabled = true;
        crmForm.all.new_existealterao.Disabled = true;
        crmForm.all.new_1alterao.Disabled = true;
        crmForm.all.new_1observao.Disabled = true;
        crmForm.all.new_2alteracao.Disabled = true;
        crmForm.all.new_2alterao.Disabled = true;
        crmForm.all.new_2observao.Disabled = true;
        crmForm.all.new_3alteracao.Disabled = true;
        crmForm.all.new_3alterao.Disabled = true;
        crmForm.all.new_3observao.Disabled = true;
        crmForm.all.new_4alteracao.Disabled = true;
        crmForm.all.new_4alterao.Disabled = true;
        crmForm.all.new_4observao.Disabled = true;
        crmForm.all.new_5alteracao.Disabled = true;
        crmForm.all.new_5alterao.Disabled = true;
        crmForm.all.new_5observao.Disabled = true;
        crmForm.all.new_6alteracao.Disabled = true;
        crmForm.all.new_6alterao.Disabled = true;
        crmForm.all.new_6observao.Disabled = true;
        crmForm.all.new_7alteracao.Disabled = true;
        crmForm.all.new_7alterao.Disabled = true;
        crmForm.all.new_7observao.Disabled = true;
        crmForm.all.new_8alteracao.Disabled = true;
        crmForm.all.new_8alterao.Disabled = true;
        crmForm.all.new_8observao.Disabled = true;
        crmForm.all.new_9alteracao.Disabled = true;
        crmForm.all.new_9alterao.Disabled = true;
        crmForm.all.new_9observao.Disabled = true;
        crmForm.all.new_10alteracao.Disabled = true;
        crmForm.all.new_10alterao.Disabled = true;
        crmForm.all.new_10observao.Disabled = true;
    }

    //Desabilitar campos
    crmForm.all.new_dataprevistadoprotocolo.Disabled = true;
    crmForm.all.new_dataprevistadeemissoiss.Disabled = true;

    crmForm.all.new_dataprevistaprotocolocb.Disabled = true;
    crmForm.all.new_dataprevistadeemissocb.Disabled = true;

    crmForm.all.new_dataprevistaprotocolohab.Disabled = true;
    crmForm.all.new_dataprevistadeemissohab.Disabled = true;

    crmForm.all.new_dataprevistaprotocolocnd.Disabled = true;
    crmForm.all.new_dataprevistadeemissocnd.Disabled = true;
    crmForm.all.new_data_prev_protocolo_atu_cnd.Disabled = true;
    crmForm.all.new_data_prev_emissao_atu_cnd.Disabled = true;

    crmForm.all.new_dt_prev_entrega_material.Disabled = true;

    crmForm.all.new_dataprevistaentradaprotocolo.Disabled = true;
    crmForm.all.new_dataprevistadeaverbao.Disabled = true;
    crmForm.all.new_data_prev_protocolo_atu_averb.Disabled = true;
    crmForm.all.new_data_prev_averbacao_atu.Disabled = true;

    crmForm.all.new_dataprevistainicio.Disabled = true;
    crmForm.all.new_dataprevistadeconcluso.Disabled = true;
    crmForm.all.new_data_prev_inicio_proc_atu.Disabled = true;
    crmForm.all.new_data_prev_conclusao_proc_atu.Disabled = true;

    // Salva a data de termino da obra, a data prevista protocolo atualizada e a data prevista emissão atualizada
    DataPrevEmissaoAtuHab = crmForm.all.new_data_prev_emissao_atu_hab.DataValue == null ? null : crmForm.all.new_data_prev_emissao_atu_hab.DataValue;
    DataPrevProtocoloAtuHab = crmForm.all.new_data_prev_protocolo_atu_hab.DataValue == null ? null : crmForm.all.new_data_prev_protocolo_atu_hab.DataValue;
    DataTerminoObra = crmForm.all.new_dataprevistadeterminodeobra.DataValue == null ? null : crmForm.all.new_dataprevistadeterminodeobra.DataValue;

    // Verifica se a diferença entre a data prevista protocolo atualizada e a data prevista emissão atualizada
    // é maior que 30 dias
    crmForm.DifDtProtocoloDtEmissao = function() {
        if (crmForm.all.new_data_prev_emissao_atu_hab.DataValue != null && crmForm.all.new_data_prev_protocolo_atu_hab.DataValue != null) {
            var Data1 = crmForm.all.new_data_prev_emissao_atu_hab.DataValue;
            var Data2 = crmForm.all.new_data_prev_protocolo_atu_hab.DataValue;
            var Dif = Date.UTC(Data1.getYear(), Data1.getMonth(), Data1.getDate(), 0, 0, 0) -
				Date.UTC(Data2.getYear(), Data2.getMonth(), Data2.getDate(), 0, 0, 0);
            Dif = (Dif / 1000 / 60 / 60 / 24);

            if (Dif < 30)
                alert("A diferença entre a Data Prevista de Protocolo Atualizada e a Data Prevista de Emissão Atualizada \ndeve ser maior ou igual a 30 dias!");

            return (Dif >= 30);
        }

        return true;
    }

    // Soma dias a uma data
    crmForm.AddDays = function(Data, NumDias) {
        if (Data == null)
            return null;

        var Dt = new Date(Data.toString());
        return Dt.setDate(Dt.getDate() + NumDias)
    }

    /// Aba:    Doc. Encerramento de Obra
    /// Sessão: Pedido de Baixa de Obra - Habite-se
    crmForm.CalculaBaixaDeObraHabitese = function(dataContratoCliente) {
        if (crmForm.all.new_dataprevistaprotocolohab.DataValue == null)
            crmForm.all.new_dataprevistaprotocolohab.DataValue = crmForm.AddDays(DataTerminoObra, 30);

        if (crmForm.all.new_atua_dt_atualizada_habitese.DataValue)
            crmForm.all.new_data_prev_protocolo_atu_hab.DataValue = crmForm.AddDays(DataTerminoObra, 30);

        if (crmForm.all.new_dataprevistadeemissohab.DataValue == null)
            crmForm.all.new_dataprevistadeemissohab.DataValue = crmForm.AddDays(DataTerminoObra, 90);

        if (crmForm.all.new_atua_dt_emis_atualizada_hab.DataValue)
            crmForm.all.new_data_prev_emissao_atu_hab.DataValue = crmForm.AddDays(DataTerminoObra, 90);
    }

    /// Aba:    Doc. Encerramento de Obra
    /// Sessão: Averbação do Habite-se
    crmForm.CalculaAverbacaoHabitese = function() {
        var DataPrevistaProtocoloAverb = null;
        var DataPrevistaAverbacao = null;

        if (crmForm.all.new_datarealemissocnd.DataValue != null) {
            DataPrevistaProtocoloAverb = crmForm.AddDays(crmForm.all.new_datarealemissocnd.DataValue, 3);
            DataPrevistaAverbacao = crmForm.AddDays(crmForm.all.new_datarealemissocnd.DataValue, 125);
        } else if (crmForm.all.new_data_prev_emissao_atu_cnd.DataValue != null) {
            DataPrevistaProtocoloAverb = crmForm.AddDays(crmForm.all.new_data_prev_emissao_atu_cnd.DataValue, 3);
            DataPrevistaAverbacao = crmForm.AddDays(crmForm.all.new_data_prev_emissao_atu_cnd.DataValue, 125);
        }

        if (crmForm.all.new_dataprevistaentradaprotocolo.DataValue == null)
            crmForm.all.new_dataprevistaentradaprotocolo.DataValue = DataPrevistaProtocoloAverb;

        if (crmForm.all.new_dataprevistadeaverbao.DataValue == null)
            crmForm.all.new_dataprevistadeaverbao.DataValue = DataPrevistaAverbacao;

        if (crmForm.all.new_atua_dt_protocolo_atualizada_averb.DataValue)
            crmForm.all.new_data_prev_protocolo_atu_averb.DataValue = DataPrevistaProtocoloAverb;

        if (crmForm.all.new_atua_dt_averbacao_atualizada.DataValue)
            crmForm.all.new_data_prev_averbacao_atu.DataValue = DataPrevistaAverbacao;
    }

    /// Calcula as datas Habite-se/Averbação/Certidão/Laudo/etc.
    crmForm.CalculaDatasEngenharia = function() {
        var DataTerminoObra = crmForm.all.new_dataprevistadeterminodeobra.DataValue;

        //protocolo ISS, emissao ISS----------------------------//
        crmForm.all.new_dataprevistadoprotocolo.DataValue = crmForm.AddDays(DataTerminoObra, -30);
        crmForm.all.new_dataprevistadeemissoiss.DataValue = crmForm.all.new_dataprevistadeterminodeobra.DataValue;

        //protocolo Bombeiro, emissao Bombeiro--------------//
        crmForm.all.new_dataprevistaprotocolocb.DataValue = crmForm.AddDays(DataTerminoObra, -15);
        crmForm.all.new_dataprevistadeemissocb.DataValue = crmForm.AddDays(DataTerminoObra, 30);

        crmForm.CalculaBaixaDeObraHabitese(DataTerminoObra);

        //salva a data prevista protocolo atualizada e a data prevista emissão atualizada
        DataPrevEmissaoAtuHab = crmForm.all.new_data_prev_emissao_atu_hab.DataValue == null ? null : crmForm.all.new_data_prev_emissao_atu_hab.DataValue;
        DataPrevProtocoloAtuHab = crmForm.all.new_data_prev_protocolo_atu_hab.DataValue == null ? null : crmForm.all.new_data_prev_protocolo_atu_hab.DataValue;
        crmForm.DifDtProtocoloDtEmissao();

        //protocolo CND INSS, emissao CND INSS------------//
        var DataPrevistaProtocoloCND = null;
        var DataPrevistaEmissaoCND = null;

        if (crmForm.all.new_datarealemissobaixa.DataValue != null) {
            DataPrevistaProtocoloCND = crmForm.AddDays(crmForm.all.new_datarealemissobaixa.DataValue, 10);
            DataPrevistaEmissaoCND = crmForm.AddDays(crmForm.all.new_datarealemissobaixa.DataValue, 25);
        } else if (crmForm.all.new_data_prev_emissao_atu_hab.DataValue != null) {
            DataPrevistaProtocoloCND = crmForm.AddDays(crmForm.all.new_data_prev_emissao_atu_hab.DataValue, 10);
            DataPrevistaEmissaoCND = crmForm.AddDays(crmForm.all.new_data_prev_emissao_atu_hab.DataValue, 25);
        }

        if (crmForm.all.new_dataprevistaprotocolocnd.DataValue == null)
            crmForm.all.new_dataprevistaprotocolocnd.DataValue = DataPrevistaProtocoloCND;
        if (crmForm.all.new_dataprevistadeemissocnd.DataValue == null)
            crmForm.all.new_dataprevistadeemissocnd.DataValue = DataPrevistaEmissaoCND;

        crmForm.all.new_data_prev_protocolo_atu_cnd.DataValue = DataPrevistaProtocoloCND;
        crmForm.all.new_data_prev_emissao_atu_cnd.DataValue = DataPrevistaEmissaoCND;

        //Elaboração material instituição-----------------------//
        var DtPrevEntregaMaterial = null;

        if (crmForm.all.new_datarealprotocolocnd.DataValue != null)
            DtPrevEntregaMaterial = crmForm.AddDays(crmForm.all.new_datarealprotocolocnd.DataValue, 10);
        else if (crmForm.all.new_data_prev_protocolo_atu_cnd.DataValue != null)
            DtPrevEntregaMaterial = crmForm.AddDays(crmForm.all.new_data_prev_protocolo_atu_cnd.DataValue, 10);

        crmForm.all.new_dt_prev_entrega_material.DataValue = DtPrevEntregaMaterial;

        crmForm.CalculaAverbacaoHabitese();

        // Desmembramento IPTU-------------------------------------------//
        var DataPrevistaInicio = null;
        var DataPrevistaConclusao = null;

        if (crmForm.all.new_datarealemissobaixa.DataValue != null) {
            DataPrevistaInicio = crmForm.AddDays(crmForm.all.new_datarealemissobaixa.DataValue, 5);
            DataPrevistaConclusao = crmForm.AddDays(crmForm.all.new_datarealemissobaixa.DataValue, 35);
        } else if (crmForm.all.new_data_prev_emissao_atu_hab.DataValue != null) {
            DataPrevistaInicio = crmForm.AddDays(crmForm.all.new_data_prev_emissao_atu_hab.DataValue, 5);
            DataPrevistaConclusao = crmForm.AddDays(crmForm.all.new_data_prev_emissao_atu_hab.DataValue, 35);
        }

        if (crmForm.all.new_dataprevistainicio.DataValue == null)
            crmForm.all.new_dataprevistainicio.DataValue = DataPrevistaInicio;
        if (crmForm.all.new_dataprevistadeconcluso.DataValue == null)
            crmForm.all.new_dataprevistadeconcluso.DataValue = DataPrevistaConclusao;

        crmForm.all.new_data_prev_inicio_proc_atu.DataValue = DataPrevistaInicio;
        crmForm.all.new_data_prev_conclusao_proc_atu.DataValue = DataPrevistaConclusao;
    }

    // Desabilita a tab Doc Encerramento de Obra e Desm IPTU
    crmForm.all.tab2Tab.style.display = 'none';
    crmForm.all.tab3Tab.style.display = 'none';

    crmForm.DesabilitarFormulario = function(valor) {
        var todosAtributos = document.getElementsByTagName("label");
        for (var i = 0; i < todosAtributos.length; i++) {
            if (todosAtributos[i].htmlFor != "") {
                var atributo = eval("crmForm.all." + todosAtributos[i].htmlFor.replace("_ledit", ""));
                if (atributo != undefined && atributo != null)
                    atributo.Disabled = valor;
            }
        }
    }

    if (crmForm.UsuarioPertenceEquipe("Coordenação - KIT Acabamento")) {
        crmForm.DesabilitarFormulario(true);
    }

    /// Desabilita campo no formulário
    crmForm.DesabilitarCampos = function(campos, tipo) {
        var tmp = campos.toString().split(";");

        for (var i = 0; i < tmp.length; i++) {
            var atributo = eval("crmForm.all." + tmp[i]);
            if (atributo != null)
                atributo.Disabled = tipo;
        }
    }
    //Valida se usuario pertence a equipe AT_Prazos
    crmForm.ConfiguraPerfilAtPrazos = function() {


        crmForm.all.new_data_entrega_chaves.Disabled = true;
        crmForm.all.new_data_aceite_at.Disabled = true;
        crmForm.all.new_datafimcronogreal.Disabled = true;

        if (crmForm.UsuarioPertenceEquipe(Equipe.AT_PRAZOS_GERENCIAL)) {
            crmForm.all.new_datafimcronogreal.Disabled = false;
            //Desabilita campo Entrega de chaves caso o campo Data Aceite At estiver vazio e Desabilita campo Data Aceite caso o campo Data Fim Cronog.Real estiver vazio
            crmForm.all.new_data_aceite_at.Disabled = (crmForm.all.new_datafimcronogreal.DataValue == undefined);
            crmForm.all.new_data_entrega_chaves.Disabled = (crmForm.all.new_data_aceite_at.DataValue == undefined);
        } else {
            if (crmForm.UsuarioPertenceEquipe(Equipe.AT_PRAZOS)) {
                crmForm.all.new_datafimcronogreal.Disabled = (crmForm.all.new_datafimcronogreal.DataValue != undefined);

                if (crmForm.all.new_data_aceite_at.DataValue == undefined) {
                    crmForm.all.new_data_aceite_at.Disabled = (crmForm.all.new_datafimcronogreal.DataValue == undefined);
                } else {
                    crmForm.all.new_data_aceite_at.Disabled = true;
                    if (crmForm.all.new_data_entrega_chaves.DataValue == undefined) {
                        crmForm.all.new_data_entrega_chaves.Disabled = (crmForm.all.new_data_aceite_at.DataValue == undefined);
                    } else {
                        crmForm.all.new_data_entrega_chaves.Disabled = true;
                    }
                }
            } else {
                if (crmForm.UsuarioPertenceEquipe(Equipe.ENGENHARIA_PRAZOS)) {
                    crmForm.all.new_datafimcronogreal.Disabled = (crmForm.all.new_datafimcronogreal.DataValue != undefined);
                }
            }
        }
        if (crmForm.UsuarioPertenceEquipe(Equipe.ENTREGA_CHAVES_BLOCO)) {
            crmForm.all.new_data_entrega_chaves.Disabled = (crmForm.all.new_data_aceite_at.DataValue == undefined);
        }
    }

    crmForm.ConfiguraPerfilEngenhariaPrazos = function() {

        var Campos_TabelaPrazos_CronogramaAtual = "" +
			"new_statusgeral;new_data_previsao_entrega;" +
			"new_dt_inicio_1cronog_previsto;new_dt_fim_1cronog_previsto;new_duracao_obra_1cronog;" +
			"new_datainiciocronog;new_datafimcronog;new_duracaodaobra;" +
			"new_datafimcronogreal;new_duracaodaobrareal;" +
			"new_observaodaobra;new_motivo_alteracao";

        var Campos_TabelaPrazos_PrevisaoTerminoObraAtualizado = "" +
			"new_dataprimeirocontratocliente;new_atraso;" +
			"new_existealterao;new_1alterao;new_1observao;" +
			"new_2alteracao;new_2alterao;new_2observao;" +
			"new_3alteracao;new_3alterao;new_3observao;" +
			"new_4alteracao;new_4alterao;new_4observao;" +
			"new_5alteracao;new_5alterao;new_5observao;" +
			"new_6alteracao;new_6alterao;new_6observao;" +
			"new_7alteracao;new_7alterao;new_7observao;" +
			"new_8alteracao;new_8alterao;new_8observao;" +
			"new_9alteracao;new_9alterao;new_9observao;" +
			"new_10alteracao;new_10alterao;new_10observao";

        var Campos_Contrato_Cliente = "new_observacao_alteracao";

        //Verifica se usuário logado pertence a equipe 'Engenharia - Prazos'
        var perfilEngenhariaPrazos = crmForm.UsuarioPertenceEquipe("Engenharia - Prazos");
        var perfilEngenhariaPrazosAtualizaPrevistaAverbacao = crmForm.UsuarioPertenceEquipe("Altera data Prevista de Averbação");
        crmForm.DesabilitarCampos(Campos_TabelaPrazos_CronogramaAtual, !perfilEngenhariaPrazos);
        crmForm.DesabilitarCampos(Campos_TabelaPrazos_PrevisaoTerminoObraAtualizado, !perfilEngenhariaPrazos);
        crmForm.DesabilitarCampos(Campos_Contrato_Cliente, !perfilEngenhariaPrazos);

        if (perfilEngenhariaPrazos) { //se for automático a data prevista de emissão atualizada fica desabilitada
            crmForm.all.new_atua_dt_emis_atualizada_hab.FireOnChange();
            crmForm.all.new_atua_dt_termino_obra.FireOnChange();
            crmForm.all.new_atua_dt_atualizada_habitese.FireOnChange();
        }

        if (perfilEngenhariaPrazosAtualizaPrevistaAverbacao) {
            if (crmForm.all.new_datarealemissobaixa.DataValue != null) {
                crmForm.all.new_atua_dt_averbacao_atualizada.Disabled = false;
            }
        }
    }

    var motivoAlteracaoInicial = "";
    var dataInicioCronogramaPrevistoInicial = "";
    var dataFimCronogramaPrevistoInicial = "";
    var duracaoObraCronogramaPrevistoInicial = "";
    var teveAlteracaoDeDatas = false;

    crmForm.TrataDatasPrevistasDoCronograma = function() {
        debugger;
        teveAlteracaoDeDatas = true;

        if (crmForm.HouveAlteracaoDasDataPrevistasDoCronograma(false)) {
            //Limpa o motivo atual
            crmForm.all.new_motivo_alteracao.DataValue = "";
            //Obriga informar um novo motivo da alteração
            crmForm.SetFieldReqLevel('new_motivo_alteracao', true);
        } else
            crmForm.SetFieldReqLevel('new_motivo_alteracao', false);
    }

    crmForm.GuardaValoresIniciaisDasDataPrevistasDoCronograma = function() {
        dataInicioCronogramaPrevistoInicial = crmForm.all.new_datainiciocronog.DataValue;
        dataFimCronogramaPrevistoInicial = crmForm.all.new_datafimcronog.DataValue;
        duracaoObraCronogramaPrevistoInicial = crmForm.all.new_duracaodaobra.DataValue;
        motivoAlteracaoInicial = crmForm.all.new_motivo_alteracao.DataValue;
    }

    crmForm.HouveAlteracaoDasDataPrevistasDoCronograma = function(registroSendoSalvo) {

        var retorno = false;

        if (dataInicioCronogramaPrevistoInicial != null && dataInicioCronogramaPrevistoInicial.toString() == crmForm.all.new_datainiciocronog.DataValue &&
			dataFimCronogramaPrevistoInicial != null && dataFimCronogramaPrevistoInicial.toString() == crmForm.all.new_datafimcronog.DataValue &&
			duracaoObraCronogramaPrevistoInicial != null && duracaoObraCronogramaPrevistoInicial.toString() == crmForm.all.new_duracaodaobra.DataValue &&
			teveAlteracaoDeDatas) {

            if (registroSendoSalvo) {
                retorno = confirm("Não houve nenhum alteração de data, más deseja manter o Motivo Alteração atual? ");
                event.returnValue = retorno;
            } else {
                crmForm.all.new_motivo_alteracao.selectedIndex = motivoAlteracaoInicial;
                retorno = false;
            }
        } else
            retorno = true;

        return retorno;
    }

    //Valida data aceite, datafimcronog real e data de liberação de entrega de chaves.
    //data1 = data fim
    //data2 = dataaceite
    //data3 = liberação de entrega de chaves

    crmForm.CompararDataAt = function(data1, data2, data3, mensagem, mensagem2) {
        if (data1 == null) {
            data2.DataValue = null;
            data3.DataValue = null;
            data1.SetFocus();
            event.returnValue = false;
            return;
        }
        if ((data2.DataValue != null) && (data2.DataValue < data1.DataValue)) {
            alert(mensagem);
            data2.DataValue = null;
            data3.DataValue = null;
            data2.SetFocus();
            event.returnValue = false;
            return;
        }
        if ((data3.DataValue != null) && (data3.DataValue < data2.DataValue)) {
            alert(mensagem2);
            data3.DataValue = null;
            data3.SetFocus();
            event.returnValue = false;
        }

        if (((data1.DataValue == null) && (data2.DataValue != null)) || ((data2.DataValue == null) && (data3.DataValue != null))) {
            alert(mensagem);
            data2.DataValue = null;
            data3.DataValue = null;
            data2.SetFocus();
            event.returnValue = false;
        }
    }
    
    function Load() {

        var oParam = "objectTypeCode=10257&filterDefault=false&attributesearch=new_name";
        var typeCode = "10257";
        if (ORG_UNIQUE_NAME == "QAS")
            typeCode = "10264";
        else if (ORG_UNIQUE_NAME == "MRV")
            typeCode = "10238";
        oParam = "objectTypeCode=10238&filterDefault=false&attributesearch=new_name";

        crmForm.FilterLookup(crmForm.all.new_curvadevendasid, typeCode, "/MRVWeb/FilterLookup/FilterLookup.aspx", oParam);
        dataAtualEntregaDeChaves = crmForm.all.new_data_prevista_entrega.DataValue;

        if (dataInicioCronogramaPrevistoInicial == "" && dataFimCronogramaPrevistoInicial == "" && duracaoObraCronogramaPrevistoInicial == "")
            crmForm.GuardaValoresIniciaisDasDataPrevistasDoCronograma(); //Guarda valores das datas previstas do cronograma

        if (!crmForm.UsuarioPertenceEquipe("Núcleo - Notificação;Núcleo - NIAC;Núcleo - Encante;Núcleo - Controle Estratégico")) {
            crmForm.all.new_data_da_visita.Disabled = true;
            crmForm.all.new_visitamarcada.Disabled = true;
            crmForm.all.new_visita_realizada.Disabled = true;
        }
        crmForm.ConfiguraPerfilEngenhariaPrazos();
        crmForm.ConfiguraPerfilAtPrazos();

        if (crmForm.UsuarioPertenceEquipe(Equipe.ENGENHARIA_PREVISAO_ENTREGA)) {
            crmForm.all.new_data_prevista_entrega.Disabled = false;
        }
        else {
            crmForm.all.new_data_prevista_entrega.Disabled = true;
        }
        if (!crmForm.UsuarioPertenceEquipe("ENG_ATUALIZA_ALVENARIA")) {
            crmForm.all.new_datainiciocronogreal.Disabled = true;
            crmForm.all.new_data_real_de_alvenaria.Disabled = true;
        }
        crmForm.ConfiguraPerfilEngenhariaPrazos();
        crmForm.ConfiguraPerfilAtPrazos();
        
        if (crmForm.UsuarioPertenceEquipe(Equipe.ENGENHARIA_PREVISAO_ENTREGA)) {
			crmForm.all.new_data_prevista_entrega.Disabled = false;
		} else {
			crmForm.all.new_data_prevista_entrega.Disabled = true;
		}
    }

    function loadScript() {
        var oHead = document.getElementsByTagName('head')[0];
        var oScript = document.createElement('script');
        oScript.type = "text/javascript";
        oScript.src = "/_static/Base.js";

        oScript.onreadystatechange = function() {
            if (this.readyState == 'complete' || this.readyState == 'loaded') {
                Load();
            }
        }
        oHead.appendChild(oScript);
    }

    loadScript();

    if (crmForm.UsuarioPertenceEquipe("ENG_Kit_Acabamento")) {
        crmForm.all.new_data_limite_aquisicao_kit_acabamento.Disabled = false;
    } else {
        crmForm.all.new_data_limite_aquisicao_kit_acabamento.Disabled = true;
    }

    crmForm.AtualizarDataPrevistaEntregaChaves = function() {
        try {

            var dataInserida;

            if (dataAtualEntregaDeChaves == null) {
                dataAtualEntregaDeChaves = "";
            }

            if (crmForm.all.new_data_prevista_entrega.DataValue == null) {
                dataInserida = "";
            } else {
                dataInserida = crmForm.all.new_data_prevista_entrega.DataValue;
            }

            if (dataInserida.toString() != dataAtualEntregaDeChaves.toString()) {
                var nomeCampo = crmForm.all.new_data_prevista_entrega.id;
                var valorCampo = crmForm.all.new_data_prevista_entrega.DataValue != null ? crmForm.all.new_data_prevista_entrega.DataValue.toLocaleDateString() : "";
                var valorCampoAnterior = dataAtualEntregaDeChaves != "" ? dataAtualEntregaDeChaves.toLocaleDateString() : dataAtualEntregaDeChaves;
                var idRegistro = crmForm.ObjectId;
                var nomeTecnicoEntidade = crmForm.ObjectTypeName;

                var oParam = "nomeCampo=" + nomeCampo + "&nomeTecnicoEntidade=" + nomeTecnicoEntidade + "&tipoLog=" + TipoLogCampos.DATA_PREVISTA_ENTREGA_CHAVES;

                var url = '/MRVWeb/LogAlteracao/MotivoAlteracaoCampo.aspx?' + oParam;

                var retorno = openStdDlg(url, null, 750, 440);

                if (retorno != null && retorno != undefined) {
                    var cmd = new RemoteCommand("MrvService", "AtualizarDataPrevistaEntregaChavesUnico", "/MRVCustomizations/");
                    cmd.SetParameter("idModuloBloco", idRegistro);
                    cmd.SetParameter("tipo", TipoEntidade.MODULO_VILLAGE);
                    cmd.SetParameter("dataEntregaAtualizada", valorCampo);
                    cmd.SetParameter("justificativaAlteracao", retorno.Justivicativa);
                    cmd.SetParameter("dataEntregaAnterior", valorCampoAnterior);

                    var resultado = cmd.Execute();
                    if (crmForm.TratarRetornoRemoteCommand(resultado, "AtualizarDataPrevistaEntregaChavesUnico")) {
                        if (resultado.ReturnValue.Mrv.dataAtualizada == true) {
                            if (typeof (resultado.ReturnValue.Mrv.mensagem) != "object") {
                                alert(resultado.ReturnValue.Mrv.mensagem);
                            }
                            if (typeof (resultado.ReturnValue.Mrv.mensagemAviso) != "object") {
                                alert(resultado.ReturnValue.Mrv.mensagemAviso);
                            }
                            return true;
                        } else {
                            if (dataAtualEntregaDeChaves == "") {
                                dataAtualEntregaDeChaves = null;
                            }
                            crmForm.all.new_data_prevista_entrega.DataValue = dataAtualEntregaDeChaves;
                            crmForm.all.new_data_prevista_entrega.SetFocus();
                            alert(resultado.ReturnValue.Mrv.mensagem);
                        }
                    } else {
                        if (dataAtualEntregaDeChaves == "") {
                            dataAtualEntregaDeChaves = null;
                        }
                        crmForm.all.new_data_prevista_entrega.DataValue = dataAtualEntregaDeChaves;
                        crmForm.all.new_data_prevista_entrega.SetFocus();
                        alert("Operação cancelada.");
                        event.returnValue = false;
                        return false;
                    }
                } else {
                    alert("Operação cancelada.");
                    event.returnValue = false;
                    return false;
                }
            } else {
                return true;
            }
        } catch (error) {
            alert("Ocorreu um erro no formulário.\n" + error.description);
            event.returnValue = false;
            return false;
        }
    }

} catch (error) {
    formularioValido = false;
    alert("Ocorreu um erro no formulário.\n" + error.description);
}
