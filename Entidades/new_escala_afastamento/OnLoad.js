﻿formularioValido = true;
try {

    var motivo = crmForm.all.new_motivo.DataValue;
    if (motivo == 1 || motivo == 2) {
        crmForm.all.new_motivo.Disabled = true;
        crmForm.all.new_corretorid.Disabled = true;
        crmForm.all.new_descricao.Disabled = true;
        crmForm.all.new_fim.Disabled = true;
        crmForm.all.new_inicio.Disabled = true;
        crmForm.all.ownerid.Disabled = true;
    }

} catch (error) {
    formularioValido = false;
    alert("Ocorreu um erro no formulário.\n" + error.description);
}