﻿try {
 if (!formularioValido) {
        alert("Ocorreu um erro no formulário. Impossível salvar.");
        event.returnValue = false;
        return false;
    }
    var motivo = crmForm.all.new_motivo.DataValue;
    if(motivo == 1 || motivo == 2)
    {
        alert("Motivo férias e afastamento é de uso exclusivo para importação HCM.");
        event.returnValue = false;
        return false;
    }
    if(crmForm.all.new_fim_local.DataValue <= crmForm.all.new_inicio_local.DataValue)
    {
        alert("Data/Hora fim deve ser maior que Data/Hora de início.");
        event.returnValue = false;
        return false;
    }
        
    
} catch (error) {
    formularioValido = false;
    alert("Ocorreu um erro no formulário.\n" + error.description);
}