﻿if (crmForm.all.new_produtoid.DataValue == null) {
    crmForm.all.new_contratoid.DataValue = null;
    crmForm.all.new_contract_number.DataValue = null;
    crmForm.all.new_codigo_contrato_sap.DataValue = null;
    crmForm.all.new_codigo_contrato_sap6.DataValue = null;
}
else {
    var cmd = new RemoteCommand("MrvService", "ValidarLocalizacaoContratoParaIncluir", "/MRVCustomizations/");
    cmd.SetParameter("produtoId", crmForm.all.new_produtoid.DataValue[0].id);
    var oResult = cmd.Execute();
    if (oResult.Success) {
        if (oResult.ReturnValue.Mrv.Erro) {
            alert(oResult.ReturnValue.Mrv.Result);
            crmForm.all.new_produtoid.DataValue = null
            crmForm.all.new_contratoid.DataValue = null;
            crmForm.all.new_contract_number.DataValue = null;
            crmForm.all.new_codigo_contrato_sap.DataValue = null;
            crmForm.all.new_codigo_contrato_sap6.DataValue = null;
        }
        else {
            AddValueLookup(crmForm.all.new_contratoid, oResult.ReturnValue.Mrv.ContratoCRMId, 1010, oResult.ReturnValue.Mrv.ContratoCRMNome);
            if (oResult.ReturnValue.Mrv.ContractNumber != "null")
                crmForm.all.new_contract_number.DataValue = oResult.ReturnValue.Mrv.ContractNumber.toString();
            if (oResult.ReturnValue.Mrv.ContratoSAP != "null")
                crmForm.all.new_codigo_contrato_sap.DataValue = oResult.ReturnValue.Mrv.ContratoSAP.toString();
            if (oResult.ReturnValue.Mrv.ContratoSAP6 != "null")
                crmForm.all.new_codigo_contrato_sap6.DataValue = oResult.ReturnValue.Mrv.ContratoSAP6.toString();                
        }
    }
    else
        alert("Erro na consulta ao WebService.\nContate o administrador do sistema.");    
}

function AddValueLookup (Attribute, Id, TypeCode, Name) {
    var lookupData = new Array();
    lookupItem = new Object();
    lookupItem.id = Id;
    lookupItem.type = TypeCode;
    lookupItem.name = Name;
    lookupData[0] = lookupItem;
    Attribute.DataValue = lookupData;
}