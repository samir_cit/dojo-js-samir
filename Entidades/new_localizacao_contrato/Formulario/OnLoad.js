﻿formularioValido = true;

try {

    crmForm.all.new_clienteid.Disabled = true;
    crmForm.all.new_contratoid.Disabled = true;
    crmForm.all.new_contract_number.Disabled = true;
    crmForm.all.new_codigo_contrato_sap.Disabled = true;
    crmForm.all.new_codigo_contrato_sap6.Disabled = true;

function FilterLookup(attribute, url, param) {
    if (param != null)
        url += '?' + param;

    oImg = eval('attribute' + '.parentNode.childNodes[0]');

    oImg.onclick = function() {
        retorno = openStdDlg(url, null, 700, 450);

        if (typeof (retorno) != "undefined") {
            strValues = retorno.split('*|*');
            var lookupData = new Array();
            lookupItem = new Object();
            lookupItem.id = "{" + strValues[1] + "}";
            lookupItem.type = 1024;//Object Type Code product (padrão)
            lookupItem.name = strValues[0];
            lookupData[0] = lookupItem;
            attribute.DataValue = lookupData;
            attribute.FireOnChange();
        }
    };
}


var customer = crmForm.all.new_clienteid;
var oParam = "objectTypeCode=1028&filterDefault=false&attributesearch=name&_customerid=" + (customer.DataValue != null ? customer.DataValue[0].id : "");
FilterLookup(crmForm.all.new_produtoid, "/MRVWeb/FilterLookup/FilterLookup.aspx", oParam);

} catch (error) {
    formularioValido = false;
	alert("Ocorreu um erro no formulário.\n" + error.description);
}