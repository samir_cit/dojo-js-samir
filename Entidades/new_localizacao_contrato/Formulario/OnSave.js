﻿try {
    if (!formularioValido) {
        alert("Ocorreu um erro no formulário. Impossível salvar.");
        event.returnValue = false;
        return false;
    }
    
    crmForm.all.new_clienteid.Disabled = false;
    crmForm.all.new_contratoid.Disabled = false;
    crmForm.all.new_contract_number.Disabled = false;
    crmForm.all.new_codigo_contrato_sap.Disabled = false;
    crmForm.all.new_codigo_contrato_sap6.Disabled = false;
    
} catch (error) {
    alert("Ocorreu um erro no formulário.\n" + error.description);
    event.returnValue = false;
    return false;
}