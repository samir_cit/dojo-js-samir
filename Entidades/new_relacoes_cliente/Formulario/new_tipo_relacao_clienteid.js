﻿crmForm.all.new_codigo_sap.DataValue = null;

if (crmForm.all.new_tipo_relacao_clienteid.DataValue) {
    var cmd = new RemoteCommand("MrvService", "GetEntity", "/MRVCustomizations/");
    cmd.SetParameter("entity", "new_tipo_relacao_cliente");
    cmd.SetParameter("primaryField", "new_tipo_relacao_clienteid");
    cmd.SetParameter("entityId", crmForm.all.new_tipo_relacao_clienteid.DataValue[0].id);
    cmd.SetParameter("fieldList", "new_codigo_sap");

    var oResult = cmd.Execute();
    if (oResult.Success) {
        if (oResult.ReturnValue.Mrv.new_codigo_sap != null || oResult.ReturnValue.Mrv.new_codigo_sap != "") {
            crmForm.all.new_codigo_sap.DataValue = oResult.ReturnValue.Mrv.new_codigo_sap;
            crmForm.PreencheCampoNome();
        }
        else {
            alert("Tipo de relação está sem o código SAP");
            formularioValido = false;
        }
    }
}
crmForm.LimpaCampos("new_cliente_relacionadoid");
crmForm.HabilitaCampoClienteRelacionado();