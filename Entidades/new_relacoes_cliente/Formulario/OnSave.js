﻿try {
    if (!formularioValido) {
        alert("Ocorreu um erro no formulário. Impossível salvar.");
        event.returnValue = false;
        return false;
    }
    
    //Habilitar campos
    crmForm.all.new_opportunityid.Disabled = false;
    crmForm.all.new_name.Disabled = false;
    crmForm.all.new_codigo_sap.Disabled = false;
    
} catch (error) {
    alert("Ocorreu um erro no formulário.\n" + error.description);
    event.returnValue = false;
    return false;
}