formularioValido = true;
try {
    function Load() {
        var urlFilter = "/MRVWeb/FilterLookup/FilterLookup.aspx";
        var caminho = "objectTypeCode={0}&filterDefault=false&attributesearch=name";
        //fun��o para preencher o campo nome
        crmForm.PreencheCampoNome = function() {
            if (crmForm.all.new_cliente_relacionadoid.DataValue != null && crmForm.all.new_tipo_relacao_clienteid.DataValue != null) {
                crmForm.all.new_name.DataValue = crmForm.all.new_cliente_relacionadoid.DataValue[0].name + " - " + crmForm.all.new_tipo_relacao_clienteid.DataValue[0].name;
            }
        }

        crmForm.ObterNumeroPac = function() {
            if (crmForm.all.new_opportunityid.DataValue) {
                var cmd = new RemoteCommand("MrvService", "ObterNumeroPac", "/MRVCustomizations/");
                cmd.SetParameter("oportunidadeId", crmForm.all.new_opportunityid.DataValue[0].id);

                var retorno = cmd.Execute();

                if (crmForm.TratarRetornoRemoteCommand(retorno, "ObterNumeroPac")) {
                    return retorno.ReturnValue.Mrv.NumeroPAC;
                }
            }
            return undefined;
        }
		
		crmForm.OportunidadeParticipaVendaGarantida = function()
		{
			if (crmForm.all.new_opportunityid.DataValue) {
		        var atributos = "new_venda_garantida";
                var cmd = new RemoteCommand("MrvService", "GetEntity", "/MRVCustomizations/");
                cmd.SetParameter("entity", "opportunity");
                cmd.SetParameter("primaryField", "opportunityid");
                cmd.SetParameter("entityId", crmForm.all.new_opportunityid.DataValue[0].id);
                cmd.SetParameter("fieldList", atributos);
                var oResult = cmd.Execute();

                if (crmForm.TratarRetornoRemoteCommand(oResult, "GetEntity")) {
                    if (oResult.ReturnValue.Mrv.new_venda_garantida != null) {
                        return oResult.ReturnValue.Mrv.new_venda_garantida;
                    }
                }
            }
            return false;
		}

		crmForm.HabilitaCampoClienteRelacionado = function() {
		    if (crmForm.FormType == TypeCreate || crmForm.FormType == TypeUpdate) {
		        if (crmForm.all.new_tipo_relacao_clienteid.DataValue) {

		            crmForm.DesabilitarCampos("new_cliente_relacionadoid", false);

		            if (crmForm.all.new_opportunityid.DataValue) {
		                var oportunidadeVG = crmForm.OportunidadeParticipaVendaGarantida();

		                if (crmForm.all.new_tipo_relacao_clienteid.DataValue[0].name.indexOf('Fiador') >= 0 &&
		                oportunidadeVG) {
		                    var oParam = caminho.replace("{0}", "3003");
		                    var numeroPac = crmForm.ObterNumeroPac();
		                    if (!numeroPac) {
		                        numeroPac = -1;
		                    }
		                    oParam += "&_new_pac_relacionada_fiador=" + numeroPac;
		                    crmForm.FilterLookup(crmForm.all.new_cliente_relacionadoid, "1", urlFilter, oParam);
		                    return;
		                }
		            }

		            var oParam = caminho.replace("{0}", "3004");
		            crmForm.FilterLookup(crmForm.all.new_cliente_relacionadoid, "1", urlFilter, oParam);
		        }
		        else {
		            crmForm.DesabilitarCampos("new_cliente_relacionadoid", true);
		            crmForm.FilterLookup(crmForm.all.new_cliente_relacionadoid, "1", urlFilter, oParam);
		            var oParam = caminho.replace("{0}", "3004");
		            crmForm.FilterLookup(crmForm.all.new_cliente_relacionadoid, "1", urlFilter, oParam);
		        }
		    }
		}

        crmForm.HabilitaCampoClienteRelacionado();
    }

    function loadScript() {
        var oHead = document.getElementsByTagName('head')[0];
        var oScript = document.createElement('script');
        oScript.type = "text/javascript";
        oScript.src = "/_static/Base.js";

        oScript.onreadystatechange = function() {
            if (this.readyState == 'complete' || this.readyState == 'loaded') {
                Load();
            }
        }
        oHead.appendChild(oScript);
    }

    loadScript();
} catch (error) {
    formularioValido = false;
    alert("Ocorreu um erro no formul�rio.\n" + error.description);
}