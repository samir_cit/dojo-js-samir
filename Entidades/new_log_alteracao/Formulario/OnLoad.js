﻿try {
    function Load() {
        if (crmForm.all.new_produtoid.DataValue != null) {
            crmForm.EscondeSecao("new_nome_entidade", true);
            crmForm.EscondeCampo("new_systemuserid", false);
            crmForm.EscondeCampo("new_solicitado_por", false);
            crmForm.EscondeCampo("new_aprovado_por", false);
            crmForm.EscondeCampo("createdby", true);
            crmForm.EscondeCampo("new_produtoid", false);
            crmForm.EscondeCampo("new_descricao", false);
            crmForm.EscondeCampo("new_help_desk", false);
        }
        else {
            crmForm.EscondeSecao("new_nome_entidade", false);
            crmForm.EscondeCampo("new_systemuserid", false);
            crmForm.EscondeCampo("new_solicitado_por", true);
            crmForm.EscondeCampo("new_aprovado_por", true);
            crmForm.EscondeCampo("createdby", true);
            crmForm.EscondeCampo("new_produtoid", true);
            crmForm.EscondeCampo("new_descricao", true);
            crmForm.EscondeCampo("new_help_desk", true);
        }
    }
    function loadScript() {
        var oHead = document.getElementsByTagName('head')[0];
        var oScript = document.createElement('script');
        oScript.type = "text/javascript";
        oScript.src = "/_static/Base.js";

        oScript.onreadystatechange = function() {
            if (this.readyState == 'complete' || this.readyState == 'loaded') {
                Load();
            }
        }
        oHead.appendChild(oScript);
    }

    loadScript();

}
catch (erro) {
    alert("Ocorreu um erro no formulário.\n" + erro.description);
}
