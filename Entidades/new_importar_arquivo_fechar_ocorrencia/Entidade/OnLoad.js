﻿formularioValido = true;
try {
    function Load() {
        //desabilitar formulário se status diferente de Aguardando.
        if (crmForm.all.statuscode.DataValue != 1)
            crmForm.DesabilitarFormulario(true);
    }

    function loadScript() {
        var oHead = document.getElementsByTagName('head')[0];
        var oScript = document.createElement('script');
        oScript.type = "text/javascript";
        oScript.src = "/_static/Base.js";

        oScript.onreadystatechange = function() {
            if (this.readyState == 'complete' || this.readyState == 'loaded') {
                Load();
            }
        }
        oHead.appendChild(oScript);
    }
    loadScript();
} catch (error) {
    formularioValido = false;
	alert("Ocorreu um erro no formulário.\n" + error.description);
}