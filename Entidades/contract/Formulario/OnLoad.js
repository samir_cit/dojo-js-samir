﻿formularioValido = true;
try {
    function loadScript() {
        var oScript = document.createElement('script');
        oScript.type = "text/javascript";
        oScript.src = "/_static/Base.js";

        var oHead = document.getElementsByTagName('head')[0];
        oHead.appendChild(oScript);

        oScript.onreadystatechange = function() {
            if (this.readyState == 'complete' || this.readyState == 'loaded')
                Load();
        }
    }

    loadScript();

    function Load() {
        TipoDeContrato =
        {
            KITACABAMENTO: "Venda de Kit de Acabamento",
            SERVICO: "Venda de serviço",
            PERMUTA: "Venda de Permuta"
        }

        StatusCode =
        {
            RASCUNHO: "1",
            PRE_ATIVO: "200002",
            ATIVO: "200003"
        }

        Mensagem =
        {
            AGUARDANDO_AUDITORIA_RI: "Aguardando auditoria do Número do RI para impressão do Contrato. Dúvidas, entrar em contato com nce@mrv.com.br"
        }

        crmForm.ValidarPreenchimentoDadosRI = function() {
            var rCmd = new RemoteCommand("MrvService", "ValidarPreenchimentoDadosRI", "/MrvCustomizations/");
            rCmd.SetParameter("oportunidadeId", crmForm.all.new_opportunityid.DataValue);
            var retorno = rCmd.Execute();
            if (crmForm.TratarRetornoRemoteCommand(retorno, "ValidarPreenchimentoDadosRI")) {
                if (!retorno.ReturnValue.Mrv.DadosPreenchidos) {
                    alert(Mensagem.AGUARDANDO_AUDITORIA_RI);
                    return false;
                }
            }
            else {
                return false;
            }
            return true;
        }

        crmForm.ValidaCpfCliente = function(cpf) {
            var erro = '';

            cpf = cpf.replace('.', '');
            cpf = cpf.replace('.', '');
            cpf = cpf.replace('.', '');
            cpf = cpf.replace('-', '');

            if (cpf.length < 11) {
                erro += "Sao necessarios 11 digitos para verificacao do CPF! \n\n";
            }
            var nonNumbers = /\D/;
            if (nonNumbers.test(cpf)) {
                erro += "A verificacao de CPF suporta apenas numeros! \n\n";
            }
            if (cpf == "00000000000" || cpf == "11111111111" || cpf == "22222222222" || cpf == "33333333333" || cpf == "44444444444" || cpf == "55555555555" || cpf == "66666666666" || cpf == "77777777777" || cpf == "88888888888" || cpf == "99999999999") {
                erro += "Numero de CPF invalido!"
            }

            var a = [];
            var b = new Number;
            var c = 11;

            for (i = 0; i < 11; i++) {
                a[i] = cpf.charAt(i);
                if (i < 9) b += (a[i] * --c);
            }

            if ((x = b % 11) < 2) {
                a[9] = 0
            }
            else {
                a[9] = 11 - x;
            }

            b = 0;
            c = 11;
            for (y = 0; y < 10; y++) {
                b += (a[y] * c--);
            }

            if ((x = b % 11) < 2) {
                a[10] = 0;
            }
            else {
                a[10] = 11 - x;
            }

            if ((cpf.charAt(9) != a[9]) || (cpf.charAt(10) != a[10])) {
                erro += "CPF inválido!";
            }

            if (erro.length > 0) {
                alert(erro);
                return false;
            }
            else {
                return true;
            }
        }

        crmForm.ValidaCpfDiferentes = function(cpf1, cpf2) {
            if (cpf1 != null && cpf2 != null &&
                cpf1 == cpf2) {
                alert("Os CPF's das testemunhas devem ser diferentes!");
                return false;
            }
            else {
                return true
            }
        }

        function configurarOnLoad() {
            crmForm.all.title.DataValue = 'CONTRATO PARTICULAR DE PROMESSA DE COMPRA E VENDA';
            crmForm.all.title.Disabled = true;

            /// Guia Detalhes
            document.getElementById("serviceaddress_d").parentNode.parentNode.style.display = "none";

            if (crmForm.all.new_totalprice.DataValue != null)
                new_totalprice_onchange0();

            if (crmForm.UsuarioPertenceEquipe("Administradores")) {
                crmForm.all.new_codigo_sap.Disabled = false;
            }
            else {
                crmForm.all.new_fimobra.Disabled = true;
                crmForm.all.new_tipodeplanodefinanciamentocontid.Disabled = true;
            }

            /// Não exibe o relatório de indicação premiada a quem não pertencer a equipe Núcleo Administração de Contratos
            if (!crmForm.UsuarioPertenceEquipe("Núcleo Administração de Contratos"))
                crmForm.RemoveBotao("IndicaoPremiada");

            if (crmForm.all.statuscode.DataValue == StatusCode.PRE_ATIVO ||
                crmForm.all.statuscode.DataValue == StatusCode.ATIVO) {
                crmForm.DesabilitarFormulario(true);
            }
        }
        configurarOnLoad();

        function ConfigurarContratoPorTipo() {
            var tipoContrato = crmForm.all.contracttemplateid.DataValue[0].name;

            if (tipoContrato == TipoDeContrato.SERVICO ||
                tipoContrato == TipoDeContrato.PERMUTA) {
                crmForm.SetFieldReqLevel("new_testemunha1", false);
                crmForm.SetFieldReqLevel("new_testemunha2", false);
                crmForm.SetFieldReqLevel("new_cpf1", false);
                crmForm.SetFieldReqLevel("new_cpf2", false);
            }

            if (tipoContrato == TipoDeContrato.SERVICO)
                crmForm.all.new_data_base.Disabled = true;

            crmForm.all.activeon.Disabled = tipoContrato != TipoDeContrato.KITACABAMENTO;
        }
        ConfigurarContratoPorTipo();
    }
}
catch (error) {
    formularioValido = false;
    alert("Ocorreu um erro no formulário.\n" + error.description);
}