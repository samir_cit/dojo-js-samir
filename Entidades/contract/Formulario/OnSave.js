﻿try {
    if (!formularioValido) {
        alert("Ocorreu um erro no formulário. Impossível salvar.");
        event.returnValue = false;
        return false;
    }

    if (!crmForm.ValidaCpfDiferentes(crmForm.all.new_cpf1.DataValue, crmForm.all.new_cpf2.DataValue)) {
        event.returnValue = false;
        return false;
    }

    if (crmForm.all.new_cpf1.DataValue != null) {
        crmForm.AddMascara(crmForm.all.new_cpf1.DataValue, "new_cpf1");
        var ret = crmForm.ValidaCpfCliente(crmForm.all.new_cpf1.DataValue);

        if (!ret) {
            crmForm.all.new_cpf1.SetFocus();
            event.returnValue = false;
            return false;
        }
    }
    if (crmForm.all.new_cpf2.DataValue != null) {

        crmForm.AddMascara(crmForm.all.new_cpf2.DataValue, "new_cpf2");
        var ret = crmForm.ValidaCpfCliente(crmForm.all.new_cpf2.DataValue);
        if (!ret) {
            crmForm.all.new_cpf2.SetFocus();
            event.returnValue = false;
            return false;
        }
    }
    crmForm.TransformaTodosCaracteresEmMaiusculo();

    //Habilitar campo para salvar
    crmForm.all.new_fimobra.Disabled = false;
}
catch (error) {
    alert("Ocorreu um erro no formulário.\n" + error.description);
    event.returnValue = false;
    return false;
}