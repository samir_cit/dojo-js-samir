﻿var valor = crmForm.all.new_totalprice.DataValue.toString();
valor = valor.replace(".", ",");

var cmd = new RemoteCommand("MrvService", "GetExtensionValue", "/MRVCustomizations/");
cmd.SetParameter("value", valor);
var result = cmd.Execute();

if (result.Success)
    crmForm.all.new_valortotalextensao.DataValue = result.ReturnValue.Mrv.texto;