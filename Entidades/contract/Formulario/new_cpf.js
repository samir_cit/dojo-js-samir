﻿    if (!crmForm.ValidaCpfDiferentes(crmForm.all.new_cpf1.DataValue, crmForm.all.new_cpf2.DataValue)) {
        event.returnValue = false;
        return false;
    }

    if (crmForm.all.new_cpf1.DataValue != null) {
        crmForm.AddMascara(crmForm.all.new_cpf1.DataValue, "new_cpf1");
        var ret = crmForm.ValidaCpfCliente(crmForm.all.new_cpf1.DataValue);

        if (!ret) {
            crmForm.all.new_cpf1.SetFocus();
            event.returnValue = false;
            return false;
        }
    }