﻿function LimparCampos() {
    crmForm.all.new_data_base.DataValue = null;
    crmForm.all.new_data_registro_contrato_pj.DataValue = null;
    crmForm.all.new_previsao_entrega_chaves_meses.DataValue = null;
}

function MostraMensagem(mensagem) 
{
    alert(mensagem);
    LimparCampos();
}

function RetornaData(data) {
    var tmp = data.split('-');
    var dataPrev = new Date(tmp[0], tmp[1] - 1, tmp[2], 3, 0, 0);
    return dataPrev;
}

function DataPrevista() 
{
    var cmd = new RemoteCommand("MrvService", "ObterDatasContrato", "/MRVCustomizations/");
    cmd.SetParameter("contratoId", crmForm.ObjectId);
    var oResult = cmd.Execute();
    
    if (oResult.Success) {
        if (oResult.ReturnValue.Mrv.Error)
            MostraMensagem(oResult.ReturnValue.Mrv.Error);
        else if (oResult.ReturnValue.Mrv.ErrorSoap)
            MostraMensagem(oResult.ReturnValue.Mrv.ErrorSoap);
        else {
            crmForm.all.new_fimobra.DataValue = RetornaData(oResult.ReturnValue.Mrv.DataPrevisaoEntrega.toString());
            crmForm.all.new_data_registro_contrato_pj.DataValue = RetornaData(oResult.ReturnValue.Mrv.DataRegistro.toString());
            crmForm.all.new_previsao_entrega_chaves_meses.DataValue = oResult.ReturnValue.Mrv.PrevisaoChaves;
            crmForm.all.new_data_registro_contrato_pj.ForceSubmit = true;
            crmForm.all.new_previsao_entrega_chaves_meses.ForceSubmit = true;
        }
    }
}

function ValidaData(campo) 
{
    if (eval(campo).DataValue != null) {
        var Data = eval(campo).DataValue;
        var oCmd = new RemoteCommand("MrvService", "ValidaDataContrato", "/MrvCustomizations/");
        oCmd.SetParameter("ContratoId", crmForm.ObjectId);
        oCmd.SetParameter("DataBase", Data.getDate() + "/" + (Data.getMonth() + 1) + "/" + Data.getYear());
        var oResult = oCmd.Execute();

        if (oResult.Success) {
            if (oResult.ReturnValue.Mrv.Mensagem == true)
                DataPrevista();
            else {
                alert(oResult.ReturnValue.Mrv.Mensagem);
                crmForm.all.new_data_base.DataValue = null;
            }
        }
        else {
            alert("Erro na consulta ao WebService. Contate o administrador.");
            crmForm.all.new_data_base.DataValue = null;
        }
    }
    else
        LimparCampos();
}

var tipoContrato = crmForm.all.contracttemplateid.DataValue[0].name;

/// Alterado para fazer não validacao na venda de kit
if (tipoContrato != TipoDeContrato.KITACABAMENTO &&
    tipoContrato != TipoDeContrato.SERVICO &&
    tipoContrato != TipoDeContrato.PERMUTA) 
{
    if (crmForm.UsuarioPertenceEquipe("Administradores"))
        DataPrevista();
    else
        ValidaData(crmForm.all.new_data_base);

}
else if (crmForm.all.new_data_base.DataValue != null && tipoContrato != TipoDeContrato.PERMUTA)
    crmForm.all.new_fimobra.DataValue = crmForm.all.new_data_base.DataValue;