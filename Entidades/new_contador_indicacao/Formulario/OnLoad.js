﻿formularioValido = true;

try {

    function Load() {
        if (crmForm.UsuarioPertenceEquipe(Equipe.MARKETING)) {
            crmForm.all.new_qtd_disponivel_exclusivo.Disabled = false;
            crmForm.all.new_qtd_disponivel_generico.Disabled = false;
        }
        else {
            crmForm.all.new_qtd_disponivel_exclusivo.Disabled = true;
            crmForm.all.new_qtd_disponivel_generico.Disabled = true;

        }
    }
    function loadScript() {
        var oScript = document.createElement('script');
        oScript.type = "text/javascript";
        oScript.src = "/_static/Base.js";

        var oHead = document.getElementsByTagName('head')[0];
        oHead.appendChild(oScript);

        oScript.onreadystatechange = function() {
            if (this.readyState == 'complete' || this.readyState == 'loaded')
                Load();
        }
    }
    loadScript();

}
catch (error) {
    formularioValido = false;
    alert("Ocorreu um erro no formulário.\n" + error.description);
}