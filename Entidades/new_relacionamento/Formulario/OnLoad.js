﻿/*
* Obrigando o preenchimento dos campos Cliente e Função do Cliente.
*
* Autor: Ricardo Cunha
* Data: 27/01/09
*/
crmForm.SetFieldReqLevel('new_accountid', true);
crmForm.SetFieldReqLevel('new_funcao_cliente', true);
crmForm.SetFieldReqLevel('new_oportunidade_renegociacaoid', true);