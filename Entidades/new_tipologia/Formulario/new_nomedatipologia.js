﻿//Atribuindo ao campo nome o texto selecionado no campo tipologia------------------------------------
if (crmForm.all.new_nomedatipologia.DataValue != null)
    crmForm.all.new_name.DataValue = crmForm.all.new_nomedatipologia.SelectedText;
else
    crmForm.all.new_name.DataValue = '';

// tratamento da tipologia---------------------------------------------------------------------------------------//

if (crmForm.all.new_nomedatipologia.DataValue == 4 || crmForm.all.new_nomedatipologia.DataValue == 17 || crmForm.all.new_nomedatipologia.DataValue == 20) {
    document.getElementById("new_nome_tipologia_d").parentNode.parentNode.style.display = "inline";
    crmForm.SetFieldReqLevel("new_nome_tipologia", true);
}
else {
    document.getElementById("new_nome_tipologia_d").parentNode.parentNode.style.display = "none";
    crmForm.SetFieldReqLevel("new_nome_tipologia", false);
    crmForm.all.new_nome_tipologia.DataValue = crmForm.all.new_nomedatipologia.DataValue;
}