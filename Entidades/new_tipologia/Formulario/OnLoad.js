﻿var tamanhoTipologia = null;

//Desabilita campos Nome quando o nome contém Histórico
if (crmForm.all.new_empreendimentobloco.DataValue != null && crmForm.all.new_nomedatipologia.DataValue != null) {
    if (crmForm.all.new_empreendimentobloco.DataValue.indexOf("Histórico de Carga") >= 0) {
        crmForm.all.new_empreendimentobloco.Disabled = true;
        crmForm.all.new_nomedatipologia.Disabled = true;
    }
}

//Adiciona Script para ser usado no click do botão
var elm = document.createElement("script");
elm.src = "/_static/_grid/cmds/util.js";
document.appendChild(elm);

//Adiciona funções auxiliares
var el = document.createElement("script");
el.src = "/MRVWeb/_scriptForm/formscript.js";
document.appendChild(el);

if (crmForm.FormType == 2 && crmForm.ObjectId == '{3CD3327B-AC55-DD11-BC22-001B78B9D994}') {
    crmForm.all.new_tipologiablocoid.Disabled = true;
}

//ocultando campos----------------------------------------------------------
crmForm.all.new_name_c.style.visibility = "hidden";
crmForm.all.new_name_d.style.visibility = "hidden";
crmForm.all.new_unidades_geradas_c.style.visibility = "hidden";
crmForm.all.new_unidades_geradas_d.style.visibility = "hidden";

//crmForm.all.new_empreendimentobloco.readOnly = true;

//desabilitando campo total de unidades---------------------------------------------
crmForm.all.new_totaldeunidades.readOnly = true;

//ocultando guias--------------------------------------------
crmForm.all.tab1Tab.style.display = "none";
crmForm.all.tab2Tab.style.display = "none";

crmForm.all.new_nomedatipologia.FireOnChange();
