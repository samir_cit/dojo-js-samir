﻿formularioValido = true;
try {

    function RemoveBotao(nome) {
        var lista = document.getElementsByTagName("LI");
        for (i = 0; i < lista.length; i++) {
            if (lista[i].id.indexOf(nome) >= 0) {
                var o = lista[i].parentElement;
                o.removeChild(lista[i]);
                break;
            }
        }
    }

    statuscode =
    {
        RESOLVIDO: 3,
        QUEDA: 7    
    }
    

    crmForm.EscondeCampo = function(nomeCampo, esconde) {
        var tipo = "";
        if (esconde)
            tipo = "none";

        var atributo_c = eval("crmForm.all." + nomeCampo + "_c");
        var atributo_d = eval("crmForm.all." + nomeCampo + "_d");
        atributo_c.style.display = tipo;
        atributo_d.style.display = tipo;
    }

    if (crmForm.FormType == 1) {
        crmForm.all.createdon.DataValue = new Date();
    }
    else {
        if (crmForm.all.statuscode.DataValue == statuscode.RESOLVIDO || crmForm.all.statuscode.DataValue == statuscode.QUEDA)
            RemoveBotao("FecharOcorrncia");
        else
            RemoveBotao("Ativar");
    }

    crmForm.EscondeCampo("new_name", true);
    crmForm.EscondeCampo("new_pendencia_tipopendenciaid", true);

    if (crmForm.FormType == 2 && crmForm.all.new_pendencia_tipopendenciaid.DataValue != null) {
        crmForm.EscondeCampo("new_pendencia_tipopendenciaid", false);
        crmForm.EscondeCampo("new_tipo_pendencia", false);
        crmForm.EscondeCampo("new_subjectid", true);
    }
    else {
        crmForm.EscondeCampo("new_pendencia_tipopendenciaid", true);
        crmForm.EscondeCampo("new_tipo_pendencia", true);
        crmForm.EscondeCampo("new_subjectid", false);
    }

    crmForm.CalcularDataFinal = function(assuntoId) {
        var cmd = new RemoteCommand("MrvService", "ObterDiasAssuntoPendencia", "/MRVCustomizations/");
        cmd.SetParameter("assuntoId", crmForm.all.new_subjectid.DataValue[0].id);
        var result = cmd.Execute();

        if (result.ReturnValue.Mrv.Sucesso) {

            crmForm.all.new_dias.DataValue = result.ReturnValue.Mrv.Dias;
            var primeiradata = crmForm.all.createdon.DataValue;
            primeiradata.setDate(primeiradata.getDate() + crmForm.all.new_dias.DataValue);
            crmForm.all.new_data_conclusao.DataValue = primeiradata;
        }
        else {
            crmForm.all.new_subjectid.DataValue = null;
            crmForm.all.new_dias.DataValue = null;
            alert(result.ReturnValue.Mrv.Mensagem);
        }
    }

    if (crmForm.all.new_subjectid.DataValue != null)
        crmForm.CalcularDataFinal(crmForm.all.new_subjectid.DataValue[0].id);

    //Desabiltar todos os campos
    if (crmForm.all.statuscode.DataValue != null && (crmForm.all.statuscode.DataValue == statuscode.RESOLVIDO|| crmForm.all.statuscode.DataValue == statuscode.QUEDA)) {
        var todosAtributos = document.getElementsByTagName("label");
        for (var i = 0; i < todosAtributos.length; i++) {
            if (todosAtributos[i].htmlFor != "") {
                var object = eval("crmForm.all." + todosAtributos[i].htmlFor.replace("_ledit", ""))
                object.Disabled = true;
            }
        }
    }

} catch (error) {
    formularioValido = false;
    alert("Ocorreu um erro no formulário.\n" + error.description);
}
