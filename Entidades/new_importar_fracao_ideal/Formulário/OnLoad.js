﻿formularioValido = true;
try {

    function loadScript() {
        var oScript = document.createElement('script');
        oScript.type = "text/javascript";
        oScript.src = "/_static/Base.js";

        var oHead = document.getElementsByTagName('head')[0];
        oHead.appendChild(oScript);

        oScript.onreadystatechange = function() {
            if (this.readyState == 'complete' || this.readyState == 'loaded')
                Load();
        }
    }

    crmForm.Recarregar = function() {
        crmForm.SubmitCrmForm(1, true, true, false);
    }
    
    //Recuperar valores dentro do Iframe
    function RetornaValorIFrame(nomeIFrame, nomeAtributo) {
        if (window.frames[nomeIFrame] != null && window.frames[nomeIFrame].document.getElementById(nomeAtributo) != null)
            return window.frames[nomeIFrame].document.getElementById(nomeAtributo).value;
        else
            return false;
    }

    loadScript();
    
    function Load() {

        //Adiciona Script para ser usado no click do botão
        var elm = document.createElement("script");
        elm.src = "/_static/_grid/cmds/util.js";
        document.appendChild(elm);

        //Separador
        separador = "";
        
        //verficar se o arquivo está carregado
        var carregado = RetornaValorIFrame("IFRAME_enviar_arquivo", "hCarregado");
        var status = crmForm.all.statuscode.DataValue;
        
        //função do botão importar
        crmForm.Importar = function() {
            var identificador = crmForm.ObjectId;            
            if (!carregado && status != 6) {
                alert("Carregue o arquivo antes de importar");
                return;
            }
            //importar
            var cmd = new RemoteCommand("MrvService", "ImportarFracaoIdeal", "/MrvCustomizations/");
            cmd.SetParameter("identificador", identificador);
            cmd.SetParameter("separador", separador);
            var result = cmd.Execute();
            if (crmForm.TratarRetornoRemoteCommand(result, "ImportarFracaoIdeal")) {
                alert(result.ReturnValue.Mrv.Mensagem);
                crmForm.RemoveBotao("Importar");
                crmForm.all.new_realizado.DataValue = true;
                crmForm.DesabilitaCampo("new_realizado", false);
                crmForm.Save();
            }
            else {
                formularioValido = false;
                return;
            }
        }

        crmForm.DesabilitaCampo = function(atributo, verdadeiroOuFalso) {
            var object = eval("crmForm.all." + atributo)
            object.Disabled = verdadeiroOuFalso;
        }
        if (crmForm.all.statuscode.DataValue != 6 && !carregado)
            crmForm.RemoveBotao("Importar");

        if (crmForm.all.statuscode.DataValue != 1 ) {
            var todosAtributos = document.getElementsByTagName("label");
            for (var i = 0; i < todosAtributos.length; i++) {
                if (todosAtributos[i].htmlFor != "")
                    crmForm.DesabilitaCampo(todosAtributos[i].htmlFor.replace("_ledit", ""), true);
            }
        }

        //Esconder aba de anotações.
        crmForm.all.tab1Tab.style.display = "none";

        crmForm.DesabilitaCampo("new_outros", true);

        var identificador = "";
        //Nome do registro
        if (crmForm.FormType == 1) {
            crmForm.all.new_name.DataValue = "Fração ideal - Data:";
            crmForm.all.new_name.ForceSubmit = true;
        } else
            identificador = crmForm.ObjectId;


        if (crmForm.all.new_separador.DataValue != null) {
            switch (crmForm.all.new_separador.DataValue) {
                case "1":
                    separador = ";";
                    break;
                case "2":
                    separador = "\\t";
                    break;
                case "3":
                    separador = crmForm.all.new_outros.DataValue;
                    break;
            }
        }
        document.getElementById("IFRAME_enviar_arquivo").src = "/mrvweb/Importar_Fracao/Default.aspx?identificador=" + identificador + "&separador=" + separador + "&realizado=" + crmForm.all.new_realizado.DataValue + "&empreendimentoid=" + crmForm.all.new_empreendimentoid.DataValue;
    }
} catch (error) {
    formularioValido = false;
    alert("Ocorreu um erro no formulário.\n" + error.description);
}