﻿var flag = true;
if (crmForm.all.new_separador.DataValue != null) {
    if (crmForm.all.new_separador.DataValue == 3) {
        //Setar campo outros para ficar obrigatorio.
        crmForm.DesabilitaCampo("new_outros", false);
        crmForm.SetFieldReqLevel("new_outros", 1);
        flag = false;
    }

    if (crmForm.all.new_separador.IsDirty)
        crmForm.RemoveBotao("Importar");
}

if (flag) {
    crmForm.DesabilitaCampo("new_outros", true);
    crmForm.SetFieldReqLevel("new_outros", 0);
    crmForm.all.new_outros.DataValue = null;
}
