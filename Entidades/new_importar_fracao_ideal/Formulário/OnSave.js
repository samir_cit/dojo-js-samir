﻿try {
    if (!formularioValido) {
        alert("Ocorreu um erro no formulário. Impossível salvar.");
        event.returnValue = false;
        return false;
    }
    
    //Apenas na criação altera o nome
    if (crmForm.FormType == 1) {
        var dataHoje = new Date();
        crmForm.all.new_name.DataValue += " "+ dataHoje.getDate() + "/" + (dataHoje.getMonth() + 1) + "/" + dataHoje.getFullYear();
    }
    crmForm.DesabilitaCampo("new_outros", false);

} catch (error) {
    alert("Ocorreu um erro no formulário.\n" + error.description);
    event.returnValue = false;
    return false;
}