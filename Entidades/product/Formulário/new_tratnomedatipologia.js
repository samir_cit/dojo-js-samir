﻿if (crmForm.all.new_tipologiaid.DataValue != null) 
{
    var nomeExtenso = crmForm.all.new_tipologiaid.DataValue[0].name;

    if ((nomeExtenso.ToUpper() != "VAGA") && (nomeExtenso.ToUpper() != "LOTE")) 
    {
        if (Number(nomeExtenso.substring(0, 1)) == 1)
            nomeExtenso = nomeExtenso.replace("Q", "Quarto");
        else
            nomeExtenso = nomeExtenso.replace("Q", "Quartos");
    }

    nomeExtenso = nomeExtenso.replace(" S", " com Suíte").replace("COB", ", Cobertura").replace("ESC","Escritório");
    crmForm.all.new_tratnomedatipologia.DataValue = nomeExtenso;
}