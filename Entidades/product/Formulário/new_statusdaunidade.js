﻿if (crmForm.all.new_statusdaunidade.DataValue == StatusDaUnidade.PERMUTADO) 
{
    document.getElementById("new_permuta_d").parentNode.parentNode.style.display = "inline";
    crmForm.all.new_permuta_c.style.visibility = 'visible';
    crmForm.all.new_permuta_d.style.visibility = 'visible';
}
else 
{
    crmForm.all.new_terrenoid.DataValue = null;
    crmForm.all.new_permuta.DataValue = null;
    crmForm.all.new_permuta_c.style.visibility = 'hidden';
    crmForm.all.new_permuta_d.style.visibility = 'hidden';
    document.getElementById("new_permuta_d").parentNode.parentNode.style.display = "none";
}

crmForm.all.new_permuta.FireOnChange();