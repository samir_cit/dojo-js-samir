﻿if (crmForm.all.new_permuta.DataValue == 2)
{
    crmForm.all.new_terrenoid_c.style.visibility = 'visible';
    crmForm.all.new_terrenoid_d.style.visibility = 'visible';
    crmForm.SetFieldReqLevel("new_terrenoid", true);  
}    
else
{
    crmForm.all.new_terrenoid_c.style.visibility = 'hidden';
    crmForm.all.new_terrenoid_d.style.visibility = 'hidden';
    crmForm.all.new_terrenoid.DataValue = null;
    crmForm.SetFieldReqLevel("new_terrenoid", false);
}