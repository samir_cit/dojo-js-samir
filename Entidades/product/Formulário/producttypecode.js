﻿if (crmForm.all.producttypecode.DataValue == TipoDeProduto.UNIDADE) 
{
    crmForm.all.tab1Tab.style.display = "inline";
    crmForm.all.tab2Tab.style.display = "inline";
    crmForm.all.new_descricaogaragem.Disabled = true;
    crmForm.all.new_descricaobox.Disabled = true;
    crmForm.all.new_tipodepadrao.DataValue = null;
    crmForm.all.new_tipodepadrao.setAttribute("req", 0);
    crmForm.all.new_tipodepadrao_c.className = "n";
    crmForm.all.new_tipodepadrao_c.style.visibility = "hidden";
    crmForm.all.new_tipodepadrao_d.style.visibility = "hidden";
    document.getElementById("new_original_d").parentNode.parentNode.style.display = "none";
    document.getElementById("new_descricaokit_d").parentNode.parentNode.style.display = "none";
    document.getElementById("new_descricaogaragem_d").parentNode.parentNode.style.display = "none";
}
else if (crmForm.all.producttypecode.SelectedText == TipoDeProduto.GARAGEM) 
{
    crmForm.all.tab1Tab.style.display = "none";
    crmForm.all.tab2Tab.style.display = "none";
    crmForm.all.new_descricaogaragem.Disabled = false;
    crmForm.all.new_descricaobox.Disabled = true;
    crmForm.all.new_tipodepadrao.DataValue = null;
    crmForm.all.new_tipodepadrao.setAttribute("req", 0);
    crmForm.all.new_tipodepadrao_c.className = "n";
    crmForm.all.new_tipodepadrao_c.style.visibility = "hidden";
    crmForm.all.new_tipodepadrao_d.style.visibility = "hidden";
    document.getElementById("new_original_d").parentNode.parentNode.style.display = "none";
    document.getElementById("new_descricaokit_d").parentNode.parentNode.style.display = "none";
    document.getElementById("new_descricaogaragem_d").parentNode.parentNode.style.display = "inline";
}
else if (crmForm.all.producttypecode.SelectedText == TipoDeProduto.BOX) 
{
    crmForm.all.tab1Tab.style.display = "none";
    crmForm.all.tab2Tab.style.display = "none";
    crmForm.all.new_descricaogaragem.Disabled = true;
    crmForm.all.new_descricaobox.Disabled = false;
    crmForm.all.new_tipodepadrao.DataValue = null;
    crmForm.all.new_tipodepadrao.setAttribute("req", 0);
    crmForm.all.new_tipodepadrao_c.className = "n";
    crmForm.all.new_tipodepadrao_c.style.visibility = "hidden";
    crmForm.all.new_tipodepadrao_d.style.visibility = "hidden";
    document.getElementById("new_original_d").parentNode.parentNode.style.display = "none";
    document.getElementById("new_descricaokit_d").parentNode.parentNode.style.display = "none";
    document.getElementById("new_descricaogaragem_d").parentNode.parentNode.style.display = "inline";
}
else if (crmForm.all.producttypecode.SelectedText == TipoDeProduto.KITACABEMNTO) 
{
    crmForm.all.tab1Tab.style.display = "none";
    crmForm.all.tab2Tab.style.display = "none";
    crmForm.all.new_descricaogaragem.Disabled = true;
    crmForm.all.new_descricaobox.Disabled = true;
    crmForm.all.new_tipodepadrao.setAttribute("req", 2);
    crmForm.all.new_tipodepadrao_c.className = "req";
    crmForm.all.new_tipodepadrao_c.style.visibility = "visible";
    crmForm.all.new_tipodepadrao_d.style.visibility = "visible";
    document.getElementById("new_original_d").parentNode.parentNode.style.display = "inline";
    document.getElementById("new_descricaokit_d").parentNode.parentNode.style.display = "inline";
    document.getElementById("new_descricaogaragem_d").parentNode.parentNode.style.display = "none";
}
else 
{
    crmForm.all.tab1Tab.style.display = "none";
    crmForm.all.tab2Tab.style.display = "none";
    crmForm.all.new_descricaogaragem.Disabled = true;
    crmForm.all.new_descricaobox.Disabled = true;
    crmForm.all.new_tipodepadrao.DataValue = null;
    crmForm.all.new_tipodepadrao.setAttribute("req", 0);
    crmForm.all.new_tipodepadrao_c.className = "n";
    crmForm.all.new_tipodepadrao_c.style.visibility = "hidden";
    crmForm.all.new_tipodepadrao_d.style.visibility = "hidden";
    document.getElementById("new_original_d").parentNode.parentNode.style.display = "none";
    document.getElementById("new_descricaokit_d").parentNode.parentNode.style.display = "none";
    document.getElementById("new_descricaogaragem_d").parentNode.parentNode.style.display = "none";
}