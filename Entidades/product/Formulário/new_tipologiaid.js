﻿var quartos = " Quartos";
var suite = " com Suíte";
var escritorio = " + Escritório";
var cobertura = ", Cobertura";

if (crmForm.all.new_tipologiaid.DataValue != null) {
    var nome = crmForm.all.new_tipologiaid.DataValue[0].name;

    switch (nome) {
    case "1 Q":
        crmForm.all.new_tratnomedatipologia.DataValue = "1 Quarto";
        break;
    case "1 Q S":
        crmForm.all.new_tratnomedatipologia.DataValue = "1 Quarto com Suíte";
        break;
    case "1 Q + ESC":
        crmForm.all.new_tratnomedatipologia.DataValue = "1 Quarto + Escritório";
        break;
    case "1 Q S + ESC":
        crmForm.all.new_tratnomedatipologia.DataValue = "1 Quarto com Suíte + Escritório";
        break;
    case "2 Q":
        crmForm.all.new_tratnomedatipologia.DataValue = "2" + quartos;
        break;
    case "2 Q S":
        crmForm.all.new_tratnomedatipologia.DataValue = "2" + quartos + suite;
        break;
    case "2 Q COB":
        crmForm.all.new_tratnomedatipologia.DataValue = "2" + quartos + cobertura;
        break;
    case "2 Q S COB":
        crmForm.all.new_tratnomedatipologia.DataValue = "2" + quartos + suite + cobertura;
        break;
    case "2 Q + ESC":
        crmForm.all.new_tratnomedatipologia.DataValue = "2" + quartos + escritorio;
        break;
    case "2 Q S + ESC":
        crmForm.all.new_tratnomedatipologia.DataValue = "2" + quartos + suite + escritorio;
        break;
    case "3 Q":
        crmForm.all.new_tratnomedatipologia.DataValue = "3" + quartos;
        break;
    case "3 Q S":
        crmForm.all.new_tratnomedatipologia.DataValue = "3" + quartos + suite;
        break;
    case "3 Q COB":
        crmForm.all.new_tratnomedatipologia.DataValue = "3" + quartos + cobertura;
        break;
    case "3 Q S COB":
        crmForm.all.new_tratnomedatipologia.DataValue = "3" + quartos + suite + cobertura;
        break;
    case "4 Q":
        crmForm.all.new_tratnomedatipologia.DataValue = "4" + quartos;
        break;
    case "4 Q S":
        crmForm.all.new_tratnomedatipologia.DataValue = "4" + quartos + suite;
        break;
    case "4 Q COB":
        crmForm.all.new_tratnomedatipologia.DataValue = "4" + quartos + cobertura;
        break;
    case "4 Q S COB":
        crmForm.all.new_tratnomedatipologia.DataValue = "4" + quartos + suite + cobertura;
        break;
    case "5 Q":
        crmForm.all.new_tratnomedatipologia.DataValue = "5" + quartos;
        break;
    case "5 Q S":
        crmForm.all.new_tratnomedatipologia.DataValue = "5" + quartos + suite;
        break;
    case "5 Q COB":
        crmForm.all.new_tratnomedatipologia.DataValue = "5" + quartos + cobertura;
        break;
    case "5 Q S COB":
        crmForm.all.new_tratnomedatipologia.DataValue = "5" + quartos + suite + cobertura;
        break;
    }

    var cmd = new RemoteCommand("MrvService", "ValidarAlteracaoTipologiaProduto", "/MRVCustomizations/");
    cmd.SetParameter("produtoID", crmForm.ObjectId);
    cmd.SetParameter("tipologiaID", crmForm.all.new_tipologiaid.DataValue[0].id);
    var result = cmd.Execute();
        if (result.Success) {
            if (result.ReturnValue.Mrv.Error) {
               alert(result.ReturnValue.Mrv.Error);

                            var lookupData = new Array();
                            var lookupItem = new Object();
                            lookupItem.id = tipologiaAtual;
                            lookupItem.typename = 'new_tipologia';
                            lookupItem.name = nomeTipologia;
                            lookupData[0] = lookupItem;
                            crmForm.all.new_tipologiaid.DataValue = lookupData;

                //return false;
            } else {
                if (!result.ReturnValue.Mrv.Resultado) {
                    alert('Não é possível alterar a Tipologia para outra com Tamanho diferente.');
                    event.returnValue = false;
                    return false;
         }
        }
    }
}