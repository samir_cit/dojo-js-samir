﻿try 
{
    if (!formularioValido) 
    {
        alert("Ocorreu um erro no formulário. Impossível salvar.");
        event.returnValue = false;
        return false;
    }
    
    function ValidarTipoPermuta()
    {
        var owner = crmForm.all.new_tipologiaid.DataValue;
        var strOwnerId= owner[0].id.replace('{', '').replace('}','');

        var cmdTipologia = new RemoteCommand('MrvService', 'GetEntity', '/MRVCustomizations/');
        cmdTipologia.SetParameter('entity', 'new_tipologia');
        cmdTipologia.SetParameter('primaryField', 'new_tipologiaid');
        cmdTipologia.SetParameter('entityId', strOwnerId);
        cmdTipologia.SetParameter('fieldList', 'new_tipologiablocoid');

        var result = cmdTipologia.Execute();
        
        
        
        var idBloco = null; 
        
        if (result.Success)
        {
            idBloco = result.ReturnValue.Mrv.new_tipologiablocoid;
            
            var cmdBloco = new RemoteCommand('MrvService', 'GetEntity', '/MRVCustomizations/');
            cmdBloco.SetParameter('entity', 'new_blocos');
            cmdBloco.SetParameter('primaryField', 'new_blocosid');
            cmdBloco.SetParameter('entityId', idBloco);
            cmdBloco.SetParameter('fieldList', 'new_qtde_permuta_dentro;new_qtde_permuta_fora;new_qtde_permuta_dentro_gerada;new_qtde_permuta_fora_gerada');
            
            var resultBloco = cmdBloco.Execute();
            
            if (resultBloco.Success)
            {
                var qtdEsperadaDentro = resultBloco.ReturnValue.Mrv.new_qtde_permuta_dentro;
                var qtdEsperadaFora = resultBloco.ReturnValue.Mrv.new_qtde_permuta_fora;
                var qtdGeradaDentro = resultBloco.ReturnValue.Mrv.new_qtde_permuta_dentro_gerada;
                var qtdGeradaFora = resultBloco.ReturnValue.Mrv.new_qtde_permuta_fora_gerada;

                if(crmForm.all.new_permuta.DataValue == 2 && qtdGeradaFora >= qtdEsperadaFora )
                {
                    alert('Quantidade de Permuta Fora já foi atingida para o bloco. Permuta não pode ser deste tipo!');
                    event.returnValue = false;
                }
                else if (crmForm.all.new_permuta.DataValue == 1 && qtdGeradaDentro >= qtdEsperadaDentro)
                {
                    alert('Quantidade de Permuta Dentro já foi atingida para o bloco. Permuta não pode ser deste tipo!');
                    event.returnValue = false;
                }
            }
            else
                alert('Falha na atualização - validação de permuta: ' + result.ReturnValue.Mrv.ErrorSoap);
        }
        else
        {
            alert('Falha na atualização: ' + result.ReturnValue.Mrv.ErrorSoap);
            event.returnValue = false;
            return false;
        }
    }
    
    function VerificarSeCodigoSAPJaExiste(campoCodigoSap, valorCodigoSap)
    {
        var cmd = new RemoteCommand("MrvService", "ExistCodSAP", "/MRVCustomizations/");
         cmd.SetParameter("entity", "product");
         cmd.SetParameter("primaryField", "productid");
         cmd.SetParameter("fieldContainsCodSAP", campoCodigoSap);
         cmd.SetParameter("codSAP", valorCodigoSap);
         
         var entityId = crmForm.ObjectId;
         
         if (!entityId)
             entityId = "";

         cmd.SetParameter("entityId", entityId);

         var result = cmd.Execute();
         
         if (result.Success)
         {
             if (result.ReturnValue.Mrv.Found)
             {
                   alert("Código do material já está associado a outro produto.\nAltere o código do material e tente salvar novamente!");
                   event.returnValue = false;
                   return false;
              }
         }
    }
    
      var valorCampo = crmForm.all.new_flag_produto_elegivel.DataValue;
      
		if (crmForm.valorCampoElegibilidade != valorCampo) {
	var nomeCampo = crmForm.all.new_flag_produto_elegivel.id;
	var valorCampo = crmForm.all.new_flag_produto_elegivel.DataValue;
	var nomeRegistro = crmForm.all.name.DataValue;
	
	var idRegistro = crmForm.ObjectId;
	var nomeTecnicoEntidade = crmForm.ObjectTypeName;

	var oParam = "nomeCampo=" + nomeCampo + "&nomeTecnicoEntidade=" + nomeTecnicoEntidade;

	var url = '/MRVWeb/LogAlteracao/MotivoAlteracaoCampo.aspx?' + oParam;

	var retorno = openStdDlg(url, null, 750, 440);
	if (retorno != null && retorno != undefined) {
		var cmd = new RemoteCommand("MrvService", "CriarMotivoAlteracaoElegibilidade", "/MRVCustomizations/");
		cmd.SetParameter("nomeTecnicoEntidade", nomeTecnicoEntidade);
		cmd.SetParameter("registroId", idRegistro);
		cmd.SetParameter("nomeRegistro", nomeRegistro);
		cmd.SetParameter("valorCampo", valorCampo);
		cmd.SetParameter("motivo", retorno.Motivo);
		cmd.SetParameter("justificativa", retorno.Justivicativa);
		var oResult = cmd.Execute();

		if (!crmForm.TratarRetornoRemoteCommand(oResult, "CriarMotivoAlteracaoElegibilidade")) {
			alert("Operação cancelada.");
			event.returnValue = false;
			return false;
		}
	} else {
		alert("Operação cancelada.");
		event.returnValue = false;
		return false;
	}
}
		
	}
			
    
    if (crmForm.all.new_codmaterial.DataValue && crmForm.all.new_codmaterial.IsDirty)
        VerificarSeCodigoSAPJaExiste("new_codmaterial", crmForm.all.new_codmaterial.DataValue);
    
    if (crmForm.all.new_codigomaterialsap6.DataValue && crmForm.all.new_codigomaterialsap6.IsDirty)
        VerificarSeCodigoSAPJaExiste("new_codigomaterialsap6", crmForm.all.new_codigomaterialsap6.DataValue);
    
    if (crmForm.all.new_statusdaunidade.DataValue == StatusDaUnidade.PERMUTADO)
        ValidarTipoPermuta();
    
    crmForm.TransformaTodosCaracteresEmMaiusculo();
    
    crmForm.all.currentcost.Disabled = false;
    crmForm.all.new_flag_produto_elegivel.ForceSubmit = true;    
    crmForm.all.new_sicaq.ForceSubmit = true;
    crmForm.all.new_sac_bb.ForceSubmit = true;
    crmForm.all.new_correspondente_sicaq.ForceSubmit = true;
    crmForm.all.new_correspondente_sac_bb.ForceSubmit = true;
    crmForm.all.new_id_correspondente_sicaq.ForceSubmit = true;
    crmForm.all.new_id_correspondente_sacbb.ForceSubmit = true;
} 
catch (error) 
{
    alert("Ocorreu um erro no formulário.\n" + error.description);
    event.returnValue = false;
    return false;
}