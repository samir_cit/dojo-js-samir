﻿formularioValido = true;
tipologiaAtual = null;
nomeTipologia = null;

try {
    function loadScript() {
        var oScript = document.createElement('script');
        oScript.type = "text/javascript";
        oScript.src = "/_static/Base.js";

        var oHead = document.getElementsByTagName('head')[0];
        oHead.appendChild(oScript);

        oScript.onreadystatechange = function() {
            if (this.readyState == 'complete' || this.readyState == 'loaded')
                Load();
        }
    }
    


    loadScript();

    function Load() {
        var oScript = document.createElement("script");
        oScript.src = "/_static/_grid/cmds/util.js";
        document.appendChild(oScript);

        crmForm.valorCampoElegibilidade = crmForm.all.new_flag_produto_elegivel.DataValue;

        TipoDeProduto =
        {
            UNIDADE: "1",
            GARAGEM: "2",
            BOX: "3",
            KITACABEMNTO: "5",
            SERVICO: "200000",
            LOTE: "200001",
            LOJA: "200002"
        }

        TipoDoBloco =
        {
            TORRE: "2",
            CASA: "3"
        }

        StatusDaUnidade =
        {
            DEV_ACABADO: "10",
            DEV_SEMIACABADO: "9",
            LANCADO: "2",
            MANUTENCAO: "8",
            NAO_LANCADO: "1",
            PERMUTADO: "3",
            RESERVA_CONTRATO: "13",
            RESERVA_DIRETOR: "11",
            RESERVA_JURIDICO: "12",
            RESERVADO: "4",
            RESERVADO_COM_CONTRATO_PRE_ATIVO: "15",
            RESERVADO_PARA_PERMUTA: "14",
            VENDIDO: "5",
            VENDIDOS_COM_ESTOQUE_1: "7",
            VENDIDOS_SEM_OPORTUNIDADE: "6"
        }

        function configurarOnLoad() {
        
            if (crmForm.all.new_areadequintal.DataValue != 1) {
                crmForm.all.new_tamanhodareadequintal.Disabled = true;
            }

            crmForm.all.new_tamanhoareacobrada.Disabled = true;

            crmForm.all.new_descricaorevitalizacao.Disabled = true;
            crmForm.all.new_descricaodecoracao.Disabled = true;
            crmForm.all.new_descricaogaragem.Disabled = true;
            crmForm.all.new_descricaobox.Disabled = true;

            if (crmForm.all.quantitydecimal.DataValue == null)
                crmForm.all.quantitydecimal.DataValue = 2;

            new_permuta_onchange0();
            crmForm.all.new_statusdaunidade.FireOnChange();
            crmForm.all.new_terrenoid.FireOnChange();

            tipologiaAtual = crmForm.all.new_tipologiaid.DataValue != null ? crmForm.all.new_tipologiaid.DataValue[0].id : null;
            nomeTipologia = crmForm.all.new_tipologiaid.DataValue != null ? crmForm.all.new_tipologiaid.DataValue[0].name : '';
            
            if (!crmForm.UsuarioPertenceEquipe(Equipe.EMPREENDIMENTO_ESTRATEGIA_NEGOCIACAO)){
				crmForm.all.new_estrategia_negociacao.Disabled = true;
            }
        }

        configurarOnLoad();

        /// Ocultando guias a partir da seleção do tipo de produto
        function ConfigurarVisaoPorTipoProduto() {
            var mostrarKit = "none";
            var mostrarTpPadrao = "none";
            var mostrarDescricaoGaragem = "none";
            var mostrar = "none";
            var boleanoGaragem = true;
            var boleanoBox = true;
            var nulo = true;
            var boleanoAPTO = true;
            var nuloTpPadrao = true;
            var setApto = 0;
            var setTpPadrao = 0;
            var classe = "n";
            var classeTpPadrao = "n";

            if (crmForm.all.producttypecode.DataValue != null) {
                var tipoDeProduto = crmForm.all.producttypecode.DataValue;

                var tipo = crmForm.all.producttypecode.SelectedText.toLowerCase();

                mostrar = tipoDeProduto == TipoDeProduto.UNIDADE ? "inline" : "none";
                setApto = tipoDeProduto == TipoDeProduto.UNIDADE ? 2 : 0;
                classe = tipoDeProduto == TipoDeProduto.UNIDADE ? "req" : "n";
                nulo = tipoDeProduto != TipoDeProduto.UNIDADE;
                boleanoAPTO = tipoDeProduto != TipoDeProduto.UNIDADE;
                boleanoGaragem = tipoDeProduto != TipoDeProduto.GARAGEM;
                boleanoBox = tipoDeProduto != TipoDeProduto.BOX;
                setTpPadrao = tipoDeProduto == TipoDeProduto.KITACABEMNTO ? 2 : 0;
                nuloTpPadrao = tipoDeProduto != TipoDeProduto.KITACABEMNTO;
                classeTpPadrao = tipoDeProduto == TipoDeProduto.KITACABEMNTO ? "req" : "n";
                mostrarTpPadrao = tipoDeProduto == TipoDeProduto.KITACABEMNTO ? "" : "none";
                mostrarKit = tipoDeProduto == TipoDeProduto.KITACABEMNTO ? "inline" : "none";
                mostrarDescricaoGaragem = (tipoDeProduto == TipoDeProduto.GARAGEM || tipoDeProduto == TipoDeProduto.BOX) ? "inline" : "none";
            }

            crmForm.all.tab1Tab.style.display = mostrar;
            crmForm.all.tab2Tab.style.display = mostrar;
            crmForm.all.new_descricaogaragem.Disabled = boleanoGaragem;
            crmForm.all.new_descricaobox.Disabled = boleanoBox;

            if (nulo) {
                crmForm.all.new_aptocasanumero.DataValue = null;
                crmForm.all.new_tipodepadrao.DataValue = null;
            }

            crmForm.all.new_aptocasanumero.setAttribute("req", setApto);
            crmForm.all.new_aptocasanumero_c.className = classe;
            crmForm.all.new_aptocasanumero.Disabled = boleanoAPTO;
            crmForm.all.new_tipodepadrao.setAttribute("req", setTpPadrao);
            crmForm.all.new_tipodepadrao_c.className = classeTpPadrao;
            crmForm.all.new_tipodepadrao_c.style.display = mostrarTpPadrao;
            crmForm.all.new_tipodepadrao_d.style.display = mostrarTpPadrao;
            document.getElementById("new_original_d").parentNode.parentNode.style.display = mostrarKit;
            document.getElementById("new_descricaokit_d").parentNode.parentNode.style.display = mostrarKit;
            document.getElementById("new_descricaogaragem_d").parentNode.parentNode.style.display = mostrarDescricaoGaragem;
        }

        ConfigurarVisaoPorTipoProduto();

        function ConfigurarNomeTipologia() {
            if (crmForm.all.new_tipologiaid.DataValue == null)
                return null;

            var nomeTipologia = crmForm.all.new_tipologiaid.DataValue[0].name.toUpperCase();
            if (nomeTipologia == "VAGA" || nomeTipologia == "LOTE" || nomeTipologia == "LOJA")
                return null;

            nomeTipologia = (nomeTipologia.substr(0, 1) == "1") ? nomeTipologia.replace("Q", "Quarto") : nomeTipologia.replace("Q", "Quartos");
            nomeTipologia = nomeTipologia.replace(" S", " com Suíte").replace("COB", ", Cobertura").replace("ESC", "Escritório");
            return nomeTipologia;
        }

        crmForm.all.new_tratnomedatipologia.DataValue = ConfigurarNomeTipologia();

        crmForm.BloquearTipologia = function() {
            if (crmForm.all.new_statusdaunidade.DataValue != null) {
                var statusUnidade = crmForm.all.new_statusdaunidade.DataValue;

                return
                statusUnidade == StatusDaUnidade.RESERVADO ||
                    statusUnidade == StatusDaUnidade.RESERVADO_COM_CONTRATO_PRE_ATIVO ||
                    statusUnidade == StatusDaUnidade.RESERVADO_PARA_PERMUTA ||
                    statusUnidade == StatusDaUnidade.VENDIDO ||
                    statusUnidade == StatusDaUnidade.VENDIDOS_COM_ESTOQUE_1 ||
                    statusUnidade == StatusDaUnidade.VENDIDOS_SEM_OPORTUNIDADE;
            }
            return false;
        }

        crmForm.all.new_tipologiaid.Disabled = crmForm.BloquearTipologia();

        crmForm.ConfigurarVisaoFormulario = function() {
            /// Configurações Por Equipe
            administrador = crmForm.UsuarioPertenceEquipe(Equipe.ADMINISTRADORES);
            desenvImobiliario = crmForm.UsuarioPertenceEquipe(Equipe.DESENV_IMOBILIARIO);
            coordenacao = crmForm.UsuarioPertenceEquipe(Equipe.COORDENACAO_VENDAS_CONFIGURACOES);
            alterarStatusProduto = crmForm.UsuarioPertenceEquipe(Equipe.ALTERAR_STATUS_PRODUTO);

            if (!desenvImobiliario && !administrador)
                crmForm.DesabilitarFormulario(true);

            if (crmForm.UsuarioPertenceEquipe(Equipe.NUCLEO_NOTIFICACAO + ";" + Equipe.NUCLEO_NIAC + ";" + Equipe.NUCLEO_ENCANTE + ";" + Equipe.NUCLEO_CONTROLE_ESTRATEGICO)) {
                crmForm.all.new_data_da_visita.Disabled = false;
                crmForm.all.new_visita_marcada.Disabled = false;
                crmForm.all.new_visita_realizada.Disabled = false;
                // crmForm.EscondeSecao("new_data_da_visita", false);
            }

            if (administrador || alterarStatusProduto)
                crmForm.all.new_statusdaunidade.Disabled = false;
            else
                crmForm.all.new_statusdaunidade.Disabled = true;

            /// Habilitar a Tab1 caso a equipe seja da Cooredenação de Vendas.
            if (coordenacao) {
                var atributosTab1 = document.getElementById("tab1").getElementsByTagName("label");

                for (var i = 0; i < atributosTab1.length; i++) {
                    if (atributosTab1[i].htmlFor != "") {
                        var object = eval("crmForm.all." + atributosTab1[i].htmlFor.replace("_ledit", ""))
                        object.Disabled = false;
                    }
                }
            }

            crmForm.ObterTipoBloco();

            if (crmForm.all.producttypecode.DataValue != TipoDeProduto.UNIDADE) {
                crmForm.SetFieldReqLevel("new_visita_marcada", false);
                crmForm.SetFieldReqLevel("new_visita_realizada", false);
                crmForm.EscondeSecao("new_data_da_visita", true);
            }

            if (crmForm.UsuarioPertenceEquipe(Equipe.ELEGIBILIDADE_EMPREENDIMENTO)) {
                crmForm.all.new_flag_produto_elegivel.Disabled = false;
            }
            else {
                crmForm.all.new_flag_produto_elegivel.Disabled = true;
            }
        }

        crmForm.ConfigurarAreaQuintal = function() {

            if (crmForm.all.new_areadequintal.DataValue) {
                crmForm.all.new_tamanhodareadequintal.Disabled = false;
                crmForm.SetFieldReqLevel("new_tamanhodareadequintal", true);

                crmForm.all.new_tamanhoareacobrada.Disabled = true;
                crmForm.SetFieldReqLevel("new_tamanhoareacobrada", true);
            }
            else {
                crmForm.all.new_tamanhodareadequintal.DataValue = null;
                crmForm.all.new_tamanhodareadequintal.Disabled = true;
                crmForm.SetFieldReqLevel("new_tamanhodareadequintal", false);
                crmForm.new_tamanhodareadequintal.ForceSubmit = true;

                crmForm.all.new_tamanhoareacobrada.DataValue = null;
                crmForm.all.new_tamanhoareacobrada.Disabled = true;
                crmForm.SetFieldReqLevel("new_tamanhoareacobrada", false);
                crmForm.new_tamanhoareacobrada.ForceSubmit = true;
            }
        }
        
        crmForm.ConfigurarVisaoFormulario();
        crmForm.BloquearCamposDesenvImobiliario();

        crmForm.DesabilitaVendaGarantida = function() {
            if (crmForm.FormType == TypeCreate || crmForm.FormType == TypeUpdate) {
                if (crmForm.UsuarioPertenceEquipe(Equipe.COM_VENDA_GARANTIDA)) {
                    crmForm.all.new_venda_garantida.Disabled = false;
                } else {
                    crmForm.all.new_venda_garantida.Disabled = true;
                }
            } 
        }
        crmForm.DesabilitaVendaGarantida();
		
		crmForm.HabilitarHipoteca = function() {
			if (crmForm.FormType == TypeCreate || crmForm.FormType == TypeUpdate) {
				if (crmForm.all.new_unidade_hipotecada.DataValue && crmForm.UsuarioPertenceEquipe(Equipe.CR_CONTROLE_HIPOTECA))
				{
					crmForm.DesabilitarCampos("new_baixa_hipoteca", false);
                } else {
                    crmForm.DesabilitarCampos("new_baixa_hipoteca", true);
				}
			}
		}
		
		crmForm.HabilitarHipoteca();
    } 

    crmForm.ObterTipoBloco = function() {
        var rCmd = new RemoteCommand('MrvService', 'ObterInformacoesBloco', '/MRVCustomizations/');
        rCmd.SetParameter('produtoId', crmForm.ObjectId);
        var retorno = rCmd.Execute();

        if (crmForm.TratarRetornoRemoteCommand(retorno, "ObterInformacoesBloco")) {
            if (retorno.ReturnValue.Mrv.Tipo != null) {
                var tipo = retorno.ReturnValue.Mrv.Tipo;
                if (tipo != TipoDoBloco.CASA) {
                    if (crmForm.UsuarioPertenceEquipe(Equipe.EG_CARACTERISTICA_PREMIUM)) {
                        crmForm.all.new_apartamento_pne.Disabled = false;
                        crmForm.all.new_varanda.Disabled = false;
                        crmForm.all.new_elevador.Disabled = tipo == TipoDoBloco.TORRE ? true : false;
                        crmForm.all.new_girardino.Disabled = false;
                        crmForm.all.new_circulacao_maior.Disabled = false;
                        crmForm.all.new_nascente.Disabled = false;
                    }
                    else {
                        DesabilitaSecaoCaracteristicasPremium();
                    }
                }
                else {
                    DesabilitaSecaoCaracteristicasPremium();
                }
                if (tipo == TipoDoBloco.TORRE) {
                    crmForm.all.new_elevador.DataValue = true;
                    crmForm.all.new_elevador.Disabled = true;
                }
            }
        }
    }

    function DesabilitaSecaoCaracteristicasPremium() {
        crmForm.all.new_apartamento_pne.Disabled = true;
        crmForm.all.new_varanda.Disabled = true;
        crmForm.all.new_elevador.Disabled = true;
        crmForm.all.new_girardino.Disabled = true;
        crmForm.all.new_circulacao_maior.Disabled = true;
        crmForm.all.new_nascente.Disabled = true;
    }

    crmForm.LimparElegibilidade = function() {
        crmForm.all.new_sicaq.DataValue = false;
        crmForm.all.new_sac_bb.DataValue = false;
        crmForm.all.new_correspondente_sicaq.DataValue = null;
        crmForm.all.new_correspondente_sac_bb.DataValue = null;
        crmForm.all.new_id_correspondente_sicaq.DataValue = null;
        crmForm.all.new_id_correspondente_sacbb.DataValue = null;
    }

    crmForm.BloquearCamposDesenvImobiliario = function() {
        var unidadeNaoLancada;

        if (crmForm.all.new_statusdaunidade.DataValue != null) {
            unidadeNaoLancada = (StatusDaUnidade.NAO_LANCADO == crmForm.all.new_statusdaunidade.DataValue);
        }

        var camposQueDeveraoSerBloqueados = [
									"name",
									"new_tipologiaid",
									"new_areadequintal",
									"new_tamanhodareadequintal",
									"new_tamanhoareacobrada",
									"new_area_apartamento",
									"new_area_varanda"];
									
        var bloquearCampos = (!crmForm.UsuarioPertenceEquipe(Equipe.DI_ALTERACAO_PRODUTO) && !unidadeNaoLancada);
        crmForm.DesabilitarCampos(camposQueDeveraoSerBloqueados.join(";"), bloquearCampos);

        if (StatusDaUnidade.NAO_LANCADO == crmForm.all.new_statusdaunidade.DataValue || crmForm.UsuarioPertenceEquipe(Equipe.DI_ALTERACAO_PRODUTO))
            crmForm.ConfigurarAreaQuintal();
    }

    crmForm.ConfigurarAbaIntegracao = function() {
        if (!crmForm.UsuarioPertenceEquipe(Equipe.ADMINISTRADORES)) {
            crmForm.all.tab5Tab.style.display = "none";
        }
    }        
}
catch (error) {
    formularioValido = false;
    alert("Ocorreu um erro no formulário.\n" + error.description);
}
