﻿try {

    function Load() {
        TipoAuditoria =
        {
            PROPOSTA: 1,
            OPORTUNIDADE_RENEGOCIACAO: 2,
            PROPOSTA_CSG: 3,
            EMPREENDIMENTO: 4,
            RETIFICACAO_RI:5
        }

        TipoStatusCartorio =
	    {
	        Protocolado: 1,
	        Deferido: 2,
	        EmAnalise: 3,
	        AtendendoExigencias: 4,
	        Indeferido: 5,
	        EmContratacao: 6
	    }

        AbrirRegistro = function(typecode, id) {
            openObj(typecode, id);
        }
        crmForm.ObterPermissaoDeCriacaoDaRetificacaoDeferidaParaEmpreendimento()
        Auditar = function(auditoriaCSG) {
            var cmd = new RemoteCommand("MrvService", "Auditar", "/MrvCustomizations/");
            cmd.SetParameter("entidadeId", crmForm.ObjectId);
            cmd.SetParameter("tipoAuditoria", TipoAuditoria.RETIFICACAO_RI);

            var resultado = cmd.Execute();

            if (resultado.Success) {
                if (resultado.ReturnValue.Mrv.Error)
                    alert(resultado.ReturnValue.Mrv.Error);
                else if (resultado.ReturnValue.Mrv.ErrorSoap)
                    alert(resultado.ReturnValue.Mrv.ErrorSoap)
                else {
                    document.getElementById("IFRAME_auditoria").src = document.getElementById("IFRAME_auditoria").src;
                    crmForm.DesabilitarCampos("new_status_cartorio", true);
                    crmForm.DesabilitarCampos("new_numero_ri", true);
                }
            }
        }

        crmForm.VerificaObrigatoriedadeCampos = function() {
            if (crmForm.all.new_status_cartorio.DataValue &&
                crmForm.all.new_status_cartorio.DataValue == TipoStatusCartorio.Deferido) {
                crmForm.SetFieldReqLevel("new_numero_ri", true);
            }
            else {
                crmForm.SetFieldReqLevel("new_numero_ri", false);
            }
        }

        crmForm.DesabilitarAuditoria = function() {
            // O botão auditoria só irá aparecer caso o usuário pertence a equipe NCE_Auditoria_RI e o status do cartório for
            // igual a Deferido
            if (crmForm.FormType == TypeUpdate) {
                if (crmForm.all.new_status_cartorio.DataValue &&
                    crmForm.all.new_status_cartorio.DataValue == TipoStatusCartorio.Deferido &&
                    crmForm.UsuarioPertenceEquipe(Equipe.NCE_AUDITORIA_RI)) {
                    document.getElementById('IFRAME_auditoria').style.display = '';
                    return;
                }
            }

            document.getElementById('IFRAME_auditoria').style.display = 'none';
        }

        crmForm.DesabilitarAuditoria();
        crmForm.DesabilitarCamposRIAuditoria = function() {
            if (crmForm.FormType == TypeUpdate) {
                var cmd = new RemoteCommand("MrvService", "VerificarExisteAuditoria", "/MRVCustomizations/");
                cmd.SetParameter("entidadeId", crmForm.ObjectId);
                cmd.SetParameter("tipoAuditoria", TipoAuditoria.RETIFICACAO_RI);
                var result = cmd.Execute();
                if (result.Success) {
                    if (result.ReturnValue.Mrv.ExisteAuditoria) {
                        crmForm.DesabilitarCampos("new_status_cartorio", true);
                        crmForm.DesabilitarCampos("new_numero_ri", true);
                    }
                }
            }
        }
        crmForm.VerificaObrigatoriedadeCampos();
        crmForm.DesabilitarAuditoria();
        crmForm.DesabilitarCamposRIAuditoria();   
    }

    function loadScript() {
        var oHead = document.getElementsByTagName('head')[0];
        var oScript = document.createElement('script');
        oScript.type = "text/javascript";
        oScript.src = "/_static/Base.js";

        oScript.onreadystatechange = function() {
            if (this.readyState == 'complete' || this.readyState == 'loaded') {
                Load();
            }
        }
        oHead.appendChild(oScript);
    }

    loadScript();

    

    crmForm.ObterPermissaoDeCriacaoDaRetificacaoDeferidaParaEmpreendimento = function() {

        if (crmForm.FormType == TypeCreate) {

            crmForm.all.new_name.value = 'RETIFICAÇÃO DE RI';

            document.getElementById('new_name_c').parentElement.style.visibility = 'hidden';
            document.getElementById('new_name_c').parentElement.style.display = 'none';

            if (window.opener.parent.crmForm.new_statusdocartrio.DataValue != 2) {//2 = Deferido
                alert('Retificação não Permitida, as retificações são permitidas apenas para os registros deferidos!')
                self.close();
            }

            var retorno = false;
            var cmd = new RemoteCommand("MrvService", "ObterPermissaoDeCriacaoDaRetificacaoDeferidaParaEmpreendimento", "/MRVCustomizations/");
            cmd.SetParameter("empreendimentoId", window.opener.parent.crmForm.ObjectId);
            var result = cmd.Execute();

            if (result.Success) {
                if (result.ReturnValue.Mrv.TotalRegistros > 0) { //se o ultimo registro
                    if (result.ReturnValue.Mrv.Resultado == false) { //não for deferido
                        alert('Retificação não Permitida, as retificações são permitidas apenas para os registros deferidos!');
                        self.close();
                    }
                    else {
                        //for deferido, deixa criar o registro
                    }
                }
                else {
                    //se não for inserido nenhuma retificação ainda, deixa criar o registro
                }
            }
        }
    }

} catch (error) {
    formularioValido = false;
    alert("Ocorreu um erro no formulário.\n" + error.description);
}