﻿var descricaoStatusCartorio = '';

if (crmForm.all.new_status_cartorio.DataValue != null) {
    switch (parseInt(crmForm.all.new_status_cartorio.DataValue)) {
        case 1: descricaoStatusCartorio = "Protocolado"; break;
        case 2: descricaoStatusCartorio = "Deferido"; break;
        case 3: descricaoStatusCartorio = "Em Análise"; break;
        case 4: descricaoStatusCartorio = "Atendendo Exigências"; break;
        case 5: descricaoStatusCartorio = "Indeferido"; break;
        case 6: descricaoStatusCartorio = "Em Contratação"; break;
    }
}

crmForm.all.new_name.value = 'RETIFICAÇÃO DE RI - ' + descricaoStatusCartorio;

crmForm.VerificaObrigatoriedadeCampos();