﻿formularioValido = true;
try {

    AdicionaNotificacao = function(message, tipo) {
        var imagem = "";
        if (tipo == "erro")
            imagem = "/_imgs/error/notif_icn_crit16.png";
        else if (tipo == "alerta")
            imagem = "/_imgs/error/notif_icn_warn16.png";
        else if (tipo == "sucesso")
            imagem = "/_imgs/ico/16_L_check.gif";

        var notificationHTML = '<DIV class="Notification"><TABLE cellSpacing="0" cellPadding="0"><TBODY><TR><TD vAlign="top"><IMG class="ms-crm-Lookup-Item" alt="" src="' + imagem + '" /></TD><TD><SPAN>' + message + '</SPAN></TD></TR></TBODY></TABLE></DIV>';
        var notificationsArea = document.getElementById('Notifications');
        if (notificationsArea == null) 
            return;
        notificationsArea.innerHTML = notificationHTML;
        notificationsArea.style.display = 'block';
    }
    
    //se status igual a enviado, desabilitar tudo
    if (crmForm.all.statuscode.DataValue == 3) {
        var todosAtributos = document.getElementsByTagName("label");
        for (var i = 0; i < todosAtributos.length; i++) {
            if (todosAtributos[i].htmlFor != "") {
                var object = eval("crmForm.all." + todosAtributos[i].htmlFor.replace("_ledit", ""))
                object.Disabled = true;
            }
        }
        AdicionaNotificacao("Registro Enviado.", "sucesso");
    }

    crmForm.VerificaFuncao = function(nomeFuncao) {
        var cmd = new RemoteCommand("MrvService", "UserContainsTeams", "/MrvCustomizations/");
        cmd.SetParameter("teams", nomeFuncao);
        var resultado = cmd.Execute();
        if (resultado.Success) {
            return resultado.ReturnValue.Mrv.Found;
        } else {
            alert("Erro na consulta do método UserContainsTeams");
            AdicionaNotificacao("Erro na consulta do método UserContainsTeams", "erro");
            formularioValido = false;
            return false;
        }
    }

    crmForm.AlterarEnderecoSAP_CRM = function() {
        if (crmForm.all.statuscode.DataValue == 3) {
            alert("Registro já foi enviado.");
            AdicionaNotificacao("Registro já foi enviado.", "alerta");
            return false;
        }

        if (!crmForm.IsValid())
            return false;

        if (crmForm.IsDirty) {
            alert('Salve antes de enviar.');
            AdicionaNotificacao("Salve antes de enviar.", "alerta");
            return false;
        }

        if (!crmForm.VerificaFuncao("AtualizaEndereço")) {
            alert("Usuário não possui privilégio para executar está função");
            AdicionaNotificacao("Usuário não possui privilégio para executar está função", "erro");
            return false;
        }

        var cmd = new RemoteCommand('MrvService', 'AlterarEnderecoSAP_CRM', '/MRVCustomizations/');
        cmd.SetParameter('alteracaoEnderecoId', crmForm.ObjectId);
        var resultado = cmd.Execute();
        if (!resultado.Success) {
            alert('Erro ao acessar o WebService. Tente novamente mais tarde.');
            AdicionaNotificacao("Erro ao acessar o WebService. Tente novamente mais tarde.", "erro");
            return false;
        }

        if (resultado.ReturnValue.Mrv.Error) {
            alert(resultado.ReturnValue.Mrv.Error);
            return false;
        } else if (resultado.ReturnValue.Mrv.ErrorSoap) {
            alert(resultado.ReturnValue.Mrv.ErrorSoap);
            return false;
        } else {
            window.location.reload(true);
        }
    }

    crmForm.EscondeCampo = function(nomeCampo, esconde) {
        var tipo = "";
        if (esconde)
            tipo = "none";

        var atributo_c = eval("crmForm.all." + nomeCampo + "_c");
        var atributo_d = eval("crmForm.all." + nomeCampo + "_d");
        atributo_c.style.display = tipo;
        atributo_d.style.display = tipo;
    }

    //Verifica se cobrança, se sim, mostra campo oportunidade.
    crmForm.MostrarOportunidade = function() {
        if (crmForm.all.new_tipo_endereco.DataValue != null) {
            if (crmForm.all.new_tipo_endereco.DataValue == "1") {
                crmForm.EscondeCampo("new_oportunidadeid", false);
                crmForm.SetFieldReqLevel("new_oportunidadeid", true);
            }
            else if (crmForm.all.new_tipo_endereco.DataValue == "3") {
                crmForm.EscondeCampo("new_oportunidadeid", true);
                crmForm.SetFieldReqLevel("new_oportunidadeid", false);
            }
        }
        else {
            crmForm.EscondeCampo("new_oportunidadeid", true);
            crmForm.SetFieldReqLevel("new_oportunidadeid", 1);
        }
    }

    //Montar nome do registro
    crmForm.MontaNome = function() {
        var nomeTmp = null;
        if (crmForm.all.new_tipo_endereco.DataValue != null)
            nomeTmp = crmForm.all.new_tipo_endereco.SelectedText + " - ";
        if (crmForm.all.new_endereco.DataValue != null)
            nomeTmp += crmForm.all.new_endereco.DataValue;

        crmForm.all.new_name.DataValue = nomeTmp;
    }

    BotaoAjuda = function(nomeCampo) {
        var atributo = this;

        atributo.Field = crmForm.all[nomeCampo];

        if (!atributo.Field) {
            return alert("Campo desconhecido: " + nomeCampo);
        }

        atributo.Click = null;
        atributo.Image = new ImagemBotao();
        atributo.Paint = function() {
            var atributo_d = document.all[atributo.Field.id + "_d"];
            if (atributo_d) {
                atributo_d.style.whiteSpace = "nowrap";
                atributo_d.appendChild(atributo.Image.ToObject())
            }
        }

        atributo.MouseOver = function() {
            event.srcElement.src = atributo.Image.MouseOver;
        }

        atributo.MouseOut = function() {
            event.srcElement.src = atributo.Image.MouseOut;
        }

        function ImagemBotao() {
            this.MouseOut = "";
            this.MouseOver = "";
            this.Width = 21;
            this.Height = 19;
            this.Cursor = "auto";
            this.Title = "";

            this.ToObject = function() {
                var img = document.createElement("IMG");
                img.onmouseover = atributo.MouseOver;
                img.onmouseout = atributo.MouseOut;
                img.onclick = atributo.Click;
                img.src = this.MouseOut;
                img.title = this.Title;

                var cssText = "vertical-align:bottom;";
                cssText += "margin:1px;";
                cssText += "position:relative;";
                cssText += "cursor:" + this.Cursor + ";";
                cssText += "right:" + (this.Width + 1) + "px;";
                cssText += "height:" + (this.Height + 1) + "px";
                img.style.cssText = cssText;
                return img;
            }
        }
    }

    function RemoveBotao(nome) {
        var lista = document.getElementsByTagName("LI");
        for (i = 0; i < lista.length; i++) {
            if (lista[i].id.indexOf(nome) >= 0) {
                var o = lista[i].parentElement;
                o.removeChild(lista[i]);
                break;
            }
        }
    }

    function AdicionaBotaoLookup(campo, title, funcao) {
        //Passar o nome do atributo como parâmetro
        var actnButton = new BotaoAjuda(campo);
        //Mensagem
        actnButton.Image.Title = title;
        //Tamanho e Altura
        actnButton.Image.Width = 21;
        actnButton.Image.Height = 18;
        //Cursor
        //Função que o clique deve chamar
        //Imagens Over e Out
        if (!crmForm.all[campo].Disabled) {
            actnButton.Image.Cursor = "hand";
            actnButton.Image.MouseOver = "/_imgs/btn_on_lookup.gif";
            actnButton.Image.MouseOut = "/_imgs/btn_off_lookup.gif";
            actnButton.Click = eval(funcao);
        } else {
            actnButton.Image.MouseOver = "/_imgs/btn_dis_lookup.gif";
            actnButton.Image.MouseOut = "/_imgs/btn_dis_lookup.gif";
            actnButton.Click = "";
        }

        //Adiciona actnButton imagem ao lado do campo
        actnButton.Paint();
    }

    crmForm.LimparCampos = function() {
        crmForm.all.new_endereco.DataValue = null;
        crmForm.all.new_enderecoid.DataValue = null;
        crmForm.all.new_oportunidadeid.DataValue = null;
    }

    crmForm.AbreLookupEndereco = function() {
        if (crmForm.all.new_tipo_endereco.DataValue == null) {
            alert("Escolha o tipo de endereço.");
            crmForm.all.new_tipo_endereco.SetFocus();
            return;
        }

        var oParam = "objectTypeCode=1071&filterDefault=false&attributesearch=name&_addresstypecode=" + crmForm.all.new_tipo_endereco.DataValue + "&_parentid=" + crmForm.all.new_clienteid.DataValue[0].id + " ";
        var url = null;
        if (oParam != null)
            url = '/MRVWeb/FilterLookup/FilterLookup.aspx?' + oParam;

        if (url == null) {
            alert("Sem parâmentros. Erro 1.");
            return;
        }

        var retorno = openStdDlg(url, null, 750, 440);
        if (typeof (retorno) != "undefined" && typeof (retorno) != "unknown" && retorno != null) {
            var strValues = retorno.split('*|*');
            crmForm.all.new_enderecoid.DataValue = "{" + strValues[1] + "}";
            crmForm.all.new_endereco.DataValue = strValues[0];
        }
        crmForm.MontaNome();
    }

    //filterlookup
    crmForm.FilterLookup = function(attribute, url, param) {
        if (param != null)
            url += '?' + param;

        oImg = eval('attribute' + '.parentNode.childNodes[0]');
        oImg.onclick = function() {
            retorno = openStdDlg(url, null, 750, 440);
            if (typeof (retorno) != "undefined") {
                strValues = retorno.split('*|*');
                var lookupData = new Array();
                lookupItem = new Object();
                lookupItem.id = "{" + strValues[1] + "}";
                lookupItem.type = parseInt(3);
                lookupItem.name = strValues[0];
                lookupData[0] = lookupItem;
                attribute.DataValue = lookupData;
                attribute.FireOnChange();
            }
        };
    }

    var IdCustomer = (crmForm.all.new_clienteid.DataValue != null) ? crmForm.all.new_clienteid.DataValue[0].id : "";
    var oParam = "objectTypeCode=300&filterDefault=false&attributesearch=title&_customerid=" + IdCustomer + "&_statuscode=2&_new_tipodeoportunidade=1&isUnion=true";

    crmForm.FilterLookup(crmForm.all.new_oportunidadeid, "/MRVWeb/FilterLookup/FilterLookup.aspx", oParam);

    //simula botão lookup do endereço
    AdicionaBotaoLookup("new_endereco", "Clique para selecionar um valor para endereço.", "crmForm.AbreLookupEndereco");

    //Desabilitar campos
    crmForm.all.new_endereco.readOnly = true;
    crmForm.all.new_clienteid.Disabled = true;
    crmForm.all.new_dados_anteriores.readOnly = true;

    //Esconder campos da seção administrativa
    crmForm.EscondeCampo("new_name", true);
    crmForm.EscondeCampo("new_enderecoid", true);

    crmForm.MostrarOportunidade();

    //Esconde botões SaveAndClose e SaveAndNew
    RemoveBotao("_MBcrmFormSaveAndClose");
    RemoveBotao("_MBcrmFormSubmitCrmForm59truetruefalse");
    
    if (crmForm.FormType == 2 && crmForm.all.statuscode.DataValue != 3) {
        crmForm.AlterarEnderecoSAP_CRM();
    }
    
} catch (error) {
    formularioValido = false;
    alert("Ocorreu um erro no formulário.\n" + error.description);
}