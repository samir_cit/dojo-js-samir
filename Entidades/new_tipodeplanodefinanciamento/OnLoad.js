﻿formularioValido = true;
try {
    crmForm.all.new_name_c.style.visibility = "hidden";
    crmForm.all.new_name_d.style.visibility = "hidden";
    crmForm.DefinirNome = function() {
        if (crmForm.all.new_tipodeplanodefinanciamentoprodid.DataValue != null && crmForm.all.new_planodefinanciamentoid.DataValue != null)
            crmForm.all.new_name.DataValue = crmForm.all.new_tipodeplanodefinanciamentoprodid.DataValue[0].name + " - " + crmForm.all.new_planodefinanciamentoid.DataValue[0].name;
    }
} catch (error) {
    formularioValido = false;
    alert("Ocorreu um erro no formulário.\n" + error.description);
}