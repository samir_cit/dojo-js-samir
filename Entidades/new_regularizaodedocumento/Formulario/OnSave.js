﻿try {
    if (!formularioValido) {
        alert("Ocorreu um erro no formulário. Impossível salvar.");
        event.returnValue = false;
        return false;
    }

    //Verifica se o código do SAP informado já existe em outro empreendimento
    if ((crmForm.all.new_regularizaodedocumentosid.DataValue && crmForm.all.new_regularizaodedocumentosid.IsDirty) || crmForm.FormType == 1) {
        var cmd = new RemoteCommand("MrvService", "ExisteRegularizacaoDocumento", "/MRVCustomizations/");
        cmd.SetParameter("terrenoId", crmForm.all.new_regularizaodedocumentosid.DataValue[0].id);
        var entityId = crmForm.ObjectId;
        if (!entityId)
            entityId = "";

        cmd.SetParameter("regularizacaoId", entityId);
        var result = cmd.Execute();
        if (result.Success) {
            if (result.ReturnValue.Mrv.Found) {
                alert("Não é possível criar mais de uma Reg. Documento para o mesmo terreno.\nAltere o terreno e tente salvar novamente!");
                event.returnValue = false;
                return false;
            }
        }
    }


    //Script OnChange Lookup

    if (crmForm.all.new_regularizaodedocumentosid.DataValue != null) {
        crmForm.all.new_name.DataValue = "Reg. de Doc. - " +
  crmForm.all.new_regularizaodedocumentosid.DataValue[0].name;
    }

    //Desabilitar campos----------------------------------------------
    crmForm.all.new_diferena.Disabled = false;
    crmForm.all.new_diferencaiptu_top.Disabled = false;
    crmForm.all.new_diferencamat_top.Disabled = false;
    crmForm.all.new_diferencaemmatxiptu.Disabled = false;
    crmForm.all.new_diferencaemtopxiptu.Disabled = false;
    crmForm.all.new_diferenaemporcentagem.Disabled = false;

    crmForm.all.new_dataprevistaassinatura.Disabled = false;
    crmForm.all.new_dataprevisaorecolhimentoitbi.Disabled = false;
    crmForm.all.new_dataprevistadeinicioregistro.Disabled = false;
    crmForm.all.new_dataprevistadeconclusoregistro.Disabled = false;

    // Transformar caracteres em maiusculos----------------------------------------------------------------//
    var elm = document.getElementsByTagName("input");
    for (i = 0; i < elm.length; i++) {
        if (elm[i].type == "text")
            if (elm[i].DataValue != null) {
            try {
                elm[i].DataValue = elm[i].DataValue.toUpperCase();
            }
            catch (e) { }
        }
    }

} catch (error) {
    alert("Ocorreu um erro no formulário.\n" + error.description);
    event.returnValue = false;
    return false;
}