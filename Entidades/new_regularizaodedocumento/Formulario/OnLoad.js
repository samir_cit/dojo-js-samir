﻿formularioValido = true;
try {
    //GUIA 01 ===CONSULTAS BASICAS===============================

    //Script OnChange Lookup------------------------------------------------

    if (crmForm.all.new_regularizaodedocumentosid.DataValue != null) {
        crmForm.all.new_name.DataValue =
  crmForm.all.new_regularizaodedocumentosid.DataValue[0].name;
    }

    //SEÇAO 01 somar data guia 1 mapeada terreno doc. vendedores---------------------

    if (crmForm.all.new_datamapeamento.DataValue != null) {
        var primeiradata = crmForm.all.new_datamapeamento.DataValue;
        var datademolicao = crmForm.all.new_datamapeamento.DataValue;

        primeiradata.setDate(primeiradata.getDate() + 25);
        crmForm.all.new_dtprevistaincioiptu.DataValue = primeiradata;
        crmForm.all.new_dtprevistainicioluz.DataValue = primeiradata;
        crmForm.all.new_dtprevistainicioagua.DataValue = primeiradata;
        crmForm.all.new_dtprevistainiciomultas.DataValue = primeiradata;


        //SEÇAO 01 somar data guia 1 mapeada terreno doc. vendedores--------------------

        var primeiradata = crmForm.all.new_dtprevistaincioiptu.DataValue;

        primeiradata.setDate(primeiradata.getDate() + 5);
        crmForm.all.new_dtprevistaconclusaoiptu.DataValue = primeiradata;

        var primeiradataluz = crmForm.all.new_dtprevistainicioluz.DataValue;

        primeiradataluz.setDate(primeiradataluz.getDate() + 5);
        crmForm.all.new_dtprevistaconclusaoluz.DataValue = primeiradataluz;

        var primeiradataagua = crmForm.all.new_dtprevistainicioagua.DataValue;

        primeiradataagua.setDate(primeiradataagua.getDate() + 5);
        crmForm.all.new_dtprevistaconclusaoagua.DataValue = primeiradataagua;

        var primeiradatamulta = crmForm.all.new_dtprevistainiciomultas.DataValue;

        primeiradatamulta.setDate(primeiradatamulta.getDate() + 5);
        crmForm.all.new_dtprevistaconclusaomultas.DataValue = primeiradatamulta;
    }

    if (crmForm.all.new_datamapeamento.DataValue != null) {
        var primeiradata = crmForm.all.new_datamapeamento.DataValue;
        var datademolicao = crmForm.all.new_datamapeamento.DataValue;

        // soma data em unificacao de area-----------------------------------------------
        crmForm.all.new_dataprevistadeiniciounificacao.DataValue = primeiradata;

        primeiradata.setDate(primeiradata.getDate() - 5);
        crmForm.all.new_datadaconferencia.DataValue = primeiradata;

        primeiradata.setDate(primeiradata.getDate() + 50);
        crmForm.all.new_dataprevistadeconclusominuta.DataValue = primeiradata;

        primeiradata.setDate(primeiradata.getDate() + 10);
        crmForm.all.new_datadoprotocoloiptu.DataValue = primeiradata;

        primeiradata.setDate(primeiradata.getDate() + 120);
        crmForm.all.new_dataprevistadeconclusoiptu.DataValue = primeiradata;

        // script para calculo do alvara de demlicao -----------------------------
        //data de previsao: 30 dias depois da data de recebimento da doc--
        datademolicao.setDate(datademolicao.getDate() + 30);
        crmForm.all.new_dataprevistadeinicioaldemolicao.DataValue = datademolicao;

        //data de previsao: 30 dias depois da data de inicio---------------------
        datademolicao.setDate(datademolicao.getDate() + 30);
        crmForm.all.new_dataprevistadeconclusoaldemolica.DataValue = datademolicao;
    }

    if (crmForm.all.new_statusdesocupacao.DataValue == false) {
        crmForm.all.new_dataprevistadeiniciodemo.DataValue = crmForm.all.new_dataprevistadeconclusoaldemolica.DataValue;
    }
    else {

        if (crmForm.all.new_dataprevistadeconcluso.DataValue >= crmForm.all.new_dataprevistadeconclusoaldemolica.DataValue) {
            crmForm.all.new_dataprevistadeiniciodemo.DataValue = crmForm.all.new_dataprevistadeconcluso.DataValue;
        }
        else {
            crmForm.all.new_dataprevistadeiniciodemo.DataValue = crmForm.all.new_dataprevistadeconclusoaldemolica.DataValue;
        }
    }

    //========FIM-seçao 1=====================================

    //SEÇÃO 02 - 03 - 04 - 05 - 06 - 07 - se e nulo---------------

    if (crmForm.all.new_dtprevistaincioiptu.DataValue == null) {
        crmForm.all.new_existedebitoiptu.DataValue = false;
    }

    if (crmForm.all.new_dtprevistainicioluz.DataValue == null) {
        crmForm.all.new_existedebitodeluz.DataValue = false;
    }

    if (crmForm.all.new_dtprevistainicioagua.DataValue == null) {
        crmForm.all.new_existedebitodeagua.DataValue = false;
    }

    if (crmForm.all.new_dtprevistainiciomultas.DataValue == null) {
        crmForm.all.new_existedebitodemultas.DataValue = false;
    }

    if (crmForm.all.new_dtprevistainiciohipoteca.DataValue == null) {
        crmForm.all.new_existedebitodehipoteca.DataValue = false;
    }

    if (crmForm.all.new_dtprevistainiciopenhora.DataValue == null) {
        crmForm.all.new_existedebitodepenhora.DataValue = false;
    }

    //script para ocultar as datas
    if (crmForm.all.new_existedebitoiptu.DataValue == true) {
        crmForm.all.new_dtprevistaincioiptu_c.style.visibility = 'visible';
        crmForm.all.new_dtprevistaincioiptu_d.style.visibility = 'visible';

        crmForm.all.new_dtprevistaconclusaoiptu_c.style.visibility = 'visible';
        crmForm.all.new_dtprevistaconclusaoiptu_d.style.visibility = 'visible';

        crmForm.all.new_dtinicioiptu_c.style.visibility = 'visible';
        crmForm.all.new_dtinicioiptu_d.style.visibility = 'visible';

        crmForm.all.new_dtconclusaoiptu_c.style.visibility = 'visible';
        crmForm.all.new_dtconclusaoiptu_d.style.visibility = 'visible';
    }
    else {
        crmForm.all.new_dtprevistaincioiptu_c.style.visibility = 'hidden';
        crmForm.all.new_dtprevistaincioiptu_d.style.visibility = 'hidden';

        crmForm.all.new_dtprevistaconclusaoiptu_c.style.visibility = 'hidden';
        crmForm.all.new_dtprevistaconclusaoiptu_d.style.visibility = 'hidden';

        crmForm.all.new_dtinicioiptu_c.style.visibility = 'hidden';
        crmForm.all.new_dtinicioiptu_d.style.visibility = 'hidden';

        crmForm.all.new_dtconclusaoiptu_c.style.visibility = 'hidden';
        crmForm.all.new_dtconclusaoiptu_d.style.visibility = 'hidden';
    }

    //script para ocultar as datas LUZ--------------------------------------------
    if (crmForm.all.new_existedebitodeluz.DataValue == false) {
        crmForm.all.new_dtprevistainicioluz_c.style.visibility = 'hidden';
        crmForm.all.new_dtprevistainicioluz_d.style.visibility = 'hidden';

        crmForm.all.new_dtprevistaconclusaoluz_c.style.visibility = 'hidden';
        crmForm.all.new_dtprevistaconclusaoluz_d.style.visibility = 'hidden';

        crmForm.all.new_dtinicioluz_c.style.visibility = 'hidden';
        crmForm.all.new_dtinicioluz_d.style.visibility = 'hidden';

        crmForm.all.new_dtconclusaoluz_c.style.visibility = 'hidden';
        crmForm.all.new_dtconclusaoluz_d.style.visibility = 'hidden';
    }
    else {
        crmForm.all.new_dtprevistainicioluz_c.style.visibility = 'visible';
        crmForm.all.new_dtprevistainicioluz_d.style.visibility = 'visible';

        crmForm.all.new_dtprevistaconclusaoluz_c.style.visibility = 'visible';
        crmForm.all.new_dtprevistaconclusaoluz_d.style.visibility = 'visible';

        crmForm.all.new_dtinicioluz_c.style.visibility = 'visible';
        crmForm.all.new_dtinicioluz_d.style.visibility = 'visible';

        crmForm.all.new_dtconclusaoluz_c.style.visibility = 'visible';
        crmForm.all.new_dtconclusaoluz_d.style.visibility = 'visible';
    }

    //script para ocultar as datas AGUA-----------------------------------------
    if (crmForm.all.new_existedebitodeagua.DataValue == false) {
        crmForm.all.new_dtprevistainicioagua_c.style.visibility = 'hidden';
        crmForm.all.new_dtprevistainicioagua_d.style.visibility = 'hidden';

        crmForm.all.new_dtprevistaconclusaoagua_c.style.visibility = 'hidden';
        crmForm.all.new_dtprevistaconclusaoagua_d.style.visibility = 'hidden';

        crmForm.all.new_dtinicioagua_c.style.visibility = 'hidden';
        crmForm.all.new_dtinicioagua_d.style.visibility = 'hidden';

        crmForm.all.new_dtconclusaoagua_c.style.visibility = 'hidden';
        crmForm.all.new_dtconclusaoagua_d.style.visibility = 'hidden';
    }
    else {
        crmForm.all.new_dtprevistainicioagua_c.style.visibility = 'visible';
        crmForm.all.new_dtprevistainicioagua_d.style.visibility = 'visible';

        crmForm.all.new_dtprevistaconclusaoagua_c.style.visibility = 'visible';
        crmForm.all.new_dtprevistaconclusaoagua_d.style.visibility = 'visible';

        crmForm.all.new_dtinicioagua_c.style.visibility = 'visible';
        crmForm.all.new_dtinicioagua_d.style.visibility = 'visible';

        crmForm.all.new_dtconclusaoagua_c.style.visibility = 'visible';
        crmForm.all.new_dtconclusaoagua_d.style.visibility = 'visible';
    }

    //script para ocultar as datas MULTAS---------------------------------------
    if (crmForm.all.new_existedebitodemultas.DataValue == false) {
        crmForm.all.new_dtprevistainiciomultas_c.style.visibility = 'hidden';
        crmForm.all.new_dtprevistainiciomultas_d.style.visibility = 'hidden';

        crmForm.all.new_dtprevistaconclusaomultas_c.style.visibility = 'hidden';
        crmForm.all.new_dtprevistaconclusaomultas_d.style.visibility = 'hidden';

        crmForm.all.new_dtiniciomultas_c.style.visibility = 'hidden';
        crmForm.all.new_dtiniciomultas_d.style.visibility = 'hidden';

        crmForm.all.new_dtconclusaomultas_c.style.visibility = 'hidden';
        crmForm.all.new_dtconclusaomultas_d.style.visibility = 'hidden';
    }
    else {
        crmForm.all.new_dtprevistainiciomultas_c.style.visibility = 'visible';
        crmForm.all.new_dtprevistainiciomultas_d.style.visibility = 'visible';

        crmForm.all.new_dtprevistaconclusaomultas_c.style.visibility = 'visible';
        crmForm.all.new_dtprevistaconclusaomultas_d.style.visibility = 'visible';

        crmForm.all.new_dtiniciomultas_c.style.visibility = 'visible';
        crmForm.all.new_dtiniciomultas_d.style.visibility = 'visible';

        crmForm.all.new_dtconclusaomultas_c.style.visibility = 'visible';
        crmForm.all.new_dtconclusaomultas_d.style.visibility = 'visible';
    }

    //script para ocultar as datas HIPOTECA--------------------------------------
    if (crmForm.all.new_existedebitodehipoteca.DataValue == false) {
        crmForm.all.new_dtprevistainiciohipoteca_c.style.visibility = 'hidden';
        crmForm.all.new_dtprevistainiciohipoteca_d.style.visibility = 'hidden';

        crmForm.all.new_dtprevistaconclusaohipoteca_c.style.visibility = 'hidden';
        crmForm.all.new_dtprevistaconclusaohipoteca_d.style.visibility = 'hidden';

        crmForm.all.new_dtiniciohipoteca_c.style.visibility = 'hidden';
        crmForm.all.new_dtiniciohipoteca_d.style.visibility = 'hidden';

        crmForm.all.new_dtconclusaohipoteca_c.style.visibility = 'hidden';
        crmForm.all.new_dtconclusaohipoteca_d.style.visibility = 'hidden';
    }
    else {
        crmForm.all.new_dtprevistainiciohipoteca_c.style.visibility = 'visible';
        crmForm.all.new_dtprevistainiciohipoteca_d.style.visibility = 'visible';

        crmForm.all.new_dtprevistaconclusaohipoteca_c.style.visibility = 'visible';
        crmForm.all.new_dtprevistaconclusaohipoteca_d.style.visibility = 'visible';

        crmForm.all.new_dtiniciohipoteca_c.style.visibility = 'visible';
        crmForm.all.new_dtiniciohipoteca_d.style.visibility = 'visible';

        crmForm.all.new_dtconclusaohipoteca_c.style.visibility = 'visible';
        crmForm.all.new_dtconclusaohipoteca_d.style.visibility = 'visible';
    }

    //script para ocultar as datas PENHORA---------------------------------------
    if (crmForm.all.new_existedebitodepenhora.DataValue == false) {
        crmForm.all.new_dtprevistainiciopenhora_c.style.visibility = 'hidden';
        crmForm.all.new_dtprevistainiciopenhora_d.style.visibility = 'hidden';

        crmForm.all.new_dtprevistaconclusaopenhora_c.style.visibility = 'hidden';
        crmForm.all.new_dtprevistaconclusaopenhora_d.style.visibility = 'hidden';

        crmForm.all.new_dtiniciopenhora_c.style.visibility = 'hidden';
        crmForm.all.new_dtiniciopenhora_d.style.visibility = 'hidden';

        crmForm.all.new_dtconclusaopenhora_c.style.visibility = 'hidden';
        crmForm.all.new_dtconclusaopenhora_d.style.visibility = 'hidden';
    }
    else {
        crmForm.all.new_dtprevistainiciopenhora_c.style.visibility = 'visible';
        crmForm.all.new_dtprevistainiciopenhora_d.style.visibility = 'visible';

        crmForm.all.new_dtprevistaconclusaopenhora_c.style.visibility = 'visible';
        crmForm.all.new_dtprevistaconclusaopenhora_d.style.visibility = 'visible';

        crmForm.all.new_dtiniciopenhora_c.style.visibility = 'visible';
        crmForm.all.new_dtiniciopenhora_d.style.visibility = 'visible';

        crmForm.all.new_dtconclusaopenhora_c.style.visibility = 'visible';
        crmForm.all.new_dtconclusaopenhora_d.style.visibility = 'visible';
    }


    //GUIA 02 ==============================================

    //SEÇÃO 01 - HABILITAR CAMPO---------------------------------------------------------------------------//
    if (crmForm.all.new_possuiconferenciadearea.DataValue == true) {
        crmForm.all.new_datadaconferencia_c.style.visibility = 'visible';
        crmForm.all.new_datadaconferencia_d.style.visibility = 'visible';
    }
    else {
        crmForm.all.new_datadaconferencia_c.style.visibility = 'hidden';
        crmForm.all.new_datadaconferencia_d.style.visibility = 'hidden';
    }

    //script para ocultar nome do campo e campo------------------------------------------------------------------//
    if (crmForm.all.new_tipodeprocesso.DataValue == 01) {
        document.getElementById("new_status_d").parentNode.parentNode.style.display = "inline";
        document.getElementById("new_statusjudicial_d").parentNode.parentNode.style.display = "none";

        var datasimples = crmForm.all.new_datamapeamento.DataValue;
        datasimples.setDate(datasimples.getDate() + 35); //tive q somar 20 + 15
        crmForm.all.new_dataprevistadeinicioconferencia.DataValue = datasimples;
        datasimples.setDate(datasimples.getDate() + 30);
        crmForm.all.new_dataprevistadeconclusoconferenci.DataValue = datasimples;
    }
    else {
        if (crmForm.all.new_tipodeprocesso.DataValue == 02) {
            document.getElementById("new_statusjudicial_d").parentNode.parentNode.style.display = "inline";
            document.getElementById("new_status_d").parentNode.parentNode.style.display = "none";

            var dataadm = crmForm.all.new_datamapeamento.DataValue;
            dataadm.setDate(dataadm.getDate() + 70); //tive que somar 20+50
            crmForm.all.new_dataprevistadeiniciojudicial.DataValue = dataadm;
            dataadm.setDate(dataadm.getDate() + 180);
            crmForm.all.new_dataprevistadeconclusojudicial.DataValue = dataadm;
        }
        else {
            document.getElementById("new_statusjudicial_d").parentNode.parentNode.style.display = "none";
            document.getElementById("new_status_d").parentNode.parentNode.style.display = "none";
        }
    }

    //GUIA 04 ===============================================

    //SEÇÃO 01 - DEMOLIÇÃO habilitar campos-----------------------------------------------------//

    if (crmForm.all.new_existereaconstruida.DataValue == false) {
        crmForm.all.new_metragenm2_c.style.visibility = 'hidden';
        crmForm.all.new_metragenm2_d.style.visibility = 'hidden';
        crmForm.all.new_reade_c.style.visibility = 'hidden';
        crmForm.all.new_reade_d.style.visibility = 'hidden';
        crmForm.all.new_dataprevistadeiniciominuta_c.style.visibility = 'hidden';
        crmForm.all.new_dataprevistadeiniciominuta_d.style.visibility = 'hidden';
        crmForm.all.new_dataprevistadeconcluso_c.style.visibility = 'hidden';
        crmForm.all.new_dataprevistadeconcluso_d.style.visibility = 'hidden';
        crmForm.all.new_datadeiniciominuta_c.style.visibility = 'hidden';
        crmForm.all.new_datadeiniciominuta_d.style.visibility = 'hidden';
        crmForm.all.new_datadeconclusominuta_c.style.visibility = 'hidden';
        crmForm.all.new_datadeconclusominuta_d.style.visibility = 'hidden';
        crmForm.all.new_dataprevistadeinicioaldemolicao_c.style.visibility = 'hidden';
        crmForm.all.new_dataprevistadeinicioaldemolicao_d.style.visibility = 'hidden';
        crmForm.all.new_dataprevistadeconclusoaldemolica_c.style.visibility = 'hidden';
        crmForm.all.new_dataprevistadeconclusoaldemolica_d.style.visibility = 'hidden';
        crmForm.all.new_datadeinicioaldemolicao_c.style.visibility = 'hidden';
        crmForm.all.new_datadeinicioaldemolicao_d.style.visibility = 'hidden';
        crmForm.all.new_datadeconclusoaldemolicao_c.style.visibility = 'hidden';
        crmForm.all.new_datadeconclusoaldemolicao_d.style.visibility = 'hidden';
        crmForm.all.new_dataprevistadeiniciodemo_c.style.visibility = 'hidden';
        crmForm.all.new_dataprevistadeiniciodemo_d.style.visibility = 'hidden';
        crmForm.all.new_dataprevistadeconclusodemo_c.style.visibility = 'hidden';
        crmForm.all.new_dataprevistadeconclusodemo_d.style.visibility = 'hidden';
        crmForm.all.new_datadeiniciodemo_c.style.visibility = 'hidden';
        crmForm.all.new_datadeiniciodemo_d.style.visibility = 'hidden';
        crmForm.all.new_datadeconclusodemo_c.style.visibility = 'hidden';
        crmForm.all.new_datadeconclusodemo_d.style.visibility = 'hidden';
        crmForm.all.new_dataprevistadeiniciobaixa_c.style.visibility = 'hidden';
        crmForm.all.new_dataprevistadeiniciobaixa_d.style.visibility = 'hidden';
        crmForm.all.new_dataprevistadeconclusobaixa_c.style.visibility = 'hidden';
        crmForm.all.new_dataprevistadeconclusobaixa_d.style.visibility = 'hidden';
        crmForm.all.new_datadeiniciobaixa_c.style.visibility = 'hidden';
        crmForm.all.new_datadeiniciobaixa_d.style.visibility = 'hidden';
        crmForm.all.new_datadeconclusobaixa_c.style.visibility = 'hidden';
        crmForm.all.new_datadeconclusobaixa_d.style.visibility = 'hidden';
        crmForm.all.new_dataprevistadeiniciocnd_c.style.visibility = 'hidden';
        crmForm.all.new_dataprevistadeiniciocnd_d.style.visibility = 'hidden';
        crmForm.all.new_dataprevistadeconclusocnd_c.style.visibility = 'hidden';
        crmForm.all.new_dataprevistadeconclusocnd_d.style.visibility = 'hidden';
        crmForm.all.new_datainiciocnd_c.style.visibility = 'hidden';
        crmForm.all.new_datainiciocnd_d.style.visibility = 'hidden';
        crmForm.all.new_datadeconclusocnd_c.style.visibility = 'hidden';
        crmForm.all.new_datadeconclusocnd_d.style.visibility = 'hidden';
        crmForm.all.new_dataprevistadeinicioaverbacao_c.style.visibility = 'hidden';
        crmForm.all.new_dataprevistadeinicioaverbacao_d.style.visibility = 'hidden';
        crmForm.all.new_dataprevistadeconclusoaverbacao_c.style.visibility = 'hidden';
        crmForm.all.new_dataprevistadeconclusoaverbacao_d.style.visibility = 'hidden';
        crmForm.all.new_datainicioaverbacao_c.style.visibility = 'hidden';
        crmForm.all.new_datainicioaverbacao_d.style.visibility = 'hidden';
        crmForm.all.new_datadeconclusoaverbacao_c.style.visibility = 'hidden';
        crmForm.all.new_datadeconclusoaverbacao_d.style.visibility = 'hidden';
    }
    else {
        crmForm.all.new_metragenm2_c.style.visibility = 'visible';
        crmForm.all.new_metragenm2_d.style.visibility = 'visible';
        crmForm.all.new_reade_c.style.visibility = 'visible';
        crmForm.all.new_reade_d.style.visibility = 'visible';
        crmForm.all.new_dataprevistadeiniciominuta_c.style.visibility = 'visible';
        crmForm.all.new_dataprevistadeiniciominuta_d.style.visibility = 'visible';
        crmForm.all.new_dataprevistadeconcluso_c.style.visibility = 'visible';
        crmForm.all.new_dataprevistadeconcluso_d.style.visibility = 'visible';
        crmForm.all.new_datadeiniciominuta_c.style.visibility = 'visible';
        crmForm.all.new_datadeiniciominuta_d.style.visibility = 'visible';
        crmForm.all.new_datadeconclusominuta_c.style.visibility = 'visible';
        crmForm.all.new_datadeconclusominuta_d.style.visibility = 'visible';
        crmForm.all.new_dataprevistadeinicioaldemolicao_c.style.visibility = 'visible';
        crmForm.all.new_dataprevistadeinicioaldemolicao_d.style.visibility = 'visible';
        crmForm.all.new_dataprevistadeconclusoaldemolica_c.style.visibility = 'visible';
        crmForm.all.new_dataprevistadeconclusoaldemolica_d.style.visibility = 'visible';
        crmForm.all.new_datadeinicioaldemolicao_c.style.visibility = 'visible';
        crmForm.all.new_datadeinicioaldemolicao_d.style.visibility = 'visible';
        crmForm.all.new_datadeconclusoaldemolicao_c.style.visibility = 'visible';
        crmForm.all.new_datadeconclusoaldemolicao_d.style.visibility = 'visible';
        crmForm.all.new_dataprevistadeiniciodemo_c.style.visibility = 'visible';
        crmForm.all.new_dataprevistadeiniciodemo_d.style.visibility = 'visible';
        crmForm.all.new_dataprevistadeconclusodemo_c.style.visibility = 'visible';
        crmForm.all.new_dataprevistadeconclusodemo_d.style.visibility = 'visible';
        crmForm.all.new_datadeiniciodemo_c.style.visibility = 'visible';
        crmForm.all.new_datadeiniciodemo_d.style.visibility = 'visible';
        crmForm.all.new_datadeconclusodemo_c.style.visibility = 'visible';
        crmForm.all.new_datadeconclusodemo_d.style.visibility = 'visible';
        crmForm.all.new_dataprevistadeiniciobaixa_c.style.visibility = 'visible';
        crmForm.all.new_dataprevistadeiniciobaixa_d.style.visibility = 'visible';
        crmForm.all.new_dataprevistadeconclusobaixa_c.style.visibility = 'visible';
        crmForm.all.new_dataprevistadeconclusobaixa_d.style.visibility = 'visible';
        crmForm.all.new_datadeiniciobaixa_c.style.visibility = 'visible';
        crmForm.all.new_datadeiniciobaixa_d.style.visibility = 'visible';
        crmForm.all.new_datadeconclusobaixa_c.style.visibility = 'visible';
        crmForm.all.new_datadeconclusobaixa_d.style.visibility = 'visible';
        crmForm.all.new_dataprevistadeiniciocnd_c.style.visibility = 'visible';
        crmForm.all.new_dataprevistadeiniciocnd_d.style.visibility = 'visible';
        crmForm.all.new_dataprevistadeconclusocnd_c.style.visibility = 'visible';
        crmForm.all.new_dataprevistadeconclusocnd_d.style.visibility = 'visible';
        crmForm.all.new_datainiciocnd_c.style.visibility = 'visible';
        crmForm.all.new_datainiciocnd_d.style.visibility = 'visible';
        crmForm.all.new_datadeconclusocnd_c.style.visibility = 'visible';
        crmForm.all.new_datadeconclusocnd_d.style.visibility = 'visible';
        crmForm.all.new_dataprevistadeinicioaverbacao_c.style.visibility = 'visible';
        crmForm.all.new_dataprevistadeinicioaverbacao_d.style.visibility = 'visible';
        crmForm.all.new_dataprevistadeconclusoaverbacao_c.style.visibility = 'visible';
        crmForm.all.new_dataprevistadeconclusoaverbacao_d.style.visibility = 'visible';
        crmForm.all.new_datainicioaverbacao_c.style.visibility = 'visible';
        crmForm.all.new_datainicioaverbacao_d.style.visibility = 'visible';
        crmForm.all.new_datadeconclusoaverbacao_c.style.visibility = 'visible';
        crmForm.all.new_datadeconclusoaverbacao_d.style.visibility = 'visible';
    }

    //SEÇÃO 01 - ocultar as 2 ultimas seções--------------------------------------

    if (crmForm.all.new_reade.DataValue == true) {
        crmForm.all.new_statuscnd_c.style.visibility = 'hidden';
        crmForm.all.new_statuscnd_d.style.visibility = 'hidden';
        crmForm.all.new_dataprevistadeiniciocnd_c.style.visibility = 'hidden';
        crmForm.all.new_dataprevistadeiniciocnd_d.style.visibility = 'hidden';
        crmForm.all.new_data_validade_cnd_c.style.visibility = 'hidden';
        crmForm.all.new_data_validade_cnd_d.style.visibility = 'hidden';
        crmForm.all.new_dataprevistadeconclusocnd_c.style.visibility = 'hidden';
        crmForm.all.new_dataprevistadeconclusocnd_d.style.visibility = 'hidden';
        crmForm.all.new_datainiciocnd_c.style.visibility = 'hidden';
        crmForm.all.new_datainiciocnd_d.style.visibility = 'hidden';
        crmForm.all.new_datadeconclusocnd_c.style.visibility = 'hidden';
        crmForm.all.new_datadeconclusocnd_d.style.visibility = 'hidden';

        crmForm.all.new_statusbaixademolicao_c.style.visibility = 'hidden';
        crmForm.all.new_statusbaixademolicao_d.style.visibility = 'hidden';
        crmForm.all.new_dataprevistadeinicioaverbacao_c.style.visibility = 'hidden';
        crmForm.all.new_dataprevistadeinicioaverbacao_d.style.visibility = 'hidden';
        crmForm.all.new_dataprevistadeconclusoaverbacao_c.style.visibility = 'hidden';
        crmForm.all.new_dataprevistadeconclusoaverbacao_d.style.visibility = 'hidden';
        crmForm.all.new_datainicioaverbacao_c.style.visibility = 'hidden';
        crmForm.all.new_datainicioaverbacao_d.style.visibility = 'hidden';
        crmForm.all.new_datadeconclusoaverbacao_c.style.visibility = 'hidden';
        crmForm.all.new_datadeconclusoaverbacao_d.style.visibility = 'hidden';
    }
    else {
        crmForm.all.new_statuscnd_c.style.visibility = 'visible';
        crmForm.all.new_statuscnd_d.style.visibility = 'visible';
        crmForm.all.new_dataprevistadeiniciocnd_c.style.visibility = 'visible';
        crmForm.all.new_dataprevistadeiniciocnd_d.style.visibility = 'visible';
        crmForm.all.new_data_validade_cnd_c.style.visibility = 'visible';
        crmForm.all.new_data_validade_cnd_d.style.visibility = 'visible';
        crmForm.all.new_dataprevistadeconclusocnd_c.style.visibility = 'visible';
        crmForm.all.new_dataprevistadeconclusocnd_d.style.visibility = 'visible';
        crmForm.all.new_datainiciocnd_c.style.visibility = 'visible';
        crmForm.all.new_datainiciocnd_d.style.visibility = 'visible';
        crmForm.all.new_datadeconclusocnd_c.style.visibility = 'visible';
        crmForm.all.new_datadeconclusocnd_d.style.visibility = 'visible';

        crmForm.all.new_statusbaixademolicao_c.style.visibility = 'visible';
        crmForm.all.new_statusbaixademolicao_d.style.visibility = 'visible';
        crmForm.all.new_dataprevistadeinicioaverbacao_c.style.visibility = 'visible';
        crmForm.all.new_dataprevistadeinicioaverbacao_d.style.visibility = 'visible';
        crmForm.all.new_dataprevistadeconclusoaverbacao_c.style.visibility = 'visible';
        crmForm.all.new_dataprevistadeconclusoaverbacao_d.style.visibility = 'visible';
        crmForm.all.new_datainicioaverbacao_c.style.visibility = 'visible';
        crmForm.all.new_datainicioaverbacao_d.style.visibility = 'visible';
        crmForm.all.new_datadeconclusoaverbacao_c.style.visibility = 'visible';
        crmForm.all.new_datadeconclusoaverbacao_d.style.visibility = 'visible';
    }

    //script para ocultar as datas-----------------------------------------

    if (crmForm.all.new_statusdesocupacao.DataValue == 2) {
        crmForm.all.new_dataprevistadeiniciominuta_c.style.visibility = 'hidden';
        crmForm.all.new_dataprevistadeiniciominuta_d.style.visibility = 'hidden';

        crmForm.all.new_dataprevistadeconcluso_c.style.visibility = 'hidden';
        crmForm.all.new_dataprevistadeconcluso_d.style.visibility = 'hidden';

        crmForm.all.new_datadeiniciominuta_c.style.visibility = 'hidden';
        crmForm.all.new_datadeiniciominuta_d.style.visibility = 'hidden';

        crmForm.all.new_datadeconclusominuta_c.style.visibility = 'hidden';
        crmForm.all.new_datadeconclusominuta_d.style.visibility = 'hidden';
    }
    else {
        crmForm.all.new_dataprevistadeiniciominuta_c.style.visibility = 'visible';
        crmForm.all.new_dataprevistadeiniciominuta_d.style.visibility = 'visible';

        crmForm.all.new_dataprevistadeconcluso_c.style.visibility = 'visible';
        crmForm.all.new_dataprevistadeconcluso_d.style.visibility = 'visible';

        crmForm.all.new_datadeiniciominuta_c.style.visibility = 'visible';
        crmForm.all.new_datadeiniciominuta_d.style.visibility = 'visible';

        crmForm.all.new_datadeconclusominuta_c.style.visibility = 'visible';
        crmForm.all.new_datadeconclusominuta_d.style.visibility = 'visible';
    }


    //GUIA 05 ===============================================

    //SEÇAO 01 Habilitar datas --------------------------------------------
    if (crmForm.all.new_existeunifiodematricula.DataValue == true) {
        crmForm.all.new_dataprevistadeiniciounificacao_c.style.visibility = 'visible';
        crmForm.all.new_dataprevistadeiniciounificacao_d.style.visibility = 'visible';

        crmForm.all.new_dataprevistadeconclusominuta_c.style.visibility = 'visible';
        crmForm.all.new_dataprevistadeconclusominuta_d.style.visibility = 'visible';

        crmForm.all.new_datadeinicioregistro_c.style.visibility = 'visible';
        crmForm.all.new_datadeinicioregistro_d.style.visibility = 'visible';

        crmForm.all.new_datadeconclusounificacao_c.style.visibility = 'visible';
        crmForm.all.new_datadeconclusounificacao_d.style.visibility = 'visible';
    }
    else {
        crmForm.all.new_dataprevistadeiniciounificacao_c.style.visibility = 'hidden';
        crmForm.all.new_dataprevistadeiniciounificacao_d.style.visibility = 'hidden';

        crmForm.all.new_dataprevistadeconclusominuta_c.style.visibility = 'hidden';
        crmForm.all.new_dataprevistadeconclusominuta_d.style.visibility = 'hidden';

        crmForm.all.new_datadeinicioregistro_c.style.visibility = 'hidden';
        crmForm.all.new_datadeinicioregistro_d.style.visibility = 'hidden';

        crmForm.all.new_datadeconclusounificacao_c.style.visibility = 'hidden';
        crmForm.all.new_datadeconclusounificacao_d.style.visibility = 'hidden';
    }

    //SEÇAO 01 Habilitar datas -----------------------------------------------
    if (crmForm.all.new_existeunificaodeiptu.DataValue == true) {
        crmForm.all.new_datadoprotocoloiptu_c.style.visibility = 'visible';
        crmForm.all.new_datadoprotocoloiptu_d.style.visibility = 'visible';

        crmForm.all.new_dataprevistadeconclusoiptu_c.style.visibility = 'visible';
        crmForm.all.new_dataprevistadeconclusoiptu_d.style.visibility = 'visible';

        crmForm.all.new_datarealdeinicio_c.style.visibility = 'visible';
        crmForm.all.new_datarealdeinicio_d.style.visibility = 'visible';

        crmForm.all.new_datarealdeconcluso_c.style.visibility = 'visible';
        crmForm.all.new_datarealdeconcluso_d.style.visibility = 'visible';
    }
    else {
        crmForm.all.new_datadoprotocoloiptu_c.style.visibility = 'hidden';
        crmForm.all.new_datadoprotocoloiptu_d.style.visibility = 'hidden';

        crmForm.all.new_dataprevistadeconclusoiptu_c.style.visibility = 'hidden';
        crmForm.all.new_dataprevistadeconclusoiptu_d.style.visibility = 'hidden';

        crmForm.all.new_datarealdeinicio_c.style.visibility = 'hidden';
        crmForm.all.new_datarealdeinicio_d.style.visibility = 'hidden';

        crmForm.all.new_datarealdeconcluso_c.style.visibility = 'hidden';
        crmForm.all.new_datarealdeconcluso_d.style.visibility = 'hidden';
    }


    //GUIA 06 ==============================================

    //script para ocultar as datas Aprovação da Minuta de contrato---------------

    if (crmForm.all.new_elaboraodaminuta.DataValue == true) {
        crmForm.all.new_datadorecebimentodaminuta_c.style.visibility = 'visible';
        crmForm.all.new_datadorecebimentodaminuta_d.style.visibility = 'visible';
        crmForm.all.new_dataprevistaelaboracaominuta_c.style.visibility = 'hidden';
        crmForm.all.new_dataprevistaelaboracaominuta_d.style.visibility = 'hidden';
    }
    else {
        crmForm.all.new_datadorecebimentodaminuta_c.style.visibility = 'hidden';
        crmForm.all.new_datadorecebimentodaminuta_d.style.visibility = 'hidden';
        crmForm.all.new_dataprevistaelaboracaominuta_c.style.visibility = 'visible';
        crmForm.all.new_dataprevistaelaboracaominuta_d.style.visibility = 'visible';
    }


    //GUIA 08 ====ADMINISTRAÇÃO================================

    //SEÇÃO 02  Desaparecer o campo de data de mapeamento------
    crmForm.all.new_datamapeamento_c.style.visibility = 'hidden';
    crmForm.all.new_datamapeamento_d.style.visibility = 'hidden';

    //SEÇÃO 03  Desaparecer o campo de data de mapeamento------
    crmForm.all.new_name_c.style.visibility = 'hidden';
    crmForm.all.new_name_d.style.visibility = 'hidden';

    //SEÇÃO 04  Desaparecer o campo de data de mapeamento-------
    crmForm.all.new_dataaquisicaodoterreno_c.style.visibility = 'hidden';
    crmForm.all.new_dataaquisicaodoterreno_d.style.visibility = 'hidden';

    //SEÇÃO 06  Desaparecer o campo de data de mapeamento------
    //crmForm.all.new_mapeamentoregional_c.style.visibility = 'hidden';
    //crmForm.all.new_mapeamentoregional_d.style.visibility = 'hidden';
    crmForm.all.new_regionalid_d.style.display = 'none';
    crmForm.all.new_regionalid_c.style.display = 'none';

    //SEÇÃO 07  Desaparecer o campo de data de mapeamento-------
    crmForm.all.new_retificaomaior_c.style.visibility = 'hidden';
    crmForm.all.new_retificaomaior_d.style.visibility = 'hidden';

    //SEÇÃO 07  Desaparecer o campo de data de mapeamento-------
    crmForm.all.new_unificaomatriculamaior_c.style.visibility = 'hidden';
    crmForm.all.new_unificaomatriculamaior_d.style.visibility = 'hidden';

    //SEÇÃO 07  Desaparecer o campo de data de mapeamento-------
    crmForm.all.new_unificaoiptumaior_c.style.visibility = 'hidden';
    crmForm.all.new_unificaoiptumaior_d.style.visibility = 'hidden';

    //SEÇÃO 07  Desaparecer o campo de data de mapeamento-------
    crmForm.all.new_dataprevistaescrituamaior_c.style.visibility = 'hidden';
    crmForm.all.new_dataprevistaescrituamaior_d.style.visibility = 'hidden';

    //SEÇÃO 07  Desaparecer o campo de data de mapeamento-------
    crmForm.all.new_datamapeamentomaior_c.style.visibility = 'hidden';
    crmForm.all.new_datamapeamentomaior_d.style.visibility = 'hidden';


    //Levar a data de retificacao para a guia administração - campo retificacaomaior ---
    if (crmForm.all.new_tipodeprocesso.DataValue == 1) {
        crmForm.all.new_retificaomaior.DataValue = crmForm.all.new_dataprevistadeconclusoconferenci.DataValue;
    }
    else {
        if (crmForm.all.new_tipodeprocesso.DataValue == 2) {
            crmForm.all.new_retificaomaior.DataValue = crmForm.all.new_dataprevistadeconclusojudicial.DataValue;
        }
        else {
            crmForm.all.new_retificaomaior.DataValue = null;
        }
    }


    //Levar a data de unificacao matricula para a guia administração - campo matriculamaior ---
    if (crmForm.all.new_existeunifiodematricula.DataValue == true) {
        crmForm.all.new_unificaomatriculamaior.DataValue = crmForm.all.new_dataprevistadeconclusominuta.DataValue;
    }


    //Levar a data de unificacao iptu para a guia administração - campo iptumaior ----------------
    if (crmForm.all.new_existeunificaodeiptu.DataValue == true) {
        crmForm.all.new_unificaoiptumaior.DataValue = crmForm.all.new_dataprevistadeconclusoiptu.DataValue;
    }


    //Levar a data de prev.escritura para a guia administração - campo dataprevistaescrituramaior --------------------
    if (crmForm.all.new_datadorecebimentodaminuta.DataValue != null) {
        crmForm.all.new_dataprevistaescrituamaior.DataValue = crmForm.all.new_dataprevistadeconclusoregistro.DataValue;
    }


    //-----comparacao das datas acima------------------
    if ((crmForm.all.new_retificaomaior.DataValue >= crmForm.all.new_unificaomatriculamaior.DataValue) &&
   (crmForm.all.new_retificaomaior.DataValue >= crmForm.all.new_unificaoiptumaior.DataValue) &&
   (crmForm.all.new_retificaomaior.DataValue >= crmForm.all.new_dataprevistaescrituamaior.DataValue)) {
        crmForm.all.new_datamapeamentomaior.DataValue = crmForm.all.new_retificaomaior.DataValue;
    }
    else {
        if ((crmForm.all.new_unificaomatriculamaior.DataValue >= crmForm.all.new_unificaoiptumaior.DataValue) &&
     (crmForm.all.new_unificaomatriculamaior.DataValue >= crmForm.all.new_dataprevistaescrituamaior.DataValue) &&
     (crmForm.all.new_unificaomatriculamaior.DataValue >= crmForm.all.new_retificaomaior.DataValue)) {
            crmForm.all.new_datamapeamentomaior.DataValue = crmForm.all.new_unificaomatriculamaior.DataValue;
        }
        else {
            if ((crmForm.all.new_unificaoiptumaior.DataValue >= crmForm.all.new_retificaomaior.DataValue) &&
       (crmForm.all.new_unificaoiptumaior.DataValue >= crmForm.all.new_unificaomatriculamaior.DataValue) &&
       (crmForm.all.new_unificaoiptumaior.DataValue >= crmForm.all.new_dataprevistaescrituamaior.DataValue)) {
                crmForm.all.new_datamapeamentomaior.DataValue = crmForm.all.new_unificaoiptumaior.DataValue;
            }
            else {
                crmForm.all.new_datamapeamentomaior.DataValue = crmForm.all.new_dataprevistaescrituamaior.DataValue;
            }
        }
    }

    //-------------------------------------------------------------------------------------
    //Desabilitar campos---------------------------------------------------------------
    crmForm.all.new_diferena.Disabled = true;
    crmForm.all.new_diferencaiptu_top.Disabled = true;
    crmForm.all.new_diferencamat_top.Disabled = true;
    crmForm.all.new_diferencaemmatxiptu.Disabled = true;
    crmForm.all.new_diferencaemtopxiptu.Disabled = true;
    crmForm.all.new_diferenaemporcentagem.Disabled = true;

    //--guia6--------------
    crmForm.all.new_dataprevistaassinatura.Disabled = true;
    crmForm.all.new_dataprevisaorecolhimentoitbi.Disabled = true;
    crmForm.all.new_dataprevistadeinicioregistro.Disabled = true;
    crmForm.all.new_dataprevistadeconclusoregistro.Disabled = true;
} catch (error) {
    formularioValido = false;
    alert("Ocorreu um erro no formulário.\n" + error.description);
}