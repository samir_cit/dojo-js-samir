﻿/* 
****************************************************
Arquivo deve ficar no diretório /_static do servidor 
****************************************************
*/

/* CONSTANTES */
/// FormType
TypeUndefined = 0;
TypeCreate = 1;
TypeUpdate = 2;
TypeRead = 3;
TypeDisabled = 4;
TypeQuickCreate = 5;
TypeBulkEdit = 6;

/*CONSTANTES */
/// Events Mode OnSave
EVENT_MODE_ONSAVE_NONE = 0;
EVENT_MODE_ONSAVE_SAVE = 1;
EVENT_MODE_ONSAVE_SAVE_AND_CLOSE = 2;
EVENT_MODE_ONSAVE_DELETE = 3;
EVENT_MODE_ONSAVE_LOAD = 4;
EVENT_MODE_ONSAVE_DEACTIVATE = 5;
EVENT_MODE_ONSAVE_REACTIVATE = 6;
EVENT_MODE_ONSAVE_EMAIL_SEND = 7;
EVENT_MODE_ONSAVE_EMAIL_REPLY = 8;
EVENT_MODE_ONSAVE_EMAIL_FORWARD = 9;
EVENT_MODE_ONSAVE_KB_SUBMIT = 10;
EVENT_MODE_ONSAVE_KB_REJECT = 11;
EVENT_MODE_ONSAVE_KB_PUBLISH = 12;
EVENT_MODE_ONSAVE_KB_UNPUBLISH = 13;
EVENT_MODE_ONSAVE_KB_RATE = 14;
EVENT_MODE_ONSAVE_LEAD_UNQUALIFY = 15;
EVENT_MODE_ONSAVE_LEAD_QUALIFY = 16;
EVENT_MODE_ONSAVE_QUOTE_ACCEPT = 17;
EVENT_MODE_ONSAVE_QUOTE_CREATEORDER = 18;
EVENT_MODE_ONSAVE_ORDER_PROCESSORDER = 19;
EVENT_MODE_ONSAVE_OPPORTUNITY_ADDRELATEDORDER = 21;
EVENT_MODE_ONSAVE_OPPORTUNITY_ADDRELATEDQUOTE = 22;
EVENT_MODE_ONSAVE_OPPORTUNITY_ADDRELATEDINVOICE = 23;
EVENT_MODE_ONSAVE_QUOTE_CREATEREVISION = 24;
EVENT_MODE_ONSAVE_QUOTE_CLOSEQUOTE = 25;
EVENT_MODE_ONSAVE_ORDER_CANCELORDER = 26;
EVENT_MODE_ONSAVE_INVOICE_CLOSE = 27;
EVENT_MODE_ONSAVE_QUOTE_GETPRODUCTS = 28;
EVENT_MODE_ONSAVE_QUOTE_ACTIVATE = 29;
EVENT_MODE_ONSAVE_EMAIL_REPLYALL = 30;
EVENT_MODE_ONSAVE_CONTRACT_HOLD = 31;
EVENT_MODE_ONSAVE_CONTRACT_RELEASEHOLD = 32;
EVENT_MODE_ONSAVE_CONTRACT_CANCEL = 33;
EVENT_MODE_ONSAVE_CONTRACT_RENEW = 34;
EVENT_MODE_ONSAVE_PRODUCT_CONVERTTOKIT = 35;
EVENT_MODE_ONSAVE_PRODUCT_CONVERTFROMKIT = 36;
EVENT_MODE_ONSAVE_CONTRACTDETAIL_CANCEL = 37;
EVENT_MODE_ONSAVE_CONTRACT_INVOICE = 38;
EVENT_MODE_ONSAVE_CONTRACT_CLONE = 39;
EVENT_MODE_ONSAVE_INCIDENT_CANCEL = 40;
EVENT_MODE_ONSAVE_EMAIL_ASSIGN = 41;
EVENT_MODE_ONSAVE_CHANGE_SALESSTAGE = 42;
EVENT_MODE_ONSAVE_SALESORDER_GETPRODUCTS = 43;
EVENT_MODE_ONSAVE_INVOICE_GETPRODUCTS = 44;
EVENT_MODE_ONSAVE_TEMPLATE_MAKEORGAVAILABLE = 45;
EVENT_MODE_ONSAVE_TEMPLATE_MAKEORGUNAVAILABLE = 46;
EVENT_MODE_ONSAVE_ASSIGN = 47;
EVENT_MODE_ONSAVE_INCIDENT_ASSIGNTOUSER = 49;
EVENT_MODE_ONSAVE_ORDER_LOCK = 50;
EVENT_MODE_ONSAVE_ORDER_UNLOCK = 51;
EVENT_MODE_ONSAVE_INVOICE_LOCK = 52;
EVENT_MODE_ONSAVE_INVOICE_UNLOCK = 53;
EVENT_MODE_ONSAVE_CONVERT_RESPONSE = 54;
EVENT_MODE_ONSAVE_SAVE_AND_NEW = 59;
EVENT_MODE_ONSAVE_REPORT_MAKE_ORGAVAILABLE = 60;
EVENT_MODE_ONSAVE_REPORT_MAKE_ORGUNAVAILABLE = 61;
EVENT_MODE_ONSAVE_WORKFLOW_ADD_CHECKSTEP = 62;
EVENT_MODE_ONSAVE_WORKFLOW_UPDATE_CONDITION = 63;
EVENT_MODE_ONSAVE_WORKFLOW_CREATE_ACTION = 64;
EVENT_MODE_ONSAVE_SEND_INVITE = 65;
EVENT_MODE_ONSAVE_WORKFLOW_ADDELSEIF_STEP = 66;
EVENT_MODE_ONSAVE_WORKFLOW_ADDELSE_SEVENT_MODE_ONSAVE_TEP = 67;
EVENT_MODE_ONSAVE_WORKFLOW_DELETE_STEP = 68;

// Códigos definidos no picklist de Estado
EstadoPicklist = {
    AC: 1,
    AL: 2,
    AM: 3,
    AP: 4,
    BA: 5,
    CE: 6,
    DF: 7,
    ES: 8,
    GO: 9,
    MA: 10,
    MG: 11,
    MS: 12,
    MT: 13,
    PA: 14,
    PB: 15,
    PE: 16,
    PI: 17,
    PR: 18,
    RJ: 19,
    RN: 20,
    RO: 21,
    RR: 22,
    RS: 23,
    SC: 24,
    SE: 25,
    SP: 26,
    TO: 27
}

GuidVazio = "00000000-0000-0000-0000-000000000000";

TipoDeOportunidade =
{
    UNIDADE: "1",
    GARAGEM: "2",
    BOX: "3",
    KITACABEMNTO: "4",
    SERVICO: "5",
    LOTE: "6",
    PERMUTA: "7",
    LOJA: "8"
}

Equipe = {
    ADMINISTRADORES: "Administradores",
    ATUALIZACAO_CADASTRAL: "CR - Grupo de atualização cadastral",
    AT_CLIENTE_SAP: "AT – COD CLIENTE SAP",
    COORDENACAO_VENDAS_LANCAMENTO: "Coordenação de Vendas - Lançamento",
    DESENVOLVIMENTO_IMOBILIARIO: "Desenv.Imobiliário",
    CO_REABRE_OPORTUNIDADE: "CO_REABRE_OPORTUNIDADE",
    EMPREENDIMENTO_ESTRATEGIA_NEGOCIACAO: "Empreendimento - Estratégia de Negociação",
    NUCLEO_NOTIFICACAO: "Núcleo - Notificação",
    NUCLEO_NIAC: "Núcleo - NIAC",
    NUCLEO_ENCANTE: "Núcleo - Encante",
    NUCLEO_CONTROLE_ESTRATEGICO: "Núcleo - Controle Estratégico",
    ENGENHARIA_SEQUENCIA_OBRA: "Engenharia - Sequência de Obra",
    ENGENHARIA_PRAZOS: "Engenharia - Prazos",
    ALTERACOES_INFORMACOES_BANCARIAS: "Alterações de Informações Bancárias",
    ELEGIBILIDADE_EMPREENDIMENTO: "CO_EMPREENDIMENTO_ELEGIVEL",
    ALTERACAO_CLIENTE: "Alteração Cliente",
    CO_LIBERACAO_ANALISE_CREDITO: "CO_Liberação_analise_credito",
    CO_PAGAMENTO_RPA: "CO_PAGAMENTO_RPA",
    CO_Elegibilidade_Automatica: "CO_Elegibilidade_Automatica",
	CO_CSG: "CO_CSG",
	CI_PASTA_MAE: "CI_PASTA_MAE",
	CO_ACABAMENTO_BLOCO: "CO_Acabamento_Bloco",
	CR_APOIO_PRODUCAO_BB: "CR_Apoio_Produção_BB",
	AT_PRAZOS_GERENCIAL: "AT_Prazos_Gerencial",
	AT_PRAZOS: "AT_Prazos",
	ENTREGA_CHAVES_BLOCO : "Entrega de chave bloco",
	ENGENHARIA_PREVISAO_ENTREGA: "Engenharia - Previsao Entrega",
	NCE_AUDITORIA_RI: "NCE_Auditoria_RI",
	COM_VENDA_GARANTIDA: "COM_Venda_Garantida",
	COORDENACAO_VENDAS_CONFIGURACOES: "Coordenação de Vendas - Configurações",
	ALTERAR_STATUS_PRODUTO: "Alterar Status Produto",
	EG_CARACTERISTICA_PREMIUM: "EG_CARACTERISTICA_PREMIUM",
	DI_ALTERACAO_PRODUTO: "DI_Alteração_Produto",
	DESENV_IMOBILIARIO: "Desenv.Imobiliário",
	CR_AUDITORIACONTBANCO : "CR_AuditoriaContBanco",
	CR_PARAMETRO_COMERCIAL_DATABASE: "CR_PARAMETRO_COMERCIAL_DATABASE",
	CR_TAXAS: "CR_TAXAS",
	CR_Cancelar_Renegociacao: "CR_Cancelar_Renegociacao",
	NUCLEO_GESTAO_RENEGOCIACOES_CONTROLE_INADI: "Núcleo Gestão de Renegociações e Controle de Inadi",
	CO_PROCESSO_CCB: "CO_Processo_CCB",
	CR_CONTROLE_HIPOTECA: "CR_Controle_Hipoteca",
	MARKETING: "Marketing"
}

TipoProcessamento = {
    ENTREGA_CHAVES: "1",
    LOCAL_INSTALACAO: "2",
    EMAIL_ASSISTENCIA_TECNICA: "3",
    RECLASSIFICACAO_OCORRENCIA_MASSA: "4",
    ELEGIBILIDADE: "5",
    CANCELAMENTO_AGENDAMENTO: "6",
    ENVIAR_OPORTUNIDADE_GANHA_MRV_CREDITO: "7",
    EXCLUSAO_VINCULO_CORRESPONDENTE: "8",
    NIVEL_RETENCAO: "9",
    COMERCIAL_CSG: "10",
    PROCESSAR_ATUALIZACAO_DISTRIBUICAO_EMPREENDIMENTO: "11",
    NOTIFICACAO_ENTREGA_CHAVES_COMPROVA: "12",
    NOTIFICACAO_PENDENCIA_COMPROVA: "13",
    NOTIFICACAO_ANTECIPACAO_COMPROVA: "14",
    NOTIFICACAO_PENDENCIA_DESLIGAMENTO: "15",
    NOTIFICACAO_PENDENCIA_PAGAMENTO: "16",
    CRONOGRAMA_OBRA: "17",
    PREVISAO_TERMINO: "18"
}

TIPOLOGRADOURO = {
    alameda: { value: 1, name: "Alameda", code: "AL" },
    avenida: { value: 2, name: "Avenida", code: "AV" },
    balneario: { value: 3, name: "Balneário", code: "BAL" },
    bloco: { value: 4, name: "Bloco", code: "BL" },
    chacara: { value: 5, name: "Chácara", code: "CH" },
    conjunto: { value: 6, name: "Conjunto", code: "CJ" },
    condominio: { value: 7, name: "Condomínio", code: "COND" },
    estrada: { value: 8, name: "Estrada", code: "EST" },
    fazenda: { value: 9, name: "Fazenda", code: "FAZ" },
    galeria: { value: 10, name: "Galeria", code: "GAL" },
    granja: { value: 11, name: "Granja", code: "GJA" },
    jardim: { value: 12, name: "Jardim", code: "JD" },
    largo: { value: 13, name: "Largo", code: "LG" },
    loteamento: { value: 14, name: "Loteamento", code: "LOT" },
    praca: { value: 15, name: "Praça", code: "PÇ" },
    praia: { value: 16, name: "Praia", code: "PR" },
    parque: { value: 17, name: "Parque", code: "PRQ" },
    quadra: { value: 18, name: "Quadra", code: "Q" },
    rua: { value: 19, name: "Rua", code: "R" },
    setor: { value: 20, name: "Setor", code: "ST" },
    travessa: { value: 21, name: "Travessa", code: "TV" },
    vila: { value: 22, name: "Vila", code: "VL" }
};

crmForm.Recusar = function() {
    if (crmForm.IsValid()) {
        if (crmForm.FormType == TypeUpdate) {
            var winParams = 'dialogWidth:456px;dialogHeight:300px;help:0;status:1;scroll:auto;center:1;resizable:false';
            var args = new Array(crmForm.ObjectId);
            var criadorId = (crmForm.all.createdby.DataValue != null) ? crmForm.all.createdby.DataValue[0].id : "";
            var retorno = showModalDialog('/MRVWeb/Recusa/Default.aspx?iTotal=1&iTypeCode=' + crmForm.ObjectTypeCode + '&iFromForm=1' + '&iCreatedById=' + criadorId, args, winParams);
            if (retorno != undefined) {
                if (retorno.Success) {
                    crmForm.SubmitCrmForm(1, true, true, false);
                }
            }
        }
    }
}

crmForm.TratarRetornoRemoteCommand = function(resultado, nomeMetodo) {
    var retorno = false;
    if (resultado.Success) {
        if (resultado.ReturnValue.Mrv.Error)
            alert(resultado.ReturnValue.Mrv.Error);
        else if (resultado.ReturnValue.Mrv.ErrorSoap)
            alert(resultado.ReturnValue.Mrv.ErrorSoap);
        else
            retorno = true;
    } else {
        alert("Erro na consulta do método '" + nomeMetodo + "'. Contate o suporte.");
    }
    return retorno;
}

/// Adiciona um Lookup customizado ao formulário
crmForm.FilterLookup = function(attribute, objectTypeCode, url, param) {
    if (param != null)
        url += '?' + param;

    oImg = eval('attribute' + '.parentNode.childNodes[0]');

    oImg.onclick = function() {
        retorno = openStdDlg(url, null, 600, 450);

        if (typeof (retorno) != "undefined") {
            strValues = retorno.split('*|*');
            var lookupData = new Array();
            lookupItem = new Object();
            lookupItem.id = "{" + strValues[1] + "}";
            lookupItem.type = objectTypeCode;
            lookupItem.name = strValues[0];
            lookupData[0] = lookupItem;
            attribute.DataValue = lookupData;
            attribute.FireOnChange();
        }
    };
}

crmForm.AddValueLookup = function(Attribute, Id, TypeCode, Name) {
    var lookupData = new Array();
    lookupItem = new Object();
    lookupItem.id = Id;
    lookupItem.type = TypeCode;
    lookupItem.name = Name;
    lookupData[0] = lookupItem;
    Attribute.DataValue = lookupData;
}

/// Desabilita campo no formulário
crmForm.DesabilitarCampos = function(campos, tipo) {
    var tmp = campos.toString().split(";");

    for (var i = 0; i < tmp.length; i++) {
        var atributo = eval("crmForm.all." + tmp[i]);
        if (atributo != null)
            atributo.Disabled = tipo;
    }
}

crmForm.LimpaCampos = function(campos) {
    var tmp = campos.toString().split(";");
    for (var i = 0; i < tmp.length; i++) {
        var atributo = eval("crmForm.all." + tmp[i]);
        if (atributo != null) {
            atributo.ForceSubmit = true;
            atributo.DataValue = null;
        }
    }
}

/// Desabilita ou Habilitar o formulário de acordo com o parâmetro
crmForm.DesabilitarFormulario = function(valor) {
    var todosAtributos = document.getElementsByTagName("label");
    for (var i = 0; i < todosAtributos.length; i++) {
        if (todosAtributos[i].htmlFor != "") {
            var atributo = eval("crmForm.all." + todosAtributos[i].htmlFor.replace("_ledit", ""));
            if (atributo != undefined && atributo != null)
                atributo.Disabled = valor;
        }
    }
}

/// Verifica se o usuário logado é membro da equipe
crmForm.UsuarioPertenceEquipe = function(nomeFuncao) {
    var cmd = new RemoteCommand("MrvService", "UserContainsTeams", "/MrvCustomizations/");
    cmd.SetParameter("teams", nomeFuncao);
    var resultado = cmd.Execute();

    if (resultado.Success)
        return resultado.ReturnValue.Mrv.Found;
    else
        return false;
}

/// Oculta/Mostra campo no formulário
crmForm.EscondeCampo = function(nomeCampo, esconde) {
    var tipo = "";
    if (esconde)
        tipo = "none";

    var atributo_c = eval("crmForm.all." + nomeCampo + "_c");
    var atributo_d = eval("crmForm.all." + nomeCampo + "_d");
    atributo_c.style.display = tipo;
    atributo_d.style.display = tipo;
}

crmForm.EscondeSecao = function(atributo, esconde) {
    var tipo = "";
    if (esconde)
        tipo = "none";
    document.getElementById(atributo + "_d").parentNode.parentNode.style.display = tipo;
}

crmForm.RemoveBotao = function(nome) {
    var lista = document.getElementsByTagName("LI");
    for (i = 0; i < lista.length; i++) {
        if (lista[i].id.indexOf(nome) >= 0) {
            var o = lista[i].parentElement;
            o.removeChild(lista[i]);
            break;
        }
    }
}

crmForm.RemoveBotaoPadraoCRM = function(nome) {
    if (document.getElementById(nome))
        document.getElementById(nome).style.display = "none";
}

crmForm.EscondeAba = function(aba, esconde) {
    var tipo = "";
    if (esconde)
        tipo = "none";
    eval('crmForm.all.' + aba).style.display = tipo;
}

crmForm.QueryString = function(name) {
    name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
    var regexS = "[\\?&]" + name + "=([^&#]*)";
    var regex = new RegExp(regexS);
    var results = regex.exec(window.location.href);
    if (results == null)
        return "";
    else
        return results[1];
}

/// Máscara CPF
crmForm.AddMascara = function(cpf, atributo) {
    var campo = eval("crmForm.all." + atributo);
    if (typeof (cpf) != "undefined" && cpf != null) {
        var sTmp = cpf.replace(/[^0-9]/g, "");
        if (sTmp.length != 11) {
            cpf = '';
            alert("Campo fora da faixa de dígitos sequenciais!");
            campo.SetFocus();
        }
        else {
            if (sTmp.length == 11) {
                campo.DataValue = sTmp.substr(0, 3) + "." + sTmp.substr(3, 3) + "." + sTmp.substr(6, 3) + "-" + sTmp.substr(9, 2);
            }
        }
    }
}

AdicionaNotificacao = function() {
    var objeto = this;

    this.ERRO = "/_imgs/error/notif_icn_crit16.png";
    this.ALERTA = "/_imgs/error/notif_icn_warn16.png";
    this.SUCESSO = "/_imgs/ico/16_L_check.gif";

    objeto.Mostra = function(messagem, imagem) {
        var notificationHTML = '<DIV class="Notification"><TABLE cellSpacing="0" cellPadding="0"><TBODY><TR><TD vAlign="top"><IMG class="ms-crm-Lookup-Item" alt="" src="' + imagem + '" /></TD><TD><SPAN>' + messagem + '</SPAN></TD></TR></TBODY></TABLE></DIV>';
        var notificationsArea = document.getElementById('Notifications');
        if (notificationsArea == null) {
            alert(messagem);
            return;
        }
        notificationsArea.innerHTML = notificationHTML;
        notificationsArea.style.display = 'block';
    }

    objeto.Oculta = function() {
        var notificationsArea = document.getElementById('Notifications');
        if (notificationsArea == null)
            return;
        notificationsArea.innerHTML = '';
        notificationsArea.style.display = 'inline';
    }
}
Notificacao = new AdicionaNotificacao();

function InlineToolbar(atributo) {
    var toolbar = this;
    var container = document.all[atributo];

    if (!container) {
        return alert("Atributo: " + atributo + " não encontrato");
    }

    container.style.display = "none";
    container = container.parentElement;

    toolbar.AddButton = function(id, text, width, callback, imgSrc) {
        var btn = document.createElement("button");
        var btStyle = new StyleBuilder();
        btStyle.Add("font-family", "Arial");
        btStyle.Add("font-size", "12px");
        btStyle.Add("line-height", "16px");
        btStyle.Add("text-align", "center");
        btStyle.Add("cursor", "hand");
        btStyle.Add("border", "1px solid #3366CC");
        btStyle.Add("background-color", "#CEE7FF");
        btStyle.Add("background-image", "url( '/_imgs/btn_rest.gif' )");
        btStyle.Add("background-repeat", "repeat-x");
        btStyle.Add("padding-left", "5px");
        btStyle.Add("padding-right", "5px");
        btStyle.Add("overflow", "visible");
        btStyle.Add("width", width);

        btn.style.cssText = btStyle.ToString();
        btn.attachEvent("onclick", callback);
        btn.id = id;

        if (imgSrc) {
            var img = document.createElement("img");
            img.src = imgSrc;
            img.style.verticalAlign = "middle";
            btn.appendChild(img);
            btn.appendChild(document.createTextNode(" "));
            var spn = document.createElement("span");
            spn.innerText = text;
            btn.appendChild(spn);
        }
        else {
            btn.innerText = text;
        }

        container.appendChild(btn);
        container.appendChild(document.createTextNode(" "));

        return btn;
    }

    toolbar.GetButton = function(id) {
        return document.getElementById(id);
    }

    function StyleBuilder() {
        var cssText = new StringBuilder();
        this.Add = function(key, value) { cssText.Append(key).Append(":").Append(value).Append(";"); }
        this.ToString = function() { return cssText.ToString(); }
    }

    function StringBuilder() {
        var parts = [];
        this.Append = function(text) { parts[parts.length] = text; return this; }
        this.Reset = function() { parts = []; }
        this.ToString = function() { return parts.join(""); }
    }
}

function AddNote() {
    window.open('/' + ORG_UNIQUE_NAME + '/notes/edit.aspx?pId=' + crmForm.ObjectId + '&pType=' + crmForm.ObjectTypeCode + '&refreshgrid=1', 'NovaAnotacao', 'toolbar=no,location=no,directories=no,status=no,menubar=no,resizable=no,width=450px,height=400px');
}

BotaoAjuda = function(nomeCampo) {
    var atributo = this;

    atributo.Field = crmForm.all[nomeCampo];

    if (!atributo.Field)
        return alert("Campo desconhecido: " + nomeCampo);

    atributo.Click = null;
    atributo.Image = new ImagemBotao();
    atributo.Paint = function() {
        var atributo_d = document.all[atributo.Field.id + "_d"];
        if (atributo_d) {
            atributo_d.style.whiteSpace = "nowrap";
            if (atributo_d.hasChildNodes()) {
                for (i = 0; i < atributo_d.childNodes.length; i++) {
                    //Remove as imagens dos atributo
                    if (atributo_d.childNodes[i].nodeName.toUpperCase() == "IMG") {
                        atributo_d.removeChild(atributo_d.childNodes[i]);
                    }
                }
            }
            atributo_d.appendChild(atributo.Image.ToObject())
        }
    }

    atributo.MouseOver = function() {
        event.srcElement.src = atributo.Image.MouseOver;
    }

    atributo.MouseOut = function() {
        event.srcElement.src = atributo.Image.MouseOut;
    }

    function ImagemBotao() {
        this.MouseOut = "";
        this.MouseOver = "";
        this.Width = 21;
        this.Height = 19;
        this.Cursor = "auto";
        this.Title = "";

        this.ToObject = function() {
            var img = document.createElement("IMG");
            img.onmouseover = atributo.MouseOver;
            img.onmouseout = atributo.MouseOut;
            img.onclick = atributo.Click;
            img.src = this.MouseOut;
            img.title = this.Title;

            var cssText = "vertical-align:bottom;";
            cssText += "margin:1px;";
            cssText += "position:relative;";
            cssText += "cursor:" + this.Cursor + ";";
            cssText += "right:" + (this.Width + 1) + "px;";
            cssText += "height:" + (this.Height + 1) + "px";
            img.style.cssText = cssText;
            return img;
        }
    }
}

crmForm.TransformaTodosCaracteresEmMaiusculo = function() {
    var elm = document.getElementsByTagName("input");
    for (i = 0; i < elm.length; i++) {
        if (elm[i].type == "text")
            if (elm[i].DataValue != null) {
            try {
                elm[i].DataValue = elm[i].DataValue.toUpperCase();
            }
            catch (e) { }
        }
    }
}

///Função para desabilitar o formulário 
///Parâmetro para remover botoes padrões do CRM.
crmForm.SomenteProprietario = function(botoesPadroes) {
    if ((crmForm.FormType == TypeUpdate) && (crmForm.all.ownerid.DataValue != null)) {
        var cmd = new RemoteCommand('MrvService', 'ObterUsuarioLogado', '/MRVCustomizations/');
        var result = cmd.Execute();
        if (crmForm.TratarRetornoRemoteCommand(result, "ObterUsuarioLogado")) {
            if (result.ReturnValue.Mrv.UsuarioId.toUpperCase() != crmForm.all.ownerid.DataValue[0].id.toString().toUpperCase()) {
                Notificacao.Mostra('Somente o(a) proprietário(a) da atividade pode alterá-la.', Notificacao.ALERTA);
                crmForm.DesabilitarFormulario(true);
                crmForm.all.ownerid.Disabled = false;
                //Remover botões padrões se houver
                if (botoesPadroes != "") {
                    var btTmp = botoesPadroes.split(';');
                    for (var i = 0; i <= btTmp.length; i++) {
                        crmForm.RemoveBotaoPadraoCRM(btTmp[i]);
                    }
                }
                //remover botões de salvar e fechar.
                crmForm.RemoveBotaoPadraoCRM("_MBcrmFormSave");
                crmForm.RemoveBotaoPadraoCRM("_MBcrmFormSaveAndClose");
                crmForm.RemoveBotaoPadraoCRM("_MBcrmFormSubmitCrmForm59truetruefalse");
            }
        }
        else
            crmForm.DesabilitarFormulario(true);
    }
}

crmForm.GetMes = function(valor) {
    var Retorno = '';
    switch (valor) {
        case 0:
            Retorno = "Janeiro";
            break
        case 1:
            Retorno = "Fevereiro";
            break
        case 2:
            Retorno = "Março";
            break
        case 3:
            Retorno = "Abril";
            break
        case 4:
            Retorno = "Maio";
            break
        case 5:
            Retorno = "Junho";
            break
        case 6:
            Retorno = "Julho";
            break
        case 7:
            Retorno = "Agosto";
            break
        case 8:
            Retorno = "Setembro";
            break
        case 9:
            Retorno = "Outubro";
            break
        case 10:
            Retorno = "Novembro";
            break
        case 11:
            Retorno = "Dezembro";
            break
        default:
            Retorno = "Erro na seleção do Mês.";
    }
    return Retorno;
}

crmForm.ValidaValorTaxaDespachante = function(campoFormaPgto, campoValor, campoInfCalculo) {
    var valor = campoValor.DataValue;
    var condicao = campoFormaPgto.DataValue;
    switch (condicao) {
        case "203":
        case "206":
        case "209":
            if (valor == 800 || valor == 1000) {
                campoFormaPgto.SelectedIndex = 0;
                campoInfCalculo.DataValue = "";
                alert('Pagamentos nos valores de R$800,00 ou R$1.000,00 reais não podem ser feitos nas formas de pagamento (3X, 6X e 9X).');
            }
            break;
        case "207":
            if (valor == 800 || valor == 900 || valor == 1000) {
                campoFormaPgto.SelectedIndex = 0;
                campoInfCalculo.DataValue = "";
                alert('Pagamentos nos valores de R$800,00, R$900,00 ou R$1.000,00 reais não podem ser feitos nas forma de pagamento 7x.');
            }
            break;

        case "201":
        case "202":
        case "204":
        case "205":
        case "208":
        case "210":
            break;

        default:
            campoInfCalculo.DataValue = null;
            campoValor.DataValue = null;
            alert('Valor de taxa inválido!!!. Valores aceitos: R$800,00, R$900,00 ou R$1.000,00 reais.');
            break;
    }
}

String.prototype.format = function() {
    var formatted = this;
    for (i = 0; i < arguments.length; i++) {
        formatted = formatted.replace("{" + i + "}", arguments[i]);
    }
    return formatted;
}

InformacoesDoUsuario = function() {
    var objeto = this;
    this.Fullname = "";
    this.Id = "";
    this.Login = "";
    this.EquipeRelacionamentoNome = "";
    this.EquipeRelacionamentoValue = "";
    this.Equipes;

    //Obter informações.
    var cmd = new RemoteCommand("MrvService", "ObterInformacoesUsuario", "/MRVCustomizations/");
    var result = cmd.Execute();
    if (result.Success) {
        if (result.ReturnValue.Mrv.Error) {
            alert(result.ReturnValue.Mrv.Error);
            crmForm.DesabilitarFormulario();
        } else if (result.ReturnValue.Mrv.ErrorSoap) {
            alert(result.ReturnValue.Mrv.ErrorSoap);
            crmForm.DesabilitarFormularioErro();
        }
        else {
            this.Fullname = result.ReturnValue.Mrv.Fullname;
            this.Id = result.ReturnValue.Mrv.Id;
            this.Login = result.ReturnValue.Mrv.Login;
            this.EquipeRelacionamentoNome = result.ReturnValue.Mrv.EquipeRelacionamentoNome;
            this.EquipeRelacionamentoValue = result.ReturnValue.Mrv.EquipeRelacionamentoValue;

            if (result.ReturnValue.Mrv.Equipes) {
                this.Equipes = result.ReturnValue.Mrv.Equipes.split(';');
            }
        }
    }
    else {
        //Desabilitar todo o formulário.
        alert("Erro na consulta das informações do usuário.");
        window.location.href = window.location.href;
    }

    objeto.PertenceA = function(nomeEquipe) {
        for (var i = 0; i < this.Equipes.length; i++) {
            var temp = this.Equipes[i].split('*');
            if (temp[0].toUpperCase() == nomeEquipe.toUpperCase()) {
                return true;
            }
        }
        return false;
    }
}

crmForm.ConverterFloat = function(valor, casasDecimais) {
    if (casasDecimais > 0) {
        return parseFloat(valor.toFixed(casasDecimais));
    }
    else {
        return parseFloat(valor.toFixed());
    }
}

crmForm.GetElementsByClassName = function (className, tag, elm){
	if (document.getElementsByClassName) {
		getElementsByClassName = function (className, tag, elm) {
			elm = elm || document;
			var elements = elm.getElementsByClassName(className),
				nodeName = (tag)? new RegExp("\\b" + tag + "\\b", "i") : null,
				returnElements = [],
				current;
			for(var i=0, il=elements.length; i<il; i+=1){
				current = elements[i];
				if(!nodeName || nodeName.test(current.nodeName)) {
					returnElements.push(current);
				}
			}
			return returnElements;
		};
	}
	else if (document.evaluate) {
		getElementsByClassName = function (className, tag, elm) {
			tag = tag || "*";
			elm = elm || document;
			var classes = className.split(" "),
				classesToCheck = "",
				xhtmlNamespace = "http://www.w3.org/1999/xhtml",
				namespaceResolver = (document.documentElement.namespaceURI === xhtmlNamespace)? xhtmlNamespace : null,
				returnElements = [],
				elements,
				node;
			for(var j=0, jl=classes.length; j<jl; j+=1){
				classesToCheck += "[contains(concat(' ', @class, ' '), ' " + classes[j] + " ')]";
			}
			try	{
				elements = document.evaluate(".//" + tag + classesToCheck, elm, namespaceResolver, 0, null);
			}
			catch (e) {
				elements = document.evaluate(".//" + tag + classesToCheck, elm, null, 0, null);
			}
			while ((node = elements.iterateNext())) {
				returnElements.push(node);
			}
			return returnElements;
		};
	}
	else {
		getElementsByClassName = function (className, tag, elm) {
			tag = tag || "*";
			elm = elm || document;
			var classes = className.split(" "),
				classesToCheck = [],
				elements = (tag === "*" && elm.all)? elm.all : elm.getElementsByTagName(tag),
				current,
				returnElements = [],
				match;
			for(var k=0, kl=classes.length; k<kl; k+=1){
				classesToCheck.push(new RegExp("(^|\\s)" + classes[k] + "(\\s|$)"));
			}
			for(var l=0, ll=elements.length; l<ll; l+=1){
				current = elements[l];
				match = false;
				for(var m=0, ml=classesToCheck.length; m<ml; m+=1){
					match = classesToCheck[m].test(current.className);
					if (!match) {
						break;
					}
				}
				if (match) {
					returnElements.push(current);
				}
			}
			return returnElements;
		};
	}
	return getElementsByClassName(className, tag, elm);
}
crmForm.VerificarEntidadePai = function(FormPai) {
    return window.opener == null || window.opener.crmForm == undefined || window.opener.crmForm == null;
}
