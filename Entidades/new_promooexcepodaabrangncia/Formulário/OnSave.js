﻿try {
    if (!formularioValido) {
        alert(mensagemErro);
        event.returnValue = false;
        return false;
    }

    if (!ValidaGravacaoExcecao()) {
        var camposAlerta = camposObrigatorios.replace("new_regionalid;", "Regional,");
        camposAlerta = camposAlerta.replace("new_cidadeid;", "Cidade,");
        camposAlerta = camposAlerta.replace("new_empreendimentoid;", "Empreendimento,");
        camposAlerta = camposAlerta.replace("new_blocoid;", "Bloco,");
        camposAlerta = camposAlerta.replace("new_produtoid;", "Produto,");
        camposAlerta = camposAlerta.substr(0, camposAlerta.length - 1);
        alert("É obrigatório informar pelo menos um dos campos: "+ camposAlerta);
        event.returnValue = false;
        return false;
    }

    //Garante a consistência dos dados        
    CompletarDadosExcecao();        

    crmForm.all.new_produtoid.ForceSubmit = true;
    crmForm.all.new_blocoid.ForceSubmit = true;
    crmForm.all.new_empreendimentoid.ForceSubmit = true;
    crmForm.all.new_cidadeid.ForceSubmit = true;
    crmForm.all.new_regionalid.ForceSubmit = true;

    //Atribui um nome para a abrangência
    crmForm.all.new_name.DataValue = DefinirNomeExcecao();
    crmForm.all.new_name.ForceSubmit = true;
} 
catch (error) {
    alert("Ocorreu um erro no formulário.\n" + error.description);
    event.returnValue = false;
    return false;
}
