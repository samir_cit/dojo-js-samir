﻿formularioValido = true;
mensagemErro = "Ocorreu um erro no formulário. Impossível salvar.";
camposObrigatorios = "new_regionalid;new_cidadeid;new_empreendimentoid;new_blocoid;new_produtoid;"
try {
    function loadScript() {
        var oScript = document.createElement('script');
        oScript.type = "text/javascript";
        oScript.src = "/_static/Base.js";

        var oHead = document.getElementsByTagName('head')[0];
        oHead.appendChild(oScript);

        oScript.onreadystatechange = function() {
            if (this.readyState == 'complete' || this.readyState == 'loaded')
                Load();
        }
    }
    loadScript();

    function Load() {
        TravarDadosDaAbrangencia = function() {
            var cmd = new RemoteCommand("MrvService", "RecuperaRastroDaAbrangencia", "/MRVCustomizations/");
            cmd.SetParameter("abrangenciaId", crmForm.all.new_promooabrangnciaid.DataValue[0].id);
            var result = cmd.Execute();
            if (result.Success) {
                if (result.ReturnValue.Mrv.Error) {
                    formularioValido = false;
                    mensagemErro = result.ReturnValue.Mrv.Error;
                } else if (result.ReturnValue.Mrv.ErrorSoap) {
                    formularioValido = false;
                    mensagemErro = result.ReturnValue.Mrv.ErrorSoap;
                }
                else {
                    if (typeof (result.ReturnValue.Mrv.ProdutoId) != "object") {
                        formularioValido = false;
                        mensagemErro = "Não é possível atribuir exceções para a ramificação desta abrangência.";
                    }
                    else {
                        if (typeof (result.ReturnValue.Mrv.RegionalId) != "object") {
                            if (crmForm.all.new_regionalid.DataValue == null) {
                                var lookup = result.ReturnValue.Mrv.RegionalId.split(";");
                                if (lookup.length == 3) {
                                    crmForm.AddValueLookup(crmForm.all.new_regionalid, lookup[0], 10141, lookup[1]);
                                    AtribuirFilterLookup(crmForm.all.new_regionalid, crmForm.all.new_cidadeid, "new_name", "new_regionalid");
                                }
                            }
                            crmForm.DesabilitarCampos("new_regionalid", true);
                            camposObrigatorios = camposObrigatorios.replace("new_regionalid;", "");
                        }

                        if (typeof (result.ReturnValue.Mrv.CidadeId) != "object") {
                            if (crmForm.all.new_cidadeid.DataValue == null) {
                                var lookup = result.ReturnValue.Mrv.CidadeId.split(";");
                                if (lookup.length == 3) {
                                    crmForm.AddValueLookup(crmForm.all.new_cidadeid, lookup[0], 10031, lookup[1]);
                                    AtribuirFilterLookup(crmForm.all.new_cidadeid, crmForm.all.new_empreendimentoid, "new_nome", "new_cidadeid");
                                }
                            }
                            crmForm.DesabilitarCampos("new_cidadeid", true);
                            camposObrigatorios = camposObrigatorios.replace("new_cidadeid;", "");
                        }

                        if (typeof (result.ReturnValue.Mrv.EmpreendimentoId) != "object") {
                            if (crmForm.all.new_empreendimentoid.DataValue == null) {
                                var lookup = result.ReturnValue.Mrv.EmpreendimentoId.split(";");
                                if (lookup.length == 3) {
                                    crmForm.AddValueLookup(crmForm.all.new_empreendimentoid, lookup[0], 10001, lookup[1]);
                                    AtribuirFilterLookup(crmForm.all.new_empreendimentoid, crmForm.all.new_blocoid, "new_name", "new_blocoid");
                                }
                            }
                            crmForm.DesabilitarCampos("new_empreendimentoid", true);
                            camposObrigatorios = camposObrigatorios.replace("new_empreendimentoid;", "");
                        }

                        if (typeof (result.ReturnValue.Mrv.BlocoId) != "object") {
                            if (crmForm.all.new_blocoid.DataValue == null) {
                                var lookup = result.ReturnValue.Mrv.BlocoId.split(";");
                                if (lookup.length == 3) {
                                    crmForm.AddValueLookup(crmForm.all.new_blocoid, lookup[0], 10020, lookup[1]);
                                    AtribuirFilterLookup(crmForm.all.new_blocoid, crmForm.all.new_produtoid, "name", "new_blocosid");
                                }
                            }
                            crmForm.DesabilitarCampos("new_blocoid", true);
                            camposObrigatorios = camposObrigatorios.replace("new_blocoid;", "");
                        }
                    }
                }
            }
        }

        CompletarDadosExcecao = function() {
            var produtoId = crmForm.all.new_produtoid.DataValue == null ? "" : crmForm.all.new_produtoid.DataValue[0].id;
            var blocoId = crmForm.all.new_blocoid.DataValue == null ? "" : crmForm.all.new_blocoid.DataValue[0].id;
            var empreendimentoId = crmForm.all.new_empreendimentoid.DataValue == null ? "" : crmForm.all.new_empreendimentoid.DataValue[0].id;
            var cidadeId = crmForm.all.new_cidadeid.DataValue == null ? "" : crmForm.all.new_cidadeid.DataValue[0].id;
            var regionalId = crmForm.all.new_regionalid.DataValue == null ? "" : crmForm.all.new_regionalid.DataValue[0].id;

            if (produtoId != "") {
                blocoId = ""; empreendimentoId = ""; cidadeId = ""; regionalId = "";
            }
            else if (blocoId != "") {
                empreendimentoId = ""; cidadeId = ""; regionalId = "";
            }
            else if (empreendimentoId != "") {
                cidadeId = ""; regionalId = "";
            }
            else if (cidadeId != "") {
                regionalId = "";
            }

            var cmd = new RemoteCommand("MrvService", "CompletarDadosAbrangencia", "/MRVCustomizations/");
            cmd.SetParameter("regionalId", regionalId);
            cmd.SetParameter("cidadeId", cidadeId);
            cmd.SetParameter("empreendimentoId", empreendimentoId);
            cmd.SetParameter("blocoId", blocoId);
            cmd.SetParameter("produtoId", produtoId);

            var result = cmd.Execute();
            if (result.Success) {
                if (result.ReturnValue.Mrv.Error) {
                    formularioValido = false;
                    mensagemErro = result.ReturnValue.Mrv.Error;
                } else if (result.ReturnValue.Mrv.ErrorSoap) {
                    formularioValido = false;
                    mensagemErro = result.ReturnValue.Mrv.ErrorSoap;
                }
                else {
                    if (!crmForm.all.new_regionalid.disabled && typeof (result.ReturnValue.Mrv.RegionalId) != "object") {
                        var lookup = result.ReturnValue.Mrv.RegionalId.split(";");
                        if (lookup.length == 3) {
                            crmForm.AddValueLookup(crmForm.all.new_regionalid, lookup[0], 10141, lookup[1]);
                            AtribuirFilterLookup(crmForm.all.new_regionalid, crmForm.all.new_cidadeid, "new_name", "new_regionalid");
                        }
                    }

                    if (!crmForm.all.new_cidadeid.disabled && typeof (result.ReturnValue.Mrv.CidadeId) != "object") {
                        var lookup = result.ReturnValue.Mrv.CidadeId.split(";");
                        if (lookup.length == 3) {
                            crmForm.AddValueLookup(crmForm.all.new_cidadeid, lookup[0], 10031, lookup[1]);
                            AtribuirFilterLookup(crmForm.all.new_cidadeid, crmForm.all.new_empreendimentoid, "new_nome", "new_cidadeid");
                        }
                    }

                    if (!crmForm.all.new_empreendimentoid.disabled && typeof (result.ReturnValue.Mrv.EmpreendimentoId) != "object") {
                        var lookup = result.ReturnValue.Mrv.EmpreendimentoId.split(";");
                        if (lookup.length == 3) {
                            crmForm.AddValueLookup(crmForm.all.new_empreendimentoid, lookup[0], 10001, lookup[1]);
                            AtribuirFilterLookup(crmForm.all.new_empreendimentoid, crmForm.all.new_blocoid, "new_name", "new_blocoid");
                        }
                    }

                    if (!crmForm.all.new_blocoid.disabled && typeof (result.ReturnValue.Mrv.BlocoId) != "object") {
                        var lookup = result.ReturnValue.Mrv.BlocoId.split(";");
                        if (lookup.length == 3) {
                            crmForm.AddValueLookup(crmForm.all.new_blocoid, lookup[0], 10020, lookup[1]);
                            AtribuirFilterLookup(crmForm.all.new_blocoid, crmForm.all.new_produtoid, "name", "new_blocosid");
                        }
                    }
                }
            }
        }

        DefinirNomeExcecao = function() {
            if (crmForm.all.new_produtoid.DataValue != null)
                return crmForm.all.new_produtoid.DataValue[0].name;

            if (crmForm.all.new_blocoid.DataValue != null)
                return "{0} - {1}".format(
                    crmForm.all.new_empreendimentoid.DataValue == null ? "" : crmForm.all.new_empreendimentoid.DataValue[0].name,
                    crmForm.all.new_blocoid.DataValue[0].name);

            if (crmForm.all.new_empreendimentoid.DataValue != null)
                return crmForm.all.new_empreendimentoid.DataValue[0].name;

            if (crmForm.all.new_cidadeid.DataValue != null)
                return crmForm.all.new_cidadeid.DataValue[0].name;

            if (crmForm.all.new_regionalid.DataValue != null)
                return "Regional: {0}".format(crmForm.all.new_regionalid.DataValue[0].name);
        }

        AtribuirFilterLookup = function(objectSrc, objectTo, attributeSearch, attributeFilter) {
            if (objectTo.Disabled == true) return;

            var filterValue = objectSrc.DataValue != null ? objectSrc.DataValue[0].id : "";
            filterValue = filterValue.substr(1, filterValue.length - 2);

            var typeCode = "";
            var returnCode = 0;
            switch (attributeFilter) {
                case "new_regionalid":
                    typeCode = filterValue != "" ? "10031" : "100312";
                    returnCode = 10031;
                    break;
                case "new_cidadeid":
                    typeCode = filterValue != "" ? "10001" : "100012";
                    returnCode = 10001;
                    break;
                case "new_blocoid":
                    typeCode = filterValue != "" ? "10020" : "100202";
                    returnCode = 10020;
                    break;
                case "new_blocosid":
                    typeCode = filterValue != "" ? "1024002" : "10240022";
                    returnCode = 1024;
                    break;
            }

            var oParam = "objectTypeCode=" + typeCode + "&filterDefault=false&attributesearch=" + attributeSearch + "&_" + attributeFilter + "=" + filterValue;
            crmForm.FilterLookup(objectTo, returnCode, "/MRVWeb/FilterLookup/FilterLookup.aspx", oParam);
        }

        DefinieLookupsPersonalizados = function() {
            AtribuirFilterLookup(crmForm.all.new_regionalid, crmForm.all.new_cidadeid, "new_name", "new_regionalid");
            AtribuirFilterLookup(crmForm.all.new_cidadeid, crmForm.all.new_empreendimentoid, "new_nome", "new_cidadeid");
            AtribuirFilterLookup(crmForm.all.new_empreendimentoid, crmForm.all.new_blocoid, "new_name", "new_blocoid");
            AtribuirFilterLookup(crmForm.all.new_blocoid, crmForm.all.new_produtoid, "name", "new_blocosid");
        }

        ValidaGravacaoExcecao = function() {
            var tmp = camposObrigatorios.toString().split(";");

            if (tmp.length == 0) return true;

            for (var i = 0; i < tmp.length - 1; i++) {
                var atributo = eval("crmForm.all." + tmp[i]);
                if (atributo.DataValue != null) return true;
            }
            
            return false;
        }

        //Inicio
        if (crmForm.FormType == TypeCreate && crmForm.all.new_promooabrangnciaid.DataValue == null) {
            formularioValido = false;
            mensagemErro = "Para incluir exceções deve-se utilizar o botão \"Novo Exceção\" do formulario de exceções.";
            alert(mensagemErro);
            window.close();
        }
        else {
            TravarDadosDaAbrangencia();
            if (formularioValido)
                DefinieLookupsPersonalizados();
            else {
                var camposBloqueados = ["new_produtoid", "new_blocoid", "new_empreendimentoid", "new_cidadeid", "new_regionalid"];
                crmForm.DesabilitarCampos(camposBloqueados.join(";"), true);
                alert(mensagemErro);
            }
        }
    }
}
catch (error) {
    formularioValido = false;
    alert("Ocorreu um erro no formulário.\n" + error.description);
}
