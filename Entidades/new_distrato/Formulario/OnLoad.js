﻿formularioValido = true;

try {
		SatusCode = {
			RASCUNHO : "1",
			DISTRATO_EFETUADO : "3"
		}

		TipoDeContrato = {
			UNIDADE : "1",
			GARAGEM : "2",
			BOX : "3",
			KITACABEMNTO : "4",
			SERVICO : "5",
			PERMUTA : "7"
		}

		function EsconderMenuLateral() {
			if (document.getElementById("nav_new_new_distrato_new_reembolso_aditivo_filho"))
				document.getElementById("nav_new_new_distrato_new_reembolso_aditivo_filho").style.display = "none";
		}

		function PreencherInformacoesFormularioPrincipal(oResult) {
			crmForm.all.new_imovelconcluido.DataValue = oResult.ReturnValue.Mrv.ImovelConcluido;
			crmForm.all.new_possuikit.DataValue = oResult.ReturnValue.Mrv.PossuiKits;
			crmForm.all.new_possuibox.DataValue = oResult.ReturnValue.Mrv.PossuiBoxes;
			crmForm.all.new_possuigaragem.DataValue = oResult.ReturnValue.Mrv.PossuiGaragens;

			if (oResult.ReturnValue.Mrv.ProdutoNome && oResult.ReturnValue.Mrv.ProdutoId) {
				var lookupData = new Array();
				var lookupItem = new Object();

				lookupItem.id = oResult.ReturnValue.Mrv.ProdutoId;
				lookupItem.type = 1024;

				lookupItem.name = oResult.ReturnValue.Mrv.ProdutoNome
					lookupData[0] = lookupItem;
				crmForm.all.new_produtoid.DataValue = lookupData;
				new_produtoid_onchange0();
			}
		}

		crmForm.ObterInformacoesDoContrato = function (contratoId) {
			var rCmd = new RemoteCommand("DistratoService", "ObterInformacoesContratoParaDistrato", "/MRVCustomizations/DistratoRenegociacao/");
			rCmd.SetParameter("contratoId", contratoId);
			retorno = rCmd.Execute();

			if (crmForm.TratarRetornoRemoteCommand(retorno, "ObterInformacoesContratoParaDistrato")) {
				switch (retorno.ReturnValue.Mrv.TipoContrato) {
				case 1:
					PreencherInformacoesFormularioPrincipal(retorno);
					break;
				case 4:
					if (retorno.ReturnValue.Mrv.ExistemKitsNaoComercializaveis) {
						crmForm.all.new_contratoid.DataValue = null;
						crmForm.all.new_numero_contrato_legado.DataValue = null;
						alert('Existem Kits fora do prazo permitido para Distrato. Operação cancelada.');
						return;
					}
					PreencherInformacoesFormularioPrincipal(retorno);
					break;
				}
			}
		}

		crmForm.ObterInformacoesDaOportunidade = function (oportunidadeId) {
			var rCmd = new RemoteCommand("DistratoService", "ObterInformacoesProdutoParaDistrato", "/MRVCustomizations/DistratoRenegociacao/");
			rCmd.SetParameter("oportunidadeId", oportunidadeId);
			retorno = rCmd.Execute();

			if (crmForm.TratarRetornoRemoteCommand(retorno, "ObterInformacoesProdutoParaDistrato")) {
				if (retorno.ReturnValue.Mrv.ProdutoNome && retorno.ReturnValue.Mrv.ProdutoId) {
					var lookupData = new Array();
					var lookupItem = new Object();

					lookupItem.id = retorno.ReturnValue.Mrv.ProdutoId;
					lookupItem.type = 1024;

					lookupItem.name = retorno.ReturnValue.Mrv.ProdutoNome
						lookupData[0] = lookupItem;
					crmForm.all.new_produtoid.DataValue = lookupData;
					new_produtoid_onchange0();
				}
			}
		}

		crmForm.ApresentaTabRelacionamento = function () {
			if (!crmForm.ValidarSap45()) {
				var url = "";

				if (crmForm.all.new_contratoid.DataValue) {
					/// Obtém a oportunidade de venda do contrato para que sejam listados os relacionamentos do cliente na oportunidade de venda.
					var contrato = crmForm.ObterContratoQueEstaSendoRenegociado(crmForm.all.new_contratoid.DataValue[0].id);

					//// TODO: Alterar os parametros oType e security para os valores do objeto no ambiente de produção.
					url = "/sfa/opps/areas.aspx?oId=%7b" + contrato.new_opportunityid.replace('{', '').replace('}', '') + "%7d&oType=3&security=852023&tabSet=new_opportunity_new_relacoes_do_cliente";
				} else
					alert('Por favor, preencha o contrato que será distratado.');

				document.getElementById("IFRAME_Relacionamentos").src = url;

				/// Tab Relacionamento Do Cliente
				crmForm.all.tab2Tab.style.display = "block";
			}
		}

		crmForm.ObterContratoQueEstaSendoRenegociado = function (contratoId) {
			var rCmd = new RemoteCommand('DistratoRenegociacaoService', 'ObterContratoQueEstaSendoRenegociado', '/MRVCustomizations/DistratoRenegociacao/');
			rCmd.SetParameter('contratoId', contratoId);

			var retorno = rCmd.Execute();

			if (crmForm.TratarRetornoRemoteCommand(retorno, "ObterContratoQueEstaSendoRenegociado")) {
				if (retorno.ReturnValue.Mrv.Encontrou)
					return retorno.ReturnValue.Mrv;
			}

			return null;
		}

		var IsEquipeContrato = null;
		crmForm.EquipeContrato = function () {
			if (IsEquipeContrato == null) {
				IsEquipeContrato = crmForm.UsuarioPertenceEquipe("contrato");
			}
			return IsEquipeContrato;
		}

		var oScript = document.createElement("script");
		oScript.src = "/_static/_grid/cmds/util.js";
		document.appendChild(oScript);

		crmForm.DesabilitarCamposCreate = function () {
			crmForm.all.new_motivo_da_ordem.Disabled = true;
			crmForm.all.new_name.Disabled = true;
			crmForm.all.new_numero_compensao_sap6.Disabled = true;
			crmForm.all.new_numero_distrato_sap6.Disabled = true;
			crmForm.all.new_origemdasolicitacao.Disabled = true;
			crmForm.all.new_possuibox.Disabled = true;
			crmForm.all.new_possuigaragem.Disabled = true;
			crmForm.all.new_possuikit.Disabled = true;
			crmForm.all.new_quantidade_parcelas1.Disabled = true;
			crmForm.all.new_quantidade_parcelas2.Disabled = true;
			crmForm.all.new_quantidade_parcelas3.Disabled = true;
			crmForm.all.new_quantidade_parcelas4.Disabled = true;
			crmForm.all.new_tipodecontrato.Disabled = true;
			crmForm.all.new_valor_do_reembolso.Disabled = true;
			crmForm.all.new_valor_parcela1.Disabled = true;
			crmForm.all.new_valor_parcela2.Disabled = true;
			crmForm.all.new_valor_parcela3.Disabled = true;
			crmForm.all.new_valor_parcela4.Disabled = true;
			crmForm.all.new_vencimento_1a_parcela1.Disabled = true;
			crmForm.all.new_vencimento_1a_parcela2.Disabled = true;
			crmForm.all.new_vencimento_1a_parcela3.Disabled = true;
			crmForm.all.new_vencimento_1a_parcela4.Disabled = true;
			crmForm.all.statuscode.Disabled = true;
			crmForm.all.new_tipo_renegociacaoid.Disabled = true;
			crmForm.all.new_clienteid.Disabled = true;
			crmForm.all.new_contratoid.Disabled = true;
			crmForm.all.new_aditivoid.Disabled = true;
			crmForm.all.new_datadocumento.Disabled = true;
			crmForm.all.new_oportunidadeid.Disabled = true;
			crmForm.all.new_carta_credito.Disabled = true;
			crmForm.all.new_data_termo_extincao.Disabled = true;
		}

		crmForm.CalcularValorReembolso = function () {
			var valorReembolso = 0;
			//linha 1
			if (crmForm.all.new_quantidade_parcelas1.DataValue != null && crmForm.all.new_valor_parcela1.DataValue != null && crmForm.all.new_vencimento_1a_parcela1.DataValue != null) {
				valorReembolso += crmForm.all.new_quantidade_parcelas1.DataValue * crmForm.all.new_valor_parcela1.DataValue;
			}
			//linha 2
			if (crmForm.all.new_quantidade_parcelas2.DataValue != null && crmForm.all.new_valor_parcela2.DataValue != null && crmForm.all.new_vencimento_1a_parcela2.DataValue != null) {
				valorReembolso += crmForm.all.new_quantidade_parcelas2.DataValue * crmForm.all.new_valor_parcela2.DataValue;
			}
			//linha 3
			if (crmForm.all.new_quantidade_parcelas3.DataValue != null && crmForm.all.new_valor_parcela3.DataValue != null && crmForm.all.new_vencimento_1a_parcela3.DataValue != null) {
				valorReembolso += crmForm.all.new_quantidade_parcelas3.DataValue * crmForm.all.new_valor_parcela3.DataValue;
			}
			//linha 4
			if (crmForm.all.new_quantidade_parcelas4.DataValue != null && crmForm.all.new_valor_parcela4.DataValue != null && crmForm.all.new_vencimento_1a_parcela4.DataValue != null) {
				valorReembolso += crmForm.all.new_quantidade_parcelas4.DataValue * crmForm.all.new_valor_parcela4.DataValue;
			}

			crmForm.all.new_valor_do_reembolso.DataValue = valorReembolso;
			crmForm.all.new_valor_do_reembolso.ForceSubmit = true;
		}

		/// Verificar se existe aditivos para o contrato. Se existir, abrir janela para adicionar valores de reembolso
		crmForm.ConsultarExisteAditivosFilhos = function () {
			if (!crmForm.ValidarSap45()) {

				var retorno = new Object();
				retorno.PossuiAditivosFilhos = false;
				retorno.CadastroReembolsoAditivosFilhosFeito = false;

				var rCmd = new RemoteCommand("DistratoService", "ExisteAditivosFilhos", "/MRVCustomizations/DistratoRenegociacao/");
				rCmd.SetParameter("distratoId", crmForm.ObjectId);
				var resultado = rCmd.Execute();

				if (crmForm.TratarRetornoRemoteCommand(resultado, "ExisteAditivosFilhos")) {
					if (resultado.ReturnValue.Mrv.Sucesso) {
						retorno.PossuiAditivosFilhos = resultado.ReturnValue.Mrv.PossuiAditivosFilhos;
						retorno.CadastroReembolsoAditivosFilhosFeito = resultado.ReturnValue.Mrv.CadastroReembolsoAditivosFilhosFeito;
						return retorno;
					} else {
						alert(resultado.ReturnValue.Mrv.Mensagem);
						return null;
					}
				} else {
					alert("Erro na consulta do método ExisteAditivosFilhos. Contate o Administrador.");
					return null;
				}
			} else {
				return null;
			}
		}
		crmForm.ExisteAditivosFilhos = function() {
		    var verificaFilhos = crmForm.all.new_tipodecontrato.DataValue == TipoDeContrato.UNIDADE || crmForm.all.new_tipodecontrato.DataValue == TipoDeContrato.PERMUTA;
		    if ((crmForm.all.new_tipodecontrato.DataValue != null && verificaFilhos) &&
				crmForm.all.statuscode.DataValue == SatusCode.RASCUNHO) {
		        var consulta = crmForm.ConsultarExisteAditivosFilhos();

		        if (consulta != null) {
		            if (!consulta.PossuiAditivosFilhos)
		                return false;
		            if (consulta.CadastroReembolsoAditivosFilhosFeito)
		                return false;

		            /// Abrir janela para criar registros de reembolso
		            if (window.confirm("Contrato possui aditivos filhos. Deseja definir valores para reembolso para os aditivos?")) {
		                var retorno = window.showModalDialog('/MRVWEB/DistratoRenegociacao/distratarfilhos.aspx?distratoid=' + crmForm.ObjectId, '', 'dialogHeight: 300px;dialogWidth:800px');
		                if (typeof (retorno) != 'undefined') {
		                    if (retorno.Sucesso == true)
		                        alert('Informações sobre reembolso dos aditivos filhos foram cadastradas com sucesso!');
		                    else
		                        alert('Ação cancelada.');
		                } else
		                    alert('Ação não finalizada.');
		            }
		        }
		    }
		}

		function loadScript() {
			var oScript = document.createElement('script');
			oScript.type = "text/javascript";
			oScript.src = "/_static/Base.js";

			var oHead = document.getElementsByTagName('head')[0];
			oHead.appendChild(oScript);

			oScript.onreadystatechange = function () {
				if (this.readyState == 'complete' || this.readyState == 'loaded')
					Load();
			}
		}

		loadScript();

		function Load() {
			crmForm.EscondeCampo("new_oportunidadeid", true);
			crmForm.SetFieldReqLevel("new_oportunidadeid", false);

			//Se a entidade distrato for aberta através da entidade incident
			if (window.opener.opener && crmForm.FormType == TypeCreate && window.opener.crmForm.ObjectTypeCode == 112 && window.opener.crmForm) {

				//Busca cliente da ocorrência
				if (window.opener.crmForm.customerid.DataValue != null) {
					var idCliente = window.opener.crmForm.customerid.DataValue[0].id;
					var nomeCliente = window.opener.crmForm.customerid.DataValue[0].name;
					var typeCode = window.opener.crmForm.customerid.DataValue[0].type;
					crmForm.AddValueLookup(crmForm.all.new_clienteid, idCliente, typeCode, nomeCliente);
				}
				crmForm.all.new_clienteid.FireOnChange();

				//Busca contrato e tipo de contrato da oportunidade da Ocorrência
				if (window.opener.crmForm.all.new_opportunityid.DataValue != null) {

					//Contrato
					var cmd = new RemoteCommand("MrvService", "ObterIdContratoPorOportunidade", "/MRVCustomizations/");
					cmd.SetParameter("oportunidadeId", window.opener.crmForm.all.new_opportunityid.DataValue[0].id);
					var resultado = cmd.Execute();

					if (crmForm.TratarRetornoRemoteCommand(resultado, "ObterIdContratoPorOportunidade"))
						if (resultado.ReturnValue.Mrv.contractId != '' && resultado.ReturnValue.Mrv.contractName != '') {
							crmForm.AddValueLookup(crmForm.all.new_contratoid, resultado.ReturnValue.Mrv.contractId, '1010', resultado.ReturnValue.Mrv.contractName);

							crmForm.ApresentaTabRelacionamento();
							crmForm.ObterInformacoesDoContrato(resultado.ReturnValue.Mrv.contractId);
							crmForm.all.new_name.DataValue = 'DISTRATO - ' + resultado.ReturnValue.Mrv.contractName;
							crmForm.all.new_aditivoid.DataValue = null;
						}

					//Tipo de Contrato
					cmd = new RemoteCommand("MrvService", "ObterTipoDoContratoDaOportunidade", "/MRVCustomizations/");
					cmd.SetParameter("oportunidadeId", window.opener.crmForm.all.new_opportunityid.DataValue[0].id);
					resultado = cmd.Execute();

					if (crmForm.TratarRetornoRemoteCommand(resultado, "ObterIdContratoPorOportunidade"))
						if (resultado.ReturnValue.Mrv.TipoContrato != undefined)
							crmForm.all.new_tipodecontrato.DataValue = resultado.ReturnValue.Mrv.TipoContrato;
				}
			}

			crmForm.all.statuscode.Disabled = true;

			if (crmForm.ValidarSap45()) {
				crmForm.SetFieldReqLevel("new_contratoid", false);
				crmForm.SetFieldReqLevel("new_aditivoid", false);
				crmForm.all.new_contratoid.Disabled = true;
			}

			/// Tab Relacionamento Do Cliente
			crmForm.all.tab2Tab.style.display = "none";

			if (crmForm.all.new_valor_do_reembolso.DataValue == null)
				crmForm.all.new_valor_do_reembolso.DataValue = 0;

            var oTypeCode = "10132";
            var oParam = "objectTypeCode=10106&filterDefault=false&attributesearch=new_name";
            crmForm.FilterLookup(crmForm.all.new_tipo_renegociacaoid, oTypeCode, "/MRVWeb/FilterLookup/FilterLookup.aspx", oParam);

			crmForm.all.new_oportunidadeid.FireOnChange();
			crmForm.all.new_contratoid.FireOnChange();
			crmForm.all.new_aditivoid.FireOnChange();
			crmForm.all.new_valor_do_reembolso.FireOnChange();

			if (crmForm.all.new_aditivoid.DataValue != null)
				crmForm.all.IFRAME_saldo.src = "/mrvweb/saldo/saldo.aspx?CodigoAditivo=" + crmForm.all.new_aditivoid.DataValue[0].id;

			if (crmForm.FormType == 1) {
				crmForm.all.new_datadocumento.DataValue = new Date();
				crmForm.all.new_imovelconcluido.DataValue = null;
				crmForm.all.new_possuikit.DataValue = null;
				crmForm.all.new_possuibox.DataValue = null;
				crmForm.all.new_possuigaragem.DataValue = null;
			} else {
				crmForm.ApresentaTabRelacionamento();

				if (crmForm.all.new_aditivoid.DataValue != null)
				{
					crmForm.all.IFRAME_saldo.src = "/mrvweb/saldo/saldo.aspx?CodigoAditivo=" + crmForm.all.new_aditivoid.DataValue[0].id;
				}
			}

			if (crmForm.all.statuscode.DataValue != SatusCode.RASCUNHO)
			{
				crmForm.DesabilitarCamposCreate();
			}
			
			//DETS 487055: Melhorias no módulo de renegociação e distrato: Item 2.1.1.1
			//else
			//crmForm.all.new_datadocumento.Disabled = !crmForm.EquipeContrato();

			if (crmForm.all.new_tipodecontrato.DataValue == null || crmForm.all.new_tipodecontrato.DataValue != TipoDeContrato.UNIDADE)
			{
				EsconderMenuLateral();
			}

			/// Se distrato foi resolvido remover botão Efetuar distrato
			if (crmForm.all.statuscode.DataValue == SatusCode.DISTRATO_EFETUADO || !crmForm.EquipeContrato())
			{
				crmForm.RemoveBotao("EfetivarDistrato");
			}

			if (crmForm.FormType != TypeCreate)
			{
				crmForm.ExisteAditivosFilhos();
			}

            crmForm.EsconderCamposVendaGarantida = function() {
                if (crmForm.ValidarDistratoVendaGarantida()) {
                    crmForm.EscondeCampo("new_carta_credito", false);
                    crmForm.EscondeCampo("new_data_termo_extincao", false);
                    crmForm.EscondeCampo("new_saldo_devedor_atualizado", false);
                }
                else {
                    crmForm.EscondeCampo("new_carta_credito", true);
                    crmForm.EscondeCampo("new_data_termo_extincao", true);
                    crmForm.EscondeCampo("new_saldo_devedor_atualizado", true);
                }
            }

            crmForm.EsconderCamposVendaGarantida();
            if (crmForm.all.new_tipo_renegociacaoid.DataValue == null || crmForm.all.new_tipo_renegociacaoid.DataValue === null || crmForm.all.new_tipo_renegociacaoid.DataValue == undefined) {
                window.oldValue = null;
            } else {
                window.oldValue = crmForm.all.new_tipo_renegociacaoid.DataValue[0].name;
            }
		}

		crmForm.ValidarSap45 = function () {
			if (crmForm.all.new_tipo_renegociacaoid != null && crmForm.all.new_tipo_renegociacaoid.DataValue != null) {
				var cmd = new RemoteCommand('MrvService', 'ObterParametroDistrato45', '/MRVCustomizations/');
				var result = cmd.Execute();
				if (result.Success) {
					var tipoRenegociacao = crmForm.all.new_tipo_renegociacaoid.DataValue[0].id.toLowerCase();
					if (result.ReturnValue.Mrv.TipoDistratoSAP45.toLowerCase() == tipoRenegociacao) {
						return true;
					} else {
						return false;
					}
				} else {
					alert(result.Mensagem);
					return false;

				}

			} else {
				return false;
			}
		}

		function RevisarInformacoesFornecidasParaDistrato() {
			if (crmForm.all.statuscode.DataValue != SatusCode.RASCUNHO) {
				alert('Esta operação só é permitida para Distratos em Rascunho');
				return false;
			}
			if (crmForm.all.new_tipo_renegociacaoid.DataValue == null) {
				alert('Preencha o Tipo de Distrato');
				return false;
			}
			if (crmForm.all.new_contratoid.DataValue == null && crmForm.all.new_numero_contrato_legado.DataValue == null) {
				alert('Selecione um Contrato ou Preencha o N° Contrato (Legado).');
				return false;
			}
			if (crmForm.IsDirty) {
				alert('Salve antes de efetuar o distrato.');
				return false;
			}

			return true;
		}

		crmForm.CriarSolicitacaoEfetivacaoDistrato = function (distratarFilhos) {
			if (crmForm.ValidarSap45() || RevisarInformacoesFornecidasParaDistrato()) {

				var cmd = new RemoteCommand('DistratoService', 'ValidarPermisaoDistratoCSG', '/MRVCustomizations/DistratoRenegociacao/');
				cmd.SetParameter('distratoId', crmForm.ObjectId);
				resultado = cmd.Execute();

				if (resultado.Success) {
					if (resultado.ReturnValue.Mrv.Mensagens) {
						var mensagem = resultado.ReturnValue.Mrv.Mensagens.split(';');

							for (i = 0; i < mensagem.length; i++) {

								if (!window.confirm(mensagem[i])) {
									return false;
								}
							}
					}
				} else {
					alert('Ocorreu um erro não tratado ao tentar efetivar um distrato. Entre em contato com o suporte técnico.');
				}

				cmd = new RemoteCommand('DistratoService', 'CriarOrdemDistratoSAP', '/MRVCustomizations/DistratoRenegociacao/');
				cmd.SetParameter('distratoId', crmForm.ObjectId);
				cmd.SetParameter('distratarFilhos', distratarFilhos.toString());
				resultado = cmd.Execute();

				if (!resultado.Success) {
					alert('Ocorreu um erro não tratado ao tentar efetivar um distrato. Entre em contato com o suporte técnico.');
				} else if (resultado.ReturnValue.Mrv.Sucesso) {
					alert('Distrato Efetuado.');
					window.document.location.reload();
				} else {
					/// Verificar se a mensagem de erro contém o código para distratar filhos.
					var erro = resultado.ReturnValue.Mrv.Mensagem.toString();
					var codigoDistratarFilhos = '149209221519';

					if (erro.indexOf(codigoDistratarFilhos, 0) == -1)
						alert(erro);
						else {
							var tmp = erro.split('*');
							if (window.confirm('Este contrato contém aditivos filhos.\n' + tmp[1] + 'Deseja distratá-los automaticamente?')) {
								var consulta = crmForm.ConsultarExisteAditivosFilhos();

								if (consulta != null) {
									if (!consulta.CadastroReembolsoAditivosFilhosFeito)
										alert('Contrato possui aditivos filhos e ainda não foram cadastrados valores para reembolso. Clique em Reembolso para efetuar o cadastro.');
									else {
										DisplayActionMsg('Aguarde, processo demorado.', 400, 150);
										setTimeout('crmForm.CriarSolicitacaoEfetivacaoDistrato(true)', 1000);
									}
								}
							} else
								alert('Distrato não efetivado pois possuir aditivos filhos.');
						}
					}
				}
			}

			function VerificarVendaConsignada() {
			    if (crmForm.all.new_tipo_renegociacaoid.DataValue && crmForm.all.new_tipo_renegociacaoid.DataValue[0].name === "CANCELAMENTO VENDA GARANTIDA") {
			        if (!crmForm.UsuarioPertenceEquipe("CR_Cancelamento_Venda_Garantida")) {
			            alert("Somente usuários autorizados podem efetivar distratos (Cancelamentos) do tipo venda Garantida");
			            return false;
			        }
			    }
			
			    var cmd = new RemoteCommand('DistratoService', 'ExisteOcorrenciaVendaConsignada', '/MRVCustomizations/DistratoRenegociacao/');
				cmd.SetParameter('distratoId', crmForm.ObjectId);
				resultado = cmd.Execute();
				if (resultado.Success && resultado.ReturnValue.Mrv.Existe) {
					alertaVendaConsignada = 'Atenção: Existe ocorrência de venda consignada em aberto. Deseja realmente confirmar a efetivação do distrato?';
				}
				return true;
		    }

			function crmForm.EfetivarDistrato() {
				var alertaVendaConsignada = 'Confirma a efetivação do distrato?';
				if (VerificarVendaConsignada()) {
				    if (window.confirm(alertaVendaConsignada)) {
				        DisplayActionMsg('Aguarde, processo demorado.', 400, 150);
				        setTimeout('crmForm.CriarSolicitacaoEfetivacaoDistrato(false)', 1000);
				    }
				}
            }
            
            crmForm.ValidarDistratoVendaGarantida = function() {
                if (crmForm.all.new_tipo_renegociacaoid.DataValue) {
                    var atributos = "new_distrato_venda_garantida";

                    var cmd = new RemoteCommand("MrvService", "GetEntity", "/MRVCustomizations/");
                    cmd.SetParameter("entity", "new_tipo_renegociacao");
                    cmd.SetParameter("primaryField", "new_tipo_renegociacaoid");
                    cmd.SetParameter("entityId", crmForm.all.new_tipo_renegociacaoid.DataValue[0].id);
                    cmd.SetParameter("fieldList", atributos);
                    var oResult = cmd.Execute();
                    if (oResult.Success) {
                        if (oResult.ReturnValue.Mrv.new_distrato_venda_garantida)
                            return oResult.ReturnValue.Mrv.new_distrato_venda_garantida;
                    }
                    return false;
                }
                else {
                    return false;
                }
            }

	} catch (error) {
		formularioValido = false;
		alert("Ocorreu um erro no formulário.\n" + error.description);
	}