﻿function FilterLookup(attribute, url, param) {
    if (param != null)
        url += '?' + param;

    oImg = eval('attribute' + '.parentNode.childNodes[0]');

    oImg.onclick = function() {
        var retorno = openStdDlg(url, null, 600, 450);

        if (typeof (retorno) != "undefined") {
            var strValues = retorno.split('*|*');
            var lookupData = new Array();
            var lookupItem = new Object();

            lookupItem.id = "{" + strValues[1] + "}";

            /// ObjectTypeCode opportunty
            lookupItem.type = 3;

            lookupItem.name = strValues[0];
            lookupData[0] = lookupItem;
            attribute.DataValue = lookupData;
            crmForm.ObterInformacoesDaOportunidade(strValues[1]);
            crmForm.all.new_name.DataValue = 'DISTRATO - ' + strValues[0];
            crmForm.all.new_aditivoid.DataValue = null;
        }
    };
}
if (crmForm.all.new_clienteid.DataValue != null) {
    var idCliente = crmForm.all.new_clienteid.DataValue[0].id;
    var oParam = "objectTypeCode=3001&filterDefault=false&attributesearch=accountid&_accountid=" + idCliente;
    FilterLookup(crmForm.all.new_oportunidadeid, "/MRVWeb/FilterLookup/FilterLookup.aspx", oParam);
}