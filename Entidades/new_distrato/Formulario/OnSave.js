﻿try 
{
    if (!formularioValido) 
    {
        alert("Ocorreu um erro no formulário. Impossível salvar.");
        event.returnValue = false;
        return false;
    }
    
   crmForm.all.statuscode.Disabled = false;
} 
catch (e) 
{
    alert("Ocorreu um erro no formulário.\n" + e.description);
    event.returnValue = false;
    return false;
}