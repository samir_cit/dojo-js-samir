﻿if (crmForm.all.new_valor_do_reembolso.DataValue != null &&
    crmForm.all.new_valor_do_reembolso.DataValue > 0) 
{
    crmForm.SetFieldReqLevel("new_quantidade_parcelas1", true);
    crmForm.SetFieldReqLevel("new_valor_parcela1", true);
    crmForm.SetFieldReqLevel("new_vencimento_1a_parcela1", true);
}
else 
{
    crmForm.SetFieldReqLevel("new_quantidade_parcelas1", false);
    crmForm.SetFieldReqLevel("new_valor_parcela1", false);
    crmForm.SetFieldReqLevel("new_vencimento_1a_parcela1", false);
}