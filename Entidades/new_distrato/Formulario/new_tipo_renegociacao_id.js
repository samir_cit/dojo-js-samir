﻿var oTypeCode = "10132";
var oParam = "objectTypeCode=10106&filterDefault=false&attributesearch=name";
crmForm.FilterLookup(crmForm.all.new_tipo_renegociacaoid, oTypeCode, "/MRVWeb/FilterLookup/FilterLookup.aspx", oParam);

if (((window.oldValue == null || window.oldValue == undefined) && crmForm.all.new_tipo_renegociacaoid.DataValue[0].name === "CANCELAMENTO VENDA GARANTIDA") ||
    window.oldValue === "CANCELAMENTO VENDA GARANTIDA" || crmForm.all.new_tipo_renegociacaoid.DataValue[0].name === "CANCELAMENTO VENDA GARANTIDA") {

    window.oldValue = crmForm.all.new_tipo_renegociacaoid.DataValue[0].name;

    crmForm.all.new_clienteid.DataValue = null;
    crmForm.all.new_clienteid.FireOnChange();

    crmForm.all.new_produtoid.DataValue = null;
    crmForm.all.new_produtoid.FireOnChange();

    crmForm.all.new_contratoid.DataValue = null;
    crmForm.all.new_contratoid.FireOnChange();

    crmForm.all.new_aditivoid.DataValue = null;
    crmForm.all.new_aditivoid.FireOnChange();

    crmForm.all.new_oportunidadeid.DataValue = null;
    crmForm.all.new_oportunidadeid.FireOnChange();
}
if (crmForm.ValidarSap45()) {
    crmForm.EscondeCampo("new_oportunidadeid", false);
    crmForm.EscondeCampo("new_contratoid", true);
    crmForm.SetFieldReqLevel("new_oportunidadeid", true);
    crmForm.SetFieldReqLevel("new_contratoid", false);
    crmForm.SetFieldReqLevel("new_aditivoid", false);
    crmForm.all.new_contratoid.Disabled = true;

}
else {
    crmForm.EscondeCampo("new_oportunidadeid", true);
    crmForm.EscondeCampo("new_contratoid", false);
    crmForm.SetFieldReqLevel("new_oportunidadeid", false);
    crmForm.SetFieldReqLevel("new_aditivoid", true);
    crmForm.SetFieldReqLevel("new_contratoid", true);
    crmForm.all.new_contratoid.Disabled = false;

}

function FilterLookup(attribute, url, param) {
    if (param != null)
        url += '?' + param;

    oImg = eval('attribute' + '.parentNode.childNodes[0]');

    oImg.onclick = function() {
        var retorno = openStdDlg(url, null, 600, 450);

        if (typeof (retorno) != "undefined") {
            var strValues = retorno.split('*|*');
            var lookupData = new Array();
            var lookupItem = new Object();

            lookupItem.id = "{" + strValues[1] + "}";

            /// ObjectTypeCode Contract
            lookupItem.type = 1010;

            lookupItem.name = strValues[0];
            lookupData[0] = lookupItem;
            attribute.DataValue = lookupData;
            crmForm.ApresentaTabRelacionamento();
            crmForm.ObterInformacoesDoContrato(strValues[1]);
            crmForm.all.new_name.DataValue = 'DISTRATO - ' + strValues[0];
            crmForm.all.new_aditivoid.DataValue = null;
            attribute.FireOnChange();
        }
    };
}


var tipocontrato = TipoDeContrato.UNIDADE;

if (crmForm.all.new_tipodecontrato.DataValue != null && crmForm.all.new_tipodecontrato.DataValue == TipoDeContrato.PERMUTA) {
    tipocontrato = crmForm.all.new_tipodecontrato.DataValue;
}

var clienteId = "";

if (crmForm.all.new_clienteid.DataValue) {
    clienteId = crmForm.all.new_clienteid.DataValue[0].id;
}

if (crmForm.ValidarDistratoVendaGarantida()) {
    var oParam = "objectTypeCode=1012&filterDefault=false&attributesearch=title&_customerid=" + clienteId + "&_tipocontrato=" + tipocontrato;
    FilterLookup(crmForm.all.new_contratoid, "/MRVWeb/FilterLookup/FilterLookup.aspx", oParam);
}
else {
    var oParam = "objectTypeCode=1011&filterDefault=false&attributesearch=title&_customerid=" + clienteId + "&_tipocontrato=" + tipocontrato;
    FilterLookup(crmForm.all.new_contratoid, "/MRVWeb/FilterLookup/FilterLookup.aspx", oParam);
}

if (crmForm.EsconderCamposVendaGarantida) {
    crmForm.EsconderCamposVendaGarantida();
}

crmForm.all.new_aditivoid.FireOnChange();