﻿Equipe = {
    DESENVOLVIMENTO_IMOBILIARIO: "Desenv.Imobiliário"
}

formularioValido = true;
try {

    //Funções

    crmForm.FilterLookup = function(attribute, url, param, typecode) {
        if (param != null)
            url += '?' + param;

        oImg = eval('attribute' + '.parentNode.childNodes[0]');

        oImg.onclick = function() {
            retorno = openStdDlg(url, null, 750, 450);

            if (typeof (retorno) != "undefined") {
                strValues = retorno.split('*|*');
                var lookupData = new Array();
                lookupItem = new Object();
                lookupItem.id = "{" + strValues[1] + "}";
                lookupItem.type = typecode;

                lookupItem.name = strValues[0];
                lookupData[0] = lookupItem;
                attribute.DataValue = lookupData;
                attribute.FireOnChange();
            }
        };
    }

    /// Verifica se o usuário logado é membro da equipe
    crmForm.UsuarioPertenceEquipe = function(nomeFuncao) {
        var cmd = new RemoteCommand("MrvService", "UserContainsTeams", "/MrvCustomizations/");
        cmd.SetParameter("teams", nomeFuncao);
        var resultado = cmd.Execute();

        if (resultado.Success)
            return resultado.ReturnValue.Mrv.Found;
        else
            return false;
    }

    //Montar nome do registro
    crmForm.MontaNome = function() {
        if (crmForm.all.new_produtoid.DataValue != null)
            crmForm.all.new_name.DataValue = crmForm.all.new_produtoid.DataValue[0].name;
    }

    crmForm.EscondeCampo = function(nomeCampo, esconde) {
        var tipo = "";
        if (esconde)
            tipo = "none";

        var atributo_c = eval("crmForm.all." + nomeCampo + "_c");
        var atributo_d = eval("crmForm.all." + nomeCampo + "_d");
        atributo_c.style.display = tipo;
        atributo_d.style.display = tipo;
    }

    AdicionaNotificacao = function() {
        var objeto = this;

        this.ERRO = "/_imgs/error/notif_icn_crit16.png";
        this.ALERTA = "/_imgs/error/notif_icn_warn16.png";
        this.SUCESSO = "/_imgs/ico/16_L_check.gif";

        objeto.Mostra = function(messagem, imagem) {
            var notificationHTML = '<DIV class="Notification"><TABLE cellSpacing="0" cellPadding="0"><TBODY><TR><TD vAlign="top"><IMG class="ms-crm-Lookup-Item" alt="" src="' + imagem + '" /></TD><TD><SPAN>' + messagem + '</SPAN></TD></TR></TBODY></TABLE></DIV>';
            var notificationsArea = document.getElementById('Notifications');
            if (notificationsArea == null)
                return;
            notificationsArea.innerHTML = notificationHTML;
            notificationsArea.style.display = 'block';
        }

        objeto.Oculta = function() {
            var notificationsArea = document.getElementById('Notifications');
            if (notificationsArea == null)
                return;
            notificationsArea.innerHTML = '';
            notificationsArea.style.display = 'inline';
        }
    }

    Notificacao = new AdicionaNotificacao();

    //Ocultar campo Nome
    crmForm.EscondeCampo("new_name", true);

    //Chamar onChange do new_tipo
    if (crmForm.FormType == 1)
        crmForm.all.new_tipo.FireOnChange();
    else if (crmForm.FormType == 2) {
        //Desabilitar todos os campos
        crmForm.all.new_tipo.Disabled = true;
        crmForm.all.new_empreendimentoid.Disabled = true;
        crmForm.all.new_produtoid.Disabled = true;
        //verificar se usuário pode alterar o campo Prazo de entrega.
        EquipeDesenvImobiliario = crmForm.UsuarioPertenceEquipe(Equipe.DESENVOLVIMENTO_IMOBILIARIO);
        if (EquipeDesenvImobiliario) {
            crmForm.all.new_prazo_entrega.Disabled = false;
        } else {
            crmForm.all.new_prazo_entrega.Disabled = true;
        }
    }

} catch (error) {
    formularioValido = false;
    alert("Ocorreu um erro no formulário.\n" + error.description);
}