﻿if (crmForm.all.new_tipo.DataValue != null) {
    //Habilita os campos
    crmForm.all.new_empreendimentoid.Disabled = false;
    crmForm.all.new_prazo_entrega.Disabled = false;
    
    
    //Dentro
    if (crmForm.all.new_tipo.DataValue == 1) {
        //Lookup dentro. Listar empreendimentos do terreno.
        var oParam = "objectTypeCode=100011111&filterDefault=false&attributesearch=new_nome&_new_terrenoid=" + crmForm.all.new_terrenoid.DataValue[0].id + " ";
        crmForm.FilterLookup(crmForm.all.new_empreendimentoid, "/MRVWeb/FilterLookup/FilterLookup.aspx", oParam, 10001); 
    }
    else {
        //Lookup fora. Listar todos os empreendimentos do sistema.
        var oImg = eval('crmForm.all.new_empreendimentoid' + '.parentNode.childNodes[0]');
        oImg.onclick = null;
    }
    //Limpar campo new_empreendimeto
    crmForm.all.new_empreendimentoid.DataValue = null;
    crmForm.all.new_produtoid.DataValue = null;
}
else {
    //Desabilita os campos e limpa valores.
    crmForm.all.new_empreendimentoid.Disabled = true;
    crmForm.all.new_empreendimentoid.DataValue = null;
    crmForm.all.new_prazo_entrega.Disabled = true;
    crmForm.all.new_prazo_entrega.DataValue = null;
    crmForm.all.new_produtoid.Disabled = true;
    crmForm.all.new_produtoid.DataValue = null;
    crmForm.all.new_prazo_entrega.Disabled = true;
    crmForm.all.new_prazo_entrega.DataValue = null;
}
