﻿function loadScript() {
    var oScript = document.createElement('script');
    oScript.type = "text/javascript";
    oScript.src = "/_static/Base.js";

    var oHead = document.getElementsByTagName('head')[0];
    oHead.appendChild(oScript);

    oScript.onreadystatechange = function() {
        if (this.readyState == 'complete' || this.readyState == 'loaded')
            Load();
    }
}

loadScript();

function Load() {
    if (crmForm.UsuarioPertenceEquipe(Equipe.CO_LIBERACAO_ANALISE_CREDITO)) {
        crmForm.all.new_processo_sicaq_sacbb.Disabled = false;
    }
    else {
        crmForm.all.new_processo_sicaq_sacbb.Disabled = true;

    }
    
    if (crmForm.UsuarioPertenceEquipe(Equipe.CO_PAGAMENTO_RPA)) {
        crmForm.all.new_pagamento_rpa.Disabled = false;
    }
    else {
        crmForm.all.new_pagamento_rpa.Disabled = true;

    }
    
    if (crmForm.UsuarioPertenceEquipe(Equipe.ADMINISTRADORES)) {
        crmForm.all.new_agendamentomrvcredito.Disabled = false;
        crmForm.all.new_exige_protocolo_averbacao.Disabled = false;
    }
    else {
        crmForm.all.new_agendamentomrvcredito.Disabled = true;
        crmForm.all.new_exige_protocolo_averbacao.Disabled = true;

    }

    crmForm.all.new_taxa_despachante_obrigatoria.FireOnChange();

    crmForm.DesabilitaVendaGarantida = function() {
        if (crmForm.FormType == TypeCreate || crmForm.FormType == TypeUpdate) {
            if (crmForm.UsuarioPertenceEquipe(Equipe.COM_VENDA_GARANTIDA)) {
                crmForm.all.new_venda_garantida.Disabled = false;
            } else {
                crmForm.all.new_venda_garantida.Disabled = true;
            }
        } 
    }

    crmForm.DesabilitaVendaGarantida();

    if (crmForm.UsuarioPertenceEquipe(Equipe.CR_TAXAS)) {
        crmForm.DesabilitarFormulario(true);
        crmForm.all.new_percentual_itbi.Disabled = false;
        crmForm.all.new_percentual_registro.Disabled = false;
        crmForm.all.new_percentual_funrejus.Disabled = false;
    }
    else {
        crmForm.all.new_percentual_itbi.Disabled = true;
        crmForm.all.new_percentual_registro.Disabled = true;
        crmForm.all.new_percentual_funrejus.Disabled = true;
    }
    
    
}