﻿try {
    function loadScript() {
        var oScript = document.createElement('script');
        oScript.type = "text/javascript";
        oScript.src = "/_static/Base.js";
        var oHead = document.getElementsByTagName('head')[0];
        oHead.appendChild(oScript);
        oScript.onreadystatechange = function() {
            if (this.readyState == 'complete' || this.readyState == 'loaded')
                Load();
        }
    }
    loadScript();

    function Load() {
        crmForm.ObterInformacoesDaOcorrencia = function(ocorrenciaId) {
            var rCmd = new RemoteCommand("wsKitAcabamento", "ObterInformacoesCancelamento", "/KitAcabamento/");

            rCmd.SetParameter("ocorrenciaID", ocorrenciaId);
            retorno = rCmd.Execute();
            if (retorno.ReturnValue.Mrv.Erro) {
                alert(retorno.ReturnValue.Mrv.Mensagem);
                //Em caso de mensagem de erro ou fora do prazo de cancelamento a janela será fechada
                window.close();
            }
            else {
                crmForm.SetaLookup(crmForm.all.new_clienteid, retorno.ReturnValue.Mrv.IDCliente, retorno.ReturnValue.Mrv.NomeCliente, "account");
                crmForm.all.new_name.DataValue = "CANCELAMENTO KIT - " + crmForm.all.new_clienteid.DataValue[0].name;
                crmForm.SetaLookup(crmForm.all.new_oportunidadeid, retorno.ReturnValue.Mrv.IDOportunidade, retorno.ReturnValue.Mrv.NomeOportunidade, "opportunity");
                crmForm.SetaLookup(crmForm.all.new_unidadeid, retorno.ReturnValue.Mrv.IDProduto, retorno.ReturnValue.Mrv.NomeProduto, "product");
                crmForm.all.new_unidadeid.FireOnChange();
                crmForm.all.new_oportunidadeid.FireOnChange();
                crmForm.all.new_name.SetFocus();
            }
        }

        crmForm.SetaLookup = function(campo, id, nome, nomeEntidade) {
            if (id != null && id != "") {
                var lookupData = new Array();
                var lookupItem = new Object();
                lookupItem.id = id;
                lookupItem.typename = nomeEntidade;
                lookupItem.name = nome;
                lookupData[0] = lookupItem;
                campo.DataValue = lookupData;
            }
        }
        //preenche campos a partir da ocorrencia
        if (crmForm.QueryString("_CreateFromType") == "112" && crmForm.FormType == 1) {
            var ocId = crmForm.QueryString("_CreateFromId").replace("%7b", "").replace("%7d", "");
            crmForm.ObterInformacoesDaOcorrencia(ocId);
        }

        if (crmForm.FormType == TypeUpdate) {
            //desabilitar campos se não está em rascunho
            if (crmForm.all.statuscode.DataValue != 1) {
                crmForm.all.new_clienteid.Disabled = true;
                crmForm.all.new_motivo_cancelamento.Disabled = true;
                crmForm.RemoveBotao("EfetivarCancelamento");
            }

            var obj = document.getElementById("IFRAME_CancelaKit");
            if (obj != null) {
                var url = "/mrvweb/kit/cancelarkit.aspx?Id=" + crmForm.ObjectId
                if(crmForm.all.new_oportunidadeid.DataValue != null)
                    url += "&Op=" + crmForm.all.new_oportunidadeid.DataValue[0].id;                
                obj.src = url;
            }            
            else
                alert("Não foi possível carregar os kits da oportunidade. Entre em contato com o helpdesk.");
        }

    } //Fim Load
}
catch (error) {
    formularioValido = false;
    alert("Ocorreu um erro no formulário.\n" + error.description);
}