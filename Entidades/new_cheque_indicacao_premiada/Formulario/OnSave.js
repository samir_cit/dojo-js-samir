﻿try
{
    if (crmForm.all.statuscode.DataValue != RazaoStatus.BLOQUEADO) {
         alert("Apenas permitido salvar com o status Bloqueado");
         event.returnValue = false;
    return false;
    }
} catch (erro) {
    alert("Ocorreu um erro no formulário." + erro.description);
    event.returnValue = false;
    return false;
}