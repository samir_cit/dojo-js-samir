﻿formularioValido = true;
RazaoStatus = {
    NAO_UTILIZADO: 1,
    COMPARTILHADO: 3,
    UTILIZADO: 4,
    EXPIRADO: 5,
    BLOQUEADO: 6,
    APROVADO:7
}
try {
    function loadScript() {
        var oScript = document.createElement('script');
        oScript.type = "text/javascript";
        oScript.src = "/_static/Base.js";

        var oHead = document.getElementsByTagName('head')[0];
        oHead.appendChild(oScript);

        oScript.onreadystatechange = function() {
            if (this.readyState == 'complete' || this.readyState == 'loaded')
                Load();
        }
    }

    loadScript();

    function Load() {
        crmForm.MarcarObrigatorioJustificativa = function() {
        var obrigatorio = crmForm.all.statuscode.DataValue == RazaoStatus.BLOQUEADO && !crmForm.all.new_justificativa.DataValue;
            crmForm.SetFieldReqLevel("new_justificativa", obrigatorio);
            crmForm.DesabilitarCampos("new_justificativa", !obrigatorio);
        }
        
        var desabilitarCampo = true;
        if (crmForm.UsuarioPertenceEquipe(Equipe.MARKETING) || crmForm.UsuarioPertenceEquipe(Equipe.ADMINISTRADORES)) {
            if (crmForm.all.statuscode.DataValue == RazaoStatus.NAO_UTILIZADO || crmForm.all.statuscode.DataValue == RazaoStatus.COMPARTILHADO
            || crmForm.all.statuscode.DataValue == RazaoStatus.APROVADO) {
                desabilitarCampo = false;
            }
        }
        
        crmForm.MarcarObrigatorioJustificativa();

        crmForm.DesabilitarCampos("statuscode", desabilitarCampo);
        if (desabilitarCampo) {
            crmForm.DesabilitarCampos("new_justificativa", desabilitarCampo);
        }
    }
}
catch (error) {
    formularioValido = false;
    alert("Ocorreu um erro no formulário.\n" + error.description);
}