﻿formularioValido = true;

try 
{
    crmForm.EscondeCampo = function(nomeCampo, esconde) {
        var tipo = "";
        if (esconde)
            tipo = "none";

        var atributo_c = eval("crmForm.all." + nomeCampo + "_c");
        var atributo_d = eval("crmForm.all." + nomeCampo + "_d");
        atributo_c.style.display = tipo;
        atributo_d.style.display = tipo;
    }

    crmForm.EscondeCampo("new_name", true);

    function DisabledField(Fields) {
        var tmp = Fields.split(";");
        for (index = 0; index < tmp.length; index++) {
            if (tmp[index].toString() != 'ownerid') {
                var object = eval("crmForm.all." + tmp[index])
                object.Disabled = true;
            }
        }
    }
    
    function DisabledAllFields() {
        var todosAtributos = document.getElementsByTagName("label");
        for (var i = 0; i < todosAtributos.length; i++) {
            if (todosAtributos[i].htmlFor != "")
                DisabledField(todosAtributos[i].htmlFor.replace("_ledit", ""));
        }
    }

    DisabledAllFields();
}
catch (erro) {
    formularioValido = false;
    alert("Ocorreu um erro no formulário.\n" + erro.description);
}