﻿var vlrSinal = 0;
var MENSAL = 2;
var QUANTIDADE = 1;

if (crmForm.all.new_valorsinalavista.DataValue) {
    crmForm.SetFieldReqLevel('new_datadopagamento', true);
    vlrSinal += crmForm.all.new_valorsinalavista.DataValue;
}
else
    crmForm.SetFieldReqLevel('new_datadopagamento', false);

if (crmForm.all.new_divisaosinal.DataValue == true) {
    if (crmForm.all.new_qtdedeparcelas.DataValue != null || crmForm.all.new_valordaparcela.DataValue != null || crmForm.all.new_datadevencimento1parcela.DataValue != null) {

        if (crmForm.all.new_qtdedeparcelas.DataValue != null && crmForm.all.new_valordaparcela.DataValue != null)
            vlrSinal += crmForm.all.new_qtdedeparcelas.DataValue * crmForm.all.new_valordaparcela.DataValue;
    }

    if (crmForm.all.new_qtdedeparcelassinal1.DataValue != null || crmForm.all.new_valordaparcelasinal1.DataValue != null || crmForm.all.new_datadevencimento1parcelasinal1.DataValue != null) {

        if (crmForm.all.new_qtdedeparcelassinal1.DataValue != null && crmForm.all.new_valordaparcelasinal1.DataValue != null)
            vlrSinal += crmForm.all.new_qtdedeparcelassinal1.DataValue * crmForm.all.new_valordaparcelasinal1.DataValue;
    }

    if (crmForm.all.new_qtdedeparcelassinal2.DataValue != null || crmForm.all.new_valordasparcelassinal2.DataValue != null || crmForm.all.new_datadevencimento1parcelasinal2.DataValue != null) {

        if (crmForm.all.new_qtdedeparcelassinal2.DataValue != null && crmForm.all.new_valordasparcelassinal2.DataValue != null)
            vlrSinal += crmForm.all.new_qtdedeparcelassinal2.DataValue * crmForm.all.new_valordasparcelassinal2.DataValue;
    }

    if (crmForm.all.new_qtdedeparcelassinal3.DataValue != null || crmForm.all.new_valordasparcelassinal3.DataValue != null || crmForm.all.new_datadevencimento1parcelasinal3.DataValue != null) {

        if (crmForm.all.new_qtdedeparcelassinal3.DataValue != null && crmForm.all.new_valordasparcelassinal3.DataValue != null)
            vlrSinal += crmForm.all.new_qtdedeparcelassinal3.DataValue * crmForm.all.new_valordasparcelassinal3.DataValue;
    }
}

crmForm.all.new_sinal.DataValue = vlrSinal;

var vlrTotalDifFinanciamento = 0;
if (crmForm.all.new_qtd_parcela_df1.DataValue != null || crmForm.all.new_valor_parcela_df1.DataValue != null || crmForm.all.new_dat_vencimento_parceladf1.DataValue != null) {
    if (crmForm.all.new_qtd_parcela_df1.DataValue != null && crmForm.all.new_valor_parcela_df1.DataValue != null)
        vlrTotalDifFinanciamento += crmForm.all.new_qtd_parcela_df1.DataValue * crmForm.all.new_valor_parcela_df1.DataValue;
}
if (crmForm.all.new_qtd_parcela_df2.DataValue != null || crmForm.all.new_valor_parcela_df2.DataValue != null || crmForm.all.new_dat_vencimento_parceladf2.DataValue != null) {
    if (crmForm.all.new_qtd_parcela_df2.DataValue != null && crmForm.all.new_valor_parcela_df2.DataValue != null)
        vlrTotalDifFinanciamento += crmForm.all.new_qtd_parcela_df2.DataValue * crmForm.all.new_valor_parcela_df2.DataValue;
}
if (crmForm.all.new_qtd_parcela_df3.DataValue != null || crmForm.all.new_valor_parcela_df3.DataValue != null || crmForm.all.new_dat_vencimento_parceladf3.DataValue != null) {
    if (crmForm.all.new_qtd_parcela_df3.DataValue != null && crmForm.all.new_valor_parcela_df3.DataValue != null)
        vlrTotalDifFinanciamento += crmForm.all.new_qtd_parcela_df3.DataValue * crmForm.all.new_valor_parcela_df3.DataValue;
}
if (crmForm.all.new_qtd_parcela_df4.DataValue != null || crmForm.all.new_valor_parcela_df4.DataValue != null || crmForm.all.new_dat_vencimento_parceladf4.DataValue != null) {
    if (crmForm.all.new_qtd_parcela_df4.DataValue != null && crmForm.all.new_valor_parcela_df4.DataValue != null)
        vlrTotalDifFinanciamento += crmForm.all.new_qtd_parcela_df4.DataValue * crmForm.all.new_valor_parcela_df4.DataValue;
}
if (crmForm.all.new_qtd_parcela_df5.DataValue != null || crmForm.all.new_valor_parcela_df5.DataValue != null || crmForm.all.new_dat_vencimento_parceladf5.DataValue != null) {
    if (crmForm.all.new_qtd_parcela_df5.DataValue != null && crmForm.all.new_valor_parcela_df5.DataValue != null)
        vlrTotalDifFinanciamento += crmForm.all.new_qtd_parcela_df5.DataValue * crmForm.all.new_valor_parcela_df5.DataValue;
}
if (crmForm.all.new_qtd_parcela_df6.DataValue != null || crmForm.all.new_valor_parcela_df6.DataValue != null || crmForm.all.new_dat_vencimento_parceladf6.DataValue != null) {
    if (crmForm.all.new_qtd_parcela_df6.DataValue != null && crmForm.all.new_valor_parcela_df6.DataValue != null)
        vlrTotalDifFinanciamento += crmForm.all.new_qtd_parcela_df6.DataValue * crmForm.all.new_valor_parcela_df6.DataValue;
}
if (crmForm.all.new_qtd_parcela_df7.DataValue != null || crmForm.all.new_valor_parcela_df7.DataValue != null || crmForm.all.new_dat_vencimento_parceladf7.DataValue != null) {
    if (crmForm.all.new_qtd_parcela_df7.DataValue != null && crmForm.all.new_valor_parcela_df7.DataValue != null)
        vlrTotalDifFinanciamento += crmForm.all.new_qtd_parcela_df7.DataValue * crmForm.all.new_valor_parcela_df7.DataValue;
}
if (crmForm.all.new_qtd_parcela_df8.DataValue != null || crmForm.all.new_valor_parcela_df8.DataValue != null || crmForm.all.new_dat_vencimento_parceladf8.DataValue != null) {
    if (crmForm.all.new_qtd_parcela_df8.DataValue != null && crmForm.all.new_valor_parcela_df8.DataValue != null)
        vlrTotalDifFinanciamento += crmForm.all.new_qtd_parcela_df8.DataValue * crmForm.all.new_valor_parcela_df8.DataValue;
}


crmForm.all.new_total_parcela_df.DataValue = vlrTotalDifFinanciamento;

var vlrMensal = 0;
if (crmForm.all.new_qtdedeparcelasmensais.DataValue != null || crmForm.all.new_valordaparcelamensal.DataValue != null || crmForm.all.new_datadepagamentomensal.DataValue != null) {

    if (crmForm.all.new_qtdedeparcelasmensais.DataValue != null && crmForm.all.new_valordaparcelamensal.DataValue != null)
        vlrMensal += crmForm.all.new_qtdedeparcelasmensais.DataValue * crmForm.all.new_valordaparcelamensal.DataValue;
}

if (crmForm.all.new_qtdedeparcelasmensais1.DataValue != null || crmForm.all.new_valordaparcelamensal1.DataValue != null || crmForm.all.new_datado1pagamentomensal1.DataValue != null) {

    if (crmForm.all.new_qtdedeparcelasmensais1.DataValue != null && crmForm.all.new_valordaparcelamensal1.DataValue != null)
        vlrMensal += crmForm.all.new_qtdedeparcelasmensais1.DataValue * crmForm.all.new_valordaparcelamensal1.DataValue;
}

if (crmForm.all.new_qtdedeparcelasmensais2.DataValue != null || crmForm.all.new_valordaparcelamensal2.DataValue != null || crmForm.all.new_datado1pagamentomensal2.DataValue != null) {

    if (crmForm.all.new_qtdedeparcelasmensais2.DataValue != null && crmForm.all.new_valordaparcelamensal2.DataValue != null)
        vlrMensal += crmForm.all.new_qtdedeparcelasmensais2.DataValue * crmForm.all.new_valordaparcelamensal2.DataValue;
}

if (crmForm.all.new_qtdedeparcelasmensais3.DataValue != null || crmForm.all.new_valordaparcelamensal3.DataValue != null || crmForm.all.new_datado1pagamentomensal3.DataValue != null) {

    if (crmForm.all.new_qtdedeparcelasmensais3.DataValue != null && crmForm.all.new_valordaparcelamensal3.DataValue != null)
        vlrMensal += crmForm.all.new_qtdedeparcelasmensais3.DataValue * crmForm.all.new_valordaparcelamensal3.DataValue;
}

if (crmForm.all.new_qtdedeparcelasmensais4.DataValue != null || crmForm.all.new_valordaparcelamensal4.DataValue != null || crmForm.all.new_datado1pagamentomensal4.DataValue != null) {

    if (crmForm.all.new_qtdedeparcelasmensais4.DataValue != null && crmForm.all.new_valordaparcelamensal4.DataValue != null)
        vlrMensal += crmForm.all.new_qtdedeparcelasmensais4.DataValue * crmForm.all.new_valordaparcelamensal4.DataValue;
}

if (crmForm.all.new_qtdedeparcelasmensais5.DataValue != null || crmForm.all.new_valordaparcelamensal5.DataValue != null || crmForm.all.new_datado1pagamentomensal5.DataValue != null) {

    if (crmForm.all.new_qtdedeparcelasmensais5.DataValue != null && crmForm.all.new_valordaparcelamensal5.DataValue != null)
        vlrMensal += crmForm.all.new_qtdedeparcelasmensais5.DataValue * crmForm.all.new_valordaparcelamensal5.DataValue;
}

if (crmForm.all.new_qtdedeparcelasmensais6.DataValue != null || crmForm.all.new_valordaparcelamensal6.DataValue != null || crmForm.all.new_datado1pagamentomensal6.DataValue != null) {

    if (crmForm.all.new_qtdedeparcelasmensais6.DataValue != null && crmForm.all.new_valordaparcelamensal6.DataValue != null)
        vlrMensal += crmForm.all.new_qtdedeparcelasmensais6.DataValue * crmForm.all.new_valordaparcelamensal6.DataValue;
}

if (crmForm.all.new_qtdedeparcelasmensais7.DataValue != null || crmForm.all.new_valordaparcelamensal7.DataValue != null || crmForm.all.new_datado1pagamentomensal7.DataValue != null) {

    if (crmForm.all.new_qtdedeparcelasmensais7.DataValue != null && crmForm.all.new_valordaparcelamensal7.DataValue != null)
        vlrMensal += crmForm.all.new_qtdedeparcelasmensais7.DataValue * crmForm.all.new_valordaparcelamensal7.DataValue;
}

if (crmForm.all.new_qtdedeparcelasmensais8.DataValue != null || crmForm.all.new_valordaparcelamensal8.DataValue != null || crmForm.all.new_datado1pagamentomensal8.DataValue != null) {

    if (crmForm.all.new_qtdedeparcelasmensais8.DataValue != null && crmForm.all.new_valordaparcelamensal8.DataValue != null)
        vlrMensal += crmForm.all.new_qtdedeparcelasmensais8.DataValue * crmForm.all.new_valordaparcelamensal8.DataValue;
}

if (crmForm.all.new_qtdedeparcelasmensais9.DataValue != null || crmForm.all.new_valordaparcelamensal9.DataValue != null || crmForm.all.new_datado1pagamentomensal9.DataValue != null) {

    if (crmForm.all.new_qtdedeparcelasmensais9.DataValue != null && crmForm.all.new_valordaparcelamensal9.DataValue != null)
        vlrMensal += crmForm.all.new_qtdedeparcelasmensais9.DataValue * crmForm.all.new_valordaparcelamensal9.DataValue;
}

if (crmForm.all.new_qtdedeparcelasmensais10.DataValue != null || crmForm.all.new_valordaparcelamensal10.DataValue != null || crmForm.all.new_datado1pagamentomensal10.DataValue != null) {

    if (crmForm.all.new_qtdedeparcelasmensais10.DataValue != null && crmForm.all.new_valordaparcelamensal10.DataValue != null)
        vlrMensal += crmForm.all.new_qtdedeparcelasmensais10.DataValue * crmForm.all.new_valordaparcelamensal10.DataValue;
}

if (crmForm.all.new_qtdedeparcelasmensais11.DataValue != null || crmForm.all.new_valordaparcelamensal11.DataValue != null || crmForm.all.new_datado1pagamentomensal11.DataValue != null) {

    if (crmForm.all.new_qtdedeparcelasmensais11.DataValue != null && crmForm.all.new_valordaparcelamensal11.DataValue != null)
        vlrMensal += crmForm.all.new_qtdedeparcelasmensais11.DataValue * crmForm.all.new_valordaparcelamensal11.DataValue;
}

if (crmForm.all.new_qtdedeparcelasmensais12.DataValue != null || crmForm.all.new_valordaparcelamensal12.DataValue != null || crmForm.all.new_datado1pagamentomensal12.DataValue != null) {

    if (crmForm.all.new_qtdedeparcelasmensais12.DataValue != null && crmForm.all.new_valordaparcelamensal12.DataValue != null)
        vlrMensal += crmForm.all.new_qtdedeparcelasmensais12.DataValue * crmForm.all.new_valordaparcelamensal12.DataValue;
}

if (crmForm.all.new_qtdedeparcelasmensais13.DataValue != null || crmForm.all.new_valordaparcelamensal13.DataValue != null || crmForm.all.new_datado1pagamentomensal13.DataValue != null) {

    if (crmForm.all.new_qtdedeparcelasmensais13.DataValue != null && crmForm.all.new_valordaparcelamensal13.DataValue != null)
        vlrMensal += crmForm.all.new_qtdedeparcelasmensais13.DataValue * crmForm.all.new_valordaparcelamensal13.DataValue;
}

if (crmForm.all.new_qtdedeparcelasmensais14.DataValue != null || crmForm.all.new_valordaparcelamensal14.DataValue != null || crmForm.all.new_datado1pagamentomensal14.DataValue != null) {

    if (crmForm.all.new_qtdedeparcelasmensais14.DataValue != null && crmForm.all.new_valordaparcelamensal14.DataValue != null)
        vlrMensal += crmForm.all.new_qtdedeparcelasmensais14.DataValue * crmForm.all.new_valordaparcelamensal14.DataValue;
}

if (crmForm.all.new_qtdedeparcelasmensais15.DataValue != null || crmForm.all.new_valordaparcelamensal15.DataValue != null || crmForm.all.new_datado1pagamentomensal15.DataValue != null) {

    if (crmForm.all.new_qtdedeparcelasmensais15.DataValue != null && crmForm.all.new_valordaparcelamensal15.DataValue != null)
        vlrMensal += crmForm.all.new_qtdedeparcelasmensais15.DataValue * crmForm.all.new_valordaparcelamensal15.DataValue;
}

if (crmForm.all.new_qtdedeparcelasmensais16.DataValue != null || crmForm.all.new_valordaparcelamensal16.DataValue != null || crmForm.all.new_datado1pagamentomensal16.DataValue != null) {

    if (crmForm.all.new_qtdedeparcelasmensais16.DataValue != null && crmForm.all.new_valordaparcelamensal16.DataValue != null)
        vlrMensal += crmForm.all.new_qtdedeparcelasmensais16.DataValue * crmForm.all.new_valordaparcelamensal16.DataValue;
}

if (crmForm.all.new_qtdedeparcelasmensais17.DataValue != null || crmForm.all.new_valordaparcelamensal17.DataValue != null || crmForm.all.new_datado1pagamentomensal17.DataValue != null) {

    if (crmForm.all.new_qtdedeparcelasmensais17.DataValue != null && crmForm.all.new_valordaparcelamensal17.DataValue != null)
        vlrMensal += crmForm.all.new_qtdedeparcelasmensais17.DataValue * crmForm.all.new_valordaparcelamensal17.DataValue;
}

if (crmForm.all.new_qtdedeparcelasmensais18.DataValue != null || crmForm.all.new_valordaparcelamensal18.DataValue != null || crmForm.all.new_datado1pagamentomensal18.DataValue != null) {

    if (crmForm.all.new_qtdedeparcelasmensais18.DataValue != null && crmForm.all.new_valordaparcelamensal18.DataValue != null)
        vlrMensal += crmForm.all.new_qtdedeparcelasmensais18.DataValue * crmForm.all.new_valordaparcelamensal18.DataValue;
}

if (crmForm.all.new_qtdedeparcelasmensais19.DataValue != null || crmForm.all.new_valordaparcelamensal19.DataValue != null || crmForm.all.new_datado1pagamentomensal19.DataValue != null) {

    if (crmForm.all.new_qtdedeparcelasmensais19.DataValue != null && crmForm.all.new_valordaparcelamensal19.DataValue != null)
        vlrMensal += crmForm.all.new_qtdedeparcelasmensais19.DataValue * crmForm.all.new_valordaparcelamensal19.DataValue;
}

crmForm.all.new_mensal.DataValue = vlrMensal;

var vltInter = 0;
if (crmForm.all.new_qtdeintermediarias.DataValue != null || crmForm.all.new_valosdaparcelaintermediaria.DataValue != null || crmForm.all.new_datadepagamentointermediaria.DataValue != null) {

    if (crmForm.all.new_qtdeintermediarias.DataValue != null && crmForm.all.new_valosdaparcelaintermediaria.DataValue != null)
        vltInter += crmForm.all.new_qtdeintermediarias.DataValue * crmForm.all.new_valosdaparcelaintermediaria.DataValue;
}

if (crmForm.all.new_qtdedeparcelasintermediarias1.DataValue != null || crmForm.all.new_valordaparcelaintermediaria1.DataValue != null || crmForm.all.new_datadepagamentointermediria1.DataValue != null) {

    if (crmForm.all.new_qtdedeparcelasintermediarias1.DataValue != null && crmForm.all.new_valordaparcelaintermediaria1.DataValue != null)
        vltInter += crmForm.all.new_qtdedeparcelasintermediarias1.DataValue * crmForm.all.new_valordaparcelaintermediaria1.DataValue;
}

if (crmForm.all.new_qtdedeparcelasintermediarias2.DataValue != null || crmForm.all.new_valordaparcelaintermediaria2.DataValue != null || crmForm.all.new_datadepagamentointermediaria2.DataValue != null) {

    if (crmForm.all.new_qtdedeparcelasintermediarias2.DataValue != null && crmForm.all.new_valordaparcelaintermediaria2.DataValue != null)
        vltInter += crmForm.all.new_qtdedeparcelasintermediarias2.DataValue * crmForm.all.new_valordaparcelaintermediaria2.DataValue;
}

crmForm.all.new_intermediaria.DataValue = vltInter;

crmForm.all.new_valortotalproposta.DataValue = vlrSinal + vlrMensal + vltInter + vlrTotalDifFinanciamento;

if (crmForm.all.new_financiamento.DataValue != null)
    crmForm.all.new_valortotalproposta.DataValue += crmForm.all.new_financiamento.DataValue;

if (crmForm.all.new_valordofgts.DataValue != null)
    crmForm.all.new_valortotalproposta.DataValue += crmForm.all.new_valordofgts.DataValue;

if (crmForm.all.new_recurso_proprio.DataValue != null)
    crmForm.all.new_valortotalproposta.DataValue += crmForm.all.new_recurso_proprio.DataValue;

if (crmForm.all.new_recurso_proprio_cliente.DataValue != null)
    crmForm.all.new_valortotalproposta.DataValue += crmForm.all.new_recurso_proprio_cliente.DataValue;

if (crmForm.all.new_valordescontopromocional.DataValue)
    crmForm.all.new_valortotalproposta.DataValue += crmForm.all.new_valordescontopromocional.DataValue;

if (crmForm.all.new_qtde_parcelas_tac.DataValue != null || crmForm.all.new_valor_parcela_tac.DataValue != null || crmForm.all.new_data_vencimento_parcela_tac.DataValue != null)
    crmForm.all.new_valortotalproposta.DataValue += (crmForm.all.new_qtde_parcelas_tac.DataValue * crmForm.all.new_valor_parcela_tac.DataValue);


if (crmForm.all.new_valor_parcela_dm.DataValue != null || crmForm.all.new_data_vencimento_parcela_dm.DataValue != null) {
    if (crmForm.all.new_qtde_parcelas_dm.DataValue == null) {
        crmForm.all.new_qtde_parcelas_dm.DataValue = 1;
        crmForm.all.new_periodicidade_dm.DataValue = 2;
    }
    crmForm.all.new_valortotalproposta.DataValue += (crmForm.all.new_qtde_parcelas_dm.DataValue * crmForm.all.new_valor_parcela_dm.DataValue);
}

if (crmForm.all.new_qtd_parcelas_pc.DataValue != null || crmForm.all.new_valor_parcela_pc.DataValue != null || crmForm.all.new_data_vencimento_pc.DataValue != null) {
    crmForm.all.new_valortotalproposta.DataValue += (crmForm.all.new_qtd_parcelas_pc.DataValue * crmForm.all.new_valor_parcela_pc.DataValue);
}

if (crmForm.all.new_qtd_parcelas_vm.DataValue != null || crmForm.all.new_valor_parcela_vm.DataValue != null || crmForm.all.new_data_vencimento_vm.DataValue != null) {
    crmForm.all.new_valortotalproposta.DataValue += (crmForm.all.new_qtd_parcelas_vm.DataValue * crmForm.all.new_valor_parcela_vm.DataValue);
}

if (crmForm.all.new_valor_parcela_ih.DataValue != null || crmForm.all.new_data_vencimento_parcela_ih.DataValue != null) {
    if (crmForm.all.new_qtde_parcelas_ih.DataValue == null) {
        crmForm.all.new_qtde_parcelas_ih.DataValue = QUANTIDADE;
        crmForm.all.new_periodicidade_ih.DataValue = MENSAL;
    }
    crmForm.all.new_valortotalproposta.DataValue += (crmForm.all.new_qtde_parcelas_ih.DataValue * crmForm.all.new_valor_parcela_ih.DataValue);
}

if (crmForm.all.new_valor_parcela_ti.DataValue != null || crmForm.all.new_data_vencimento_parcela_ti.DataValue != null) {
    if (crmForm.all.new_qtde_parcelas_ti.DataValue == null) {
        crmForm.all.new_qtde_parcelas_ti.DataValue = QUANTIDADE;
        crmForm.all.new_periodicidade_ti.DataValue = MENSAL;
    }
    crmForm.all.new_valortotalproposta.DataValue += (crmForm.all.new_qtde_parcelas_ti.DataValue * crmForm.all.new_valor_parcela_ti.DataValue);
}

if (crmForm.all.new_valor_parcela_dn.DataValue != null || crmForm.all.new_data_vencimento_parcela_dn.DataValue != null) {
    if (crmForm.all.new_qtde_parcelas_dn.DataValue == null) {
        crmForm.all.new_qtde_parcelas_dn.DataValue = QUANTIDADE;
        crmForm.all.new_periodicidade_dn.DataValue = MENSAL;
    }
    crmForm.all.new_valortotalproposta.DataValue += (crmForm.all.new_qtde_parcelas_dn.DataValue * crmForm.all.new_valor_parcela_dn.DataValue);
}
var valorTotalParcelaMP = 0;

if (crmForm.all.new_qtd_parcelas_mp1.DataValue != null || crmForm.all.new_valor_parcelas_mp1.DataValue != null || crmForm.all.new_data_vencimento_mp1.DataValue != null) {
    if (crmForm.all.new_qtd_parcelas_mp1.DataValue != null && crmForm.all.new_valor_parcelas_mp1.DataValue != null) {
        valorTotalParcelaMP += crmForm.all.new_qtd_parcelas_mp1.DataValue * crmForm.all.new_valor_parcelas_mp1.DataValue;
    }
}

if (crmForm.all.new_qtd_parcelas_mp2.DataValue != null || crmForm.all.new_valor_parcelas_mp2.DataValue != null || crmForm.all.new_data_vencimento_mp2.DataValue != null) {
    if (crmForm.all.new_qtd_parcelas_mp2.DataValue != null && crmForm.all.new_valor_parcelas_mp2.DataValue != null) {
        valorTotalParcelaMP += crmForm.all.new_qtd_parcelas_mp2.DataValue * crmForm.all.new_valor_parcelas_mp2.DataValue;
    }
}

if (crmForm.all.new_qtd_parcelas_mp3.DataValue != null || crmForm.all.new_valor_parcelas_mp3.DataValue != null || crmForm.all.new_data_vencimento_mp3.DataValue != null) {
    if (crmForm.all.new_qtd_parcelas_mp3.DataValue != null && crmForm.all.new_valor_parcelas_mp3.DataValue != null) {
        valorTotalParcelaMP += crmForm.all.new_qtd_parcelas_mp3.DataValue * crmForm.all.new_valor_parcelas_mp3.DataValue;
    }
}

if (crmForm.all.new_qtd_parcelas_mp4.DataValue != null || crmForm.all.new_valor_parcelas_mp4.DataValue != null || crmForm.all.new_data_vencimento_mp4.DataValue != null) {
    if (crmForm.all.new_qtd_parcelas_mp4.DataValue != null && crmForm.all.new_valor_parcelas_mp4.DataValue != null) {
        valorTotalParcelaMP += crmForm.all.new_qtd_parcelas_mp4.DataValue * crmForm.all.new_valor_parcelas_mp4.DataValue;
    }
}

if (crmForm.all.new_qtd_parcelas_mp5.DataValue != null || crmForm.all.new_valor_parcelas_mp5.DataValue != null || crmForm.all.new_data_vencimento_mp5.DataValue != null) {
    if (crmForm.all.new_qtd_parcelas_mp5.DataValue != null && crmForm.all.new_valor_parcelas_mp5.DataValue != null) {
        valorTotalParcelaMP += crmForm.all.new_qtd_parcelas_mp5.DataValue * crmForm.all.new_valor_parcelas_mp5.DataValue;
    }
}

if (valorTotalParcelaMP == 0) {
    //Soma valor parcelas FP
    if (crmForm.all.new_valor_parcelas_fp.DataValue != null)
        crmForm.all.new_valortotalproposta.DataValue += crmForm.all.new_valor_parcelas_fp.DataValue;
}
else {
    crmForm.all.new_valortotalproposta.DataValue += valorTotalParcelaMP;
    crmForm.all.new_valor_total_parcela_mp.DataValue = valorTotalParcelaMP;
}
