﻿var desabilitar = false;
if (crmForm.all.new_divisaosinal.DataValue == 0) {
    desabilitar = true;    
    crmForm.all.new_qtdedeparcelas.DataValue = null;
    crmForm.SetFieldReqLevel("new_qtdedeparcelas", false);
        
    crmForm.all.new_valordaparcela.DataValue = null;
    crmForm.SetFieldReqLevel("new_valordaparcela", false);
    
    crmForm.all.new_periodicidadesinal.DataValue = null;
    crmForm.SetFieldReqLevel("new_periodicidadesinal", false);
    
    crmForm.all.new_datadevencimento1parcela.DataValue = null;
    crmForm.SetFieldReqLevel("new_datadevencimento1parcela", false);
    
    crmForm.all.new_qtdedeparcelassinal1.DataValue = null;
    crmForm.SetFieldReqLevel("new_qtdedeparcelassinal1", false);
    
    crmForm.all.new_qtdedeparcelassinal2.DataValue = null;
    crmForm.SetFieldReqLevel("new_qtdedeparcelassinal2", false);
    
    crmForm.all.new_qtdedeparcelassinal3.DataValue = null;
    crmForm.SetFieldReqLevel("new_qtdedeparcelassinal3", false);
    
    crmForm.all.new_valordaparcelasinal1.DataValue = null;
    crmForm.SetFieldReqLevel("new_valordaparcelasinal1", false);

    crmForm.all.new_valordasparcelassinal2.DataValue = null;
    crmForm.SetFieldReqLevel("new_valordasparcelassinal2", false);

    crmForm.all.new_valordasparcelassinal3.DataValue = null;
    crmForm.SetFieldReqLevel("new_valordasparcelassinal3", false);

    crmForm.all.new_datadevencimento1parcelasinal1.DataValue = null;
    crmForm.SetFieldReqLevel("new_datadevencimento1parcelasinal1", false);
    
    crmForm.all.new_datadevencimento1parcelasinal2.DataValue = null;
    crmForm.SetFieldReqLevel("new_datadevencimento1parcelasinal2", false);
        
    crmForm.all.new_datadevencimento1parcelasinal3.DataValue = null;
    crmForm.SetFieldReqLevel("new_datadevencimento1parcelasinal3", false);
    
    crmForm.all.new_periodicidadesinal1.DataValue = null;
    crmForm.SetFieldReqLevel("new_periodicidadesinal1", false);
        
    crmForm.all.new_periodicidadesinal2.DataValue = null;
    crmForm.SetFieldReqLevel("new_periodicidadesinal2", false);
    
    crmForm.all.new_periodicidadesinal3.DataValue = null;
    crmForm.SetFieldReqLevel("new_periodicidadesinal3", false);
}
else {
    desabilitar = false;    
}

crmForm.all.new_qtdedeparcelas.Disabled = desabilitar;
crmForm.all.new_valordaparcela.Disabled = desabilitar;
crmForm.all.new_datadevencimento1parcela.Disabled = desabilitar;
crmForm.all.new_periodicidadesinal.Disabled = desabilitar;


crmForm.all.new_qtdedeparcelassinal1.Disabled = desabilitar;
crmForm.all.new_valordaparcelasinal1.Disabled = desabilitar;
crmForm.all.new_datadevencimento1parcelasinal1.Disabled = desabilitar;
crmForm.all.new_periodicidadesinal1.Disabled = desabilitar;


crmForm.all.new_qtdedeparcelassinal2.Disabled = desabilitar;
crmForm.all.new_valordasparcelassinal2.Disabled = desabilitar;
crmForm.all.new_datadevencimento1parcelasinal2.Disabled = desabilitar;
crmForm.all.new_periodicidadesinal2.Disabled = desabilitar;

crmForm.all.new_qtdedeparcelassinal3.Disabled = desabilitar;
crmForm.all.new_valordasparcelassinal3.Disabled = desabilitar;
crmForm.all.new_datadevencimento1parcelasinal3.Disabled = desabilitar;
crmForm.all.new_periodicidadesinal3.Disabled = desabilitar;

new_valortotalproposta_onchange0();