﻿crmForm.DesabilitaCamposConsistencia("DEVOLUCAO");
var requerido = crmForm.all.new_qtd_parcelas_devolucao.DataValue != null ||
     crmForm.all.new_valor_parcela_devolucao.DataValue != null ||
      crmForm.all.new_dat_primeira_parcela_devolucao.DataValue != null;
if (requerido && crmForm.all.new_mensal.DataValue != null && crmForm.all.new_mensal.DataValue > 0) {
    alert('Processo não aceita gerar parcelas mensais e fazer devolução ao mesmo tempo.');
    crmForm.all.new_qtd_parcelas_devolucao.DataValue = null;
}
else {
    crmForm.SetFieldReqLevel('new_qtd_parcelas_devolucao', requerido);
    crmForm.SetFieldReqLevel('new_valor_parcela_devolucao', requerido);
    crmForm.SetFieldReqLevel('new_dat_primeira_parcela_devolucao', requerido);
}