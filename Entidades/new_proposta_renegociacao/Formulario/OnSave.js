﻿try {    
    if (!formularioValido) {
        alert("Ocorreu um erro no formulário. Impossível salvar.");
        event.returnValue = false;
        return false;
    }
    
    crmForm.ValidarValorDescontoPropostaEspecial();
    crmForm.ValidaDatasPropostas();
    crmForm.ValidarValoresProposta();
    crmForm.ConfigurarCamposAtualizacao();
} 
catch (error) {
    alert("Ocorreu um erro no formulário.\n" + error.description);
    event.returnValue = false;
    return false;
}
