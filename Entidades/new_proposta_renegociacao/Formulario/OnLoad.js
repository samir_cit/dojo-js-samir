﻿formularioValido = true;
var dataAssinatura = null;
try {
    GrupoRenegociacao = {
        RENEGOCIACAO_SALDO_DEVEDOR: "1",
        FINANCIAMENTO_BANCARIO: "2",
        CESSAO_DIREITO_TOTAL: "3",
        CESSAO_DIREITO_PARCIAL: "4",
        SOMENTE_CRM: "5",
        DISTRATO: "6",
        RENEGOCIACAO_TAXAS: "7",
        RENEGOCIACAO_KITACABAMENTO: "8"
    }

    StatusCode = {
        RASCUNHO: "1",
        GANHA: "3",
        PERDIDA: "4"

    }

    TipoDeContrato = {
        UNIDADE: "1",
        GARAGEM: "2",
        BOX: "3",
        KITACABAMENTO: "4",
        SERVICO: "5"
    }

    TipoCampoBit = {
        SIM: true,
        NAO: false
    }

    TipoRenegociacao = {
        RENEGOCIACAO_IN: 900001
    }

    Mensagem = {
        VALIDACAO_VALOR_BLOQUEIO: "Valor da renegociação inválido! A soma do Valor Total Proposta com o Valor Pago não pode ser inferior ao de bloqueio calculado pelo SAP na consulta de saldo."
    }

    IsDescontoEspecial = false;

    crmForm.ConfigurarVisaoFormulario = function(indicadorDesabilitar) {
        crmForm.all.new_valortotalproposta.Disabled = indicadorDesabilitar;
        crmForm.all.new_accountid.Disabled = indicadorDesabilitar;
        crmForm.all.new_name.Disabled = indicadorDesabilitar;
        crmForm.all.new_tipodecotacao.Disabled = indicadorDesabilitar;
        crmForm.all.new_nome_grupo_renegociacao.Disabled = indicadorDesabilitar;
        crmForm.all.new_nome_tipo_renegociacao.Disabled = indicadorDesabilitar;
        crmForm.all.new_sinal.Disabled = indicadorDesabilitar;
        crmForm.all.new_mensal.Disabled = indicadorDesabilitar;
        crmForm.all.new_intermediaria.Disabled = indicadorDesabilitar;
        crmForm.all.new_total_parcela_df.Disabled = indicadorDesabilitar;
        crmForm.all.new_valor_venda.Disabled = indicadorDesabilitar;
        crmForm.all.new_valor_bloqueio.Disabled = indicadorDesabilitar;
        crmForm.all.new_valor_pago.Disabled = indicadorDesabilitar;
    }

    function OcultaParcelasAdicional() {
        crmForm.EscondeSecao("new_total_parcela_df", true);
        crmForm.EscondeSecao("new_valordescontopromocional", true);
        crmForm.EscondeSecao("new_qtd_parcelas_devolucao", true);
    }

    function OcutarParcelasMensais(ocultar) {
        crmForm.EscondeSecao("new_qtdedeparcelasmensais6", ocultar);
        crmForm.EscondeCampo("new_qtdedeparcelasmensais6", ocultar);
        crmForm.EscondeCampo("new_valordaparcelamensal6", ocultar);
        crmForm.EscondeCampo("new_datado1pagamentomensal6", ocultar);
        crmForm.EscondeCampo("new_aplicar_juros_mensal_7", ocultar);
        crmForm.EscondeCampo("new_desconto_mensal_7", ocultar);
        crmForm.EscondeCampo("new_acrescimo_mensal_7", ocultar);

        crmForm.EscondeSecao("new_qtdedeparcelasmensais7", ocultar);
        crmForm.EscondeCampo("new_qtdedeparcelasmensais7", ocultar);
        crmForm.EscondeCampo("new_valordaparcelamensal7", ocultar);
        crmForm.EscondeCampo("new_datado1pagamentomensal7", ocultar);
        crmForm.EscondeCampo("new_aplicar_juros_mensal_8", ocultar);
        crmForm.EscondeCampo("new_desconto_mensal_8", ocultar);
        crmForm.EscondeCampo("new_acrescimo_mensal_8", ocultar);

        crmForm.EscondeSecao("new_qtdedeparcelasmensais8", ocultar);
        crmForm.EscondeCampo("new_qtdedeparcelasmensais8", ocultar);
        crmForm.EscondeCampo("new_valordaparcelamensal8", ocultar);
        crmForm.EscondeCampo("new_datado1pagamentomensal8", ocultar);
        crmForm.EscondeCampo("new_aplicar_juros_mensal_9", ocultar);
        crmForm.EscondeCampo("new_desconto_mensal_9", ocultar);
        crmForm.EscondeCampo("new_acrescimo_mensal_9", ocultar);

        crmForm.EscondeSecao("new_qtdedeparcelasmensais9", ocultar);
        crmForm.EscondeCampo("new_qtdedeparcelasmensais9", ocultar);
        crmForm.EscondeCampo("new_valordaparcelamensal9", ocultar);
        crmForm.EscondeCampo("new_datado1pagamentomensal9", ocultar);
        crmForm.EscondeCampo("new_aplicar_juros_mensal_10", ocultar);
        crmForm.EscondeCampo("new_desconto_mensal_10", ocultar);
        crmForm.EscondeCampo("new_acrescimo_mensal_10", ocultar);

        crmForm.EscondeSecao("new_qtdedeparcelasmensais10", ocultar);
        crmForm.EscondeCampo("new_qtdedeparcelasmensais10", ocultar);
        crmForm.EscondeCampo("new_valordaparcelamensal10", ocultar);
        crmForm.EscondeCampo("new_datado1pagamentomensal10", ocultar);
        crmForm.EscondeCampo("new_aplicar_juros_mensal_11", ocultar);
        crmForm.EscondeCampo("new_desconto_mensal_11", ocultar);
        crmForm.EscondeCampo("new_acrescimo_mensal_11", ocultar);

        crmForm.EscondeSecao("new_qtdedeparcelasmensais11", ocultar);
        crmForm.EscondeCampo("new_qtdedeparcelasmensais11", ocultar);
        crmForm.EscondeCampo("new_valordaparcelamensal11", ocultar);
        crmForm.EscondeCampo("new_datado1pagamentomensal11", ocultar);
        crmForm.EscondeCampo("new_aplicar_juros_mensal_12", ocultar);
        crmForm.EscondeCampo("new_desconto_mensal_12", ocultar);
        crmForm.EscondeCampo("new_acrescimo_mensal_12", ocultar);

        crmForm.EscondeSecao("new_qtdedeparcelasmensais12", ocultar);
        crmForm.EscondeCampo("new_qtdedeparcelasmensais12", ocultar);
        crmForm.EscondeCampo("new_valordaparcelamensal12", ocultar);
        crmForm.EscondeCampo("new_datado1pagamentomensal12", ocultar);
        crmForm.EscondeCampo("new_aplicar_juros_mensal_13", ocultar);
        crmForm.EscondeCampo("new_desconto_mensal_13", ocultar);
        crmForm.EscondeCampo("new_acrescimo_mensal_13", ocultar);

        crmForm.EscondeSecao("new_qtdedeparcelasmensais13", ocultar);
        crmForm.EscondeCampo("new_qtdedeparcelasmensais13", ocultar);
        crmForm.EscondeCampo("new_valordaparcelamensal13", ocultar);
        crmForm.EscondeCampo("new_datado1pagamentomensal13", ocultar);
        crmForm.EscondeCampo("new_aplicar_juros_mensal_14", ocultar);
        crmForm.EscondeCampo("new_desconto_mensal_14", ocultar);
        crmForm.EscondeCampo("new_acrescimo_mensal_14", ocultar);

        crmForm.EscondeSecao("new_qtdedeparcelasmensais14", ocultar);
        crmForm.EscondeCampo("new_qtdedeparcelasmensais14", ocultar);
        crmForm.EscondeCampo("new_valordaparcelamensal14", ocultar);
        crmForm.EscondeCampo("new_datado1pagamentomensal14", ocultar);
        crmForm.EscondeCampo("new_aplicar_juros_mensal_15", ocultar);
        crmForm.EscondeCampo("new_desconto_mensal_15", ocultar);
        crmForm.EscondeCampo("new_acrescimo_mensal_15", ocultar);

        crmForm.EscondeSecao("new_qtdedeparcelasmensais15", ocultar);
        crmForm.EscondeCampo("new_qtdedeparcelasmensais15", ocultar);
        crmForm.EscondeCampo("new_valordaparcelamensal15", ocultar);
        crmForm.EscondeCampo("new_datado1pagamentomensal15", ocultar);
        crmForm.EscondeCampo("new_aplicar_juros_mensal_16", ocultar);
        crmForm.EscondeCampo("new_desconto_mensal_16", ocultar);
        crmForm.EscondeCampo("new_acrescimo_mensal_16", ocultar);

        crmForm.EscondeSecao("new_qtdedeparcelasmensais16", ocultar);
        crmForm.EscondeCampo("new_qtdedeparcelasmensais16", ocultar);
        crmForm.EscondeCampo("new_valordaparcelamensal16", ocultar);
        crmForm.EscondeCampo("new_datado1pagamentomensal16", ocultar);
        crmForm.EscondeCampo("new_aplicar_juros_mensal_17", ocultar);
        crmForm.EscondeCampo("new_desconto_mensal_17", ocultar);
        crmForm.EscondeCampo("new_acrescimo_mensal_17", ocultar);

        crmForm.EscondeSecao("new_qtdedeparcelasmensais17", ocultar);
        crmForm.EscondeCampo("new_qtdedeparcelasmensais17", ocultar);
        crmForm.EscondeCampo("new_valordaparcelamensal17", ocultar);
        crmForm.EscondeCampo("new_datado1pagamentomensal17", ocultar);
        crmForm.EscondeCampo("new_aplicar_juros_mensal_17", ocultar);
        crmForm.EscondeCampo("new_desconto_mensal_18", ocultar);
        crmForm.EscondeCampo("new_acrescimo_mensal_18", ocultar);

        crmForm.EscondeSecao("new_qtdedeparcelasmensais18", ocultar);
        crmForm.EscondeCampo("new_qtdedeparcelasmensais18", ocultar);
        crmForm.EscondeCampo("new_valordaparcelamensal18", ocultar);
        crmForm.EscondeCampo("new_datado1pagamentomensal18", ocultar);
        crmForm.EscondeCampo("new_aplicar_juros_mensal_19", ocultar);
        crmForm.EscondeCampo("new_desconto_mensal_19", ocultar);
        crmForm.EscondeCampo("new_acrescimo_mensal_19", ocultar);

        crmForm.EscondeSecao("new_qtdedeparcelasmensais19", ocultar);
        crmForm.EscondeCampo("new_qtdedeparcelasmensais19", ocultar);
        crmForm.EscondeCampo("new_valordaparcelamensal19", ocultar);
        crmForm.EscondeCampo("new_datado1pagamentomensal19", ocultar);
        crmForm.EscondeCampo("new_aplicar_juros_mensal_20", ocultar);
        crmForm.EscondeCampo("new_desconto_mensal_20", ocultar);
        crmForm.EscondeCampo("new_acrescimo_mensal_20", ocultar);
    }

    function OcultaParcelas() {
        crmForm.EscondeSecao("new_sinal", true);
        crmForm.EscondeSecao("new_valorsinalavista", true);
        crmForm.EscondeSecao("new_divisaosinal", true);
        crmForm.EscondeSecao("new_qtdedeparcelas", true);
        crmForm.EscondeSecao("new_qtdedeparcelassinal1", true);
        crmForm.EscondeSecao("new_qtdedeparcelassinal2", true);
        crmForm.EscondeSecao("new_qtdedeparcelassinal3", true);
        crmForm.EscondeSecao("new_intermediaria", true);
        crmForm.EscondeSecao("new_qtdeintermediarias", true);
        crmForm.EscondeSecao("new_qtdedeparcelasintermediarias1", true);
        crmForm.EscondeSecao("new_qtdedeparcelasintermediarias2", true);
        crmForm.EscondeSecao("new_financiamento", true);
        crmForm.EscondeSecao("new_valordofgts", true);
        crmForm.EscondeSecao("new_datavencimentoparcela_fgts", true);
        crmForm.EscondeSecao("new_recurso_proprio", true);
        crmForm.EscondeSecao("new_datavencimentoparcela_recursosproprios", true);
        crmForm.EscondeSecao("new_total_parcela_df", true);
        crmForm.EscondeSecao("new_qtd_parcela_df1", true);
        crmForm.EscondeSecao("new_qtd_parcela_df2", true);
        crmForm.EscondeSecao("new_qtd_parcela_df3", true);
        crmForm.EscondeSecao("new_qtd_parcela_df4", true);
        crmForm.EscondeSecao("new_qtd_parcela_df5", true);
        crmForm.EscondeSecao("new_qtd_parcela_df6", true);
        crmForm.EscondeSecao("new_qtd_parcela_df7", true);
        crmForm.EscondeSecao("new_qtd_parcela_df8", true);
        crmForm.EscondeSecao("new_qtde_parcelas_tac", true);
        crmForm.EscondeSecao("new_qtde_parcelas_dm", true);
        crmForm.EscondeSecao("new_qtd_parcelas_pc", true);
        crmForm.EscondeSecao("new_qtd_parcelas_vm", true);
        crmForm.EscondeSecao("new_qtde_parcelas_ih", true);
        crmForm.EscondeSecao("new_qtde_parcelas_dn", true);
        crmForm.EscondeSecao("new_qtde_parcelas_ti", true);
        crmForm.EscondeSecao("new_qtd_parcelas_mp1", true);
        crmForm.EscondeSecao("new_qtd_parcelas_mp2", true);
        crmForm.EscondeSecao("new_qtd_parcelas_mp3", true);
        crmForm.EscondeSecao("new_qtd_parcelas_mp4", true);
        crmForm.EscondeSecao("new_qtd_parcelas_mp5", true);
    }

    function OcultaParcelasKit() {
        crmForm.EscondeSecao("new_sinal", true);
        crmForm.EscondeSecao("new_valorsinalavista", true);
        crmForm.EscondeSecao("new_divisaosinal", true);
        crmForm.EscondeSecao("new_qtdedeparcelas", true);
        crmForm.EscondeSecao("new_qtdedeparcelassinal1", true);
        crmForm.EscondeSecao("new_qtdedeparcelassinal2", true);
        crmForm.EscondeSecao("new_qtdedeparcelassinal3", true);
        crmForm.EscondeSecao("new_intermediaria", true);
        crmForm.EscondeSecao("new_qtdeintermediarias", true);
        crmForm.EscondeSecao("new_qtdedeparcelasintermediarias1", true);
        crmForm.EscondeSecao("new_qtdedeparcelasintermediarias2", true);
        crmForm.EscondeSecao("new_recurso_proprio", true);
        crmForm.EscondeSecao("new_datavencimentoparcela_recursosproprios", true);
        crmForm.EscondeSecao("new_total_parcela_df", true);
        crmForm.EscondeSecao("new_qtd_parcela_df1", true);
        crmForm.EscondeSecao("new_qtd_parcela_df2", true);
        crmForm.EscondeSecao("new_qtd_parcela_df3", true);
        crmForm.EscondeSecao("new_qtd_parcela_df4", true);
        crmForm.EscondeSecao("new_qtd_parcela_df5", true);
        crmForm.EscondeSecao("new_qtd_parcela_df6", true);
        crmForm.EscondeSecao("new_qtd_parcela_df7", true);
        crmForm.EscondeSecao("new_qtd_parcela_df8", true);
        crmForm.EscondeSecao("new_qtde_parcelas_tac", true);
        crmForm.EscondeSecao("new_qtde_parcelas_dm", true);
        crmForm.EscondeSecao("new_qtde_parcelas_dn", true);
        crmForm.EscondeSecao("new_qtd_parcelas_mp1", true);
        crmForm.EscondeSecao("new_qtd_parcelas_mp2", true);
        crmForm.EscondeSecao("new_qtd_parcelas_mp3", true);
        crmForm.EscondeSecao("new_qtd_parcelas_mp4", true);
        crmForm.EscondeSecao("new_qtd_parcelas_mp5", true);
    }

    function OcultaCampos() {
        crmForm.EscondeCampo("new_tipo_parcela_financiamento", true);
        crmForm.SetFieldReqLevel("new_tipo_parcela_financiamento", false);
        crmForm.EscondeCampo("new_tipo_parcela_fgts", true);
        crmForm.SetFieldReqLevel("new_tipo_parcela_fgts", false);
    }

    function OcultaParcelasMP() {
        crmForm.EscondeSecao("new_qtd_parcelas_mp1", true);
        crmForm.EscondeSecao("new_qtd_parcelas_mp2", true);
        crmForm.EscondeSecao("new_qtd_parcelas_mp3", true);
        crmForm.EscondeSecao("new_qtd_parcelas_mp4", true);
        crmForm.EscondeSecao("new_qtd_parcelas_mp5", true);
    }

    function GrupoNaoPermitCriarProposta(grupoRenegociacaoId) {
        if (null == grupoRenegociacao)
            grupoRenegociacao = crmForm.ObterGrupoRenegociacao(grupoRenegociacaoId);

        return grupoRenegociacao.new_codigo_grupo == GrupoRenegociacao.SOMENTE_CRM;
    }

    crmForm.ObterPropostaPorOportunidade = function(oportunidadeId) {
        var rCmd = new RemoteCommand('DistratoRenegociacaoService', 'ObterPropostaPorOportunidade', '/MRVCustomizations/DistratoRenegociacao/');
        rCmd.SetParameter('oportunidadeId', oportunidadeId);
        var retorno = rCmd.Execute();

        if (crmForm.TratarRetornoRemoteCommand(retorno, "ObterPropostaPorOportunidade")) {
            if (retorno.ReturnValue.Mrv.Encontrou)
                return retorno.ReturnValue.Mrv;
        }
        return null;
    }

    crmForm.ObterGrupoRenegociacao = function(grupoRenegociacaoId) {
        if (null == grupoRenegociacaoId)
            return null;

        var rCmd = new RemoteCommand('DistratoRenegociacaoService', 'ObterGrupoRenegociacao', '/MRVCustomizations/DistratoRenegociacao/');
        rCmd.SetParameter('grupoRenegociacaoId', grupoRenegociacaoId[0].id);
        var retorno = rCmd.Execute();

        if (crmForm.TratarRetornoRemoteCommand(retorno, "ObterGrupoRenegociacao")) {
            if (retorno.ReturnValue.Mrv.new_grupo_renegociacaoid != null)
                grupoRenegociacao = retorno.ReturnValue.Mrv;
        }
        return grupoRenegociacao;
    }

    crmForm.ObterValorVenda = function(idRenegociacao) {
        if (idRenegociacao != null) {
            crmForm.all.new_valor_venda.Disabled = false;
            crmForm.all.new_valor_pago.Disabled = false;
            crmForm.all.new_valor_bloqueio.Disabled = false;
            try {
                var rCmd = new RemoteCommand('DistratoRenegociacaoService', 'ObterValoresSaldo', '/MRVCustomizations/DistratoRenegociacao/');
                rCmd.SetParameter('idRenegociacao', idRenegociacao);
                var retorno = rCmd.Execute();
                if (retorno.Success) {
                    if (!retorno.ReturnValue.Mrv.Sucesso) {
                        formularioValido = false;
                        alert(retorno.ReturnValue.Mrv.Mensagem + "\n" + "Não será possível salvar a proposta.");
                    }
                    if (retorno.ReturnValue.Mrv.ValorVenda && crmForm.all.new_tipodecotacao.DataValue == TipoDeContrato.UNIDADE) {
                        var valorVenda = retorno.ReturnValue.Mrv.ValorVenda;
                        crmForm.all.new_valor_venda.DataValue = parseFloat(valorVenda.toString().replace(',', '.'));
                    }
                    if (retorno.ReturnValue.Mrv.ValorPago && crmForm.all.new_tipodecotacao.DataValue == TipoDeContrato.UNIDADE) {
                        var valorpago = retorno.ReturnValue.Mrv.ValorPago;
                        crmForm.all.new_valor_pago.DataValue = parseFloat(valorpago.toString().replace(',', '.'));
                    }
                    if (retorno.ReturnValue.Mrv.ValorBloqueio && crmForm.all.new_tipodecotacao.DataValue == TipoDeContrato.UNIDADE) {
                        var valorbloqueio = retorno.ReturnValue.Mrv.ValorBloqueio;
                        crmForm.all.new_valor_bloqueio.DataValue = parseFloat(valorbloqueio.toString().replace(',', '.'));
                    }
                    if (retorno.ReturnValue.Mrv.SaldoMultaMora) {
                        var saldoMultaMora = retorno.ReturnValue.Mrv.SaldoMultaMora;
                        crmForm.all.new_saldo_multa_mora.DataValue = parseFloat(saldoMultaMora.toString().replace(',', '.'));
                    }

                    if (retorno.ReturnValue.Mrv.SaldoMultaMoraMP) {
                        var saldoMultaMoraMP = retorno.ReturnValue.Mrv.SaldoMultaMoraMP;
                        crmForm.all.new_saldo_multa_mora_mp.DataValue = parseFloat(saldoMultaMoraMP.toString().replace(',', '.'));
                    }
                    
                    if (retorno.ReturnValue.Mrv.ValorParcelasFP) {
                        var valorFP = retorno.ReturnValue.Mrv.ValorParcelasFP;
                        crmForm.all.new_valor_parcelas_fp.DataValue = parseFloat(valorFP.toString().replace(',', '.'));
                        new_valortotalproposta_onchange0();
                    }

                    if (retorno.ReturnValue.Mrv.ValorParcelasMP) {
                        var valorMP = retorno.ReturnValue.Mrv.ValorParcelasMP;
                        saldoParcelaMP = parseFloat(valorMP.toString().replace(',', '.'));
                    }

                    if (crmForm.FormType == TypeCreate) {
                        crmForm.all.new_desconto_especial.DataValue = null;
                        crmForm.all.new_desconto_especial.ForceSubmit = true;
                    }

                    if (retorno.ReturnValue.Mrv.DescontoEspecial) {
                        IsDescontoEspecial = true;

                        crmForm.all.new_desconto_especial.Disabled = true;

                        if (crmForm.all.statuscode.DataValue != StatusCode.GANHA) {
                            crmForm.all.new_desconto_especial.ForceSubmit = true;
                            crmForm.all.new_desconto_especial.DataValue = TipoCampoBit.NAO;
                        }

                    } else if (retorno.ReturnValue.Mrv.DescontoEspecial != undefined && !retorno.ReturnValue.Mrv.DescontoEspecial && crmForm.UsuarioPertenceEquipe(Equipe.NUCLEO_GESTAO_RENEGOCIACOES_CONTROLE_INADI)) {
                        crmForm.all.new_desconto_especial.Disabled = false;
                    } else if (retorno.ReturnValue.Mrv.DescontoEspecial != undefined && !retorno.ReturnValue.Mrv.DescontoEspecial && !crmForm.UsuarioPertenceEquipe(Equipe.NUCLEO_GESTAO_RENEGOCIACOES_CONTROLE_INADI)) {
                        crmForm.all.new_desconto_especial.Disabled = true;
                    } else {
                        crmForm.all.new_desconto_especial.Disabled = true;
                        crmForm.all.new_desconto_especial.DataValue = null;
                    }
                } else {
                    crmForm.all.new_valor_venda.DataValue = 0;
                    crmForm.all.new_valor_pago.DataValue = 0;
                    crmForm.all.new_valor_bloqueio.DataValue = 0;
                    crmForm.all.new_saldo_multa_mora.DataValue = 0;
                }
            } catch (error) {
                crmForm.all.new_valor_venda.DataValue = 0;
                crmForm.all.new_valor_bloqueio.DataValue = 0;
                crmForm.all.new_valor_pago.DataValue = 0;
                crmForm.all.new_saldo_multa_mora.DataValue = 0;
                formularioValido = false;
                alert("Ocorreu um erro ao obter o valor de venda do contrato.\n" + error.description);
            }
            crmForm.all.new_valor_venda.Disabled = true;
            crmForm.all.new_valor_pago.Disabled = true;
            crmForm.all.new_valor_bloqueio.Disabled = true;
        }
    }

    crmForm.ObterTipoRenegociacao = function(tipoRenegociacaoId) {
        if (null == tipoRenegociacaoId)
            return null;

        var rCmd = new RemoteCommand('DistratoRenegociacaoService', 'ObterTipoRenegociacao', '/MRVCustomizations/DistratoRenegociacao/');
        rCmd.SetParameter('tipoRenegociacaoId', tipoRenegociacaoId[0].id);
        var retorno = rCmd.Execute();

        if (crmForm.TratarRetornoRemoteCommand(retorno, "ObterTipoRenegociacao")) {
            if (retorno.ReturnValue.Mrv.new_tipo_renegociacaoid != null)
                return retorno.ReturnValue.Mrv;
        }
        return tipoRenegociacao;
    }

    InformacoesProposta = function() {
        var objeto = this;
        this.GrupoRenegociacao = null;
        this.TipoRenegociacao = null;
        this.CodigoTipoRenegociacao = 0;
        this.IndicadorRenegociacaoIN = false;

        objeto.ValidarRenegociacaoIN = function() {
            return this.CodigoTipoRenegociacao == TipoRenegociacao.RENEGOCIACAO_IN;
        }
    }

    var codigoTipoRenegociacao = 0;

    function ObterInformacaoTipo() {
        if (null == grupoRenegociacao)
            grupoRenegociacao = crmForm.ObterGrupoRenegociacao(janelaRenegociacao.all.new_grupo_renegociacao_id.DataValue);        

        if (null == tipoRenegociacao)
            tipoRenegociacao = crmForm.ObterTipoRenegociacao(janelaRenegociacao.all.new_tipo_renegociacao_id.DataValue);

        codigoTipoRenegociacao = parseInt(tipoRenegociacao.new_codigo_tipo_renegociacao);
        Informacoes.GrupoRenegociacao = grupoRenegociacao;
        Informacoes.TipoRenegociacao = tipoRenegociacao;
        Informacoes.CodigoTipoRenegociacao = codigoTipoRenegociacao;
    }

    function PreencheCamposDeAcordoComOportunidade() {

        crmForm.all.new_name.DataValue = janelaRenegociacao.new_name.DataValue;
        crmForm.all.new_tipodecotacao.DataValue = janelaRenegociacao.all.new_tipo_contrato.DataValue;

        crmForm.all.new_nome_grupo_renegociacao.DataValue = janelaRenegociacao.all.new_grupo_renegociacao_id.DataValue[0].name;
        crmForm.all.new_nome_tipo_renegociacao.DataValue = janelaRenegociacao.all.new_tipo_renegociacao_id.DataValue[0].name;

        if (grupoRenegociacao.new_codigo_grupo == GrupoRenegociacao.CESSAO_DIREITO_TOTAL ||
            (grupoRenegociacao.new_codigo_grupo == GrupoRenegociacao.CESSAO_DIREITO_PARCIAL &&
            (codigoTipoRenegociacao >= 6 && codigoTipoRenegociacao <= 13))) {
            crmForm.all.new_accountid.DataValue = janelaRenegociacao.all.new_novoclienteid.DataValue;
        } else
            crmForm.all.new_accountid.DataValue = janelaRenegociacao.all.new_accountid.DataValue;
    }

    var validationGroups = new Array();

    function AddGroup(groupName, field) {
        var currentGroup;

        if (validationGroups[groupName] == null) {
            validationGroups[groupName] = new Array();
        }

        currentGroup = validationGroups[groupName];

        currentGroup[currentGroup.length] = field;
        currentGroup[currentGroup.length - 1].attachEvent("onchange", field.onchange);
        currentGroup[currentGroup.length - 1].unchangedRequiredLevel = field.RequiredLevel;
        field.ValidationGroupName = groupName;

        field.onchange = function() {
            UpdateValidationStatus(groupName);
            if (currentGroup[currentGroup.length - 1].changeEvent != null)
                currentGroup[currentGroup.length - 1].changeEvent();
        }
    }

    function UpdateValidationStatus(groupName) {
        var group = validationGroups[groupName];
        var allClear = true;

        for (var i = 0; i < group.length; i++) {
            if (group[i].DataValue != null) {
                allClear = false;
                break;
            }
        }

        for (var i = 0; i < group.length; i++) {
            if (!allClear)
                crmForm.SetFieldReqLevel(group[i].id, true);
            else
                crmForm.SetFieldReqLevel(group[i].id, false);
        }

    }

    function AtivarGrupos() {
        if (crmForm.FormType == TypeUpdate) {
            UpdateValidationStatus('SINAL_A_VISTA');
            UpdateValidationStatus('DIVISAO_SINAL_D1');
            UpdateValidationStatus('DIVISAO_SINAL_D2');
            UpdateValidationStatus('DIVISAO_SINAL_D3');
            UpdateValidationStatus('DIVISAO_SINAL_D4');
            UpdateValidationStatus('MENSAL_M1');
            UpdateValidationStatus('MENSAL_M2');
            UpdateValidationStatus('MENSAL_M3');
            UpdateValidationStatus('MENSAL_M4');
            UpdateValidationStatus('MENSAL_M5');
            UpdateValidationStatus('MENSAL_M6');
            UpdateValidationStatus('MENSAL_M7');
            UpdateValidationStatus('MENSAL_M8');
            UpdateValidationStatus('MENSAL_M9');
            UpdateValidationStatus('MENSAL_M10');
            UpdateValidationStatus('MENSAL_M11');
            UpdateValidationStatus('MENSAL_M12');
            UpdateValidationStatus('MENSAL_M13');
            UpdateValidationStatus('MENSAL_M14');
            UpdateValidationStatus('MENSAL_M15');
            UpdateValidationStatus('MENSAL_M16');
            UpdateValidationStatus('MENSAL_M17');
            UpdateValidationStatus('MENSAL_M18');
            UpdateValidationStatus('MENSAL_M19');
            UpdateValidationStatus('MENSAL_M20');

            UpdateValidationStatus('DIVISAO_INTERMEDIARIA_I1');
            UpdateValidationStatus('DIVISAO_INTERMEDIARIA_I2');
            UpdateValidationStatus('DIVISAO_INTERMEDIARIA_I3');
            UpdateValidationStatus('FINANCIAMENTO');
            UpdateValidationStatus('FGTS');
            UpdateValidationStatus('RECURSO_PROPRIO');
            UpdateValidationStatus('RECURSO_PROPRIO_CLIENTE');
            UpdateValidationStatus('DF_1');
            UpdateValidationStatus('DF_2');
            UpdateValidationStatus('DF_3');
            UpdateValidationStatus('DF_4');
            UpdateValidationStatus('DF_5');
            UpdateValidationStatus('DF_6');
            UpdateValidationStatus('DF_7');
            UpdateValidationStatus('DF_8');
            UpdateValidationStatus('TAC');
            UpdateValidationStatus('DM');
            UpdateValidationStatus('PC');
            UpdateValidationStatus('VM');
            UpdateValidationStatus('DN');
            UpdateValidationStatus('IH');
            UpdateValidationStatus('MP1');
            UpdateValidationStatus('MP2');
            UpdateValidationStatus('MP3');
            UpdateValidationStatus('MP4');
            UpdateValidationStatus('MP5');
        }
    }

    crmForm.ConfigurarIsencaoTotalMultaMora = function() {
        var desabilitar = false;
        if (crmForm.all.new_isencao_total.DataValue == TipoCampoBit.SIM) {
            desabilitar = true;
            var tipoCampoBit = TipoCampoBit.NAO;
            crmForm.all.new_aplicar_juros_sinal_1.DataValue = tipoCampoBit;
            crmForm.all.new_aplicar_juros_sinal_2.DataValue = tipoCampoBit;
            crmForm.all.new_aplicar_juros_sinal_3.DataValue = tipoCampoBit;
            crmForm.all.new_aplicar_juros_sinal_4.DataValue = tipoCampoBit;

            crmForm.all.new_aplicar_juros_mensal_1.DataValue = tipoCampoBit;
            crmForm.all.new_aplicar_juros_mensal_2.DataValue = tipoCampoBit;
            crmForm.all.new_aplicar_juros_mensal_3.DataValue = tipoCampoBit;
            crmForm.all.new_aplicar_juros_mensal_4.DataValue = tipoCampoBit;
            crmForm.all.new_aplicar_juros_mensal_5.DataValue = tipoCampoBit;
            crmForm.all.new_aplicar_juros_mensal_6.DataValue = tipoCampoBit;
            crmForm.all.new_aplicar_juros_mensal_7.DataValue = tipoCampoBit;
            crmForm.all.new_aplicar_juros_mensal_8.DataValue = tipoCampoBit;
            crmForm.all.new_aplicar_juros_mensal_9.DataValue = tipoCampoBit;
            crmForm.all.new_aplicar_juros_mensal_10.DataValue = tipoCampoBit;
            crmForm.all.new_aplicar_juros_mensal_11.DataValue = tipoCampoBit;
            crmForm.all.new_aplicar_juros_mensal_12.DataValue = tipoCampoBit;
            crmForm.all.new_aplicar_juros_mensal_13.DataValue = tipoCampoBit;
            crmForm.all.new_aplicar_juros_mensal_14.DataValue = tipoCampoBit;
            crmForm.all.new_aplicar_juros_mensal_15.DataValue = tipoCampoBit;
            crmForm.all.new_aplicar_juros_mensal_16.DataValue = tipoCampoBit;
            crmForm.all.new_aplicar_juros_mensal_17.DataValue = tipoCampoBit;
            crmForm.all.new_aplicar_juros_mensal_18.DataValue = tipoCampoBit;
            crmForm.all.new_aplicar_juros_mensal_19.DataValue = tipoCampoBit;
            crmForm.all.new_aplicar_juros_mensal_20.DataValue = tipoCampoBit;

            crmForm.all.new_aplicar_juros_dif_1.DataValue = tipoCampoBit;
            crmForm.all.new_aplicar_juros_dif_2.DataValue = tipoCampoBit;
            crmForm.all.new_aplicar_juros_dif_3.DataValue = tipoCampoBit;
            crmForm.all.new_aplicar_juros_dif_4.DataValue = tipoCampoBit;
            crmForm.all.new_aplicar_juros_dif_5.DataValue = tipoCampoBit;
            crmForm.all.new_aplicar_juros_dif_6.DataValue = tipoCampoBit;
            crmForm.all.new_aplicar_juros_dif_7.DataValue = tipoCampoBit;
            crmForm.all.new_aplicar_juros_dif_8.DataValue = tipoCampoBit;

            crmForm.all.new_aplicar_juros_intermediaria_1.DataValue = tipoCampoBit;
            crmForm.all.new_aplicar_juros_intermediaria_2.DataValue = tipoCampoBit;
            crmForm.all.new_aplicar_juros_intermediaria_3.DataValue = tipoCampoBit;

            crmForm.all.new_aplicar_juros_financiamento.DataValue = tipoCampoBit;
            crmForm.all.new_aplicar_juros_fgts.DataValue = tipoCampoBit;
            crmForm.all.new_aplicar_juros_desc_promocional.DataValue = tipoCampoBit;
            crmForm.all.new_aplicar_juros_dev_valores.DataValue = tipoCampoBit;

            crmForm.all.new_aplicar_juros_pc.DataValue = tipoCampoBit;
            crmForm.all.new_aplicar_juros_vm.DataValue = tipoCampoBit;

            crmForm.all.new_aplicar_juros_mp1.DataValue = tipoCampoBit;
            crmForm.all.new_aplicar_juros_mp2.DataValue = tipoCampoBit;
            crmForm.all.new_aplicar_juros_mp3.DataValue = tipoCampoBit;
            crmForm.all.new_aplicar_juros_mp4.DataValue = tipoCampoBit;
            crmForm.all.new_aplicar_juros_mp5.DataValue = tipoCampoBit;
        }

        crmForm.all.new_aplicar_juros_sinal_1.Disabled = desabilitar;
        crmForm.all.new_aplicar_juros_sinal_2.Disabled = desabilitar;
        crmForm.all.new_aplicar_juros_sinal_3.Disabled = desabilitar;
        crmForm.all.new_aplicar_juros_sinal_4.Disabled = desabilitar;

        crmForm.all.new_aplicar_juros_mensal_1.Disabled = desabilitar;
        crmForm.all.new_aplicar_juros_mensal_2.Disabled = desabilitar;
        crmForm.all.new_aplicar_juros_mensal_3.Disabled = desabilitar;
        crmForm.all.new_aplicar_juros_mensal_4.Disabled = desabilitar;
        crmForm.all.new_aplicar_juros_mensal_5.Disabled = desabilitar;
        crmForm.all.new_aplicar_juros_mensal_6.Disabled = desabilitar;
        crmForm.all.new_aplicar_juros_mensal_7.Disabled = desabilitar;
        crmForm.all.new_aplicar_juros_mensal_8.Disabled = desabilitar;
        crmForm.all.new_aplicar_juros_mensal_9.Disabled = desabilitar;
        crmForm.all.new_aplicar_juros_mensal_10.Disabled = desabilitar;
        crmForm.all.new_aplicar_juros_mensal_11.Disabled = desabilitar;
        crmForm.all.new_aplicar_juros_mensal_12.Disabled = desabilitar;
        crmForm.all.new_aplicar_juros_mensal_13.Disabled = desabilitar;
        crmForm.all.new_aplicar_juros_mensal_14.Disabled = desabilitar;
        crmForm.all.new_aplicar_juros_mensal_15.Disabled = desabilitar;
        crmForm.all.new_aplicar_juros_mensal_16.Disabled = desabilitar;
        crmForm.all.new_aplicar_juros_mensal_17.Disabled = desabilitar;
        crmForm.all.new_aplicar_juros_mensal_18.Disabled = desabilitar;
        crmForm.all.new_aplicar_juros_mensal_19.Disabled = desabilitar;
        crmForm.all.new_aplicar_juros_mensal_20.Disabled = desabilitar;

        crmForm.all.new_aplicar_juros_dif_1.Disabled = desabilitar;
        crmForm.all.new_aplicar_juros_dif_2.Disabled = desabilitar;
        crmForm.all.new_aplicar_juros_dif_3.Disabled = desabilitar;
        crmForm.all.new_aplicar_juros_dif_4.Disabled = desabilitar;
        crmForm.all.new_aplicar_juros_dif_5.Disabled = desabilitar;
        crmForm.all.new_aplicar_juros_dif_6.Disabled = desabilitar;
        crmForm.all.new_aplicar_juros_dif_7.Disabled = desabilitar;
        crmForm.all.new_aplicar_juros_dif_8.Disabled = desabilitar;

        crmForm.all.new_aplicar_juros_intermediaria_1.Disabled = desabilitar;
        crmForm.all.new_aplicar_juros_intermediaria_2.Disabled = desabilitar;
        crmForm.all.new_aplicar_juros_intermediaria_3.Disabled = desabilitar;

        crmForm.all.new_aplicar_juros_financiamento.Disabled = desabilitar;
        crmForm.all.new_aplicar_juros_fgts.Disabled = desabilitar;
        crmForm.all.new_aplicar_juros_desc_promocional.Disabled = desabilitar;
        crmForm.all.new_aplicar_juros_dev_valores.Disabled = desabilitar;

        crmForm.all.new_aplicar_juros_pc.Disabled = desabilitar;
        crmForm.all.new_aplicar_juros_vm.Disabled = desabilitar;

        crmForm.all.new_aplicar_juros_mp1.Disabled = desabilitar;
        crmForm.all.new_aplicar_juros_mp2.Disabled = desabilitar;
        crmForm.all.new_aplicar_juros_mp3.Disabled = desabilitar;
        crmForm.all.new_aplicar_juros_mp4.Disabled = desabilitar;
        crmForm.all.new_aplicar_juros_mp5.Disabled = desabilitar;

        crmForm.ConfigurarCamposConsistencia();
    }

    function loadScript() {
        var oScript = document.createElement('script');
        oScript.type = "text/javascript";
        oScript.src = "/_static/Base.js";

        var oHead = document.getElementsByTagName('head')[0];
        oHead.appendChild(oScript);

        oScript.onreadystatechange = function() {
            if (this.readyState == 'complete' || this.readyState == 'loaded')
                Load();
        }
    }

    crmForm.ConfigurarParaUpdate = function() {
        if (null != janelaRenegociacao) {
            if (janelaRenegociacao.all.new_codigo_renegociacao_sap6.DataValue != null)
                crmForm.DesabilitarFormulario(true);
            else {
                var proposta = crmForm.ObterPropostaPorOportunidade(janelaRenegociacao.ObjectId);
                if (null != proposta) {
                    if (proposta.statuscode == StatusCode.GANHA)
                        crmForm.DesabilitarFormulario(true);
                    else {
                        crmForm.ConfigurarVisaoFormulario(true);
                        crmForm.all.new_divisaosinal.FireOnChange();
                    }
                }
            }
        }
    }

    crmForm.ObtemDataAssinaturaWebService = function() {
        var rCmd = new RemoteCommand('MrvService', 'ObterDataAssinaturaCliente', '/MRVCustomizations/');
        rCmd.SetParameter('oportunidadeId', crmForm.all.new_oportunidadeid.DataValue[0].id);
        var retorno = rCmd.Execute();

        if (crmForm.TratarRetornoRemoteCommand(retorno, "ObterDataAssinaturaCliente")) {
            if (retorno.ReturnValue.Mrv.Mensagem != null) {
                alert(retorno.ReturnValue.Mrv.Mensagem);
            } else {
                dataAssinatura = retorno.ReturnValue.Mrv.DataAssinatura;
            }
        }
    }


    crmForm.ValidaDatasPropostas = function() {

        var dataAssinaturaConvertida = new Date(dataAssinatura);
        var exibirAlerta = false;
        if ((crmForm.all.new_datadopagamento.DataValue != null) && (crmForm.all.new_datadopagamento.DataValue < dataAssinaturaConvertida)) {
            crmForm.all.new_datadopagamento.DataValue = null;
            crmForm.all.new_datadopagamento.SetFocus();
            exibirAlerta = true;
        }
        if ((crmForm.all.new_datadevencimento1parcela.DataValue != null) && (crmForm.all.new_datadevencimento1parcela.DataValue < dataAssinaturaConvertida)) {
            crmForm.all.new_datadevencimento1parcela.DataValue = null;
            crmForm.all.new_datadevencimento1parcela.SetFocus();
            exibirAlerta = true;
        }

        if ((crmForm.all.new_datadevencimento1parcelasinal1.DataValue != null) && (crmForm.all.new_datadevencimento1parcelasinal1.DataValue < dataAssinaturaConvertida)) {
            crmForm.all.new_datadevencimento1parcelasinal1.DataValue = null;
            crmForm.all.new_datadevencimento1parcelasinal1.SetFocus();
            exibirAlerta = true;
        }

        if ((crmForm.all.new_datadevencimento1parcelasinal2.DataValue != null) && (crmForm.all.new_datadevencimento1parcelasinal2.DataValue < dataAssinaturaConvertida)) {
            crmForm.all.new_datadevencimento1parcelasinal2.DataValue = null;
            crmForm.all.new_datadevencimento1parcelasinal2.SetFocus();
            exibirAlerta = true;
        }
        if ((crmForm.all.new_datadevencimento1parcelasinal3.DataValue != null) && (crmForm.all.new_datadevencimento1parcelasinal3.DataValue < dataAssinaturaConvertida)) {
            crmForm.all.new_datadevencimento1parcelasinal3.DataValue = null;
            crmForm.all.new_datadevencimento1parcelasinal3.SetFocus();
            exibirAlerta = true;
        }
        if ((crmForm.all.new_dat_vencimento_parceladf1.DataValue != null) && (crmForm.all.new_dat_vencimento_parceladf1.DataValue < dataAssinaturaConvertida)) {
            crmForm.all.new_dat_vencimento_parceladf1.DataValue = null;
            crmForm.all.new_dat_vencimento_parceladf1.SetFocus();
            exibirAlerta = true;
        }
        if ((crmForm.all.new_dat_vencimento_parceladf2.DataValue != null) && (crmForm.all.new_dat_vencimento_parceladf2.DataValue < dataAssinaturaConvertida)) {
            crmForm.all.new_dat_vencimento_parceladf2.DataValue = null;
            crmForm.all.new_dat_vencimento_parceladf2.SetFocus();
            exibirAlerta = true;
        }
        if ((crmForm.all.new_dat_vencimento_parceladf3.DataValue != null) && (crmForm.all.new_dat_vencimento_parceladf3.DataValue < dataAssinaturaConvertida)) {
            crmForm.all.new_dat_vencimento_parceladf3.DataValue = null;
            crmForm.all.new_dat_vencimento_parceladf3.SetFocus();
            exibirAlerta = true;
        }
        if ((crmForm.all.new_dat_vencimento_parceladf4.DataValue != null) && (crmForm.all.new_dat_vencimento_parceladf4.DataValue < dataAssinaturaConvertida)) {
            crmForm.all.new_dat_vencimento_parceladf4.DataValue = null;
            crmForm.all.new_dat_vencimento_parceladf4.SetFocus();
            exibirAlerta = true;
        }
        if ((crmForm.all.new_dat_vencimento_parceladf5.DataValue != null) && (crmForm.all.new_dat_vencimento_parceladf5.DataValue < dataAssinaturaConvertida)) {
            crmForm.all.new_dat_vencimento_parceladf5.DataValue = null;
            crmForm.all.new_dat_vencimento_parceladf5.SetFocus();
            exibirAlerta = true;
        }
        if ((crmForm.all.new_dat_vencimento_parceladf6.DataValue != null) && (crmForm.all.new_dat_vencimento_parceladf6.DataValue < dataAssinaturaConvertida)) {
            crmForm.all.new_dat_vencimento_parceladf6.DataValue = null;
            crmForm.all.new_dat_vencimento_parceladf6.SetFocus();
            exibirAlerta = true;
        }
        if ((crmForm.all.new_dat_vencimento_parceladf7.DataValue != null) && (crmForm.all.new_dat_vencimento_parceladf7.DataValue < dataAssinaturaConvertida)) {
            crmForm.all.new_dat_vencimento_parceladf7.DataValue = null;
            crmForm.all.new_dat_vencimento_parceladf7.SetFocus();
            exibirAlerta = true;
        }
        if ((crmForm.all.new_dat_vencimento_parceladf8.DataValue != null) && (crmForm.all.new_dat_vencimento_parceladf8.DataValue < dataAssinaturaConvertida)) {
            crmForm.all.new_dat_vencimento_parceladf8.DataValue = null;
            crmForm.all.new_dat_vencimento_parceladf8.SetFocus();
            exibirAlerta = true;
        }
        if ((crmForm.all.new_datadepagamentomensal.DataValue != null) && (crmForm.all.new_datadepagamentomensal.DataValue < dataAssinaturaConvertida)) {
            crmForm.all.new_datadepagamentomensal.DataValue = null;
            crmForm.all.new_datadepagamentomensal.SetFocus();
            exibirAlerta = true;
        }
        if ((crmForm.all.new_datado1pagamentomensal1.DataValue != null) && (crmForm.all.new_datado1pagamentomensal1.DataValue < dataAssinaturaConvertida)) {
            crmForm.all.new_datado1pagamentomensal1.DataValue = null;
            crmForm.all.new_datado1pagamentomensal1.SetFocus();
            exibirAlerta = true;
        }
        if ((crmForm.all.new_datado1pagamentomensal2.DataValue != null) && (crmForm.all.new_datado1pagamentomensal2.DataValue < dataAssinaturaConvertida)) {
            crmForm.all.new_datado1pagamentomensal2.DataValue = null;
            crmForm.all.new_datado1pagamentomensal2.SetFocus();
            exibirAlerta = true;
        }
        if ((crmForm.all.new_datado1pagamentomensal3.DataValue != null) && (crmForm.all.new_datado1pagamentomensal3.DataValue < dataAssinaturaConvertida)) {
            crmForm.all.new_datado1pagamentomensal3.DataValue = null;
            crmForm.all.new_datado1pagamentomensal3.SetFocus();
            exibirAlerta = true;
        }
        if ((crmForm.all.new_datado1pagamentomensal4.DataValue != null) && (crmForm.all.new_datado1pagamentomensal4.DataValue < dataAssinaturaConvertida)) {
            crmForm.all.new_datado1pagamentomensal4.DataValue = null;
            crmForm.all.new_datado1pagamentomensal4.SetFocus();
            exibirAlerta = true;
        }
        if ((crmForm.all.new_datado1pagamentomensal5.DataValue != null) && (crmForm.all.new_datado1pagamentomensal5.DataValue < dataAssinaturaConvertida)) {
            crmForm.all.new_datado1pagamentomensal5.DataValue = null;
            crmForm.all.new_datado1pagamentomensal5.SetFocus();
            exibirAlerta = true;
        }

        if ((crmForm.all.new_datado1pagamentomensal6.DataValue != null) && (crmForm.all.new_datado1pagamentomensal6.DataValue < dataAssinaturaConvertida)) {
            crmForm.all.new_datado1pagamentomensal6.DataValue = null;
            crmForm.all.new_datado1pagamentomensal6.SetFocus();
            exibirAlerta = true;
        }
        if ((crmForm.all.new_datado1pagamentomensal7.DataValue != null) && (crmForm.all.new_datado1pagamentomensal7.DataValue < dataAssinaturaConvertida)) {
            crmForm.all.new_datado1pagamentomensal7.DataValue = null;
            crmForm.all.new_datado1pagamentomensal7.SetFocus();
            exibirAlerta = true;
        }
        if ((crmForm.all.new_datado1pagamentomensal8.DataValue != null) && (crmForm.all.new_datado1pagamentomensal8.DataValue < dataAssinaturaConvertida)) {
            crmForm.all.new_datado1pagamentomensal8.DataValue = null;
            crmForm.all.new_datado1pagamentomensal8.SetFocus();
            exibirAlerta = true;
        }
        if ((crmForm.all.new_datado1pagamentomensal9.DataValue != null) && (crmForm.all.new_datado1pagamentomensal9.DataValue < dataAssinaturaConvertida)) {
            crmForm.all.new_datado1pagamentomensal9.DataValue = null;
            crmForm.all.new_datado1pagamentomensal9.SetFocus();
            exibirAlerta = true;
        }
        if ((crmForm.all.new_datado1pagamentomensal10.DataValue != null) && (crmForm.all.new_datado1pagamentomensal10.DataValue < dataAssinaturaConvertida)) {
            crmForm.all.new_datado1pagamentomensal10.DataValue = null;
            crmForm.all.new_datado1pagamentomensal10.SetFocus();
            exibirAlerta = true;
        }
        if ((crmForm.all.new_datado1pagamentomensal11.DataValue != null) && (crmForm.all.new_datado1pagamentomensal11.DataValue < dataAssinaturaConvertida)) {
            crmForm.all.new_datado1pagamentomensal11.DataValue = null;
            crmForm.all.new_datado1pagamentomensal11.SetFocus();
            exibirAlerta = true;
        }
        if ((crmForm.all.new_datado1pagamentomensal12.DataValue != null) && (crmForm.all.new_datado1pagamentomensal12.DataValue < dataAssinaturaConvertida)) {
            crmForm.all.new_datado1pagamentomensal12.DataValue = null;
            crmForm.all.new_datado1pagamentomensal12.SetFocus();
            exibirAlerta = true;
        }
        if ((crmForm.all.new_datado1pagamentomensal13.DataValue != null) && (crmForm.all.new_datado1pagamentomensal13.DataValue < dataAssinaturaConvertida)) {
            crmForm.all.new_datado1pagamentomensal13.DataValue = null;
            crmForm.all.new_datado1pagamentomensal13.SetFocus();
            exibirAlerta = true;
        }
        if ((crmForm.all.new_datado1pagamentomensal14.DataValue != null) && (crmForm.all.new_datado1pagamentomensal14.DataValue < dataAssinaturaConvertida)) {
            crmForm.all.new_datado1pagamentomensal14.DataValue = null;
            crmForm.all.new_datado1pagamentomensal14.SetFocus();
            exibirAlerta = true;
        }
        if ((crmForm.all.new_datado1pagamentomensal15.DataValue != null) && (crmForm.all.new_datado1pagamentomensal15.DataValue < dataAssinaturaConvertida)) {
            crmForm.all.new_datado1pagamentomensal15.DataValue = null;
            crmForm.all.new_datado1pagamentomensal15.SetFocus();
            exibirAlerta = true;
        }
        if ((crmForm.all.new_datado1pagamentomensal16.DataValue != null) && (crmForm.all.new_datado1pagamentomensal16.DataValue < dataAssinaturaConvertida)) {
            crmForm.all.new_datado1pagamentomensal16.DataValue = null;
            crmForm.all.new_datado1pagamentomensal16.SetFocus();
            exibirAlerta = true;
        }
        if ((crmForm.all.new_datado1pagamentomensal17.DataValue != null) && (crmForm.all.new_datado1pagamentomensal17.DataValue < dataAssinaturaConvertida)) {
            crmForm.all.new_datado1pagamentomensal17.DataValue = null;
            crmForm.all.new_datado1pagamentomensal17.SetFocus();
            exibirAlerta = true;
        }
        if ((crmForm.all.new_datado1pagamentomensal18.DataValue != null) && (crmForm.all.new_datado1pagamentomensal18.DataValue < dataAssinaturaConvertida)) {
            crmForm.all.new_datado1pagamentomensal18.DataValue = null;
            crmForm.all.new_datado1pagamentomensal18.SetFocus();
            exibirAlerta = true;
        }
        if ((crmForm.all.new_datado1pagamentomensal19.DataValue != null) && (crmForm.all.new_datado1pagamentomensal19.DataValue < dataAssinaturaConvertida)) {
            crmForm.all.new_datado1pagamentomensal19.DataValue = null;
            crmForm.all.new_datado1pagamentomensal19.SetFocus();
            exibirAlerta = true;
        }

        if ((crmForm.all.new_datadepagamentointermediaria.DataValue != null) && (crmForm.all.new_datadepagamentointermediaria.DataValue < dataAssinaturaConvertida)) {
            crmForm.all.new_datadepagamentointermediaria.DataValue = null;
            crmForm.all.new_datadepagamentointermediaria.SetFocus();
            exibirAlerta = true;
        }
        if ((crmForm.all.new_datadepagamentointermediria1.DataValue != null) && (crmForm.all.new_datadepagamentointermediria1.DataValue < dataAssinaturaConvertida)) {
            crmForm.all.new_datadepagamentointermediria1.DataValue = null;
            crmForm.all.new_datadepagamentointermediria1.SetFocus();
            exibirAlerta = true;
        }
        if ((crmForm.all.new_datadepagamentointermediaria2.DataValue != null) && (crmForm.all.new_datadepagamentointermediaria2.DataValue < dataAssinaturaConvertida)) {
            crmForm.all.new_datadepagamentointermediaria2.DataValue = null;
            crmForm.all.new_datadepagamentointermediaria2.SetFocus();
            exibirAlerta = true;
        }
        if ((crmForm.all.new_vencimentodescontopromocional.DataValue != null) && (crmForm.all.new_vencimentodescontopromocional.DataValue < dataAssinaturaConvertida)) {
            crmForm.all.new_vencimentodescontopromocional.DataValue = null;
            crmForm.all.new_vencimentodescontopromocional.SetFocus();
            exibirAlerta = true;
        }
        if ((crmForm.all.new_dat_primeira_parcela_devolucao.DataValue != null) && (crmForm.all.new_dat_primeira_parcela_devolucao.DataValue < dataAssinaturaConvertida)) {
            crmForm.all.new_dat_primeira_parcela_devolucao.DataValue = null;
            crmForm.all.new_dat_primeira_parcela_devolucao.SetFocus();
            exibirAlerta = true;
        }
        if ((crmForm.all.new_data_vencimento_parcela_dm.DataValue != null) && (crmForm.all.new_data_vencimento_parcela_dm.DataValue < dataAssinaturaConvertida)) {
            crmForm.all.new_data_vencimento_parcela_dm.DataValue = null;
            crmForm.all.new_data_vencimento_parcela_dm.SetFocus();
            exibirAlerta = true;
        }

        if ((crmForm.all.new_data_vencimento_parcela_ih.DataValue != null) && (crmForm.all.new_data_vencimento_parcela_ih.DataValue < dataAssinaturaConvertida)) {
            crmForm.all.new_data_vencimento_parcela_ih.DataValue = null;
            crmForm.all.new_data_vencimento_parcela_ih.SetFocus();
            exibirAlerta = true;
        }

        if ((crmForm.all.new_data_vencimento_parcela_ti.DataValue != null) && (crmForm.all.new_data_vencimento_parcela_ti.DataValue < dataAssinaturaConvertida)) {
            crmForm.all.new_data_vencimento_parcela_ti.DataValue = null;
            crmForm.all.new_data_vencimento_parcela_ti.SetFocus();
            exibirAlerta = true;
        }

        if ((crmForm.all.new_data_vencimento_parcela_dn.DataValue != null) && (crmForm.all.new_data_vencimento_parcela_dn.DataValue < dataAssinaturaConvertida)) {
            crmForm.all.new_data_vencimento_parcela_dn.DataValue = null;
            crmForm.all.new_data_vencimento_parcela_dn.SetFocus();
            exibirAlerta = true;
        }

        if ((crmForm.all.new_data_vencimento_mp1.DataValue != null) && (crmForm.all.new_data_vencimento_mp1.DataValue < dataAssinaturaConvertida)) {
            crmForm.all.new_data_vencimento_mp1.DataValue = null;
            crmForm.all.new_data_vencimento_mp1.SetFocus();
            exibirAlerta = true;
        }

        if ((crmForm.all.new_data_vencimento_mp2.DataValue != null) && (crmForm.all.new_data_vencimento_mp2.DataValue < dataAssinaturaConvertida)) {
            crmForm.all.new_data_vencimento_mp2.DataValue = null;
            crmForm.all.new_data_vencimento_mp2.SetFocus();
            exibirAlerta = true;
        }

        if ((crmForm.all.new_data_vencimento_mp3.DataValue != null) && (crmForm.all.new_data_vencimento_mp3.DataValue < dataAssinaturaConvertida)) {
            crmForm.all.new_data_vencimento_mp3.DataValue = null;
            crmForm.all.new_data_vencimento_mp3.SetFocus();
            exibirAlerta = true;
        }

        if ((crmForm.all.new_data_vencimento_mp4.DataValue != null) && (crmForm.all.new_data_vencimento_mp4.DataValue < dataAssinaturaConvertida)) {
            crmForm.all.new_data_vencimento_mp4.DataValue = null;
            crmForm.all.new_data_vencimento_mp4.SetFocus();
            exibirAlerta = true;
        }

        if ((crmForm.all.new_data_vencimento_mp5.DataValue != null) && (crmForm.all.new_data_vencimento_mp5.DataValue < dataAssinaturaConvertida)) {
            crmForm.all.new_data_vencimento_mp5.DataValue = null;
            crmForm.all.new_data_vencimento_mp5.SetFocus();
            exibirAlerta = true;
        }

        if (exibirAlerta) {
            alert('As datas de vencimento não podem ser menor que data de assinatura da Oportunidade de Renegociação');
            event.returnValue = false;
            return false;
        }
    }

    crmForm.DesabilitaCamposConsistencia = function(grupo) {
        var indicadorDesabilitar = false;
        var camposDesabilitar = "";
        var campoAplicarJuros = "";
        if (grupo == "DIVISAO_SINAL_D1") {
            camposDesabilitar = "new_desconto_sinal_1;new_acrescimo_sinal_1";
            campoAplicarJuros = "new_aplicar_juros_sinal_1";
            if (crmForm.all.new_qtdedeparcelas.DataValue != null || crmForm.all.new_valordaparcela.DataValue != null || crmForm.all.new_datadevencimento1parcela.DataValue != null) {
                if (crmForm.all.new_qtdedeparcelas.DataValue != null && crmForm.all.new_valordaparcela.DataValue != null) {
                    indicadorDesabilitar = false;
                }
            }
            else {
                indicadorDesabilitar = true;
            }
        }

        if (grupo == "DIVISAO_SINAL_D2") {
            camposDesabilitar = "new_desconto_sinal_2;new_acrescimo_sinal_2";
            campoAplicarJuros = "new_aplicar_juros_sinal_2";
            if (crmForm.all.new_qtdedeparcelassinal1.DataValue != null || crmForm.all.new_valordaparcelasinal1.DataValue != null || crmForm.all.new_datadevencimento1parcelasinal1.DataValue != null) {
                if (crmForm.all.new_qtdedeparcelassinal1.DataValue != null && crmForm.all.new_valordaparcelasinal1.DataValue != null)
                    indicadorDesabilitar = false;
            }
            else {
                indicadorDesabilitar = true;
            }
        }
        if (grupo == "DIVISAO_SINAL_D3") {
            campoAplicarJuros = "new_aplicar_juros_sinal_3";
            camposDesabilitar = "new_desconto_sinal_3;new_acrescimo_sinal_3";
            if (crmForm.all.new_qtdedeparcelassinal2.DataValue != null || crmForm.all.new_valordasparcelassinal2.DataValue != null || crmForm.all.new_datadevencimento1parcelasinal2.DataValue != null) {
                if (crmForm.all.new_qtdedeparcelassinal2.DataValue != null && crmForm.all.new_valordasparcelassinal2.DataValue != null)
                    indicadorDesabilitar = false;
            }
            else {
                indicadorDesabilitar = true;
            }
        }
        if (grupo == "DIVISAO_SINAL_D4") {
            campoAplicarJuros = "new_aplicar_juros_sinal_4";
            camposDesabilitar = "new_desconto_sinal_4;new_acrescimo_sinal_4";
            if (crmForm.all.new_qtdedeparcelassinal3.DataValue != null || crmForm.all.new_valordasparcelassinal3.DataValue != null || crmForm.all.new_datadevencimento1parcelasinal3.DataValue != null) {
                if (crmForm.all.new_qtdedeparcelassinal3.DataValue != null && crmForm.all.new_valordasparcelassinal3.DataValue != null)
                    indicadorDesabilitar = false;
            }
            else {
                indicadorDesabilitar = true;
            }
        }
        if (grupo == "DF_1") {
            campoAplicarJuros = "new_aplicar_juros_dif_1";
            camposDesabilitar = "new_desconto_dif_finan_1;new_acrescimo_dif_1";
            if (crmForm.all.new_qtd_parcela_df1.DataValue != null || crmForm.all.new_valor_parcela_df1.DataValue != null || crmForm.all.new_dat_vencimento_parceladf1.DataValue != null) {
                if (crmForm.all.new_qtd_parcela_df1.DataValue != null && crmForm.all.new_valor_parcela_df1.DataValue != null) {
                    indicadorDesabilitar = false;
                }
            }
            else {
                indicadorDesabilitar = true;
            }
        }
        if (grupo == "DF_2") {
            campoAplicarJuros = "new_aplicar_juros_dif_2";
            camposDesabilitar = "new_desconto_dif_finan_2;new_acrescimo_dif_2";
            if (crmForm.all.new_qtd_parcela_df2.DataValue != null || crmForm.all.new_valor_parcela_df2.DataValue != null || crmForm.all.new_dat_vencimento_parceladf2.DataValue != null) {
                if (crmForm.all.new_qtd_parcela_df2.DataValue != null && crmForm.all.new_valor_parcela_df2.DataValue != null) {
                    indicadorDesabilitar = false;
                }
            }
            else {
                indicadorDesabilitar = true;
            }
        }
        if (grupo == "DF_3") {
            campoAplicarJuros = "new_aplicar_juros_dif_3";
            camposDesabilitar = "new_desconto_dif_finan_3;new_acrescimo_dif_3";
            if (crmForm.all.new_qtd_parcela_df3.DataValue != null || crmForm.all.new_valor_parcela_df3.DataValue != null || crmForm.all.new_dat_vencimento_parceladf3.DataValue != null) {
                if (crmForm.all.new_qtd_parcela_df3.DataValue != null && crmForm.all.new_valor_parcela_df3.DataValue != null) {
                    indicadorDesabilitar = false;
                }
            }
            else {
                indicadorDesabilitar = true;
            }
        }
        if (grupo == "DF_4") {
            campoAplicarJuros = "new_aplicar_juros_dif_4";
            camposDesabilitar = "new_desconto_dif_finan_4;new_acrescimo_dif_4";
            if (crmForm.all.new_qtd_parcela_df4.DataValue != null || crmForm.all.new_valor_parcela_df4.DataValue != null || crmForm.all.new_dat_vencimento_parceladf4.DataValue != null) {
                if (crmForm.all.new_qtd_parcela_df4.DataValue != null && crmForm.all.new_valor_parcela_df4.DataValue != null) {
                    indicadorDesabilitar = false;
                }
            }
            else {
                indicadorDesabilitar = true;
            }
        }
        if (grupo == "DF_5") {
            campoAplicarJuros = "new_aplicar_juros_dif_5";
            camposDesabilitar = "new_desconto_dif_finan_5;new_acrescimo_dif_5";
            if (crmForm.all.new_qtd_parcela_df5.DataValue != null || crmForm.all.new_valor_parcela_df5.DataValue != null || crmForm.all.new_dat_vencimento_parceladf5.DataValue != null) {
                if (crmForm.all.new_qtd_parcela_df5.DataValue != null && crmForm.all.new_valor_parcela_df5.DataValue != null) {
                    indicadorDesabilitar = false;
                }
            }
            else {
                indicadorDesabilitar = true;
            }
        }
        if (grupo == "DF_6") {
            campoAplicarJuros = "new_aplicar_juros_dif_6";
            camposDesabilitar = "new_desconto_dif_finan_6;new_acrescimo_dif_6";
            if (crmForm.all.new_qtd_parcela_df6.DataValue != null || crmForm.all.new_valor_parcela_df6.DataValue != null || crmForm.all.new_dat_vencimento_parceladf6.DataValue != null) {
                if (crmForm.all.new_qtd_parcela_df6.DataValue != null && crmForm.all.new_valor_parcela_df6.DataValue != null) {
                    indicadorDesabilitar = false;
                }
            }
            else {
                indicadorDesabilitar = true;
            }
        }
        if (grupo == "DF_7") {
            campoAplicarJuros = "new_aplicar_juros_dif_7";
            camposDesabilitar = "new_desconto_dif_finan_7;new_acrescimo_dif_7";
            if (crmForm.all.new_qtd_parcela_df7.DataValue != null || crmForm.all.new_valor_parcela_df7.DataValue != null || crmForm.all.new_dat_vencimento_parceladf7.DataValue != null) {
                if (crmForm.all.new_qtd_parcela_df7.DataValue != null && crmForm.all.new_valor_parcela_df7.DataValue != null) {
                    indicadorDesabilitar = false;
                }
            }
            else {
                indicadorDesabilitar = true;
            }
        }
        if (grupo == "DF_8") {
            campoAplicarJuros = "new_aplicar_juros_dif_8";
            camposDesabilitar = "new_desconto_dif_finan_8;new_acrescimo_dif_8";
            if (crmForm.all.new_qtd_parcela_df8.DataValue != null || crmForm.all.new_valor_parcela_df8.DataValue != null || crmForm.all.new_dat_vencimento_parceladf8.DataValue != null) {
                if (crmForm.all.new_qtd_parcela_df8.DataValue != null && crmForm.all.new_valor_parcela_df8.DataValue != null) {
                    indicadorDesabilitar = false;
                }
            }
            else {
                indicadorDesabilitar = true;
            }
        }
        if (grupo == "MENSAL_M1") {
            campoAplicarJuros = "new_aplicar_juros_mensal_1";
            camposDesabilitar = "new_desconto_mensal_1;new_acrescimo_mensal_1";
            if (crmForm.all.new_qtdedeparcelasmensais.DataValue != null || crmForm.all.new_valordaparcelamensal.DataValue != null || crmForm.all.new_datadepagamentomensal.DataValue != null) {
                if (crmForm.all.new_qtdedeparcelasmensais.DataValue != null && crmForm.all.new_valordaparcelamensal.DataValue != null) {
                    indicadorDesabilitar = false;
                }
            }
            else {
                indicadorDesabilitar = true;
            }
        }
        if (grupo == "MENSAL_M2") {
            campoAplicarJuros = "new_aplicar_juros_mensal_2";
            camposDesabilitar = "new_desconto_mensal_2;new_acrescimo_mensal_2";
            if (crmForm.all.new_qtdedeparcelasmensais1.DataValue != null || crmForm.all.new_valordaparcelamensal1.DataValue != null || crmForm.all.new_datado1pagamentomensal1.DataValue != null) {
                if (crmForm.all.new_qtdedeparcelasmensais1.DataValue != null && crmForm.all.new_valordaparcelamensal1.DataValue != null) {
                    indicadorDesabilitar = false;
                }
            }
            else {
                indicadorDesabilitar = true;
            }
        }
        if (grupo == "MENSAL_M3") {
            campoAplicarJuros = "new_aplicar_juros_mensal_3";
            camposDesabilitar = "new_desconto_mensal_3;new_acrescimo_mensal_3";
            if (crmForm.all.new_qtdedeparcelasmensais2.DataValue != null || crmForm.all.new_valordaparcelamensal2.DataValue != null || crmForm.all.new_datado1pagamentomensal2.DataValue != null) {
                if (crmForm.all.new_qtdedeparcelasmensais2.DataValue != null && crmForm.all.new_valordaparcelamensal2.DataValue != null) {
                    indicadorDesabilitar = false;
                }
            }
            else {
                indicadorDesabilitar = true;
            }
        }

        if (grupo == "MENSAL_M4") {
            campoAplicarJuros = "new_aplicar_juros_mensal_4";
            camposDesabilitar = "new_desconto_mensal_4;new_acrescimo_mensal_4";
            if (crmForm.all.new_qtdedeparcelasmensais3.DataValue != null || crmForm.all.new_valordaparcelamensal3.DataValue != null || crmForm.all.new_datado1pagamentomensal3.DataValue != null) {
                if (crmForm.all.new_qtdedeparcelasmensais3.DataValue != null && crmForm.all.new_valordaparcelamensal3.DataValue != null) {
                    indicadorDesabilitar = false;
                }
            }
            else {
                indicadorDesabilitar = true;
            }
        }
        if (grupo == "MENSAL_M5") {
            campoAplicarJuros = "new_aplicar_juros_mensal_5";
            camposDesabilitar = "new_desconto_mensal_5;new_acrescimo_mensal_5";
            if (crmForm.all.new_qtdedeparcelasmensais4.DataValue != null || crmForm.all.new_valordaparcelamensal4.DataValue != null || crmForm.all.new_datado1pagamentomensal4.DataValue != null) {
                if (crmForm.all.new_qtdedeparcelasmensais4.DataValue != null && crmForm.all.new_valordaparcelamensal4.DataValue != null) {
                    indicadorDesabilitar = false;
                }
            }
            else {
                indicadorDesabilitar = true;
            }
        }
        if (grupo == "MENSAL_M6") {
            campoAplicarJuros = "new_aplicar_juros_mensal_6";
            camposDesabilitar = "new_desconto_mensal_6;new_acrescimo_mensal_6";
            if (crmForm.all.new_qtdedeparcelasmensais5.DataValue != null || crmForm.all.new_valordaparcelamensal5.DataValue != null || crmForm.all.new_datado1pagamentomensal5.DataValue != null) {
                if (crmForm.all.new_qtdedeparcelasmensais5.DataValue != null && crmForm.all.new_valordaparcelamensal5.DataValue != null) {
                    indicadorDesabilitar = false;
                }
            }
            else {
                indicadorDesabilitar = true;
            }
        }

        if (grupo == "MENSAL_M7") {
            campoAplicarJuros = "new_aplicar_juros_mensal_7";
            camposDesabilitar = "new_desconto_mensal_7;new_acrescimo_mensal_7";
            if (crmForm.all.new_qtdedeparcelasmensais6.DataValue != null || crmForm.all.new_valordaparcelamensal6.DataValue != null || crmForm.all.new_datado1pagamentomensal6.DataValue != null) {
                if (crmForm.all.new_qtdedeparcelasmensais6.DataValue != null && crmForm.all.new_valordaparcelamensal6.DataValue != null) {
                    indicadorDesabilitar = false;
                }
            }
            else {
                indicadorDesabilitar = true;
            }
        }
        if (grupo == "MENSAL_M8") {
            campoAplicarJuros = "new_aplicar_juros_mensal_8";
            camposDesabilitar = "new_desconto_mensal_8;new_acrescimo_mensal_8";
            if (crmForm.all.new_qtdedeparcelasmensais7.DataValue != null || crmForm.all.new_valordaparcelamensal7.DataValue != null || crmForm.all.new_datado1pagamentomensal7.DataValue != null) {
                if (crmForm.all.new_qtdedeparcelasmensais7.DataValue != null && crmForm.all.new_valordaparcelamensal7.DataValue != null) {
                    indicadorDesabilitar = false;
                }
            }
            else {
                indicadorDesabilitar = true;
            }
        }

        if (grupo == "MENSAL_M9") {
            campoAplicarJuros = "new_aplicar_juros_mensal_9";
            camposDesabilitar = "new_desconto_mensal_9;new_acrescimo_mensal_9";
            if (crmForm.all.new_qtdedeparcelasmensais8.DataValue != null || crmForm.all.new_valordaparcelamensal8.DataValue != null || crmForm.all.new_datado1pagamentomensal8.DataValue != null) {
                if (crmForm.all.new_qtdedeparcelasmensais8.DataValue != null && crmForm.all.new_valordaparcelamensal8.DataValue != null) {
                    indicadorDesabilitar = false;
                }
            }
            else {
                indicadorDesabilitar = true;
            }
        }

        if (grupo == "MENSAL_M10") {
            campoAplicarJuros = "new_aplicar_juros_mensal_10";
            camposDesabilitar = "new_desconto_mensal_10;new_acrescimo_mensal_10";
            if (crmForm.all.new_qtdedeparcelasmensais9.DataValue != null || crmForm.all.new_valordaparcelamensal9.DataValue != null || crmForm.all.new_datado1pagamentomensal9.DataValue != null) {
                if (crmForm.all.new_qtdedeparcelasmensais9.DataValue != null && crmForm.all.new_valordaparcelamensal9.DataValue != null) {
                    indicadorDesabilitar = false;
                }
            }
            else {
                indicadorDesabilitar = true;
            }
        }

        if (grupo == "MENSAL_M11") {
            campoAplicarJuros = "new_aplicar_juros_mensal_11";
            camposDesabilitar = "new_desconto_mensal_11;new_acrescimo_mensal_11";
            if (crmForm.all.new_qtdedeparcelasmensais10.DataValue != null || crmForm.all.new_valordaparcelamensal10.DataValue != null || crmForm.all.new_datado1pagamentomensal10.DataValue != null) {
                if (crmForm.all.new_qtdedeparcelasmensais10.DataValue != null && crmForm.all.new_valordaparcelamensal10.DataValue != null) {
                    indicadorDesabilitar = false;
                }
            }
            else {
                indicadorDesabilitar = true;
            }
        }

        if (grupo == "MENSAL_M12") {
            campoAplicarJuros = "new_aplicar_juros_mensal_12";
            camposDesabilitar = "new_desconto_mensal_12;new_acrescimo_mensal_12";
            if (crmForm.all.new_qtdedeparcelasmensais11.DataValue != null || crmForm.all.new_valordaparcelamensal11.DataValue != null || crmForm.all.new_datado1pagamentomensal11.DataValue != null) {
                if (crmForm.all.new_qtdedeparcelasmensais11.DataValue != null && crmForm.all.new_valordaparcelamensal11.DataValue != null) {
                    indicadorDesabilitar = false;
                }
            }
            else {
                indicadorDesabilitar = true;
            }
        }

        if (grupo == "MENSAL_M13") {
            campoAplicarJuros = "new_aplicar_juros_mensal_13";
            camposDesabilitar = "new_desconto_mensal_13;new_acrescimo_mensal_13";
            if (crmForm.all.new_qtdedeparcelasmensais12.DataValue != null || crmForm.all.new_valordaparcelamensal12.DataValue != null || crmForm.all.new_datado1pagamentomensal12.DataValue != null) {
                if (crmForm.all.new_qtdedeparcelasmensais12.DataValue != null && crmForm.all.new_valordaparcelamensal12.DataValue != null) {
                    indicadorDesabilitar = false;
                }
            }
            else {
                indicadorDesabilitar = true;
            }
        }

        if (grupo == "MENSAL_M14") {
            campoAplicarJuros = "new_aplicar_juros_mensal_14";
            camposDesabilitar = "new_desconto_mensal_14;new_acrescimo_mensal_14";
            if (crmForm.all.new_qtdedeparcelasmensais13.DataValue != null || crmForm.all.new_valordaparcelamensal13.DataValue != null || crmForm.all.new_datado1pagamentomensal13.DataValue != null) {
                if (crmForm.all.new_qtdedeparcelasmensais13.DataValue != null && crmForm.all.new_valordaparcelamensal13.DataValue != null) {
                    indicadorDesabilitar = false;
                }
            }
            else {
                indicadorDesabilitar = true;
            }
        }

        if (grupo == "MENSAL_M15") {
            campoAplicarJuros = "new_aplicar_juros_mensal_15";
            camposDesabilitar = "new_desconto_mensal_15;new_acrescimo_mensal_15";
            if (crmForm.all.new_qtdedeparcelasmensais14.DataValue != null || crmForm.all.new_valordaparcelamensal14.DataValue != null || crmForm.all.new_datado1pagamentomensal14.DataValue != null) {
                if (crmForm.all.new_qtdedeparcelasmensais14.DataValue != null && crmForm.all.new_valordaparcelamensal14.DataValue != null) {
                    indicadorDesabilitar = false;
                }
            }
            else {
                indicadorDesabilitar = true;
            }
        }

        if (grupo == "MENSAL_M16") {
            campoAplicarJuros = "new_aplicar_juros_mensal_16";
            camposDesabilitar = "new_desconto_mensal_16;new_acrescimo_mensal_16";
            if (crmForm.all.new_qtdedeparcelasmensais15.DataValue != null || crmForm.all.new_valordaparcelamensal15.DataValue != null || crmForm.all.new_datado1pagamentomensal15.DataValue != null) {
                if (crmForm.all.new_qtdedeparcelasmensais15.DataValue != null && crmForm.all.new_valordaparcelamensal15.DataValue != null) {
                    indicadorDesabilitar = false;
                }
            }
            else {
                indicadorDesabilitar = true;
            }
        }

        if (grupo == "MENSAL_M17") {
            campoAplicarJuros = "new_aplicar_juros_mensal_17";
            camposDesabilitar = "new_desconto_mensal_17;new_acrescimo_mensal_17";
            if (crmForm.all.new_qtdedeparcelasmensais16.DataValue != null || crmForm.all.new_valordaparcelamensal16.DataValue != null || crmForm.all.new_datado1pagamentomensal16.DataValue != null) {

                if (crmForm.all.new_qtdedeparcelasmensais16.DataValue != null && crmForm.all.new_valordaparcelamensal16.DataValue != null) {
                    indicadorDesabilitar = false;
                }
            }
            else {
                indicadorDesabilitar = true;
            }
        }

        if (grupo == "MENSAL_M18") {
            campoAplicarJuros = "new_aplicar_juros_mensal_18";
            camposDesabilitar = "new_desconto_mensal_18;new_acrescimo_mensal_18";
            if (crmForm.all.new_qtdedeparcelasmensais17.DataValue != null || crmForm.all.new_valordaparcelamensal17.DataValue != null || crmForm.all.new_datado1pagamentomensal17.DataValue != null) {
                if (crmForm.all.new_qtdedeparcelasmensais17.DataValue != null && crmForm.all.new_valordaparcelamensal17.DataValue != null) {
                    indicadorDesabilitar = false;
                }
            }
            else {
                indicadorDesabilitar = true;
            }
        }

        if (grupo == "MENSAL_M19") {
            campoAplicarJuros = "new_aplicar_juros_mensal_19";
            camposDesabilitar = "new_desconto_mensal_19;new_acrescimo_mensal_19";
            if (crmForm.all.new_qtdedeparcelasmensais18.DataValue != null || crmForm.all.new_valordaparcelamensal18.DataValue != null || crmForm.all.new_datado1pagamentomensal18.DataValue != null) {
                if (crmForm.all.new_qtdedeparcelasmensais18.DataValue != null && crmForm.all.new_valordaparcelamensal18.DataValue != null) {
                    indicadorDesabilitar = false;
                }
            }
            else {
                indicadorDesabilitar = true;
            }
        }

        if (grupo == "MENSAL_M20") {
            campoAplicarJuros = "new_aplicar_juros_mensal_20";
            camposDesabilitar = "new_desconto_mensal_20;new_acrescimo_mensal_20";
            if (crmForm.all.new_qtdedeparcelasmensais19.DataValue != null || crmForm.all.new_valordaparcelamensal19.DataValue != null || crmForm.all.new_datado1pagamentomensal19.DataValue != null) {
                if (crmForm.all.new_qtdedeparcelasmensais19.DataValue != null && crmForm.all.new_valordaparcelamensal19.DataValue != null) {
                    indicadorDesabilitar = false;
                }
            }
            else {
                indicadorDesabilitar = true;
            }
        }
        if (grupo == "DIVISAO_INTERMEDIARIA_I1") {
            campoAplicarJuros = "new_aplicar_juros_intermediaria_1";
            camposDesabilitar = "new_desconto_intermediaria_1;new_acrescimo_intermediaria_1";
            if (crmForm.all.new_qtdeintermediarias.DataValue != null || crmForm.all.new_valosdaparcelaintermediaria.DataValue != null || crmForm.all.new_datadepagamentointermediaria.DataValue != null) {
                if (crmForm.all.new_qtdeintermediarias.DataValue != null && crmForm.all.new_valosdaparcelaintermediaria.DataValue != null) {
                    indicadorDesabilitar = false;
                }
            }
            else {
                indicadorDesabilitar = true;
            }
        }
        if (grupo == "DIVISAO_INTERMEDIARIA_I2") {
            campoAplicarJuros = "new_aplicar_juros_intermediaria_2";
            camposDesabilitar = "new_desconto_intermediaria_2;new_acrescimo_intermediaria_2";
            if (crmForm.all.new_qtdedeparcelasintermediarias1.DataValue != null || crmForm.all.new_valordaparcelaintermediaria1.DataValue != null || crmForm.all.new_datadepagamentointermediria1.DataValue != null) {
                if (crmForm.all.new_qtdedeparcelasintermediarias1.DataValue != null && crmForm.all.new_valordaparcelaintermediaria1.DataValue != null) {
                    indicadorDesabilitar = false;
                }
            }
            else {
                indicadorDesabilitar = true;
            }
        }
        if (grupo == "DIVISAO_INTERMEDIARIA_I3") {
            campoAplicarJuros = "new_aplicar_juros_intermediaria_3";
            camposDesabilitar = "new_desconto_intermediaria_3;new_acrescimo_intermediaria_3";
            if (crmForm.all.new_qtdedeparcelasintermediarias2.DataValue != null || crmForm.all.new_valordaparcelaintermediaria2.DataValue != null || crmForm.all.new_datadepagamentointermediaria2.DataValue != null) {
                if (crmForm.all.new_qtdedeparcelasintermediarias2.DataValue != null && crmForm.all.new_valordaparcelaintermediaria2.DataValue != null) {
                    indicadorDesabilitar = false;
                }
            }
            else {
                indicadorDesabilitar = true;
            }
        }
        if (grupo == "FINANCIAMENTO") {
            campoAplicarJuros = "new_aplicar_juros_financiamento";
            camposDesabilitar = "new_desconto_financiamento;new_acrescimo_financiamento";
            if (crmForm.all.new_financiamento.DataValue != null) {
                indicadorDesabilitar = false;
            }
            else {
                indicadorDesabilitar = true;
            }
        }

        if (grupo == "FGTS") {
            campoAplicarJuros = "new_aplicar_juros_fgts";
            camposDesabilitar = "new_desconto_fgts;new_acrescimo_fgts";
            if (crmForm.all.new_valordofgts.DataValue != null && crmForm.all.new_datavencimentoparcela_fgts != null) {
                indicadorDesabilitar = false;
            }
            else {
                indicadorDesabilitar = true;
            }
        }

        if (grupo == "DESCONTO_PROMO") {
            campoAplicarJuros = "new_aplicar_juros_desc_promocional";
            camposDesabilitar = "new_desconto_desc_promocional;new_acrescimo_desc_promo";
            if (crmForm.all.new_valordescontopromocional.DataValue) {
                indicadorDesabilitar = false;
            }
            else {
                indicadorDesabilitar = true;
            }
        }

        if (grupo == "DEVOLUCAO") {
            campoAplicarJuros = "new_aplicar_juros_dev_valores";
            camposDesabilitar = "new_desconto_dev_valores;new_acrescimo_dev_valores";
            if (crmForm.all.new_qtd_parcelas_devolucao.DataValue != null || crmForm.all.new_valor_parcela_devolucao.DataValue != null) {
                indicadorDesabilitar = false;
            }
            else {
                indicadorDesabilitar = true;
            }
        }

        if (grupo == "PC") {
            campoAplicarJuros = "new_aplicar_juros_pc";
            camposDesabilitar = "new_desconto_pc;new_acrescimo_pc";
            if (crmForm.all.new_qtd_parcelas_pc.DataValue != null || crmForm.all.new_valor_parcela_pc.DataValue != null || crmForm.all.new_data_vencimento_pc.DataValue != null) {
                indicadorDesabilitar = false;
            }
            else {
                indicadorDesabilitar = true;
            }
        }

        if (grupo == "VM") {
            campoAplicarJuros = "new_aplicar_juros_vm";
            camposDesabilitar = "new_desconto_vm;new_acrescimo_vm";
            if (crmForm.all.new_qtd_parcelas_vm.DataValue != null || crmForm.all.new_valor_parcela_vm.DataValue != null || crmForm.all.new_data_vencimento_vm.DataValue != null) {
                indicadorDesabilitar = false;
            }
            else {
                indicadorDesabilitar = true;
            }
        }

        if (grupo == "MP1") {
            campoAplicarJuros = "new_aplicar_juros_mp1";
            camposDesabilitar = "new_desconto_mp1;new_acrescimo_mp1";
            if (crmForm.all.new_qtd_parcelas_mp1.DataValue != null || crmForm.all.new_valor_parcelas_mp1.DataValue != null || crmForm.all.new_data_vencimento_mp1.DataValue != null) {
                indicadorDesabilitar = false;
            }
            else {
                indicadorDesabilitar = true;
            }
        }

        if (grupo == "MP2") {
            campoAplicarJuros = "new_aplicar_juros_mp2";
            camposDesabilitar = "new_desconto_mp2;new_acrescimo_mp2";
            if (crmForm.all.new_qtd_parcelas_mp2.DataValue != null || crmForm.all.new_valor_parcelas_mp2.DataValue != null || crmForm.all.new_data_vencimento_mp2.DataValue != null) {
                indicadorDesabilitar = false;
            }
            else {
                indicadorDesabilitar = true;
            }
        }

        if (grupo == "MP3") {
            campoAplicarJuros = "new_aplicar_juros_mp3";
            camposDesabilitar = "new_desconto_mp3;new_acrescimo_mp3";
            if (crmForm.all.new_qtd_parcelas_mp3.DataValue != null || crmForm.all.new_valor_parcelas_mp3.DataValue != null || crmForm.all.new_data_vencimento_mp3.DataValue != null) {
                indicadorDesabilitar = false;
            }
            else {
                indicadorDesabilitar = true;
            }
        }

        if (grupo == "MP4") {
            campoAplicarJuros = "new_aplicar_juros_mp4";
            camposDesabilitar = "new_desconto_mp4;new_acrescimo_mp4";
            if (crmForm.all.new_qtd_parcelas_mp4.DataValue != null || crmForm.all.new_valor_parcelas_mp4.DataValue != null || crmForm.all.new_data_vencimento_mp4.DataValue != null) {
                indicadorDesabilitar = false;
            }
            else {
                indicadorDesabilitar = true;
            }
        }

        if (grupo == "MP5") {
            campoAplicarJuros = "new_aplicar_juros_mp5";
            camposDesabilitar = "new_desconto_mp5;new_acrescimo_mp5";
            if (crmForm.all.new_qtd_parcelas_mp5.DataValue != null || crmForm.all.new_valor_parcelas_mp5.DataValue != null || crmForm.all.new_data_vencimento_mp5.DataValue != null) {
                indicadorDesabilitar = false;
            }
            else {
                indicadorDesabilitar = true;
            }
        }

        if (indicadorDesabilitar) {
            if (camposDesabilitar.length > 0) {
                crmForm.LimpaCampos(camposDesabilitar);
            }
        }

        if (camposDesabilitar.length > 0) {
            crmForm.DesabilitarCampos(camposDesabilitar, indicadorDesabilitar);
        }

        if (campoAplicarJuros.length > 0) {
            crmForm.ConfigurarAplicarJuros(campoAplicarJuros, indicadorDesabilitar);
        }
    }

    crmForm.ConfigurarCamposConsistencia = function() {
        crmForm.DesabilitaCamposConsistencia("DF_1");
        crmForm.DesabilitaCamposConsistencia("DF_2");
        crmForm.DesabilitaCamposConsistencia("DF_3");
        crmForm.DesabilitaCamposConsistencia("DF_4");
        crmForm.DesabilitaCamposConsistencia("DF_5");
        crmForm.DesabilitaCamposConsistencia("DF_6");
        crmForm.DesabilitaCamposConsistencia("DF_7");
        crmForm.DesabilitaCamposConsistencia("DF_8");
        crmForm.DesabilitaCamposConsistencia("DIVISAO_SINAL_D1");
        crmForm.DesabilitaCamposConsistencia("DIVISAO_SINAL_D2");
        crmForm.DesabilitaCamposConsistencia("DIVISAO_SINAL_D3");
        crmForm.DesabilitaCamposConsistencia("DIVISAO_SINAL_D4");
        crmForm.DesabilitaCamposConsistencia("MENSAL_M1");
        crmForm.DesabilitaCamposConsistencia("MENSAL_M2");
        crmForm.DesabilitaCamposConsistencia("MENSAL_M3");
        crmForm.DesabilitaCamposConsistencia("MENSAL_M4");
        crmForm.DesabilitaCamposConsistencia("MENSAL_M5");
        crmForm.DesabilitaCamposConsistencia("MENSAL_M6");
        crmForm.DesabilitaCamposConsistencia("MENSAL_M7");
        crmForm.DesabilitaCamposConsistencia("MENSAL_M8");
        crmForm.DesabilitaCamposConsistencia("MENSAL_M9");
        crmForm.DesabilitaCamposConsistencia("MENSAL_M10");
        crmForm.DesabilitaCamposConsistencia("MENSAL_M11");
        crmForm.DesabilitaCamposConsistencia("MENSAL_M12");
        crmForm.DesabilitaCamposConsistencia("MENSAL_M13");
        crmForm.DesabilitaCamposConsistencia("MENSAL_M14");
        crmForm.DesabilitaCamposConsistencia("MENSAL_M15");
        crmForm.DesabilitaCamposConsistencia("MENSAL_M16");
        crmForm.DesabilitaCamposConsistencia("MENSAL_M17");
        crmForm.DesabilitaCamposConsistencia("MENSAL_M18");
        crmForm.DesabilitaCamposConsistencia("MENSAL_M19");
        crmForm.DesabilitaCamposConsistencia("MENSAL_M20");
        crmForm.DesabilitaCamposConsistencia("DIVISAO_INTERMEDIARIA_I1");
        crmForm.DesabilitaCamposConsistencia("DIVISAO_INTERMEDIARIA_I2");
        crmForm.DesabilitaCamposConsistencia("DIVISAO_INTERMEDIARIA_I3");
        crmForm.DesabilitaCamposConsistencia("FINANCIAMENTO");
        crmForm.DesabilitaCamposConsistencia("FGTS");
        crmForm.DesabilitaCamposConsistencia("DEVOLUCAO");
        crmForm.DesabilitaCamposConsistencia("DESCONTO_PROMO");
        crmForm.DesabilitaCamposConsistencia("PC");
        crmForm.DesabilitaCamposConsistencia("VM");
        crmForm.DesabilitaCamposConsistencia("MP1");
        crmForm.DesabilitaCamposConsistencia("MP2");
        crmForm.DesabilitaCamposConsistencia("MP3");
        crmForm.DesabilitaCamposConsistencia("MP4");
        crmForm.DesabilitaCamposConsistencia("MP5");
    }

    crmForm.ConfigurarAplicarJuros = function(nomeAtributo, desabilitar) {
        var atributo = eval("crmForm.all." + nomeAtributo);
        if (crmForm.all.new_isencao_total.DataValue == TipoCampoBit.SIM) {
            desabilitar = true;
            var tipoCampoBit = TipoCampoBit.NAO;
            atributo.DataValue = tipoCampoBit;
        }

        atributo.Disabled = desabilitar;
    }

    loadScript();

    function Load() {
        //// Procurando recuperar a instância do formulário da oportunidade de renegociação.
        try {
            janelaRenegociacao = window.opener.parent.crmForm;
            var tipoContrato = janelaRenegociacao.all.new_tipo_contrato.DataValue;

            dataAssinatura = janelaRenegociacao.all.new_data_assinatura_cliente.DataValue;
        } catch (e) {
            crmForm.ObtemDataAssinaturaWebService();
        }

        crmForm.all.new_qtde_parcelas_dm.ForceSubmit = true;
        crmForm.all.new_valor_total_desconto.ForceSubmit = true;
        /// Esta variável é utilizada nos eventos OnChange, Onsave e também no arquivo ISV.config.
        grupoRenegociacao = null;
        tipoRenegociacao = null;

        Informacoes = new InformacoesProposta();
        ObterInformacaoTipo();

        saldoParcelaMP = 0;

        var renegociacaoIN = Informacoes.ValidarRenegociacaoIN();
        if (tipoContrato == TipoDeContrato.SERVICO || renegociacaoIN) {
            OcultaParcelas();
        }
        if (tipoContrato == TipoDeContrato.KITACABAMENTO) {
            OcultaParcelasKit();
        }
        if (renegociacaoIN) {
            OcultaParcelasAdicional();
            OcutarParcelasMensais(false);
        }
        else {
            OcutarParcelasMensais(true);
        }
        if (tipoContrato != TipoDeContrato.KITACABAMENTO) {
            OcultaCampos();
        }

        if (crmForm.FormType == TypeCreate) {
            if (null != janelaRenegociacao) {
                if (GrupoNaoPermitCriarProposta(janelaRenegociacao.all.new_grupo_renegociacao_id.DataValue)) {
                    alert("Para o Grupo de Renegociação " + janelaRenegociacao.all.new_grupo_renegociacao_id.DataValue[0].name + " não é permitida a criação de propostas.");
                    window.close();
                } else {
                    proposta = crmForm.ObterPropostaPorOportunidade(crmForm.all.new_oportunidadeid.DataValue[0].id);

                    if (null != proposta) {
                        alert("Não é permitido a criação de mais que uma proposta para uma mesma oportunidade.");
                        window.close();
                    } else {

                        PreencheCamposDeAcordoComOportunidade();
                        crmForm.ConfigurarVisaoFormulario(true);
                        crmForm.all.new_divisaosinal.FireOnChange();
                    }
                }
            } else
                crmForm.DesabilitarFormulario(true);
        } else if (crmForm.FormType == TypeUpdate) {
            crmForm.ConfigurarParaUpdate();
        }
        if (crmForm.FormType == TypeUpdate || crmForm.FormType == TypeCreate) {
            crmForm.ObterValorVenda(janelaRenegociacao.ObjectId);
        }
        if ((crmForm.all.new_valor_parcelas_fp.DataValue == null || crmForm.all.new_valor_parcelas_fp.DataValue == 0) && saldoParcelaMP == 0) {
            OcultaParcelasMP();
        }


        AddGroup('SINAL_A_VISTA', crmForm.all.new_valorsinalavista);
        AddGroup('SINAL_A_VISTA', crmForm.all.new_datadopagamento);

        AddGroup('DIVISAO_SINAL_D1', crmForm.all.new_qtdedeparcelas);
        AddGroup('DIVISAO_SINAL_D1', crmForm.all.new_valordaparcela);
        AddGroup('DIVISAO_SINAL_D1', crmForm.all.new_datadevencimento1parcela);
        AddGroup('DIVISAO_SINAL_D1', crmForm.all.new_periodicidadesinal);

        AddGroup('DIVISAO_SINAL_D2', crmForm.all.new_qtdedeparcelassinal1);
        AddGroup('DIVISAO_SINAL_D2', crmForm.all.new_valordaparcelasinal1);
        AddGroup('DIVISAO_SINAL_D2', crmForm.all.new_datadevencimento1parcelasinal1);
        AddGroup('DIVISAO_SINAL_D2', crmForm.all.new_periodicidadesinal1);

        AddGroup('DIVISAO_SINAL_D3', crmForm.all.new_qtdedeparcelassinal2);
        AddGroup('DIVISAO_SINAL_D3', crmForm.all.new_valordasparcelassinal2);
        AddGroup('DIVISAO_SINAL_D3', crmForm.all.new_datadevencimento1parcelasinal2);
        AddGroup('DIVISAO_SINAL_D3', crmForm.all.new_periodicidadesinal2);

        AddGroup('DIVISAO_SINAL_D4', crmForm.all.new_qtdedeparcelassinal3);
        AddGroup('DIVISAO_SINAL_D4', crmForm.all.new_valordasparcelassinal3);
        AddGroup('DIVISAO_SINAL_D4', crmForm.all.new_datadevencimento1parcelasinal3);
        AddGroup('DIVISAO_SINAL_D4', crmForm.all.new_periodicidadesinal3);

        AddGroup('MENSAL_M1', crmForm.all.new_qtdedeparcelasmensais);
        AddGroup('MENSAL_M1', crmForm.all.new_valordaparcelamensal);
        AddGroup('MENSAL_M1', new_datadepagamentomensal);

        AddGroup('MENSAL_M2', crmForm.all.new_qtdedeparcelasmensais1);
        AddGroup('MENSAL_M2', crmForm.all.new_valordaparcelamensal1);
        AddGroup('MENSAL_M2', crmForm.all.new_datado1pagamentomensal1);

        AddGroup('MENSAL_M3', crmForm.all.new_qtdedeparcelasmensais2);
        AddGroup('MENSAL_M3', crmForm.all.new_valordaparcelamensal2);
        AddGroup('MENSAL_M3', crmForm.all.new_datado1pagamentomensal2);

        AddGroup('MENSAL_M4', crmForm.all.new_qtdedeparcelasmensais3);
        AddGroup('MENSAL_M4', crmForm.all.new_valordaparcelamensal3);
        AddGroup('MENSAL_M4', crmForm.all.new_datado1pagamentomensal3);

        AddGroup('MENSAL_M5', crmForm.all.new_qtdedeparcelasmensais4);
        AddGroup('MENSAL_M5', crmForm.all.new_valordaparcelamensal4);
        AddGroup('MENSAL_M5', crmForm.all.new_datado1pagamentomensal4);

        AddGroup('MENSAL_M6', crmForm.all.new_qtdedeparcelasmensais5);
        AddGroup('MENSAL_M6', crmForm.all.new_valordaparcelamensal5);
        AddGroup('MENSAL_M6', crmForm.all.new_datado1pagamentomensal5);

        AddGroup('MENSAL_M7', crmForm.all.new_qtdedeparcelasmensais6);
        AddGroup('MENSAL_M7', crmForm.all.new_valordaparcelamensal6);
        AddGroup('MENSAL_M7', crmForm.all.new_datado1pagamentomensal6);

        AddGroup('MENSAL_M8', crmForm.all.new_qtdedeparcelasmensais7);
        AddGroup('MENSAL_M8', crmForm.all.new_valordaparcelamensal7);
        AddGroup('MENSAL_M8', crmForm.all.new_datado1pagamentomensal7);

        AddGroup('MENSAL_M9', crmForm.all.new_qtdedeparcelasmensais8);
        AddGroup('MENSAL_M9', crmForm.all.new_valordaparcelamensal8);
        AddGroup('MENSAL_M9', crmForm.all.new_datado1pagamentomensal8);

        AddGroup('MENSAL_M10', crmForm.all.new_qtdedeparcelasmensais9);
        AddGroup('MENSAL_M10', crmForm.all.new_valordaparcelamensal9);
        AddGroup('MENSAL_M10', crmForm.all.new_datado1pagamentomensal9);

        AddGroup('MENSAL_M11', crmForm.all.new_qtdedeparcelasmensais10);
        AddGroup('MENSAL_M11', crmForm.all.new_valordaparcelamensal10);
        AddGroup('MENSAL_M11', crmForm.all.new_datado1pagamentomensal10);

        AddGroup('MENSAL_M12', crmForm.all.new_qtdedeparcelasmensais11);
        AddGroup('MENSAL_M12', crmForm.all.new_valordaparcelamensal11);
        AddGroup('MENSAL_M12', crmForm.all.new_datado1pagamentomensal11);

        AddGroup('MENSAL_M13', crmForm.all.new_qtdedeparcelasmensais12);
        AddGroup('MENSAL_M13', crmForm.all.new_valordaparcelamensal12);
        AddGroup('MENSAL_M13', crmForm.all.new_datado1pagamentomensal12);

        AddGroup('MENSAL_M14', crmForm.all.new_qtdedeparcelasmensais13);
        AddGroup('MENSAL_M14', crmForm.all.new_valordaparcelamensal13);
        AddGroup('MENSAL_M14', crmForm.all.new_datado1pagamentomensal13);

        AddGroup('MENSAL_M15', crmForm.all.new_qtdedeparcelasmensais14);
        AddGroup('MENSAL_M15', crmForm.all.new_valordaparcelamensal14);
        AddGroup('MENSAL_M15', crmForm.all.new_datado1pagamentomensal14);

        AddGroup('MENSAL_M16', crmForm.all.new_qtdedeparcelasmensais15);
        AddGroup('MENSAL_M16', crmForm.all.new_valordaparcelamensal15);
        AddGroup('MENSAL_M16', crmForm.all.new_datado1pagamentomensal15);

        AddGroup('MENSAL_M17', crmForm.all.new_qtdedeparcelasmensais16);
        AddGroup('MENSAL_M17', crmForm.all.new_valordaparcelamensal16);
        AddGroup('MENSAL_M17', crmForm.all.new_datado1pagamentomensal16);

        AddGroup('MENSAL_M18', crmForm.all.new_qtdedeparcelasmensais17);
        AddGroup('MENSAL_M18', crmForm.all.new_valordaparcelamensal17);
        AddGroup('MENSAL_M18', crmForm.all.new_datado1pagamentomensal17);

        AddGroup('MENSAL_M19', crmForm.all.new_qtdedeparcelasmensais18);
        AddGroup('MENSAL_M19', crmForm.all.new_valordaparcelamensal18);
        AddGroup('MENSAL_M19', crmForm.all.new_datado1pagamentomensal18);

        AddGroup('MENSAL_M20', crmForm.all.new_qtdedeparcelasmensais19);
        AddGroup('MENSAL_M20', crmForm.all.new_valordaparcelamensal19);
        AddGroup('MENSAL_M20', crmForm.all.new_datado1pagamentomensal19);

        AddGroup('DIVISAO_INTERMEDIARIA_I1', crmForm.all.new_qtdeintermediarias);
        AddGroup('DIVISAO_INTERMEDIARIA_I1', crmForm.all.new_valosdaparcelaintermediaria);
        AddGroup('DIVISAO_INTERMEDIARIA_I1', crmForm.all.new_datadepagamentointermediaria);
        AddGroup('DIVISAO_INTERMEDIARIA_I1', crmForm.all.new_periododepagamentointermediaria);

        AddGroup('DIVISAO_INTERMEDIARIA_I2', crmForm.all.new_qtdedeparcelasintermediarias1);
        AddGroup('DIVISAO_INTERMEDIARIA_I2', crmForm.all.new_valordaparcelaintermediaria1);
        AddGroup('DIVISAO_INTERMEDIARIA_I2', crmForm.all.new_datadepagamentointermediria1);
        AddGroup('DIVISAO_INTERMEDIARIA_I2', crmForm.all.new_periododepagamentointermediaria1);

        AddGroup('DIVISAO_INTERMEDIARIA_I3', crmForm.all.new_qtdedeparcelasintermediarias2);
        AddGroup('DIVISAO_INTERMEDIARIA_I3', crmForm.all.new_valordaparcelaintermediaria2);
        AddGroup('DIVISAO_INTERMEDIARIA_I3', crmForm.all.new_datadepagamentointermediaria2);
        AddGroup('DIVISAO_INTERMEDIARIA_I3', crmForm.all.new_periododepagamentointermediaria2);

        AddGroup('FINANCIAMENTO', crmForm.all.new_financiamento);
        AddGroup('FINANCIAMENTO', crmForm.all.new_datavencimentoparcela_financiamento);
        if (tipoContrato == TipoDeContrato.KITACABAMENTO) {
            AddGroup('FINANCIAMENTO', crmForm.all.new_tipo_parcela_financiamento);
        }


        AddGroup('FGTS', crmForm.all.new_valordofgts);
        AddGroup('FGTS', crmForm.all.new_datavencimentoparcela_fgts);
        if (tipoContrato == TipoDeContrato.KITACABAMENTO) {
            AddGroup('FGTS', crmForm.all.new_tipo_parcela_fgts);
        }


        AddGroup('RECURSO_PROPRIO', crmForm.all.new_recurso_proprio);
        AddGroup('RECURSO_PROPRIO', crmForm.all.new_datavencimentoparcela_recursosproprios);

        AddGroup('RECURSO_PROPRIO_CLIENTE', crmForm.all.new_recurso_proprio_cliente);
        AddGroup('RECURSO_PROPRIO_CLIENTE', crmForm.all.new_data_recurso_proprio_cliente);


        AddGroup('DF_1', crmForm.all.new_qtd_parcela_df1);
        AddGroup('DF_1', crmForm.all.new_valor_parcela_df1);
        AddGroup('DF_1', new_dat_vencimento_parceladf1);

        AddGroup('DF_2', crmForm.all.new_qtd_parcela_df2);
        AddGroup('DF_2', crmForm.all.new_valor_parcela_df2);
        AddGroup('DF_2', crmForm.all.new_dat_vencimento_parceladf2);

        AddGroup('DF_3', crmForm.all.new_qtd_parcela_df3);
        AddGroup('DF_3', crmForm.all.new_valor_parcela_df3);
        AddGroup('DF_3', crmForm.all.new_dat_vencimento_parceladf3);

        AddGroup('DF_4', crmForm.all.new_qtd_parcela_df4);
        AddGroup('DF_4', crmForm.all.new_valor_parcela_df4);
        AddGroup('DF_4', crmForm.all.new_dat_vencimento_parceladf4);

        AddGroup('DF_5', crmForm.all.new_qtd_parcela_df5);
        AddGroup('DF_5', crmForm.all.new_valor_parcela_df5);
        AddGroup('DF_5', crmForm.all.new_dat_vencimento_parceladf5);

        AddGroup('DF_6', crmForm.all.new_qtd_parcela_df6);
        AddGroup('DF_6', crmForm.all.new_valor_parcela_df6);
        AddGroup('DF_6', crmForm.all.new_dat_vencimento_parceladf6);

        AddGroup('DF_7', crmForm.all.new_qtd_parcela_df7);
        AddGroup('DF_7', crmForm.all.new_valor_parcela_df7);
        AddGroup('DF_7', crmForm.all.new_dat_vencimento_parceladf7);

        AddGroup('DF_8', crmForm.all.new_qtd_parcela_df8);
        AddGroup('DF_8', crmForm.all.new_valor_parcela_df8);
        AddGroup('DF_8', crmForm.all.new_dat_vencimento_parceladf8);
        AddGroup('TAC', crmForm.all.new_valor_parcela_tac);
        AddGroup('TAC', crmForm.all.new_data_vencimento_parcela_tac);
        AddGroup('TAC', crmForm.all.new_qtde_parcelas_tac);

        AddGroup('DM', crmForm.all.new_valor_parcela_dm);
        AddGroup('DM', crmForm.all.new_data_vencimento_parcela_dm);

        AddGroup('PC', crmForm.all.new_qtd_parcelas_pc);
        AddGroup('PC', crmForm.all.new_data_vencimento_pc);
        AddGroup('PC', crmForm.all.new_valor_parcela_pc);
        AddGroup('PC', crmForm.all.new_periodicidade_pc);

        AddGroup('VM', crmForm.all.new_qtd_parcelas_vm);
        AddGroup('VM', crmForm.all.new_data_vencimento_vm);
        AddGroup('VM', crmForm.all.new_valor_parcela_vm);
        AddGroup('VM', crmForm.all.new_periodicidade_vm);

        AddGroup('IH', crmForm.all.new_valor_parcela_ih);
        AddGroup('IH', crmForm.all.new_data_vencimento_parcela_ih);

        AddGroup('TI', crmForm.all.new_valor_parcela_ti);
        AddGroup('TI', crmForm.all.new_data_vencimento_parcela_ti);

        AddGroup('DN', crmForm.all.new_valor_parcela_dn);
        AddGroup('DN', crmForm.all.new_data_vencimento_parcela_dn);

        AddGroup('MP1', crmForm.all.new_qtd_parcelas_mp1);
        AddGroup('MP1', crmForm.all.new_data_vencimento_mp1);
        AddGroup('MP1', crmForm.all.new_valor_parcelas_mp1);
        AddGroup('MP1', crmForm.all.new_periodicidade_mp1);

        AddGroup('MP2', crmForm.all.new_qtd_parcelas_mp2);
        AddGroup('MP2', crmForm.all.new_data_vencimento_mp2);
        AddGroup('MP2', crmForm.all.new_valor_parcelas_mp2);
        AddGroup('MP2', crmForm.all.new_periodicidade_mp2);

        AddGroup('MP3', crmForm.all.new_qtd_parcelas_mp3);
        AddGroup('MP3', crmForm.all.new_data_vencimento_mp3);
        AddGroup('MP3', crmForm.all.new_valor_parcelas_mp3);
        AddGroup('MP3', crmForm.all.new_periodicidade_mp3);

        AddGroup('MP4', crmForm.all.new_qtd_parcelas_mp4);
        AddGroup('MP4', crmForm.all.new_data_vencimento_mp4);
        AddGroup('MP4', crmForm.all.new_valor_parcelas_mp4);
        AddGroup('MP4', crmForm.all.new_periodicidade_mp4);

        AddGroup('MP5', crmForm.all.new_qtd_parcelas_mp5);
        AddGroup('MP5', crmForm.all.new_data_vencimento_mp5);
        AddGroup('MP5', crmForm.all.new_valor_parcelas_mp5);
        AddGroup('MP5', crmForm.all.new_periodicidade_mp5);


        AtivarGrupos();

        new_valor_total_desconto_onchange0();
        new_valor_total_acrescimo_onchange0();

        crmForm.ConfigurarIsencaoTotalMultaMora();

        function filtrarProduto() {
            var cliente;
            var oParam;
            if (crmForm.all.new_accountid.DataValue != null) {
                cliente = crmForm.all.new_accountid.DataValue[0].id;
                oParam = "objectTypeCode=1024004&filterDefault=false&attributesearch=name&_new_accountid=" + cliente;
                crmForm.FilterLookup(crmForm.all.new_unidade_carta_credito, 1024, "/MRVWeb/FilterLookup/FilterLookup.aspx", oParam);
            }
        }
        filtrarProduto();
    }

    crmForm.ValidarValoresProposta = function() {
        var indicadorDesabilitar = false;
        new_valortotalproposta_onchange0();
        new_valor_total_desconto_onchange0();
        new_valor_total_acrescimo_onchange0();
        crmForm.ConfigurarVisaoFormulario(indicadorDesabilitar);
        if (!Informacoes.ValidarRenegociacaoIN()) {
            var vlrBloqueio = 0;
            var vlrPago = 0;
            var vlrRenegociado = 0;
            var valorTotalDesconto = 0;
            if (crmForm.all.new_valortotalproposta.DataValue != null) {
                vlrRenegociado = crmForm.all.new_valortotalproposta.DataValue;
            }
            if (crmForm.all.new_valor_pago.DataValue != null) {
                vlrPago = crmForm.all.new_valor_pago.DataValue;
            }
            if (crmForm.all.new_valor_bloqueio.DataValue != null) {
                vlrBloqueio = parseFloat(crmForm.all.new_valor_bloqueio.DataValue.toFixed(2));
            }
            if (crmForm.all.new_valor_total_desconto.DataValue != null) {
                valorTotalDesconto = crmForm.all.new_valor_total_desconto.DataValue;
            }

            var valorProposta = crmForm.ConverterFloat(vlrRenegociado + vlrPago + valorTotalDesconto, 2);

            if (valorProposta < vlrBloqueio) {
                alert(Mensagem.VALIDACAO_VALOR_BLOQUEIO);
                indicadorDesabilitar = true;
                crmForm.ConfigurarVisaoFormulario(indicadorDesabilitar);
                event.returnValue = false;
                return false;
            }
        }
    }

    crmForm.ConfigurarCamposAtualizacao = function() {
        crmForm.all.new_qtde_parcelas_dm.ForceSubmit = true;
        crmForm.all.new_saldo_multa_mora.ForceSubmit = true;
        crmForm.all.new_saldo_multa_mora_mp.ForceSubmit = true;
        crmForm.all.new_valor_total_desconto.ForceSubmit = true;
        crmForm.all.new_valor_total_acrescimo.ForceSubmit = true;
        crmForm.all.new_qtde_parcelas_ih.ForceSubmit = true;
        crmForm.all.new_periodicidade_ih.ForceSubmit = true;
        crmForm.all.new_qtde_parcelas_dn.ForceSubmit = true;
        crmForm.all.new_periodicidade_dn.ForceSubmit = true;
        crmForm.all.new_qtde_parcelas_ti.ForceSubmit = true;
        crmForm.all.new_periodicidade_ti.ForceSubmit = true;
        crmForm.all.new_valor_parcelas_fp.ForceSubmit = true;
    }

    crmForm.CalcularTotalAcrescimo = function() {
        var valorTotal = 0;
        if (crmForm.all.new_acrescimo_sinal_1.DataValue != null)
            valorTotal += crmForm.all.new_acrescimo_sinal_1.DataValue;
        if (crmForm.all.new_acrescimo_sinal_2.DataValue != null)
            valorTotal += crmForm.all.new_acrescimo_sinal_2.DataValue;
        if (crmForm.all.new_acrescimo_sinal_3.DataValue != null)
            valorTotal += crmForm.all.new_acrescimo_sinal_3.DataValue;
        if (crmForm.all.new_acrescimo_sinal_4.DataValue != null)
            valorTotal += crmForm.all.new_acrescimo_sinal_4.DataValue;
        if (crmForm.all.new_acrescimo_dif_1.DataValue != null)
            valorTotal += crmForm.all.new_acrescimo_dif_1.DataValue;
        if (crmForm.all.new_acrescimo_dif_2.DataValue != null)
            valorTotal += crmForm.all.new_acrescimo_dif_2.DataValue;
        if (crmForm.all.new_acrescimo_dif_3.DataValue != null)
            valorTotal += crmForm.all.new_acrescimo_dif_3.DataValue;
        if (crmForm.all.new_acrescimo_dif_4.DataValue != null)
            valorTotal += crmForm.all.new_acrescimo_dif_4.DataValue;
        if (crmForm.all.new_acrescimo_dif_5.DataValue != null)
            valorTotal += crmForm.all.new_acrescimo_dif_5.DataValue;
        if (crmForm.all.new_acrescimo_dif_6.DataValue != null)
            valorTotal += crmForm.all.new_acrescimo_dif_6.DataValue;
        if (crmForm.all.new_acrescimo_dif_7.DataValue != null)
            valorTotal += crmForm.all.new_acrescimo_dif_7.DataValue;
        if (crmForm.all.new_acrescimo_dif_8.DataValue != null)
            valorTotal += crmForm.all.new_acrescimo_dif_8.DataValue;
        if (crmForm.all.new_acrescimo_mensal_1.DataValue != null)
            valorTotal += crmForm.all.new_acrescimo_mensal_1.DataValue;
        if (crmForm.all.new_acrescimo_mensal_2.DataValue != null)
            valorTotal += crmForm.all.new_acrescimo_mensal_2.DataValue;
        if (crmForm.all.new_acrescimo_mensal_3.DataValue != null)
            valorTotal += crmForm.all.new_acrescimo_mensal_3.DataValue;
        if (crmForm.all.new_acrescimo_mensal_4.DataValue != null)
            valorTotal += crmForm.all.new_acrescimo_mensal_4.DataValue;
        if (crmForm.all.new_acrescimo_mensal_5.DataValue != null)
            valorTotal += crmForm.all.new_acrescimo_mensal_5.DataValue;
        if (crmForm.all.new_acrescimo_mensal_6.DataValue != null)
            valorTotal += crmForm.all.new_acrescimo_mensal_6.DataValue;
        if (crmForm.all.new_acrescimo_mensal_7.DataValue != null)
            valorTotal += crmForm.all.new_acrescimo_mensal_7.DataValue;
        if (crmForm.all.new_acrescimo_mensal_8.DataValue != null)
            valorTotal += crmForm.all.new_acrescimo_mensal_8.DataValue;
        if (crmForm.all.new_acrescimo_mensal_9.DataValue != null)
            valorTotal += crmForm.all.new_acrescimo_mensal_9.DataValue;
        if (crmForm.all.new_acrescimo_mensal_10.DataValue != null)
            valorTotal += crmForm.all.new_acrescimo_mensal_10.DataValue;
        if (crmForm.all.new_acrescimo_mensal_11.DataValue != null)
            valorTotal += crmForm.all.new_acrescimo_mensal_11.DataValue;
        if (crmForm.all.new_acrescimo_mensal_12.DataValue != null)
            valorTotal += crmForm.all.new_acrescimo_mensal_12.DataValue;
        if (crmForm.all.new_acrescimo_mensal_13.DataValue != null)
            valorTotal += crmForm.all.new_acrescimo_mensal_13.DataValue;
        if (crmForm.all.new_acrescimo_mensal_14.DataValue != null)
            valorTotal += crmForm.all.new_acrescimo_mensal_14.DataValue;
        if (crmForm.all.new_acrescimo_mensal_15.DataValue != null)
            valorTotal += crmForm.all.new_acrescimo_mensal_15.DataValue;
        if (crmForm.all.new_acrescimo_mensal_16.DataValue != null)
            valorTotal += crmForm.all.new_acrescimo_mensal_16.DataValue;
        if (crmForm.all.new_acrescimo_mensal_17.DataValue != null)
            valorTotal += crmForm.all.new_acrescimo_mensal_17.DataValue;
        if (crmForm.all.new_acrescimo_mensal_18.DataValue != null)
            valorTotal += crmForm.all.new_acrescimo_mensal_18.DataValue;
        if (crmForm.all.new_acrescimo_mensal_19.DataValue != null)
            valorTotal += crmForm.all.new_acrescimo_mensal_19.DataValue;
        if (crmForm.all.new_acrescimo_mensal_20.DataValue != null)
            valorTotal += crmForm.all.new_acrescimo_mensal_20.DataValue;
        if (crmForm.all.new_acrescimo_intermediaria_1.DataValue != null)
            valorTotal += crmForm.all.new_acrescimo_intermediaria_1.DataValue;
        if (crmForm.all.new_acrescimo_intermediaria_2.DataValue != null)
            valorTotal += crmForm.all.new_acrescimo_intermediaria_2.DataValue;
        if (crmForm.all.new_acrescimo_intermediaria_3.DataValue != null)
            valorTotal += crmForm.all.new_acrescimo_intermediaria_3.DataValue;
        if (crmForm.all.new_acrescimo_financiamento.DataValue != null)
            valorTotal += crmForm.all.new_acrescimo_financiamento.DataValue;
        if (crmForm.all.new_acrescimo_fgts.DataValue != null)
            valorTotal += crmForm.all.new_acrescimo_fgts.DataValue;
        if (crmForm.all.new_acrescimo_desc_promo.DataValue != null)
            valorTotal += crmForm.all.new_acrescimo_desc_promo.DataValue;
        if (crmForm.all.new_acrescimo_dev_valores.DataValue != null)
            valorTotal += crmForm.all.new_acrescimo_dev_valores.DataValue;
        if (crmForm.all.new_acrescimo_pc.DataValue != null)
            valorTotal += crmForm.all.new_acrescimo_pc.DataValue;
        if (crmForm.all.new_acrescimo_vm.DataValue != null)
            valorTotal += crmForm.all.new_acrescimo_vm.DataValue;
        if (crmForm.all.new_acrescimo_mp1.DataValue != null)
            valorTotal += crmForm.all.new_acrescimo_mp1.DataValue;
        if (crmForm.all.new_acrescimo_mp2.DataValue != null)
            valorTotal += crmForm.all.new_acrescimo_mp2.DataValue;
        if (crmForm.all.new_acrescimo_mp3.DataValue != null)
            valorTotal += crmForm.all.new_acrescimo_mp3.DataValue;
        if (crmForm.all.new_acrescimo_mp4.DataValue != null)
            valorTotal += crmForm.all.new_acrescimo_mp4.DataValue;
        if (crmForm.all.new_acrescimo_mp5.DataValue != null)
            valorTotal += crmForm.all.new_acrescimo_mp5.DataValue;

        crmForm.all.new_valor_total_acrescimo.DataValue = valorTotal;
    }

    crmForm.ValidarValorDescontoPropostaEspecial = function() {
        var msgErro = 'Esse Contrato já teve Desconto Especial e o desconto total informado ultrapassou o valor de multa e mora. Não é possível salvar a proposta.'

        if (!crmForm.all.new_valor_total_desconto.DataValue) {
            return;
        }

        if (IsDescontoEspecial && crmForm.all.new_valor_total_desconto.DataValue > 0 &&
             (!crmForm.all.new_saldo_multa_mora.DataValue || crmForm.all.new_valor_total_desconto.DataValue > crmForm.all.new_saldo_multa_mora.DataValue)) {
            alert(msgErro);
            event.returnValue = false;
            return false;
        }
    }
} catch (error) {
    formularioValido = false;
    alert("Ocorreu um erro no formulário.\n" + error.description);
}
