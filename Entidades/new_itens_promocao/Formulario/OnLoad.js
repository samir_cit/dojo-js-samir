﻿formularioValido = true;
try {
    function loadScript() {
        var oHead = document.getElementsByTagName('head')[0];
        var oScript = document.createElement('script');
        oScript.type = "text/javascript";
        oScript.src = "/_static/Base.js";

        oScript.onreadystatechange = function() {
            if (this.readyState == 'complete' || this.readyState == 'loaded') {
                Load();
            }
        }
        oHead.appendChild(oScript);
    }

    loadScript();

    function Load() {

        TipoItem = {
            ITBI: "1",
            REGISTRO: "2",
            PRODUTOS: "3",
            DESCONTO: "4"
        }

        crmForm.MontaNome = function() {
            if (crmForm.all.new_tipo.DataValue == TipoItem.DESCONTO) {
                crmForm.all.new_name.DataValue = "Parcela Desconto - " + crmForm.all.new_valor.DataValue;
                crmForm.all.new_name.ForceSubmit = true;
            }
        }

        crmForm.AtualizaTipo = function() {
            if (crmForm.all.new_tipo.DataValue == TipoItem.DESCONTO) {
                crmForm.EscondeCampo("new_valor", false);
                crmForm.EscondeCampo("transactioncurrencyid", false);
                crmForm.DesabilitarCampos("new_name", true);
                crmForm.SetFieldReqLevel("new_valor", true);
            }
            else {
                crmForm.EscondeCampo("new_valor", true);
                crmForm.EscondeCampo("transactioncurrencyid", true);
                crmForm.DesabilitarCampos("new_name", false);
                crmForm.SetFieldReqLevel("new_valor", false);
            }
        }

        crmForm.AtualizaTipo();
        
        //Remove a opção de tipo de desconto
        if (crmForm.FormType == TypeCreate || crmForm.FormType == TypeUpdate) {
            crmForm.all.new_tipo.remove(TipoItem.DESCONTO);
        }
    }

} catch (error) {
    formularioValido = false;
    alert("Ocorreu um erro no formulário.\n" + error.description);
}
