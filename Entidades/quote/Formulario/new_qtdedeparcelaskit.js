﻿function ExibirDesconto(percentualDesconto) {
    crmForm.all.new_desconto.DataValue = 100 * percentualDesconto; // exibe o valor do percentual do desconto
    crmForm.all.new_desconto.ForceSubmit = true;
}

function CalculaReajusteTabelaTaxa(tempoCarencia) {
    var valorReajustadoTaxaCarencia = 0;
    var valorTotal = crmForm.all.new_valor_total_kits.DataValue;

    switch (tempoCarencia) {
        case 1:
            valorReajustadoTaxaCarencia = valorTotal * TabelaReajusteTaxaCarencia.TAXA_1_MES_CARENCIA;
            break;
        case 2:
            valorReajustadoTaxaCarencia = valorTotal * TabelaReajusteTaxaCarencia.TAXA_2_MESES_CARENCIA;
            break;
        case 3:
            valorReajustadoTaxaCarencia = valorTotal * TabelaReajusteTaxaCarencia.TAXA_3_MESES_CARENCIA;
            break;
        case 4:
            valorReajustadoTaxaCarencia = valorTotal * TabelaReajusteTaxaCarencia.TAXA_4_MESES_CARENCIA;
            break;
        case 5:
            valorReajustadoTaxaCarencia = valorTotal * TabelaReajusteTaxaCarencia.TAXA_5_MESES_CARENCIA;
            break;
        case 6:
            valorReajustadoTaxaCarencia = valorTotal * TabelaReajusteTaxaCarencia.TAXA_6_MESES_CARENCIA;
            break;
        default:
            valorReajustadoTaxaCarencia = valorTotal;
            break;
    }
    return valorReajustadoTaxaCarencia;
}

if (crmForm.all.new_qtdedeparcelaskit.DataValue != null && crmForm.all.new_qtdedeparcelaskit.DataValue != OpcoesQuantidadeParcelas.LIM_INFERIOR) {
    /// Recuperando a condição de pagamento e a quantidade de parcelas escolhida.
    var condicaoPagamento = crmForm.all.paymenttermscode.DataValue;
    var quantidadeParcela = crmForm.all.new_qtdedeparcelaskit.DataValue;
    var valorEntrada = crmForm.all.new_valor_entrada.DataValue;
    var valorParcela = null;
    var tempoCarencia = crmForm.all.new_tempodecarencia.DataValue;
    /// Configurando o valor da parcela de acordo com a condição de pagamento selecionada.
    if (condicaoPagamento == CondicoesDePagamento.A_PRAZO) {
        if (tempoCarencia != null) {
            if (parseInt(tempoCarencia) + quantidadeParcela > OpcoesQuantidadeParcelas.LIM_SUPERIOR) {
                crmForm.all.new_qtdedeparcelaskit.DataValue = null;
                alert("Soma do tempo de carência com a quantidade de parcelas não pode ser maior que " + OpcoesQuantidadeParcelas.LIM_SUPERIOR + ".");
                crmForm.all.new_qtdedeparcelaskit.SetFocus();
                return;
            }
        }
        if (quantidadeParcela > OpcoesQuantidadeParcelas.LIM_SUPERIOR) {
            crmForm.all.new_qtdedeparcelaskit.DataValue = null;
            alert("Para venda de kit a quantidade máxima de parcela é " + OpcoesQuantidadeParcelas.LIM_SUPERIOR + ".");
            crmForm.all.new_qtdedeparcelaskit.SetFocus();
        } else {

            if (quantidadeParcela <= OpcoesQuantidadeParcelas.LIM_INTERMEDIARIO && tempoCarencia == null) {
                valorParcela = crmForm.all.new_valor_total_kits.DataValue / quantidadeParcela;
            } else if (tempoCarencia != null) {
                var valorReajustadoTaxa = CalculaReajusteTabelaTaxa(parseInt(tempoCarencia));

                valorParcela = crmForm.CalcularValorParcelaKit(quantidadeParcela, valorReajustadoTaxa);
            } else if (quantidadeParcela > OpcoesQuantidadeParcelas.LIM_INTERMEDIARIO && tempoCarencia == null) {
                valorParcela = crmForm.CalcularValorParcelaKit(quantidadeParcela, crmForm.all.new_valor_total_kits.DataValue);
            }

            if (null == valorParcela) alert("Ocorreu um erro ao gerar o valor da parcela.\nInsira a quantidade de parcelas novamente para que o sistema possa recalcular o valor.\nSe o erro persistir contate o administrador do sistema.");
            else {
                /// O valor é repassado como "string", pois ao fazer um parseFloat para atribuir ao campo money do Crm o mesmo era arredondado.
                crmForm.all.new_valordaparcelakit.DataValue = parseFloat(valorParcela.toString().replace(',', '.'));

				var dataBase = crmForm.all.new_data_base_contrato.DataValue;
                if (dataBase != null) {
                    
                    var mes = parseInt(dataBase.getMonth()) + 1;

                    if (tempoCarencia == null) {
                        tempoCarencia = 0;
                    } else {
                        mes = parseInt(dataBase.getMonth()) + parseInt(tempoCarencia) + 1;
                    }
                    crmForm.all.new_datado1vencimento.Disabled = true;

                    var novaData = new Date;
                    novaData.setDate(20);
                    novaData.setMonth(mes);

                    if (mes >= 12) {
                        novaData.setFullYear(dataBase.getFullYear() + 1);
                    } else {
                        novaData.setFullYear(dataBase.getFullYear());
                    }

                    crmForm.all.new_datado1vencimento.DataValue = novaData;

                } else {
                    alert("Necessário preencher a Data Base do Contrato para determinar o tempo de carência.");
                    crmForm.all.new_qtdedeparcelaskit.DataValue = null;
                    crmForm.all.new_datado1vencimento.Disabled = false;
                    crmForm.all.new_data_base_contrato.SetFocus();
                }
                if ((quantidadeParcela > 0 && parseInt(tempoCarencia) > 0) || quantidadeParcela > OpcoesQuantidadeParcelas.LIM_INTERMEDIARIO) {
                    crmForm.all.new_visualizacao_valor_total_parcelamento_kit.DataValue = parseFloat(crmForm.all.new_valordaparcelakit.DataValue * quantidadeParcela);
                    crmForm.all.new_valortotalproposta.DataValue = parseFloat(crmForm.all.new_valordaparcelakit.DataValue * quantidadeParcela);
                    crmForm.all.new_valordaparcelakit.DataValue = parseFloat(crmForm.all.new_visualizacao_valor_total_parcelamento_kit.DataValue / quantidadeParcela);
                } else {
                    crmForm.all.new_visualizacao_valor_total_parcelamento_kit.DataValue = crmForm.all.new_valor_total_kits.DataValue;
                    crmForm.all.new_valortotalproposta.DataValue = crmForm.all.new_valor_total_kits.DataValue;
                }
            }
        }
    }
    /// Recalcula parcelas se tiver valor da parcela
    else if (condicaoPagamento == CondicoesDePagamento.OUTRAS_CONDICOES) crmForm.all.new_valordaparcelakit.FireOnChange();

    else if (condicaoPagamento == CondicoesDePagamento.ENTRADA_MAIS_PRAZO && quantidadeParcela != null) {
        if (quantidadeParcela > OpcoesQuantidadeParcelas.LIM_SUPERIOR) {
            crmForm.all.new_qtdedeparcelaskit.DataValue = null;
            alert("Para venda de kit a quantidade máxima de parcela é " + OpcoesQuantidadeParcelas.LIM_SUPERIOR + ".");
            crmForm.all.new_qtdedeparcelaskit.SetFocus();
        } else {
            // Aplicar desconto
            //var valorDebitarParcelamento = 0;
            if (valorEntrada <= OpcoesPagamentoAVista.FAIXA_FIM_A) {
                //valorDebitarParcelamento = parseFloat(valorEntrada.toString().replace(',', '.')) * OpcoesDescontoPgtoVista.DESC_05; /* 5% Desconto */
                ExibirDesconto(OpcoesDescontoPgtoVista.DESC_05); // exibe valor do desconto
            } else if (valorEntrada > OpcoesPagamentoAVista.FAIXA_INI_B && valorEntrada <= OpcoesPagamentoAVista.FAIXA_FIM_B) {
                //valorDebitarParcelamento = parseFloat(valorEntrada.toString().replace(',', '.')) * OpcoesDescontoPgtoVista.DESC_08; /* 8% Desconto */
                ExibirDesconto(OpcoesDescontoPgtoVista.DESC_08); // exibe valor do desconto
            } else if (valorEntrada > OpcoesPagamentoAVista.FAIXA_INI_C) {
                //valorDebitarParcelamento = parseFloat(valorEntrada.toString().replace(',', '.')) * OpcoesDescontoPgtoVista.DESC_10; /* 10% Desconto */
                ExibirDesconto(OpcoesDescontoPgtoVista.DESC_10); // exibe valor do desconto
            }

            //var valorDivisaoParcelas = (parseFloat(crmForm.all.new_valor_total_kits.DataValue.toString().replace(',', '.')) - parseFloat(valorEntrada.toString().replace(',', '.'))) - valorDebitarParcelamento;

            /// Validação para calcular o indice price de cada parcela do financiamento.
            //if (quantidadeParcela <= OpcoesQuantidadeParcelas.LIM_INTERMEDIARIO && quantidadeParcela > OpcoesQuantidadeParcelas.LIM_INFERIOR) { // cálculo sem juros
                //valorParcela = valorDivisaoParcelas / quantidadeParcela;
            //} else if (quantidadeParcela > OpcoesQuantidadeParcelas.LIM_INTERMEDIARIO) {
               // valorParcela = crmForm.CalcularValorParcelaKit(quantidadeParcela, valorDivisaoParcelas);
            //}

            valorParcela = crmForm.CalcularValorParcelaKitComEntrada(quantidadeParcela, crmForm.all.new_valor_total_kits.DataValue, valorEntrada);

            if (null == valorParcela) alert("Ocorreu um erro ao gerar o valor da parcela.\nInsira a quantidade de parcelas novamente para que o sistema possa recalcular o valor.\nSe o erro persistir contate o administrador do sistema.");
            else {
			    /// O valor é repassado como "string", pois ao fazer um parseFloat para atribuir ao campo money do Crm o mesmo era arredondado.
                if (parseFloat(valorParcela.toString().replace(',', '.')) >= 0) {
                    crmForm.all.new_valordaparcelakit.DataValue = parseFloat(valorParcela.toString().replace(',', '.'));
                       crmForm.all.new_visualizacao_valor_total_parcelamento_kit.DataValue = parseFloat(valorEntrada.toString().replace(',', '.')) + parseFloat(crmForm.all.new_valordaparcelakit.DataValue * quantidadeParcela);
                       crmForm.all.new_valortotalproposta.DataValue = parseFloat(valorEntrada.toString().replace(',', '.')) + parseFloat(crmForm.all.new_valordaparcelakit.DataValue * quantidadeParcela);
                } else crmForm.all.new_valordaparcelakit.DataValue = null
            }
        }
    }
   if(crmForm.all.new_datado1vencimento != null)
   {
   		crmForm.all.new_datado1vencimento.FireOnChange();
   } 
    if (condicaoPagamento != null && crmForm.all.new_visualizacao_valor_total_parcelamento_kit.DataValue != null) {
        crmForm.all.new_valortotalservicos.DataValue = 0;
        crmForm.all.new_valortotalgeral.DataValue = crmForm.all.new_visualizacao_valor_total_parcelamento_kit.DataValue;
    }
}
