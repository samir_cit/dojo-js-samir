﻿if (crmForm.all.new_tipodecotacao.DataValue == TipoDeProposta.SERVICO || crmForm.all.new_tipodecotacao.DataValue == TipoDeProposta.UNIDADE)
{
    var vlrGrantiaProp = 0;

    if (crmForm.all.new_qtde_cs_garant_1.DataValue != null && crmForm.all.new_vlr_parc_cs_garant_1.DataValue != null)
            vlrGrantiaProp += crmForm.all.new_qtde_cs_garant_1.DataValue * crmForm.all.new_vlr_parc_cs_garant_1.DataValue;
    
    if (crmForm.all.new_qtde_cs_garant_2.DataValue != null && crmForm.all.new_vlr_parc_cs_garant_2.DataValue != null)
        vlrGrantiaProp += crmForm.all.new_qtde_cs_garant_2.DataValue * crmForm.all.new_vlr_parc_cs_garant_2.DataValue;

    /// Calcula o valor total garantia
    crmForm.all.new_vlr_tot_mes_cs_garant.DataValue = vlrGrantiaProp;
    
     /// Calcula Valor Total da Proposta
    new_valortotalproposta_onchange0();
}