﻿//Calcula valor total da seção Reembolso Cliente da Guia Despesas Gerais
new_vlr_tot_mens_reemb_onchange0();

if (crmForm.all.new_qtde_parc_reemb_2.DataValue != null && crmForm.all.new_qtde_parc_reemb_2.DataValue > 0) {
    crmForm.SetFieldReqLevel("new_vlt_parc_reemb_2", true);
    crmForm.SetFieldReqLevel("new_vcto_parc1_reemb_2", true);
    crmForm.SetFieldReqLevel("new_tipo_pagto_reemb_2", true);
}
else {
    crmForm.SetFieldReqLevel("new_vlt_parc_reemb_2", false);
    crmForm.SetFieldReqLevel("new_vcto_parc1_reemb_2", false);
    crmForm.SetFieldReqLevel("new_tipo_pagto_reemb_2", false);
}
