﻿var condicaoPagamento = crmForm.all.paymenttermscode;
var quantidadeParcela = crmForm.all.new_qtdedeparcelaskit;
var dentroTabelaVendas = crmForm.all.new_dentro_tabela_vendas;
var valorParcelaKit = crmForm.all.new_valordaparcelakit;
var valorEntrada = crmForm.all.new_valor_entrada;
var valorTotalParcelamentoKits = crmForm.all.new_visualizacao_valor_total_parcelamento_kit;
var valorTotalProposta = crmForm.all.new_valortotalproposta;
var valorTotalKits = crmForm.all.new_valor_total_kits;
var valorDesconto = crmForm.all.new_desconto;
var tempoCarencia = crmForm.all.new_tempodecarencia;
var data1vencimento = crmForm.all.new_datado1vencimento;

function CalcularDesconto(percentualDesconto) {
    valorDesconto.DataValue = (100 * percentualDesconto); // exibe o valor do percentual do desconto
    valorDesconto.ForceSubmit = true;
}

function ExibirDesconto() {
    if (valorTotalKits.DataValue != null) {
        if (valorTotalKits.DataValue <= OpcoesPagamentoAVista.FAIXA_FIM_A) {
            valorTotalParcelamentoKits.DataValue = valorTotalKits.DataValue * (1 - OpcoesDescontoPgtoVista.DESC_05); /* 5% Desconto */
            valorTotalProposta.DataValue = valorParcelaKit.DataValue = valorTotalParcelamentoKits.DataValue;
            CalcularDesconto(OpcoesDescontoPgtoVista.DESC_05); // calcula valor do desconto
        }
        else if (valorTotalKits.DataValue >= OpcoesPagamentoAVista.FAIXA_INI_B && valorTotalKits.DataValue <= OpcoesPagamentoAVista.FAIXA_FIM_B) {
            valorTotalParcelamentoKits.DataValue = valorTotalKits.DataValue * (1 - OpcoesDescontoPgtoVista.DESC_08); /* 8% Desconto */
            valorTotalProposta.DataValue = valorParcelaKit.DataValue = valorTotalParcelamentoKits.DataValue;
            CalcularDesconto(OpcoesDescontoPgtoVista.DESC_08); // calcula valor do desconto
        }
        else if (valorTotalKits.DataValue >= OpcoesPagamentoAVista.FAIXA_INI_C) {
            valorTotalParcelamentoKits.DataValue = valorTotalKits.DataValue * (1 - OpcoesDescontoPgtoVista.DESC_10); /*10% Desconto */
            valorTotalProposta.DataValue = valorParcelaKit.DataValue = valorTotalParcelamentoKits.DataValue;
            CalcularDesconto(OpcoesDescontoPgtoVista.DESC_10); // calcula valor do desconto
        }
    }
}
function ConfigurarCondicaoPagamentoAVista() {
    dentroTabelaVendas.DataValue = 1;
    quantidadeParcela.DataValue = 1;
    valorEntrada.DataValue = 0;
    valorEntrada.ForceSubmit = true;
    ExibirDesconto();
    tempoCarencia.Disabled = true;
    tempoCarencia.DataValue = null;
    data1vencimento.Disabled = false;
    quantidadeParcela.disabled = false;
    valorParcelaKit.disabled =  false;
    
}
function ConfigurarCondicaoPagamentoAPrazo() {
    dentroTabelaVendas.DataValue = 1;
    quantidadeParcela.DataValue = 0;
    valorParcelaKit.DataValue = 0;
    valorEntrada.DataValue = 0;
    valorEntrada.ForceSubmit = true;
    valorTotalParcelamentoKits.DataValue = 0;
    valorTotalProposta.DataValue = 0;
    valorDesconto.DataValue = 0;
    tempoCarencia.disabled = false;
    tempoCarencia.DataValue = null;
    quantidadeParcela.disabled = false;
    valorParcelaKit.disabled =  false;
}

function ConfigurarCondicaoPagamentoOutras() {
    if (crmForm.UsuarioPertenceEquipe("KIT – CONDIÇÕES DE PAGAMENTO")) {
        dentroTabelaVendas.DataValue = 2;
        quantidadeParcela.DataValue = 1;
        quantidadeParcela.disabled = true;
        valorParcelaKit.DataValue = 0;
        valorEntrada.DataValue = 0;
        valorEntrada.ForceSubmit = true;
        valorTotalParcelamentoKits.DataValue = 0;
        valorTotalProposta.DataValue = 0;
        valorDesconto.DataValue = 0;
        tempoCarencia.disabled = true;
        tempoCarencia.DataValue = null;
        data1vencimento.Disabled = false;
		valorParcelaKit.disabled =  true;
        ExibirDesconto();
    }
    else {
        alert("Você não tem permissão para escolher esta opção.");
        condicaoPagamento.DataValue = null;
        data1vencimento.DataValue = null;
    }
}


function LimparCamposPagina() {
    quantidadeParcela.DataValue = 0;
    valorParcelaKit.DataValue = 0;
    valorEntrada.DataValue = 0;
    valorTotalParcelamentoKits.DataValue = 0;
    valorTotalProposta.DataValue = 0;
    valorDesconto.DataValue = 0;
    tempoCarencia.disabled = false;
    tempoCarencia.DataValue = null;
    quantidadeParcela.disabled = false;
    valorParcelaKit.disabled = true;
    data1vencimento.DataValue = null;
}

function ConfigurarCondicaoPagamentoEntradaMaisPrazo() {
    dentroTabelaVendas.DataValue = 1;
    quantidadeParcela.DataValue = 0;
    valorParcelaKit.DataValue = 0;
    valorEntrada.DataValue = 0;
    valorEntrada.ForceSubmit = true;
    valorTotalParcelamentoKits.DataValue = 0;
    valorTotalProposta.DataValue = 0;
    valorDesconto.DataValue = 0;
    tempoCarencia.disabled = true;
    tempoCarencia.DataValue = null;
    data1vencimento.Disabled = false;
    quantidadeParcela.disabled = false;
}
if (valorTotalKits.DataValue == 0) {
    condicaoPagamento.DataValue = 1;
    condicaoPagamento.Disabled = true;
    quantidadeParcela.DataValue = 0;
    valorParcelaKit.DataValue = 0;
    valorParcelaKit.Disabled = true;
    valorEntrada.DataValue = 0;
    valorEntrada.ForceSubmit = true;
    valorTotalParcelamentoKits.DataValue = 0;
    crmForm.all.new_datado1vencimento.DataValue = new Date();
}
else {
    crmForm.ConfigurarCamposAbaDetalhesDaPropostaKit(condicaoPagamento.DataValue);
    LimparCamposPagina();
    switch (condicaoPagamento.DataValue) {
        case CondicoesDePagamento.A_VISTA:
            ConfigurarCondicaoPagamentoAVista();
            break;

        case CondicoesDePagamento.A_PRAZO:
            ConfigurarCondicaoPagamentoAPrazo();
            break;

        case CondicoesDePagamento.ENTRADA_MAIS_PRAZO:
            ConfigurarCondicaoPagamentoEntradaMaisPrazo();
            break;

        case CondicoesDePagamento.OUTRAS_CONDICOES:
            ConfigurarCondicaoPagamentoOutras();
            break;
    }
}