﻿/// Total reembolso - Cartorios - despesas gerais
if (crmForm.all.new_tipodecotacao.DataValue == TipoDeProposta.SERVICO || crmForm.all.new_tipodecotacao.DataValue == TipoDeProposta.UNIDADE) 
{
    var vlrReembCliProp = 0;
    if (crmForm.all.new_qtdeparcelasrembclientecart1.DataValue != null && crmForm.all.new_valordasparcelasreembclientecart1.DataValue != null)
        vlrReembCliProp += crmForm.all.new_qtdeparcelasrembclientecart1.DataValue * crmForm.all.new_valordasparcelasreembclientecart1.DataValue;
    if (crmForm.all.new_qtdeparcelasrembclientecart2.DataValue != null && crmForm.all.new_valordasparcelasreembclientecart2.DataValue != null)
        vlrReembCliProp += crmForm.all.new_qtdeparcelasrembclientecart2.DataValue * crmForm.all.new_valordasparcelasreembclientecart2.DataValue;
   
    /// Obrigar o preenchimento de ambos os campos de ITBI e cartorios
    if (vlrReembCliProp > 0) 
    {
        crmForm.all.new_valortotalreembclientecart.DataValue = vlrReembCliProp;
        crmForm.SetFieldReqLevel("new_qtde_parc_reemb_1", true);
        crmForm.SetFieldReqLevel("new_vlt_parc_reemb_1", true);
        crmForm.SetFieldReqLevel("new_vcto_parc1_reemb_1", true);
        crmForm.SetFieldReqLevel("new_tipo_pagto_reemb_1", true);
        crmForm.SetFieldReqLevel("new_qtdeparcelasrembclientecart1", true);
        crmForm.SetFieldReqLevel("new_valordasparcelasreembclientecart1", true);
        crmForm.SetFieldReqLevel("new_vencparcelareembclientecart1", true);
        crmForm.SetFieldReqLevel("new_tipodepagamentoreembclientecart1", true);
    }
    else 
    {
        crmForm.all.new_valortotalreembclientecart.DataValue = null;
        if (crmForm.all.new_vlr_tot_mens_reemb.DataValue == null || crmForm.all.new_vlr_tot_mens_reemb.DataValue == 0) 
        {
            crmForm.SetFieldReqLevel("new_qtde_parc_reemb_1", false);
            crmForm.SetFieldReqLevel("new_vlt_parc_reemb_1", false);
            crmForm.SetFieldReqLevel("new_vcto_parc1_reemb_1", false);
            crmForm.SetFieldReqLevel("new_tipo_pagto_reemb_1", false);
            crmForm.SetFieldReqLevel("new_qtdeparcelasrembclientecart1", false);
            crmForm.SetFieldReqLevel("new_valordasparcelasreembclientecart1", false);
            crmForm.SetFieldReqLevel("new_vencparcelareembclientecart1", false);
            crmForm.SetFieldReqLevel("new_tipodepagamentoreembclientecart1", false);
        }
    }
    new_valortotalproposta_onchange0();
}