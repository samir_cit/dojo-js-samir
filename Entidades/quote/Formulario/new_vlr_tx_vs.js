﻿crmForm.SetFieldReqLevel('new_forma_pagamento_tx_desp_vs', crmForm.all.new_vlr_tx_vs.DataValue != null);
crmForm.SetFieldReqLevel('new_forma_pgto_taxa_externa', crmForm.all.new_vlr_tx_vs.DataValue != null);
if (crmForm.all.new_vlr_tx_vs != null && crmForm.all.new_vlr_tx_vs.DataValue != null) {
    if (crmForm.all.new_forma_pagamento_tx_desp_vs.DataValue != null) {
        crmForm.all.new_parcelamentotaxasexterno.DataValue =
            crmForm.CalculaValor(crmForm.all.new_vlr_tx_vs.DataValue,
            crmForm.all.new_forma_pagamento_tx_desp_vs.DataValue, crmForm.all.new_parcelamentotaxasexterno);

        if (crmForm.verificarTipoServicoTaxaDespachante()) {
            crmForm.ValidaValorTaxaDespachante(crmForm.all.new_forma_pagamento_tx_desp_vs,
            crmForm.all.new_vlr_tx_vs, crmForm.all.new_parcelamentotaxasexterno);
        }
    }
}
else
    crmForm.all.new_parcelamentotaxasexterno.DataValue = null;