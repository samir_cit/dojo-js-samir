﻿//Calcula valor total da seção mensal da guia Detalhes da Proposta Kit/Garagem...
new_new_vlr_tot_mens_money_onchange0();
if (crmForm.all.new_qtde_parc_s2.DataValue != null && crmForm.all.new_qtde_parc_s2.DataValue > 0) {
    crmForm.SetFieldReqLevel("new_vlr_parc_mens_2", true);
    crmForm.SetFieldReqLevel("new_dt_vencto_parc_2", true);
    crmForm.SetFieldReqLevel("new_tipo_pagto_mensal_2", true);
}
else {
    crmForm.SetFieldReqLevel("new_vlr_parc_mens_2", false);
    crmForm.SetFieldReqLevel("new_dt_vencto_parc_2", false);
    crmForm.SetFieldReqLevel("new_tipo_pagto_mensal_2", false);
}