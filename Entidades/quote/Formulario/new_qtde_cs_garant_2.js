﻿//Calcula valor total da seção Casa Garantia da Guia Despesas Gerais
new_vlr_tot_mes_cs_garant_onchange0();

if (crmForm.all.new_qtde_cs_garant_2.DataValue != null && crmForm.all.new_qtde_cs_garant_2.DataValue > 0) {
    crmForm.SetFieldReqLevel("new_vlr_parc_cs_garant_2", true);
    crmForm.SetFieldReqLevel("new_dt_vcto_1parcela_2", true);
    crmForm.SetFieldReqLevel("new_tp_pagto_cs_garant_2", true);
}
else {
    crmForm.SetFieldReqLevel("new_vlr_parc_cs_garant_2", false);
    crmForm.SetFieldReqLevel("new_dt_vcto_1parcela_2", false);
    crmForm.SetFieldReqLevel("new_tp_pagto_cs_garant_2", false);
}
