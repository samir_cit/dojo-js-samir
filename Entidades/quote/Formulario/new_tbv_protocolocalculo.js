﻿//---------Fazer o cálculo do total de financiamento------------------------------------------------//

var assessoria = 0;
if (crmForm.all.new_tbv_assessoria.DataValue != null)
    assessoria = parseFloat(crmForm.all.new_tbv_assessoria.DataValue);
    
var sinal = 0;

if (crmForm.all.new_tbv_ato.DataValue != null)
    sinal = parseFloat(crmForm.all.new_tbv_ato.DataValue);
if (crmForm.all.new_tbv_qtdeparcsinal.DataValue != null && crmForm.all.new_tbv_valorparcsinal.DataValue != null)
    sinal += (parseInt(crmForm.all.new_tbv_qtdeparcsinal.DataValue) * parseFloat(crmForm.all.new_tbv_valorparcsinal.DataValue));

crmForm.all.new_tbv_vtotalsinal.DataValue = parseFloat(sinal);
crmForm.all.new_tbv_vtotalsinal.ForceSubmit = true;

var mensal = 0;

if (crmForm.all.new_tbv_qtdeparcmensal1.DataValue != null && crmForm.all.new_tbv_valorparcmensal1.DataValue != null)
    mensal += (parseInt(crmForm.all.new_tbv_qtdeparcmensal1.DataValue) * parseFloat(crmForm.all.new_tbv_valorparcmensal1.DataValue));

if (crmForm.all.new_tbv_qtdeparcmensal2.DataValue != null && crmForm.all.new_tbv_valorparcmensal2.DataValue != null)
    mensal += (parseInt(crmForm.all.new_tbv_qtdeparcmensal2.DataValue) * parseFloat(crmForm.all.new_tbv_valorparcmensal2.DataValue));

crmForm.all.new_tbv_vtotalmensais.DataValue = parseFloat(mensal);
crmForm.all.new_tbv_vtotalmensais.ForceSubmit = true;

var intermediaria = 0;

if (crmForm.all.new_tbv_qtdeparcintermed.DataValue != null && crmForm.all.new_tbv_valorparcintermed.DataValue != null)
    intermediaria = parseInt(crmForm.all.new_tbv_qtdeparcintermed.DataValue) * parseFloat(crmForm.all.new_tbv_valorparcintermed.DataValue);

crmForm.all.new_tbv_vtotalintermediarias.DataValue = parseFloat(intermediaria);
crmForm.all.new_tbv_vtotalintermediarias.ForceSubmit = true;

var financiamento = 0;

if (crmForm.all.new_tbv_totalfinanciamento.DataValue != null)
    financiamento = parseFloat(crmForm.all.new_tbv_totalfinanciamento.DataValue);

var total = assessoria + sinal + mensal + intermediaria + financiamento;
crmForm.all.new_tbv_protocolocalculo.DataValue = parseFloat(total);
crmForm.all.new_tbv_protocolocalculo.ForceSubmit = true;