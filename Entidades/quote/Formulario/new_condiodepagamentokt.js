﻿if (crmForm.all.new_parcelamentodekit.DataValue == 1) 
{
    crmForm.all.new_qtdedeparcelaskit.readOnly = false;
    crmForm.all.new_qtdedeparcelaskit.SetFocus();
    crmForm.all.new_valordaparcelakit.readOnly = true;

    crmForm.all.new_condiodepagamentokt.DataValue = " Parcelamentos sob as seguintes condições: \n" + crmForm.all.new_qtdedeparcelaskit.DataValue + " parcelas  no valor de R$ " + crmForm.all.new_valordaparcelakit.DataValue + " ,00 cada uma, com vencimentos mensais e sucessivos. Representados pelos cheque(s): " + crmForm.all.new_chequekitacabamento.DataValue + ", Agência nº: " + crmForm.all.new_agenciakitacabamento.DataValue + ", Conta nº:" + crmForm.all.new_contakitacabamento.DataValue + ", Banco:" + crmForm.all.new_bancokitacabamento.DataValue + ".";
}
else 
{
    crmForm.all.new_qtdedeparcelaskit.DataValue = null;
    crmForm.all.new_valordaparcelakit.DataValue = null;
    crmForm.all.new_qtdedeparcelaskit.readOnly = true;
    crmForm.all.new_valordaparcelakit.readOnly = true;

    crmForm.all.new_condiodepagamentokt.DataValue = null;
    crmForm.all.new_condiodepagamentokt.DataValue = " Á Vista no valor de R$ " + crmForm.all.totalamount.DataValue + ",00 , representado pelo cheque: " + crmForm.all.new_chequekitacabamento.DataValue + ", Agência nº: " + crmForm.all.new_agenciakitacabamento.DataValue + ", Conta nº:" + crmForm.all.new_contakitacabamento.DataValue + ", Banco:" + crmForm.all.new_bancokitacabamento.DataValue + ".";

}