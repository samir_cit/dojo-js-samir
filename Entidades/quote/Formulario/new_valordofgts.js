﻿//Impede que o valor de fgts seja maior que o aprovado pelo sistema de credito
var valorFgtsCredito = 0;
var valorFGTS = 0;
if (crmForm.all.new_valor_fgts_credito.DataValue != null)
    valorFgtsCredito = crmForm.all.new_valor_fgts_credito.DataValue;

if (crmForm.all.new_valordofgts.DataValue != null)
    valorFGTS = crmForm.all.new_valordofgts.DataValue;

if (valorFgtsCredito > 0 && valorFGTS > valorFgtsCredito) {
    alert("Valor do FGTS não pode ser maior que o valor aprovado pelo Crédito!");
    crmForm.all.new_valordofgts.DataValue = 0;
}
new_valortotalproposta_onchange0();