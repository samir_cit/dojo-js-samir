﻿try {
    if(crmForm.all.new_data_base_contrato.DataValue == null){
        return;
    }
    
    crmForm.all.new_datadopagamento.Disabled = false;
    crmForm.all.new_datadopagamento.DataValue = null;
    crmForm.all.new_tipo_pagamento_sinal.DataValue = null;
    
    if(!crmForm.ValidarDataBase()){
        crmForm.all.new_data_base_contrato.DataValue = null;
        crmForm.all.new_data_base_contrato.SetFocus();
        return;
    }    
}
catch (erro) {
    alert("Ocorreu um erro na validação da data base.\n" + erro.description);
}
