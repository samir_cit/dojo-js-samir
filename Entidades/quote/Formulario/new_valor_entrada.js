﻿if (crmForm.all.new_valor_entrada.DataValue == null) {
    crmForm.all.new_valor_entrada.DataValue = 0;
}
if (crmForm.all.paymenttermscode.DataValue != null && crmForm.all.paymenttermscode.DataValue == CondicoesDePagamento.OUTRAS_CONDICOES) {
    var valorEntrada = crmForm.all.new_valor_entrada.DataValue;
    crmForm.all.paymenttermscode.FireOnChange();
    crmForm.all.new_valor_entrada.DataValue = valorEntrada;
    crmForm.all.new_valordaparcelakit.DataValue = parseFloat(crmForm.all.new_visualizacao_valor_total_parcelamento_kit.DataValue) - parseFloat(crmForm.all.new_valor_entrada.DataValue);
    
}
else {
    crmForm.all.new_qtdedeparcelaskit.FireOnChange();
}