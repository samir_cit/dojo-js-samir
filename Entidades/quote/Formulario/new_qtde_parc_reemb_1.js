﻿//Calcula valor total da seção Reembolso Cliente da Guia Despesas Gerais
if (crmForm.all.new_qtde_parc_reemb_1.DataValue != null && crmForm.all.new_qtde_parc_reemb_1.DataValue == 0) {
    crmForm.all.new_qtde_parc_reemb_1.DataValue = null;
}
new_vlr_tot_mens_reemb_onchange0();
if (crmForm.all.new_qtde_parc_reemb_1.DataValue != null && crmForm.all.new_qtde_parc_reemb_1.DataValue > 0) {
    crmForm.SetFieldReqLevel("new_qtde_parc_reemb_1", true);
    crmForm.SetFieldReqLevel("new_vlt_parc_reemb_1", true);
    crmForm.SetFieldReqLevel("new_vcto_parc1_reemb_1", true);
    crmForm.SetFieldReqLevel("new_tipo_pagto_reemb_1", true);
}