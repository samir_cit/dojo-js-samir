﻿try 
{
    //Desabilitar a execução do OnSave quando o formulário é somente leitura ou desabilitado.
    //Este procedimento é necessário para não causar erro na revisão da proposta.
    if (crmForm.FormType == 3 || crmForm.FormType == 4)
        return;
    
    if (!formularioValido) {
        alert("Ocorreu um erro no formulário. Impossível salvar.");
        event.returnValue = false;
        return false;
    }

    if (crmForm.all.opportunityid.DataValue == null) {
        alert("Selecione uma oportunidade!");
        event.returnValue = false;
        return false;
    }
    
    /* Se a Taxa Despachante estiver preenchida, não precisa ter uma justificativa obrigatória. */
    function ValidarTaxaDespachante()
    {
        if (crmForm.all.new_vlr_tx.DataValue != null && crmForm.all.new_vlr_tx.DataValue != "0")
            crmForm.SetFieldReqLevel("new_justificativa_taxa_despachante", false);
    }
    
    function ValidaPreenchimento() 
    {
       var database = crmForm.all.new_data_base_contrato.DataValue;      
       var datas = 'new_datadopagamento;new_datadevencimento1parcela;new_datadevencimento1parcelasinal1;new_datadevencimento1parcelasinal2;new_datadevencimento1parcelasinal3;new_datadevencimento1parcelasinal4;new_datadepagamentomensal;new_datado1pagamentomensal1;new_datado1pagamentomensal2;new_datado1pagamentomensal3;new_datado1pagamentomensal4;new_datado1pagamentomensal5;new_datadepagamentointermediaria;new_datadepagamentointermediria1;new_datadepagamentointermediria2;new_datado1vencimento';
       var tmp = datas.split(";");
       for (var i = 0; i < tmp.length; i++) {
          var atributo = eval("crmForm.all." + tmp[i]);
          if (atributo != null && atributo.DataValue != null && atributo.DataValue < database )
          {
            alert('Existe data de vencimento de parcela menor que a data base do contrato. Favor alterar a data de vencimento.');
            return false;
          }   
       }
       return true;
    }
    
    function ValidarLimiteCreditoKit() {
        if (event.Mode == EVENT_MODE_ONSAVE_SAVE || event.Mode == EVENT_MODE_ONSAVE_SAVE_AND_CLOSE) {
            if (crmForm.all.paymenttermscode.DataValue != null &&
                crmForm.all.paymenttermscode.DataValue != CondicoesDePagamento.A_VISTA &&
                crmForm.all.new_qtdedeparcelaskit.DataValue != null &&
                crmForm.all.new_valordaparcelakit.DataValue != null) {

                var valor = 0;

                switch (crmForm.all.paymenttermscode.DataValue) {
                    case CondicoesDePagamento.A_PRAZO:
                        valor = crmForm.all.new_valor_total_kits.DataValue;
                        break;

                    /// Retira o valor da entrada     
                    case CondicoesDePagamento.ENTRADA_MAIS_PRAZO:
                        valor = (crmForm.all.new_valor_total_kits.DataValue - (crmForm.all.new_valor_entrada.DataValue / (1 - OpcoesDescontoPgtoVista.DESC_05)));
                        break;

                    /// Considera o valor final     
                    case CondicoesDePagamento.OUTRAS_CONDICOES:
                        valor = crmForm.all.new_visualizacao_valor_total_parcelamento_kit.DataValue;
                        break;
                }

                var cmd = new RemoteCommand("MrvService", "ValidarCreditoKit", "/MRVCustomizations/");
                cmd.SetParameter("oportunidadeId", crmForm.all.opportunityid.DataValue[0].id);
                cmd.SetParameter("numeroParcela", "1");
                cmd.SetParameter("valorParcela", valor.toLocaleString());
                var oResult = cmd.Execute();

                if (oResult.Success) {
                    if (!oResult.ReturnValue.Mrv.Aprovado) {
                        alert(oResult.ReturnValue.Mrv.Mensagem);
                        return false;
                    }
                }
                else {
                    alert("Ocorreu um erro na validação do limite de crédito.\n Por favor tente novamente em alguns instantes.\n" + erro.description);
                    return false;
                }
            }
        }
        return true;
    }
    
    function VerificarQuantidadeParcelasDivisaoSinal() {
        var quantidadeDeParcelas = 0;
        
        if (crmForm.all.new_valorsinalavista.DataValue != null)
            quantidadeDeParcelas = 1;

        if (crmForm.all.new_qtdedeparcelas.DataValue != null)
            quantidadeDeParcelas += crmForm.all.new_qtdedeparcelas.DataValue;

        if (crmForm.all.new_qtdedeparcelassinal1.DataValue != null)
            quantidadeDeParcelas += crmForm.all.new_qtdedeparcelassinal1.DataValue;

        if (crmForm.all.new_qtdedeparcelassinal2.DataValue != null)
            quantidadeDeParcelas += crmForm.all.new_qtdedeparcelassinal2.DataValue;

        if (crmForm.all.new_qtdedeparcelassinal3.DataValue != null)
            quantidadeDeParcelas += crmForm.all.new_qtdedeparcelassinal3.DataValue;
            
        if (crmForm.all.new_qtdedeparcelassinal4.DataValue != null)
            quantidadeDeParcelas += crmForm.all.new_qtdedeparcelassinal4.DataValue;
            
        if (planoFinanciamentoFlexAVista && quantidadeDeParcelas > 6)
        {
            alert('Plano Flex à Vista só permite parcelar divisão sinal em até 6 vezes.');
            event.returnValue = false;
            return false;
        }
        else if (!planoFinanciamentoFlexAVista  && quantidadeDeParcelas > 3)
        {
            alert('Não é possível parcelar divisão sinal mais de 3 vezes.');
            event.returnValue = false;
            return false;
        }
    }
    
    ValidarTaxaDespachante();
    var tipoDeProposta = crmForm.all.new_tipodecotacao.DataValue;
    var statusDaProposta = crmForm.all.new_statusdacotacao.DataValue;
    if (tipoDeProposta == TipoDeProposta.KITACABAMENTO) {
        if (!ValidarLimiteCreditoKit()) {
            event.returnValue = false;
            return false;
        }
    }    
   
    VerificarQuantidadeParcelasDivisaoSinal();
    crmForm.ValidarParcelaDesconto();

    if (event.Mode == EVENT_MODE_ONSAVE_QUOTE_CLOSEQUOTE) {
        if (document.getElementById('cqOppStatus').value == 3) {
            alert("Você não pode fechar a oportunidade como Ganha.");
            event.returnValue = false;
            return false;
        }

        document.getElementById("cqReviseQuote").value = "false";

        /// O Corretor não pode fechar a proposta - Equipe Vendas.
        if (crmForm.UsuarioPertenceEquipe("Corretor") &&
            document.getElementById('cqNewStatus').value != 5) {
            alert("Você não tem permissão para fechar a proposta como Revisado/Cancelada.\nAção cancelada!");
            event.returnValue = false;
            return false;
        }
		if (!ValidarAnaliseCredito()) {
			event.returnValue = false;
            return false;
		}
    }
    else if (event.Mode == EVENT_MODE_ONSAVE_QUOTE_ACTIVATE) {
        if ((tipoDeProposta != TipoDeProposta.KITACABAMENTO && tipoDeProposta != TipoDeProposta.PERMUTA) &&
            (crmForm.all.new_valortotalproposta.DataValue == null || crmForm.all.new_valortotalproposta.DataValue == 0)) {
            alert("A proposta deve conter um valor.");
            event.returnValue = false;
            return false;
        }
        if (tipoDeProposta == TipoDeProposta.UNIDADE &&
            crmForm.all.new_valortotalproposta.DataValue != null &&
            crmForm.all.new_valortotalproposta.DataValue < 18858) {
            alert("Valor da proposta não aceito pelo sistema.");
            event.returnValue = false;
            return false;
        }
        
        ///Validar as datas da proposta
        if (tipoDeProposta != TipoDeProposta.SERVICO &&
            tipoDeProposta != TipoDeProposta.PERMUTA) 
        {
           if(! ValidaPreenchimento() )
           {
              event.returnValue = false;
              return false;
           }
        }

        var funcao_requerida = tipoDeProposta == TipoDeProposta.SERVICO
            ? "Contrato-Gerência"
            : tipoDeProposta == TipoDeProposta.PERMUTA
                ? "Contrato Permuta"
                : crmForm.all.new_dentro_tabela_vendas.DataValue != null && crmForm.all.new_dentro_tabela_vendas.DataValue == 1
                    ? "Coordenação de Vendas - FT;Coordenação de Vendas - DT"
                    : "Coordenação de Vendas - FT";

        if (!crmForm.UsuarioPertenceEquipe(funcao_requerida) &&
            statusDaProposta != StatusDaProposta.EM_APROVACAO &&
            statusDaProposta != StatusDaProposta.CRIADA) {
            alert("Você não tem permissão para ativar proposta.\nAção cancelada!");
            event.returnValue = false;
            return false;
        }
        else {
            if (tipoDeProposta == TipoDeProposta.UNIDADE ||
                tipoDeProposta == TipoDeProposta.LOTE) {
                var dtAtual = GetServerDate();
                var dataInicio = new Date(crmForm.all.new_inicio_reserva.DataValue.getFullYear(), crmForm.all.new_inicio_reserva.DataValue.getMonth(), crmForm.all.new_inicio_reserva.DataValue.getDate());
                var dataFim = new Date(crmForm.all.new_fim_reserva.DataValue.getFullYear(), crmForm.all.new_fim_reserva.DataValue.getMonth(), crmForm.all.new_fim_reserva.DataValue.getDate());

                if (crmForm.all.new_inicio_reserva.DataValue != null &&
                    crmForm.all.new_fim_reserva.DataValue != null) {
                    if (statusDaProposta == StatusDaProposta.EM_APROVACAO ||
                        statusDaProposta == StatusDaProposta.CONTRA_PROPOSTA) {
                        if (!(dtAtual >= dataInicio && dtAtual <= dataFim)) {
                            alert("Proposta não poderá ser ativada porque está fora da sua vigência de reserva.\nOperação cancelada!");
                            crmForm.all.new_statusdacotacao.DataValue = crmForm.all.new_statusdacotacao.DefaultValue;
                            event.returnValue = false;
                            return false;
                        }
                    }
                }
                else {
                    alert("Não existe data de reserva para a proposta.\nOperação Cancelada!");
                    event.returnValue = false;
                }
            }
        }
    }
	
	// Força a persistência de campos readonly
	crmForm.all.new_datado1vencimento.ForceSubmit = true;
	crmForm.all.new_datadopagamento.ForceSubmit = true;
	crmForm.all.new_datadevencimento1parcelasinal1.ForceSubmit = true;
    crmForm.all.new_datadevencimento1parcelasinal2.ForceSubmit = true;
    crmForm.all.new_datadevencimento1parcelasinal3.ForceSubmit = true;
    crmForm.all.new_datadevencimento1parcelasinal4.ForceSubmit = true;
    crmForm.all.new_datado1pagamentomensal1.ForceSubmit = true;
    crmForm.all.new_datado1pagamentomensal2.ForceSubmit = true;
    crmForm.all.new_datado1pagamentomensal3.ForceSubmit = true;
    crmForm.all.new_datado1pagamentomensal4.ForceSubmit = true;
    crmForm.all.new_datado1pagamentomensal5.ForceSubmit = true;
    crmForm.all.new_datadepagamentointermediria1.ForceSubmit = true;
    crmForm.all.new_datadepagamentointermediria2.ForceSubmit = true;
    
    crmForm.DesabilitarCampos("new_statusdacotacao", false);
    
    if(!crmForm.ValidarTodosVencimentos()){
        event.returnValue = false;
        return false;
    }
    
    if(!crmForm.ValidarDataBase()){
        crmForm.all.new_data_base_contrato.DataValue = null;
        crmForm.all.new_data_base_contrato.SetFocus();
        event.returnValue = false;
        return false;
    }
	
	if (crmForm.IsDirty){
		var campos = "new_statusdacotacao;new_qtdedeparcelas;new_valordaparcela;new_periodicidadesinal;new_datadevencimento1parcela;new_tipodepagamentodivsinal;new_qtdedeparcelassinal1;new_qtdedeparcelassinal2;new_qtdedeparcelassinal3;new_qtdedeparcelassinal4;new_valordaparcelasinal1;new_valordasparcelassinal2;new_valordasparcelassinal3;new_valordasparcelassinal4;new_datadevencimento1parcelasinal1;new_datadevencimento1parcelasinal2;new_datadevencimento1parcelasinal3;new_datadevencimento1parcelasinal4;new_corretor_ganha;new_periodicidadesinal1;new_periodicidadesinal2;new_periodicidadesinal3;new_periodicidadesinal4;new_sinal;new_mensal;new_intermediaria;new_valorassessoria;new_valortotalproposta;new_valor_total_assessoria;new_valordaparcelakit;new_qtdedeparcelaskit;new_dentro_tabela_vendas;new_valortotalgeral;new_valortotalservicos;new_visualizacao_valor_total_parcelamento_kit;new_parcelamentotaxasexterno;paymenttermscode;new_valor_fgts_credito;new_valor_financiamento_credito;new_vlr_tx_vs;new_tipodeplanodefinanciamentocotid;new_divisaosinal;new_datadopagamento";
		crmForm.DesabilitarCampos(campos, false);
		crmForm.TransformaTodosCaracteresEmMaiusculo();
	}
	else return
}
catch (erro) {
    alert("Ocorreu um erro no formulário.\n" + erro.description);
    event.returnValue = false;
    return false;
}
