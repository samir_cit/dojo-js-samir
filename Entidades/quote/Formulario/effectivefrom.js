﻿/// Calculando a Data de Término da Vigência
if (crmForm.all.new_tipodecotacao.DataValue == TipoDeProposta.KITACABAMENTO &&
    crmForm.all.effectivefrom.DataValue != null) 
{
    var primeiradata = crmForm.all.effectivefrom.DataValue;
    primeiradata.setDate(primeiradata.getDate() + 7);
    crmForm.all.effectiveto.DataValue = primeiradata;
}
else
    crmForm.all.effectiveto.DataValue = null;