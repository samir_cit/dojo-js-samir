﻿//crmForm.ValidaData(crmForm.all.new_datado1vencimento, Grupo.KIT);
var dataUltimaParcela = new Date(crmForm.all.new_datado1vencimento.DataValue);
var quantidadeParcela = parseFloat(crmForm.all.new_qtdedeparcelaskit.DataValue);
var tempoCarencia = parseFloat(crmForm.all.new_tempodecarencia.DataValue);
var condicaoPagamento = crmForm.all.paymenttermscode.DataValue
var dataHabiteseConvetida = crmForm.RetornaData(dataHabitese);

if (quantidadeParcela > 0) {
    if (condicaoPagamento == CondicoesDePagamento.ENTRADA_MAIS_PRAZO) {
        dataUltimaParcela.setMonth(dataUltimaParcela.getMonth() + (quantidadeParcela));
    }
    else {
        dataUltimaParcela.setMonth(dataUltimaParcela.getMonth() + (quantidadeParcela - 1));
    }
}
if (dataUltimaParcela > dataHabiteseConvetida) {
    if (condicaoPagamento == CondicoesDePagamento.A_PRAZO || (condicaoPagamento == CondicoesDePagamento.ENTRADA_MAIS_PRAZO && quantidadeParcela != null)) {

        crmForm.all.new_qtdedeparcelaskit.DataValue = null;
        crmForm.all.new_qtdedeparcelaskit.SetFocus();
        data1parcela = new Date(crmForm.all.new_datado1vencimento.DataValue);

        /// Calcula o numero de meses ate a entrega
        var diff_date = dataHabiteseConvetida - data1parcela;
        var num_months = (diff_date / 31536000000) * 12;
        var meses = Math.floor(num_months);
        if (condicaoPagamento == CondicoesDePagamento.A_PRAZO) {
            meses = meses + 1;
        }
        alert("Quantidade máxima de parcelas permitidas são " + meses.toString() + " vezes.");
    }
    else {
        crmForm.all.new_datado1vencimento.DataValue = null;
        crmForm.all.new_datado1vencimento.SetFocus();
        alert("Data do 1º vencimento não permitida");
    }


}