﻿if (crmForm.all.new_tipodecotacao.DataValue == TipoDeProposta.SERVICO || crmForm.all.new_tipodecotacao.DataValue == TipoDeProposta.UNIDADE) 
{
    var vlrSegObraProp = 0;

        if (crmForm.all.new_qtde_parc_seg_1.DataValue != null && crmForm.all.new_vlr_parc_seg_1.DataValue != null)
            vlrSegObraProp += crmForm.all.new_qtde_parc_seg_1.DataValue * crmForm.all.new_vlr_parc_seg_1.DataValue;
        
        if (crmForm.all.new_qtde_parc_seg_2.DataValue != null && crmForm.all.new_vlr_parc_seg_2.DataValue != null)
            vlrSegObraProp += crmForm.all.new_qtde_parc_seg_2.DataValue * crmForm.all.new_vlr_parc_seg_2.DataValue;
    
    /// Calcula o valor total Seguro Obra
    crmForm.all.new_vl_tot_mes_seg.DataValue = vlrSegObraProp;
    
    /// Calcula Valor Total da Proposta
    new_valortotalproposta_onchange0();
}