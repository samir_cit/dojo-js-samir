﻿formularioValido = true;
dataHabitese = null;
var DataAtualServidor = new Date();
var DataServidor = new Date();
var DataServidorMensal = new Date();
var informacoesPlano = null;
var DIA_DATA_BASE = 5;
var MENSAGEM_FORMA_PAGAMENTO_JUSTIFICATIVA_NULO = "Os campos Forma de Pagamento ou Justificativa de Opção de Serviços de Registro de Contrato devem ser preenchidos";
var MENSAGEM_FORMA_PAGAMENTO_JUSTIFICATIVA_NAO_NULO = "Os campo Forma de Pagamento e Justificativa de Opção de Serviços de Registro de Contrato estão preenchidos.\n Somente um dos dois campos deve ser preenchido.";
var MENSAGEM_VALOR_TAXA_TIPO_PAGAMENTO_PARCELAMENTO_TAXA_NULO = "Os campos Valor da taxa, Tipo de Pagamento e Parcelamento Taxas devem ser preenchidos.";
var MENSAGEM_VALOR_TAXA_TIPO_PAGAMENTO_PARCELAMENTO_TAXA_NAO_NULO = "Os campos Valor da taxa, Tipo de Pagamento e Parcelamento Taxas não devem ser preenchidos.";
var PRODUTO_OPCAO_REGISTRO_CONTRATO = "OPÇÃO DE REGISTRO DO CONTRATO";
var MENSAGEM_PERIODICIDADE_PARCELAUNICA = "Para periodicidade Parcela Única, a quantidade de parcelas deve ser igual a 1.";
var MENSAGEM_PLANO_ASSOCIATIVO_BANCARIO = "Essa ação para Propostas de Oportunidades com Planos de Financiamento classificados como Associativo ou Bancário, só pode ser realizada pelo sistema Validador de Proposta.";
var MENSAGEM_DATAS_RESERVA_NAO_CARREGADAS = "Datas da reserva ainda não foram carregadas. \nSalve antes de solicitar aprovação.";
var MENSAGEN_SERVICO_VENDIDO_SEM_VALOR = "Você deve informar valor para o serviço a ser vendido!";
var MENSAGEM_PROPOSTA_PERMUTA_SEM_VALOR = "A proposta de permuta deve conter um valor.";
var MENSAGEM_PROPOSTA_NAO_SALVA = "A proposta foi alterada. \nSalve antes de solicitar aprovação.";
try {

    var guid = (function () {
        function s4() {
            return Math.floor((1 + Math.random()) * 0x10000)
               .toString(16)
               .substring(1);
        }
        return function () {
            return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
				   s4() + '-' + s4() + s4() + s4();
        };
    })();

    function loadScript() {
        var oScript = document.createElement('script');
        oScript.type = "text/javascript";
        oScript.src = "/_static/Base.js?guid=" + guid();
        var oHead = document.getElementsByTagName('head')[0];
        oHead.appendChild(oScript);

        oScript.onreadystatechange = function () {
            if (this.readyState == 'complete' || this.readyState == 'loaded')
                Load();
        }
    }

    loadScript();

    function Load() {

        if (crmForm.all.paymenttermscode.DataValue == 2 || crmForm.all.new_tempodecarencia.DataValue != null) {
            crmForm.all.new_datado1vencimento.Disabled = true;
        }

        TabelaReajusteTaxaCarencia =
        {
            TAXA_1_MES_CARENCIA: 1.01180,
            TAXA_2_MESES_CARENCIA: 1.02374,
            TAXA_3_MESES_CARENCIA: 1.03582,
            TAXA_4_MESES_CARENCIA: 1.04804,
            TAXA_5_MESES_CARENCIA: 1.06041,
            TAXA_6_MESES_CARENCIA: 1.07292
        }

        TipoDeProposta =
        {
            UNIDADE: "1",
            GARAGEM: "2",
            BOX: "3",
            KITACABAMENTO: "4",
            SERVICO: "5",
            LOTE: "6",
            PERMUTA: "7",
            LOJA: "8"
        }

        StatusDaProposta =
        {
            EM_APROVACAO: "1",
            CONTRA_PROPOSTA: "2",
            REPROVADA: "3",
            CRIADA: "4",
            APROVADA: "5",
            CANCELADA_FINAL_DE_VIGENCIA: "6",
            KIT_APROVADA: "7",
            CANCELADA_OUTRO_CONTRATO_ATIVO: "8",
            CANCELADA_PELO_USUARIO: "9",
            PERMUTA_APROVADA: "10"
        }

        StatusCode =
        {
            RASCUNHO_EM_ANDAMENTO: "1",
            ATIVA_EM_ANDAMENTO: "2",
            ATIVA_ABERTA: "3",
            GANHA: "4",
            FECHADO_PERDIDA: "5",
            FECHADO_CANCELADA: "6",
            FECHADO_REVISADO: "7"
        }

        PropostaDentroDaTabela =
        {
            SIM: "1",
            NAO: "2"
        }

        CondicoesDePagamento =
        {
            A_VISTA: "1",
            A_PRAZO: "2",
            ENTRADA_MAIS_PRAZO: "200000",
            OUTRAS_CONDICOES: "3"
        }

        Grupo =
        {
            CASA_GARANTIA: 1, //"Casa Garantia",
            DESCONTO_PROMOCIONAL: 2, //"Desconto Promocional",
            DIVISAO_SINAL: 3, //"Divisão Sinal",
            FGTS: 4, //"FGTS",
            FINANCIAMENTO: 7, //"Financiamento",
            INTERMEDIARIA: 5, //"Intermediaria",
            MENSAL: 6, //"Mensal",
            RECURSO_PROPRIO: 8, //"Recurso Próprio",
            SINAL: 9, //"Sinal",
            TRANSFERENCIA: 10, //"Transferência",
            SERVICO: 11, //"Serviço",
            KIT: 12  //"Kit"
        }

        PeriodicidadeSinal =
        {
            ANUAL: "1",
            MENSAL: "2",
            SEMESTRAL: "3",
            TRIMESTRAL: "4",
            QUINZENAL: "5"
        }

        PeriodicidadeIntermediaria =
        {
            ANUAL: "1",
            MENSAL: "2",
            SEMESTRAL: "3",
            TRIMESTRAL: "4",
            QUINZENAL: "5",
            PARCELAUNICA: "6"
        }

        OpcoesPagamentoAVista =
        {
            FAIXA_FIM_A: 2000.00,
            FAIXA_FIM_B: 4000.00
        }
        OpcoesDescontoPgtoVista =
        {
            DESC_05: 0.05,
            DESC_08: 0.08,
            DESC_10: 0.10
        }
        OpcoesQuantidadeParcelas =
        {
            LIM_INFERIOR: 0,
            LIM_INTERMEDIARIO: 6,
            LIM_SUPERIOR: 36
        }

        OpcoesTipoPagamento =
		{
		    DESCONTO_PROMOCIONAL: 1,
		    ESPÉCIE: 2,
		    CHEQUE: 3,
		    NOTA_PROMISSÓRIA: 4,
		    BOLETO: 5,
            CC: 1
		}

        planoFinanciamentoFlexAVista = false;

        Secao =
        {
            SINAL_A_VISTA: 1,
            DIVISAO_SINAL_D1: 2,
            DIVISAO_SINAL_D2: 3,
            DIVISAO_SINAL_D3: 4,
            DIVISAO_SINAL_D4: 5,
            DIVISAO_SINAL_D5: 6,
            MENSAL_M1: 7,
            MENSAL_M2: 8,
            MENSAL_M3: 9,
            MENSAL_M4: 10,
            MENSAL_M5: 11,
            MENSAL_M6: 12,
            DIVISAO_INTERMEDIARIA_I1: 13,
            DIVISAO_INTERMEDIARIA_I2: 14,
            DIVISAO_INTERMEDIARIA_I3: 15
        }

        TipoCampoParcela =
        {
            QUANTIDADE: 1,
            DATA_VENCIMENTO: 2,
            PERIODICIDADE: 3
        }

        Mensagem =
        {
            DIVISAO_SINAL_D1_QUANTIDADE_INVALIDA: "A quantidade de parcelas da Primeira parcela de Divisão do Sinal deve ser igual a 1."
        }

        DiaReferencia = {
            DIA_FECHAMENTO_VENCIMENTO_A_VISTA: 0,
            DATA_BASE_KIT: 10
        }

        TipoAuditoria =
        {
            PROPOSTA: 1,
            OPORTUNIDADE_RENEGOCIACAO: 2,
            PROPOSTA_CSG: 3
        }

        TipoGrupoFinanciamento =
        {
            Nenhum: 0,
            Bancario: 1,
            Flex: 2,
            FlexCorrigido: 3,
            FlexComFGTS: 4,
            FlexCorrigidoComFGTS: 5
        }

        var oScript = document.createElement("script");
        oScript.src = "/MRVWeb/_scriptForm/formscript.js";
        document.appendChild(oScript);

        oScript.onreadystatechange = function () {
            if (this.readyState == 'complete' || this.readyState == 'loaded') {
                VerificarTipoDeProposta(crmForm.all.new_tipodecotacao.DataValue);
            }
        }

        var oScript = document.createElement("script");
        oScript.src = "/_static/_grid/cmds/util.js";
        document.appendChild(oScript);

        crmForm.RetornaData = function (data) {
            var tmp = data.split('-');
            var dataPrev = new Date(tmp[0], tmp[1] - 1, tmp[2], 3, 0, 0);
            return dataPrev;
        }

        crmForm.VerificarPlanoAssociativoBancario = function () {
            var cmd = new RemoteCommand("MrvService", "VerificarPlanoAssociativoBancario", "/MrvCustomizations/");
            cmd.SetParameter("oportunidadeId", crmForm.all.opportunityid.DataValue[0].id);

            var resultado = cmd.Execute();

            if (resultado.Success) {
                if (resultado.ReturnValue.Mrv.Error)
                    alert(resultado.ReturnValue.Mrv.Error);
                else if (resultado.ReturnValue.Mrv.ErrorSoap)
                    alert(resultado.ReturnValue.Mrv.ErrorSoap)
                else {
                    return resultado.ReturnValue.Mrv.PlanoAssociativoBancario;
                }
            }
            return false;
        }

        var planoFinanciamentoBancarioAssociativo = crmForm.VerificarPlanoAssociativoBancario();


        function OcultarSecoesGuiaDespesaGerais() {
            /// Campos Campos da seção Endereço para cobrança
            crmForm.EscondeCampo("billto_name", true);
            crmForm.EscondeCampo("billto_stateorprovince", true);
            crmForm.EscondeCampo("billto_line1", true);
            crmForm.EscondeCampo("billto_postalcode", true);
            crmForm.EscondeCampo("billto_line2", true);
            crmForm.EscondeCampo("billto_country", true);
            crmForm.EscondeCampo("billto_line3", true);
            crmForm.EscondeCampo("billto_telephone", true);
            crmForm.EscondeCampo("billto_city", true);
            crmForm.EscondeCampo("billto_fax", true);
            crmForm.EscondeCampo("billto_contactname", true);

            /// Seção Endereço para cobrança
            crmForm.all.billto_contactname_d.parentNode.parentNode.style.display = "none";

            /// Campos da seção Endereço para entrega
            crmForm.EscondeCampo("willcall", true);
            crmForm.EscondeCampo("shipto_name", true);
            crmForm.EscondeCampo("shipto_stateorprovince", true);
            crmForm.EscondeCampo("shipto_line1", true);
            crmForm.EscondeCampo("shipto_postalcode", true);
            crmForm.EscondeCampo("shipto_line2", true);
            crmForm.EscondeCampo("shipto_country", true);
            crmForm.EscondeCampo("shipto_line3", true);
            crmForm.EscondeCampo("shipto_city", true);
            crmForm.EscondeCampo("shipto_telephone", true);
            crmForm.EscondeCampo("shipto_fax", true);
            crmForm.EscondeCampo("shipto_contactname", true);

            /// Seção Endereço para entrega
            crmForm.all.shipto_contactname_d.parentNode.parentNode.style.display = "none";
        }

        function OcultarSecaoFormaDePagamentoGuiaDetalheDaPropostaKitBoxGaragemServico() {
            crmForm.EscondeCampo("new_valor_total_kits", true);
            crmForm.EscondeCampo("paymenttermscode", true);
            crmForm.EscondeCampo("new_qtdedeparcelaskit", true);
            crmForm.EscondeCampo("new_valor_entrada", true);
            crmForm.EscondeCampo("new_datado1vencimento", true);
            crmForm.EscondeCampo("new_valordaparcelakit", true);
            crmForm.EscondeCampo("new_visualizacao_valor_total_parcelamento_kit", true);

            /// Seções da Guia Detalhe 
            crmForm.all.new_valor_total_kits_c.parentNode.parentNode.style.display = "none";
        }

        function ConfigurarSecaoMensalDaGuiaDetalheDaPropostaKitBoxGaragemServico(acao) {
            crmForm.EscondeCampo("new_vlr_parc_mens_1", acao);
            crmForm.EscondeCampo("new_qtde_parc_s1", acao);
            crmForm.EscondeCampo("new_tipo_pagto_mensal_1", acao);
            crmForm.EscondeCampo("new_vlr_parc_mens_2", acao);
            crmForm.EscondeCampo("new_qtde_parc_s2", acao);
            crmForm.EscondeCampo("new_dt_vencto_parc_2", acao);
            crmForm.EscondeCampo("new_tipo_pagto_mensal_2", acao);
            crmForm.EscondeCampo("new_vlr_parc_mens_3", acao);
            crmForm.EscondeCampo("new_qtde_parc_s3", acao);
            crmForm.EscondeCampo("new_dt_vencto_parc_3", acao);
            crmForm.EscondeCampo("new_new_vlr_tot_mens_money", acao);

            /// Seções da Guia Detalhe Da Proposta/Kit/Box/Garagem/Servico
            crmForm.all.new_vlr_parc_mens_1_c.parentNode.parentNode.style.display = acao ? "none" : "block";
        }

        function ConfigurarTaxaDespachanteVS(acao) {
            crmForm.EscondeCampo("new_forma_pagamento_tx_desp_vs", acao);
            crmForm.EscondeCampo("new_vlr_tx_vs", acao);
            crmForm.EscondeCampo("new_parcelamentotaxasexterno", acao);

            /// Seção Taxa Despachante da Guia Detalhe Da Proposta/Kit/Box/Garagem/Servico
            crmForm.all.new_parcelamentotaxasexterno_d.parentNode.parentNode.style.display = acao ? "none" : "block";
        }

        crmForm.ValidaData = function (campo, grupo) {
            if (crmForm.VerificaAno2020(campo)) {
                if (eval(campo).DataValue != null) {
                    var Data = eval(campo).DataValue;

                    if (Data < DataServidor) {
                        eval(campo).DataValue = null;
                        alert("Data inferior a permitida.\nSelecione apenas datas a partir de " + crmForm.GetMes(DataServidor.getMonth()));
                    }
                    else
                        crmForm.BuscaDataDeVencimento(campo, grupo);

                }
            }
        }

        crmForm.BuscaDataDeVencimento = function (campo, grupo) {

            var diaEscolhido = eval(campo).DataValue.getDate();

            if (grupo != undefined && !crmForm.ValidaDataDeVencimento(grupo, diaEscolhido))
                eval(campo).DataValue = null;
        }

        crmForm.VerificaAno2020 = function (campo) {
            if (eval(campo).DataValue != null) {
                if (eval(campo).DataValue.getYear() > 2020) {
                    alert("Ano da data errado!!");
                    eval(campo).DataValue = null;
                }
            }
            return true;
        }

        crmForm.ValidaAtributoData = function (campo) {
            if (eval(campo).DataValue != null) {
                var Data = eval(campo).DataValue;
                var diaEscolhido = Data.getDate();
                var Next = new Date();
                Next.setHours(0);
                Next.setMinutes(0);
                Next.setSeconds(0);
                Next.setMilliseconds(0);

                if (Data < Next) {
                    eval(campo).DataValue = null;
                    alert("Data inferior a permitida");
                }
            }
        }

        crmForm.RemoveValoresPicklist = function (Grupo, naoRemoverValor) {

            var remover = "";

            for (index = 0; index < Grupo.Options.length; index++) {
                var valor = Grupo[index].value;

                if (naoRemoverValor.toString().indexOf(valor) < 0 && Grupo[index].text != "")
                    remover += index + ",";
            }

            for (x = 0; x < remover.split(",").length - 1; x++) {
                Grupo.DeleteOption(remover.split(",")[x]);
            }
        }

        function ConfigurarOnLoad() {
            /// Guia de Despesas Gerais(Guia de Endereços), pois não será utilizada no negócio
            crmForm.all.tab4Tab.style.display = "none";

            //Ocultar Seção Permuta
            crmForm.all.new_qtd_parcelas_permuta_c.parentNode.parentNode.style.display = "none";

            ConfigurarSecaoMensalDaGuiaDetalheDaPropostaKitBoxGaragemServico(true);

            ConfigurarTaxaDespachanteVS(true);

            var mnuCriarPedido = document.getElementById("_MICreateOrder");
            if (mnuCriarPedido)
                mnuCriarPedido.style.display = "none";

            var mnuAceitarProposta = document.getElementById("_MIAcceptQuote");
            if (mnuAceitarProposta)
                mnuAceitarProposta.style.display = "none";

            var mnuAtivarProposta = document.getElementById("_MIactivateQuote");
            if (mnuAtivarProposta)
                mnuAtivarProposta.style.display = "none";

            var mnuRevisar = document.getElementById("_MIreviseClosedQuote");
            if (mnuRevisar)
                mnuRevisar.style.display = "none";

            var botaoGanharProposta = document.getElementById("_MBAcceptQuote");
            if (botaoGanharProposta) {
                var descricaoGanharProposta = "Ganhar Proposta";
                // Atribui a ação do botão                
                botaoGanharProposta.action = "crmForm.ValidarGanharProposta()";
                // Alterara as decrições de ação do botão
                botaoGanharProposta.title = descricaoGanharProposta;
                if (botaoGanharProposta.children[0].children[0].children[0]) {
                    // Altera o atributo alt da imagem relacionada ao botão
                    botaoGanharProposta.children[0].children[0].children[0].getAttributeNode("alt").value = descricaoGanharProposta;
                }
                if (botaoGanharProposta.children[0].children[0].children[1]) {
                    // Altera o texto do span relacioando ao botão
                    botaoGanharProposta.children[0].children[0].children[1].innerHTML = descricaoGanharProposta;
                }
            }

            var botaoRevisarProposta = document.getElementById("_MIreviseActiveQuote");

            if (botaoRevisarProposta) {
                var descricaoRevisarProposta = "Revisar";

                botaoRevisarProposta.action = "crmForm.ValidarRevisarProposta()";
                botaoRevisarProposta.title = descricaoRevisarProposta;

            }

            if (crmForm.all.statuscode.DataValue == StatusCode.GANHA) {
                if (document.getElementById("_MBCreateOrder")) {
                    var parent = document.getElementById("_MBCreateOrder").parentElement;
                    parent.removeChild(document.getElementById("_MBCreateOrder"));
                }

                crmForm.RemoveBotao("AtivarProposta");
                crmForm.RemoveBotao("AtivaProposta");
            }

            if (crmForm.FormType == TypeDisabled) {
                crmForm.RemoveBotao("AtivarProposta");
                crmForm.RemoveBotao("AtivaProposta");
            }
            else if (crmForm.all.new_tipodecotacao.DataValue == TipoDeProposta.KITACABAMENTO ||
                crmForm.all.new_tipodecotacao.DataValue == TipoDeProposta.PERMUTA) {
                crmForm.RemoveBotao("AtivarProposta");
            }
            else {
                var funcao = crmForm.all.new_dentro_tabela_vendas.DataValue != null && crmForm.all.new_dentro_tabela_vendas.DataValue == PropostaDentroDaTabela.SIM
                    ? "Coordenação de Vendas - FT;Coordenação de Vendas - DT"
                    : "Coordenação de Vendas - FT";

                var rCmd = new RemoteCommand("MrvService", "UserContainsTeams", "/MRVCustomizations/");
                rCmd.SetParameter("teams", funcao);
                var retorno = rCmd.Execute();

                if (retorno.Success) {
                    if (retorno.ReturnValue.Mrv.Found != true)
                        crmForm.RemoveBotao("AtivarProposta");
                    else
                        crmForm.RemoveBotao("AtivaProposta");
                }
            }

            /// Seção "Informações de Remessa
            document.getElementById("shippingmethodcode_d").parentNode.parentNode.style.display = "none";

            crmForm.EscondeCampo("freightamount", true);
            //crmForm.EscondeCampo("discountamount", true);

            if (crmForm.FormType != TypeRead && crmForm.FormType != TypeDisabled) //Tratamento para o bug da revisão da proposta
            {
                crmForm.all.new_valortotalproposta.Disabled = true;
                if (!planoFinanciamentoBancarioAssociativo) {
                    new_valortotalproposta_onchange0();
                }

                if (crmForm.all.new_tbv_vtotalsinal.DataValue == null)
                    crmForm.all.new_tbv_vtotalsinal.DataValue = 0;

                if (crmForm.all.new_tbv_vtotalmensais.DataValue == null)
                    crmForm.all.new_tbv_vtotalmensais.DataValue = 0;

                if (crmForm.all.new_tbv_vtotalintermediarias.DataValue == null)
                    crmForm.all.new_tbv_vtotalintermediarias.DataValue = 0;

                if (crmForm.all.new_tbv_totalfinanciamento.DataValue == null)
                    crmForm.all.new_tbv_totalfinanciamento.DataValue = 0;

                if (crmForm.FormType == TypeCreate || crmForm.FormType == TypeUpdate || crmForm.FormType == TypeQuickCreate) {
                    ObterDataServidor();
                }

                crmForm.all.new_parcelamentotaxasinterno.ForceSubmit = true;
                crmForm.all.new_vlr_tx.ForceSubmit = true;

                if (crmForm.all.new_forma_pagamento_tx_desp.DataValue != null) {
                    crmForm.all.new_vlr_tx.Disabled = true;
                    crmForm.all.new_justificativa_taxa_despachante.Disabled = true;
                }
                                                                                                                                crmForm.all.new_tipo_pagamento_sinal 
                var camposQueDeveraoSerBloqueados = [
                "new_valorassessoria",
                "new_sinal",
                "new_mensal",
                "new_intermediaria",
                "name", "customerid", "new_tipodecotacao", "new_tipodeplanodefinanciamentocotid"];

                crmForm.DesabilitarCampos(camposQueDeveraoSerBloqueados.join(";"), true);

                crmForm.EscondeCampo("new_regra_nova_periodicidade", true);

                if (crmForm.FormType == TypeCreate)
                    crmForm.all.new_regra_nova_periodicidade.DataValue = true;

                if (crmForm.all.new_regra_nova_periodicidade.DataValue != null) {
                    crmForm.RemoveValoresPicklist(crmForm.all.new_periodicidadesinal, PeriodicidadeSinal.MENSAL);
                    crmForm.RemoveValoresPicklist(crmForm.all.new_periodicidadesinal1, PeriodicidadeSinal.MENSAL);
                    crmForm.RemoveValoresPicklist(crmForm.all.new_periodicidadesinal2, PeriodicidadeSinal.MENSAL);
                    crmForm.RemoveValoresPicklist(crmForm.all.new_periodicidadesinal3, PeriodicidadeSinal.MENSAL);
                    crmForm.RemoveValoresPicklist(crmForm.all.new_periodicidadesinal4, PeriodicidadeSinal.MENSAL);
                }

                if (crmForm.all.new_tipo_pagamento_sinal != null && crmForm.all.new_tipo_pagamento_sinal.DataValue != null && crmForm.all.new_tipo_pagamento_sinal.DataValue == OpcoesTipoPagamento.BOLETO) {
                    crmForm.all.new_datadopagamento.Disabled = true;
                }
                else {
                    crmForm.all.new_datadopagamento.Disabled = false;
                }

                crmForm.DesabilitarSalvar();
            }
        }

        crmForm.ValidarPreenchimentoTaxaDespachante = function () {
            if (crmForm.all.new_tipodecotacao.DataValue == TipoDeProposta.UNIDADE) {
                if (informacoesPlano == null)
                    informacoesPlano = crmForm.ObterInformacoesPlanoFinanciamento(crmForm.all.opportunityid.DataValue[0].id);
                if (informacoesPlano != null) {
                    var nomePlano = informacoesPlano.ReturnValue.Mrv.Nome.toUpperCase();

                    var obrigatoriedadeTaxaDespachante =
						{
						    taxaObrigatoriaPorCidade: informacoesPlano.ReturnValue.Mrv.TaxaObrigatoriaPorCidade,
						    taxaObrigatoriaPorPlano: informacoesPlano.ReturnValue.Mrv.TaxaObrigatoriaPorPlano
						}

                    if (obrigatoriedadeTaxaDespachante.taxaObrigatoriaPorCidade && obrigatoriedadeTaxaDespachante.taxaObrigatoriaPorPlano) {
                        var formaPagamento = crmForm.all.new_forma_pagamento_tx_desp.DataValue;
                        var justificativaTaxaDespachante = crmForm.all.new_justificativa_taxa_despachante.DataValue;
                        var valorTaxa = crmForm.all.new_vlr_tx.DataValue;
                        var tipoPagamento = crmForm.all.new_forma_pgto_taxa_interna.DataValue;
                        var parcelamentoTaxa = crmForm.all.new_parcelamentotaxasinterno.DataValue;
                        if (formaPagamento == null && justificativaTaxaDespachante == null) {
                            alert(MENSAGEM_FORMA_PAGAMENTO_JUSTIFICATIVA_NULO);
                            return false;
                        }
                        else if (formaPagamento != null && justificativaTaxaDespachante != null) {
                            alert(MENSAGEM_FORMA_PAGAMENTO_JUSTIFICATIVA_NAO_NULO);
                            return false;
                        }
                        else {
                            if (formaPagamento != null) {
                                if (valorTaxa == null || tipoPagamento == null || parcelamentoTaxa == null) {
                                    alert(MENSAGEM_VALOR_TAXA_TIPO_PAGAMENTO_PARCELAMENTO_TAXA_NULO);
                                    return false;
                                }
                            }
                            else {
                                if (valorTaxa != null || tipoPagamento != null || parcelamentoTaxa != null) {
                                    alert(MENSAGEM_VALOR_TAXA_TIPO_PAGAMENTO_PARCELAMENTO_TAXA_NAO_NULO);
                                    return false;
                                }
                            }
                        }
                    }
                }
            }
            return true;
        }

        crmForm.DesabilitarSalvar = function () {
            if (planoFinanciamentoBancarioAssociativo) {
                document.all._MIcrmFormSave.style.display = 'none';
                document.all._MIcrmFormSaveAndClose.style.display = 'none';
                document.all._MIcrmFormSubmitCrmForm59truetruefalse.style.display = 'none';
                document.all._MBcrmFormSave.style.display = 'none';
                document.all._MBcrmFormSaveAndClose.style.display = 'none';
                document.all._MBcrmFormSubmitCrmForm59truetruefalse.style.display = 'none';
            }
        }

        ConfigurarOnLoad();

        function GetValues(Entidade, CampoChave, Identificador, Atributos) {
            var rCmd = new RemoteCommand("MrvService", "GetEntity", "/MRVCustomizations/");
            rCmd.SetParameter("entity", Entidade);
            rCmd.SetParameter("primaryField", CampoChave);
            rCmd.SetParameter("entityId", Identificador);
            rCmd.SetParameter("fieldList", Atributos);

            var retorno = rCmd.Execute();

            if (retorno.Success)
                return retorno;
            else
                return null;
        }

        function DesativarCamposTabela() {
            var campos = "new_tbv_ato;new_tbv_qtdeparcsinal;new_tbv_valorparcsinal;new_tbv_qtdeparcmensal1;new_tbv_qtdeparcmensal2;new_tbv_valorparcmensal1;new_tbv_valorparcmensal2;new_tbv_qtdeparcintermed;new_tbv_valorparcintermed;new_tbv_totalfinanciamento;new_tbv_vencimentointermed;new_tbv_periodicidadeintermed";
            crmForm.DesabilitarCampos(campos, true);
        }

        /// Responsável por verificar se o tipo de serviço é taxa despachante.
        crmForm.verificarTipoServicoTaxaDespachante = function () {
            var rCmd = new RemoteCommand("MrvService", "ObterProdutoServico", "/MRVCustomizations/");
            rCmd.SetParameter("OpportunityId", crmForm.all.opportunityid.DataValue[0].id);
            var retorno = rCmd.Execute();

            if (crmForm.TratarRetornoRemoteCommand(retorno, "ObterProdutoServico"))
                return retorno.ReturnValue.Mrv.ProdutoServico == PRODUTO_OPCAO_REGISTRO_CONTRATO;
            else
                return false;
        }

        /// Usuário Adm
        Administrador = null;
        crmForm.UsuarioAdministrador = function () {
            if (Administrador == null)
                Administrador = crmForm.UsuarioPertenceEquipe("Administradores");

            return Administrador;
        }

        if (crmForm.UsuarioAdministrador()) {
            //Somente executar se o formulário não estiver ready-only ou disabled
            if (crmForm.FormType != TypeRead && crmForm.FormType != TypeDisabled) {
                crmForm.all.new_inicio_reserva.Disabled = false;
                crmForm.all.new_fim_ativacao.Disabled = false;
                crmForm.all.new_fim_negociacao.Disabled = false;
                crmForm.all.new_fim_reserva.Disabled = false;
            }
        }

        function DesabilitarCamposMensal() {
            //Somente executar se o formulário não estiver ready-only ou disabled
            if (crmForm.FormType == TypeRead || crmForm.FormType == TypeDisabled) return;

            var camposQueDeveraoSerBloqueados = [
                "new_mensal",
                "new_qtdedeparcelasmensais",
                "new_qtdedeparcelasmensais1",
                "new_qtdedeparcelasmensais2",
                "new_qtdedeparcelasmensais3",
                "new_qtdedeparcelasmensais4",
                "new_qtdedeparcelasmensais5",
                "new_valordaparcelamensal",
                "new_valordaparcelamensal1",
                "new_valordaparcelamensal2",
                "new_valordaparcelamensal3",
                "new_valordaparcelamensal4",
                "new_valordaparcelamensal5",
                "new_datadepagamentomensal",
                "new_datado1pagamentomensal1",
                "new_datado1pagamentomensal2",
                "new_datado1pagamentomensal3",
                "new_datado1pagamentomensal4",
                "new_datado1pagamentomensal5"];

            crmForm.DesabilitarCampos(camposQueDeveraoSerBloqueados.join(";"), true);
        }

        function DesabilitarCamposIntermediaria() {
            //Somente executar se o formulário não estiver ready-only ou disabled
            if (crmForm.FormType == TypeRead || crmForm.FormType == TypeDisabled) return;

            var camposQueDeveraoSerBloqueados = [
                "new_intermediaria",
                "new_qtdedeintermediarias",
                "new_qtdedeparcelasintermediarias1",
                "new_qtdedeparcelasintermediarias2",
                "new_valosdaparcelaintermediaria",
                "new_valordaparcelaintermediaria1",
                "new_valordaparcelaintermediaria2",
                "new_datadepagamentointermediaria",
                "new_datadepagamentointermediria1",
                "new_datadepagamentointermediria2",
                "new_periododepagamentointermediaria",
                "new_periododepagamentointermediaria1",
                "new_periododepagamentointermediaria2"];

            crmForm.DesabilitarCampos(camposQueDeveraoSerBloqueados.join(";"), true);
        }

        function DesabilitarCamposFinanciamento() {
            //Somente executar se o formulário não estiver ready-only ou disabled
            if (crmForm.FormType == TypeRead || crmForm.FormType == TypeDisabled) return;

            var camposQueDeveraoSerBloqueados = [
                "new_financiamento",
                "new_valor_financiamento_credito"];

            crmForm.DesabilitarCampos(camposQueDeveraoSerBloqueados.join(";"), true);
        }

        function DesabilitarCamposFGTS() {
            //Somente executar se o formulário não estiver ready-only ou disabled
            if (crmForm.FormType == TypeRead || crmForm.FormType == TypeDisabled) return;

            var camposQueDeveraoSerBloqueados = [
                "new_valordofgts",
                "new_valor_fgts_credito"];

            crmForm.DesabilitarCampos(camposQueDeveraoSerBloqueados.join(";"), true);
        }

        function DesabilitarCamposTaxaDespachante() {
            //Somente executar se o formulário não estiver ready-only ou disabled
            if (crmForm.FormType == TypeRead || crmForm.FormType == TypeDisabled) return;

            var camposQueDeveraoSerBloqueados = [
                "new_forma_pagamento_tx_desp",
                "new_forma_pgto_taxa_interna",
                "new_justificativa_taxa_despachante"];

            crmForm.DesabilitarCampos(camposQueDeveraoSerBloqueados.join(";"), true);
        }

        crmForm.ObterInformacoesPlanoFinanciamento = function (oportunidadeId) {
            var rCmd = new RemoteCommand("MrvService", "ObterInformacoesPlanoFinanciamento", "/MRVCustomizations/");
            rCmd.SetParameter("oportunidadeId", oportunidadeId);
            var retorno = rCmd.Execute();

            if (crmForm.TratarRetornoRemoteCommand(retorno, "ObterInformacoesPlanoFinanciamento"))
                return retorno;
            else
                return null;
        }

        if (crmForm.all.new_tipodecotacao.DataValue != null &&
            IsProductSale(crmForm.all.new_tipodecotacao.DataValue)) {
            /// Mostra a aba de 'Despesas Gerais' para possibilitar a venda casada.
            crmForm.all.tab4Tab.style.display = "block";

            //Executar abaixo somente se o formulário não estiver ready-only ou inativo
            if (crmForm.FormType != TypeRead && crmForm.FormType != TypeDisabled) {
                OcultarSecoesGuiaDespesaGerais();
                if (informacoesPlano == null)
                    informacoesPlano = crmForm.ObterInformacoesPlanoFinanciamento(crmForm.all.opportunityid.DataValue[0].id);

                if (informacoesPlano != null) {
                    var nome = informacoesPlano.ReturnValue.Mrv.Nome.toUpperCase();
                    //Atualiza plano se tipo plano da proposta diferente da oportunidade
                    if (crmForm.all.new_tipodeplanodefinanciamentocotid.DataValue == null
                        || crmForm.all.new_tipodeplanodefinanciamentocotid.DataValue[0].name != informacoesPlano.ReturnValue.Mrv.NomeTipoPlano) {
                        crmForm.all.new_tipodeplanodefinanciamentocotid.Disabled = false;
                        crmForm.all.new_tipodeplanodefinanciamentocotid.DataValue = null;
                        crmForm.AddValueLookup(crmForm.all.new_tipodeplanodefinanciamentocotid, '{' + informacoesPlano.ReturnValue.Mrv.IdTipoPlano + '}', '10030', informacoesPlano.ReturnValue.Mrv.NomeTipoPlano);
                        crmForm.all.new_tipodeplanodefinanciamentocotid.FireOnChange();
                        crmForm.all.new_tipodeplanodefinanciamentocotid.Disabled = true;
                    }

                    var obrigatoriedadeTaxaDespachante =
						{
						    taxaObrigatoriaPorCidade: informacoesPlano.ReturnValue.Mrv.TaxaObrigatoriaPorCidade,
						    taxaObrigatoriaPorPlano: informacoesPlano.ReturnValue.Mrv.TaxaObrigatoriaPorPlano
						}

                    ValidarObrigatoriedadeTaxaDespachante(nome, obrigatoriedadeTaxaDespachante);

                    /* Planos Referentes a Financiamento Obrigatório */
                    if (informacoesPlano.ReturnValue.Mrv.GrupoFinanciamento == TipoGrupoFinanciamento.Bancario) {
                        crmForm.SetFieldReqLevel("new_financiamento", true);
                    }

                    if (informacoesPlano.ReturnValue.Mrv.GrupoFinanciamento == TipoGrupoFinanciamento.Flex) {
                        planoFinanciamentoFlexAVista = true;
                        DesabilitarCamposMensal();
                        DesabilitarCamposIntermediaria();
                        DesabilitarCamposFinanciamento();
                        DesabilitarCamposFGTS();
                    }
                    else if (informacoesPlano.ReturnValue.Mrv.GrupoFinanciamento == TipoGrupoFinanciamento.FlexCorrigido) {
                        DesabilitarCamposFinanciamento();
                        DesabilitarCamposFGTS();
                    }
                    else if (informacoesPlano.ReturnValue.Mrv.GrupoFinanciamento == TipoGrupoFinanciamento.FlexComFGTS) {
                        DesabilitarCamposMensal();
                        DesabilitarCamposIntermediaria();
                        DesabilitarCamposFinanciamento();
                        crmForm.SetFieldReqLevel("new_valordofgts", true);
                    }
                    else if (informacoesPlano.ReturnValue.Mrv.GrupoFinanciamento == TipoGrupoFinanciamento.FlexCorrigidoComFGTS) {
                        DesabilitarCamposFinanciamento();
                        crmForm.SetFieldReqLevel("new_valordofgts", true);
                    }
                }
            }
        }

        /* Validação de obrigatoriedade da TAXA DESPACHANTE */
        function ValidarObrigatoriedadeTaxaDespachante(nomePlano, obrigatoriedadeTaxaDespachante) {
            //Executar somente se o formulário não estiver ready-only ou inativo
            if (crmForm.FormType == TypeRead || crmForm.FormType == TypeDisabled) return;

            if (crmForm.all.new_tipodecotacao.DataValue == TipoDeProposta.UNIDADE &&
                crmForm.all.new_vlr_tx.DataValue == null) {

                if (obrigatoriedadeTaxaDespachante != null) {
                    if (obrigatoriedadeTaxaDespachante.taxaObrigatoriaPorCidade && obrigatoriedadeTaxaDespachante.taxaObrigatoriaPorPlano) {
                        crmForm.all.new_justificativa_taxa_despachante.Disabled = false;
                        crmForm.SetFieldReqLevel("new_justificativa_taxa_despachante", true);
                    }
                    else
                        DesabilitarCamposTaxaDespachante();
                }
            }
        }

        function ConfigureFields(proposalType) {
            var isProductSale = IsProductSale(proposalType);
            crmForm.EscondeCampo("totallineitemamount", !isProductSale);
            crmForm.EscondeCampo("new_valor_total_assessoria", !isProductSale);

            /// Tab Tabela de vendas
            crmForm.all.tab1Tab.style.display = isProductSale ? "block" : "none";

            /// Tab Detalhes da Proposta.
            crmForm.all.tab2Tab.style.display = isProductSale ? "block" : "none";

            /// Tab Reserva.
            crmForm.all.tab3Tab.style.display = isProductSale ? "block" : "none";

            /// Tab Kits.
            crmForm.all.tab7Tab.style.display = !isProductSale ? "block" : "none";

            /// Obrigando a informação do campo valor total quando for uma proposta diferente de "Venda de Unidades"
            crmForm.SetFieldReqLevel('crmForm.all.paymenttermscode', !IsProductSale);

            crmForm.all.new_dentro_tabela_vendas.Disabled = false;
        }

        function IsProductSale(proposalType) {
            return proposalType == TipoDeProposta.UNIDADE ||
                proposalType == TipoDeProposta.GARAGEM ||
                proposalType == TipoDeProposta.LOTE ||
                proposalType == TipoDeProposta.LOJA;
        }

        /// Método executado para configurar a visão entre a venda de unidades e a venda de kits/box/garage/servico
        ConfigureFields(crmForm.all.new_tipodecotacao.DataValue);

        crmForm.ConfigurarCamposAbaDetalhesDaPropostaKit = function (condicaoPagamento) {
            //Executar somente se o formulário não estiver ready-only ou inativo
            if (crmForm.FormType == TypeRead || crmForm.FormType == TypeDisabled) return;

            switch (condicaoPagamento) {
                case CondicoesDePagamento.A_VISTA:
                    crmForm.all.new_dentro_tabela_vendas.DataValue = 1;
                    crmForm.all.new_qtdedeparcelaskit.DataValue = 1;
                    crmForm.all.new_qtdedeparcelaskit.Disabled = true;
                    crmForm.all.new_valor_entrada.Disabled = true;
                    break;

                case CondicoesDePagamento.A_PRAZO:
                    crmForm.all.new_dentro_tabela_vendas.DataValue = 1;
                    crmForm.all.new_qtdedeparcelaskit.Disabled = false;
                    crmForm.all.new_valor_entrada.Disabled = true;
                    crmForm.all.new_tempodecarencia.Disabled = false;
                    break;

                case CondicoesDePagamento.ENTRADA_MAIS_PRAZO:
                    crmForm.all.new_dentro_tabela_vendas.DataValue = 1;
                    crmForm.all.new_qtdedeparcelaskit.Disabled = false;
                    crmForm.all.new_valor_entrada.Disabled = false;
                    break;

                case CondicoesDePagamento.OUTRAS_CONDICOES:
                    crmForm.all.new_dentro_tabela_vendas.DataValue = 2;
                    crmForm.all.new_qtdedeparcelaskit.Disabled = true;
                    crmForm.all.new_valordaparcelakit.Disabled = true;
                    crmForm.all.new_valor_entrada.Disabled = false;
                    crmForm.all.new_visualizacao_valor_total_parcelamento_kit.Disabled = true;
                    break;

            }
        }

        function ValidarTipoKit() {
            var nomeGuia = document.getElementById("tab7Tab");
            nomeGuia.innerText = "Detalhes - Kit";

            /// Ganhar Proposta Automático
            if (crmForm.all.new_statusdacotacao.DataValue == StatusDaProposta.KIT_APROVADA && crmForm.all.statuscode.DataValue != StatusCode.GANHA)
                New_AcceptQuote();

            //Executar abaixo somente se o formulário não estiver ready-only ou inativo
            if (crmForm.FormType == TypeRead || crmForm.FormType == TypeDisabled) return;

            crmForm.all.new_chequekitacabamento.Disabled = true;
            crmForm.SetFieldReqLevel("paymenttermscode", true);
            crmForm.SetFieldReqLevel("new_qtdedeparcelaskit", true);
            crmForm.SetFieldReqLevel("new_datado1vencimento", true);
            crmForm.SetFieldReqLevel("new_valordaparcelakit", true);

            /// Configurar o valor apenas de visualização da totalização de kits, igual ao valor total da proposta.
            crmForm.all.new_visualizacao_valor_total_parcelamento_kit.DataValue = crmForm.all.new_valortotalproposta.DataValue;

            /// Desabilitar campo de indicação se a proposta está dentro da tabela de vendas.
            crmForm.all.new_dentro_tabela_vendas.Disabled = true;

            /// Para pagamento a vista o campo com a qtde de parcela fica desabilitado para edição.
            var condicaoPagamento = crmForm.all.paymenttermscode.DataValue;
            crmForm.all.new_qtdedeparcelaskit.Disabled = condicaoPagamento == 1 || condicaoPagamento == null;

            crmForm.ConfigurarCamposAbaDetalhesDaPropostaKit(condicaoPagamento);
        }

        ValidarTipoPermuta = function () {
            if (!crmForm.UsuarioPertenceEquipe("Contrato Permuta")) {
                alert("Usuário sem permissão para criar proposta de permuta.");
                window.close();
            }

            var nomeGuia = document.getElementById("tab7Tab");
            nomeGuia.innerText = "Detalhes - Permuta";

            /// Ganhar Proposta Automático
            if (crmForm.all.new_statusdacotacao.DataValue == StatusDaProposta.PERMUTA_APROVADA && crmForm.all.statuscode.DataValue != StatusCode.GANHA)
                New_AcceptQuote();

            crmForm.all.new_qtd_parcelas_permuta_c.parentNode.parentNode.style.display = "block";

            /// Ocultar campos
            OcultarSecaoFormaDePagamentoGuiaDetalheDaPropostaKitBoxGaragemServico();
            crmForm.all.new_data_tabela_vendas_c.style.display = "none";
            crmForm.all.new_data_tabela_vendas_d.style.display = "none";
            crmForm.all.new_dentro_tabela_vendas_c.style.display = "none";
            crmForm.all.new_dentro_tabela_vendas_d.style.display = "none";
            crmForm.all.new_valortotalservicos_c.style.display = "none";
            crmForm.all.new_valortotalservicos_d.style.display = "none";
            crmForm.all.totallineitemamount_c.style.display = "none";
            crmForm.all.totallineitemamount_d.style.display = "none";

            /// Esconde seções
            crmForm.all.new_chequekitacabamento_d.parentNode.parentNode.style.display = "none";
            crmForm.all.new_condiodepagamentokt_d.parentNode.parentNode.style.display = "none";

            /// Setar não obrigatorio
            crmForm.SetFieldReqLevel("new_data_tabela_vendas", false);

            /// Setar obrigatorio
            crmForm.SetFieldReqLevel("new_qtd_parcelas_permuta", true);
            crmForm.SetFieldReqLevel("new_valor_permuta", true);
            crmForm.SetFieldReqLevel("new_vencimento_permuta", true);

            /// Setar campo new_dentro_tabela_vendas para dentro da tabela
            if (crmForm.all.new_dentro_tabela_vendas.DataValue == null)
                crmForm.all.new_dentro_tabela_vendas.DataValue = 1;
        }

        ValidarTipoBox = function () {
            var nomeGuia = document.getElementById("tab7Tab");
            nomeGuia.innerText = "Detalhes - Box";
        }

        ValidarTipoGaragem = function () {
            var nomeGuia = document.getElementById("tab7Tab");
            nomeGuia.innerText = "Detalhes - Garagem";
        }

        ValidarTipoServico = function () {
            if (!crmForm.UsuarioPertenceEquipe("Contrato-Gerência")) {
                alert("Somente usuário da equipe Contrato-Gerência pode realizar venda de serviço. A janela será fechada.");
                window.close();
            }

            var nomeGuia = document.getElementById("tab7Tab");
            nomeGuia.innerText = "Detalhes - Serviço";

            /// Ganhar Proposta Automático
            if (crmForm.all.new_statusdacotacao.DataValue == StatusDaProposta.KIT_APROVADA && crmForm.all.statuscode.DataValue != StatusCode.GANHA)
                New_AcceptQuote();

            /// Ocultando Seção
            document.getElementById("{845502ff-99c6-4979-ab91-4626ee1d1c08}").style.display = "none";

            /// Removendo a obrigatoriedade
            crmForm.SetFieldReqLevel('new_data_tabela_vendas', false);
            crmForm.SetFieldReqLevel('new_valor_assessoria_rpa_nf', false);
            crmForm.SetFieldReqLevel('new_dentro_tabela_vendas', false);

            /// Atribuindo a obrigatoriedade
            crmForm.SetFieldReqLevel('new_data_tabela_vendas', false);
            crmForm.SetFieldReqLevel('new_dentro_tabela_vendas', false);

            /// Ocultando Campos
            crmForm.all.new_data_tabela_vendas_c.style.display = "none";
            crmForm.all.new_data_tabela_vendas_d.style.display = "none";
            crmForm.all.new_dentro_tabela_vendas_c.style.display = "none";
            crmForm.all.new_dentro_tabela_vendas_d.style.display = "none";

            /// Ocultando campos Campos da seção Detalhe Da Proposta/Kit/Box/Garagem/Servico
            OcultarSecaoFormaDePagamentoGuiaDetalheDaPropostaKitBoxGaragemServico();

            /// Configurando Guias para vendedor interno
            crmForm.all.tab1Tab.style.display = "none";
            crmForm.all.tab2Tab.style.display = "none";
            crmForm.all.tab3Tab.style.display = "none";
            crmForm.all.tab4Tab.style.display = "none";
            crmForm.all.tab7Tab.style.display = "block";

            if (crmForm.verificarTipoServicoTaxaDespachante()) {
                ConfigurarTaxaDespachanteVS(false);
                ConfigurarSecaoMensalDaGuiaDetalheDaPropostaKitBoxGaragemServico(true);
            }
            else {
                ConfigurarTaxaDespachanteVS(true);
                ConfigurarSecaoMensalDaGuiaDetalheDaPropostaKitBoxGaragemServico(false);
            }
        }

        function VerificarTipoDeProposta(tipoDeProposta) {
            switch (tipoDeProposta) {
                case TipoDeProposta.GARAGEM:
                    ValidarTipoGaragem();
                    break;

                case TipoDeProposta.BOX:
                    ValidarTipoBox();
                    break;

                case TipoDeProposta.KITACABAMENTO:
                    crmForm.ObterDataHabitese();
                    ValidarTipoKit();
                    break;

                case TipoDeProposta.SERVICO:
                    ValidarTipoServico();
                    break;

                case TipoDeProposta.PERMUTA:
                    ValidarTipoPermuta();
                    break;
            }
        }

        /// Calcula o numero de meses ate a entrega
        crmForm.GetMesesEntrega = function (dia, mes, ano) {
            var dataEntrega = new Date(ano, mes, dia)
            var dataAtual = new Date();
            var diff_date = dataEntrega - dataAtual;
            var num_months = (diff_date / 31536000000) * 12;
            var meses = Math.floor(num_months);
            return meses;
        }

        crmForm.CalcularValorParcelaKit = function (numeroParcela, valor) {
            /// Realizando a pesquisa para recuperar a data de entrega do empreendimento.
            var cmd = null;
            cmd = new RemoteCommand("MrvService", "CalcularValorParcelaKit", "/MRVCustomizations/");
            cmd.SetParameter("numeroParcela", numeroParcela);
            cmd.SetParameter("valorTotal", valor.toLocaleString());
            oResult = null;
            oResult = cmd.Execute();

            if (oResult.Success) {
                if (oResult.ReturnValue.Mrv.valorParcela) {
                    return oResult.ReturnValue.Mrv.valorParcela;
                }
            }

            return null;
        }

        /// Calcula o valor das parcelas de um kit com entrada. Aplica um desconto de 5% sobre o valor da entrada.
        crmForm.CalcularValorParcelaKitComEntrada = function (numeroParcelas, valor, valorEntrada) {
            var cmd = new RemoteCommand("MrvService", "CalcularValorParcelaKitComEntrada", "/MRVCustomizations/");
            cmd.SetParameter("numeroParcela", numeroParcelas);
            cmd.SetParameter("valorTotal", valor.toLocaleString());
            cmd.SetParameter("valorEntrada", valorEntrada.toLocaleString());
            var oResult = cmd.Execute();

            if (oResult.Success) {
                if (oResult.ReturnValue.Mrv.valorParcela)
                    return oResult.ReturnValue.Mrv.valorParcela;
            }

            return null;
        }

        function roundNumber(rnum, rlength) {
            var newnumber = Math.round(rnum * Math.pow(10, rlength)) / Math.pow(10, rlength);
            return newnumber.toFixed(2).toString().replace('.', ',');
        }

        crmForm.CalculaValor = function (valor, formaPagamento) {
            var texto = "";
            var qtdeParcelas = parseFloat(formaPagamento.toString().substr(1, 3));
            var parcela = valor / qtdeParcelas;
            texto = "Parcela 1: " + roundNumber(parcela, 2);
            for (var i = 2; i <= qtdeParcelas; i++) {
                texto += "\nParcela " + i + ": " + roundNumber(parcela, 2);
            }
            return texto;
        }

        function ValidaDia(valor) {
            var Retorno = true;
            switch (valor) {
                case 5:
                    Retorno = false;
                    break
                case 10:
                    Retorno = false;
                    break
            }
            return Retorno;
        }

        crmForm.ValidarDataReserva = function () {
            if (IsProductSale(crmForm.all.new_tipodecotacao.DataValue)) {
                return
                crmForm.all.new_inicio_reserva.DataValue == null ||
                crmForm.all.new_fim_ativacao.DataValue == null ||
                crmForm.all.new_fim_negociacao.DataValue == null ||
                crmForm.all.new_fim_reserva.DataValue == null;
            }
            else
                return false;
        }

        AuditarCredito = function (userLogado) {
            var cmd = new RemoteCommand("MrvService", "AuditarPropostaCredito", "/MrvCustomizations/");
            cmd.SetParameter("propostaId", crmForm.ObjectId);
            cmd.SetParameter("userLogado", userLogado);
            var resultado = cmd.Execute();

            if (resultado.Success) {
                if (resultado.ReturnValue.Mrv.Error) {
                    alert(resultado.ReturnValue.Mrv.Error);
                    return false;
                }
                else if (resultado.ReturnValue.Mrv.ErrorSoap) {
                    alert(resultado.ReturnValue.Mrv.ErrorSoap);
                    return false;
                }
                else {
                    document.getElementById("IFRAME_auditoria_credito").src = document.getElementById("IFRAME_auditoria_credito").src;
                    return true;
                }
            }
            return false;
        }

        ValidarAnaliseCredito = function () {
            if (!crmForm.ValidarPreenchimentoTaxaDespachante()) {
                return false;
            }
            var cmd = new RemoteCommand("MrvService", "ValidarAnaliseCredito", "/MrvCustomizations/");
            cmd.SetParameter("propostaId", crmForm.ObjectId);

            var resultado = cmd.Execute();
            var retorno = true;
            if (resultado.ReturnValue.Mrv.Valid) {
                retorno = true;
            } else {
                alert(resultado.ReturnValue.Mrv.Mensagem.toString());
                retorno = false;
            }
            return retorno;
        }

        crmForm.SolicitarAprovacao = function () {
            if (crmForm.FormType == TypeRead || crmForm.FormType == TypeDisabled) return;
            if (planoFinanciamentoBancarioAssociativo) {
                alert(MENSAGEM_PLANO_ASSOCIATIVO_BANCARIO);
                return;
            }
            if (!crmForm.IsDirty) {
                if (crmForm.IsValid()) {
                    if (AuditarCredito(false)) {
                        if (crmForm.ValidarDataReserva())
                            alert(MENSAGEM_DATAS_RESERVA_NAO_CARREGADAS);
                        else {
                            switch (crmForm.all.new_tipodecotacao.DataValue) {
                                case TipoDeProposta.UNIDADE:
                                case TipoDeProposta.GARAGEM:
                                case TipoDeProposta.LOTE:
                                case TipoDeProposta.LOJA:
                                    crmForm.all.new_statusdacotacao.DataValue = StatusDaProposta.EM_APROVACAO;
                                    activateQuote();
                                    break;

                                case TipoDeProposta.KITACABAMENTO:
                                    if (crmForm.all.paymenttermscode == CondicoesDePagamento.OUTRAS_CONDICOES)
                                        crmForm.all.new_statusdacotacao.DataValue = StatusDaProposta.EM_APROVACAO;
                                    else
                                        crmForm.all.new_statusdacotacao.DataValue = StatusDaProposta.KIT_APROVADA;
                                    activateQuote();
                                    break;

                                case TipoDeProposta.SERVICO:
                                    /// Se valor total do parcelamento for diferente de nulo, ganhar a proposta
                                    if (crmForm.all.new_new_vlr_tot_mens_money.DataValue != null || crmForm.all.new_vlr_tx_vs.DataValue != null) {
                                        var cmd = new RemoteCommand('MrvService', 'UserContainsTeams', '/MRVCustomizations/');
                                        var funcao_requerida;
                                        funcao_requerida = 'Contrato-Gerência';

                                        cmd.SetParameter('teams', funcao_requerida);
                                        var result = cmd.Execute();
                                        if (result.Success) {
                                            if (result.ReturnValue.Mrv.Found == true) {
                                                crmForm.all.new_statusdacotacao.DataValue = StatusDaProposta.KIT_APROVADA;
                                                activateQuote();
                                            }
                                            else {
                                                activateQuote();
                                            }
                                        }
                                    }
                                    else
                                        alert(MENSAGEN_SERVICO_VENDIDO_SEM_VALOR);
                                    break;

                                case TipoDeProposta.PERMUTA:
                                    /// Verificar se o valor da permuta é maio que 0 e ou diferente de nulo.
                                    if (crmForm.all.new_valor_permuta.DataValue == null ||
									crmForm.all.new_valor_permuta.DataValue == 0) {
                                        alert(PROPOSTA_PERMUTA_SEM_VALOR);
                                        return;
                                    }
                                    crmForm.all.new_statusdacotacao.DataValue = StatusDaProposta.PERMUTA_APROVADA;
                                    activateQuote();
                                    break;
                            }
                        }
                    }
                }
            }
            else
                alert(MENSAGEM_PROPOSTA_NAO_SALVA);
        }

        crmForm.EnviarContraProposta = function () {
            if (crmForm.FormType == TypeRead || crmForm.FormType == TypeDisabled) return;

            if (planoFinanciamentoBancarioAssociativo) {
                alert(MENSAGEM_PLANO_ASSOCIATIVO_BANCARIO);
                return;
            }
            if (!crmForm.IsDirty) {
                if (crmForm.IsValid()) {
                    if (ValidarAnaliseCredito()) {
                        if (AuditarCredito(false)) {
                            if (crmForm.ValidarDataReserva())
                                alert('Datas da reserva ainda não foram carregadas. \nSalve antes de solicitar aprovação.');
                            else {
                                var cmd = new RemoteCommand('MrvService', 'UserContainsTeams', '/MRVCustomizations/');
                                var funcao_requerida;

                                if (crmForm.all.new_dentro_tabela_vendas.DataValue != null && crmForm.all.new_dentro_tabela_vendas.DataValue == 1)
                                    funcao_requerida = 'Coordenação de Vendas - FT;Coordenação de Vendas - DT';
                                else
                                    funcao_requerida = 'Coordenação de Vendas - FT';

                                cmd.SetParameter('teams', funcao_requerida);
                                var result = cmd.Execute();

                                if (result.Success) {
                                    if (result.ReturnValue.Mrv.Found == true) {
                                        crmForm.all.new_corretor_ganha.DataValue = true;
                                        crmForm.all.new_statusdacotacao.DataValue = 2;
                                        activateQuote();
                                    }
                                    else
                                        alert('Você não tem privilégios necessário para executar esse tipo de operação.\nOperação Cancelada!');
                                }
                            }
                        }
                    }
                }
            }
            else
                alert('A proposta foi alterada. \nSalve antes de ativar a proposta.');
        }

        AbrirRegistro = function (typecode, id) {
            openObj(typecode, id);
        }

        crmForm.RemoverTipoNotaPromissoria = function (atributos) {
            /// Valores dos atributos == 1
            var tmp = atributos.split(";");
            for (index = 0; index < tmp.length; index++) {
                var object = eval("crmForm.all." + tmp[index])
                if (object.DataValue == null) {
                    object.DeleteOption(1);
                    continue;
                }
                if (object.DataValue != 1) {
                    object.DeleteOption(1);
                    continue;
                }
            }
        }

        crmForm.RemoverTipoNotaPromissoria("new_tipo_pgto_div_sinal2;new_tipodepagamentodivsinal;new_tipo_pgto_div_sinal3;new_tipo_pgto_div_sinal4;new_tipo_pgto_div_sinal5");

        crmForm.RemoverTipoPagamentoExcetoBoleto = function (atributos) {

            if (crmForm.FormType == TypeCreate || crmForm.FormType == TypeUpdate) {
                var tmp = atributos.split(";");
                var valuesOptions = "";

                for (index = 0; index < tmp.length; index++) {
                    var object = eval("crmForm.all." + tmp[index])

                    for (i = 0; i < object.Options.length; i++) {
                        if (object.Options[i].DataValue != null) {
                            if (tmp[index] == "new_forma_pgto_taxa_externa" ||
                           tmp[index] == "new_forma_pgto_taxa_interna") {
                                //remove deste combo todos os tipos de pagamento
                                //tratamento diferente pois o valor não é padronizado
                                if (object.Options[i].DataValue != 3) {
                                    valuesOptions += object.Options[i].DataValue + ";";
                                    continue;
                                }
                            }
                            else if (tmp[index] == "new_tipo_pagamento_sinal") {
                                if (object.Options[i].DataValue != 5 && object.Options[i].DataValue != 1) {
                                    valuesOptions += object.Options[i].DataValue + ";";
                                    continue;
                                }
                            }
                            else {
                                if (object.Options[i].DataValue != 5) {
                                    valuesOptions += object.Options[i].DataValue + ";";
                                    continue;
                                }
                            }
                        }
                    }

                    /// Remove os options
                    valuesOptions = valuesOptions.substring(1, valuesOptions.length - 1)

                    var tempOptions = valuesOptions.split(";");
                    var numeroItensOptions = tempOptions.length;

                    for (j = 0; j < numeroItensOptions; j++) {
                        object.DeleteOption(tempOptions[j]);
                    }

                    valuesOptions = ""
                }
            }
        }

        crmForm.CarregaValoresAnaliseCredito = function (oportunidadeId) {
            if (crmForm.FormType == TypeRead || crmForm.FormType == TypeDisabled)
                return;

            if (crmForm.FormType != TypeCreate && crmForm.FormType != TypeUpdate)
                return;

            if (crmForm.all.new_tipodecotacao.DataValue == TipoDeProposta.KITACABAMENTO || crmForm.all.new_tipodecotacao.DataValue == TipoDeProposta.SERVICO || crmForm.all.new_tipodecotacao.DataValue == TipoDeProposta.PERMUTA)
                return;

            var cmd = new RemoteCommand('WsAnaliseCredito', 'ObterValoresAnalise', '/AnaliseCredito/');
            cmd.SetParameter('oportunidadeId', oportunidadeId);
            var result = cmd.Execute();

            if (result.Success) {
                var fgts = result.ReturnValue.Mrv.FGTS;
                var financiamento = result.ReturnValue.Mrv.Financiamento;
                crmForm.all.new_valor_fgts_credito.DataValue = parseFloat(fgts.replace('.', '').replace(',', '.'));
                crmForm.all.new_valor_financiamento_credito.DataValue = parseFloat(financiamento.replace('.', '').replace(',', '.'));
            }
            else
                alert('Não foi possível carregar os valores de análise de crédito');
        }

        crmForm.RemoverTipoPagamentoExcetoBoleto("new_tipo_pagamento_sinal;new_tipodepagamentodivsinal;new_tipo_pgto_div_sinal2;new_tipo_pgto_div_sinal3;new_tipo_pgto_div_sinal4;new_tipo_pgto_div_sinal5;new_tp_pagto_cs_garant_1;new_tp_pagto_cs_garant_2;new_tipo_pagto_seg_1;new_tipo_pagto_seg_2;new_tipo_pagto_reemb_1;new_tipo_pagto_reemb_2;new_tipodepagamentoreembclientecart1;new_tipodepagamentoreembclientecart2;new_forma_pgto_taxa_interna;new_forma_pgto_taxa_externa");

        if (crmForm.all.opportunityid.DataValue != null && !planoFinanciamentoBancarioAssociativo) {
            crmForm.CarregaValoresAnaliseCredito(crmForm.all.opportunityid.DataValue[0].id);
        }

        if (crmForm.all.new_tabela_migrada.DataValue != null && crmForm.all.new_tabela_migrada.DataValue)
            DesativarCamposTabela();

        // DEFS 500946
        crmForm.ValidaDataDeVencimento = function (grupo, dia) {
            var rCmd = new RemoteCommand("MrvService", "ValidaDataDeVencimentoSeEstaValida", "/MRVCustomizations/");
            rCmd.SetParameter("oportunidadeId", crmForm.all.opportunityid.DataValue[0].id);
            rCmd.SetParameter("diaEscolhidoParaVencimento", dia);
            rCmd.SetParameter("grupo", grupo.toString());

            var retorno = rCmd.Execute();

            var diaValido = false;

            if (crmForm.TratarRetornoRemoteCommand(retorno, "ValidaDataDeVencimentoSeEstaValida")) {
                if (retorno.ReturnValue.Mrv.DiasVencimentoValida.toString() != "") {
                    diaValido = retorno.ReturnValue.Mrv.DiasVencimentoValida;
                    if (!diaValido) //se dia não é permitido
                        alert("Data inválida!\nFavor selecionar uma data válida, os dias aceitos são " + retorno.ReturnValue.Mrv.DiasVencimentoPermitidos);
                }
            }

            return diaValido;
        }

        crmForm.CalcularDataSinal = function () {
            var database = crmForm.ObterData(crmForm.all.new_data_base_contrato.DataValue);
            if (crmForm.all.new_data_base_contrato.DataValue == null) {
                alert('Informe a data base do contrato na guia Geral antes de preencher as parcelas.');
                return false;
            }

            var rCmd = new RemoteCommand("MrvService", "ObterDataSinal", "/MrvCustomizations/");
            rCmd.SetParameter('database', database);
            var retorno = rCmd.Execute();

            if (crmForm.TratarRetornoRemoteCommand(retorno, "ObterDataSinal")) {
                var dataAtual = retorno.ReturnValue.Mrv.DataAtual;
                if (dataAtual.getDate() > DiaReferencia.DIA_FECHAMENTO_VENCIMENTO_A_VISTA) {
                    crmForm.all.new_datadopagamento.Disabled = true;
                    crmForm.all.new_datadopagamento.DataValue = null;
                    crmForm.all.new_datadopagamento.DataValue = retorno.ReturnValue.Mrv.DataSinal;
                }
            }
            else {
                alert("Erro ao consultar uma data no servidor.");
                return false;
            }
        }

        crmForm.ObterDataHabitese = function () {

            var rCmd = new RemoteCommand('MrvService', 'ObterDataHabitese', '/MRVCustomizations/');
            rCmd.SetParameter('oportunidadeId', crmForm.all.opportunityid.DataValue[0].id);
            var retorno = rCmd.Execute();

            if (crmForm.TratarRetornoRemoteCommand(retorno, "ObterDataHabitese")) {
                if (retorno.ReturnValue.Mrv.Mensagem == null) {
                    if (retorno.ReturnValue.Mrv.DataHabitese != null) {
                        dataHabitese = retorno.ReturnValue.Mrv.DataHabitese;
                    }
                } else {
                    alert("Erro:" + retorno.ReturnValue.Mrv.Mensagem.toString());
                }
            }
        }

        // Obtêm a data no formato d/m/yyyy
        crmForm.ObterData = function (dataSemFormacao) {
            var dataFormatada = '';
            if (dataSemFormacao != undefined && dataSemFormacao != '') {
                try {
                    // Necessário acrescentar +1 para obter o número correto do mês.
                    var mes = dataSemFormacao.getMonth() + 1;
                    var dataFormatada = dataSemFormacao.getDate() + '/' + mes + '/' + dataSemFormacao.getFullYear();
                }
                catch (ex) {
                    dataFormatada = '';
                }
            }
            return dataFormatada;
        }

        crmForm.ObterNomesCamposParcelas = function (tipoCampo, grupoCampos) {
            var campos = [];
            var camposParcela = ListaCampos.ObterPorGrupo(grupoCampos);
            switch (tipoCampo) {
                case TipoCampoParcela.QUANTIDADE:
                    for (var i = 0; i < camposParcela.length; i++) {
                        campos.push(camposParcela[i].Quantidade)
                    }
                    break;
                case TipoCampoParcela.DATA_VENCIMENTO:
                    for (var i = 0; i < camposParcela.length; i++) {
                        campos.push(camposParcela[i].DataVencimento)
                    }
                    break;
                case TipoCampoParcela.PERIODICIDADE:
                    for (var i = 0; i < camposParcela.length; i++) {
                        campos.push(camposParcela[i].Periodicidade)
                    }
                    break;
                default:
                    break;
            }

            return campos;
        }

        crmForm.ObterDadosCamposParcelas = function (tipoCampo, grupoCampos) {
            var dadosRetorno = [];
            var nomesCampos = crmForm.ObterNomesCamposParcelas(tipoCampo, grupoCampos);
            for (var i = 0; i < nomesCampos.length; i++) {
                try {
                    // Tenta recuperar o valor de data do campo
                    var valor = crmForm.ObterData(eval(nomesCampos[i]).DataValue);
                    if (valor == '' || valor == undefined) {
                        if (tipoCampo == TipoCampoParcela.PERIODICIDADE) {
                            valor = ObterPeriodicidade(eval(nomesCampos[i]).DataValue);
                        }
                        else {
                            valor = eval(nomesCampos[i]).DataValue == null ? '' : eval(nomesCampos[i]).DataValue;
                        }
                    }
                    dadosRetorno[i] = valor;
                }
                catch (erro) {
                    dadosRetorno[i] = '';
                }
            }

            return dadosRetorno;
        }

        crmForm.ValidarVencimentoDivisaoSinal = function (campoValidacao) {
            var valido = true;
            valido = crmForm.ValidarDatasVencimento(campoValidacao, crmForm.all.new_data_base_contrato.DataValue, crmForm.all.new_datadopagamento.DataValue, Grupo.DIVISAO_SINAL, Grupo.MENSAL);
            if (valido) {
                // Valida a quantidade da primeira parcela de divisão de sinal
                var nomeCampoValidacao = "crmForm.all." + campoValidacao.id;
                var campoParcela = ListaCampos.ObterPorDataVencimento(nomeCampoValidacao);
                if (campoParcela != null && campoParcela.Secao == Secao.DIVISAO_SINAL_D1 &&
                   eval(campoParcela.Quantidade).DataValue != null && eval(campoParcela.Quantidade).DataValue != 1) {
                    alert(Mensagem.DIVISAO_SINAL_D1_QUANTIDADE_INVALIDA);
                    eval(campoParcela.Quantidade).DataValue = null;
                    valido = false;
                }

                if (valido) {
                    valido = crmForm.ValidarDatasVencimento(campoValidacao, crmForm.all.new_data_base_contrato.DataValue, crmForm.all.new_datadopagamento.DataValue, Grupo.DIVISAO_SINAL, Grupo.INTERMEDIARIA);
                }
            }
            return valido;
        }

        crmForm.ValidarVencimentoMensal = function (campoValidacao) {
            return crmForm.ValidarDatasVencimento(campoValidacao, crmForm.all.new_data_base_contrato.DataValue, crmForm.all.new_datadopagamento.DataValue, Grupo.MENSAL, Grupo.DIVISAO_SINAL);
        }

        crmForm.ValidarVencimentoIntermediaria = function (campoValidacao) {
            return crmForm.ValidarDatasVencimento(campoValidacao, crmForm.all.new_data_base_contrato.DataValue, crmForm.all.new_datadopagamento.DataValue, Grupo.INTERMEDIARIA, Grupo.DIVISAO_SINAL);
        }

        crmForm.ValidarDatasVencimento = function (campoValidacao, dataBaseContrato, dataSinal, grupoValidacao, grupoComparacao) {
            var retorno = true;
            if (eval(campoValidacao).DataValue != null) {

                var nomeCampoValidacao = "crmForm.all." + campoValidacao.id;
                var campoParcela = ListaCampos.ObterPorDataVencimento(nomeCampoValidacao);

                var quantidadesParcelaValidacao = crmForm.ObterDadosCamposParcelas(TipoCampoParcela.QUANTIDADE, grupoValidacao);
                var datasVencimentoValidacao = crmForm.ObterDadosCamposParcelas(TipoCampoParcela.DATA_VENCIMENTO, grupoValidacao);
                var periodicidadesValidacao = crmForm.ObterDadosCamposParcelas(TipoCampoParcela.PERIODICIDADE, grupoValidacao);
                var secaoValidacao = campoParcela != null ? campoParcela.Secao : "";

                var quantidadesParcelaComparacao = crmForm.ObterDadosCamposParcelas(TipoCampoParcela.QUANTIDADE, grupoComparacao);
                var datasVencimentoComparacao = crmForm.ObterDadosCamposParcelas(TipoCampoParcela.DATA_VENCIMENTO, grupoComparacao);
                var periodicidadesComparacao = crmForm.ObterDadosCamposParcelas(TipoCampoParcela.PERIODICIDADE, grupoComparacao);

                var cmd = new RemoteCommand('MrvService', 'ValidarDatasVencimento', '/MRVCustomizations/');
                cmd.SetParameter('dataValidacao', crmForm.ObterData(eval(campoValidacao).DataValue));
                cmd.SetParameter('dataBaseContrato', crmForm.ObterData(dataBaseContrato));
                cmd.SetParameter('dataSinal', crmForm.ObterData(dataSinal));
                cmd.SetParameter('grupoValidacao', grupoValidacao);
                cmd.SetParameter('secaoValidacao', secaoValidacao);
                cmd.SetParameter('grupoComparacao', grupoComparacao);
                cmd.SetParameter('quantidadesParcelasValidacao', quantidadesParcelaValidacao);
                cmd.SetParameter('datasVencimentoValidacao', datasVencimentoValidacao);
                cmd.SetParameter('periodicidadesValidacao', periodicidadesValidacao);
                cmd.SetParameter('quantidadesParcelasComparacao', quantidadesParcelaComparacao);
                cmd.SetParameter('datasVencimentoComparacao', datasVencimentoComparacao);
                cmd.SetParameter('periodicidadesComparacao', periodicidadesComparacao);
                var resultado = cmd.Execute();

                if (crmForm.TratarRetornoRemoteCommand(resultado, "ValidarDatasVencimento")) {
                    if (resultado.ReturnValue.Mrv.Valid) {
                        retorno = true;
                    } else {
                        alert(resultado.ReturnValue.Mrv.Mensagem.toString());
                        eval(campoValidacao).DataValue = null;
                        retorno = false;
                    }
                }
            }
            return retorno;
        }

        // Força a validação de vencimento das parcelas
        crmForm.ValidarTodosVencimentos = function () {
            var valido = true;
            // Valida o vencimento de sinal
            valido = crmForm.ValidarDatasVencimento(crmForm.all.new_datadopagamento, crmForm.all.new_data_base_contrato.DataValue, crmForm.all.new_datadopagamento.DataValue, Grupo.SINAL, '');
            // Valida campos de parcelas de divisão de sinal
            if (valido) {
                valido = crmForm.ValidarVencimentoDivisaoSinal(crmForm.all.new_datadevencimento1parcela);
                if (valido) {
                    valido = crmForm.ValidarVencimentoDivisaoSinal(crmForm.all.new_datadevencimento1parcelasinal1);
                }
            }
            else {
                if (!planoFinanciamentoBancarioAssociativo) {
                    crmForm.CalcularDataSinal();
                }
            }
            // Valida campos de parcelas mensais
            if (valido) { valido = crmForm.ValidarVencimentoMensal(crmForm.all.new_datadepagamentomensal); }
            // Valida campos de parcelas intermediárias
            if (valido) {
                var camposDataVencimentoIntermediaria = ListaCampos.ObterPorGrupo(Grupo.INTERMEDIARIA);
                for (var i = 0; i < camposDataVencimentoIntermediaria.length; i++) {
                    if (valido) { valido = crmForm.ValidarVencimentoIntermediaria(camposDataVencimentoIntermediaria[i].DataVencimento); }
                }
            }

            return valido;
        }

        function CampoParcela(grupoCampo, secaoCampo, nomeCampoQuantidade, nomeCampoDataVencimento, nomeCampoPeriodicidade) {
            this.Grupo = grupoCampo;
            this.Secao = secaoCampo;
            this.Quantidade = nomeCampoQuantidade;
            this.DataVencimento = nomeCampoDataVencimento;
            this.Periodicidade = nomeCampoPeriodicidade;
        }

        ListaCamposParcela = function () {
            var objeto = this;
            this.Lista = [
                new CampoParcela(Grupo.SINAL, Secao.SINAL_A_VISTA, "", "crmForm.all.new_datadopagamento", ""),
                new CampoParcela(Grupo.DIVISAO_SINAL, Secao.DIVISAO_SINAL_D1, "crmForm.all.new_qtdedeparcelas", "crmForm.all.new_datadevencimento1parcela", ""),
                new CampoParcela(Grupo.DIVISAO_SINAL, Secao.DIVISAO_SINAL_D2, "crmForm.all.new_qtdedeparcelassinal1", "crmForm.all.new_datadevencimento1parcelasinal1", ""),
                new CampoParcela(Grupo.DIVISAO_SINAL, Secao.DIVISAO_SINAL_D3, "crmForm.all.new_qtdedeparcelassinal2", "crmForm.all.new_datadevencimento1parcelasinal2", ""),
                new CampoParcela(Grupo.DIVISAO_SINAL, Secao.DIVISAO_SINAL_D4, "crmForm.all.new_qtdedeparcelassinal3", "crmForm.all.new_datadevencimento1parcelasinal3", ""),
                new CampoParcela(Grupo.DIVISAO_SINAL, Secao.DIVISAO_SINAL_D5, "crmForm.all.new_qtdedeparcelassinal4", "crmForm.all.new_datadevencimento1parcelasinal4", ""),
                new CampoParcela(Grupo.MENSAL, Secao.MENSAL_M1, "crmForm.all.new_qtdedeparcelasmensais", "crmForm.all.new_datadepagamentomensal", ""),
                new CampoParcela(Grupo.MENSAL, Secao.MENSAL_M2, "crmForm.all.new_qtdedeparcelasmensais1", "crmForm.all.new_datado1pagamentomensal1", ""),
                new CampoParcela(Grupo.MENSAL, Secao.MENSAL_M3, "crmForm.all.new_qtdedeparcelasmensais2", "crmForm.all.new_datado1pagamentomensal2", ""),
                new CampoParcela(Grupo.MENSAL, Secao.MENSAL_M4, "crmForm.all.new_qtdedeparcelasmensais3", "crmForm.all.new_datado1pagamentomensal3", ""),
                new CampoParcela(Grupo.MENSAL, Secao.MENSAL_M5, "crmForm.all.new_qtdedeparcelasmensais4", "crmForm.all.new_datado1pagamentomensal4", ""),
                new CampoParcela(Grupo.MENSAL, Secao.MENSAL_M6, "crmForm.all.new_qtdedeparcelasmensais5", "crmForm.all.new_datado1pagamentomensal5", ""),
                new CampoParcela(Grupo.INTERMEDIARIA, Secao.DIVISAO_INTERMEDIARIA_I1, "crmForm.all.new_qtdedeintermediarias", "crmForm.all.new_datadepagamentointermediaria", "crmForm.all.new_periododepagamentointermediaria"),
                new CampoParcela(Grupo.INTERMEDIARIA, Secao.DIVISAO_INTERMEDIARIA_I2, "crmForm.all.new_qtdedeparcelasintermediarias1", "crmForm.all.new_datadepagamentointermediria1", "crmForm.all.new_periododepagamentointermediaria1"),
                new CampoParcela(Grupo.INTERMEDIARIA, Secao.DIVISAO_INTERMEDIARIA_I3, "crmForm.all.new_qtdedeparcelasintermediarias2", "crmForm.all.new_datadepagamentointermediria2", "crmForm.all.new_periododepagamentointermediaria1")
            ];

            objeto.ObterPorSecao = function (secao) {
                for (var i = 0; i < this.Lista.length; i++) {
                    var item = this.Lista[i];
                    if (item.Secao == secao) {
                        return item;
                    }
                }
                return null;
            }

            objeto.ObterPorGrupo = function (grupo) {
                var lista = [];
                for (var i = 0; i < this.Lista.length; i++) {
                    var item = this.Lista[i];
                    if (item.Grupo == grupo) {
                        lista.push(item);
                    }
                }
                return lista;
            }

            objeto.ObterPorDataVencimento = function (nomeCampoDataVencimento) {
                for (var i = 0; i < this.Lista.length; i++) {
                    var item = this.Lista[i];
                    if (item.DataVencimento == nomeCampoDataVencimento) {
                        return item;
                    }
                }
                return null;
            }
        }

        ListaCampos = new ListaCamposParcela();

        crmForm.PreencherDataVencimento = function (secaoCampo) {
            switch (secaoCampo) {
                case Secao.DIVISAO_SINAL_D1:
                    break;
                case Secao.DIVISAO_SINAL_D2:
                    crmForm.CalcularDataVencimento(Secao.DIVISAO_SINAL_D3);
                    crmForm.CalcularDataVencimento(Secao.DIVISAO_SINAL_D4);
                    crmForm.CalcularDataVencimento(Secao.DIVISAO_SINAL_D5);
                    break;
                case Secao.DIVISAO_SINAL_D3:
                    crmForm.ValidarCalculoDataVencimento(Secao.DIVISAO_SINAL_D3);
                    crmForm.CalcularDataVencimento(Secao.DIVISAO_SINAL_D3);
                    crmForm.CalcularDataVencimento(Secao.DIVISAO_SINAL_D4);
                    crmForm.CalcularDataVencimento(Secao.DIVISAO_SINAL_D5);
                    break;
                case Secao.DIVISAO_SINAL_D4:
                    crmForm.ValidarCalculoDataVencimento(Secao.DIVISAO_SINAL_D4);
                    crmForm.CalcularDataVencimento(Secao.DIVISAO_SINAL_D4);
                    crmForm.CalcularDataVencimento(Secao.DIVISAO_SINAL_D5);
                    break;
                case Secao.DIVISAO_SINAL_D5:
                    crmForm.ValidarCalculoDataVencimento(Secao.DIVISAO_SINAL_D5);
                    crmForm.CalcularDataVencimento(Secao.DIVISAO_SINAL_D5);
                    break;
                case Secao.MENSAL_M1:
                    crmForm.CalcularDataVencimento(Secao.MENSAL_M2);
                    crmForm.CalcularDataVencimento(Secao.MENSAL_M3);
                    crmForm.CalcularDataVencimento(Secao.MENSAL_M4);
                    crmForm.CalcularDataVencimento(Secao.MENSAL_M5);
                    crmForm.CalcularDataVencimento(Secao.MENSAL_M6);
                    break;
                case Secao.MENSAL_M2:
                    crmForm.ValidarCalculoDataVencimento(Secao.MENSAL_M2);
                    crmForm.CalcularDataVencimento(Secao.MENSAL_M2);
                    crmForm.CalcularDataVencimento(Secao.MENSAL_M3);
                    crmForm.CalcularDataVencimento(Secao.MENSAL_M4);
                    crmForm.CalcularDataVencimento(Secao.MENSAL_M5);
                    crmForm.CalcularDataVencimento(Secao.MENSAL_M6);
                    break;
                case Secao.MENSAL_M3:
                    crmForm.ValidarCalculoDataVencimento(Secao.MENSAL_M3);
                    crmForm.CalcularDataVencimento(Secao.MENSAL_M3);
                    crmForm.CalcularDataVencimento(Secao.MENSAL_M4);
                    crmForm.CalcularDataVencimento(Secao.MENSAL_M5);
                    crmForm.CalcularDataVencimento(Secao.MENSAL_M6);
                    break;
                case Secao.MENSAL_M4:
                    crmForm.ValidarCalculoDataVencimento(Secao.MENSAL_M4);
                    crmForm.CalcularDataVencimento(Secao.MENSAL_M4);
                    crmForm.CalcularDataVencimento(Secao.MENSAL_M5);
                    crmForm.CalcularDataVencimento(Secao.MENSAL_M6);
                    break;
                case Secao.MENSAL_M5:
                    crmForm.ValidarCalculoDataVencimento(Secao.MENSAL_M5);
                    crmForm.CalcularDataVencimento(Secao.MENSAL_M5);
                    crmForm.CalcularDataVencimento(Secao.MENSAL_M6);
                    break;
                case Secao.MENSAL_M6:
                    crmForm.ValidarCalculoDataVencimento(Secao.MENSAL_M6);
                    crmForm.CalcularDataVencimento(Secao.MENSAL_M6);
                    break;
                case Secao.DIVISAO_INTERMEDIARIA_I1:
                    crmForm.CalcularDataVencimento(Secao.DIVISAO_INTERMEDIARIA_I2);
                    crmForm.CalcularDataVencimento(Secao.DIVISAO_INTERMEDIARIA_I3);
                    break;
                case Secao.DIVISAO_INTERMEDIARIA_I2:
                    crmForm.ValidarCalculoDataVencimento(Secao.DIVISAO_INTERMEDIARIA_I2);
                    crmForm.CalcularDataVencimento(Secao.DIVISAO_INTERMEDIARIA_I2);
                    crmForm.CalcularDataVencimento(Secao.DIVISAO_INTERMEDIARIA_I3);
                    break;
                case Secao.DIVISAO_INTERMEDIARIA_I3:
                    crmForm.ValidarCalculoDataVencimento(Secao.DIVISAO_INTERMEDIARIA_I3);
                    crmForm.CalcularDataVencimento(Secao.DIVISAO_INTERMEDIARIA_I3);
                    break;
            }
        }

        // Realizar o cálculo da data de vencimento
        crmForm.CalcularDataVencimento = function (secaoCampo) {
            var campoParcelaReferencia = null;
            var campoParcelaCalculo = ListaCampos.ObterPorSecao(secaoCampo);

            switch (secaoCampo) {
                case Secao.DIVISAO_SINAL_D2:
                    campoParcelaReferencia = ListaCampos.ObterPorSecao(Secao.DIVISAO_SINAL_D1);
                    break;
                case Secao.DIVISAO_SINAL_D3:
                    campoParcelaReferencia = ListaCampos.ObterPorSecao(Secao.DIVISAO_SINAL_D2);
                    break;
                case Secao.DIVISAO_SINAL_D4:
                    campoParcelaReferencia = ListaCampos.ObterPorSecao(Secao.DIVISAO_SINAL_D3);
                    break;
                case Secao.DIVISAO_SINAL_D5:
                    campoParcelaReferencia = ListaCampos.ObterPorSecao(Secao.DIVISAO_SINAL_D4);
                    break;
                case Secao.MENSAL_M2:
                    campoParcelaReferencia = ListaCampos.ObterPorSecao(Secao.MENSAL_M1);
                    break;
                case Secao.MENSAL_M3:
                    campoParcelaReferencia = ListaCampos.ObterPorSecao(Secao.MENSAL_M2);
                    break;
                case Secao.MENSAL_M4:
                    campoParcelaReferencia = ListaCampos.ObterPorSecao(Secao.MENSAL_M3);
                    break;
                case Secao.MENSAL_M5:
                    campoParcelaReferencia = ListaCampos.ObterPorSecao(Secao.MENSAL_M4);
                    break;
                case Secao.MENSAL_M6:
                    campoParcelaReferencia = ListaCampos.ObterPorSecao(Secao.MENSAL_M5);
                    break;
                case Secao.DIVISAO_INTERMEDIARIA_I2:
                    campoParcelaReferencia = ListaCampos.ObterPorSecao(Secao.DIVISAO_INTERMEDIARIA_I1);
                    break;
                case Secao.DIVISAO_INTERMEDIARIA_I3:
                    campoParcelaReferencia = ListaCampos.ObterPorSecao(Secao.DIVISAO_INTERMEDIARIA_I2);
                    break;
            }

            if (campoParcelaReferencia != null && campoParcelaCalculo != null) {
                if (eval(campoParcelaReferencia.Quantidade).DataValue != null &&
                    eval(campoParcelaReferencia.DataVencimento).DataValue != null &&
                    eval(campoParcelaCalculo.Quantidade).DataValue != null) {
                    // Calcula a data de vencimento
                    var dataReferencia = eval(campoParcelaReferencia.DataVencimento).DataValue;
                    var dataCalculada = new Date();
                    var quantidadeMeses = dataReferencia.getMonth() + 1 + eval(campoParcelaReferencia.Quantidade).DataValue;
                    var mes = (quantidadeMeses % 12) - 1;
                    var ano = Math.floor(quantidadeMeses / 12) + dataReferencia.getFullYear();
                    dataCalculada.setFullYear(ano, mes, dataReferencia.getUTCDate());
                    eval(campoParcelaCalculo.DataVencimento).DataValue = dataCalculada;
                }
                else {
                    eval(campoParcelaCalculo.DataVencimento).DataValue = null;
                }
            }
        }

        crmForm.ValidarCalculoDataVencimento = function (secaoCampo) {
            var campoParcelaReferencia = null;
            var campoParcelaCalculo = ListaCampos.ObterPorSecao(secaoCampo);
            var localCampoReferencia = null;

            switch (secaoCampo) {
                case Secao.DIVISAO_SINAL_D2:
                    break;
                case Secao.DIVISAO_SINAL_D3:
                    campoParcelaReferencia = ListaCampos.ObterPorSecao(Secao.DIVISAO_SINAL_D2);
                    localCampoReferencia = "D2 - Divisão do Sinal";
                    break;
                case Secao.DIVISAO_SINAL_D4:
                    campoParcelaReferencia = ListaCampos.ObterPorSecao(Secao.DIVISAO_SINAL_D3);
                    localCampoReferencia = "D3 - Divisão do Sinal";
                    break;
                case Secao.DIVISAO_SINAL_D5:
                    campoParcelaReferencia = ListaCampos.ObterPorSecao(Secao.DIVISAO_SINAL_D4);
                    localCampoReferencia = "D4 - Divisão do Sinal";
                    break;
                case Secao.MENSAL_M2:
                    campoParcelaReferencia = ListaCampos.ObterPorSecao(Secao.MENSAL_M1);
                    localCampoReferencia = "M1 - Mensal";
                    break;
                case Secao.MENSAL_M3:
                    campoParcelaReferencia = ListaCampos.ObterPorSecao(Secao.MENSAL_M2);
                    localCampoReferencia = "M2 - Mensal";
                    break;
                case Secao.MENSAL_M4:
                    campoParcelaReferencia = ListaCampos.ObterPorSecao(Secao.MENSAL_M3);
                    localCampoReferencia = "M3 - Mensal";
                    break;
                case Secao.MENSAL_M5:
                    campoParcelaReferencia = ListaCampos.ObterPorSecao(Secao.MENSAL_M4);
                    localCampoReferencia = "M4 - Mensal";
                    break;
                case Secao.MENSAL_M6:
                    campoParcelaReferencia = ListaCampos.ObterPorSecao(Secao.MENSAL_M5);
                    localCampoReferencia = "M5 - Mensal";
                    break;
                case Secao.DIVISAO_INTERMEDIARIA_I2:
                    campoParcelaReferencia = ListaCampos.ObterPorSecao(Secao.DIVISAO_INTERMEDIARIA_I1);
                    localCampoReferencia = "I1 - Intermediária";
                    break;
                case Secao.DIVISAO_INTERMEDIARIA_I3:
                    campoParcelaReferencia = ListaCampos.ObterPorSecao(Secao.DIVISAO_INTERMEDIARIA_I2);
                    localCampoReferencia = "I2 - Intermediária";
                    break;
            }

            if (campoParcelaReferencia != null && campoParcelaCalculo != null) {
                if (eval(campoParcelaReferencia.Quantidade).DataValue == null || eval(campoParcelaReferencia.DataVencimento).DataValue == null) {
                    alert("Favor preencher corretamente os campos " + localCampoReferencia);
                    eval(campoParcelaCalculo.Quantidade).DataValue = null;
                }

                if (eval(campoParcelaCalculo.Quantidade).DataValue == null) {
                    eval(campoParcelaCalculo.DataVencimento).DataValue = null;
                }
            }
        }

        function ObterPeriodicidade(periodicidade) {
            switch (periodicidade) {
                case PeriodicidadeSinal.ANUAL:
                    return "ANUAL";
                    break;
                case PeriodicidadeSinal.MENSAL:
                    return "MENSAL";
                    break;
                case PeriodicidadeSinal.SEMESTRAL:
                    return "SEMESTRAL";
                    break;
                case PeriodicidadeSinal.TRIMESTRAL:
                    return "TRIMESTRAL";
                    break;
                case PeriodicidadeSinal.QUINZENAL:
                    return "QUINZENAL";
                    break;
                default:
                    return "MENSAL";
            }
        }

        crmForm.ValidarDataBase = function () {
            var mensagemRetorno = "";
            var valido = true;
            if (crmForm.all.new_data_base_contrato.DataValue != null) {
                if (crmForm.all.new_tipodecotacao.DataValue == TipoDeProposta.KITACABAMENTO) {
                    var dataBase = crmForm.all.new_data_base_contrato.DataValue;
                    var dataAtual = new Date();
                    var monthDataAtual = dataAtual.getMonth() + 1;
                    var dataAtualYear = dataAtual.getFullYear();
                    if (monthDataAtual == 12 && dataBase.getMonth() == 0)
                        monthDataAtual = 0;
                    if (dataBase.getDate() != DiaReferencia.DATA_BASE_KIT || (dataBase.getMonth() != dataAtual.getMonth() && dataBase.getMonth() != monthDataAtual) || dataBase.getFullYear() != dataAtual.getFullYear()) {
                        if (monthDataAtual == 0) {
                            dataAtualYear += 1;
                            if (dataBase.getFullYear() != dataAtualYear || dataBase.getDate() != DiaReferencia.DATA_BASE_KIT) {
                                mensagemRetorno = "A data base de contrato tem que ser no dia " + DiaReferencia.DATA_BASE_KIT + " do mês atual ou seguinte";
                                valido = false;
                            }
                        }
                        else {
                            mensagemRetorno = "A data base de contrato tem que ser no dia " + DiaReferencia.DATA_BASE_KIT + " do mês atual ou seguinte";
                            valido = false;
                        }
                    }
                }
                else {
                    switch (crmForm.all.new_tipodecotacao.DataValue) {
                        case TipoDeProposta.UNIDADE:
                        case TipoDeProposta.GARAGEM:
                            // Valida a informação da data base
                            var dataBase = crmForm.all.new_data_base_contrato.DataValue;
                            var dataBaseParametro = dataBase.getUTCDate() + '/' + (dataBase.getMonth() + 1) + '/' + dataBase.getFullYear();
                            var remote = new RemoteCommand("MrvService", "ValidaDataBase", "/MRVCustomizations/");
                            remote.SetParameter("dataBase", dataBaseParametro);
                            var resultado = remote.Execute();
                            if (resultado.Success) {
                                if (resultado.ReturnValue.Mrv.Mensagem.toString() != "true") {
                                    mensagemRetorno = resultado.ReturnValue.Mrv.Mensagem.toString();
                                    valido = false;
                                }
                            }
                            break;
                        default:
                            break;
                    }
                }
            }
            else {
                mensagemRetorno = "Informe a data base do contrato na guia Geral.";
                valido = false;
            }

            if (!valido) {
                alert(mensagemRetorno);
            }

            return valido;
        }

        function ObterDataServidor() {
            var rCmd = new RemoteCommand("MrvService", "ObterDataServidor", "/MrvCustomizations/");
            var retorno = rCmd.Execute();
            if (crmForm.TratarRetornoRemoteCommand(retorno, "ObterDataServidor")) {
                DataAtualServidor = new Date();
                DataServidor = new Date();
                DataServidorMensal = new Date();
                DataAtualServidor.setFullYear(retorno.ReturnValue.Mrv.Ano, retorno.ReturnValue.Mrv.Mes - 1, retorno.ReturnValue.Mrv.Dia);
                DataAtualServidor.setHours(0, 0, 0, 0);
                DataServidor.setFullYear(retorno.ReturnValue.Mrv.Ano, retorno.ReturnValue.Mrv.Mes - 1, 1);
                DataServidor.setHours(0, 0, 0, 0);
                DataServidor.setMonth(DataServidor.getMonth() + 1);

                DataServidorMensal.setFullYear(retorno.ReturnValue.Mrv.Ano, (retorno.ReturnValue.Mrv.Mes - 1), 1);
                DataServidorMensal.setHours(0, 0, 0, 0);
                DataServidorMensal.setMonth(DataServidorMensal.getMonth() + 2);
            }
            else {
                alert("Erro ao consultar a data do servidor.");
            }
        }

        crmForm.ValidaPeriodicidadeIntermediaria = function () {
            // Intermediaria 1
            if (crmForm.all.new_periododepagamentointermediaria.DataValue != null &&
                crmForm.all.new_periododepagamentointermediaria.DataValue == PeriodicidadeIntermediaria.PARCELAUNICA) {
                if (crmForm.all.new_qtdedeintermediarias.DataValue == null || crmForm.all.new_qtdedeintermediarias.DataValue != 1) {
                    alert(MENSAGEM_PERIODICIDADE_PARCELAUNICA);
                    crmForm.all.new_qtdedeintermediarias.DataValue = 1;
                }
            }
            // Intermediaria 2
            if (crmForm.all.new_periododepagamentointermediaria1.DataValue != null &&
                crmForm.all.new_periododepagamentointermediaria1.DataValue == PeriodicidadeIntermediaria.PARCELAUNICA) {
                if (crmForm.all.new_qtdedeparcelasintermediarias1.DataValue == null || crmForm.all.new_qtdedeparcelasintermediarias1.DataValue != 1) {
                    alert(MENSAGEM_PERIODICIDADE_PARCELAUNICA);
                    crmForm.all.new_qtdedeparcelasintermediarias1.DataValue = 1;
                }
            }
            // Intermediaria 3
            if (crmForm.all.new_periododepagamentointermediaria2.DataValue != null &&
                crmForm.all.new_periododepagamentointermediaria2.DataValue == PeriodicidadeIntermediaria.PARCELAUNICA) {
                if (crmForm.all.new_qtdedeparcelasintermediarias2.DataValue == null || crmForm.all.new_qtdedeparcelasintermediarias2.DataValue != 1) {
                    alert(MENSAGEM_PERIODICIDADE_PARCELAUNICA);
                    crmForm.all.new_qtdedeparcelasintermediarias2.DataValue = 1;
                }
            }
        }

        crmForm.RemoverPeriodicidadeIntermediaria = function (atributos) {
            // Função que remove os itens Mensal Quinzenal Trimestral dos campos de periodicidade dos picklist da
            // seção intermediaria        
            if (crmForm.FormType == TypeCreate || crmForm.FormType == TypeUpdate) {
                var tmp = atributos.split(";");
                var valuesOptions = "";

                for (index = 0; index < tmp.length; index++) {
                    var object = eval("crmForm.all." + tmp[index])

                    if (object.DataValue == PeriodicidadeIntermediaria.MENSAL || object.DataValue == PeriodicidadeIntermediaria.QUINZENAL
                        || object.DataValue == PeriodicidadeIntermediaria.TRIMESTRAL)
                        object.DataValue = "";

                    object.DeleteOption(PeriodicidadeIntermediaria.MENSAL);
                    object.DeleteOption(PeriodicidadeIntermediaria.QUINZENAL);
                    object.DeleteOption(PeriodicidadeIntermediaria.TRIMESTRAL);
                }

                if (planoFinanciamentoBancarioAssociativo) {
                    new_valortotalproposta_onchange0();
                }
            }
        }

        crmForm.RemoverPeriodicidadeIntermediaria("new_periododepagamentointermediaria;new_periododepagamentointermediaria1;new_periododepagamentointermediaria2");

        crmForm.HabilitaCamposIntermediaria = function () {
            // Habilita e desabilita campos segunda intermediaria
            if (crmForm.FormType == TypeCreate || crmForm.FormType == TypeUpdate) {
                if (crmForm.all.new_qtdedeintermediarias.DataValue != null && crmForm.all.new_valosdaparcelaintermediaria.DataValue != null
             && crmForm.all.new_datadepagamentointermediaria.DataValue != null && crmForm.all.new_periododepagamentointermediaria.DataValue != null)
                    crmForm.DesabilitarCampos("new_qtdedeparcelasintermediarias1;new_valordaparcelaintermediaria1;new_datadepagamentointermediria1;new_periododepagamentointermediaria1", false);
                else {
                    crmForm.DesabilitarCampos("new_qtdedeparcelasintermediarias1;new_valordaparcelaintermediaria1;new_datadepagamentointermediria1;new_periododepagamentointermediaria1", true);
                    crmForm.LimpaCampos("new_qtdedeparcelasintermediarias1;new_valordaparcelaintermediaria1;new_datadepagamentointermediria1;new_periododepagamentointermediaria1");
                }

                // Habilita e desabilita campos terceira intermediaria
                if (crmForm.all.new_qtdedeparcelasintermediarias1.DataValue != null && crmForm.all.new_valordaparcelaintermediaria1.DataValue != null
             && crmForm.all.new_datadepagamentointermediria1.DataValue != null && crmForm.all.new_periododepagamentointermediaria1.DataValue != null)
                    crmForm.DesabilitarCampos("new_qtdedeparcelasintermediarias2;new_valordaparcelaintermediaria2;new_datadepagamentointermediria2;new_periododepagamentointermediaria2", false);
                else {
                    crmForm.DesabilitarCampos("new_qtdedeparcelasintermediarias2;new_valordaparcelaintermediaria2;new_datadepagamentointermediria2;new_periododepagamentointermediaria2", true);
                    crmForm.LimpaCampos("new_qtdedeparcelasintermediarias2;new_valordaparcelaintermediaria2;new_datadepagamentointermediria2;new_periododepagamentointermediaria2");
                }
            }
        }

        crmForm.HabilitaCamposIntermediaria();

        crmForm.FormatarData = function (data) {
            if (data != null) {
                var from = data.split("-");
                if (from.length >= 2) {
                    return new Date(from[0], from[1] - 1, from[2]);
                }
            }
            return null;
        }

        crmForm.ValidarParcelaDesconto = function () {
            if (crmForm.FormType == TypeUpdate) {
                if ((crmForm.all.new_desconto_quantidade_1.DataValue != null) && (crmForm.all.new_desconto_valor_1.DataValue != null)
                    && (crmForm.all.new_desconto_quantidade_2.DataValue != null) && (crmForm.all.new_desconto_valor_2.DataValue != null)) {

                    var resultado = crmForm.ObterValidacaoParcelaDesconto();

                    if (resultado.ReturnValue.Mrv.Valid) {
                        if (crmForm.FormType == TypeCreate || crmForm.FormType == TypeUpdate) {
                            crmForm.all.new_desconto_vencimento_1.DataValue = crmForm.FormatarData(resultado.ReturnValue.Mrv.DataVencimentoPrimeiraParcela);
                            crmForm.all.new_desconto_vencimento_1.ForceSubmit = true;
                            crmForm.all.new_desconto_vencimento_2.DataValue = crmForm.FormatarData(resultado.ReturnValue.Mrv.DataVencimentoSegundaParcela);
                            crmForm.all.new_desconto_vencimento_2.ForceSubmit = true;
                        }
                        return true;
                    } else if (resultado.ReturnValue.Mrv.Erro) {
                        alert(resultado.ReturnValue.Mrv.Msg.toString());
                        event.returnValue = false;
                        return false;
                    }
                    else {
                        alert(resultado.ReturnValue.Mrv.Mensagem.toString());
                        event.returnValue = false;
                        return false;
                    }
                }
            }
        }

        crmForm.ObterValidacaoParcelaDesconto = function () {
            if (crmForm.FormType == TypeUpdate) {
                if ((crmForm.all.new_desconto_quantidade_1.DataValue != null) && (crmForm.all.new_desconto_valor_1.DataValue != null)
                    && (crmForm.all.new_desconto_quantidade_2.DataValue != null) && (crmForm.all.new_desconto_valor_2.DataValue != null)) {

                    var cmd = new RemoteCommand("MrvService", "ValidarRegrasParcelaDesconto", "/MrvCustomizations/");

                    cmd.SetParameter("propostaId", crmForm.ObjectId);
                    cmd.SetParameter("dataBase", crmForm.all.new_data_base_contrato.DataValue.toLocaleDateString());
                    cmd.SetParameter("sinalVencimentoAto", crmForm.all.new_datadopagamento.DataValue != null ? crmForm.all.new_datadopagamento.DataValue.toLocaleDateString() : "");
                    cmd.SetParameter("sinalQuantidadeAto", "1");
                    cmd.SetParameter("sinalVencimento1", crmForm.all.new_datadevencimento1parcela.DataValue != null ? crmForm.all.new_datadevencimento1parcela.DataValue.toLocaleDateString() : "");
                    cmd.SetParameter("sinalQuantidade1", crmForm.all.new_qtdedeparcelas.DataValue != null ? crmForm.all.new_qtdedeparcelas.DataValue : "");
                    cmd.SetParameter("sinalVencimento2", crmForm.all.new_datadevencimento1parcelasinal1.DataValue != null ? crmForm.all.new_datadevencimento1parcelasinal1.DataValue.toLocaleDateString() : "");
                    cmd.SetParameter("sinalQuantidade2", crmForm.all.new_qtdedeparcelassinal1.DataValue != null ? crmForm.all.new_qtdedeparcelassinal1.DataValue : "");
                    cmd.SetParameter("sinalVencimento3", crmForm.all.new_datadevencimento1parcelasinal2.DataValue != null ? crmForm.all.new_datadevencimento1parcelasinal2.DataValue.toLocaleDateString() : "");
                    cmd.SetParameter("sinalQuantidade3", crmForm.all.new_qtdedeparcelassinal2.DataValue != null ? crmForm.all.new_qtdedeparcelassinal2.DataValue : "");
                    cmd.SetParameter("sinalVencimento4", crmForm.all.new_datadevencimento1parcelasinal3.DataValue != null ? crmForm.all.new_datadevencimento1parcelasinal3.DataValue.toLocaleDateString() : "");
                    cmd.SetParameter("sinalQuantidade4", crmForm.all.new_qtdedeparcelassinal3.DataValue != null ? crmForm.all.new_qtdedeparcelassinal3.DataValue : "");
                    cmd.SetParameter("sinalVencimento5", crmForm.all.new_datadevencimento1parcelasinal4.DataValue != null ? crmForm.all.new_datadevencimento1parcelasinal4.DataValue.toLocaleDateString() : "");
                    cmd.SetParameter("sinalQuantidade5", crmForm.all.new_qtdedeparcelassinal4.DataValue != null ? crmForm.all.new_qtdedeparcelassinal4.DataValue : "");

                    cmd.SetParameter("mensalQuantidade1", crmForm.all.new_qtdedeparcelasmensais.DataValue != null ? crmForm.all.new_qtdedeparcelasmensais.DataValue : "");
                    cmd.SetParameter("mensalValor1", crmForm.all.new_valordaparcelamensal.DataValue != null ? crmForm.all.new_valordaparcelamensal.DataValue : "");
                    cmd.SetParameter("mensalVencimento1", crmForm.all.new_datadepagamentomensal.DataValue != null ? crmForm.all.new_datadepagamentomensal.DataValue.toLocaleDateString() : "");

                    cmd.SetParameter("mensalQuantidade2", crmForm.all.new_qtdedeparcelasmensais1.DataValue != null ? crmForm.all.new_qtdedeparcelasmensais1.DataValue : "");
                    cmd.SetParameter("mensalValor2", crmForm.all.new_valordaparcelamensal1.DataValue != null ? crmForm.all.new_valordaparcelamensal1.DataValue : "");
                    cmd.SetParameter("mensalVencimento2", crmForm.all.new_datado1pagamentomensal1.DataValue != null ? crmForm.all.new_datado1pagamentomensal1.DataValue.toLocaleDateString() : "");

                    cmd.SetParameter("mensalQuantidade3", crmForm.all.new_qtdedeparcelasmensais2.DataValue != null ? crmForm.all.new_qtdedeparcelasmensais2.DataValue : "");
                    cmd.SetParameter("mensalValor3", crmForm.all.new_valordaparcelamensal2.DataValue != null ? crmForm.all.new_valordaparcelamensal2.DataValue : "");
                    cmd.SetParameter("mensalVencimento3", crmForm.all.new_datado1pagamentomensal2.DataValue != null ? crmForm.all.new_datado1pagamentomensal2.DataValue.toLocaleDateString() : "");

                    cmd.SetParameter("mensalQuantidade4", crmForm.all.new_qtdedeparcelasmensais3.DataValue != null ? crmForm.all.new_qtdedeparcelasmensais3.DataValue : "");
                    cmd.SetParameter("mensalValor4", crmForm.all.new_valordaparcelamensal3.DataValue != null ? crmForm.all.new_valordaparcelamensal3.DataValue : "");
                    cmd.SetParameter("mensalVencimento4", crmForm.all.new_datado1pagamentomensal3.DataValue != null ? crmForm.all.new_datado1pagamentomensal3.DataValue.toLocaleDateString() : "");

                    cmd.SetParameter("mensalQuantidade5", crmForm.all.new_qtdedeparcelasmensais4.DataValue != null ? crmForm.all.new_qtdedeparcelasmensais4.DataValue : "");
                    cmd.SetParameter("mensalValor5", crmForm.all.new_valordaparcelamensal4.DataValue != null ? crmForm.all.new_valordaparcelamensal4.DataValue : "");
                    cmd.SetParameter("mensalVencimento5", crmForm.all.new_datado1pagamentomensal4.DataValue != null ? crmForm.all.new_datado1pagamentomensal4.DataValue.toLocaleDateString() : "");

                    cmd.SetParameter("mensalQuantidade6", crmForm.all.new_qtdedeparcelasmensais5.DataValue != null ? crmForm.all.new_qtdedeparcelasmensais5.DataValue : "");
                    cmd.SetParameter("mensalValor6", crmForm.all.new_valordaparcelamensal5.DataValue != null ? crmForm.all.new_valordaparcelamensal5.DataValue : "");
                    cmd.SetParameter("mensalVencimento6", crmForm.all.new_datado1pagamentomensal5.DataValue != null ? crmForm.all.new_datado1pagamentomensal5.DataValue.toLocaleDateString() : "");

                    cmd.SetParameter("intermediariaQuantidade1", crmForm.all.new_qtdedeintermediarias.DataValue != null ? crmForm.all.new_qtdedeintermediarias.DataValue : "");
                    cmd.SetParameter("intermediariaValor1", crmForm.all.new_valosdaparcelaintermediaria.DataValue != null ? crmForm.all.new_valosdaparcelaintermediaria.DataValue : "");
                    cmd.SetParameter("intermediariaVencimento1", crmForm.all.new_datadepagamentointermediaria.DataValue != null ? crmForm.all.new_datadepagamentointermediaria.DataValue.toLocaleDateString() : "");
                    cmd.SetParameter("intermediariaPeriodicidade1", crmForm.all.new_periododepagamentointermediaria.SelectedText != null ? crmForm.all.new_periododepagamentointermediaria.SelectedText : "");

                    cmd.SetParameter("intermediariaQuantidade2", crmForm.all.new_qtdedeparcelasintermediarias1.DataValue != null ? crmForm.all.new_qtdedeparcelasintermediarias1.DataValue : "");
                    cmd.SetParameter("intermediariaValor2", crmForm.all.new_valordaparcelaintermediaria1.DataValue != null ? crmForm.all.new_valordaparcelaintermediaria1.DataValue : "");
                    cmd.SetParameter("intermediariaVencimento2", crmForm.all.new_datadepagamentointermediria1.DataValue != null ? crmForm.all.new_datadepagamentointermediria1.DataValue.toLocaleDateString() : "");
                    cmd.SetParameter("intermediariaPeriodicidade2", crmForm.all.new_periododepagamentointermediaria1.SelectedText != null ? crmForm.all.new_periododepagamentointermediaria1.SelectedText : "");

                    cmd.SetParameter("intermediariaQuantidade3", crmForm.all.new_qtdedeparcelasintermediarias2.DataValue != null ? crmForm.all.new_qtdedeparcelasintermediarias2.DataValue : "");
                    cmd.SetParameter("intermediariaValor3", crmForm.all.new_valordaparcelaintermediaria2.DataValue != null ? crmForm.all.new_valordaparcelaintermediaria2.DataValue : "");
                    cmd.SetParameter("intermediariaVencimento3", crmForm.all.new_datadepagamentointermediria2.DataValue != null ? crmForm.all.new_datadepagamentointermediria2.DataValue.toLocaleDateString() : "");
                    cmd.SetParameter("intermediariaPeriodicidade3", crmForm.all.new_periododepagamentointermediaria2.SelectedText != null ? crmForm.all.new_periododepagamentointermediaria2.SelectedText : "");

                    var resultado = cmd.Execute();

                    if (crmForm.TratarRetornoRemoteCommand(resultado, "ValidarRegrasParcelaDesconto")) {
                        return resultado;
                    }
                }
            }
            return null;

        }

        crmForm.HabilitarDataBaseAdministrador = function () {
            if ((crmForm.FormType == TypeCreate || crmForm.FormType == TypeUpdate) && crmForm.all.new_tipodecotacao.DataValue == TipoDeProposta.UNIDADE) {
                if (crmForm.UsuarioPertenceEquipe(Equipe.ADMINISTRADORES)) {
                    crmForm.DesabilitarCampos("new_data_base_contrato", false);
                    if (crmForm.FormType == TypeCreate) {
                        crmForm.all.new_data_base_contrato.DataValue = new Date().setDate(DIA_DATA_BASE);
                        new_data_base_contrato_onchange0();
                    }
                }
                else {
                    crmForm.DesabilitarCampos("new_data_base_contrato", true);
                    if (crmForm.FormType == TypeCreate) {
                        crmForm.all.new_data_base_contrato.DataValue = null;
                    }
                }
            }

        }

        crmForm.HabilitarDataBaseAdministrador();

        Auditar = function (auditoriaCSG) {
            var cmd = new RemoteCommand("MrvService", "Auditar", "/MrvCustomizations/");
            cmd.SetParameter("entidadeId", crmForm.ObjectId);
            cmd.SetParameter("tipoAuditoria", TipoAuditoria.PROPOSTA);

            var resultado = cmd.Execute();

            if (resultado.Success) {
                if (resultado.ReturnValue.Mrv.Error)
                    alert(resultado.ReturnValue.Mrv.Error);
                else if (resultado.ReturnValue.Mrv.ErrorSoap)
                    alert(resultado.ReturnValue.Mrv.ErrorSoap)
                else {
                    document.getElementById("IFRAME_auditoria").src = document.getElementById("IFRAME_auditoria").src;
                }
            }
        }

        crmForm.CancelarProposta = function () {

            if (planoFinanciamentoBancarioAssociativo) {
                alert(MENSAGEM_PLANO_ASSOCIATIVO_BANCARIO);
                return;
            }

            var oCmd = new RemoteCommand('MrvService', 'FecharEmRascunho', '/MrvCustomizations/');
            oCmd.SetParameter('propostaId', crmForm.ObjectId);
            oCmd.SetParameter('equipe', 'CancelaProposta');
            var oResult = oCmd.Execute();

            if (oResult.Success) {
                if (oResult.ReturnValue.Mrv.ErrorSoap)
                    alert(oResult.ReturnValue.Mrv.ErrorSoap);
                else if (oResult.ReturnValue.Mrv.Error)
                    alert(oResult.ReturnValue.Mrv.Error);
                else
                    alert('Proposta cancelada.'); window.close();
            }
        }

        crmForm.ValidarGanharProposta = function () {
            if (planoFinanciamentoBancarioAssociativo) {
                alert(MENSAGEM_PLANO_ASSOCIATIVO_BANCARIO);
                return;
            }
            else {
                New_AcceptQuote();
            }
        }

        crmForm.ValidarRevisarProposta = function () {
            if (planoFinanciamentoBancarioAssociativo) {
                alert(MENSAGEM_PLANO_ASSOCIATIVO_BANCARIO);
                return;
            }
            else {
                reviseActiveQuote();
            }
        }

        if (planoFinanciamentoBancarioAssociativo) {
            crmForm.DesabilitarFormulario(true);
        }
    }
}
catch (erro) {
    formularioValido = false;
    alert("Ocorreu um erro no formulário.\n" + erro.description);
}