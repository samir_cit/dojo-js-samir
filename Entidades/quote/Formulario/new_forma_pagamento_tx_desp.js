﻿crmForm.SetFieldReqLevel('new_vlr_tx', crmForm.all.new_forma_pagamento_tx_desp.DataValue != null);
crmForm.SetFieldReqLevel('new_forma_pgto_taxa_interna', crmForm.all.new_forma_pagamento_tx_desp.DataValue != null);
if (crmForm.all.new_forma_pagamento_tx_desp.DataValue != null) {
    var cmd = new RemoteCommand("MrvService", "ObterValorTaxaDespachante", "/MrvCustomizations/");
    cmd.SetParameter('oportunidadeId', crmForm.all.opportunityid.DataValue[0].id);
    var result = cmd.Execute();
    if (result.Success) {
        if (result.ReturnValue.Mrv.Erro) {
            alert(result.ReturnValue.Mrv.Mensagem);
            crmForm.all.new_vlr_tx.DataValue = null;
            crmForm.all.new_parcelamentotaxasinterno.DataValue = null;
        }
        else {
            var valorStr = result.ReturnValue.Mrv.Valor;
            crmForm.all.new_vlr_tx.DataValue = parseFloat(valorStr.replace('.', '').replace(',', '.'));
            crmForm.all.new_justificativa_taxa_despachante.DataValue = null;
            crmForm.all.new_justificativa_taxa_despachante.Disabled = true;
            crmForm.SetFieldReqLevel('new_justificativa_taxa_despachante', false);
            crmForm.all.new_justificativa_taxa_despachante.ForceSubmit = true;
        }
    }
}
else {
    crmForm.all.new_vlr_tx.DataValue = null;
    crmForm.all.new_forma_pgto_taxa_interna.DataValue = null;
    crmForm.all.new_justificativa_taxa_despachante.Disabled = false;
}
crmForm.all.new_vlr_tx.Disabled = false;
crmForm.all.new_vlr_tx.FireOnChange();

if (crmForm.all.new_vlr_tx.DataValue == "0.00" || crmForm.all.new_vlr_tx.DataValue == null) {
    crmForm.SetFieldReqLevel('new_justificativa_taxa_despachante', true);
    crmForm.all.new_vlr_tx.Disabled = false;
}
else {
    crmForm.all.new_vlr_tx.Disabled = true;
}