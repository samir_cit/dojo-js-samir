﻿crmForm.SetFieldReqLevel('new_forma_pagamento_tx_desp', crmForm.all.new_vlr_tx.DataValue != null);
crmForm.SetFieldReqLevel('new_forma_pgto_taxa_interna', crmForm.all.new_vlr_tx.DataValue != null);
if (crmForm.all.new_vlr_tx != null && crmForm.all.new_vlr_tx.DataValue != null) {
    if (crmForm.all.new_forma_pagamento_tx_desp.DataValue != null) {
        crmForm.all.new_parcelamentotaxasinterno.DataValue =
            crmForm.CalculaValor(crmForm.all.new_vlr_tx.DataValue, crmForm.all.new_forma_pagamento_tx_desp.DataValue);

        crmForm.ValidaValorTaxaDespachante(crmForm.all.new_forma_pagamento_tx_desp,
            crmForm.all.new_vlr_tx, crmForm.all.new_parcelamentotaxasinterno);
    }
}
else
    crmForm.all.new_parcelamentotaxasinterno.DataValue = null;