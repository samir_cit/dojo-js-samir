﻿crmForm.SetFieldReqLevel('new_vlr_tx_vs', crmForm.all.new_forma_pagamento_tx_desp_vs.DataValue != null);
crmForm.SetFieldReqLevel('new_forma_pgto_taxa_externa', crmForm.all.new_forma_pagamento_tx_desp_vs.DataValue != null);
if (crmForm.all.new_forma_pagamento_tx_desp_vs.DataValue != null) {
    var cmd = new RemoteCommand("MrvService", "ObterValorTaxaDespachante", "/MrvCustomizations/");
    cmd.SetParameter('oportunidadeId', crmForm.all.opportunityid.DataValue[0].id);
    var result = cmd.Execute();
    if (result.Success) {
        if (result.ReturnValue.Mrv.Erro) {
            alert(result.ReturnValue.Mrv.Mensagem);
            crmForm.all.new_vlr_tx_vs.DataValue = null;
            crmForm.all.new_parcelamentotaxasexterno.DataValue = null;
        }
        else {
            var valorStr = result.ReturnValue.Mrv.Valor;
            crmForm.all.new_vlr_tx_vs.DataValue = parseFloat(valorStr.replace('.', '').replace(',', '.'));
        }
    }
}
else {
    crmForm.all.new_vlr_tx_vs.DataValue = null;
    crmForm.all.new_forma_pgto_taxa_externa.DataValue = null;
}
crmForm.all.new_vlr_tx_vs.Disabled = false;
crmForm.all.new_vlr_tx_vs.FireOnChange();
if (crmForm.all.new_vlr_tx_vs.DataValue == "0.00" || crmForm.all.new_vlr_tx_vs.DataValue == null)
    crmForm.all.new_vlr_tx_vs.Disabled = false;
else {
    crmForm.all.new_vlr_tx_vs.Disabled = true;
}