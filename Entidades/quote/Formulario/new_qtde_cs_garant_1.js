﻿//Calcula valor total da seção Casa Garantia da Guia Despesas Gerais
new_vlr_tot_mes_cs_garant_onchange0();

if (crmForm.all.new_qtde_cs_garant_1.DataValue != null && crmForm.all.new_qtde_cs_garant_1.DataValue > 0) {
    crmForm.SetFieldReqLevel("new_vlr_parc_cs_garant_1", true);
    crmForm.SetFieldReqLevel("new_dt_vcto_1parcela_1", true);
    crmForm.SetFieldReqLevel("new_tp_pagto_cs_garant_1", true);
}
else {
    crmForm.SetFieldReqLevel("new_vlr_parc_cs_garant_1", false);
    crmForm.SetFieldReqLevel("new_dt_vcto_1parcela_1", false);
    crmForm.SetFieldReqLevel("new_tp_pagto_cs_garant_1", false);
}
