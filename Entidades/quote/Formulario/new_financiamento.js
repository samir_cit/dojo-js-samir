﻿//Impede que o valor de financiamento seja maior que o aprovado pelo sistema de credito
var valorCredito = 0;
var valorFinanciamento = 0;
if (crmForm.all.new_valor_financiamento_credito.DataValue != null)
    valorCredito = crmForm.all.new_valor_financiamento_credito.DataValue;

if (crmForm.all.new_financiamento.DataValue != null)
    valorFinanciamento = crmForm.all.new_financiamento.DataValue;

if (valorCredito > 0 && valorFinanciamento > valorCredito) {
    alert("Valor do financiamento não pode ser maior que o valor aprovado pelo Crédito!");
    crmForm.all.new_financiamento.DataValue = 0;
}
new_valortotalproposta_onchange0();