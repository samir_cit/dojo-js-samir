﻿if (crmForm.all.new_tipodecotacao.DataValue == TipoDeProposta.KITACABAMENTO) {
    if (crmForm.all.new_visualizacao_valor_total_parcelamento_kit.DataValue != null) {
        crmForm.all.new_valortotalproposta.DataValue = crmForm.all.new_visualizacao_valor_total_parcelamento_kit.DataValue;
        crmForm.all.new_valortotalservicos.DataValue = 0;
        crmForm.all.new_valortotalgeral.DataValue = crmForm.all.new_valortotalproposta.DataValue;
    }
}
else if (crmForm.all.new_tipodecotacao.DataValue == TipoDeProposta.SERVICO) {
    var vlrTotalServicos =
        crmForm.all.new_new_vlr_tot_mens_money.DataValue +
        crmForm.all.new_vlr_tx.DataValue +
        crmForm.all.new_vlr_tx_vs.DataValue +
        crmForm.all.new_vl_tot_mes_seg.DataValue +
        crmForm.all.new_vlr_tot_mes_cs_garant.DataValue;

    crmForm.all.new_valortotalproposta.DataValue = vlrTotalServicos;
    crmForm.all.new_valortotalservicos.DataValue = vlrTotalServicos;
    crmForm.all.new_valortotalgeral.DataValue = vlrTotalServicos;
}
else if (crmForm.all.new_tipodecotacao.DataValue == TipoDeProposta.PERMUTA) {
    if (crmForm.all.new_valor_permuta.DataValue != null && crmForm.all.new_qtd_parcelas_permuta.DataValue != null) {
        var valorTotalPermuta = crmForm.all.new_qtd_parcelas_permuta.DataValue * crmForm.all.new_valor_permuta.DataValue;

        crmForm.all.new_valortotalproposta.DataValue = crmForm.all.new_valortotalgeral.DataValue = valorTotalPermuta;
    }
}
else {
    /// Cálculo de Assessoria
    var vlrAss = 0;
    if (crmForm.all.new_qtdedeparcelasassessoria.DataValue != null || crmForm.all.new_valordasparcelasassessoria.DataValue != null || crmForm.all.new_datadevencimento1parcelaassessoria.DataValue != null) {
        crmForm.SetFieldReqLevel('new_qtdedeparcelasassessoria', true);
        crmForm.SetFieldReqLevel('new_valordasparcelasassessoria', true);
        crmForm.SetFieldReqLevel('new_datadevencimento1parcelaassessoria', true);
        crmForm.SetFieldReqLevel('new_tipodepagamentoassessoria', true);

        if (crmForm.all.new_qtdedeparcelasassessoria.DataValue > 1)
            crmForm.SetFieldReqLevel('new_periodicidadeassessoria', true);
        else
            crmForm.SetFieldReqLevel('new_periodicidadeassessoria', false);

        if (crmForm.all.new_qtdedeparcelasassessoria.DataValue != null && crmForm.all.new_valordasparcelasassessoria.DataValue != null)
            vlrAss += crmForm.all.new_qtdedeparcelasassessoria.DataValue * crmForm.all.new_valordasparcelasassessoria.DataValue;
    }
    else {
        crmForm.SetFieldReqLevel('new_qtdedeparcelasassessoria', false);
        crmForm.SetFieldReqLevel('new_valordasparcelasassessoria', false);
        crmForm.SetFieldReqLevel('new_datadevencimento1parcelaassessoria', false);
        crmForm.SetFieldReqLevel('new_tipodepagamentoassessoria', false);
        crmForm.SetFieldReqLevel('new_periodicidadeassessoria', false);
    }

    if (crmForm.all.new_qtdedeparcelasassessoria1.DataValue != null || crmForm.all.new_valordasparcelasassessoria1.DataValue != null || crmForm.all.new_datadevencimento1parcelaassessoria1.DataValue != null) {
        crmForm.SetFieldReqLevel('new_qtdedeparcelasassessoria1', true);
        crmForm.SetFieldReqLevel('new_valordasparcelasassessoria1', true);
        crmForm.SetFieldReqLevel('new_datadevencimento1parcelaassessoria1', true);

        if (crmForm.all.new_qtdedeparcelasassessoria1.DataValue > 1)
            crmForm.SetFieldReqLevel('new_periodicidadeassessoria1', true);
        else
            crmForm.SetFieldReqLevel('new_periodicidadeassessoria1', false);

        if (crmForm.all.new_qtdedeparcelasassessoria1.DataValue != null && crmForm.all.new_valordasparcelasassessoria1.DataValue != null)
            vlrAss += crmForm.all.new_qtdedeparcelasassessoria1.DataValue * crmForm.all.new_valordasparcelasassessoria1.DataValue;
    }
    else {
        crmForm.SetFieldReqLevel('new_qtdedeparcelasassessoria1', false);
        crmForm.SetFieldReqLevel('new_valordasparcelasassessoria1', false);
        crmForm.SetFieldReqLevel('new_datadevencimento1parcelaassessoria1', false);
        crmForm.SetFieldReqLevel('new_periodicidadeassessoria1', false);
    }

    crmForm.all.new_valorassessoria.DataValue = vlrAss;

    var vlrDivisaoSinal = 0;
    /// Cálculo Sinal
    if (crmForm.all.new_valorsinalavista.DataValue != null ||
        crmForm.all.new_datadopagamento.DataValue != null ||
        crmForm.all.new_tipo_pagamento_sinal.DataValue != null) {
        crmForm.SetFieldReqLevel('new_valorsinalavista', true);
        crmForm.SetFieldReqLevel('new_datadopagamento', true); 
        crmForm.SetFieldReqLevel('new_tipo_pagamento_sinal', true);
    }
    else {
        crmForm.SetFieldReqLevel('new_valorsinalavista', false);
        crmForm.SetFieldReqLevel('new_datadopagamento', false);
        crmForm.SetFieldReqLevel('new_tipo_pagamento_sinal', false);
    }
       
    if (crmForm.all.new_qtdedeparcelas.DataValue != null || crmForm.all.new_valordaparcela.DataValue != null || crmForm.all.new_datadevencimento1parcela.DataValue != null) {
        crmForm.SetFieldReqLevel('new_qtdedeparcelas', true);
        crmForm.SetFieldReqLevel('new_valordaparcela', true);
        crmForm.SetFieldReqLevel('new_datadevencimento1parcela', true);
        crmForm.SetFieldReqLevel('new_tipodepagamentodivsinal', true);

        if (crmForm.all.new_qtdedeparcelas.DataValue >= 1)
            crmForm.SetFieldReqLevel('new_periodicidadesinal', true);
        else
            crmForm.SetFieldReqLevel('new_periodicidadesinal', false);

        if (crmForm.all.new_qtdedeparcelas.DataValue != null && crmForm.all.new_valordaparcela.DataValue != null)
            vlrDivisaoSinal += crmForm.all.new_qtdedeparcelas.DataValue * crmForm.all.new_valordaparcela.DataValue;
    }
    else {
        crmForm.SetFieldReqLevel('new_qtdedeparcelas', false);
        crmForm.SetFieldReqLevel('new_valordaparcela', false);
        crmForm.SetFieldReqLevel('new_datadevencimento1parcela', false);
        crmForm.SetFieldReqLevel('new_tipodepagamentodivsinal', false);
        crmForm.SetFieldReqLevel('new_periodicidadesinal', false);
    }

    if (crmForm.all.new_qtdedeparcelassinal1.DataValue != null || crmForm.all.new_valordaparcelasinal1.DataValue != null || crmForm.all.new_datadevencimento1parcelasinal1.DataValue != null) {
        crmForm.SetFieldReqLevel('new_qtdedeparcelassinal1', true);
        crmForm.SetFieldReqLevel('new_valordaparcelasinal1', true);
        crmForm.SetFieldReqLevel('new_datadevencimento1parcelasinal1', true);
        crmForm.SetFieldReqLevel("new_tipo_pgto_div_sinal2", true);

        if (crmForm.all.new_qtdedeparcelassinal1.DataValue >= 1)
            crmForm.SetFieldReqLevel('new_periodicidadesinal1', true);
        else
            crmForm.SetFieldReqLevel('new_periodicidadesinal1', false);

        if (crmForm.all.new_qtdedeparcelassinal1.DataValue != null && crmForm.all.new_valordaparcelasinal1.DataValue != null)
            vlrDivisaoSinal += crmForm.all.new_qtdedeparcelassinal1.DataValue * crmForm.all.new_valordaparcelasinal1.DataValue;
    }
    else {
        crmForm.SetFieldReqLevel("new_tipo_pgto_div_sinal2", false);
        crmForm.SetFieldReqLevel('new_qtdedeparcelassinal1', false);
        crmForm.SetFieldReqLevel('new_valordaparcelasinal1', false);
        crmForm.SetFieldReqLevel('new_datadevencimento1parcelasinal1', false);
        crmForm.SetFieldReqLevel('new_periodicidadesinal1', false);
    }

    if (crmForm.all.new_qtdedeparcelassinal2.DataValue != null || crmForm.all.new_valordasparcelassinal2.DataValue != null || crmForm.all.new_datadevencimento1parcelasinal2.DataValue != null) {
        crmForm.SetFieldReqLevel('new_qtdedeparcelassinal2', true);
        crmForm.SetFieldReqLevel('new_valordasparcelassinal2', true);
        crmForm.SetFieldReqLevel('new_datadevencimento1parcelasinal2', true);
        crmForm.SetFieldReqLevel("new_tipo_pgto_div_sinal3", true);

        if (crmForm.all.new_qtdedeparcelassinal2.DataValue >= 1)
            crmForm.SetFieldReqLevel('new_periodicidadesinal2', true);
        else
            crmForm.SetFieldReqLevel('new_periodicidadesinal2', false);

        if (crmForm.all.new_qtdedeparcelassinal2.DataValue != null && crmForm.all.new_valordasparcelassinal2.DataValue != null)
            vlrDivisaoSinal += crmForm.all.new_qtdedeparcelassinal2.DataValue * crmForm.all.new_valordasparcelassinal2.DataValue;
    }
    else {
        crmForm.SetFieldReqLevel('new_qtdedeparcelassinal2', false);
        crmForm.SetFieldReqLevel('new_valordasparcelassinal2', false);
        crmForm.SetFieldReqLevel('new_datadevencimento1parcelasinal2', false);
        crmForm.SetFieldReqLevel('new_periodicidadesinal2', false);
        crmForm.SetFieldReqLevel("new_tipo_pgto_div_sinal3", false);
    }

    if (crmForm.all.new_qtdedeparcelassinal3.DataValue != null || crmForm.all.new_valordasparcelassinal3.DataValue != null || crmForm.all.new_datadevencimento1parcelasinal3.DataValue != null) {
        crmForm.SetFieldReqLevel('new_qtdedeparcelassinal3', true);
        crmForm.SetFieldReqLevel('new_valordasparcelassinal3', true);
        crmForm.SetFieldReqLevel('new_datadevencimento1parcelasinal3', true);
        crmForm.SetFieldReqLevel("new_tipo_pgto_div_sinal4", true);

        if (crmForm.all.new_qtdedeparcelassinal3.DataValue >= 1)
            crmForm.SetFieldReqLevel('new_periodicidadesinal3', true);
        else
            crmForm.SetFieldReqLevel('new_periodicidadesinal3', false);

        if (crmForm.all.new_qtdedeparcelassinal3.DataValue != null && crmForm.all.new_valordasparcelassinal3.DataValue != null)
            vlrDivisaoSinal += crmForm.all.new_qtdedeparcelassinal3.DataValue * crmForm.all.new_valordasparcelassinal3.DataValue;
    }
    else {
        crmForm.SetFieldReqLevel('new_qtdedeparcelassinal3', false);
        crmForm.SetFieldReqLevel('new_valordasparcelassinal3', false);
        crmForm.SetFieldReqLevel('new_datadevencimento1parcelasinal3', false);
        crmForm.SetFieldReqLevel('new_periodicidadesinal3', false);
        crmForm.SetFieldReqLevel("new_tipo_pgto_div_sinal4", false);
    }

    if (crmForm.all.new_qtdedeparcelassinal4.DataValue != null || crmForm.all.new_valordasparcelassinal4.DataValue != null || crmForm.all.new_datadevencimento1parcelasinal4.DataValue != null) {
        crmForm.SetFieldReqLevel('new_qtdedeparcelassinal4', true);
        crmForm.SetFieldReqLevel('new_valordasparcelassinal4', true);
        crmForm.SetFieldReqLevel('new_datadevencimento1parcelasinal4', true);
        crmForm.SetFieldReqLevel("new_tipo_pgto_div_sinal5", true);

        if (crmForm.all.new_qtdedeparcelassinal4.DataValue >= 1)
            crmForm.SetFieldReqLevel('new_periodicidadesinal4', true);
        else
            crmForm.SetFieldReqLevel('new_periodicidadesinal4', false);

        if (crmForm.all.new_qtdedeparcelassinal4.DataValue != null && crmForm.all.new_valordasparcelassinal4.DataValue != null)
            vlrDivisaoSinal += crmForm.all.new_qtdedeparcelassinal4.DataValue * crmForm.all.new_valordasparcelassinal4.DataValue;
    }
    else {
        crmForm.SetFieldReqLevel('new_qtdedeparcelassinal4', false);
        crmForm.SetFieldReqLevel('new_valordasparcelassinal4', false);
        crmForm.SetFieldReqLevel('new_datadevencimento1parcelasinal4', false);
        crmForm.SetFieldReqLevel('new_periodicidadesinal4', false);
        crmForm.SetFieldReqLevel("new_tipo_pgto_div_sinal5", false);
    }

    crmForm.all.new_sinal.DataValue = vlrDivisaoSinal;
    if (crmForm.all.new_valorsinalavista.DataValue != null)
        crmForm.all.new_sinal.DataValue += crmForm.all.new_valorsinalavista.DataValue;
    crmForm.all.new_divisaosinal.Disabled = false;
    crmForm.all.new_divisaosinal.DataValue = vlrDivisaoSinal > 0;
    crmForm.all.new_divisaosinal.Disabled = true;
    
    /// Cálculo Mensal
    var vlrMensal = 0;
    if (crmForm.all.new_qtdedeparcelasmensais.DataValue != null || crmForm.all.new_valordaparcelamensal.DataValue != null || crmForm.all.new_datadepagamentomensal.DataValue != null) {
        crmForm.SetFieldReqLevel('new_qtdedeparcelasmensais', true);
        crmForm.SetFieldReqLevel('new_valordaparcelamensal', true);
        crmForm.SetFieldReqLevel('new_datadepagamentomensal', true);

        if (crmForm.all.new_qtdedeparcelasmensais.DataValue != null && crmForm.all.new_valordaparcelamensal.DataValue != null)
            vlrMensal += crmForm.all.new_qtdedeparcelasmensais.DataValue * crmForm.all.new_valordaparcelamensal.DataValue;
    }
    else {
        crmForm.SetFieldReqLevel('new_qtdedeparcelasmensais', false);
        crmForm.SetFieldReqLevel('new_valordaparcelamensal', false);
        crmForm.SetFieldReqLevel('new_datadepagamentomensal', false);
    }

    if (crmForm.all.new_qtdedeparcelasmensais1.DataValue != null || crmForm.all.new_valordaparcelamensal1.DataValue != null || crmForm.all.new_datado1pagamentomensal1.DataValue != null) {
        crmForm.SetFieldReqLevel('new_qtdedeparcelasmensais1', true);
        crmForm.SetFieldReqLevel('new_valordaparcelamensal1', true);
        crmForm.SetFieldReqLevel('new_datado1pagamentomensal1', true);

        if (crmForm.all.new_qtdedeparcelasmensais1.DataValue != null && crmForm.all.new_valordaparcelamensal1.DataValue != null)
            vlrMensal += crmForm.all.new_qtdedeparcelasmensais1.DataValue * crmForm.all.new_valordaparcelamensal1.DataValue;
    }
    else {
        crmForm.SetFieldReqLevel('new_qtdedeparcelasmensais1', false);
        crmForm.SetFieldReqLevel('new_valordaparcelamensal1', false);
        crmForm.SetFieldReqLevel('new_datado1pagamentomensal1', false);
    }

    if (crmForm.all.new_qtdedeparcelasmensais2.DataValue != null || crmForm.all.new_valordaparcelamensal2.DataValue != null || crmForm.all.new_datado1pagamentomensal2.DataValue != null) {
        crmForm.SetFieldReqLevel('new_qtdedeparcelasmensais2', true);
        crmForm.SetFieldReqLevel('new_valordaparcelamensal2', true);
        crmForm.SetFieldReqLevel('new_datado1pagamentomensal2', true);

        if (crmForm.all.new_qtdedeparcelasmensais2.DataValue != null && crmForm.all.new_valordaparcelamensal2.DataValue != null)
            vlrMensal += crmForm.all.new_qtdedeparcelasmensais2.DataValue * crmForm.all.new_valordaparcelamensal2.DataValue;
    }
    else {
        crmForm.SetFieldReqLevel('new_qtdedeparcelasmensais2', false);
        crmForm.SetFieldReqLevel('new_valordaparcelamensal2', false);
        crmForm.SetFieldReqLevel('new_datado1pagamentomensal2', false);
    }

    if (crmForm.all.new_qtdedeparcelasmensais3.DataValue != null || crmForm.all.new_valordaparcelamensal3.DataValue != null || crmForm.all.new_datado1pagamentomensal3.DataValue != null) {
        crmForm.SetFieldReqLevel('new_qtdedeparcelasmensais3', true);
        crmForm.SetFieldReqLevel('new_valordaparcelamensal3', true);
        crmForm.SetFieldReqLevel('new_datado1pagamentomensal3', true);

        if (crmForm.all.new_qtdedeparcelasmensais3.DataValue != null && crmForm.all.new_valordaparcelamensal3.DataValue != null)
            vlrMensal += crmForm.all.new_qtdedeparcelasmensais3.DataValue * crmForm.all.new_valordaparcelamensal3.DataValue;
    }
    else {
        crmForm.SetFieldReqLevel('new_qtdedeparcelasmensais3', false);
        crmForm.SetFieldReqLevel('new_valordaparcelamensal3', false);
        crmForm.SetFieldReqLevel('new_datado1pagamentomensal3', false);
    }

    if (crmForm.all.new_qtdedeparcelasmensais4.DataValue != null || crmForm.all.new_valordaparcelamensal4.DataValue != null || crmForm.all.new_datado1pagamentomensal4.DataValue != null) {
        crmForm.SetFieldReqLevel('new_qtdedeparcelasmensais4', true);
        crmForm.SetFieldReqLevel('new_valordaparcelamensal4', true);
        crmForm.SetFieldReqLevel('new_datado1pagamentomensal4', true);

        if (crmForm.all.new_qtdedeparcelasmensais4.DataValue != null && crmForm.all.new_valordaparcelamensal4.DataValue != null)
            vlrMensal += crmForm.all.new_qtdedeparcelasmensais4.DataValue * crmForm.all.new_valordaparcelamensal4.DataValue;
    }
    else {
        crmForm.SetFieldReqLevel('new_qtdedeparcelasmensais4', false);
        crmForm.SetFieldReqLevel('new_valordaparcelamensal4', false);
        crmForm.SetFieldReqLevel('new_datado1pagamentomensal4', false);
    }

    if (crmForm.all.new_qtdedeparcelasmensais5.DataValue != null || crmForm.all.new_valordaparcelamensal5.DataValue != null || crmForm.all.new_datado1pagamentomensal5.DataValue != null) {
        crmForm.SetFieldReqLevel('new_qtdedeparcelasmensais5', true);
        crmForm.SetFieldReqLevel('new_valordaparcelamensal5', true);
        crmForm.SetFieldReqLevel('new_datado1pagamentomensal5', true);

        if (crmForm.all.new_qtdedeparcelasmensais5.DataValue != null && crmForm.all.new_valordaparcelamensal5.DataValue != null)
            vlrMensal += crmForm.all.new_qtdedeparcelasmensais5.DataValue * crmForm.all.new_valordaparcelamensal5.DataValue;
    }
    else {
        crmForm.SetFieldReqLevel('new_qtdedeparcelasmensais5', false);
        crmForm.SetFieldReqLevel('new_valordaparcelamensal5', false);
        crmForm.SetFieldReqLevel('new_datado1pagamentomensal5', false);
    }
    crmForm.all.new_mensal.DataValue = vlrMensal;

    /// Cálculo Intermediário.
    var vltInter = 0;
    if (crmForm.all.new_qtdedeintermediarias.DataValue != null || crmForm.all.new_valosdaparcelaintermediaria.DataValue != null || crmForm.all.new_datadepagamentointermediaria.DataValue != null || crmForm.all.new_periododepagamentointermediaria.DataValue != null) {
        crmForm.SetFieldReqLevel('new_qtdedeintermediarias', true);
        crmForm.SetFieldReqLevel('new_valosdaparcelaintermediaria', true);
        crmForm.SetFieldReqLevel('new_datadepagamentointermediaria', true);

        crmForm.SetFieldReqLevel('new_periododepagamentointermediaria', true);

        if (crmForm.all.new_qtdedeintermediarias.DataValue != null && crmForm.all.new_valosdaparcelaintermediaria.DataValue != null)
            vltInter += crmForm.all.new_qtdedeintermediarias.DataValue * crmForm.all.new_valosdaparcelaintermediaria.DataValue;
    }
    else {
        crmForm.SetFieldReqLevel('new_qtdedeintermediarias', false);
        crmForm.SetFieldReqLevel('new_valosdaparcelaintermediaria', false);
        crmForm.SetFieldReqLevel('new_datadepagamentointermediaria', false);
        crmForm.SetFieldReqLevel('new_periododepagamentointermediaria', false);
    }

    if (crmForm.all.new_qtdedeparcelasintermediarias1.DataValue != null || crmForm.all.new_valordaparcelaintermediaria1.DataValue != null || crmForm.all.new_datadepagamentointermediria1.DataValue != null || crmForm.all.new_periododepagamentointermediaria1.DataValue != null) {
        crmForm.SetFieldReqLevel('new_qtdedeparcelasintermediarias1', true);
        crmForm.SetFieldReqLevel('new_valordaparcelaintermediaria1', true);
        crmForm.SetFieldReqLevel('new_datadepagamentointermediria1', true);
            
        crmForm.SetFieldReqLevel('new_periododepagamentointermediaria1', true);

        if (crmForm.all.new_qtdedeparcelasintermediarias1.DataValue != null && crmForm.all.new_valordaparcelaintermediaria1.DataValue != null)
            vltInter += crmForm.all.new_qtdedeparcelasintermediarias1.DataValue * crmForm.all.new_valordaparcelaintermediaria1.DataValue;
    }
    else {
        crmForm.SetFieldReqLevel('new_qtdedeparcelasintermediarias1', false);
        crmForm.SetFieldReqLevel('new_valordaparcelaintermediaria1', false);
        crmForm.SetFieldReqLevel('new_datadepagamentointermediria1', false);
        crmForm.SetFieldReqLevel('new_periododepagamentointermediaria1', false);
    }

    if (crmForm.all.new_qtdedeparcelasintermediarias2.DataValue != null || crmForm.all.new_valordaparcelaintermediaria2.DataValue != null || crmForm.all.new_datadepagamentointermediria2.DataValue != null || crmForm.all.new_periododepagamentointermediaria2.DataValue != null) {
        crmForm.SetFieldReqLevel('new_qtdedeparcelasintermediarias2', true);
        crmForm.SetFieldReqLevel('new_valordaparcelaintermediaria2', true);
        crmForm.SetFieldReqLevel('new_datadepagamentointermediria2', true);

        crmForm.SetFieldReqLevel('new_periododepagamentointermediaria2', true);

        if (crmForm.all.new_qtdedeparcelasintermediarias2.DataValue != null && crmForm.all.new_valordaparcelaintermediaria2.DataValue != null)
            vltInter += crmForm.all.new_qtdedeparcelasintermediarias2.DataValue * crmForm.all.new_valordaparcelaintermediaria2.DataValue;
    }
    else {
        crmForm.SetFieldReqLevel('new_qtdedeparcelasintermediarias2', false);
        crmForm.SetFieldReqLevel('new_valordaparcelaintermediaria2', false);
        crmForm.SetFieldReqLevel('new_datadepagamentointermediria2', false);
        crmForm.SetFieldReqLevel('new_periododepagamentointermediaria2', false);
    }
    crmForm.all.new_intermediaria.DataValue = vltInter;

    /// Soma todos os valores
    crmForm.all.new_valortotalproposta.DataValue = crmForm.all.new_sinal.DataValue + vlrMensal + vltInter;
    if (crmForm.all.new_financiamento.DataValue != null)
        crmForm.all.new_valortotalproposta.DataValue += crmForm.all.new_financiamento.DataValue;

    if (crmForm.all.new_valordofgts.DataValue != null)
        crmForm.all.new_valortotalproposta.DataValue += crmForm.all.new_valordofgts.DataValue;

    if (crmForm.all.new_recurso_proprio.DataValue)
        crmForm.all.new_valortotalproposta.DataValue += crmForm.all.new_recurso_proprio.DataValue;

    /// Soma reembolso cliente na proposta
    if (crmForm.all.new_vlr_tot_mens_reemb.DataValue != null)
        crmForm.all.new_valortotalproposta.DataValue += crmForm.all.new_vlr_tot_mens_reemb.DataValue;

    if (crmForm.all.new_valortotalreembclientecart.DataValue != null)
        crmForm.all.new_valortotalproposta.DataValue += crmForm.all.new_valortotalreembclientecart.DataValue;

    // Soma parcela de desconto caso existir
    if ((crmForm.all.new_desconto_quantidade_1.DataValue != null) && (crmForm.all.new_desconto_valor_1.DataValue != null)
         && (crmForm.all.new_desconto_quantidade_2.DataValue != null) && (crmForm.all.new_desconto_valor_2.DataValue != null)) {
        var vlrParcelaDesconto1 = crmForm.all.new_desconto_valor_1.DataValue * crmForm.all.new_desconto_quantidade_1.DataValue;
        var vlrParcelaDesconto2 = crmForm.all.new_desconto_valor_2.DataValue * crmForm.all.new_desconto_quantidade_2.DataValue;
        crmForm.all.new_valortotalproposta.DataValue += vlrParcelaDesconto1 + vlrParcelaDesconto2;
    }
        
    crmForm.all.new_valortotalproposta.ForceSubmit = true;

    /// Calcula o valor total com assessoria.
    if (crmForm.all.new_valortotalproposta.DataValue != null) {
        crmForm.all.new_valor_total_assessoria.DataValue = vlrAss + crmForm.all.new_valortotalproposta.DataValue;
        crmForm.all.new_valor_total_assessoria.ForceSubmit = true;
    }
    /// Soma todos os valores de parcelamento de vendas de serviço na venda casada   
    var vlrTotalServicos =
        crmForm.all.new_new_vlr_tot_mens_money.DataValue +
        crmForm.all.new_vlr_tx.DataValue +
        crmForm.all.new_vlr_tx_vs.DataValue +
        crmForm.all.new_vl_tot_mes_seg.DataValue +
        crmForm.all.new_vlr_tot_mes_cs_garant.DataValue;

    crmForm.all.new_valortotalservicos.DataValue = vlrTotalServicos;
    crmForm.all.new_valortotalservicos.ForceSubmit = true;
    crmForm.all.new_valortotalgeral.DataValue = crmForm.all.new_valortotalproposta.DataValue + vlrTotalServicos;
    crmForm.all.new_valortotalgeral.ForceSubmit = true;
}
