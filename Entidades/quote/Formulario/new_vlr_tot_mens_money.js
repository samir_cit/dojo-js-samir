﻿if (crmForm.all.new_tipodecotacao.DataValue == TipoDeProposta.SERVICO || crmForm.all.new_tipodecotacao.DataValue == TipoDeProposta.UNIDADE)
{
    var vlrMensalDetProp = 0;
    
    if (crmForm.all.new_qtde_parc_s1.DataValue != null && crmForm.all.new_vlr_parc_mens_1.DataValue != null)
        vlrMensalDetProp += crmForm.all.new_qtde_parc_s1.DataValue * crmForm.all.new_vlr_parc_mens_1.DataValue;
    
    if (crmForm.all.new_qtde_parc_s2.DataValue != null && crmForm.all.new_vlr_parc_mens_2.DataValue != null)
        vlrMensalDetProp += crmForm.all.new_qtde_parc_s2.DataValue * crmForm.all.new_vlr_parc_mens_2.DataValue;
    
    if (crmForm.all.new_qtde_parc_s3.DataValue != null && crmForm.all.new_vlr_parc_mens_3.DataValue != null)
        vlrMensalDetProp += crmForm.all.new_qtde_parc_s3.DataValue * crmForm.all.new_vlr_parc_mens_3.DataValue;
    
    /// Calcula o valor total mensal
    crmForm.all.new_new_vlr_tot_mens_money.DataValue = vlrMensalDetProp;
    
    /// Calcula Valor Total da Proposta
    new_valortotalproposta_onchange0();
}