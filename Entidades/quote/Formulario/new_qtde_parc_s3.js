﻿//Calcula valor total da seção mensal da guia Detalhes da Proposta Kit/Garagem...
new_new_vlr_tot_mens_money_onchange0();
if (crmForm.all.new_qtde_parc_s3.DataValue != null && crmForm.all.new_qtde_parc_s3.DataValue > 0) {
    crmForm.SetFieldReqLevel("new_vlr_parc_mens_3", true);
    crmForm.SetFieldReqLevel("new_dt_vencto_parc_3", true);
    crmForm.SetFieldReqLevel("new_tipo_pagto_mensal_3", true);
}
else {
    crmForm.SetFieldReqLevel("new_vlr_parc_mens_3", false);
    crmForm.SetFieldReqLevel("new_dt_vencto_parc_3", false);
    crmForm.SetFieldReqLevel("new_tipo_pagto_mensal_3", false);
}