﻿formularioValido = true;
try {
    //Enumeração de Patrimônio de Afetação
    TipoPatrimonioAfetacao =
	{
	    Sim: 1,
	    Nao: 2,
	    NaoInformado: 3
	}

    TipoStatusCartorio =
	{
	    Protocolado: 1,
	    Deferido: 2,
	    EmAnalise: 3,
	    AtendendoExigencias: 4,
	    Indeferido: 5,
	    EmContratacao: 6
	}

    TipoEmpreendimento =
	{
	    Parque: 1,
	    Village: 2,
	    Spazio: 3
	}

    TiposContato = {
        ENGENHEIRO: 4,
        COORDENADOR: 5,
        DIRETOR_GESTOR_OBRA: 6
    }

    TipoAuditoria =
        {
            PROPOSTA: 1,
            OPORTUNIDADE_RENEGOCIACAO: 2,
            PROPOSTA_CSG: 3,
            EMPREENDIMENTO: 4,
            RETIFICACAO_RI: 5,
            OPORTUNIDADE: 6
        }

    //As datas de vigência da renovação do alvará de execução só podem ser preenchidas se o alvará de aprovação não for único.
    crmForm.all.new_data_inicio_renovacao_alvara_execucao.Disabled = crmForm.all.new_data_fim_renovacao_alvara_execucao.Disabled =
    crmForm.all.new_alvarunico.DataValue;


    crmForm.EscondeCampo = function (nomeCampo, esconde) {
        var tipo = "";
        if (esconde)
            tipo = "none";

        var atributo_c = eval("crmForm.all." + nomeCampo + "_c");
        var atributo_d = eval("crmForm.all." + nomeCampo + "_d");
        atributo_c.style.display = tipo;
        atributo_d.style.display = tipo;
    }

    crmForm.EscondeTodaSecao = function (primeiroObjetoTr, valor) {
        document.getElementById(primeiroObjetoTr + '_c').parentElement.style.visibility = (valor ? 'hidden' : 'visible');
        document.getElementById(primeiroObjetoTr + '_c').parentElement.style.display = (valor ? 'none' : 'block');
    }

    //calculo dias da vigência da proposta
    crmForm.CalculaDiasVigencia = function () {
        var dias = 0;
        if (crmForm.all.new_dias_solicitar_aprovacao.DataValue != null)
            dias += parseInt(crmForm.all.new_dias_solicitar_aprovacao.DataValue);

        if (crmForm.all.new_dias_negociacao.DataValue != null)
            dias += parseInt(crmForm.all.new_dias_negociacao.DataValue);

        if (crmForm.all.new_dias_pre_ativar_contrato.DataValue != null)
            dias += parseInt(crmForm.all.new_dias_pre_ativar_contrato.DataValue);

        crmForm.all.new_dias_vigencia_proposta.DataValue = dias;
    }



    //Adiciona Script para ser usado no click do botão
    var elm = document.createElement("script");
    elm.src = "/_static/_grid/cmds/util.js";
    document.appendChild(elm);

    //Desabilita campo Nome caso contenha Historico
    if (crmForm.all.new_nome.DataValue != null) {
        if (crmForm.all.new_nome.DataValue.indexOf("HISTÓRICO DE CARGA") >= 0) {
            var nomeEmpreendimento = document.getElementById("nav_new_new_empreendimento_new_nome_empreendiment");
            if (nomeEmpreendimento != null)
                nomeEmpreendimento.style.display = "none";
        }
    }


    //GUIA 01===EMPREENDIMENTO=================================

    //SEO lanamento---------------------------------------------------------------------------------//
    if (crmForm.all.new_previsodelanamento.DataValue != null) {
        var datalanc = crmForm.all.new_previsodelanamento.DataValue;

        datalanc.setDate(datalanc.getDate() + 120);
        crmForm.all.new_dtprevistaalvaraplantaovendas.DataValue = datalanc;
    }

    if (crmForm.all.new_statuslancamento.DataValue == 1)
        crmForm.EscondeCampo("new_statusempreendimento", false);
    else {
        crmForm.EscondeCampo("new_statusempreendimento", true);
        crmForm.all.new_statusempreendimento.DataValue = null;
    }
    //Data aquisio do terreno + 150 dias = data ideal p/ lanamento----------------------------//
    if (crmForm.all.new_dataaquisiodoterreno.DataValue != null) {
        var datalanc = crmForm.all.new_dataaquisiodoterreno.DataValue;

        datalanc.setDate(datalanc.getDate() + 150);
        crmForm.all.new_dataideal.DataValue = datalanc;
    }

    //Data de lanamento resultado em meses - Somar 2 datas resultado = meses------------//
    if ((crmForm.all.new_dataaquisiodoterreno.DataValue != null) && (crmForm.all.new_previsodelanamento.DataValue != null)) {
        var DataAux1 = crmForm.all.new_previsodelanamento.DataValue;
        var DataAux2 = crmForm.all.new_dataaquisiodoterreno.DataValue;

        crmForm.all.new_prazoemmesesaquisterrenoxprvlana.DataValue = (DataAux1.setDate(DataAux1.getDate()) - DataAux2.setDate(DataAux2.getDate())) / 2592000000;
    }

    //Data 06/02/2009
    //Adicionado para inclusão do novo field Região de Vendas.
    crmForm.all.new_regiao_de_vendas_id.Disabled = true;

    // Inicializa atributo Tabelão como 'Não Definido'
    if (crmForm.all.new_tabelao_definido.DataValue == null)
        crmForm.all.new_tabelao_definido.DataValue = false;

    //Desabilitar os campos de lançamento do empreendimento
    var cmd = new RemoteCommand("MrvService", "UserContainsTeams", "/MRVCustomizations/");
    cmd.SetParameter("teams", "Coordenação de Vendas - Lançamento");
    var result = cmd.Execute();
    if (result.Success) {
        if (!result.ReturnValue.Mrv.Found) {
            crmForm.all.new_previsodelanamento.Disabled = true;
            crmForm.all.new_dat_prev_lancamento_atualizada.Disabled = true;
            crmForm.all.new_tabelao_definido.Disabled = true;
            crmForm.all.new_obs_lancamento.Disabled = true;
        }
        else {
            crmForm.all.new_previsodelanamento.Disabled = false;
            crmForm.all.new_dat_prev_lancamento_atualizada.Disabled = false;
            crmForm.all.new_tabelao_definido.Disabled = false;
            crmForm.all.new_obs_lancamento.Disabled = false;
        }
    }



    //GUIA - 05 ===APROVAO DE PROJETO / ALARA======================//
    //Guia 05 seo 01 olcutar seo Alvara plantao de vendas-----------------------------------//
    if (crmForm.all.new_possuialvaraplantaovendas.DataValue == false) {
        document.getElementById("new_statusalvaraplantaovendas_d").parentNode.parentNode.style.display = "none";
        document.getElementById("new_obs_alvaraplantaovendas_d").parentNode.parentNode.style.display = "none";
    }
    else {
        document.getElementById("new_statusalvaraplantaovendas_d").parentNode.parentNode.style.display = "inline";
        document.getElementById("new_obs_alvaraplantaovendas_d").parentNode.parentNode.style.display = "inline";
    }
    //Guia 05  REAPROVAO 01---------------------------------------------------------------------//
    if (crmForm.all.new_reaprovao.DataValue == false) {
        document.getElementById("new_motivo_d").parentNode.parentNode.style.display = "none";
        document.getElementById("new_possuialvar_d").parentNode.parentNode.style.display = "none";
        document.getElementById("new_2reaprovao_d").parentNode.parentNode.style.display = "none";
    }
    else {
        document.getElementById("new_motivo_d").parentNode.parentNode.style.display = "inline";
        document.getElementById("new_possuialvar_d").parentNode.parentNode.style.display = "inline";
        document.getElementById("new_2reaprovao_d").parentNode.parentNode.style.display = "inline";
    }
    //Guia 05  REAPROVAO 02---------------------------------------------------------------------//
    if (crmForm.all.new_2reaprovao.DataValue == false) {
        document.getElementById("new_motivo02_d").parentNode.parentNode.style.display = "none";
        document.getElementById("new_possuialvarreap2_d").parentNode.parentNode.style.display = "none";
        document.getElementById("new_3reaprovaoreap3_d").parentNode.parentNode.style.display = "none";
    }
    else {
        document.getElementById("new_motivo02_d").parentNode.parentNode.style.display = "inline";
        document.getElementById("new_possuialvarreap2_d").parentNode.parentNode.style.display = "inline";
        document.getElementById("new_3reaprovaoreap3_d").parentNode.parentNode.style.display = "inline";
    }
    //Guia 05  REAPROVAO 03---------------------------------------------------------------------//
    if (crmForm.all.new_3reaprovaoreap3.DataValue == false) {
        document.getElementById("new_motivo03_d").parentNode.parentNode.style.display = "none";
        document.getElementById("new_possuialvarreap3_d").parentNode.parentNode.style.display = "none";
        document.getElementById("new_4reaprovaoreap4_d").parentNode.parentNode.style.display = "none";
    }
    else {
        document.getElementById("new_motivo03_d").parentNode.parentNode.style.display = "inline";
        document.getElementById("new_possuialvarreap3_d").parentNode.parentNode.style.display = "inline";
        document.getElementById("new_4reaprovaoreap4_d").parentNode.parentNode.style.display = "inline";
    }
    //Guia 05  REAPROVAO 04----------------------------------------------------------------------//
    if (crmForm.all.new_4reaprovaoreap4.DataValue == false) {
        document.getElementById("new_motivo04_d").parentNode.parentNode.style.display = "none";
        document.getElementById("new_possuialvarreap4_d").parentNode.parentNode.style.display = "none";
        document.getElementById("new_5reaprovaoreap5_d").parentNode.parentNode.style.display = "none";
    }
    else {
        document.getElementById("new_motivo04_d").parentNode.parentNode.style.display = "inline";
        document.getElementById("new_possuialvarreap4_d").parentNode.parentNode.style.display = "inline";
        document.getElementById("new_5reaprovaoreap5_d").parentNode.parentNode.style.display = "inline";
    }
    //Guia 05  REAPROVAO 05-----------------------------------------------------------------------//
    if (crmForm.all.new_5reaprovaoreap5.DataValue == false) {
        document.getElementById("new_motivo05_d").parentNode.parentNode.style.display = "none";
        document.getElementById("new_possuialvarreap5_d").parentNode.parentNode.style.display = "none";
    }
    else {
        document.getElementById("new_motivo05_d").parentNode.parentNode.style.display = "inline";
        document.getElementById("new_possuialvarreap5_d").parentNode.parentNode.style.display = "inline";
    }

    //GUIA - 06===INCORPORAO==================================//

    //SEO - 01 ---------ESCRITURA--------------------------------------------------------------------//
    if (crmForm.all.new_possuiescritura.DataValue != null && crmForm.all.new_possuiescritura.DataValue == true) {
        crmForm.all.new_datamapeamentoprevisaocartorio_c.style.visibility = 'hidden';
        crmForm.all.new_datamapeamentoprevisaocartorio_d.style.visibility = 'hidden';
        crmForm.all.new_datamapeamentoescritura_c.style.visibility = 'visible';
        crmForm.all.new_datamapeamentoescritura_d.style.visibility = 'visible';
    }
    else {
        crmForm.all.new_datamapeamentoprevisaocartorio_c.style.visibility = 'visible';
        crmForm.all.new_datamapeamentoprevisaocartorio_d.style.visibility = 'visible';
        crmForm.all.new_datamapeamentoescritura_c.style.visibility = 'hidden';
        crmForm.all.new_datamapeamentoescritura_d.style.visibility = 'hidden';
    }


    //SEO - 04 ====habilitar campos----------------------------------///
    if (crmForm.all.new_elaboraonbconcluida.DataValue != null && crmForm.all.new_elaboraonbconcluida.DataValue == true) {
        crmForm.all.new_datadeprevisoinc1_c.style.visibility = 'hidden';
        crmForm.all.new_datadeprevisoinc1_d.style.visibility = 'hidden';

        crmForm.all.new_dataconclusodanb_c.style.visibility = 'visible';
        crmForm.all.new_dataconclusodanb_d.style.visibility = 'visible';
    }
    else {
        crmForm.all.new_datadeprevisoinc1_c.style.visibility = 'visible';
        crmForm.all.new_datadeprevisoinc1_d.style.visibility = 'visible';

        crmForm.all.new_dataconclusodanb_c.style.visibility = 'hidden';
        crmForm.all.new_dataconclusodanb_d.style.visibility = 'hidden';
    }


    //SEO - 04 ====habilitar campos--------------------------------------------------------------//
    if (crmForm.all.new_elaboraodememorialconveno.DataValue != null && crmForm.all.new_elaboraodememorialconveno.DataValue == true) {
        crmForm.all.new_datadeprevisoinc2_c.style.visibility = 'hidden';
        crmForm.all.new_datadeprevisoinc2_d.style.visibility = 'hidden';

        crmForm.all.new_dataconclusodomemorial_c.style.visibility = 'visible';
        crmForm.all.new_dataconclusodomemorial_d.style.visibility = 'visible';
    }
    else {
        crmForm.all.new_datadeprevisoinc2_c.style.visibility = 'visible';
        crmForm.all.new_datadeprevisoinc2_d.style.visibility = 'visible';

        crmForm.all.new_dataconclusodomemorial_c.style.visibility = 'hidden';
        crmForm.all.new_dataconclusodomemorial_d.style.visibility = 'hidden';
    }
    //SEO - 04 ====habilitar campos-------------------------------------------------------------//
    if (crmForm.all.new_elaboraomemorialdecritivo.DataValue != null && crmForm.all.new_elaboraomemorialdecritivo.DataValue == true) {
        crmForm.all.new_datadeprevisoinc3_c.style.visibility = 'hidden';
        crmForm.all.new_datadeprevisoinc3_d.style.visibility = 'hidden';

        crmForm.all.new_dataconclusodaminuta_c.style.visibility = 'visible';
        crmForm.all.new_dataconclusodaminuta_d.style.visibility = 'visible';
    }
    else {
        crmForm.all.new_datadeprevisoinc3_c.style.visibility = 'visible';
        crmForm.all.new_datadeprevisoinc3_d.style.visibility = 'visible';

        crmForm.all.new_dataconclusodaminuta_c.style.visibility = 'hidden';
        crmForm.all.new_dataconclusodaminuta_d.style.visibility = 'hidden';
    }

    //SEO - 04 ====habilitar campos-------------------------------------------------------------//
    if (crmForm.all.new_possuiatestadodeindonidade.DataValue != null && crmForm.all.new_possuiatestadodeindonidade.DataValue == true) {
        crmForm.all.new_datadeprevisoinc4_c.style.visibility = 'hidden';
        crmForm.all.new_datadeprevisoinc4_d.style.visibility = 'hidden';

        crmForm.all.new_dataconclusoatestadodeindonidade_c.style.visibility = 'visible';
        crmForm.all.new_dataconclusoatestadodeindonidade_d.style.visibility = 'visible';
    }
    else {
        crmForm.all.new_datadeprevisoinc4_c.style.visibility = 'visible';
        crmForm.all.new_datadeprevisoinc4_d.style.visibility = 'visible';

        crmForm.all.new_dataconclusoatestadodeindonidade_c.style.visibility = 'hidden';
        crmForm.all.new_dataconclusoatestadodeindonidade_d.style.visibility = 'hidden';
    }
    //SEO - 04 desaparecer data do status da certido-----------------------------------------//
    if (crmForm.all.new_statusdascertidesinc.DataValue == null) {
        crmForm.all.new_datadeprevisoinc5_c.style.visibility = 'hidden';
        crmForm.all.new_datadeprevisoinc5_d.style.visibility = 'hidden';
    }
    //SEAO - 04 calculo data prevista de RI-------------------------------------------------------//
    if (crmForm.all.new_datadeprevisoinc5.DataValue != null && crmForm.all.new_dataaquisiodoterreno.DataValue != null && crmForm.all.new_dataprevistaconclusodori.DataValue != null && crmForm.all.new_dataaquisiodoterreno.DataValue != null) {
        crmForm.SetFieldReqLevel("new_datadeprevisoinc5", false);
        crmForm.all.new_datadeprevisoinc5_c.style.visibility = 'visible';
        crmForm.all.new_datadeprevisoinc5_d.style.visibility = 'visible';

        var data_aux = crmForm.all.new_datadeprevisoinc5.DataValue;

        data_aux.setDate(data_aux.getDate() + 5);
        crmForm.all.new_dataprevistaentradanocartrio.DataValue = data_aux;

        data_aux.setDate(data_aux.getDate() + 30);
        crmForm.all.new_dataprevistaconclusodori.DataValue = data_aux;

        //SEO - 04 ====Somar 2 datas resultado = meses----------------------------------//
        var DataAux1 = crmForm.all.new_dataprevistaconclusodori.DataValue;
        var DataAux2 = crmForm.all.new_dataaquisiodoterreno.DataValue;

        crmForm.all.new_prazoemmeseasscontratoxprevri.DataValue = (DataAux1.setDate(DataAux1.getDate()) - DataAux2.setDate(DataAux2.getDate())) / 2592000000;
    }
    else {
        if (crmForm.all.new_dataconclusodori.DataValue != null) {
            crmForm.SetFieldReqLevel("new_datadeprevisoinc5", false);
            crmForm.all.new_datadeprevisoinc5_c.style.visibility = 'hidden';
            crmForm.all.new_datadeprevisoinc5_d.style.visibility = 'hidden';

            //SEO - 04 ====Somar 2 datas resultado = meses--------------------------------//
            if (crmForm.all.new_dataaquisiodoterreno.DataValue != null && crmForm.all.new_dataconclusodori.DataValue != null) {
                var DataAux2 = crmForm.all.new_dataaquisiodoterreno.DataValue;
                var DataAux3 = crmForm.all.new_dataconclusodori.DataValue;
                crmForm.all.new_prazoemmesesasscontratoxconcluso.DataValue = (DataAux3.setDate(DataAux3.getDate()) - DataAux2.setDate(DataAux2.getDate())) / 2592000000;
            }
        }
    }
    //GUIA 07===FINAC. BANCARIO=================================//
    //Guia 01 - Seao - Tipo de finac. - PIP------------------------------------------------------------//
    if (crmForm.all.new_planoempresrio.DataValue == false) {
        document.getElementById("new_statuspip_d").parentNode.parentNode.style.display = "none";
    }
    else {
        document.getElementById("new_statuspip_d").parentNode.parentNode.style.display = "inline";
    }
    //Guia 01 - Seao - Tipo de finac. - CEF----------------------------------------------------------//
    if (crmForm.all.new_caixaeconmicafederalfb.DataValue == false) {
        document.getElementById("new_statuspranlisefb_d").parentNode.parentNode.style.display = "none";
        document.getElementById("new_statusanliseana_d").parentNode.parentNode.style.display = "none";
        document.getElementById("new_statusdolaudola_d").parentNode.parentNode.style.display = "none";
        document.getElementById("new_statuscontrataoco_d").parentNode.parentNode.style.display = "none";
        document.getElementById("new_rico_d").parentNode.parentNode.style.display = "none";
    }
    else {
        document.getElementById("new_statuspranlisefb_d").parentNode.parentNode.style.display = "inline";
        document.getElementById("new_statusanliseana_d").parentNode.parentNode.style.display = "inline";
        document.getElementById("new_statusdolaudola_d").parentNode.parentNode.style.display = "inline";
        document.getElementById("new_statuscontrataoco_d").parentNode.parentNode.style.display = "inline";
        document.getElementById("new_rico_d").parentNode.parentNode.style.display = "inline";
    }

    //Campo 01  + dias no Campo  02 ---------------PREVISTA----------------------------------//
    if (crmForm.all.new_previsodelanamento.DataValue != null) {
        var datalanc = crmForm.all.new_previsodelanamento.DataValue;

        datalanc.setDate(datalanc.getDate() + 150);
        crmForm.all.new_dataprevista5mesesaposolanamento.DataValue = datalanc;
    }

    //Campo 01  + dias no Campo  02 -------------------------------------------------------------------//
    if (crmForm.all.new_dataprevistaconclusodori.DataValue != null) {
        var datalanc = crmForm.all.new_dataprevistaconclusodori.DataValue;

        datalanc.setDate(datalanc.getDate() + 60);
        crmForm.all.new_dataprevista2mesesaposori.DataValue = datalanc;
    }
    //Date - Data maior campo1 campo2 resultado no campo3--------------------------------------//

    if (crmForm.all.new_dataprevista5mesesaposolanamento.DataValue >= crmForm.all.new_dataprevista2mesesaposori.DataValue) {
        crmForm.all.new_datamaiorlanceri.DataValue = crmForm.all.new_dataprevista5mesesaposolanamento.DataValue;
    }
    else {
        crmForm.all.new_datamaiorlanceri.DataValue = crmForm.all.new_dataprevista2mesesaposori.DataValue;
    }

    //Campo 01  + dias no Campo  02 -----------------REAL-------------------------------------//
    if (crmForm.all.new_datarealdelanamento.DataValue != null) {
        var datalanc = crmForm.all.new_datarealdelanamento.DataValue;

        datalanc.setDate(datalanc.getDate() + 150);
        crmForm.all.new_datarealdelanamento5meses.DataValue = datalanc;
    }

    //Campo 01  + dias no Campo  02 --------------------------------------------------//
    if (crmForm.all.new_dataconclusodori.DataValue != null) {
        var datalanc = crmForm.all.new_dataconclusodori.DataValue;

        datalanc.setDate(datalanc.getDate() + 60);
        crmForm.all.new_datarealdeconcluso2mesesri.DataValue = datalanc;
    }
    //Date - Data maior campo1 campo2 ecampo 3 resultado no campo4--------------------------------------------------------------------//
    if ((crmForm.all.new_dataprevista5mesesaposolanamento.DataValue >= crmForm.all.new_dataprevista2mesesaposori.DataValue) &&
   (crmForm.all.new_dataprevista5mesesaposolanamento.DataValue >= crmForm.all.new_dtprev5mesesaposlibcalcestrutura.DataValue)) {
        crmForm.all.new_datamaiorlanceri.DataValue = crmForm.all.new_dataprevista5mesesaposolanamento.DataValue;
    }
    else {
        if ((crmForm.all.new_dataprevista2mesesaposori.DataValue >= crmForm.all.new_dataprevista5mesesaposolanamento.DataValue) &&
     (crmForm.all.new_dataprevista2mesesaposori.DataValue >= crmForm.all.new_dtprev5mesesaposlibcalcestrutura.DataValue)) {
            crmForm.all.new_datamaiorlanceri.DataValue = crmForm.all.new_dataprevista2mesesaposori.DataValue;
        }
        else {
            crmForm.all.new_datamaiorlanceri.DataValue = crmForm.all.new_dtprev5mesesaposlibcalcestrutura.DataValue;
        }
    }

    //Date - Data maior real campo1 campo2 ecampo 3 resultado no campo4--------------------------------------------------------------//
    if ((crmForm.all.new_datarealdelanamento5meses.DataValue >= crmForm.all.new_datarealdeconcluso2mesesri.DataValue) &&
   (crmForm.all.new_datarealdelanamento5meses.DataValue >= crmForm.all.new_dtreal5mesesaposlibcalcestrutura.DataValue)) {
        crmForm.all.new_datadacontrataofb.DataValue = crmForm.all.new_datarealdelanamento5meses.DataValue;
    }
    else {
        if ((crmForm.all.new_datarealdeconcluso2mesesri.DataValue >= crmForm.all.new_datarealdelanamento5meses.DataValue) &&
     (crmForm.all.new_datarealdeconcluso2mesesri.DataValue >= crmForm.all.new_dtreal5mesesaposlibcalcestrutura.DataValue)) {
            crmForm.all.new_datadacontrataofb.DataValue = crmForm.all.new_datarealdeconcluso2mesesri.DataValue;
        }
        else {
            crmForm.all.new_datadacontrataofb.DataValue = crmForm.all.new_dtreal5mesesaposlibcalcestrutura.DataValue;
        }
    }

    // === OCULTAR  CAMPOS======================================//
    //SOMENTE LEITURA=========================================//
    crmForm.all.new_dataaquisiodoterreno.Disabled = true;
    crmForm.all.new_dataprevista5mesesaposolanamento.Disabled = true;
    crmForm.all.new_dataprevista2mesesaposori.Disabled = true;
    crmForm.all.new_datamaiorlanceri.Disabled = true;
    crmForm.all.new_dt_previa_contratacao_pe_pip.Disabled = true;
    crmForm.all.new_dt_prevista_contratacao_pe_pip.Disabled = true;
    crmForm.all.new_datarealdelanamento5meses.Disabled = true;
    crmForm.all.new_datarealdeconcluso2mesesri.Disabled = true;
    crmForm.all.new_datadacontrataofb.Disabled = true;

    crmForm.all.new_dtprev5mesesaposlibcalcestrutura.Disabled = true;
    crmForm.all.new_dtreal5mesesaposlibcalcestrutura.Disabled = true;

    //GUIA 05 Alavra - somente leitura campo de VIGENCIAS---------------------------------//
    crmForm.all.new_dtinivigenciaalvaraplantaovendas.Disabled = true;
    crmForm.all.new_dtfimvigenciaalvaraplantaovendas.Disabled = true;
    crmForm.all.new_datainiciodavigncia.Disabled = true;
    crmForm.all.new_datafinaldavigncia.Disabled = true;
    crmForm.all.new_datainciodavignciaexecucao.Disabled = true;
    crmForm.all.new_datafinaldavignciaexecucao.Disabled = true;
    crmForm.all.new_datainciovignciareap.Disabled = true;
    crmForm.all.new_datafinalvignciareap.Disabled = true;
    crmForm.all.new_datainciovignciareap2.Disabled = true;
    crmForm.all.new_datafinalvignciareap2.Disabled = true;
    crmForm.all.new_datainciovignciareap3.Disabled = true;
    crmForm.all.new_datafinalvignciareap3.Disabled = true;
    crmForm.all.new_datainciovignciareap4.Disabled = true;
    crmForm.all.new_datafinalvignciareap4.Disabled = true;
    crmForm.all.new_datainciovignciareap5.Disabled = true;
    crmForm.all.new_datafinalvignciareap5.Disabled = true;

    //DESABILITAR DO FORMULARIO=============================//

    crmForm.all.new_datamapeamentomaior_c.style.visibility = 'hidden';
    crmForm.all.new_datamapeamentomaior_d.style.visibility = 'hidden';


    //aparecer ou desaparecer sessão de pendencia do PIP-------------------------------------//
    if ((crmForm.all.new_statuspip.SelectedText == "Pendência") || (crmForm.all.new_statuspip.SelectedText == null)) {
        document.getElementById("new_dataenviodapendnciapip_d").parentNode.parentNode.style.display = "inline";
    }
    else {
        document.getElementById("new_dataenviodapendnciapip_d").parentNode.parentNode.style.display = "none";
    }

    //aparecer ou desaparecer campo seção de pendência CEF-------------------------------------//
    if ((crmForm.all.new_statusanliseana.SelectedText == "Pendência") || (crmForm.all.new_statuscontrataoco.SelectedText == "Pendência") || (crmForm.all.new_statusdolaudola.SelectedText == "Pendência")) {
        document.getElementById("new_dataenviodapendnciaanalise_d").parentNode.parentNode.style.display = "inline";
    }
    else {
        document.getElementById("new_dataenviodapendnciaanalise_d").parentNode.parentNode.style.display = "none";
    }

    //colocar o valor da data de dias da proposta com valor igual a 10 somente na criação-----------//
    if (crmForm.FormType == 1)
        crmForm.all.new_dias_vigencia_proposta.DataValue = 10;

    //Bloquear o campo regularização de Documentos se não for criação
    if (crmForm.FormType != 1)
        crmForm.all.new_regularizaoid.Disabled = true;

    //Chama o evento onchage do new_nome
    crmForm.all.new_nome.FireOnChange();

    //Verificar se ao fazer onload, o campo está do empreendimento está vazio-----------------------//
    if (crmForm.all.new_nome.DataValue == null) {
        crmForm.all.new_nome.Disabled = false;
    }

    //Scripts ART ------------------------------------------------------------------------------------------//
    crmForm.all.new_statusart.FireOnChange();

    //Evento de alteração do campo previsão 2 meses do RI----------------------------------------//

    // Calculo de 40 dias após 2 meses de precisão do RI-------------------------------------------------------//
    new_dataprevista2mesesaposori_onchange0();
    new_datarealdeconcluso2mesesri_onchange0();

    //Função para verificar equipe
    crmForm.UsuarioPertenceEquipe = function (nomeEquipe) {
        var cmd = new RemoteCommand("MrvService", "UserContainsTeams", "/MRVCustomizations/");
        cmd.SetParameter("teams", nomeEquipe);
        var resultado = cmd.Execute();
        if (resultado.Success) {
            return resultado.ReturnValue.Mrv.Found;
        } else {
            return false;
        }
    }

    //452495 - Bloqueio Informações Bancárias --
    crmForm.all.new_banco_empreendimentoid.Disabled = !crmForm.UsuarioPertenceEquipe("Alterações de Informações Bancárias");
    crmForm.all.new_agencia.Disabled = crmForm.all.new_banco_empreendimentoid.Disabled;
    crmForm.all.new_conta.Disabled = crmForm.all.new_banco_empreendimentoid.Disabled;

    crmForm.RemoveBotao = function (nome) {
        var lista = document.getElementsByTagName("LI");
        for (i = 0; i < lista.length; i++) {
            if (lista[i].id.indexOf(nome) >= 0) {
                var o = lista[i].parentElement;
                o.removeChild(lista[i]);
                break;
            }
        }
    }

    //Variável global
    UsuarioAdministrador = null;

    if (UsuarioAdministrador == null)
        UsuarioAdministrador = crmForm.UsuarioPertenceEquipe("Administradores");

    if (!UsuarioAdministrador) {
        //Esconde botões
        crmForm.RemoveBotao("EnviarMateriais");
        crmForm.RemoveBotao("EnviarBlocosPS");
    }

    //Armazenando o valor new_quantidade_fila
    quantidadeFila = null;
    if (crmForm.all.new_quantidade_fila.DataValue != null) {
        quantidadeFila = crmForm.all.new_quantidade_fila.DataValue;
    }

    crmForm.ValidarTipoDeFinanciamento = function (campo) {
        var planos = [crmForm.all.new_planoempresrio, crmForm.all.new_caixaeconmicafederalfb, crmForm.all.new_flex, crmForm.all.new_finaciamentobancrio];

        for (var i = 0; i < planos.length; i++)
            planos[i].DataValue = planos[i].id == campo;
    }

    crmForm.ObterPermissaoDeCriacaoDaRetificacaoDeferidaParaEmpreendimento = function () {

        var retorno = false;
        var cmd = new RemoteCommand("MrvService", "ObterPermissaoDeCriacaoDaRetificacaoDeferidaParaEmpreendimento", "/MRVCustomizations/");
        cmd.SetParameter("empreendimentoId", crmForm.ObjectId);
        var result = cmd.Execute();
        if (result.Success) {

            var retorno = result.ReturnValue.Mrv.Resultado;

            if (retorno || crmForm.all.new_statusdocartrio.DataValue == TipoStatusCartorio.Deferido) //permite inserção
                retorno = true;
            else {
                retorno = false;
                alert('Somente será possível criar uma retificação se o campo Status do Cartório estiver marcado como Deferido.')
            }
        }
        return retorno;
    }
    var cliqueDuplo = false;
    if (document.getElementById('nav_new_empreendimento_retificacao_de_ri') != null) {
        document.getElementById('nav_new_empreendimento_retificacao_de_ri').onclick = function () {

            try {

                if (!cliqueDuplo)
                    cliqueDuplo = true;
                else {
                    cliqueDuplo = false;
                    return;
                }

                var retorno = crmForm.ObterPermissaoDeCriacaoDaRetificacaoDeferidaParaEmpreendimento();

                if (retorno)
                    loadArea('new_empreendimento_retificacao_de_ri'); //script padrão.
            }
            catch (e) {
                alert('Erro no método de retificação. ' + e.description);
            }
        };
    }

    crmForm.ConfiguraAcessoSequenciaDeObra = function () {
        var perfilEmpreendimentoSequenciaDeObra = false; //crmForm.UsuarioPertenceEquipe("Empreendimento - Sequência de Obra")
        var perfilEngenhariaSequenciaDeObra = crmForm.UsuarioPertenceEquipe("Engenharia - Sequência de Obra")
        var perfilEngenhariaPrazos = crmForm.UsuarioPertenceEquipe("Engenharia - Prazos");

        var ativaSequenciaDeObra = (perfilEmpreendimentoSequenciaDeObra || perfilEngenhariaSequenciaDeObra || perfilEngenhariaPrazos);

        crmForm.all.new_sequencia_obras.Disabled = !ativaSequenciaDeObra;
    }

    crmForm.EscondeSecaoERequerido = function (campo, trava) {
        crmForm.EscondeTodaSecao(campo, !trava);
        //crmForm.SetFieldReqLevel(campo, trava);
        if (!trava) //limpa o campo quando esconder o campo
            eval("crmForm.all." + campo + ".DataValue = null;")
    }
    crmForm.FocoNoCampo = function (campo) {
    }

    crmForm.HabilitaCamposModulo = function (total, habilitaCamposModulo, onload) {
        for (x = 1; x <= total; x++) {
            if (onload)
                habilitaCamposModulo = eval("crmForm.all.new_modulo" + x + ".DataValue != null")

            crmForm.EscondeSecaoERequerido("new_modulo" + x, habilitaCamposModulo);
        }
    }

    crmForm.ConfiguraPerfilEngenhariaPrazos = function () {

        var perfilEngenhariaPrazos = crmForm.UsuarioPertenceEquipe("Engenharia - Prazos");

        //habilita/desabilita campos do perfil de engenharia de prazos
        crmForm.ConfiguraAcessoSequenciaDeObra();
        crmForm.all.new_gestor_diretor_obra_contatoid.Disabled = !perfilEngenhariaPrazos;
        for (x = 1; x <= 6; x++)
            eval("crmForm.all.new_modulo" + x + ".Disabled = " + !perfilEngenhariaPrazos)

        crmForm.all.new_modulacao.Disabled = !perfilEngenhariaPrazos;
        crmForm.all.new_defasagem_em_meses.Disabled = !perfilEngenhariaPrazos;

        var existeSequenciaDeObras = (crmForm.all.new_sequencia_obras.DataValue != null);

        crmForm.HabilitaCamposModulo(6, undefined, true);

        if (crmForm.all.new_modulacao.DataValue != null) {
            crmForm.HabilitaCamposModulo(crmForm.all.new_modulacao.DataValue, true, false);
            crmForm.ValidaExibicaoModulacaoEDefasagem(true);
        }

        crmForm.ValidaExibicaoModulacaoEDefasagem(existeSequenciaDeObras);
    }
    //Adiciona a esquerda o caracter específico
    String.prototype.padLeft = function (length, character) {
        return new Array(length - this.length + 1).join(character || '0') + this;
    }

    crmForm.ValidaExibicaoModulacaoEDefasagem = function (valido) {

        crmForm.EscondeSecaoERequerido("new_modulacao", valido);

        if (crmForm.all.new_modulacao.DataValue == null || crmForm.all.new_modulacao.DataValue == 1) {
            crmForm.EscondeTodaSecao("new_defasagem_em_meses", true);
            crmForm.all.new_defasagem_em_meses.DataValue = null;
        }
        else
            crmForm.EscondeTodaSecao("new_defasagem_em_meses", !valido);

        crmForm.SetFieldReqLevel("new_defasagem_em_meses", false);
    }

    crmForm.ValidaSequenciaDeObras = function (campo) {

        var existeSequenciaDeObras = (crmForm.all.new_sequencia_obras.DataValue != null);

        crmForm.ValidaExibicaoModulacaoEDefasagem(existeSequenciaDeObras);

        if (!existeSequenciaDeObras) {
            //Esconde campos de módulo
            crmForm.HabilitaCamposModulo(6, false, false);
            return;
        }

        crmForm.ConfiguraCamposModulos(crmForm.all.new_sequencia_obras, false);

        //numero de blocos/módulos
        var numeroTotalDeBlocos = crmForm.all.new_nmerodeblocos.DataValue;

        //Gera blocos permitidos na inserção de sequencia de obras
        var blocosPermitidos = "";
        for (bloco = 1; bloco <= numeroTotalDeBlocos; bloco++)
            blocosPermitidos += bloco.toString().padLeft(2, '0') + ",";

        //Reparte a sequencia de obras digitada
        var sequenciaDeObras = crmForm.all.new_sequencia_obras.DataValue.split("-");

        var sequenciaInexistentes = "";
        //Compara sequencia de obras digitada com os blocos permitidos para inserção
        for (x = 0; x < sequenciaDeObras.length; x++) {
            if (blocosPermitidos.toString().indexOf(sequenciaDeObras[x]) < 0) {
                sequenciaInexistentes += sequenciaDeObras[x] + "-";
            }
        }

        //Verifica se existe um bloco informado na sequencia de obras
        if (sequenciaInexistentes != "") {
            var sequencias = sequenciaInexistentes.substring(0, sequenciaInexistentes.length - 1);

            alert("O(s) bloco(s) " + sequencias + " não existe(m) ou não foram criados.");

            crmForm.FocoNoCampo(campo);
        }

        //Verifica se a soma dos blocos informados na sequencia de obras condiz com o total de blocos/módulos
        if (sequenciaDeObras.length < numeroTotalDeBlocos ||
            sequenciaDeObras.length > numeroTotalDeBlocos) {

            alert('A quantidade inserida de blocos na sequência de obras está incorreta.');

            crmForm.FocoNoCampo(campo);
        }
    }

    //Valida se a sequencia de obras dos módulos são existentes
    crmForm.ValidaSequenciaDeObrasDosModulos = function (campo) {

        var sequenciasPorModulos = campo.DataValue.toString().split("-");
        var sequenciasDeObras = crmForm.all.new_sequencia_obras.DataValue;

        var sequenciaInexistentes = "";
        //Compara sequencia de obras do módulo X com sequencia de obras
        for (x = 0; x < sequenciasPorModulos.length; x++) {
            if (sequenciasDeObras.toString().indexOf(sequenciasPorModulos[x]) < 0) {
                sequenciaInexistentes += sequenciasPorModulos[x] + "-";
            }
        }
        //Verifica se existem sequencia de obras do módulo X que não existem na sequencia de obras
        if (sequenciaInexistentes != "") {
            var sequencias = sequenciaInexistentes.substring(0, sequenciaInexistentes.length - 1);

            alert("O(s) bloco(s) " + sequencias + " não existe(m) na Sequencia de Obras.");

            crmForm.FocoNoCampo(campo);
        }

        return true;
    }

    //configura a inserção do "-" na sequencia de obras dos módulos
    crmForm.ConfiguraCamposModulos = function (campo, validaSequencia) {

        if (campo.DataValue != null) {
            var sTmp = campo.DataValue.replace(/[^0-9]/g, "");
            var iAux = 0;
            var sNewValue = "";
            for (var index = 0; index < sTmp.length; index++) {
                if (iAux == 2) {
                    sNewValue += "-";
                    iAux = 0;
                }
                sNewValue += sTmp.charAt(index);
                iAux++;
            }
            campo.DataValue = sNewValue;
            if (validaSequencia) {
                //Valida se a sequencia de obras dos módulos são existentes
                crmForm.ValidaSequenciaDeObrasDosModulos(campo);
            }
        }
    }
    //Configura quantos módulos serão exibidos de acordo com o valor digitado na modulação
    crmForm.ConfiguraModulacoes = function () {

        var modulacaoComValor = (crmForm.all.new_modulacao.DataValue != null);

        //Esconde campos de módulo
        crmForm.HabilitaCamposModulo(6, false, false);

        if (!modulacaoComValor) {
            for (x = 1; x <= 6; x++) {
                eval("crmForm.all.new_modulo" + x + ".DataValue = null");
            }
        }

        if (modulacaoComValor && crmForm.all.new_modulacao.DataValue > 1)
            crmForm.EscondeTodaSecao("new_defasagem_em_meses", !modulacaoComValor);
        else {
            crmForm.EscondeTodaSecao("new_defasagem_em_meses", true);
            crmForm.all.new_defasagem_em_meses.DataValue = null;
        }

        if (modulacaoComValor) {

            //exibe os módulos de acordo com a modulação
            var modulosAExibir = crmForm.all.new_modulacao.DataValue;

            if (modulosAExibir > 6 || modulosAExibir == 0) {
                alert("O número escolhido no campo Modulação deverá ser de 1 a 6.");
                crmForm.FocoNoCampo();
            }

            //Exibe campos de módulo
            crmForm.HabilitaCamposModulo(modulosAExibir, true, false);

            for (x = modulosAExibir + 1; x <= 6; x++)
                eval("crmForm.all.new_modulo" + x + ".DataValue = null");

            crmForm.all.new_modulo1.focus();
        }
    }

    crmForm.VerificaObrigatoriedadeCampos = function () {
        if (crmForm.all.new_statusdocartrio.DataValue != null &&
        crmForm.all.new_statusdocartrio.DataValue == TipoStatusCartorio.Deferido) {
            crmForm.SetFieldReqLevel("new_versaoconvencaocondominio", true);
            crmForm.SetFieldReqLevel("new_nmerodori", true);
            crmForm.SetFieldReqLevel("new_cidade_cartorio_id", true);
            crmForm.SetFieldReqLevel("new_cartorio_ri", true);
            crmForm.SetFieldReqLevel("new_tipo", true);
        } else {
            crmForm.SetFieldReqLevel("new_versaoconvencaocondominio", false);
            crmForm.SetFieldReqLevel("new_nmerodori", false);
            crmForm.SetFieldReqLevel("new_cidade_cartorio_id", false);
            crmForm.SetFieldReqLevel("new_cartorio_ri", false);
            crmForm.SetFieldReqLevel("new_tipo", false);
        }
    }
    crmForm.ConfiguraLoockupCurvaVendas = function () {
        var oParam = "objectTypeCode=10257&filterDefault=false&attributesearch=new_name&_tipo=1";
        var typeCode = "10257";
        crmForm.FilterLookup(crmForm.all.new_curvadevendasid, typeCode, "/MRVWeb/FilterLookup/FilterLookup.aspx", oParam);
    }

    //Valida data aceite, datafimcronog real e data de liberação de entrega de chaves.
    //data1 = data vistoria
    //data2 = dataaceite

    crmForm.CompararDataAt = function (data1, data2) {
        if ((data2.DataValue != null) && (data2.DataValue < data1.DataValue)) {
            alert("A data informada no campo Data de Aceite da AT deve ser maior ou Igual a  data do campo 1ª Vistoria AT ASC");
            data2.DataValue = null;
            data2.SetFocus();
            event.returnValue = false;
        }
        if ((data1.DataValue == null) && (data2.DataValue != null)) {
            alert("A data informada no campo Data de Aceite da AT deve ser maior ou Igual a  data do campo 1ª Vistoria AT ASC");
            data2.DataValue = null;
            data1.SetFocus();
            event.returnValue = false;
        }
    }

    crmForm.ValidarCamposPatrimonioAfetacao = function () {
        var patrimonioAfetacao = crmForm.all.new_patrimonio_afetacao.DataValue;
        var statusCartorio = crmForm.all.new_statusdocartrio.DataValue;
        var registroIncorporacao = crmForm.all.new_dataconclusodori.DataValue;
        if (patrimonioAfetacao == null || patrimonioAfetacao == TipoPatrimonioAfetacao.NaoInformado) {
            if (statusCartorio != null && statusCartorio == TipoStatusCartorio.Deferido && registroIncorporacao != null) {
                alert('O Empreendimento foi afetado? \n\nO campo Patrimônio de Afetação deverá ser alterado para "Sim" ou "Não", sempre que o empreendimento possuir Registro de Incorporação e campo cartório estiver como deferido. \n\n     \u2022 “Sim” para quando houver afetação na averbação da matricula de RI. \n     \u2022 “Não” para quando não houver afetação na averbação da matricula de RI, sendo necessário solicitar a afetação, pois todos os novos empreendimentos deverão ser Patrimônio de Afetação.');
                event.returnValue = false;
            }
        }
        else {
            if (statusCartorio == null || statusCartorio != TipoStatusCartorio.Deferido || registroIncorporacao == null) {
                alert('O campo Patrimônio de Afetação só pode ser marcado como Sim / Não se o empreendimento possuir “Registro Incorporação” e “Status do Cartório = Deferido”.');
                event.returnValue = false;
            }
        }
    }

    crmForm.ValidarCamposPatrimonioAfetacao = function() {
        var patrimonioAfetacao = crmForm.all.new_patrimonio_afetacao.DataValue;
        var statusCartorio = crmForm.all.new_statusdocartrio.DataValue;
        var registroIncorporacao = crmForm.all.new_dataconclusodori.DataValue;
        if (patrimonioAfetacao == null || patrimonioAfetacao == TipoPatrimonioAfetacao.NaoInformado) {
            if (statusCartorio != null && statusCartorio == TipoStatusCartorio.Deferido && registroIncorporacao != null) {
                alert('O Empreendimento foi afetado? \n\nO campo Patrimônio de Afetação deverá ser alterado para "Sim" ou "Não", sempre que o empreendimento possuir Registro de Incorporação e campo cartório estiver como deferido. \n\n     \u2022 “Sim” para quando houver afetação na averbação da matricula de RI. \n     \u2022 “Não” para quando não houver afetação na averbação da matricula de RI, sendo necessário solicitar a afetação, pois todos os novos empreendimentos deverão ser Patrimônio de Afetação.');
                event.returnValue = false;
            }
        }
        else {
            if (statusCartorio == null || statusCartorio != TipoStatusCartorio.Deferido || registroIncorporacao == null) {
                alert('O campo Patrimônio de Afetação só pode ser marcado como Sim / Não se o empreendimento possuir “Registro Incorporação” e “Status do Cartório = Deferido”.');
                event.returnValue = false;
            }
        }
    }
    crmForm.DesabilitaDataPrevistaInicialRI = function() {
        if (new_dataprevistaconclusodori.DataValue != null && !UsuarioAdministrador) {
            crmForm.DesabilitarCampos("new_dataprevistaconclusodori", true);
            if ((crmForm.FormType == TypeCreate || crmForm.FormType == TypeUpdate) && crmForm.all.new_dataprevistaconclusodori.IsDirty) {
                crmForm.all.new_dataprevistaconclusodori.ForceSubmit = true;
            }
        }
    }

    crmForm.AdicionarParceriaEngenheiro = function() {
        if (crmForm.all.new_coordenador_obra_contatoid.DataValue != null) {
            var cmd = new RemoteCommand("MrvService", "ObterParceriaEngenheiro", "/MRVCustomizations/");
            cmd.SetParameter("contatoId", crmForm.all.new_coordenador_obra_contatoid.DataValue[0].id);
            var resultado = cmd.Execute();
            if (crmForm.TratarRetornoRemoteCommand(resultado, "ObterParceriaEngenheiro") && resultado.ReturnValue.Mrv.Parceria != null) {
                crmForm.all.new_parceria_engenheiro.DataValue = resultado.ReturnValue.Mrv.Parceria;
            } else {
                crmForm.all.new_parceria_engenheiro.DataValue = null;
            }
        } else {
            crmForm.all.new_parceria_engenheiro.DataValue = null;
        }
    }

    crmForm.ConfiguraPerfilEngenhariaPrazos();

    function Load() {
        var filterDefault = "false";

        var urlFilter = "/MRVWeb/FilterLookup/FilterLookup.aspx"
        var oParam = "objectTypeCode=2&filterDefault=" + filterDefault + "&attributesearch=fullname";
        crmForm.FilterLookup(crmForm.all.new_contato_empreendimentoid, 2, urlFilter, oParam);
        crmForm.FilterLookup(crmForm.all.new_contato_empreendimento2id, 2, urlFilter, oParam);
        crmForm.ConfiguraLoockupCurvaVendas();
        if (!crmForm.UsuarioPertenceEquipe("Núcleo - Notificação;Núcleo - NIAC;Núcleo - Encante;Núcleo - Controle Estratégico")) {
            crmForm.all.new_data_da_visita.Disabled = true;
            crmForm.all.new_visitamarcada.Disabled = true;
            crmForm.all.new_visita_realizada.Disabled = true;
        }

        if (crmForm.UsuarioPertenceEquipe(Equipe.MARKETING)) {
            crmForm.all.new_valor_cheque.Disabled = false;
            crmForm.all.new_indicacao_ativa.Disabled = false;
            crmForm.all.new_prazo_validade_indicacao.Disabled = false;
        }
        else {
            crmForm.all.new_valor_cheque.Disabled = true;
            crmForm.all.new_indicacao_ativa.Disabled = true;
            crmForm.all.new_prazo_validade_indicacao.Disabled = true;
        }

        var caminho = "objectTypeCode=3002&filterDefault=false&attributesearch=fullname&_tipocontato="

        var oParam2 = caminho + TiposContato.DIRETOR_GESTOR_OBRA;
        crmForm.FilterLookup(crmForm.all.new_gestor_diretor_obra_contatoid, "2", urlFilter, oParam2);

        var oParam3 = caminho + TiposContato.COORDENADOR;
        crmForm.FilterLookup(crmForm.all.new_coordenador_obra_contatoid, "2", urlFilter, oParam3);

        var oParam4 = caminho + TiposContato.ENGENHEIRO;
        crmForm.FilterLookup(crmForm.all.new_engenheiro_obra_contatoid, "2", urlFilter, oParam4);
        crmForm.valorCampoElegibilidade = crmForm.all.new_empreendimento_elegivel.DataValue;

        var oParamCondominio = "objectTypeCode=1013&filterDefault=false&attributesearch=fullname"
        crmForm.FilterLookup(crmForm.all.new_condominioid, "2", urlFilter, oParamCondominio);

        crmForm.VerificaObrigatoriedadeCampos();

        if (!crmForm.UsuarioPertenceEquipe("CO_REABRE_OPORTUNIDADE")) {
            crmForm.all.new_prazo_ativacao_contrato.Disabled = true;
            crmForm.SetFieldReqLevel("new_prazo_ativacao_contrato", true);
        }

        if (!crmForm.UsuarioPertenceEquipe("Desenv.Imobiliário")) {
            crmForm.all.new_versaoconvencaocondominio.Disabled = true;
            crmForm.all.new_versaomemorialdescritivo.Disabled = true;
        }
        else {
            crmForm.all.new_versaoconvencaocondominio.Disabled = false;
            crmForm.all.new_versaomemorialdescritivo.Disabled = false;
        }

        if (!crmForm.UsuarioPertenceEquipe(Equipe.ELEGIBILIDADE_EMPREENDIMENTO)) {
            crmForm.all.new_empreendimento_elegivel.Disabled = true;
        }
        else {
            crmForm.all.new_empreendimento_elegivel.Disabled = false;
        }
        if (!crmForm.UsuarioPertenceEquipe(Equipe.ADMINISTRADORES)) {
            crmForm.all.new_habilitar_taxa_administracao.Disabled = true;
        }
        else {
            crmForm.all.new_habilitar_taxa_administracao.Disabled = false;
        }

        if (crmForm.all.new_tipo.DataValue == TipoEmpreendimento.Village)
            crmForm.EscondeCampo("new_kit_obra", true);
        else
            crmForm.EscondeCampo("new_kit_obra", false);

        crmForm.ValidarCampoApoioBB = function () {
            if (crmForm.all.new_apoio_producao_bb.DataValue != null && crmForm.all.new_apoio_producao_bb.DataValue) {
                crmForm.DesabilitarCampos("new_data_registro_pj", false);
                crmForm.SetFieldReqLevel("new_data_registro_pj", true);
            }
            else {
                crmForm.DesabilitarCampos("new_data_registro_pj", true);
                crmForm.SetFieldReqLevel("new_data_registro_pj", false);
            }
        }

        crmForm.ConfigurarCampoApoioBB = function () {
            if (crmForm.UsuarioPertenceEquipe(Equipe.ADMINISTRADORES) || crmForm.UsuarioPertenceEquipe(Equipe.CR_APOIO_PRODUCAO_BB)) {
                crmForm.DesabilitarCampos("new_apoio_producao_bb", false);
                crmForm.DesabilitarCampos("new_data_registro_pj", false);
                crmForm.ValidarCampoApoioBB();
            }
            else {
                crmForm.DesabilitarCampos("new_apoio_producao_bb", true);
                crmForm.DesabilitarCampos("new_data_registro_pj", true);
            }
        }

        crmForm.ConfigurarCampoDataInicial = function () {
            if (crmForm.FormType == TypeCreate) {
                crmForm.SetFieldReqLevel("new_data_inicial_prevista_conclusao", true);
                crmForm.SetFieldReqLevel("new_data_inicial_prevista_conclusao_execucao", true);
            }
            else {
                crmForm.DesabilitarCampos("new_data_inicial_prevista_conclusao", true);
                crmForm.DesabilitarCampos("new_data_inicial_prevista_conclusao_execucao", true);
            }
        }

        crmForm.ConfigurarCampoApoioBB();
        crmForm.DesabilitaDataPrevistaInicialRI();
        crmForm.ConfigurarCampoDataInicial();

        AbrirRegistro = function (typecode, id) {
            openObj(typecode, id);
        }

        Auditar = function (auditoriaCSG) {
            var cmd = new RemoteCommand("MrvService", "Auditar", "/MrvCustomizations/");
            cmd.SetParameter("entidadeId", crmForm.ObjectId);
            cmd.SetParameter("tipoAuditoria", TipoAuditoria.EMPREENDIMENTO);

            var resultado = cmd.Execute();

            if (resultado.Success) {
                if (resultado.ReturnValue.Mrv.Error)
                    alert(resultado.ReturnValue.Mrv.Error);
                else if (resultado.ReturnValue.Mrv.ErrorSoap)
                    alert(resultado.ReturnValue.Mrv.ErrorSoap)
                else {
                    document.getElementById("IFRAME_auditoria").src = document.getElementById("IFRAME_auditoria").src;
                    crmForm.DesabilitarCampos("new_statusdocartrio", true);
                    crmForm.DesabilitarCampos("new_dataprevistaconclusodori", true);
                    crmForm.DesabilitarCampos("new_dataconclusodori", true);

                    crmForm.DesabilitarCampos("new_nmerodori", true);
                    crmForm.DesabilitarCampos("new_cartorio_ri", true);
                    crmForm.DesabilitarCampos("new_cidade_cartorio_id", true);
                }
            }
        }

        crmForm.DesabilitarCamposRIAuditoria = function () {
            if (crmForm.FormType == TypeUpdate) {
                var cmd = new RemoteCommand("MrvService", "VerificarExisteAuditoria", "/MRVCustomizations/");
                cmd.SetParameter("entidadeId", crmForm.ObjectId);
                cmd.SetParameter("tipoAuditoria", TipoAuditoria.EMPREENDIMENTO);
                var result = cmd.Execute();
                if (result.Success) {
                    if (result.ReturnValue.Mrv.ExisteAuditoria) {

                        crmForm.DesabilitarCampos("new_statusdocartrio", true);
                        crmForm.DesabilitarCampos("new_dataprevistaconclusodori", true);
                        crmForm.DesabilitarCampos("new_dataconclusodori", true);

                        crmForm.DesabilitarCampos("new_nmerodori", true);
                        crmForm.DesabilitarCampos("new_cartorio_ri", true);
                        crmForm.DesabilitarCampos("new_cidade_cartorio_id", true);

                    }
                }
            }
        }
        crmForm.DesabilitarCamposRIAuditoria();

        crmForm.CopiarValoresDataPrevistaConclusao = function()
        {
            if (crmForm.FormType == TypeCreate) {
                if (crmForm.all.new_alvarunico.DataValue) {
                    crmForm.all.new_data_inicial_prevista_conclusao_execucao.DataValue = crmForm.all.new_data_inicial_prevista_conclusao.DataValue;
                    crmForm.DesabilitarCampos("new_data_inicial_prevista_conclusao_execucao", true);
                    crmForm.all.new_data_inicial_prevista_conclusao_execucao.ForceSubmit = true;
                }
                else {
                    crmForm.DesabilitarCampos("new_data_inicial_prevista_conclusao_execucao", false);
                }
            }
        }
        crmForm.CopiarValoresDataPrevistaConclusao();

        crmForm.DesabilitarAuditoria = function () {
            // O botão auditoria só irá aparecer caso o usuário pertence a equipe NCE_Auditoria_RI e o status do cartório for
            // igual a Deferido
            if (crmForm.FormType == TypeUpdate) {
                if (crmForm.all.new_statusdocartrio.DataValue &&
                    crmForm.all.new_statusdocartrio.DataValue == TipoStatusCartorio.Deferido) {
                    document.getElementById('IFRAME_auditoria').style.display = '';
                    return;
                }
            }

            document.getElementById('IFRAME_auditoria').style.display = 'none';
        }

        crmForm.DesabilitarAuditoria();

        crmForm.DesbalitaCampoImpeditivo = function() {
            if (crmForm.UsuarioPertenceEquipe("DI_Nucleo Corporativo")) {
                crmForm.DesabilitarCampos("new_impeditivo", false);
            } else {
                crmForm.DesabilitarCampos("new_impeditivo", true);
            }
        }
        crmForm.DesbalitaCampoImpeditivo();

        crmForm.DesabilitaVendaGarantida = function () {
            if (crmForm.FormType == TypeCreate || crmForm.FormType == TypeUpdate) {
                if (crmForm.UsuarioPertenceEquipe(Equipe.COM_VENDA_GARANTIDA)) {
                    crmForm.all.new_venda_garantida.Disabled = false;
                } else {
                    crmForm.all.new_venda_garantida.Disabled = true;
                }
            }
        }
        crmForm.DesabilitaVendaGarantida();

        crmForm.DesbalitaCampoImpeditivo = function() {
            if (crmForm.UsuarioPertenceEquipe("DI_Nucleo Corporativo")) {
                crmForm.DesabilitarCampos("new_impeditivo", false);
            } else {
                crmForm.DesabilitarCampos("new_impeditivo", true);
            }
        }
        crmForm.DesbalitaCampoImpeditivo();

        crmForm.DesabilitarCampoEmpreedimentoLog = function() {
            if (crmForm.FormType == TypeCreate && crmForm.UsuarioPertenceEquipe(Equipe.DESENV_IMOBILIARIO)) {
                return false;
            } else {
                return true;
            }
        }

        crmForm.DesabilitarCampoEmpreedimentoLogFar = function (valorCampoFar, valorCampoLog) {
            if (valorCampoFar) {
                crmForm.DesabilitarCampos("new_empreendimentofar", false);
                crmForm.DesabilitarCampos("new_empreendimento_log", true);
                crmForm.all.new_empreendimento_log.DataValue = false;

            }
            else if (valorCampoLog) {
                crmForm.DesabilitarCampos("new_empreendimentofar", true);
                crmForm.DesabilitarCampos("new_empreendimento_log", crmForm.DesabilitarCampoEmpreedimentoLog());
                crmForm.all.new_empreendimentofar.DataValue = false;
            }
            else {
                crmForm.DesabilitarCampos("new_empreendimentofar", false);
                crmForm.DesabilitarCampos("new_empreendimento_log", crmForm.DesabilitarCampoEmpreedimentoLog());
            }
        }

        crmForm.DesabilitarCampoEmpreedimentoLogFar(crmForm.all.new_empreendimentofar.DataValue, crmForm.all.new_empreendimento_log.DataValue);

        crmForm.DesabilitarCampoDivisao = function () {
            if (crmForm.FormType == TypeUpdate) {
                if (crmForm.UsuarioPertenceEquipe(Equipe.ADMINISTRADORES)) {
                    crmForm.DesabilitarCampos("new_divisao", false);
                }
                else {
                    var cmd = new RemoteCommand("MrvService", "ValidarEmpreendimentoLancado", "/MRVCustomizations/");
                    cmd.SetParameter("empreendimentoId", crmForm.ObjectId);
                    var resultado = cmd.Execute();
                    if (crmForm.TratarRetornoRemoteCommand(resultado, "ValidarEmpreendimentoLancado")) {
                        var retorno = resultado.ReturnValue.Mrv.EmpreendimentoLancado;
                        crmForm.DesabilitarCampos("new_divisao", retorno);
                    }
                    else {
                        crmForm.DesabilitarCampos("new_divisao", true);
                    }
                }
            }
        }

        crmForm.DesabilitarCampoDivisao();

        crmForm.ValidarCamposIndicacaoPremiada = function () {
            if (crmForm.all.new_indicacao_ativa.DataValue && crmForm.UsuarioPertenceEquipe(Equipe.MARKETING)) {
                crmForm.SetFieldReqLevel("new_valor_cheque", true);
                crmForm.SetFieldReqLevel("new_prazo_validade_indicacao", true);
            }
            else {
                crmForm.SetFieldReqLevel("new_valor_cheque", false);
                crmForm.SetFieldReqLevel("new_prazo_validade_indicacao", false);
            }
        }

        crmForm.ValidarCamposIndicacaoPremiada();
    }

    function loadScript() {
        var oHead = document.getElementsByTagName('head')[0];
        var oScript = document.createElement('script');
        oScript.type = "text/javascript";
        oScript.src = "/_static/Base.js";

        oHead.appendChild(oScript);

        oScript.onreadystatechange = function () {
            if (this.readyState == 'complete' || this.readyState == 'loaded') {
                Load();
            }
        }
    }

    loadScript();

}
catch (error) {
    formularioValido = false;
    alert("Ocorreu um erro no formulário.\n" + error.description);
}