﻿if (crmForm.all.new_statuslancamento.DataValue == 1 && crmForm.all.new_statusempreendimento.DataValue == null) {
    crmForm.all.new_statusempreendimento.DataValue = 1;
    crmForm.EscondeCampo("new_statusempreendimento", false);
}
else if (crmForm.all.new_statuslancamento.DataValue != 1 && (crmForm.all.new_statusempreendimento.DataValue == null || crmForm.all.new_statusempreendimento.DataValue == 1)) {
    crmForm.EscondeCampo("new_statusempreendimento", true);
    crmForm.all.new_statusempreendimento.DataValue = null;
}