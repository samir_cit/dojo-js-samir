﻿//Aba: Empreendimento
//Seção: Endereço

function SetUF(nome, picklistField) {
    for (i = 0; i < picklistField.Options.length; i++) {
        if (picklistField.Options[i].Text.toUpperCase() == nome.toUpperCase())
            picklistField.SelectedIndex = i;
    }
}

function LimpaCampos() {
    crmForm.all.new_enderecobairro.DataValue = null;
    //crmForm.all.new_enderecocidade.DataValue = null;
    crmForm.all.new_enderecocomplemento.DataValue = null;
    crmForm.all.new_enderecorua.DataValue = null;

}

function GetEndereco(cep) {

    if (cep != null) {
        cep = cep.replace(".", "");
        cep = cep.replace("-", "");

        var cmd = new RemoteCommand("MrvService", "GetAddressByCep", "/MRVCustomizations/");
        cmd.SetParameter("cep", cep);
        var result = cmd.Execute();
        if (result.Success) {

            if (!result.ReturnValue.Mrv.IsNull) {
                crmForm.all.new_enderecorua.DataValue = result.ReturnValue.Mrv.TIPORUA + " " + result.ReturnValue.Mrv.NOMERUA;

                if (typeof (result.ReturnValue.Mrv.COMPLEMENTO) != "object")
                    crmForm.all.new_enderecocomplemento.DataValue = result.ReturnValue.Mrv.COMPLEMENTO;

                if (typeof (result.ReturnValue.Mrv.CIDADE) != "object" && typeof (result.ReturnValue.Mrv.UF) != "object") {
                    var cmdCidade = new RemoteCommand("MrvService", "GetCityByNameUf", "/MRVCustomizations/");
                    cmdCidade.SetParameter("NameCidade", result.ReturnValue.Mrv.CIDADE);
                    cmdCidade.SetParameter("UF", result.ReturnValue.Mrv.UF);
                    var oResultado = cmdCidade.Execute();
                    if (oResultado.Success) {
                        if (oResultado.ReturnValue.Mrv.Cidade != false) {
                            var lookupData = new Array();
                            var lookupItem = new Object();
                           
                            lookupItem.id = oResultado.ReturnValue.Mrv.Cidade;
                            lookupItem.typename = 'new_cidade';
                            lookupItem.name = result.ReturnValue.Mrv.CIDADE;
                            lookupData[0] = lookupItem;
                            crmForm.all.new_cidadeid.DataValue = lookupData;
                            crmForm.all.new_cidadeid.FireOnChange();
                        }
                    }
                }

                if (typeof (result.ReturnValue.Mrv.UF) != "object")
                    SetUF(result.ReturnValue.Mrv.UF, crmForm.all.new_estadouf);

                if (typeof (result.ReturnValue.Mrv.BAIRRO) != "object")
                    crmForm.all.new_enderecobairro.DataValue = result.ReturnValue.Mrv.BAIRRO;
            }
            else {
                alert("CEP não cadastrado!");
                LimpaCampos();
            }
        }

    }
}



//mascara de CEP
var oField = event.srcElement;
LimpaCampos();
if (typeof (oField) != "undefined" && oField != null) {
    if (oField.DataValue != null) {
        var sTmp = oField.DataValue.replace(/[^0-9]/g, "");

        if (sTmp.length != 8) {
            oField.DataValue = null;
            alert("Cep fora da faixa 8 dígitos sequenciais! Ex: 31080140");
        }
        else {
            GetEndereco(oField.DataValue);
            oField.DataValue = sTmp.substr(0, 2) + "." + sTmp.substr(2, 3) + "-" + sTmp.substr(5, 3);

        }
    }
    else { GetEndereco(oField.DataValue); }
}