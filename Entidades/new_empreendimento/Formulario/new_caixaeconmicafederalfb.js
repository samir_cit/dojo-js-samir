﻿if (crmForm.all.new_caixaeconmicafederalfb.DataValue) 
{
    crmForm.ValidarTipoDeFinanciamento("new_caixaeconmicafederalfb");
    
    document.getElementById("new_statuspranlisefb_d").parentNode.parentNode.style.display = "none";
    document.getElementById("new_statusanliseana_d").parentNode.parentNode.style.display = "none";
    document.getElementById("new_statusdolaudola_d").parentNode.parentNode.style.display = "none";
    document.getElementById("new_statuscontrataoco_d").parentNode.parentNode.style.display = "none";
    document.getElementById("new_rico_d").parentNode.parentNode.style.display = "none";

    if (crmForm.all.new_planoempresrio.DataValue)
        document.getElementById("new_envio_cronograma_atualizacao_d").parentNode.parentNode.style.display = "inline";
    else
        document.getElementById("new_envio_cronograma_atualizacao_d").parentNode.parentNode.style.display = "none";

    document.getElementById("new_previsao_incendio_aprovado_d").parentNode.parentNode.style.display = "none";
}
else 
{
    document.getElementById("new_statuspranlisefb_d").parentNode.parentNode.style.display = "inline";
    document.getElementById("new_statusanliseana_d").parentNode.parentNode.style.display = "inline";
    document.getElementById("new_statusdolaudola_d").parentNode.parentNode.style.display = "inline";
    document.getElementById("new_statuscontrataoco_d").parentNode.parentNode.style.display = "inline";
    document.getElementById("new_rico_d").parentNode.parentNode.style.display = "inline";
    document.getElementById("new_envio_cronograma_atualizacao_d").parentNode.parentNode.style.display = "inline";
    document.getElementById("new_previsao_incendio_aprovado_d").parentNode.parentNode.style.display = "inline";
}