﻿//Validar se Unibanco ou Itaú
if (crmForm.all.new_banco_empreendimentoid.DataValue != null) {
    var lookupData = crmForm.all.new_banco_empreendimentoid.DataValue;
    if (lookupData[0].name.toUpperCase() != 'UNIBANCO' && lookupData[0].name.toUpperCase() != 'BANCO ITAÚ S.A.') {
        alert('Por restrições de depósito identificado somente contas do UNIBANCO ou ITAÚ podem ser utilizadas.');
        crmForm.all.new_banco_empreendimentoid.DataValue = null;
    }
}
