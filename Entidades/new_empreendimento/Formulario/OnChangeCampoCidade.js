﻿// Aba: Empreendimento
// Seção: Endereço
var cidadeSelecionada = crmForm.all.new_cidadeid;
if (cidadeSelecionada.DataValue != null)
{
    var lookupRegiaoDeVendas = null;
    var lookupItem = null;
    var cmd = new RemoteCommand("DistratoService", "ObterInformacoesCidade", "/MRVCustomizations/DistratoRenegociacao/");
    cmd.SetParameter("cidadeId", cidadeSelecionada.DataValue[0].id);
    oResult = cmd.Execute();
    if (oResult.Success && oResult.ReturnValue.Mrv != null ) {

        if (oResult.ReturnValue.Mrv.Encontrou) {
            lookupRegiaoDeVendas = new Array();
            lookupItem = new Object();
            lookupItem.id = oResult.ReturnValue.Mrv.RegiaoVendasId;
            lookupItem.name = oResult.ReturnValue.Mrv.RegiaoVendasNome;
            lookupItem.typename = 'new_regiao_de_vendas';
            lookupRegiaoDeVendas[0] = lookupItem;

            lookupRegional = new Array();
            lookupItem2 = new Object();
            lookupItem2.id = oResult.ReturnValue.Mrv.RegionalId;
            lookupItem2.name = oResult.ReturnValue.Mrv.RegionalNome;
            lookupItem2.typename = 'new_regional';
            lookupRegional[0] = lookupItem2;

            crmForm.all.new_regiao_de_vendas_id.DataValue = lookupRegiaoDeVendas;
            crmForm.all.new_regionalid.DataValue = lookupRegional;
        }
        else {
            alert(oResult.ReturnValue.Mrv.Mensagem);
            cidadeSelecionada.DataValue = null;
        }
        crmForm.all.new_regional_mrvid.Disabled = false;
        if (oResult.ReturnValue.Mrv.TemRegionalMRV) {
            lookupRegionalMRV = new Array();
            lookupItem3 = new Object();
            lookupItem3.id = oResult.ReturnValue.Mrv.RegionalMRVId;
            lookupItem3.name = oResult.ReturnValue.Mrv.RegionalMRVNome;
            lookupItem3.typename = 'new_regional_mrv';
            lookupRegionalMRV[0] = lookupItem3;
            crmForm.all.new_regional_mrvid.DataValue = lookupRegionalMRV;
        }
        else {
            crmForm.all.new_regional_mrvid.DataValue = null;
        }
        crmForm.all.new_regional_mrvid.Disabled = true;
        crmForm.all.new_regional_mrvid.ForceSubmit = true;
    }   
}