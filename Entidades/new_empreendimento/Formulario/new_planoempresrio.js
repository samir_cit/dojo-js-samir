﻿if (crmForm.all.new_planoempresrio.DataValue) 
{
    crmForm.ValidarTipoDeFinanciamento("new_planoempresrio");
    
    document.getElementById("new_statuspip_d").parentNode.parentNode.style.display = "none";
    document.getElementById("new_envio_cronograma_atualizacao_d").parentNode.parentNode.style.display = "none";
    document.getElementById("new_previsao_incendio_aprovado_d").parentNode.parentNode.style.display = "none";

    if (crmForm.all.new_caixaeconmicafederalfb.DataValue) 
    {
        document.getElementById("new_envio_cronograma_atualizacao_d").parentNode.parentNode.style.display = "inline";
        document.getElementById("new_previsao_incendio_aprovado_d").parentNode.parentNode.style.display = "inline";
    }
}
else 
{
    document.getElementById("new_statuspip_d").parentNode.parentNode.style.display = "inline";
    document.getElementById("new_envio_cronograma_atualizacao_d").parentNode.parentNode.style.display = "inline";
}