﻿if (crmForm.all.new_quantidade_fila.DataValue != null) {
    var pertence = crmForm.UsuarioPertenceEquipe("Coordenação de Vendas - Configurações");
    if (!pertence) {
        alert("Usuário sem permissão para alterar este campo.");
        crmForm.all.new_quantidade_fila.DataValue = quantidadeFila;
    }
}