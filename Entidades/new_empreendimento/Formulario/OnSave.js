﻿try {
    if (!formularioValido) {
        alert("Ocorreu um erro no formulário. Impossível salvar.");
        event.returnValue = false;
        return false;
    }
    
    /// Inativar Empreendimento
    if (event.Mode == EVENT_MODE_ONSAVE_DEACTIVATE) {
        if (!crmForm.UsuarioPertenceEquipe("DI_Inativar_Registros") && !crmForm.UsuarioPertenceEquipe("Administradores")) {
            alert('Usuário sem permissão: Não participa da equipe: "DI_Inativar_Registros"');
            event.returnValue = false;
            return false;
        }
    }

    function ValidarValorImagem() {
        var validacaoRetorno = true;
        if (crmForm.all.new_valor_cheque.DataValue && crmForm.all.new_valor_cheque.IsDirty) {
            var cmd = new RemoteCommand('MrvService', 'ValidarValorImagem', '/MRVCustomizations/');
            cmd.SetParameter('valor', crmForm.all.new_valor_cheque.DataValue);
            var retorno = cmd.Execute();
            validacaoRetorno = crmForm.TratarRetornoRemoteCommand(retorno, "ValidarValorImagem");
            if (validacaoRetorno) {
                if (retorno.ReturnValue.Mrv.Mensagem) {
                    alert(retorno.ReturnValue.Mrv.Mensagem);
                    event.returnValue = false;
                    return false;
                }
            }
            event.returnValue = validacaoRetorno;

        }
        return validacaoRetorno;
    }

    ValidarValorImagem();

    function verificaObrigatoriedadePA(){
        //verifica condições
        if(crmForm.all.new_statusdocartrio.DataValue == TipoStatusCartorio.Deferido && crmForm.all.new_dataconclusodori.DataValue != null && crmForm.all.new_patrimonio_afetacao.DataValue == TipoPatrimonioAfetacao.Sim){
            var cmd = new RemoteCommand("MrvService", "ValidarEmpreendimentoPossuiPatrimonioAfetacao", "/MRVCustomizations/");
            cmd.SetParameter("empreendimentoId", crmForm.ObjectId);

            var result = cmd.Execute();
            if (result.Success) {
                if (!result.ReturnValue.Mrv.Resultado) {
                    alert("Para este empreendimento é necessário criar um registro de Patrimônio de Afetação antes de prosseguir com a gravação.");
                    event.returnValue = false;
                    return false;
                }
            }
        }
        return true;
    }

    crmForm.all.new_statusempreendimento.ForceSubmit = true;
    crmForm.all.new_parceria_engenheiro.ForceSubmit = true;
    var indice = 0;
    //Valida se os blocos informados nos campos de módulos condizem com a sequência de obras informada
    for (indice = 1; indice <= 6; indice++){
        var campo = eval("crmForm.all.new_modulo" + indice);
        if (campo.DataValue == null) continue;
        if (!crmForm.ValidaSequenciaDeObrasDosModulos(campo));
           // return false;
    }
    
    //Valida se a sequência de obras está correta comparando com os blocos disponíveis
    crmForm.ValidaSequenciaDeObras(crmForm.all.new_sequencia_obras);

    //Nome do epreendimento (completo e reduzido) não podem conter hífen "-"
    if((!(crmForm.all.new_nome_reduzido.Disabled)) || (!(crmForm.all.new_nome.Disabled)))
    {
        var Nome_empreendimento = (crmForm.all.new_nome.DataValue != null)?crmForm.all.new_nome.DataValue:"";
        var Nome_empreendimento_reduzido = (crmForm.all.new_nome_reduzido.DataValue != null)?crmForm.all.new_nome_reduzido.DataValue:"";
        if((Nome_empreendimento_reduzido.indexOf("-") != -1) || (Nome_empreendimento.indexOf("-") != -1))
        {
            alert("O nome do empreendimento / nome reduzido não pode conter hífen.");
            crmForm.all.new_nome.SetFocus();
            event.returnValue = false;
            return false;
        }
    }

    var attributes = "new_codigocidade";
    var cmd = new RemoteCommand("MrvService", "GetEntity", "/MRVCustomizations/");

    cmd.SetParameter("entity", "new_cidade");
    cmd.SetParameter("primaryField", "new_cidadeid");
    cmd.SetParameter("entityId", crmForm.all.new_cidadeid.DataValue[0].id);
    cmd.SetParameter("fieldList", attributes);

    var oResult = cmd.Execute();

    if (oResult.Success) {

        if (!(oResult.ReturnValue.Mrv.new_codigocidade)) {
            alert('A cidade ' + crmForm.all.new_cidadeid.DataValue[0].name + ' não possui código. Favor cadastrar.');
            event.returnValue = false;
            return false;
        }

    }
    else
        alert("Erro na consulta ao WebService.\nContate o administrador do sistema.");

    //Verifica se o código do SAP informado já existe em outro empreendimento
    if (crmForm.all.new_codsap.DataValue && crmForm.all.new_codsap.IsDirty) {
        var cmd = new RemoteCommand("MrvService", "ExistCodSAP", "/MRVCustomizations/");
        cmd.SetParameter("entity", "new_empreendimento");
        cmd.SetParameter("primaryField", "new_empreendimentoid");
        cmd.SetParameter("fieldContainsCodSAP", "new_codsap");
        cmd.SetParameter("codSAP", crmForm.all.new_codsap.DataValue);
        var entityId = crmForm.ObjectId;
        if (!entityId)
            entityId = "";

        cmd.SetParameter("entityId", entityId);

        var result = cmd.Execute();
        if (result.Success) {
            if (result.ReturnValue.Mrv.Found) {
                alert("Código do SAP já está associado a outro empreendimento.\nAltere o código do SAP e tente salvar novamente!");
                event.returnValue = false;
                return false;
            }
        }
    }

    //Duplicação feita para a integração com o SAP 6.0
    //Verifica se o código do SAP informado já existe em outro empreendimento
    if (crmForm.all.new_codigo_projeto_ps.DataValue && crmForm.all.new_codigo_projeto_ps.IsDirty) {
        var cmd = new RemoteCommand("MrvService", "ExistCodSAP", "/MRVCustomizations/");
        cmd.SetParameter("entity", "new_empreendimento");
        cmd.SetParameter("primaryField", "new_empreendimentoid");
        cmd.SetParameter("fieldContainsCodSAP", "new_codigo_projeto_ps");
        cmd.SetParameter("codSAP", crmForm.all.new_codigo_projeto_ps.DataValue);
        var entityId = crmForm.ObjectId;
        if (!entityId)
            entityId = "";

        cmd.SetParameter("entityId", entityId);

        var result = cmd.Execute();
        if (result.Success) {
            if (result.ReturnValue.Mrv.Found) {
                alert("O código do projeto no PS já está associado a outro empreendimento.\nAltere o código e tente salvar novamente!");
                event.returnValue = false;
                return false;
            }
        }
    }

    //Empreendimento --Guia 01 ==========================================

    //Data aquisio do terreno + 150 dias = data ideal p/ lanamento---------------------------

    if (crmForm.all.new_dataaquisiodoterreno.DataValue != null) {
        var datalanc = crmForm.all.new_dataaquisiodoterreno.DataValue;
        datalanc.setDate(datalanc.getDate() + 150);
        crmForm.all.new_dataideal.DataValue = datalanc;
    }

    crmForm.all.new_regiao_de_vendas_id.Disabled = false;
    //Incorporao=======================================================
    //SEAO - 04 calculo data prevista de RI=============================

    if ((crmForm.all.new_datadeprevisoinc5.DataValue != null) && (crmForm.all.new_dataaquisiodoterreno.DataValue != null)) {
        crmForm.all.new_datadeprevisoinc5.setAttribute("req", 1);
        crmForm.all.new_datadeprevisoinc5_c.className = "req";
        crmForm.all.new_datadeprevisoinc5_c.style.visibility = 'visible';
        crmForm.all.new_datadeprevisoinc5_d.style.visibility = 'visible';
        var data_aux = crmForm.all.new_datadeprevisoinc5.DataValue;

        data_aux.setDate(data_aux.getDate() + 5);
        crmForm.all.new_dataprevistaentradanocartrio.DataValue = data_aux;
        data_aux.setDate(data_aux.getDate() + 30);
        crmForm.all.new_dataprevistaconclusodori.DataValue = data_aux;

        //SEO - 04 ====Somar 2 datas resultado = meses-------------------
        var DataAux1 = crmForm.all.new_dataprevistaconclusodori.DataValue;
        var DataAux2 = crmForm.all.new_dataaquisiodoterreno.DataValue;

        crmForm.all.new_prazoemmeseasscontratoxprevri.DataValue = (DataAux1.setDate(DataAux1.getDate()) - DataAux2.setDate(DataAux2.getDate())) / 2592000000;
    }
    else {
        if (crmForm.all.new_dataconclusodori.DataValue != null && crmForm.all.new_dataaquisiodoterreno.DataValue != null) {
            crmForm.all.new_datadeprevisoinc5.setAttribute("n", 0);
            crmForm.all.new_datadeprevisoinc5_c.className = "n";
            crmForm.all.new_datadeprevisoinc5_c.style.visibility = 'hidden';
            crmForm.all.new_datadeprevisoinc5_d.style.visibility = 'hidden';
            
            //SEO - 04 ====Somar 2 datas resultado = meses-------------------
            var DataAux2 = crmForm.all.new_dataaquisiodoterreno.DataValue;
            var DataAux3 = crmForm.all.new_dataconclusodori.DataValue;

            crmForm.all.new_prazoemmesesasscontratoxconcluso.DataValue = (DataAux3.setDate(DataAux3.getDate()) - DataAux2.setDate(DataAux2.getDate())) / 2592000000;
        }
    }
    // Desabilita campo Data Prevista Inicial RI
    crmForm.DesabilitaDataPrevistaInicialRI();
    
    //====habilitar campo de validade====================================
    crmForm.all.new_dtinivigenciaalvaraplantaovendas.Disabled = false;
    crmForm.all.new_dtfimvigenciaalvaraplantaovendas.Disabled = false;
    crmForm.all.new_datainiciodavigncia.Disabled = false;
    crmForm.all.new_datafinaldavigncia.Disabled = false;

    //Passar todos os caracteres para maiúsculo-----------------------------------------------
    var elm = document.getElementsByTagName("input");
    for (i = 0; i < elm.length; i++) {
        if (elm[i].type == "text")
            if (elm[i].DataValue != null) {
            try {
                elm[i].DataValue = elm[i].DataValue.toUpperCase();
            }
            catch (e) { }
        }
    }
    
    // Habilitar campos para salvar no banco-------------------------------------------------------//
    crmForm.all.new_datamaiorlanceri.Disabled = false;
    crmForm.all.new_datadacontrataofb.Disabled = false;
    crmForm.all.new_nome_reduzido.Disabled = false;
    crmForm.all.new_dias_vigencia_proposta.Disabled = false;
    crmForm.all.new_regionalid.Disabled = false;
    
	crmForm.ValidarCamposPatrimonioAfetacao();
    verificaObrigatoriedadePA();
    
    var valorCampo = crmForm.all.new_empreendimento_elegivel.DataValue;

    if (crmForm.valorCampoElegibilidade != valorCampo && event.returnValue) {
	    var nomeCampo = crmForm.all.new_empreendimento_elegivel.id;
	    var valorCampo = crmForm.all.new_empreendimento_elegivel.DataValue;
	    var idRegistro = crmForm.ObjectId;
	    var nomeRegistro = crmForm.all.new_nome.DataValue;
	    var nomeTecnicoEntidade = crmForm.ObjectTypeName;

	    var oParam = "nomeCampo=" + nomeCampo + "&nomeTecnicoEntidade=" + nomeTecnicoEntidade;

	    var url = '/MRVWeb/LogAlteracao/MotivoAlteracaoCampo.aspx?' + oParam;

	    var retorno = openStdDlg(url, null, 750, 440);
	    if (retorno != null && retorno != undefined) {
		    var cmd = new RemoteCommand("MrvService", "CriarMotivoAlteracaoElegibilidade", "/MRVCustomizations/");
		    cmd.SetParameter("nomeTecnicoEntidade", nomeTecnicoEntidade);
		    cmd.SetParameter("registroId", idRegistro);
		    cmd.SetParameter("nomeRegistro", nomeRegistro);
		    cmd.SetParameter("valorCampo", valorCampo);
		    cmd.SetParameter("motivo", retorno.Motivo);
		    cmd.SetParameter("justificativa", retorno.Justivicativa);
		    var oResult = cmd.Execute();

		    if (!crmForm.TratarRetornoRemoteCommand(oResult, "CriarMotivoAlteracaoElegibilidade")) {
			    alert("Operação cancelada.");
			    event.returnValue = false;
			    return false;
		    }
	    } else {
		    alert("Operação cancelada.");
		    event.returnValue = false;
		    return false;
	    }
    }

    function MarcarEmpreendimentoFoiCritico() {
        if((crmForm.FormType == TypeCreate || crmForm.FormType == TypeUpdate) && crmForm.all.new_empreendimento_critico.IsDirty) {
            crmForm.all.new_empreendimento_foi_critico.DataValue = true;
            crmForm.all.new_empreendimento_foi_critico.ForceSubmit = true;
        }
    }

    MarcarEmpreendimentoFoiCritico();

} catch (error) {
    alert("Ocorreu um erro no formulário.\n" + error.description);
    event.returnValue = false;
    return false;
}
