﻿if (crmForm.all.new_permite_escala.DataValue) {
    crmForm.all.new_bloquear_sem_escala.Disabled = false;
}
else {
    crmForm.all.new_bloquear_sem_escala.Disabled = true;
    if (crmForm.all.new_permite_escala.IsDirty) {
        crmForm.all.new_bloquear_sem_escala.DataValue = false;
        crmForm.all.new_bloquear_sem_escala.ForceSubmit = true;
    }
}