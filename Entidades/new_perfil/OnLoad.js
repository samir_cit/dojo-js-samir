﻿var formularioValido = true;
var TypeCreate = 1;
var TypeUpdate = 2;
try 
{
    if (crmForm.FormType == TypeCreate || crmForm.FormType == TypeUpdate) {
        crmForm.all.new_permite_escala.FireOnChange();
    }

} catch (error) {
    formularioValido = false;
    alert("Ocorreu um erro no formulário.\n" + error.description);
}