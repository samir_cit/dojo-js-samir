﻿var cnpj = crmForm.all.new_cnpj.DataValue;
RemoteCNPJ(cnpj)
function RemoteCNPJ(cnpj) {
    if (cnpj != null) {
        var cmd = new RemoteCommand("MrvService", "ValidCnpjOrCpf", "/MRVCustomizations/");
        cmd.SetParameter("pNumber", cnpj.toString());
        cmd.SetParameter("pType", "0");
        var oResult = cmd.Execute();

        if (oResult.Success) {
            if (!oResult.ReturnValue.Mrv.Valid) {
                crmForm.all.new_cnpj.DataValue = "";
                alert('Cnpj inválido.');
                crmForm.all.new_cnpj.SetFocus();
            }
            else
                AddMascara(cnpj);
        }
        else {
            alert("Erro. Contate o administrador.");
            
            return false;
        }
    }
}

function AddMascara(cnpj) {
    if (typeof (cnpj) != "undefined" && cnpj != null) {
        var sTmp = crmForm.all.new_cnpj.DataValue.replace(/[^0-9]/g, "");

        if (sTmp.length != 14) {
            crmForm.all.new_cnpj.DataValue = "";
            alert('Cnpj inválido.');
            crmForm.all.new_cnpj.SetFocus();
        }
        else {
            if (sTmp.length == 14) {
                crmForm.all.new_cnpj.DataValue = sTmp.substr(0, 2) + "." + sTmp.substr(2, 3) + "." + sTmp.substr(5, 3) + "/" + sTmp.substr(8, 4) + "-" + sTmp.substr(12, 2);
            }
        }
    }
}