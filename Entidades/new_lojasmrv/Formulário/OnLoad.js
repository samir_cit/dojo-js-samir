﻿TipoCanal = {
    4004: 1,
    FISICA: 2,
    IMOB: 3,
    VIRTUAL: 4
}

CanalDistribuicao = {
    LojaAutorizada: 0,
    LojaPropria: 1    
}   

!(function(global) {
    'use strict';
    var ListaLojas = global.RelacionamentoLojas || {},
        ListaLojas = global.RelacionamentoLojas;

    RelacionamentoLojas = (function() {
        var itensExistentes = '';
        var ID_FRAME = 'IFRAME_Lojas';
        var entidadeRelacionamento = 'new_new_lojasmrv_new_lojasmrv';

        function OnReadyStateChange() {
            var iframeTarget = document.frames(ID_FRAME);
            if (iframeTarget.frameElement.readyState == 'complete') {
                if ((crmForm.GetElementsByClassName("ms-crm-MenuBar-Left", "ul", iframeTarget.document) != null) &&
			            (crmForm.GetElementsByClassName("ms-crm-MenuBar-Left", "ul", iframeTarget.document)).length != 0) {
                    crmForm.GetElementsByClassName("ms-crm-MenuBar-Left", "ul", iframeTarget.document)[0].children[0].attachEvent("onclick", OnClickChange);
                }
            }
        }

        function OnClickChange() {
            var iframeTarget = document.frames(ID_FRAME);
            if (iframeTarget.frameElement.readyState == 'complete') {
                eval("crmForm.all." + ID_FRAME).src = ObterEnderecoIframe();
                FormatarLayoutFrame();
            }
        }

        function ObterEnderecoIframe() {
            if (crmForm.ObjectId != null) {

                var oId = crmForm.ObjectId;
                var oType = crmForm.ObjectTypeCode;
                var security = crmFormSubmit.crmFormSubmitSecurity.value;

                return "/" + ORG_UNIQUE_NAME + "/userdefined/areas.aspx?oId=" + oId + "&oType=" + oType + "&security=" + security + "&roleOrd=1&tabSet=area" + entidadeRelacionamento;
            }
            else {
                return "about:blank";
            }
        }

        //Funcao para deixar a Pagina encaixada exatamente dentro do Iframe sem que sobre espacos deixando o Layout desalinhado.
        //Aguarda a janela até que o conteúdo do IFrame  estaja pronto
        function FormatarLayoutFrame() {
            var iframeID = ID_FRAME;
            if (document.getElementById(iframeID).contentWindow.document.readyState != "complete") {
                window.setTimeout(function() { FormatarLayoutFrame() }, 10);
            }
            else {
                // Alterar a cor do fundo
                document.getElementById(iframeID).contentWindow.document.body.style.backgroundColor = "#eef0f6";
                // Remova a borda esquerda
                document.getElementById(iframeID).contentWindow.document.body.all(0).style.borderLeftStyle = "none";
                // Remove padding
                document.getElementById(iframeID).contentWindow.document.body.all(0).all(0).all(0).all(0).style.padding = "0px";
                // Deixa a célula com toda a largura do IFRAME
                document.getElementById(iframeID).contentWindow.document.body.all(0).style.width = "100%"

                // mostra o IFrame
                document.getElementById(iframeID).style.display = "block";
            }
        }

        function ExibirFrame() {
            document.getElementById(ID_FRAME + "_d").parentNode.parentNode.style.display = "block";
        }

        function OcultarFrame() {
            document.getElementById(ID_FRAME + "_d").parentNode.parentNode.style.display = "none";
        }
        return {
            Initialize: function(tipoCanal) {
                var canalDstribuicao = crmForm.all.new_canaldedistribuicao.DataValue;
                document.frames(ID_FRAME).frameElement.attachEvent("onreadystatechange", OnReadyStateChange);
                if (crmForm.FormType != TypeCreate && tipoCanal == TipoCanal.IMOB 
                    && canalDstribuicao == CanalDistribuicao.LojaPropria) {
                    ExibirFrame();
                    eval("crmForm.all." + ID_FRAME).src = ObterEnderecoIframe();
                    FormatarLayoutFrame();
                }
                else {
                    OcultarFrame();
                }
            }
        }
    })();

    global.RelacionamentoLojas = RelacionamentoLojas;

})(this);


formularioValido = true;

crmForm.EscondeCampo = function(nomeCampo, esconde) {
    var tipo = "";
    if (esconde)
        tipo = "none";

    var atributo_c = eval("crmForm.all." + nomeCampo + "_c");
    var atributo_d = eval("crmForm.all." + nomeCampo + "_d");
    atributo_c.style.display = tipo;
    atributo_d.style.display = tipo;
}

crmForm.ConfiguraCNPJ = function() {
    crmForm.SetFieldReqLevel("new_cnpj", crmForm.all.new_canaldedistribuicao.DataValue == 0);
}

crmForm.ExcluirRelacionamentoLoja = function() {
    var tipoCanal = crmForm.all.new_tipo_canal.DataValue;
    if (crmForm.FormType != TypeCreate) {
        var lojaMatriz = crmForm.all.new_canaldedistribuicao.DataValue == CanalDistribuicao.LojaPropria;
        var cmd = new RemoteCommand("MrvService", "ExcluirRelacionamentoLoja", "/MRVCustomizations/");
        cmd.SetParameter("lojaId", crmForm.ObjectId);
        cmd.SetParameter("tipoCanal", tipoCanal);
        cmd.SetParameter("lojaMatriz", lojaMatriz);
        var resultado = cmd.Execute();
        if (crmForm.TratarRetornoRemoteCommand(resultado, "ExcluirRelacionamentoLoja")) {
            return resultado.ReturnValue.Mrv.Success;
        }
        return false;
    }
    else {
        return true;
    }
}

function Load() {
    crmForm.ConfiguraCNPJ();

    if (crmForm.all.new_usamrvcredito.DataValue == 1) {
        crmForm.SetFieldReqLevel("new_data_inclusao_credito", true);
    }

    RelacionamentoLojas.Initialize(crmForm.all.new_tipo_canal.DataValue);
}

try {
    function loadScript() {

        var oScript = document.createElement('script');
        oScript.type = "text/javascript";
        oScript.src = "/_static/Base.js";

        var oHead = document.getElementsByTagName('head')[0];
        oHead.appendChild(oScript);

        oScript.onreadystatechange = function() {
            if (this.readyState == 'complete' || this.readyState == 'loaded') {
                Load();
            }
        }
    }

    loadScript();
}
catch (error) {
    formularioValido = false;
    alert("Ocorreu um erro no formulário.\n" + error.description);
}