﻿try {
	// Transformar caracteres em maiusculos------------------------------------//
	var elm = document.getElementsByTagName("input");
	for (i = 0; i < elm.length; i++) {
		if (elm[i].type == "text")
			if (elm[i].DataValue != null) {
				try {
					elm[i].DataValue = elm[i].DataValue.toUpperCase();
				} catch (e) {}
			}
	}

	if (!crmForm.ValidarAtualizacao()) {
		event.returnValue = false;
		return false;
	}

	var valorCampo = crmForm.all.new_elegibilidade_automatica.DataValue;
	if (crmForm.FormType != TypeCreate){
		if (crmForm.valorCampoElegibilidade != valorCampo) {
			var nomeCampo = crmForm.all.new_elegibilidade_automatica.id;
			var valorCampo = crmForm.all.new_elegibilidade_automatica.DataValue;
			var nomeRegistro = crmForm.all.new_name.DataValue;

			var idRegistro = crmForm.ObjectId;

			var nomeTecnicoEntidade = crmForm.ObjectTypeName;

			var oParam = "nomeCampo=" + nomeCampo + "&nomeTecnicoEntidade=" + nomeTecnicoEntidade;

			var url = '/MRVWeb/LogAlteracao/MotivoAlteracaoCampo.aspx?' + oParam;

			var retorno = openStdDlg(url, null, 750, 440);
			if (retorno != null && retorno != undefined) {
				var cmd = new RemoteCommand("MrvService", "CriarMotivoAlteracaoElegibilidade", "/MRVCustomizations/");
				cmd.SetParameter("nomeTecnicoEntidade", nomeTecnicoEntidade);
				cmd.SetParameter("registroId", idRegistro);
				cmd.SetParameter("nomeRegistro", nomeRegistro);
				cmd.SetParameter("valorCampo", valorCampo);
				cmd.SetParameter("motivo", retorno.Motivo);
				cmd.SetParameter("justificativa", retorno.Justivicativa);
				var oResult = cmd.Execute();

				if (!crmForm.TratarRetornoRemoteCommand(oResult, "CriarMotivoAlteracaoElegibilidade")) {
					alert("Operação cancelada.");
					event.returnValue = false;
					return false;
				}
			} else {
				alert("Operação cancelada.");
				event.returnValue = false;
				return false;
			}
		}
		if(!crmForm.AtualizarDataPrevistaEntregaChaves())
		{
			event.returnValue = false;
			return false;
		}
	}
} catch (erro) {
	alert("Ocorreu um erro no formulário.\n" + erro.description);
	event.returnValue = false;
	return false;
}