﻿formularioValido = true;
try {

    TipoModulo = {
        APARTAMENTO: 1,
        CASA: 2
    }

    TipoEntidade = {
        MODULO_APTO: 1,
        MODULO_VILLAGE: 2
    }
    TipoLogCampos = {
        ELEGIBILIDADE: 1,
        DATA_PREVISTA_ENTREGA_CHAVES: 2
    }

    function loadScript() {
        var oScript = document.createElement('script');
        oScript.type = "text/javascript";
        oScript.src = "/_static/Base.js";
        var oHead = document.getElementsByTagName('head')[0];
        oHead.appendChild(oScript);

        oScript.onreadystatechange = function() {
            if (this.readyState == 'complete' || this.readyState == 'loaded')
                Load();
        }
    }

    loadScript();

    function Load() {

        crmForm.valorCampoElegibilidade = crmForm.all.new_elegibilidade_automatica.DataValue;
        dataAtualEntregaDeChaves = crmForm.all.new_data_prevista_entrega.DataValue;

        if (crmForm.UsuarioPertenceEquipe(Equipe.ENGENHARIA_PREVISAO_ENTREGA)) {
            crmForm.all.new_data_prevista_entrega.Disabled = false;
        } else {
            crmForm.all.new_data_prevista_entrega.Disabled = true;
        }
        
        StatusModulo = {
            INICIADO: 1,
            EM_ANALISE: 2,
            MONTADO: 3,
            APROVADO: 4,
            PENDENCIA: 5,
            ABORTADO: 6,
            REAVALIACAO: 7
        }

        //MODULO_DOC

        if (crmForm.all.new_cartapropostaconsultaprviadoc.DataValue == 0) {
            crmForm.all.new_cartapropostaconsultaprviadoc.DataValue = null;
        }

        if (crmForm.all.new_matrizdeconjugaodoc.DataValue == 0) {
            crmForm.all.new_matrizdeconjugaodoc.DataValue = null
        }

        if (crmForm.all.new_seguroterminodeobradoc.DataValue == 0) {
            crmForm.all.new_seguroterminodeobradoc.DataValue = null
        }

        if (crmForm.all.new_ficharesumodoempreendimentodoc.DataValue == 0) {
            crmForm.all.new_ficharesumodoempreendimentodoc.DataValue = null
        }

        if (crmForm.all.new_cronograma.DataValue == 0) {
            crmForm.all.new_cronograma.DataValue = null
        }

        //Habilitar e Desabilitar Seção - Selecionar so o primeiro campo da seção----------------------

        if (crmForm.all.new_cef.DataValue == false) {
            document.getElementById("new_statusdomodulo_d").parentNode.parentNode.style.display = "none";
        } else {
            document.getElementById("new_statusdomodulo_d").parentNode.parentNode.style.display = "inline";
        }

        //Habilitar e Desabilitar Seção - Selecionar so o primeiro campo da seção---------------------

        if (crmForm.all.new_statusdomodulo.DataValue == StatusModulo.PENDENCIA) {
            document.getElementById("new_dataenviodapendencia_d").parentNode.parentNode.style.display = "inline";
        } else {
            document.getElementById("new_dataenviodapendencia_d").parentNode.parentNode.style.display = "none";
        }

        crmForm.ValidarAtualizacao = function() {
            if (crmForm.all.new_datadacontratao.DataValue == null && crmForm.ObjectId != null) {
                var cmd = new RemoteCommand("MrvService", "ValidarAtualizacaoModuloFinanciamentoBancario", "/MrvCustomizations/");
                cmd.SetParameter("idModulo", crmForm.ObjectId);
                cmd.SetParameter("tipoModulo", TipoModulo.APARTAMENTO);

                var resultado = cmd.Execute();
                if (resultado.ReturnValue.Mrv.Valid) {
                    return true;
                } else {
                    alert(resultado.ReturnValue.Mrv.Mensagem.toString());
                    return false;
                }
            } else {
                return true;
            }
        }

        if (crmForm.UsuarioPertenceEquipe(Equipe.ADMINISTRADORES) || crmForm.UsuarioPertenceEquipe(Equipe.CO_Elegibilidade_Automatica)) {
            crmForm.all.new_elegibilidade_automatica.Disabled = false;
        } else {
            crmForm.all.new_elegibilidade_automatica.Disabled = true;
        }
        if (crmForm.UsuarioPertenceEquipe(Equipe.ADMINISTRADORES)) {
            crmForm.all.new_banco_financiamentoid.Disabled = false;
        } else {
            crmForm.all.new_banco_financiamentoid.Disabled = true;
        }

        if (crmForm.UsuarioPertenceEquipe(Equipe.ADMINISTRADORES) || crmForm.UsuarioPertenceEquipe(Equipe.CI_PASTA_MAE)) {
            crmForm.all.new_pasta_mae.Disabled = false;
        } else {
            crmForm.all.new_pasta_mae.Disabled = true;
        }

        AtualizarDadosPorBlocos();
    }

    function AtualizarDadosPorBlocos() {
        document.getElementById('navnew_new_modulo_new_blocos').attachEvent("onclick", AtualizarDadosPorBlocosClick);
    }

    function AtualizarDadosPorBlocosClick() {
        var frame = 'areanew_new_modulo_new_blocosFrame';
        document.frames(frame).frameElement.attachEvent("onreadystatechange", AtualizarDadosPorBlocosFrameOnReadyStateChange);
    }

    function AtualizarDadosPorBlocosFrameOnReadyStateChange() {
        var iframeMaster = "areanew_new_modulo_new_blocosFrame";
        var adicionar = "_MBtoplocAssocObj10020newnewmodulonewblocos1";
        var remover = "_MIdoActionExcrmGrid10020topcrmFormSubmitcrmFormSubmitIdvaluedisassociatetopcrmFormSubmitcrmFormSubmitObjectTypevaluetabareanewnewmodulonewblocosassociationNamenewnewmodulonewblocosroleOrd1";

        if (document.frames(iframeMaster).frameElement.readyState == 'complete') {
            if (document.frames(iframeMaster).document.all[adicionar] != null) {
                if (document.frames(iframeMaster).document.all[adicionar].action.split(';').length == 2) {
                    document.frames(iframeMaster).document.all[adicionar].action += " parent.crmForm.AtualizarInclusaoRegistros();";
                }
            }

            if (document.frames(iframeMaster).document.all[remover] != null) {
                if (document.frames(iframeMaster).document.all[remover].action.split(';').length == 2) {
                    document.frames(iframeMaster).document.all[remover].action += " parent.crmForm.AtualizarExclusaoRegistros();";
                }
            }
        }
    }

    crmForm.AtualizarInclusaoRegistros = function() {
        crmForm.AtualizarDataLimiteAquisicaoKit();
    }

    crmForm.AtualizarExclusaoRegistros = function() {
        crmForm.AtualizarDataLimiteAquisicaoKit();
    }

    crmForm.AtualizarDataLimiteAquisicaoKit = function() {
        var tipoModulo = TipoModulo.APARTAMENTO;
        var cmd = new RemoteCommand("MrvService", "AtualizarDataLimiteAquisicaoKitModuloFinanciamento", "/MRVCustomizations/");
        cmd.SetParameter("idModuloFinanciamento", crmForm.ObjectId);
        cmd.SetParameter("tipoModuloFinanciamento", tipoModulo);
        var resultado = cmd.Execute();
        crmForm.TratarRetornoRemoteCommand(resultado, "AtualizarDataLimiteAquisicaoKit");
    }

    function crmForm.ValidarPermiteAlteracaoPastaMae() {
        var cmd = new RemoteCommand("MrvService", "ValidarPermiteAlteracaoPastaMae", "/MrvCustomizations/");
        cmd.SetParameter("moduloId", crmForm.ObjectId);
        cmd.SetParameter("tipoId", TipoModulo.APARTAMENTO);

        var result = cmd.Execute();
        if (result.Success) {
            if (!result.ReturnValue.Mrv.Resultado) {
                alert("Para marcação de Pasta mãe todos os blocos do Módulo de Financiamento devem possuir data real de Habite-se");
                event.returnValue = false;
                return false;
            } else {
                event.returnValue = true;
                return true;
            }
        }
    }



    crmForm.AtualizarDataPrevistaEntregaChaves = function() {
        try {

            var dataInserida;

            if (dataAtualEntregaDeChaves == null) {
                dataAtualEntregaDeChaves = "";
            }

            if (crmForm.all.new_data_prevista_entrega.DataValue == null) {
                dataInserida = "";
            } else {
                dataInserida = crmForm.all.new_data_prevista_entrega.DataValue;
            }

            if (dataInserida.toString() != dataAtualEntregaDeChaves.toString()) {
                var nomeCampo = crmForm.all.new_data_prevista_entrega.id;
                var valorCampo = crmForm.all.new_data_prevista_entrega.DataValue != null ? crmForm.all.new_data_prevista_entrega.DataValue.toLocaleDateString() : "";
                var valorCampoAnterior = dataAtualEntregaDeChaves != "" ? dataAtualEntregaDeChaves.toLocaleDateString() : dataAtualEntregaDeChaves;
                var idRegistro = crmForm.ObjectId;
                var nomeTecnicoEntidade = crmForm.ObjectTypeName;

                var oParam = "nomeCampo=" + nomeCampo + "&nomeTecnicoEntidade=" + nomeTecnicoEntidade + "&tipoLog=" + TipoLogCampos.DATA_PREVISTA_ENTREGA_CHAVES;

                var url = '/MRVWeb/LogAlteracao/MotivoAlteracaoCampo.aspx?' + oParam;

                var retorno = openStdDlg(url, null, 750, 440);

                if (retorno != null && retorno != undefined) {
                    var cmd = new RemoteCommand("MrvService", "AtualizarDataPrevistaEntregaChaves", "/MRVCustomizations/");
                    cmd.SetParameter("idModulo", idRegistro);
                    cmd.SetParameter("tipo", TipoEntidade.MODULO_APTO);
                    cmd.SetParameter("dataEntregaAtualizada", valorCampo);
                    cmd.SetParameter("justificativaAlteracao", retorno.Justivicativa);
                    cmd.SetParameter("dataEntregaAnterior", valorCampoAnterior);

                    var resultado = cmd.Execute();
                    if (crmForm.TratarRetornoRemoteCommand(resultado, "AtualizarDataPrevistaEntregaChaves")) {
                        if (resultado.ReturnValue.Mrv.dataAtualizada == true) {
                            if (typeof (resultado.ReturnValue.Mrv.mensagem) != "object") {
                                alert(resultado.ReturnValue.Mrv.mensagem);
                            }
                            return true;
                        } else {
                            if (dataAtualEntregaDeChaves == "") {
                                dataAtualEntregaDeChaves = null;
                            }
                            crmForm.all.new_data_prevista_entrega.DataValue = dataAtualEntregaDeChaves;
                            crmForm.all.new_data_prevista_entrega.SetFocus();
                            alert(resultado.ReturnValue.Mrv.mensagem);
                        }
                    } else {
                        if (dataAtualEntregaDeChaves == "") {
                            dataAtualEntregaDeChaves = null;
                        }
                        crmForm.all.new_data_prevista_entrega.DataValue = dataAtualEntregaDeChaves;
                        crmForm.all.new_data_prevista_entrega.SetFocus();
                        alert("Operação cancelada.");
                        event.returnValue = false;
                        return false;
                    }
                } else {
                    alert("Operação cancelada.");
                    event.returnValue = false;
                    return false;
                }
            } else {
                return true;
            }
        } catch (error) {
            alert("Ocorreu um erro no formulário.\n" + error.description);
            event.returnValue = false;
            return false;
        }
    }

} catch (error) {
    formularioValido = false;
    alert("Ocorreu um erro no formulário.\n" + error.description);
}