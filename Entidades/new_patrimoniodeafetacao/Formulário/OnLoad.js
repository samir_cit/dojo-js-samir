﻿formularioValido = true;
try {

    function loadScript() {
        var oScript = document.createElement('script');
        oScript.type = "text/javascript";
        oScript.src = "/_static/Base.js";
        var oHead = document.getElementsByTagName('head')[0];
        oHead.appendChild(oScript);

        oScript.onreadystatechange = function() {
            if (this.readyState == 'complete' || this.readyState == 'loaded')
                Load();
        }
    }

    loadScript();

    function Load() {
        function camposObrigatorios() {
            crmForm.SetFieldReqLevel("new_data_prevista_protocolo_pa", true);
            crmForm.SetFieldReqLevel("new_dataprevistadeconclusopa", true);
        }

        camposObrigatorios();
    }
    
} catch (erro) {
    formularioValido = false;
    alert("Ocorreu um erro no formulário.\n" + erro.description);
}