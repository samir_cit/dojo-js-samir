﻿function verificaObrigatoriedadePA() {
    //verifica condições
    if (crmForm.all.new_statusdocartrio.DataValue == STATUS_CARTORIO_DEFERIDO && crmForm.all.new_dataconclusodori.DataValue != null && crmForm.all.new_patrimonio_afetacao.DataValue == PATRIMONIO_AFETACAO_EMPREENDIMENTO_SIM) {
        var cmd = new RemoteCommand("MrvService", "PatrimonioAfetacaoRequerido", "/MRVCustomizations/");
        cmd.SetParameter("empreendimentoId", crmForm.all.new_empreendimento_pa.DataValue[0].id);

        var result = cmd.Execute();
        if (result.Success) {
            if (!result.ReturnValue.Mrv.Resultado) {
                alert("Para que as datas de afetação sejam enviadas para o SAP, favor atualizar as informações no empreendimento referentes a Status do Cartório, Registro de Incorporação e Afetação do empreendimento.");
                event.returnValue = false;
                return false;
            }
        }
    }
}

// Transformar caracteres em maiusculos------------------------------------//
var elm = document.getElementsByTagName("input");
for (i = 0; i < elm.length; i++) {
    if (elm[i].type == "text")
        if (elm[i].DataValue != null) {
        try {
            elm[i].DataValue = elm[i].DataValue.toUpperCase();
        }
        catch (e) { }
    }
}