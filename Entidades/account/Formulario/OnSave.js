﻿try {
    if (!formularioValido) {
        alert("Ocorreu um erro no formulário. Impossível salvar.");
        event.returnValue = false;
        return false;
    }
    crmForm.SalvaDiaMesNascimento();

    var endereco = "";
    var enderecoResumido = "";
    var complemento = "";
    var bairro = "";
    
    if (crmForm.all.telephone1.DataValue == null && crmForm.all.address1_telephone3.DataValue == null) {
        alert("O número de telefone residencial ou o telefone celular devem estar preenchidos para finalizar o cadastro do Cliente");
		event.returnValue = false;
        return false;
    }

    if(crmForm.all.address1_line1.DataValue != null)
        endereco = crmForm.all.address1_line1.DataValue.toString();

    if(crmForm.all.address2_line1.DataValue != null)
        enderecoResumido = crmForm.all.address2_line1.DataValue.toString();

    if(crmForm.all.new_complemento.DataValue != null)
        complemento = crmForm.all.new_complemento.DataValue.toString();

    if(crmForm.all.new_bairro.DataValue != null)
        bairro = crmForm.all.new_bairro.DataValue.toString();

    var nome = crmForm.all.name.DataValue.toString();
    if(endereco.indexOf(";") != -1 ||  nome.indexOf(";") != -1 || enderecoResumido.indexOf(";") != -1 ||  complemento.indexOf(";") != -1 || bairro.indexOf(";") != -1)
    {
        alert('Não é possível salvar com ";" nos campos do endereço ou no nome do cliente.');
        event.returnValue = false;
        return false;
    }
    
    if (!crmForm.ValidarEndereco()) {
        event.returnValue = false;
        return false;
    }

    var cidadeId = "";
    var ddd = "";
    if(crmForm.all.new_endereco_nacional.DataValue){
        if (crmForm.all.new_cidadeid.DataValue != null){
            cidadeId = crmForm.all.new_cidadeid.DataValue[0].id;
            
            var cmdDadosCidade = new RemoteCommand("MrvService", "retornarEstadoEDDD", "/MRVCustomizations/");
            cmdDadosCidade.SetParameter("cidadeId", cidadeId);
            var oResultado = cmdDadosCidade.Execute();
            if (oResultado.ReturnValue.Mrv.Error && oResultado.ReturnValue.Mrv.ErrorSoap){
                ddd = "";
            }
            else {
                if (crmForm.TratarRetornoRemoteCommand(oResultado, "retornarEstadoEDDD")) {
                    if (typeof (oResultado.ReturnValue.Mrv.ddd) == "object"){ //object = retorno vazio, ou seja, não existe ddd para a cidade informada
                        ddd = "";
                    }
                    else{
                        ddd = oResultado.ReturnValue.Mrv.ddd;
                    }
                }
            }
        }
    }

    var ddi = (crmForm.all.new_pais.SelectedIndex == Pais.BRASIL ? DDI.Brasil : "");

    var cpfCnpj = "";

    if(crmForm.all.new_tipodecliente.DataValue == 0){ //CPF
        cpfCnpj = crmForm.all.new_cpf == null ? '' : crmForm.all.new_cpf.DataValue;
    }
    else {
        cpfCnpj = crmForm.all.new_cnpj == null ? '' : crmForm.all.new_cnpj.DataValue;
    }
    	
    //Valida telefones
    var cmdValidaTelefones = new RemoteCommand("SrvCliente", "ValidaCadastroCliente", "/MRVCustomizations/");

    if(crmForm.all.new_tipodecliente.DataValue == 0) //CPF
    {
        if(crmForm.all.new_cpf.DataValue != null){
                cmdValidaTelefones.SetParameter("cpfCnpj", crmForm.all.new_cpf.DataValue);
        }
        else{
             cmdValidaTelefones.SetParameter("cpfCnpj", '');
        }
    }
    else{
        if(crmForm.all.new_cnpj.DataValue != null){
             cmdValidaTelefones.SetParameter("cpfCnpj", crmForm.all.new_cnpj.DataValue);
        }
        else {
            cmdValidaTelefones.SetParameter("cpfCnpj", '');
        }
    }

    if(crmForm.all.name.DataValue != null){
        cmdValidaTelefones.SetParameter("nome", crmForm.all.name.DataValue);
    }
    else{
        cmdValidaTelefones.SetParameter("nome", '');
    }

    if(crmForm.all.address1_postalcode.DataValue != null){
        cmdValidaTelefones.SetParameter("cep", crmForm.all.address1_postalcode.DataValue);
    }
    else{
        cmdValidaTelefones.SetParameter("cep", '');
    }

    cmdValidaTelefones.SetParameter("DDD", ddd.toString());
    cmdValidaTelefones.SetParameter("DDI", ddi.toString());

    
    if(crmForm.all.telephone1.DataValue != null){
        cmdValidaTelefones.SetParameter("telefoneResidencial", crmForm.all.telephone1.DataValue);
    }
    else {
        cmdValidaTelefones.SetParameter("telefoneResidencial", '');
    }
    if(crmForm.all.telephone2.DataValue != null){
        cmdValidaTelefones.SetParameter("telefoneComercial", crmForm.all.telephone2.DataValue);
    }
    else {
        cmdValidaTelefones.SetParameter("telefoneComercial", '');
    }

    if(crmForm.all.address1_telephone3.DataValue != null){
        cmdValidaTelefones.SetParameter("celular", crmForm.all.address1_telephone3.DataValue);
    }
    else {
        cmdValidaTelefones.SetParameter("celular", '');
    }

    if(crmForm.all.fax.DataValue != null){
        cmdValidaTelefones.SetParameter("faxCelular2", crmForm.all.fax.DataValue);
    }
    else{
        cmdValidaTelefones.SetParameter("faxCelular2", '');
    }

    if(crmForm.all.new_outrotelefone.DataValue != null){
        cmdValidaTelefones.SetParameter("outroTelefone", crmForm.all.new_outrotelefone.DataValue);
    }
    else {
        cmdValidaTelefones.SetParameter("outroTelefone", '');
    }

    if(crmForm.all.new_telefoneum.DataValue != null){
        cmdValidaTelefones.SetParameter("telefoneEnriquecimento1", crmForm.all.new_telefoneum.DataValue);
    }
    else {
        cmdValidaTelefones.SetParameter("telefoneEnriquecimento1", '');
    }

    if(crmForm.all.new_telefonedois.DataValue != null){
        cmdValidaTelefones.SetParameter("telefoneEnriquecimento2", crmForm.all.new_telefonedois.DataValue);
    }
    else{
        cmdValidaTelefones.SetParameter("telefoneEnriquecimento2", '');
    }

    if(crmForm.all.new_telefonetrs.DataValue !=null){
        cmdValidaTelefones.SetParameter("telefoneEnriquecimento3", crmForm.all.new_telefonetrs.DataValue);
    }
    else{
        cmdValidaTelefones.SetParameter("telefoneEnriquecimento3", '');
    }

    if(crmForm.all.new_telefonequatro.DataValue != null){
        cmdValidaTelefones.SetParameter("telefoneEnriquecimento4", crmForm.all.new_telefonequatro.DataValue);
    }
    else{
        cmdValidaTelefones.SetParameter("telefoneEnriquecimento4", '');
    }

    if(crmForm.all.new_telefonecinco.DataValue != null){
        cmdValidaTelefones.SetParameter("telefoneEnriquecimento5", crmForm.all.new_telefonecinco.DataValue);
    }
    else{
        cmdValidaTelefones.SetParameter("telefoneEnriquecimento5", '');
    }
    
    if(crmForm.all.emailaddress1.DataValue != null){
        cmdValidaTelefones.SetParameter("email", crmForm.all.emailaddress1.DataValue);
    }
    else{
        cmdValidaTelefones.SetParameter("email", '');
    }
    
    cmdValidaTelefones.SetParameter("indicadorEnderecoNacional", crmForm.all.new_endereco_nacional.DataValue);
    			
    var oResult = cmdValidaTelefones.Execute();
                   
    if(!oResult.ReturnValue.Mrv.Sucesso){
        alert(oResult.ReturnValue.Mrv.Mensagem);
        event.returnValue = false;
        return false;
    }
    
    // Transformar caracteres em maiusculos------------------------------------//
    var elm = document.getElementsByTagName("input");
    for (i = 0; i < elm.length; i++) {
        if (elm[i].type == "text")
            if (elm[i].DataValue != null) {
            try {
                elm[i].DataValue = elm[i].DataValue.toUpperCase();
            }
            catch (e) { }
        }
    }
    
    //Habilitando o campo classificação
    if (crmForm.IsDirty)
    {
        crmForm.all.accountclassificationcode.Disabled = false;
        crmForm.all.new_cidadeid.Disabled = false;
        crmForm.all.address1_line1.Disabled = false;
        crmForm.all.address2_line1.Disabled = false;
        crmForm.all.new_tipologradouro.Disabled = false;
        crmForm.all.new_bairro.Disabled = false;
        crmForm.all.new_estado.Disabled = false;
        crmForm.all.new_pais.Disabled = false;
        crmForm.all.new_cidadeid.Disabled = false;
    }

    if (!crmForm.all.new_tipodecliente.DataValue) {
        var ret = new_cpf_onchange0();
        event.returnValue = ret;
        return false;
    }
    else {
        var ret = new_cnpj_onchange0();
        event.returnValue = ret;
        return false;
    }
    
    if(crmForm.all.name.DataValue != null)
    {
        var nome = crmForm.all.name.DataValue;
        crmForm.all.name.DataValue = nome.replace("*", "");
    } 	            
} catch (error) {
    alert("Ocorreu um erro no formulário.\n" + error.description);
    event.returnValue = false;
    return false;
}