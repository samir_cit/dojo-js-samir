﻿crmForm.all.telephone1.value="";
 
var valorlookupcidade = new Array();  
valorlookupcidade = crmForm.all.new_cidadeid.DataValue;

if(valorlookupcidade != null)
{
    var cmdDadosCidade = new RemoteCommand("MrvService", "retornarEstadoEDDD", "/MRVCustomizations/");
    cmdDadosCidade.SetParameter("cidadeId", valorlookupcidade[0].id);
    var oResultado = cmdDadosCidade.Execute();
    if (oResultado.Success) {
        // Atribui o BRASIL como item selecionado no campo Pais
        crmForm.all.new_pais.DataValue = Pais.BRASIL;
        crmForm.all.new_pais.Disabled = true;

        switch (oResultado.ReturnValue.Mrv.estado) {
            case "AC":
                crmForm.all.new_estado.SelectedIndex = EstadoPicklist.AC;
                break;
            case "AL":
                crmForm.all.new_estado.SelectedIndex = EstadoPicklist.AL;
                break;
            case "AM":
                crmForm.all.new_estado.SelectedIndex = EstadoPicklist.AM;
                break;
            case "AP":
                crmForm.all.new_estado.SelectedIndex = EstadoPicklist.AP;
                break;
            case "BA":
                crmForm.all.new_estado.SelectedIndex = EstadoPicklist.BA;
                break;
            case "CE":
                crmForm.all.new_estado.SelectedIndex = EstadoPicklist.CE;
                break;
            case "DF":
                crmForm.all.new_estado.SelectedIndex = EstadoPicklist.DF;
                break;
            case "ES":
                crmForm.all.new_estado.SelectedIndex = EstadoPicklist.ES;
                break;
            case "GO":
                crmForm.all.new_estado.SelectedIndex = EstadoPicklist.GO;
                break;
            case "MA":
                crmForm.all.new_estado.SelectedIndex = EstadoPicklist.MA;
                break;
            case "MG":
                crmForm.all.new_estado.SelectedIndex = EstadoPicklist.MG;
                break;
            case "MS":
                crmForm.all.new_estado.SelectedIndex = EstadoPicklist.MS;
                break;
            case "MT":
                crmForm.all.new_estado.SelectedIndex = EstadoPicklist.MT;
                break;
            case "PA":
                crmForm.all.new_estado.SelectedIndex = EstadoPicklist.PA;
                break;
            case "PB":
                crmForm.all.new_estado.SelectedIndex = EstadoPicklist.PB;
                break;
            case "PE":
                crmForm.all.new_estado.SelectedIndex = EstadoPicklist.PE;
                break;
            case "PI":
                crmForm.all.new_estado.SelectedIndex = EstadoPicklist.PI;
                break;
            case "PR":
                crmForm.all.new_estado.SelectedIndex = EstadoPicklist.PR;
                break;
            case "RJ":
                crmForm.all.new_estado.SelectedIndex = EstadoPicklist.RJ;
                break;
            case "RN":
                crmForm.all.new_estado.SelectedIndex = EstadoPicklist.RN;
                break;
            case "RO":
                crmForm.all.new_estado.SelectedIndex = EstadoPicklist.RO;
                break;
            case "RR":
                crmForm.all.new_estado.SelectedIndex = EstadoPicklist.RR;
                break;
            case "RS":
                crmForm.all.new_estado.SelectedIndex = EstadoPicklist.RS;
                break;
            case "SC":
                crmForm.all.new_estado.SelectedIndex = EstadoPicklist.SC;
                break;
            case "SE":
                crmForm.all.new_estado.SelectedIndex = EstadoPicklist.SE;
                break;
            case "SP":
                crmForm.all.new_estado.SelectedIndex = EstadoPicklist.SP;
                break;
            case "TO":
                crmForm.all.new_estado.SelectedIndex = EstadoPicklist.TO;
                break;
            default:
                crmForm.all.new_estado.SelectedIndex = 0;
                break;        
        }
}
