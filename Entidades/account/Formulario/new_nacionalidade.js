﻿if (crmForm.all.new_nacionalidade.DataValue == null || crmForm.all.new_nacionalidade.DataValue == 2) {
    crmForm.all.new_naturalidadeid.DataValue = null;
    crmForm.all.new_naturalidadeid.Disabled = true;
    if (crmForm.FormType == TypeCreate || crmForm.FormType == TypeUpdate) {
        crmForm.all.new_naturalidadeid.ForceSubmit = true;
    }
}
else {
    crmForm.all.new_naturalidadeid.Disabled = false;
}