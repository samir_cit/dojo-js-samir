﻿try {
    if (!formularioValido) {
        alert("Ocorreu um erro no formulário. Impossível salvar.");
        event.returnValue = false;
        return false;
    }
    
    //Código aqui
    if(!crmForm.ValidaCamposTextoTransformadosPickList()){
        event.returnValue = false;
        return false;
    }    
    //Fim coódigo.
    
} catch (error) {
    alert("Ocorreu um erro no formulário.\n" + error.description);
    event.returnValue = false;
    return false;
}