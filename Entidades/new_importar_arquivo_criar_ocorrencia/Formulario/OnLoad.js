﻿formularioValido = true;
try {

    crmForm.ValidaCamposTextoTransformadosPickList = function() {
        var retorno = true;
        if(crmForm.all.new_tipo_chamado.DataValue == null){
            retorno = false;
            crmForm.all.new_tipo_chamado.SetFocus();
            alert("Você deve fornecer um valor pra Tipo de Chamado");
        }
        else if(crmForm.all.new_recebido_por.DataValue == null){
            retorno = false;
            crmForm.all.new_recebido_por.SetFocus();
            alert("Você deve fornecer um valor pra Recebido Por");
        }
        return retorno;
    }    

    crmForm.EscondeFechamentoAutomatico = function(valor) {
        crmForm.EscondeCampo("new_dias_isencao", valor);
        crmForm.EscondeCampo("new_data_fechamento", valor);
        crmForm.EscondeCampo("new_motivo_isencao", valor);
    }

    crmForm.SelecionarItem = function(atributoCorrente, valor) {
        var atributo = eval("crmForm.all." + atributoCorrente);
        for (var index = 0; index < atributo.length; index++) {
            if (atributo[index].value == valor) {
                atributo[index].selected = true;
                break;
            }
        }
    }
    crmForm.CarregarPicklist = function(atributo, atributoCorrente) {
        var objAtt = eval("crmForm.all." + atributoCorrente);
        //Verificar se existe valor nos campos que simulam picklist
        var valorAtt = null;
        if (objAtt.DataValue != null)
            valorAtt = objAtt.DataValue;

        var cmd = new RemoteCommand("MrvService", "ObterHTMLPicklistDinamico", "/MRVCustomizations/");
        cmd.SetParameter("entidade", "incident");
        cmd.SetParameter("atributo ", atributo);
        cmd.SetParameter("atributoCorrente", atributoCorrente);
        var result = cmd.Execute();
        if (crmForm.TratarRetornoRemoteCommand(result, "ObterHTMLPicklistDinamico")) {
            objAtt.parentElement.innerHTML = result.ReturnValue.Mrv.Atributos.toString();
            if (valorAtt != null)
                crmForm.SelecionarItem(atributoCorrente, valorAtt);
        }
        else
            return "";

    }
    function LimparCamposAssunto() {
        crmForm.all.new_subjectid.DataValue = null;
        crmForm.all.new_assunto_principalid.DataValue = null;
        crmForm.all.new_departamentoid.DataValue = null;
    }

    crmForm.ObterAssuntos = function() {
        if (crmForm.all.new_subjectid.DataValue == null) {
            LimparCamposAssunto();
            return false;
        }
        var cmd = new RemoteCommand("MrvService", "ObterDadosOcorrencia", "/MRVCustomizations/");
        cmd.SetParameter("assuntoId", crmForm.all.new_subjectid.DataValue[0].id);
        var result = cmd.Execute();
        if (!crmForm.TratarRetornoRemoteCommand(result, "ObterDadosOcorrencia"))
            LimparCamposAssunto();
        else if (result.ReturnValue.Mrv.Mensagem) {
            alert(result.ReturnValue.Mrv.Mensagem);
            LimparCamposAssunto();
        }
        else {
            crmForm.AddValueLookup(crmForm.all.new_assunto_principalid, result.ReturnValue.Mrv.IdAssuntoPrincipal, 10092, result.ReturnValue.Mrv.NomeAssuntoPrincipal);
            crmForm.AddValueLookup(crmForm.all.new_departamentoid, result.ReturnValue.Mrv.IdDepartamento, 10091, result.ReturnValue.Mrv.NomeDepartamento);
        }
    }

    function Load() {
        //executar fireonchage dos campos que simulam picklist
        crmForm.all.new_tipo_chamado.FireOnChange();
        crmForm.all.new_recebido_por.FireOnChange();
        crmForm.all.new_criar_como.FireOnChange();
        crmForm.all.new_motivo_isencao.FireOnChange();
        crmForm.all.new_dias_isencao.FireOnChange();
        crmForm.all.new_tipo_importacao.FireOnChange();


        //Se usuário não pertence a equipe Fechamento Automático Ocorrência, esconder os campos de isenção
        if (!crmForm.UsuarioPertenceEquipe("Fechamento Automático Ocorrência"))
            crmForm.EscondeFechamentoAutomatico(true);
            
        //desabilitar formulário se status diferente de Aguardando.
        if (crmForm.all.statuscode.DataValue != 1)
            crmForm.DesabilitarFormulario(true);

    }

    function loadScript() {
        var oHead = document.getElementsByTagName('head')[0];
        var oScript = document.createElement('script');
        oScript.type = "text/javascript";
        oScript.src = "/_static/Base.js";

        oScript.onreadystatechange = function() {
            if (this.readyState == 'complete' || this.readyState == 'loaded') {
                Load();
            }
        }
        oHead.appendChild(oScript);
    }

    loadScript();
} catch (error) {
    formularioValido = false;
    alert("Ocorreu um erro no formulário.\n" + error.description);
}