﻿function ObterDataCriacao() {
    return crmForm.all.createdon.DataValue == null ? new Date() : crmForm.all.createdon.DataValue;
}
function CalcularDataFechamento() {
    var data = ObterDataCriacao();
    data.setDate(data.getDate() + crmForm.all.new_dias_isencao.DataValue);
    return data;
}
function DefinirDataFechamento(data) {
    crmForm.all.new_data_fechamento.DataValue = data;
    crmForm.all.new_data_fechamento.ForceSubmit = true;
}

if (crmForm.all.new_dias_isencao.DataValue != null) {
    DefinirDataFechamento(CalcularDataFechamento());
}
else
    DefinirDataFechamento(null);

crmForm.SetFieldReqLevel("new_motivo_isencao", (crmForm.all.new_dias_isencao.DataValue != null));