﻿if (crmForm.new_tipo_importacao.DataValue == "1") {
    crmForm.EscondeSecao('new_tipo_chamado', true);
    crmForm.SetFieldReqLevel("new_criar_como", 0);
    crmForm.all.new_criar_como.DataValue = null;
    crmForm.all.new_tipo_chamado.DataValue = null;
    crmForm.all.new_recebido_por.DataValue = null;
}
else {
    crmForm.EscondeSecao('new_tipo_chamado', false);
    crmForm.SetFieldReqLevel("new_criar_como", 1);
}