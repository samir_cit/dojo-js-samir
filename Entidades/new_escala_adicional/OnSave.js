﻿try {
 if (!formularioValido) {
        alert("Ocorreu um erro no formulário. Impossível salvar.");
        event.returnValue = false;
        return false;
    }
    
    if(crmForm.all.new_data_hora_fim.DataValue <= crmForm.all.new_data_hora_inicio.DataValue)
    {
        alert("Data/Hora fim deve ser maior que Data/Hora de início.");
        event.returnValue = false;
        return false;
    }
        
    
} catch (error) {
    formularioValido = false;
    alert("Ocorreu um erro no formulário.\n" + error.description);
}