﻿formularioValido = true;
try {

    function loadScript() {
        var oHead = document.getElementsByTagName('head')[0];
        var oScript = document.createElement('script');
        oScript.type = "text/javascript";
        oScript.src = "/_static/Base.js";

        oScript.onreadystatechange = function() {
            if (this.readyState == 'complete' || this.readyState == 'loaded') {
                Load();
            }
        }
        oHead.appendChild(oScript);
    }

    loadScript();
    
    //codigo executado apos o load de script
    function Load() {
        var gestorEscala = crmForm.UsuarioPertenceEquipe('CV_GESTOR_ESCALA');
        crmForm.all.statuscode.Disabled = !gestorEscala;
        var status = crmForm.all.statuscode.DataValue;
        if (status != 1) {
            crmForm.all.new_corretorid.Disabled = true;
            crmForm.all.new_data_hora_inicio_local.Disabled = true;
            crmForm.all.new_data_hora_fim_local.Disabled = true;
            crmForm.all.ownerid.Disabled = true;
            crmForm.all.statuscode.Disabled = true;
            crmForm.all.new_segue_horario_verao.Disabled = true;
        }
        if (crmForm.FormType == 6)
            crmForm.all.statuscode.Disabled = false;
    }

} catch (error) {
    formularioValido = false;
    alert("Ocorreu um erro no formulário.\n" + error.description);
}