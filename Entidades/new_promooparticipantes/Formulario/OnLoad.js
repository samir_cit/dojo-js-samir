﻿formularioValido = true;
mensagemErro = "Ocorreu um erro no formulário. Impossível salvar.";
var statusGanho = 3;
var statusAguardandoAuditoria = 3;
var statusPreganho = 200001;
var statusParticipanteConfirmado = 3;
try {
    function loadScript() {
        var oScript = document.createElement('script');
        oScript.type = "text/javascript";
        oScript.src = "/_static/Base.js";

        var oHead = document.getElementsByTagName('head')[0];
        oHead.appendChild(oScript);

        oScript.onreadystatechange = function() {
            if (this.readyState == 'complete' || this.readyState == 'loaded')
                Load();
        }
    }
    loadScript();

    function Load() {
        BloqueiaPromocaoAguardandoAuditoria = function() {
            if (window.opener.parent.document.crmForm != undefined) {
                if (window.opener.parent.document.crmForm.ObjectTypeCode == 3)
                    crmForm.all.new_oportunidadeid.Disabled = true;
                else {
                    if (window.opener.parent.document.crmForm.all.statuscode.DataValue == statusAguardandoAuditoria) {
                        crmForm.all.new_oportunidadeid.Disabled = true;
                        alert('Promoção não Auditada - Não é permitido adicionar participante!');
                        window.close();
                    }
                }
            }
        }

        DefinirStatusPorOportunidade = function() {
            var cmd = new RemoteCommand("MrvService", "ObterDadosOportunidade", "/MRVCustomizations/");
            cmd.SetParameter("oportunidadeId", crmForm.all.new_oportunidadeid.DataValue[0].id);
            var result = cmd.Execute();
            if (result.Success) {
                if (result.ReturnValue.Mrv.Error) {
                    formularioValido = false;
                    mensagemErro = result.ReturnValue.Mrv.Error;
                } else if (result.ReturnValue.Mrv.ErrorSoap) {
                    formularioValido = false;
                    mensagemErro = result.ReturnValue.Mrv.ErrorSoap;
                }
                else {
                    var statusOportunidade = result.ReturnValue.Mrv.Status;
                    if (statusOportunidade == statusGanho || statusOportunidade == statusPreganho) {
                        crmForm.all.new_participanteconfirmado.DataValue = new Date();
                        crmForm.all.new_participanteconfirmado.ForceSubmit = true;
                        crmForm.all.statuscode.DataValue = statusParticipanteConfirmado;
                    }
                }
            }
        }

        DefinirNomeParticipante = function() {
            if (crmForm.all.new_oportunidadeid.DataValue != null)
                return "PARTICIPANTE - " + crmForm.all.new_oportunidadeid.DataValue[0].name;
            else {
                formularioValido = false;
                mensagemErro = "É necessário informar a oportunidade.";
            }
        }

        if (crmForm.all.new_promooid.DataValue == null) {
            formularioValido = false;
            mensagemErro = "A inclusão de participantes deve ser feita através da entidade de Promoções.";
            window.close();
        }

        if (!formularioValido) {
            crmForm.DesabilitarCampos("new_oportunidadeid", true);
            alert(mensagemErro);
        }
        else {
            crmForm.FilterLookup(crmForm.all.new_oportunidadeid, 3, "/MRVWeb/FilterLookup/FilterLookup.aspx", "objectTypeCode=300193&filterDefault=false&attributesearch=name");

            crmForm.all.new_removerdolote_c.style.visibility = "hidden";
            crmForm.all.new_removerdolote_d.style.visibility = "hidden";
            crmForm.all.new_confirmarresgate_c.style.visibility = "hidden";
            crmForm.all.new_confirmarresgate_d.style.visibility = "hidden";

            if (crmForm.all.statuscode.DataValue.toString() == "5") {
                if (window.opener.parent.document.crmForm != undefined &&
                    window.opener.parent.document.crmForm.all.new_travarlote != undefined &&
                    window.opener.parent.document.crmForm.all.new_travarlote.DataValue == false) {
                    crmForm.all.new_removerdolote_c.style.visibility = "visible";
                    crmForm.all.new_removerdolote_d.style.visibility = "visible";
                }
            }
            else if (crmForm.all.statuscode.DataValue.toString() == "8") {
                crmForm.all.new_confirmarresgate_c.style.visibility = "visible";
                crmForm.all.new_confirmarresgate_d.style.visibility = "visible";
            }
        }
        BloqueiaPromocaoAguardandoAuditoria();
    }
}
catch (error) {
    formularioValido = false;
    alert("Ocorreu um erro no formulário.\n" + error.description);
}
