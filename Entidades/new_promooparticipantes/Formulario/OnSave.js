﻿try {
    //Atribui um nome para o participante
    crmForm.all.new_name.DataValue = DefinirNomeParticipante();
    crmForm.all.new_name.ForceSubmit = true;
    
    DefinirStatusPorOportunidade();
    
    if (!formularioValido) {
        alert(mensagemErro);
        event.returnValue = false;
        return false;
    }
} 
catch (error) {
    alert("Ocorreu um erro no formulário.\n" + error.description);
    event.returnValue = false;
    return false;
}
