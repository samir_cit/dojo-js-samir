﻿formularioValido = true;
try {
    crmForm.CriarAditivo = function() {
        if (crmForm.IsDirty) {
            alert("Salve antes de Criar o Aditivo.");
            return;
        }
        if (crmForm.FormType == TypeCreate)
            return;
        var status = crmForm.all.statuscode.DataValue;
        if (status == 3 || status == 2) {
            alert((status == 3) ? "Registro já criado." : "Registro cancelado."); ;
            return;
        }
        //Chamar WebService para envio.
        var cmd = new RemoteCommand("MrvService", "CriarAditivoSAPtoCRM", "/MRVCustomizations/");
        cmd.SetParameter("identificador", crmForm.ObjectId);
        cmd.SetParameter("tipoVenda", "IPTU");
        var resultado = cmd.Execute();
        crmForm.TratarRetornoRemoteCommand(resultado, "CriarAditivoSAPtoCRM");
        crmForm.Recarregar();
    }

    crmForm.Recarregar = function() {
        crmForm.SubmitCrmForm(1, true, true, false);
    }
    
    function Load() {
        //Esconder campos
        if (crmForm.all.new_log.DataValue == null) {
            crmForm.EscondeCampo("new_log", true);
        }
        crmForm.EscondeCampo("statuscode", true);

        if (crmForm.all.statuscode.DataValue == 3) {
            crmForm.DesabilitarFormulario(true);
        }
    }
    
    function loadScript() {
        var oHead = document.getElementsByTagName('head')[0];
        var oScript = document.createElement('script');
        oScript.type = "text/javascript";
        oScript.src = "/ISV/Tridea/JavaScript/Base.js";

        oScript.onreadystatechange = function() {
            if (this.readyState == 'complete' || this.readyState == 'loaded') {
                Load();
            }
        }
        oHead.appendChild(oScript);
    }

    loadScript();
    
} catch (error) {
    formularioValido = false;
    alert("Ocorreu um erro no formulário.\n" + error.description);
}