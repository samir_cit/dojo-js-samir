﻿formularioValido = true;
try {

    crmForm.CalcularValorReembolso = function() {
        var valorReembolso = 0;
        //linha 1
        if (crmForm.all.new_quantidade_parcelas_1.DataValue != null && crmForm.all.new_valor_1.DataValue != null && crmForm.all.new_vencimento_parcela_1.DataValue != null) {
            valorReembolso += crmForm.all.new_quantidade_parcelas_1.DataValue * crmForm.all.new_valor_1.DataValue;
        }
        //linha 2
        if (crmForm.all.new_quantidade_parcelas_2.DataValue != null && crmForm.all.new_valor_2.DataValue != null && crmForm.all.new_vencimento_parcela_2.DataValue != null) {
            valorReembolso += crmForm.all.new_quantidade_parcelas_2.DataValue * crmForm.all.new_valor_2.DataValue;
        }
        //linha 3
        if (crmForm.all.new_quantidade_parcelas_3.DataValue != null && crmForm.all.new_valor_3.DataValue != null && crmForm.all.new_vencimento_parcela_3.DataValue != null) {
            valorReembolso += crmForm.all.new_quantidade_parcelas_3.DataValue * crmForm.all.new_valor_3.DataValue;
        }


        crmForm.all.new_valor_reembolso.DataValue = valorReembolso;
        crmForm.all.new_valor_reembolso.ForceSubmit = true;
    }
    
    

} catch (error) {
    formularioValido = false;
	alert("Ocorreu um erro no formulário.\n" + error.description);
}