﻿formularioValido = true;
try {
	
	//mascara de CEP--------------------------------------
	var oField = event.srcElement;
	if (typeof (oField) != "undefined" && oField != null) {
		var sTmp = oField.DataValue.replace(/[^0-9]/g, "");
		if (sTmp.length != 8) {
			oField.DataValue = '';
			alert("Cep fora da faixa 8 dígitos sequenciais! Ex: 31080140");
		} else {
			oField.DataValue = sTmp.substr(0, 2) + "." + sTmp.substr(2, 3) + "-" + sTmp.substr(5, 3);
		}
	}
		
	function Load() {

	    MENSAGEM = {
	        TERRENO_SEM_PERMUTA: "Terrenos sem permuta física devem ter o campo 'Valor R$ Permuta física contábil' e 'Valor R$ Permuta física mercado' vazios",
	        CODIGO_TERRENO_SAP_INVALIDO: "O código do SAP do terreno deve iniciar com 13"
	    }

	    CODIGO_INICIAL_SAP_TERRENO = "13";
	    
		//Função para esconder campos
		crmForm.EscondeSecao = function(camposParent, esconde) {
			var tipo = "inline";
			if (esconde)
				tipo = "none";

			var tmp = camposParent.split(";");
			for (index = 0; index < tmp.length; index++) {
				document.getElementById(tmp[index] + "_d").parentNode.parentNode.style.display = tipo;
			}
		}
		crmForm.EsconderCamposPermuta = function() {
			if (crmForm.all.new_existepermuta.DataValue != null && !crmForm.all.new_existepermuta.DataValue) {
				
				//Esconder campos de quantidade
				crmForm.EscondeCampo("new_qtde_permuta_dentro_local", true);
				crmForm.EscondeCampo("new_qtde_permuta_fora_local", true);
				crmForm.EscondeCampo("new_valor_permuta_fisica_contabil", true);
				crmForm.EscondeCampo("new_valor_permuta_fisica_mercado", true);
				
				//Definir valor null para os campos caso não seja verdadeiro
				crmForm.all.new_qtde_permuta_dentro_local.DataValue = null;
				crmForm.all.new_qtde_permuta_fora_local.DataValue = null;
				
				crmForm.SetFieldReqLevel("new_qtde_permuta_dentro_local", false);
				crmForm.SetFieldReqLevel("new_qtde_permuta_fora_local", false);
				crmForm.SetFieldReqLevel("new_valor_permuta_fisica_mercado", false);
				crmForm.SetFieldReqLevel("new_valor_permuta_fisica_contabil", false);

				crmForm.DesabilitarCampos("new_valor_permuta_fisica_contabil", true);
                
                if (crmForm.FormType == TypeCreate || crmForm.FormType == TypeUpdate) {
                    crmForm.new_valor_permuta_fisica_contabil.ForceSubmit = true;
                }				
			} else {
				//Mostrar campos de quantidade
				crmForm.EscondeCampo("new_qtde_permuta_dentro_local", false);
				crmForm.EscondeCampo("new_qtde_permuta_fora_local", false);
				crmForm.EscondeCampo("new_valor_permuta_fisica_contabil", false);
				crmForm.EscondeCampo("new_valor_permuta_fisica_mercado", false);
				
				crmForm.SetFieldReqLevel("new_qtde_permuta_dentro_local", true);
				crmForm.SetFieldReqLevel("new_qtde_permuta_fora_local", true);
				crmForm.SetFieldReqLevel("new_valor_permuta_fisica_mercado", true);

				crmForm.SetFieldReqLevel("new_valor_permuta_fisica_contabil", true);

				crmForm.DesabilitarCampos("new_valor_permuta_fisica_contabil", false);
			}
		}

		crmForm.CamposObrigatoriosCasoPermutaFinanceira = function() {
		    if (crmForm.all.new_permuta_financeira.DataValue) {
		        crmForm.SetFieldReqLevel("new_percentual", true);
		        crmForm.SetFieldReqLevel("new_valor_permuta", true);
		        crmForm.EscondeCampo("new_percentual", false);
		        crmForm.EscondeCampo("new_valor_permuta", false);
		    }
		    else {
		        crmForm.SetFieldReqLevel("new_percentual", false);
		        crmForm.SetFieldReqLevel("new_valor_permuta", false);
		        crmForm.EscondeCampo("new_percentual", true);
		        crmForm.EscondeCampo("new_valor_permuta", true);

		        crmForm.all.new_valor_permuta.DataValue = null;
		        crmForm.all.new_percentual.DataValue = null;
		        crmForm.all.new_valortotalpago.DataValue = null;
		        crmForm.all.new_valortotalpago.ForceSubmit = true;
		        
		        crmForm.CalcularValorTotalContrato();
		    }
		}
		
		//função para verificar se o usuário pertence a determinada equipe
		crmForm.UsuarioPertenceEquipe = function(NomeEquipe) {
			var cmd = new RemoteCommand("MrvService", "UserContainsTeams", "/MRVCustomizations/");
			cmd.SetParameter("teams", NomeEquipe);
			var result = cmd.Execute();
			if (result.Success) {
				return result.ReturnValue.Mrv.Found;
			} else {
				return false;
			}
		}

		//verificar se usuário pode alterar campos da permuta.
		Administrador = crmForm.UsuarioPertenceEquipe("Administradores");
		if (!Administrador) {
			//Se não for administrdor, ocultar a guia Seguro/Fiança
			crmForm.all.tab3Tab.style.display = "none";
		}

		crmForm.HabilitaCamposPermuta = function() {
			//verificar se usuário pode alterar campos da permuta.
			AlterarPermuta = crmForm.UsuarioPertenceEquipe("Permuta Financeira");
			if (!AlterarPermuta) {
				//Bloquear campos da permuta
				crmForm.all.new_existepermuta.Disabled = true;
				crmForm.all.new_permuta_financeira.Disabled = true;
				crmForm.all.new_percentual.Disabled = true;
				crmForm.all.new_valor_permuta.Disabled = true;
				crmForm.all.new_qtde_permuta_dentro_local.Disabled = true;
				crmForm.all.new_qtde_permuta_fora_local.Disabled = true;
				crmForm.all.new_valor_permuta_fisica_contabil.Disabled = true;
				crmForm.all.new_valor_permuta_fisica_mercado.Disabled = true;
			}
		}
		
		// Calculo do Valor Total do Contrato
		crmForm.CalcularValorTotalContrato = function()
		{
			//Executar somente se o formulário não estiver ready-only ou inativo
            if (crmForm.FormType == TypeRead || crmForm.FormType == TypeDisabled) 
			{
				return;
			}

			crmForm.all.new_valordacompra.DataValue = crmForm.all.new_valor_permuta_fisica_contabil.DataValue + crmForm.all.new_valor_permuta.DataValue + crmForm.all.new_valorapagar.DataValue;
			crmForm.all.new_valordacompra.ForceSubmit = true;
		}

		crmForm.HabilitaCamposPermuta();
		
		//Permuta financeira
		crmForm.EsconderCamposPermuta();
		crmForm.CamposObrigatoriosCasoPermutaFinanceira();
		//GUIA 01===============================================


		if (crmForm.all.new_valortotalpago.DataValue == null) {
			crmForm.all.new_valortotalpago.DataValue = 0;
		}
		
		//Calculo===============================================

		crmForm.CalcularValorTotalContrato();

		//GUIA 02===============================================

		//SESSAO 02 DOC_IMOVEL------------------------------------------
		if (crmForm.all.new_cpiadoregistromatricula.DataValue == 0) {
			crmForm.all.new_cpiadoregistromatricula.DataValue = null;
		}

		if (crmForm.all.new_cetidovintenriadoimovel.DataValue == 0) {
			crmForm.all.new_cetidovintenriadoimovel.DataValue = null;
		}

		if (crmForm.all.new_certidonegativadenusreaisdoimove.DataValue == 0) {
			crmForm.all.new_certidonegativadenusreaisdoimove.DataValue = null;
		}

		if (crmForm.all.new_diretrizesbsicasdoterreno.DataValue == 0) {
			crmForm.all.new_diretrizesbsicasdoterreno.DataValue = null;
		}

		if (crmForm.all.new_guiadeiptuatualizada.DataValue == 0) {
			crmForm.all.new_guiadeiptuatualizada.DataValue = null;
		}

		if (crmForm.all.new_levantamentotopogrficodoslotesad.DataValue == 0) {
			crmForm.all.new_levantamentotopogrficodoslotesad.DataValue = null;
		}

		if (crmForm.all.new_ccirnahiptesedeimovelrural.DataValue == 0) {
			crmForm.all.new_ccirnahiptesedeimovelrural.DataValue = null;
		}

		if (crmForm.all.new_certidonegativadedbitodeitrrural.DataValue == 0) {
			crmForm.all.new_certidonegativadedbitodeitrrural.DataValue = null;
		}

		//SESSAO 03 DOC_PJ---------------------------------

		if (crmForm.all.new_cnpjjuridica.DataValue == 0) {
			crmForm.all.new_cnpjjuridica.DataValue = null;
		}

		if (crmForm.all.new_contratosocialeltimaalteraocontr.DataValue == 0) {
			crmForm.all.new_contratosocialeltimaalteraocontr.DataValue = null;
		}

		if (crmForm.all.new_certidonegativadajustiafederal.DataValue == 0) {
			crmForm.all.new_certidonegativadajustiafederal.DataValue = null;
		}

		if (crmForm.all.new_certidonegativacvel.DataValue == 0) {
			crmForm.all.new_certidonegativacvel.DataValue = null;
		}

		if (crmForm.all.new_certidonegativafiscal.DataValue == 0) {
			crmForm.all.new_certidonegativafiscal.DataValue = null;
		}

		if (crmForm.all.new_certidonegativafalncia.DataValue == 0) {
			crmForm.all.new_certidonegativafalncia.DataValue = null;
		}

		if (crmForm.all.new_certidonegativadetributosfederai.DataValue == 0) {
			crmForm.all.new_certidonegativadetributosfederai.DataValue = null;
		}

		if (crmForm.all.new_certidonegativatrabalhista.DataValue == 0) {
			crmForm.all.new_certidonegativatrabalhista.DataValue = null;
		}

		if (crmForm.all.new_cndinss.DataValue == 0) {
			crmForm.all.new_cndinss.DataValue = null;
		}

		if (crmForm.all.new_crffgts.DataValue == 0) {
			crmForm.all.new_crffgts.DataValue = null;
		}

		if (crmForm.all.new_certidoatualizadadajuntacomercia.DataValue == 0) {
			crmForm.all.new_certidoatualizadadajuntacomercia.DataValue = null;
		}

		if (crmForm.all.new_certidonegativadadvidaativafeder.DataValue == 0) {
			crmForm.all.new_certidonegativadadvidaativafeder.DataValue = null;
		}

		if (crmForm.all.new_certidonegativadadvidaativaestad.DataValue == 0) {
			crmForm.all.new_certidonegativadadvidaativaestad.DataValue = null;
		}

		if (crmForm.all.new_certidonegativadadvidaativamunic.DataValue == 0) {
			crmForm.all.new_certidonegativadadvidaativamunic.DataValue = null;
		}

		if (crmForm.all.new_certidodeprotesto.DataValue == 0) {
			crmForm.all.new_certidodeprotesto.DataValue = null;
		}

		//SESSAO 04 DOC_SOCIOS-------------------

		if (crmForm.all.new_rgcpf.DataValue == 0) {
			crmForm.all.new_rgcpf.DataValue = null;
		}

		if (crmForm.all.new_certidodecasamento.DataValue == 0) {
			crmForm.all.new_certidodecasamento.DataValue = null;
		}

		if (crmForm.all.new_certidonegativadajustiafederalso.DataValue == 0) {
			crmForm.all.new_certidonegativadajustiafederalso.DataValue = null;
		}

		if (crmForm.all.new_certidonegativacivelsocio.DataValue == 0) {
			crmForm.all.new_certidonegativacivelsocio.DataValue = null;
		}

		if (crmForm.all.new_certidonegativafiscalsocio.DataValue == 0) {
			crmForm.all.new_certidonegativafiscalsocio.DataValue = null;
		}

		if (crmForm.all.new_certidonegativacriminalsocio.DataValue == 0) {
			crmForm.all.new_certidonegativacriminalsocio.DataValue = null;
		}

		if (crmForm.all.new_certidonegativadeprotestosocio.DataValue == 0) {
			crmForm.all.new_certidonegativadeprotestosocio.DataValue = null;
		}

		if (crmForm.all.new_certidonegativatrabalhistasocio.DataValue == 0) {
			crmForm.all.new_certidonegativatrabalhistasocio.DataValue = null;
		}

		if (crmForm.all.new_certidodareceitafederalsocio.DataValue == 0) {
			crmForm.all.new_certidodareceitafederalsocio.DataValue = null;
		}

		if (crmForm.all.new_certidodadvidaativasocio.DataValue == 0) {
			crmForm.all.new_certidodadvidaativasocio.DataValue = null;
		}

		//SESSAO 05 - DOC_PF--------------------------------

		if (crmForm.all.new_rgcpffisica.DataValue == 0) {
			crmForm.all.new_rgcpffisica.DataValue = null;
		}

		if (crmForm.all.new_certidodecasamentofisica.DataValue == 0) {
			crmForm.all.new_certidodecasamentofisica.DataValue = null;
		}

		if (crmForm.all.new_certidonegativadetributosfedfisc.DataValue == 0) {
			crmForm.all.new_certidonegativadetributosfedfisc.DataValue = null;
		}

		if (crmForm.all.new_cndinssfisica.DataValue == 0) {
			crmForm.all.new_cndinssfisica.DataValue = null;
		}

		if (crmForm.all.new_certidonegativadajustiafederalfi.DataValue == 0) {
			crmForm.all.new_certidonegativadajustiafederalfi.DataValue = null;
		}

		if (crmForm.all.new_certidonegativadeprocessoscivelf.DataValue == 0) {
			crmForm.all.new_certidonegativadeprocessoscivelf.DataValue = null;
		}

		if (crmForm.all.new_certidonegativadeprocessosfisfis.DataValue == 0) {
			crmForm.all.new_certidonegativadeprocessosfisfis.DataValue = null;
		}

		if (crmForm.all.new_certidonegativadeprocessoscrifis.DataValue == 0) {
			crmForm.all.new_certidonegativadeprocessoscrifis.DataValue = null;
		}

		if (crmForm.all.new_certidonegativatrabalhistafisica.DataValue == 0) {
			crmForm.all.new_certidonegativatrabalhistafisica.DataValue = null;
		}

		if (crmForm.all.new_certidonegativadocartoriodisdepr.DataValue == 0) {
			crmForm.all.new_certidonegativadocartoriodisdepr.DataValue = null;
		}

		if (crmForm.all.new_certidodareceitamunidividaafisic.DataValue == 0) {
			crmForm.all.new_certidodareceitamunidividaafisic.DataValue = null;
		}

		if (crmForm.all.new_certidodareceitaestdividaatfisic.DataValue == 0) {
			crmForm.all.new_certidodareceitaestdividaatfisic.DataValue = null;
		}

		if (crmForm.all.new_certidodareceitafedldividaatifis.DataValue == 0) {
			crmForm.all.new_certidodareceitafedldividaatifis.DataValue = null;
		}

		//SESSÃO 04 E 05 -certidao de casamento quando o estado civil for casamento---
		if (crmForm.all.new_estadocivldoc.DataValue == 1) {
			crmForm.all.new_certidodecasamento_c.style.visibility = 'visible';
			crmForm.all.new_certidodecasamento_d.style.visibility = 'visible';
		} else {
			crmForm.all.new_certidodecasamento_c.style.visibility = 'hidden';
			crmForm.all.new_certidodecasamento_d.style.visibility = 'hidden';
		}

		if (crmForm.all.new_estadocivldocfisica.DataValue == 1) {
			crmForm.all.new_certidodecasamentofisica_c.style.visibility = 'visible';
			crmForm.all.new_certidodecasamentofisica_d.style.visibility = 'visible';
		} else {
			crmForm.all.new_certidodecasamentofisica_c.style.visibility = 'hidden';
			crmForm.all.new_certidodecasamentofisica_d.style.visibility = 'hidden';
		}

		//SESSÃO 01 - 02 - 03 - 04 - 05 - Script geral Status---------------------
		if ((crmForm.all.new_cpiadoregistromatricula.DataValue == false) ||
			(crmForm.all.new_cetidovintenriadoimovel.DataValue == false) ||
			(crmForm.all.new_certidonegativadenusreaisdoimove.DataValue == false) ||
			(crmForm.all.new_diretrizesbsicasdoterreno.DataValue == false) ||
			(crmForm.all.new_certidonegativadedbitodeiptu.DataValue == false) ||
			(crmForm.all.new_guiadeiptuatualizada.DataValue == false) ||
			(crmForm.all.new_levantamentotopogrficodoslotesad.DataValue == false) ||
			(crmForm.all.new_ccirnahiptesedeimovelrural.DataValue == false) ||
			(crmForm.all.new_certidonegativadedbitodeitrrural.DataValue == false)) {
			crmForm.all.new_statusdocimovel.DataValue = 2;
		} else {
			crmForm.all.new_statusdocimovel.DataValue = 1;
		}

		//script do status da documentação de pessoa juridica-----------------------
		if ((crmForm.all.new_cnpjjuridica.DataValue == false) ||
			(crmForm.all.new_contratosocialeltimaalteraocontr.DataValue == false) ||
			(crmForm.all.new_certidonegativadajustiafederal.DataValue == false) ||
			(crmForm.all.new_certidonegativacvel.DataValue == false) ||
			(crmForm.all.new_certidonegativafiscal.DataValue == false) ||
			(crmForm.all.new_certidonegativafalncia.DataValue == false) ||
			(crmForm.all.new_certidonegativadetributosfederai.DataValue == false) ||
			(crmForm.all.new_certidonegativatrabalhista.DataValue == false) ||
			(crmForm.all.new_cndinss.DataValue == false) ||
			(crmForm.all.new_crffgts.DataValue == false) ||
			(crmForm.all.new_certidoatualizadadajuntacomercia.DataValue == false) ||
			(crmForm.all.new_certidonegativadadvidaativafeder.DataValue == false) ||
			(crmForm.all.new_certidonegativadadvidaativaestad.DataValue == false) ||
			(crmForm.all.new_certidonegativadadvidaativamunic.DataValue == false) ||
			(crmForm.all.new_certidodeprotesto.DataValue == false)) {
			crmForm.all.new_statusdocpessoajuridica.DataValue = 2;
		} else {
			crmForm.all.new_statusdocpessoajuridica.DataValue = 1;
		}

		//script do status da documentação dos sócios-------------------------------
		if ((crmForm.all.new_rgcpf.DataValue == false) ||
			(crmForm.all.new_certidonegativadajustiafederalso.DataValue == false) ||
			(crmForm.all.new_certidonegativacivelsocio.DataValue == false) ||
			(crmForm.all.new_certidonegativafiscalsocio.DataValue == false) ||
			(crmForm.all.new_certidonegativacriminalsocio.DataValue == false) ||
			(crmForm.all.new_certidonegativadeprotestosocio.DataValue == false) ||
			(crmForm.all.new_certidodareceitafederalsocio.DataValue == false) ||
			(crmForm.all.new_certidodadvidaativasocio.DataValue == false) ||
			(crmForm.all.new_certidonegativatrabalhistasocio.DataValue == false) ||
			(crmForm.all.new_estadocivldoc.DataValue == true && crmForm.all.new_certidodecasamento.DataValue == false)) {
			crmForm.all.new_statusdocsocios.DataValue = 2;
		} else {
			crmForm.all.new_statusdocsocios.DataValue = 1;
		}

		//documentacao pess.fisica-------------------------------------------------------------
		if ((crmForm.all.new_rgcpffisica.DataValue == false) ||
			(crmForm.all.new_certidonegativadetributosfedfisc.DataValue == false) ||
			(crmForm.all.new_cndinssfisica.DataValue == false) ||
			(crmForm.all.new_certidonegativadajustiafederalfi.DataValue == false) ||
			(crmForm.all.new_certidonegativadeprocessoscivelf.DataValue == false) ||
			(crmForm.all.new_certidonegativadeprocessosfisfis.DataValue == false) ||
			(crmForm.all.new_certidonegativadeprocessoscrifis.DataValue == false) ||
			(crmForm.all.new_certidonegativatrabalhistafisica.DataValue == false) ||
			(crmForm.all.new_certidonegativadocartoriodisdepr.DataValue == false) ||
			(crmForm.all.new_certidodareceitamunidividaafisic.DataValue == false) ||
			(crmForm.all.new_certidodareceitaestdividaatfisic.DataValue == false) ||
			(crmForm.all.new_certidodareceitafedldividaatifis.DataValue == false) ||
			(crmForm.all.new_estadocivldocfisica.DataValue == true && crmForm.all.new_certidodecasamentofisica.DataValue == false)) {
			crmForm.all.new_statusdocpessoafisica.DataValue = 2;
		} else {
			crmForm.all.new_statusdocpessoafisica.DataValue = 1;
		}

		//script para avaliar o status quando precisar de documentação física ------------------
		if ((crmForm.all.new_docpessoafisica.DataValue == true) && (crmForm.all.new_docpessoajuridica.DataValue == false)) {
			if ((crmForm.all.new_statusdocimovel.DataValue == 1) && (crmForm.all.new_statusdocpessoafisica.DataValue == 1)) {
				crmForm.all.new_fisica.DataValue = 1;
			} else {
				crmForm.all.new_fisica.DataValue = 2;
			}
		}

		//script para avaliar o status quando precisar de documentação jurídica ------------------
		if ((crmForm.all.new_docpessoafisica.DataValue == false) && (crmForm.all.new_docpessoajuridica.DataValue == true)) {
			if ((crmForm.all.new_statusdocimovel.DataValue == 1) && (crmForm.all.new_statusdocpessoajuridica.DataValue == 1) && (crmForm.all.new_statusdocsocios.DataValue == 1)) {
				crmForm.all.new_fisica.DataValue = 1;
			} else {
				crmForm.all.new_fisica.DataValue = 2;
			}
		}

		//GUIA 03===============================================

		//    //SESSÃO 01 - PERMUTA ocultar datas-------------------------
		//    if (crmForm.all.new_dataprevistadeincio5.DataValue == null) {
		//        crmForm.all.new_existepermuta.DataValue = false;
		//    }


		//GUIA 04===============================================

		//SESSÃO 01 - Habilitar datas
		if (crmForm.all.new_cartagarantia.DataValue == false) {
			crmForm.all.new_dataprevistadasolicitao1_c.style.visibility = 'hidden';
			crmForm.all.new_dataprevistadasolicitao1_d.style.visibility = 'hidden';
			crmForm.all.new_datasolicitaocarta_c.style.visibility = 'hidden';
			crmForm.all.new_datasolicitaocarta_d.style.visibility = 'hidden';
		} else {
			crmForm.all.new_dataprevistadasolicitao1_c.style.visibility = 'visible';
			crmForm.all.new_dataprevistadasolicitao1_d.style.visibility = 'visible';
			crmForm.all.new_datasolicitaocarta_c.style.visibility = 'visible';
			crmForm.all.new_datasolicitaocarta_d.style.visibility = 'visible';
		}

		if (crmForm.all.new_segurogarantia.DataValue == false) {
			crmForm.all.new_dataprevistadasolicitao2_c.style.visibility = 'hidden';
			crmForm.all.new_dataprevistadasolicitao2_d.style.visibility = 'hidden';
			crmForm.all.new_datasolicitaoseguro_c.style.visibility = 'hidden';
			crmForm.all.new_datasolicitaoseguro_d.style.visibility = 'hidden';
		} else {
			crmForm.all.new_dataprevistadasolicitao2_c.style.visibility = 'visible';
			crmForm.all.new_dataprevistadasolicitao2_d.style.visibility = 'visible';
			crmForm.all.new_datasolicitaoseguro_c.style.visibility = 'visible';
			crmForm.all.new_datasolicitaoseguro_d.style.visibility = 'visible';
		}

		//GUIA 05===============================================

		//SESSÃO 01 - Script para ocultar os dados do distrato do terreno-----
		if (crmForm.all.new_possuidistrato.DataValue == false) {
			crmForm.all.new_dataprevistadistrato_c.style.visibility = 'hidden';
			crmForm.all.new_dataprevistadistrato_d.style.visibility = 'hidden';
			crmForm.all.new_datarealdistrato_c.style.visibility = 'hidden';
			crmForm.all.new_datarealdistrato_d.style.visibility = 'hidden';
			crmForm.all.new_multadistrato_c.style.visibility = 'hidden';
			crmForm.all.new_multadistrato_d.style.visibility = 'hidden';
			crmForm.all.new_valormultadistrato_c.style.visibility = 'hidden';
			crmForm.all.new_valormultadistrato_d.style.visibility = 'hidden';
			crmForm.all.new_motivodistrato_c.style.visibility = 'hidden';
			crmForm.all.new_motivodistrato_d.style.visibility = 'hidden';
		} else {
			crmForm.all.new_dataprevistadistrato_c.style.visibility = 'visible';
			crmForm.all.new_dataprevistadistrato_d.style.visibility = 'visible';
			crmForm.all.new_datarealdistrato_c.style.visibility = 'visible';
			crmForm.all.new_datarealdistrato_d.style.visibility = 'visible';
			crmForm.all.new_multadistrato_c.style.visibility = 'visible';
			crmForm.all.new_multadistrato_d.style.visibility = 'visible';

			if (crmForm.all.new_multadistrato.DataValue == false) {
				crmForm.all.new_valormultadistrato_c.style.visibility = 'hidden';
				crmForm.all.new_valormultadistrato_d.style.visibility = 'hidden';
				crmForm.all.new_valormultadistrato.setAttribute("n", 0);
				crmForm.all.new_valormultadistrato_c.className = "n";
			} else {
				crmForm.all.new_valormultadistrato_c.style.visibility = 'visible';
				crmForm.all.new_valormultadistrato_d.style.visibility = 'visible';
				crmForm.all.new_valormultadistrato.setAttribute("req", 1);
				crmForm.all.new_valormultadistrato_c.className = "req";
			}
			crmForm.all.new_motivodistrato_c.style.visibility = 'visible';
			crmForm.all.new_motivodistrato_d.style.visibility = 'visible';
		}

		//GUIA 07===============================================

		//Ocultar cammpos da administração------------------------------------------------------//
		crmForm.all.new_idusuario_c.style.visibility = 'hidden';
		crmForm.all.new_idusuario_d.style.visibility = 'hidden';
		crmForm.all.new_idteam_c.style.visibility = 'hidden';
		crmForm.all.new_idteam_d.style.visibility = 'hidden';
		var teste = "";

		//------------------Criação do filtro para busca de Coordenadores de DI -----------------------//
		function FilterLookup(attribute, url, param) {
			if (param != null)
				url += '?' + param;

			oImg = eval('attribute' + '.parentNode.childNodes[0]');
			oImg.onclick = function() {
				retorno = openStdDlg(url, null, 600, 450);
				if (typeof (retorno) != "undefined") {
					strValues = retorno.split('*|*');
					var lookupData = new Array();
					lookupItem = new Object();
					lookupItem.id = "{" + strValues[1] + "}";
					// ObjectTypeCode Contact, é padrão
					lookupItem.type = parseInt(strValues[2]);
					lookupItem.name = strValues[0];
					lookupData[0] = lookupItem;
					attribute.DataValue = lookupData;
					attribute.FireOnChange();
				}
			};
		}

		var filterDefault = "false";
		var oParam = "objectTypeCode=2&filterDefault=" + filterDefault + "&attributesearch=fullname";

		FilterLookup(crmForm.all.new_contato_terrenoid, "/MRVWeb/FilterLookup/FilterLookup.aspx", oParam);

    }
	
	function loadScript() {
        var oHead = document.getElementsByTagName('head')[0];
        var oScript = document.createElement('script');
        oScript.type = "text/javascript";
        oScript.src = "/_static/Base.js";

        oScript.onreadystatechange = function() {
            if (this.readyState == 'complete' || this.readyState == 'loaded') {
                Load();
            }
        }
        oHead.appendChild(oScript);
    }

    loadScript();

} catch (error) {
    formularioValido = false;
    alert("Ocorreu um erro no formulário.\n" + error.description);
}