﻿try {
    if (!formularioValido) {
        alert("Ocorreu um erro no formulário. Impossível salvar.");
        event.returnValue = false;
        return false;
    }

    //habilitar o campo do valor de compra do terreno
    crmForm.all.new_valordacompra.Disabled = true;

    //PASSAR PARA MAIUSCULO TODOS OS CARACTERES-----------------------
    var elm = document.getElementsByTagName("input");
    for (i = 0; i < elm.length; i++) {
        if (elm[i].type == "text")
            if (elm[i].DataValue != null) {
            try {
                elm[i].DataValue = elm[i].DataValue.toUpperCase();
            }
            catch (e) { }
        }
    }

} catch (error) {
    alert("Ocorreu um erro no formulário.\n" + error.description);
    event.returnValue = false;
    return false;
}