﻿formularioValido = true;
try {
    crmForm.DefinirNomeRegistro = function() {
        crmForm.all.new_name.DataValue = (crmForm.all.new_data_prevista.DataValue != null) ? "Data Prevista: " + crmForm.all.new_data_prevista.DataValue : "Valor Indefinido.";
    }
    crmForm.DefinirNomeRegistro();

    crmForm.VerificarFuncao = function(nomeFuncao) {
        var cmd = new RemoteCommand("MrvService", "UserContainsTeams", "/MrvCustomizations/");
        cmd.SetParameter("teams", nomeFuncao);
        var resultado = cmd.Execute();
        if (resultado.Success) {
            return resultado.ReturnValue.Mrv.Found;
        } else {
            alert("Erro na consulta do método UserContainsTeams");
            formularioValido = false;
            return false;
        }
    }
    crmForm.ValidarAtualizacao = function() {
        var cmd = new RemoteCommand("MrvService", "ValidarAtualizacaoRegistroPJ", "/MrvCustomizations/");
        cmd.SetParameter("idRegistroPJ", crmForm.ObjectId);

        var resultado = cmd.Execute();
        if (resultado.ReturnValue.Mrv.Valid) {
            return true;
        } else {            
            if (resultado.ReturnValue.Mrv.Mensagem != undefined) {
                alert(resultado.ReturnValue.Mrv.Mensagem);
            }
            return false;
        }        
    }

    if (crmForm.all.new_moduloid.DataValue == null) {
        crmForm.all.new_moduloid_c.style.display = "none";
        crmForm.all.new_moduloid_d.style.display = "none";
    }
    if (crmForm.all.new_modulovillage_fbancarioid.DataValue == null) {
        crmForm.all.new_modulovillage_fbancarioid_c.style.display = "none";
        crmForm.all.new_modulovillage_fbancarioid_d.style.display = "none";
    }

    if (!crmForm.VerificarFuncao("Crédito Imobiliário")) {
        crmForm.all.new_data_real.Disabled = true;
        crmForm.all.new_data_informada.Disabled = true;
        crmForm.all.new_status.Disabled = true;
    }
    else {
        var permiteAtualizacao = crmForm.ValidarAtualizacao();
        crmForm.all.new_data_real.Disabled = !permiteAtualizacao;
    }
    
    crmForm.all.new_name_c.style.display = "none";
    crmForm.all.new_name_d.style.display = "none";

} catch (error) {
    formularioValido = false;
    alert("Ocorreu um erro no formulário.\n" + error.description);
}