﻿formularioValido = true;

try 
{
    crmForm.GetElementsByClassName = function(oElm, strTagName, strClassName) 
    {
        var arrElements = (strTagName == "*" && oElm.all) ? oElm.all : oElm.getElementsByTagName(strTagName);
        var arrReturnElements = new Array();
        strClassName = strClassName.replace(/\-/g, "\\-");
        var oRegExp = new RegExp("(^|\\s)" + strClassName + "(\\s|$)");
        var oElement;
        for (var i = 0; i < arrElements.length; i++) 
        {
            oElement = arrElements[i];
            
            if (oRegExp.test(oElement.className))
                arrReturnElements.push(oElement);
        }
        return (arrReturnElements)
    }

    /* 
        Taxa Administração não pode ser distratada
            - Esconde o botão "Distrato" do menu esquerdo.
    */
    if (crmForm.all.new_name != null && crmForm.all.new_name.DataValue.indexOf("Taxa de Admin") >= 0) 
    {
        var lista = crmForm.GetElementsByClassName(document, "li", "ms-crm-Nav-Subarea");

        if (lista != null) 
        {
            for (i = 0; i < lista.length; i++) 
            {
                if (lista[i].firstChild.title.indexOf("Exibir Distrato") >= 0) 
                {
                    var o = lista[i].parentElement;
                    o.removeChild(lista[i]);
                    break;
                }
            }
        }
    }
}
catch (error) 
{
    formularioValido = false;
    alert("Ocorreu um erro no formulário.\n" + error.description);
}