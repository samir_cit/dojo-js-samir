﻿if (crmForm.all.new_atua_dt_prot_atualizada_cnd.DataValue) 
{
    // Atualização Automática
    crmForm.all.new_data_prev_protocolo_atu_cnd.Disabled = true;
    if (AtualizaDatas) 
        crmForm.CalculaDatasEngenharia();
}
else 
{
    // Atualização Manual
    crmForm.all.new_data_prev_protocolo_atu_cnd.Disabled = false
}