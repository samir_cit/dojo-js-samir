﻿if(crmForm.all.new_data_real_emissao_hab.DataValue != null) {
     var data1 = crmForm.all.new_data_real_emissao_hab.DataValue;
     crmForm.all.new_name.DataValue =  "Data real de habite-se para " + data1.getDate()+"/"+ (data1.getMonth()+1) +"/"+data1.getYear();
}
else {
    if(crmForm.all.new_data_prev_emissao_atu_hab.DataValue != null) {   
        var data2 = crmForm.all.new_data_prev_emissao_atu_hab.DataValue;
        crmForm.all.new_name.DataValue =  "Data previsão de habite-se para " + data2.getDate()+"/"+ (data2.getMonth()+1) +"/"+data2.getYear();
    }
    else {
        if(crmForm.all.new_data_prevista_emissao_hab.DataValue != null) {
            var data3 = crmForm.all.new_data_prevista_emissao_hab.DataValue;
            crmForm.all.new_name.DataValue =  "Data previsão de habite-se para " + data3.getDate()+"/"+ (data3.getMonth()+1) +"/"+data3.getYear();
        }
        else { 
            crmForm.all.new_name.DataValue = "Sem previsão de habite-se!!";
        }
    }
}