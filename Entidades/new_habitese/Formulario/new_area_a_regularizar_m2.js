﻿crmForm.all.new_total_recolher.DataValue = crmForm.all.new_area_a_regularizar_m2.DataValue * crmForm.all.new_valor_m2.DataValue;

if (crmForm.all.new_quitacao_iss_municipio.DataValue != null) {
    crmForm.all.new_imposto_retido.DataValue = (crmForm.all.new_quitacao_iss_municipio.DataValue/100) * crmForm.all.new_total_nf_servicos.DataValue; 
    
    if ((crmForm.all.new_total_recolher.DataValue != null) && (crmForm.all.new_total_recolher.DataValue != 0)) {
        crmForm.all.new_iss_municipio_recolher.DataValue = (crmForm.all.new_total_recolher.DataValue*crmForm.all.new_quitacao_iss_municipio.DataValue)/100;
        crmForm.all.new_total_percentual.DataValue = crmForm.all.new_iss_municipio_recolher.DataValue - crmForm.all.new_imposto_retido.DataValue;
        crmForm.all.new_total_a_pagar.DataValue = crmForm.all.new_total_percentual.DataValue - (crmForm.all.new_mao_de_obra_propria_folha.DataValue + crmForm.all.new_estagiarios_folha.DataValue);
    }
    else {
        crmForm.all.new_iss_municipio_recolher.DataValue = null;
        crmForm.all.new_total_percentual.DataValue = null;
        crmForm.all.new_total_a_pagar.DataValue = null;
    }  
}
else {
    crmForm.all.new_imposto_retido.DataValue = null;
    crmForm.all.new_iss_municipio_recolher.DataValue = null;
    crmForm.all.new_total_percentual.DataValue = null;
    crmForm.all.new_total_a_pagar.DataValue = null;
}
