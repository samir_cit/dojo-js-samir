﻿formularioValido = true;

try {

 function loadScript() {
        var oScript = document.createElement('script');
        oScript.type = "text/javascript";
        oScript.src = "/_static/Base.js";
        var oHead = document.getElementsByTagName('head')[0];
        oHead.appendChild(oScript);

        oScript.onreadystatechange = function() {
            if (this.readyState == 'complete' || this.readyState == 'loaded')
                Load();
        }
    }

    loadScript();

    function Load() {

        crmForm.SetDateField = function(field, value) {
            if (value == null)
                eval(field).DataValue = null;
            else {
                value = value.split('T')[0];
                var newValue = value.split('-');
                var newDate = new Date(newValue[0], newValue[1] - 1, newValue[2], 3, 0, 0);
                eval(field).DataValue = newDate;
            }
        }

        crmForm.UsuarioPertenceEquipe = function(nomeEquipe) {
            var cmd = new RemoteCommand("MrvService", "UserContainsTeams", "/MRVCustomizations/");

            cmd.SetParameter("teams", nomeEquipe);

            var resultado = cmd.Execute();

            return resultado.Success ? resultado.ReturnValue.Mrv.Found : false;
        }

        crmForm.AtualizaDataTerminoObra = function() {
            var attributes = "new_10alterao;new_9alterao;new_8alterao;new_7alterao;new_6alterao;new_5alterao;new_4alterao;new_3alterao;new_2alterao;new_1alterao;new_dataprimeirocontratocliente";
            var cmd = new RemoteCommand("MrvService", "GetEntity", "/MRVCustomizations/");

            cmd.SetParameter("entity", "new_modulovillageengenharia");
            cmd.SetParameter("primaryField", "new_modulovillageengenhariaid");
            cmd.SetParameter("entityId", crmForm.all.new_datas_habiteseid.DataValue[0].id);
            cmd.SetParameter("fieldList", attributes);

            var oResult = cmd.Execute();

            if (oResult.Success) {
                if (oResult.ReturnValue.Mrv.Found) {
                    var Data = oResult.ReturnValue.Mrv.new_10alterao != null
                        ? oResult.ReturnValue.Mrv.new_10alterao.toString()
                        : oResult.ReturnValue.Mrv.new_9alterao != null
                        ? oResult.ReturnValue.Mrv.new_9alterao.toString()
                        : oResult.ReturnValue.Mrv.new_8alterao != null
                        ? oResult.ReturnValue.Mrv.new_8alterao.toString()
                        : oResult.ReturnValue.Mrv.new_7alterao != null
                        ? oResult.ReturnValue.Mrv.new_7alterao.toString()
                        : oResult.ReturnValue.Mrv.new_6alterao != null
                        ? oResult.ReturnValue.Mrv.new_6alterao.toString()
                        : oResult.ReturnValue.Mrv.new_5alterao != null
                        ? oResult.ReturnValue.Mrv.new_5alterao.toString()
                        : oResult.ReturnValue.Mrv.new_4alterao != null
                        ? oResult.ReturnValue.Mrv.new_4alterao.toString()
                        : oResult.ReturnValue.Mrv.new_3alterao != null
                        ? oResult.ReturnValue.Mrv.new_3alterao.toString()
                        : oResult.ReturnValue.Mrv.new_2alterao != null
                        ? oResult.ReturnValue.Mrv.new_2alterao.toString()
                        : oResult.ReturnValue.Mrv.new_1alterao != null
                        ? oResult.ReturnValue.Mrv.new_1alterao.toString()
                        : oResult.ReturnValue.Mrv.new_dataprimeirocontratocliente != null
                        ? oResult.ReturnValue.Mrv.new_dataprimeirocontratocliente.toString()
                        : null;
                    if (Data == null)
                        alert('É necessário cadastrar a Data Primeiro Contrato Cliente no Módulo Village na aba Tabela de Prazos.');
                    else {
                        crmForm.SetDateField("crmForm.all.new_data_termino_obra", Data);
                        DataTerminoObra = crmForm.all.new_data_termino_obra.DataValue;
                    }
                }
            } else
                alert("Erro na consulta ao WebService.\nContate o administrador do sistema.");
        }

        new_name_onchange0();

        //Adiciona Script para ser usado no click do botão
        var elm = document.createElement("script");
        elm.src = "/_static/_grid/cmds/util.js";
        document.appendChild(elm);

        //Guia Doc. Encerramento de Obra

        //Desabiltar seção - ISS
        crmForm.all.new_possui_iss.FireOnChange();

        //Desabiltar seção - ELEVADOR
        if (crmForm.all.new_datas_habiteseid.DataValue != null) {
            var attributes = "new_elevador";
            var cmd = new RemoteCommand("MrvService", "GetEntity", "/MRVCustomizations/");

            cmd.SetParameter("entity", "new_modulovillageengenharia");
            cmd.SetParameter("primaryField", "new_modulovillageengenhariaid");
            cmd.SetParameter("entityId", crmForm.all.new_datas_habiteseid.DataValue[0].id);
            cmd.SetParameter("fieldList", attributes);

            var oResult = cmd.Execute();
            if (oResult.Success) {

                if (oResult.ReturnValue.Mrv.Found) {
                    if ((oResult.ReturnValue.Mrv.new_elevador == null) || (oResult.ReturnValue.Mrv.new_elevador == false)) {
                        document.getElementById("new_laudo_tecnico_instalacao_eletrica_d").parentNode.parentNode.style.display = "none";
                        document.getElementById("new_obs_elevador_d").parentNode.parentNode.style.display = "none";
                    } else {
                        document.getElementById("new_laudo_tecnico_instalacao_eletrica_d").parentNode.parentNode.style.display = "inline";
                        document.getElementById("new_obs_elevador_d").parentNode.parentNode.style.display = "inline";
                    }
                }
            } else
                alert("Erro na consulta ao WebService.\nContate o administrador do sistema.");
        }

        //Desabiltar seção - STATUS DO HABITE-SE
        crmForm.all.new_status_habitese.FireOnChange();

        // Atualização automática das datas da engenharia ---------------------------------

        // Atualiza o campo 'Atualização Automática'.
        if (crmForm.all.new_atua_dt_termino_obra.DataValue == null)
            crmForm.all.new_atua_dt_termino_obra.DataValue = true;

        if (crmForm.all.new_atua_dt_prot_atualizada_hab.DataValue == null)
            crmForm.all.new_atua_dt_prot_atualizada_hab.DataValue = true;

        if (crmForm.all.new_atua_dt_emis_atualizada_hab.DataValue == null)
            crmForm.all.new_atua_dt_emis_atualizada_hab.DataValue = true;

        if (crmForm.all.new_atua_dt_prot_atualizada_averb.DataValue == null)
            crmForm.all.new_atua_dt_prot_atualizada_averb.DataValue = true;

        if (crmForm.all.new_atua_dt_averbacao_atualizada.DataValue == null)
            crmForm.all.new_atua_dt_averbacao_atualizada.DataValue = true;

        if (crmForm.all.new_atua_dt_prot_atualizada_cnd.DataValue == null)
            crmForm.all.new_atua_dt_prot_atualizada_cnd.DataValue = true;

        if (crmForm.all.new_atua_dt_emis_atualizada_cnd.DataValue == null)
            crmForm.all.new_atua_dt_emis_atualizada_cnd.DataValue = true;

        AtualizaDatas = false;
        crmForm.all.new_data_real_emissao_hab.FireOnChange();
        crmForm.all.new_data_real_protocolo_hab.FireOnChange();
        crmForm.all.new_atua_dt_termino_obra.FireOnChange();
        crmForm.all.new_atua_dt_prot_atualizada_hab.FireOnChange();
        crmForm.all.new_atua_dt_emis_atualizada_hab.FireOnChange();
        crmForm.all.new_atua_dt_prot_atualizada_averb.FireOnChange();
        crmForm.all.new_atua_dt_averbacao_atualizada.FireOnChange();
        crmForm.all.new_atua_dt_prot_atualizada_cnd.FireOnChange();
        crmForm.all.new_atua_dt_emis_atualizada_cnd.FireOnChange();
        AtualizaDatas = true;

        // Cálculo da area media das unidades
        crmForm.all.new_area_a_regularizar.FireOnChange();

        //script para aparecer a data de entrega do material, caso a empresa esteja contratada
        crmForm.all.new_empresa_contratada.FireOnChange();

        //script para mostrar status do Doc Elevador
        crmForm.all.new_laudo_tecnico_instalacao_eletrica.FireOnChange();
        crmForm.all.new_requerimento_padrao.FireOnChange();
        crmForm.all.new_contrato_manutencao.FireOnChange();
        crmForm.all.new_relatorio_inspessao_anual.FireOnChange();

        //script para mostrar status do doc INSS
        crmForm.all.new_copia_autenticada_habitese.FireOnChange();
        crmForm.all.new_guia_autenticada_gps.FireOnChange();
        crmForm.all.new_copia_cei.FireOnChange();
        crmForm.all.new_pesquisa_restricoes.FireOnChange();
        crmForm.all.new_status_pesquisa.FireOnChange();

        // Desabilitar campos

        // ISS
        crmForm.all.new_area_media_unidades_m2.readOnly = true;
        crmForm.all.new_area_a_regularizar_m2.readOnly = true;
        crmForm.all.new_total_recolher.readOnly = true;
        crmForm.all.new_iss_municipio_recolher.readOnly = true;
        crmForm.all.new_imposto_retido.readOnly = true;
        crmForm.all.new_total_percentual.readOnly = true;
        crmForm.all.new_total_a_pagar.readOnly = true;

        // BOMBEIRO
        crmForm.all.new_area_modulo_village.readOnly = true;

        // STATUS
        crmForm.all.new_status_doc.Disabled = true;
        crmForm.all.new_status_art.Disabled = true;
        crmForm.all.new_status_doc_inss.Disabled = true;

        // Datas engenharia
        crmForm.all.new_data_prevista_protocolo_iss.Disabled = true;
        crmForm.all.new_data_prevista_emissao_iss.Disabled = true;

        crmForm.all.new_data_prevista_protocolo_cb.Disabled = true;
        crmForm.all.new_data_prevista_emissao_cb.Disabled = true;

        crmForm.all.new_data_prevista_protocolo_hab.Disabled = true;
        crmForm.all.new_data_prevista_emissao_hab.Disabled = true;

        crmForm.all.new_data_prevista_protocolo_cnd.Disabled = true;
        crmForm.all.new_data_prevista_emissao_cnd.Disabled = true;

        crmForm.all.new_data_prev_entrega_material.Disabled = true;

        crmForm.all.new_data_prevista_protocolo_averb.Disabled = true;
        crmForm.all.new_data_prevista_averbacao.Disabled = true;

        crmForm.all.new_data_prevista_inicio.Disabled = true;
        crmForm.all.new_data_prevista_conclusao.Disabled = true;
        crmForm.all.new_data_prev_inicio_proc_atu.Disabled = true;
        crmForm.all.new_data_prev_conclusao_proc_atu.Disabled = true;

        // Salva a data de termino da obra, a data prevista protocolo atualizada e a data prevista emissão atualizada
        DataPrevEmissaoAtuHab = crmForm.all.new_data_prev_emissao_atu_hab.DataValue == null ? null : crmForm.all.new_data_prev_emissao_atu_hab.DataValue;
        DataPrevProtocoloAtuHab = crmForm.all.new_data_prev_protocolo_atu_hab.DataValue == null ? null : crmForm.all.new_data_prev_protocolo_atu_hab.DataValue;
        DataTerminoObra = crmForm.all.new_data_termino_obra.DataValue == null ? null : crmForm.all.new_data_termino_obra.DataValue;

        // Verifica se a diferença entre a data prevista protocolo atualizada e a data prevista emissão atualizada
        // é maior que 30 dias
        crmForm.DifDtProtocoloDtEmissao = function() {
            if (crmForm.all.new_data_prev_emissao_atu_hab.DataValue != null && crmForm.all.new_data_prev_protocolo_atu_hab.DataValue != null) {
                var Data1 = crmForm.all.new_data_prev_emissao_atu_hab.DataValue;
                var Data2 = crmForm.all.new_data_prev_protocolo_atu_hab.DataValue;
                var Dif = Date.UTC(Data1.getYear(), Data1.getMonth(), Data1.getDate(), 0, 0, 0) -
                    Date.UTC(Data2.getYear(), Data2.getMonth(), Data2.getDate(), 0, 0, 0);
                Dif = (Dif / 1000 / 60 / 60 / 24);

                if (Dif < 30)
                    alert("A diferença entre a Data Prevista de Protocolo Atualizada e a Data Prevista de Emissão Atualizada \ndeve ser maior ou igual a 30 dias!");

                return (Dif >= 30);
            }

            return true;
        }

        // Soma dias a uma data
        crmForm.AddDays = function(Data, NumDias) {
            if (Data == null)
                return null;

            var Dt = new Date(Data.toString());
            return Dt.setDate(Dt.getDate() + NumDias)
        }

        /// Aba:    Doc. Encerramento de Obra
        /// Sessão: Pedido de Baixa de Obra - Habite-se
        crmForm.CalculaBaixaDeObraHabitese = function(dataContratoCliente) {
            if (crmForm.all.new_data_prevista_protocolo_hab.DataValue == null) {
                crmForm.all.new_data_prevista_protocolo_hab.DataValue = crmForm.AddDays(DataTerminoObra, 30);
                crmForm.all.new_data_prevista_protocolo_hab.ForceSubmit = true;
            }

            if (crmForm.all.new_atua_dt_prot_atualizada_hab.DataValue) {
                crmForm.all.new_data_prev_protocolo_atu_hab.DataValue = crmForm.AddDays(DataTerminoObra, 30);
                crmForm.all.new_data_prev_protocolo_atu_hab.ForceSubmit = true;
            }

            if (crmForm.all.new_data_prevista_emissao_hab.DataValue == null) {
                crmForm.all.new_data_prevista_emissao_hab.DataValue = crmForm.AddDays(DataTerminoObra, 90);
                crmForm.all.new_data_prevista_emissao_hab.ForceSubmit = true;
            }

            if (crmForm.all.new_atua_dt_emis_atualizada_hab.DataValue) {
                crmForm.all.new_data_prev_emissao_atu_hab.DataValue = crmForm.AddDays(DataTerminoObra, 90);
                crmForm.all.new_data_prev_emissao_atu_hab.ForceSubmit = true;
            }
        }

        /// Aba:    Doc. Encerramento de Obra
        /// Sessão: Averbação do Habite-se
        crmForm.CalculaAverbacaoHabitese = function() {
            var DataPrevistaProtocoloAverb = null;
            var DataPrevistaAverbacao = null;

            if (crmForm.all.new_atua_dt_prot_atualizada_averb.DataValue) {
                if (crmForm.all.new_data_real_emissao_cnd.DataValue != null) {
                    DataPrevistaProtocoloAverb = crmForm.AddDays(crmForm.all.new_data_real_emissao_cnd.DataValue, 15);
                } else if (crmForm.all.new_data_prev_emissao_atu_cnd.DataValue != null) {
                    DataPrevistaProtocoloAverb = crmForm.AddDays(crmForm.all.new_data_prev_emissao_atu_cnd.DataValue, 15);
                }
            } else {
                DataPrevistaProtocoloAverb = crmForm.all.new_data_prev_protocolo_atu_averb.DataValue;
            }

            if (crmForm.all.new_atua_dt_averbacao_atualizada.DataValue && DataPrevistaProtocoloAverb != null) {
                DataPrevistaAverbacao = crmForm.AddDays(new Date(DataPrevistaProtocoloAverb), 75);
            }

            if (crmForm.all.new_data_prevista_protocolo_averb.DataValue == null) {
                crmForm.all.new_data_prevista_protocolo_averb.DataValue = DataPrevistaProtocoloAverb;
                crmForm.all.new_data_prevista_protocolo_averb.ForceSubmit = true;
            }

            if (crmForm.all.new_atua_dt_prot_atualizada_averb.DataValue) {
                crmForm.all.new_data_prev_protocolo_atu_averb.DataValue = DataPrevistaProtocoloAverb;
                crmForm.all.new_data_prev_protocolo_atu_averb.ForceSubmit = true;
            }

            if (crmForm.all.new_data_prevista_averbacao.DataValue == null) {
                crmForm.all.new_data_prevista_averbacao.DataValue = DataPrevistaAverbacao;
                crmForm.all.new_data_prevista_averbacao.ForceSubmit = true;
            }

            if (crmForm.all.new_atua_dt_averbacao_atualizada.DataValue) {
                crmForm.all.new_data_prev_averbacao_atu.DataValue = DataPrevistaAverbacao;
                crmForm.all.new_data_prev_averbacao_atu.ForceSubmit = true;
            }
        }

        /// Calcula as datas Habite-se/Averbação/Certidão/Laudo/etc.
        crmForm.CalculaDatasEngenharia = function() {
            var DataTerminoObra = crmForm.all.new_data_termino_obra.DataValue;

            // Protocolo ISS, emissao ISS----------------------------//
            crmForm.all.new_data_prevista_protocolo_iss.DataValue = crmForm.AddDays(DataTerminoObra, -30);
            crmForm.all.new_data_prevista_protocolo_iss.ForceSubmit = true;
            crmForm.all.new_data_prevista_emissao_iss.DataValue = crmForm.all.new_data_termino_obra.DataValue;
            crmForm.all.new_data_prevista_emissao_iss.ForceSubmit = true;

            // Protocolo Bombeiro, emissao Bombeiro--------------//
            crmForm.all.new_data_prevista_protocolo_cb.DataValue = crmForm.AddDays(DataTerminoObra, -15);
            crmForm.all.new_data_prevista_protocolo_cb.ForceSubmit = true;
            crmForm.all.new_data_prevista_emissao_cb.DataValue = crmForm.AddDays(DataTerminoObra, 30);
            crmForm.all.new_data_prevista_emissao_cb.ForceSubmit = true;

            crmForm.CalculaBaixaDeObraHabitese(DataTerminoObra);

            // Salva a data prevista protocolo atualizada e a data prevista emissão atualizada
            DataPrevEmissaoAtuHab = crmForm.all.new_data_prev_emissao_atu_hab.DataValue == null ? null : crmForm.all.new_data_prev_emissao_atu_hab.DataValue;
            DataPrevProtocoloAtuHab = crmForm.all.new_data_prev_protocolo_atu_hab.DataValue == null ? null : crmForm.all.new_data_prev_protocolo_atu_hab.DataValue;
            crmForm.DifDtProtocoloDtEmissao();

            // Protocolo CND INSS, emissao CND INSS------------//
            var DataPrevistaProtocoloCND = null;
            var DataPrevistaEmissaoCND = null;

            if (crmForm.all.new_data_real_emissao_hab.DataValue != null) {
                DataPrevistaProtocoloCND = crmForm.AddDays(crmForm.all.new_data_real_emissao_hab.DataValue, 10);
                DataPrevistaEmissaoCND = crmForm.AddDays(crmForm.all.new_data_real_emissao_hab.DataValue, 25);
            } else if (crmForm.all.new_data_prev_emissao_atu_hab.DataValue != null) {
                DataPrevistaProtocoloCND = crmForm.AddDays(crmForm.all.new_data_prev_emissao_atu_hab.DataValue, 10);
                DataPrevistaEmissaoCND = crmForm.AddDays(crmForm.all.new_data_prev_emissao_atu_hab.DataValue, 25);
            }

            if (crmForm.all.new_data_prevista_protocolo_cnd.DataValue == null) {
                crmForm.all.new_data_prevista_protocolo_cnd.DataValue = DataPrevistaProtocoloCND;
                crmForm.all.new_data_prevista_protocolo_cnd.ForceSubmit = true;
            }

            if (crmForm.all.new_data_prevista_emissao_cnd.DataValue == null) {
                crmForm.all.new_data_prevista_emissao_cnd.DataValue = DataPrevistaEmissaoCND;
                crmForm.all.new_data_prevista_emissao_cnd.ForceSubmit = true;
            }

            if (crmForm.all.new_atua_dt_prot_atualizada_cnd.DataValue) {
                crmForm.all.new_data_prev_protocolo_atu_cnd.DataValue = DataPrevistaProtocoloCND;
                crmForm.all.new_data_prev_protocolo_atu_cnd.ForceSubmit = true;
            }

            if (crmForm.all.new_atua_dt_emis_atualizada_cnd.DataValue) {
                crmForm.all.new_data_prev_emissao_atu_cnd.DataValue = DataPrevistaEmissaoCND;
                crmForm.all.new_data_prev_emissao_atu_cnd.ForceSubmit = true;
            }

            // Elaboração material instituição-----------------------//
            var DtPrevEntregaMaterial = null;

            if (crmForm.all.new_data_real_protocolo_cnd.DataValue != null)
                DtPrevEntregaMaterial = crmForm.AddDays(crmForm.all.new_data_real_protocolo_cnd.DataValue, 10);
            else if (crmForm.all.new_data_prev_protocolo_atu_cnd.DataValue != null)
                DtPrevEntregaMaterial = crmForm.AddDays(crmForm.all.new_data_prev_protocolo_atu_cnd.DataValue, 10);

            crmForm.all.new_data_prev_entrega_material.DataValue = DtPrevEntregaMaterial;
            crmForm.all.new_data_prev_entrega_material.ForceSubmit = true;

            crmForm.CalculaAverbacaoHabitese();

            // Desmembramento IPTU-------------------------------------------//
            var DataPrevistaInicio = null;
            var DataPrevistaConclusao = null;

            if (crmForm.all.new_data_real_emissao_hab.DataValue != null) {
                DataPrevistaInicio = crmForm.AddDays(crmForm.all.new_data_real_emissao_hab.DataValue, 5);
                DataPrevistaConclusao = crmForm.AddDays(crmForm.all.new_data_real_emissao_hab.DataValue, 35);
            } else if (crmForm.all.new_data_prev_emissao_atu_hab.DataValue != null) {
                DataPrevistaInicio = crmForm.AddDays(crmForm.all.new_data_prev_emissao_atu_hab.DataValue, 5);
                DataPrevistaConclusao = crmForm.AddDays(crmForm.all.new_data_prev_emissao_atu_hab.DataValue, 35);
            }

            if (crmForm.all.new_data_prevista_inicio.DataValue == null) {
                crmForm.all.new_data_prevista_inicio.DataValue = DataPrevistaInicio;
                crmForm.all.new_data_prevista_inicio.ForceSubmit = true;
            }

            if (crmForm.all.new_data_prevista_conclusao.DataValue == null) {
                crmForm.all.new_data_prevista_conclusao.DataValue = DataPrevistaConclusao;
                crmForm.all.new_data_prevista_conclusao.ForceSubmit = true;
            }

            crmForm.all.new_data_prev_inicio_proc_atu.DataValue = DataPrevistaInicio;
            crmForm.all.new_data_prev_inicio_proc_atu.ForceSubmit = true;
            crmForm.all.new_data_prev_conclusao_proc_atu.DataValue = DataPrevistaConclusao;
            crmForm.all.new_data_prev_conclusao_proc_atu.ForceSubmit = true;
        }

        if (!crmForm.UsuarioPertenceEquipe("Engenharia - Planejamento")) {
            crmForm.all.new_data_termino_obra.Disabled = true;
            crmForm.all.new_atua_dt_termino_obra.Disabled = true;
        }

        /* Atualiza a data termino obra referente ao módulo village */
        crmForm.AtualizaDataTerminoObra();
        if (crmForm.all.new_atua_dt_termino_obra.DataValue != null &&
            crmForm.all.new_atua_dt_termino_obra.DataValue) {
            crmForm.CalculaDatasEngenharia();
        }

        /// Desabilita campo no formulário
        crmForm.DesabilitarCampos = function(campos, tipo) {
            var tmp = campos.toString().split(";");

            for (var i = 0; i < tmp.length; i++) {
                var atributo = eval("crmForm.all." + tmp[i]);
                if (atributo != null)
                    atributo.Disabled = tipo;
            }
        }

        crmForm.ConfiguraPerfilEngenhariaPrazos = function() {

            var Campos_DocEncerramentoObra_PedidoHabiteSe = "new_possui_certidao_baixa_hab;new_data_termino_obra;new_atua_dt_termino_obra;new_data_prevista_protocolo_hab;new_data_prev_protocolo_atu_hab;new_atua_dt_prot_atualizada_hab;new_data_real_protocolo_hab;new_numero_protocolo_hab;new_data_prev_emissao_atu_hab;new_atua_dt_emis_atualizada_hab;new_data_real_emissao_hab";

            //Verifica se usuário logado pertence a equipe 'Engenharia - Prazos'
            var perfilEngenhariaPrazos = crmForm.UsuarioPertenceEquipe("Engenharia - Prazos");
            var perfilEngenhariaPrazosAtualizaPrevistaAverbacao = crmForm.UsuarioPertenceEquipe("Altera data Prevista de Averbação");
            crmForm.DesabilitarCampos(Campos_DocEncerramentoObra_PedidoHabiteSe, !perfilEngenhariaPrazos);

            if (perfilEngenhariaPrazos) { //se for automático a data prevista de emissão atualizada fica desabilitada
                crmForm.all.new_atua_dt_emis_atualizada_hab.FireOnChange();
                crmForm.all.new_atua_dt_termino_obra.FireOnChange();
                crmForm.all.new_atua_dt_prot_atualizada_hab.FireOnChange();

            }

            if (perfilEngenhariaPrazosAtualizaPrevistaAverbacao) {
                crmForm.all.new_atua_dt_averbacao_atualizada.FireOnChange();
                if (crmForm.all.new_data_real_emissao_hab.DataValue != null) {
                    crmForm.all.new_atua_dt_averbacao_atualizada.Disabled = false;
                }
            }

        }

        crmForm.ConfiguraPerfilEngenhariaPrazos();

        if (crmForm.UsuarioPertenceEquipe("Desenv.Imobiliário")) {
            crmForm.all.new_data_liberacao_credito.Disabled = false;
            crmForm.all.new_averbacao_retida_credito.Disabled = false;
        } else {
            crmForm.all.new_data_liberacao_credito.Disabled = true;
            crmForm.all.new_averbacao_retida_credito.Disabled = true;
        }
        if (crmForm.UsuarioPertenceEquipe(Equipe.ADMINISTRADORES)) {
            crmForm.DesabilitarCampos("new_exibir_habitese", false);
        } else {
            crmForm.DesabilitarCampos("new_exibir_habitese", true);
        }
    }
} catch (error) {
    formularioValido = false;
    alert("Ocorreu um erro no formulário.\n" + error.description);
}