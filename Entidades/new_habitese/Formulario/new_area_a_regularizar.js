﻿crmForm.all.new_area_modulo_village.DataValue = crmForm.all.new_area_a_regularizar.DataValue;
crmForm.all.new_area_a_regularizar_m2.DataValue = crmForm.all.new_area_a_regularizar.DataValue;
crmForm.all.new_area_a_regularizar_m2.FireOnChange();

// Cálculo do valor por metro quadrado
if ((crmForm.all.new_area_a_regularizar.DataValue != null) && (crmForm.all.new_datas_habiteseid.DataValue != null)) {
    var attributes = "new_ntotal_unidades_bloco";
    var cmd = new RemoteCommand("MrvService", "GetEntity", "/MRVCustomizations/");

    cmd.SetParameter("entity", "new_modulovillageengenharia");
    cmd.SetParameter("primaryField", "new_modulovillageengenhariaid");
    cmd.SetParameter("entityId", crmForm.all.new_datas_habiteseid.DataValue[0].id);
    cmd.SetParameter("fieldList", attributes);

    var oResult = cmd.Execute();
    if (oResult.Success) {
        if ((oResult.ReturnValue.Mrv.Found) && (oResult.ReturnValue.Mrv.new_ntotal_unidades_bloco != null) && (oResult.ReturnValue.Mrv.new_ntotal_unidades_bloco != 0)) {
            crmForm.all.new_area_media_unidades_m2.DataValue = crmForm.all.new_area_a_regularizar.DataValue/oResult.ReturnValue.Mrv.new_ntotal_unidades_bloco;                   
        }
        else {
            crmForm.all.new_area_media_unidades_m2.DataValue = null;
        }
    }
    else
        alert("Erro na consulta ao WebService.\nContate o administrador do sistema.");
}
else {
    crmForm.all.new_area_media_unidades_m2.DataValue = null;
}    

