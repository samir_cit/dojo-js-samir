﻿if (crmForm.all.new_data_termino_obra.DataValue == null) 
{
    alert("Você deve fornecer um valor para Data Prevista de Término de Obra.");
    crmForm.all.new_data_termino_obra.DataValue = DataTerminoObra;
    crmForm.all.new_data_termino_obra.SetFocus();
}
else 
{
    DataTerminoObra = crmForm.all.new_data_termino_obra.DataValue;
    crmForm.CalculaDatasEngenharia();
}
