﻿if (crmForm.all.new_atua_dt_prot_atualizada_averb.DataValue) 
{
    // Atualização Automática
    crmForm.all.new_data_prev_protocolo_atu_averb.Disabled = true;
    if (AtualizaDatas) 
        crmForm.CalculaDatasEngenharia();
}
else 
{
    // Atualização Manual
    crmForm.all.new_data_prev_protocolo_atu_averb.Disabled = false
}
