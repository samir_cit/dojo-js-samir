﻿//script para aparecer a data de entrega do material, caso a empresa esteja contratada

if (crmForm.all.new_empresa_contratada.DataValue == false) {
    crmForm.all.new_data_entrega_material_c.style.visibility = 'hidden';
    crmForm.all.new_data_entrega_material_d.style.visibility = 'hidden';
}
else {
    crmForm.all.new_data_entrega_material_c.style.visibility = 'visible';
    crmForm.all.new_data_entrega_material_d.style.visibility = 'visible';
}
