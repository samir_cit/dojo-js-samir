﻿try {

    if (!formularioValido) {
        alert("Ocorreu um erro no formulário. Impossível salvar.");
        event.returnValue = false;
        return false;
    }
    
    if (!crmForm.IsDirty)
    return false;
    
    // Chama método para recuperar as casas realcionadas
    if(crmForm.FormType != 1)
    {
        var oCmd = new RemoteCommand("MrvService", "SetCasasHabitese", "/MrvCustomizations/");
        oCmd.SetParameter("HabiteseId", crmForm.ObjectId);
        var oResult = oCmd.Execute();
        if(oResult.Success)
        {
           if(oResult.ReturnValue.Mrv.ErrorSoap)
              alert(oResult.ReturnValue.Mrv.ErrorSoap);
           if(oResult.ReturnValue.Mrv.Error)
              alert(oResult.ReturnValue.Mrv.Error);
           else
              crmForm.all.new_casas.DataValue = oResult.ReturnValue.Mrv.Casas.toString();
        }
    }    
        
    // Transformar caracteres em maiusculos
    var elm = document.getElementsByTagName("input");
    for (i = 0; i< elm.length; i++)
    {
        if (elm[i].type == "text")
            if ((elm[i].DataValue != null) && (elm[i].id != "new_name")) { 
                 try {
                    elm[i].DataValue = elm[i].DataValue.toUpperCase(); 
                 }
                 catch (e) {}
            }
    }

    // Habilitar campos
    crmForm.all.new_name.Disabled = false;
    crmForm.all.new_casas.Disabled = false;
    
    crmForm.all.new_data_prevista_protocolo_iss.Disabled = false;
    crmForm.all.new_data_prevista_emissao_iss.Disabled = false;

    crmForm.all.new_data_prevista_protocolo_cb.Disabled = false;
    crmForm.all.new_data_prevista_emissao_cb.Disabled = false;

    crmForm.all.new_data_prevista_protocolo_hab.Disabled = false;
    crmForm.all.new_data_prevista_emissao_hab.Disabled = false;
    crmForm.all.new_data_prev_protocolo_atu_hab.Disabled = false;
    crmForm.all.new_data_prev_emissao_atu_hab.Disabled = false;

    crmForm.all.new_data_prevista_protocolo_cnd.Disabled = false;
    crmForm.all.new_data_prevista_emissao_cnd.Disabled = false;
    crmForm.all.new_data_prev_protocolo_atu_cnd.Disabled = false;
    crmForm.all.new_data_prev_emissao_atu_cnd.Disabled = false;

    crmForm.all.new_data_prev_entrega_material.Disabled = false;

    crmForm.all.new_data_prevista_protocolo_averb.Disabled = false;
    crmForm.all.new_data_prevista_averbacao.Disabled = false;
    crmForm.all.new_data_prev_protocolo_atu_averb.Disabled = false;
    crmForm.all.new_data_prev_averbacao_atu.Disabled = false;

    crmForm.all.new_data_prevista_inicio.Disabled = false;
    crmForm.all.new_data_prevista_conclusao.Disabled = false;
    crmForm.all.new_data_prev_inicio_proc_atu.Disabled = false;
    crmForm.all.new_data_prev_conclusao_proc_atu.Disabled = false;

    crmForm.all.new_data_termino_obra.Disabled = false;

    crmForm.all.new_atua_dt_prot_atualizada_averb.Disabled = false;
    crmForm.all.new_atua_dt_averbacao_atualizada.Disabled = false;
    
} catch (error) {
    alert("Ocorreu um erro no formulário.\n" + error.description);
    event.returnValue = false;
    return false;
}