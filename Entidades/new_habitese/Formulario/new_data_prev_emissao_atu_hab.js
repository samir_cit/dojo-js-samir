﻿if (crmForm.all.new_data_prev_emissao_atu_hab.DataValue == null) 
{
    alert("Você deve fornecer um valor para Data Prevista de Emissão Atualizada.");
    crmForm.all.new_data_prev_emissao_atu_hab.DataValue = DataPrevEmissaoAtuHab;
    crmForm.all.new_data_prev_emissao_atu_hab.SetFocus();
}
else 
{
    if (crmForm.DifDtProtocoloDtEmissao()) 
    {
        DataPrevEmissaoAtuHab = crmForm.all.new_data_prev_emissao_atu_hab.DataValue;
        crmForm.CalculaDatasEngenharia();
    }
    else
        crmForm.all.new_data_prev_emissao_atu_hab.DataValue = DataPrevEmissaoAtuHab;
}
