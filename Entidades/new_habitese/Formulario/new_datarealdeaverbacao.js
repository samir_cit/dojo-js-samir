﻿var dataAtual = new Date();
if (crmForm.all.new_data_real_averbacao.DataValue && crmForm.all.new_data_real_averbacao.DataValue > dataAtual) {
    alert("Data real de averbação de habite-se não pode ser preenchida com valor maior que data atual.");
    crmForm.all.new_data_real_averbacao.DataValue = null;
}