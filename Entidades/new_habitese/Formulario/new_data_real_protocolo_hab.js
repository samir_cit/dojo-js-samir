﻿// Se não informou Data Real Protocolo, desabilita Número do Protocolo 

if (crmForm.all.new_data_real_emissao_hab.DataValue == null)
{
    crmForm.all.new_numero_protocolo_hab_c.style.visibility = 'hidden';
    crmForm.all.new_numero_protocolo_hab_d.style.visibility = 'hidden';
    crmForm.all.new_numero_protocolo_hab.DataValue = null;
    crmForm.SetFieldReqLevel('new_numero_protocolo_hab', false);    
}
else 
{
    crmForm.all.new_numero_protocolo_hab_c.style.visibility = 'visible';
    crmForm.all.new_numero_protocolo_hab_d.style.visibility = 'visible';
    crmForm.SetFieldReqLevel('new_numero_protocolo_hab', true);
}
