﻿//script para mostrar status do doc INSS------------------------------------
crmForm.all.new_guia_autenticada_gps.FireOnChange();

//HABILITANDO E DESABILITANDO CAMPOS
if (crmForm.all.new_copia_autenticada_habitese.DataValue == false) {
    crmForm.all.new_data_recebimento_habitese.Disabled = true;
    crmForm.SetFieldReqLevel('new_data_recebimento_habitese', false);

    crmForm.all.new_numero_protocolo_cnd.Disabled = true;
    crmForm.SetFieldReqLevel('new_numero_protocolo_cnd', false);
}
else {
    crmForm.all.new_data_recebimento_habitese.Disabled = false;
    crmForm.SetFieldReqLevel('new_data_recebimento_habitese', true);

    crmForm.all.new_numero_protocolo_cnd.Disabled = false;
    crmForm.SetFieldReqLevel('new_numero_protocolo_cnd', true);
}


