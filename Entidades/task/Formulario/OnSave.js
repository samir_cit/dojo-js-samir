﻿try {
    if (!formularioValido) {
        alert("Ocorreu um erro no formulário. Impossível salvar.");
        event.returnValue = false;
        return false;
    }
      
    //Deixar Maiusculo o Título
    crmForm.all.subject.DataValue = (crmForm.all.subject.DataValue != null) ? crmForm.all.subject.DataValue.toUpperCase() : "";

    var item = crmForm.ObterItemSelecionado();

    if (item == false) {
        event.returnValue = false;
        return false;
    }   
        
    if (!crmForm.UsuarioPertenceEquipe("Administradores") &&
        crmForm.all.new_tarefa_mrvcredito.DataValue && (event.Mode == 58 || event.Mode == 5)) 
    {   alert("Atenção! Essa tarefa pode ser fechada somente pelo MRV Crédito ou pelo equipe de Administradores.");
        event.returnValue = false;
        return false;
    }   
    // Verifica se teve alguma alteração    
    if (!crmForm.IsDirty) return false;     
    //Habilitando campos
    crmForm.all.new_tipo_chamado.Disabled = false;
    crmForm.all.new_data_limite_resolucao.Disabled = false;       
    
} catch (error) {
    alert("Ocorreu um erro no formulário.\n" + error.description);
    event.returnValue = false;
    return false;
}
