﻿formularioValido = true;
try {

    var motivoFechamentoProcesso;
    function AbrirJanelaArvoreAssuntos() {
        var attribute = crmForm.all.new_assunto_tarefa;
        retorno = openStdDlg("http://" + window.location.host + "/" + ORG_UNIQUE_NAME + "/MRVWeb/Assunto/ArvoreAssunto.aspx?id=" + crmForm.all.new_assuntodaocorrenciaid.DataValue[0].id + " ", null, 600, 450);

        if (typeof (retorno) != "undefined" && retorno != undefined) {
            var lookupData = new Array();
            lookupItem = new Object();
            lookupItem.id = retorno.Id;
            lookupItem.type = 129;
            lookupItem.name = retorno.Name;
            lookupData[0] = lookupItem;
            attribute.DataValue = lookupData;
            attribute.FireOnChange();
        }
    }
    
    var TipoMotivoFechamento =
        {
            OK: "17",
            NAO_OK: "18",
            CANCELADO: "334"
        }

    crmForm.ObterItemSelecionado = function() {
        var item = new Object();
        item.valor = "";

        if (!crmForm.all.ownerid.IsDirty && crmForm.all.new_fechamentodoprocesso_d.style.display != "none" ) {
            for (var index = 0; index < crmForm.all.new_fechamentodoprocesso.length; index++) {
                if (crmForm.all.new_fechamentodoprocesso[index].selected) {
                    item.valor = crmForm.all.new_fechamentodoprocesso[index].value;
                    break;
                }
            }

            if (item.valor == "") {
                alert("Você deve fornecer um valor para Fechamento do processo.");
                return false;
            }
        }
        else {
            return true;
        }

        return item;
    }


    crmForm.AbrirNovoArvoreAssuntos = function() {
        if (crmForm.all.new_assuntodaocorrenciaid.DataValue != null) {
            var attribute = crmForm.all.new_assunto_tarefa;
            oImg = eval('attribute' + '.parentNode.childNodes[0]');
            oImg.onclick = AbrirJanelaArvoreAssuntos;
        }
        else
            crmForm.EscondeCampo("new_assunto_tarefa", true);
    }


    //desabilita a seção Cessão de Direito
    function DesabilitaCessaDireito() {
        crmForm.all.new_tipo_cessao.Disabled = true;
        crmForm.all.new_primeiroclienteid.Disabled = true;
        crmForm.all.new_segundoclienteid.Disabled = true;
        crmForm.all.new_terceiroclienteid.Disabled = true;
        crmForm.all.new_quartoclienteid.Disabled = true;
    }

    //oculta a seção Cessão de Direito
    function OcultarSecao_InfCessaoDireito() {
        if (crmForm.all.new_assuntodaocorrenciaid.DataValue == null ||
            crmForm.all.new_assuntodaocorrenciaid.DataValue[0].name.toLowerCase() != 'cessão de direito') {

            //Ocultando campos Campos da seção Cessão de Direito
            crmForm.EscondeCampo("new_tipo_cessao", true);
            crmForm.EscondeCampo("new_primeiroclienteid", true);
            crmForm.EscondeCampo("new_segundoclienteid", true);
            crmForm.EscondeCampo("new_terceiroclienteid", true);
            crmForm.EscondeCampo("new_quartoclienteid", true);
            //Ocultando a seção Cessão de Direito da Guia
            crmForm.EscondeSecao("new_tipo_cessao", true);
        }
    }

    function IsTarefaAnaliseCredito() {
        if (crmForm.all.new_assuntodaocorrenciaid.DataValue == null) return false;
        return (crmForm.all.new_assuntodaocorrenciaid.DataValue[0].name.toLowerCase() != null &&
                 (crmForm.all.new_assuntodaocorrenciaid.DataValue[0].name.toLowerCase() == 'cessão de direito' ||
                    crmForm.all.new_assuntodaocorrenciaid.DataValue[0].name.toLowerCase() == 'comodato' ||
                    crmForm.all.new_assuntodaocorrenciaid.DataValue[0].name.toLowerCase() == 'inclusão de parcela intermediária no fb' ||
                    crmForm.all.new_assuntodaocorrenciaid.DataValue[0].name.toLowerCase() == 'quitações fgts / carta de crédito'
                  )
                );
    }

    function RemoverBotoesPadroes() {
        crmForm.RemoveBotaoPadraoCRM("_MIonActionMenuClickdelete4212");
        crmForm.RemoveBotaoPadraoCRM("_MIchangeStatedeactivate42125");
        crmForm.RemoveBotaoPadraoCRM("_MSsubconvertActivity");
        crmForm.RemoveBotaoPadraoCRM("_MBSaveAsCompleted");
        crmForm.RemoveBotaoPadraoCRM("convertActivity");
    }

    /* Usuários da equipe 'Bloqueio Criação de Atividades' só podem criar atividades a partir de uma ocorrência. */
    function VerificaSeUsuarioTemPermissaoParaCriarAtividades() {
        if (crmForm.FormType == TypeCreate && crmForm.UsuarioPertenceEquipe("Bloqueio Criação de Atividades")) {
            alert('Usuário possui permissão para criar atividades somente vinculadas à ocorrências.');
            window.close();
        }
    }

    //obter campos que foram escolhidos para o assunto
    crmForm.ObterCamposAdicionaisPorAssunto = function() {
        if (crmForm.all.new_assunto_tarefa.DataValue != null) {
            var cmd = new RemoteCommand("MrvService", "ObterCamposAdicionaisTarefaPorAssunto", "/MRVCustomizations/");
            cmd.SetParameter("subjectId", crmForm.all.new_assunto_tarefa.DataValue[0].id);
            var resultado = cmd.Execute();
            if (crmForm.TratarRetornoRemoteCommand(resultado, "ObterCamposAdicionaisTarefaPorAssunto")) {
                var tmpAtributos = resultado.ReturnValue.Mrv.Atributos.toString().split(';');
                for (var index = 0; index < tmpAtributos.length; index++) {
                    var tmp = tmpAtributos[index].split('|');
                    if (document.getElementById(tmp[0]) != null && document.getElementById(tmp[0]) != "") {
                        crmForm.EscondeCampo(tmp[0].toLowerCase(), false);
                        if (tmp[1] == "true")
                            crmForm.SetFieldReqLevel(tmp[0].toLowerCase(), true);
                    }
                }
            }
            else {
                formularioValido = false;
                window.close();
            }
        }
    }

    CamposMapeados = "";
    //obter todos os campos que foram mapeados para serem usados na funcionalidade Campo adicionais.
    function ObterAtributosMapeadosCamposAdicionais() {
        if (CamposMapeados == "") {
            var cmd = new RemoteCommand("MrvService", "ObterCamposMapeadosTarefa", "/MRVCustomizations/");
            var resultado = cmd.Execute();
            if (crmForm.TratarRetornoRemoteCommand(resultado, "ObterCamposMapeadosTarefa")) {
                CamposMapeados = resultado.ReturnValue.Mrv.Atributos.toString();
            } else {
                window.close();
                formularioValido = false;
            }
        }
        return CamposMapeados;
    }

    crmForm.EsconderAtributosCamposAdicionais = function(limpar) {
        var atributos = ObterAtributosMapeadosCamposAdicionais();
        var tmp = atributos.split(';');
        for (var index = 0; index < tmp.length; index++) {
            if (document.getElementById(tmp[index]) != null && document.getElementById(tmp[index]) != "") {
                crmForm.EscondeCampo(tmp[index], true);
                crmForm.SetFieldReqLevel(tmp[index], false);
                if (limpar)
                    eval("crmForm.all." + tmp[index]).DataValue = null;
            }
        }
    }

    crmForm.ConfigurarMotivosFechamento = function() {

        if (crmForm.all.new_assunto_tarefa.DataValue != null) {
            var cmd = new RemoteCommand("MrvService", "ObterItensPicklistMotivoFechamentoPorAssuntoTarefa", "/MRVCustomizations/");
            cmd.SetParameter("assuntoTarefaId", crmForm.all.new_assunto_tarefa.DataValue[0].id);
            var result = cmd.Execute();

            if (crmForm.TratarRetornoRemoteCommand(result, "ObterItensPicklistMotivoFechamentoPorAssuntoTarefa")) {
                var motivosFechamento = result.ReturnValue.Mrv.MotivosFechamento;
                while (crmForm.new_fechamentodoprocesso.Options.length > 0)
                    crmForm.new_fechamentodoprocesso.DeleteOption(crmForm.all.new_fechamentodoprocesso.Options[0].DataValue);

                if (motivosFechamento != "null") {
                    if (motivosFechamento.length != 0) {
                        var itens = motivosFechamento.split(';');
                        for (var index = 0; index < itens.length; index++) {
                            if (itens[index].toString() != "null" && itens[index].toString() != "") {
                                var valores = itens[index].split('*');
                                crmForm.all.new_fechamentodoprocesso.AddOption(valores[0], valores[1]);
                            }
                        }
                    }
                }
            }
            else
                formularioValido = false;
        }
    }

    crmForm.CarregaMotivoFechamentoProcessoExistente = function() {
        crmForm.all.new_fechamentodoprocesso.DataValue = motivoFechamentoProcesso;

    }

    crmForm.EfetuarAgendamento = function() {
        var urlAgenda = "/MRVWEB/Atividade/AgendamentoAT.aspx?id=" + crmForm.ObjectId;
        window.open(urlAgenda, 'Agendamento', 'toolbar=no,location=no,directories=no,status=no,menubar=no,resizable=yes,scrollbars=yes,width=850px,height=600px');
    }
    
    crmForm.ValidarFechamentoProcesso = function() {
		if (!crmForm.all.ownerid.IsDirty && crmForm.all.new_fechamentodoprocesso_d.style.display != "none" ) {
            if (crmForm.all.new_fechamentodoprocesso.DataValue == TipoMotivoFechamento.CANCELADO) {
				alert("A tarefa de efetivar distrato não pode ser fechada com motivo de fechamento do processo igual a Cancelado.\n Favor informar Ok ou Não Ok");
				crmForm.all.new_fechamentodoprocesso.DataValue = "";
                return false;
            }
        }
        return true;
    }

    crmForm.BloqueioAssuntoTarefa = function() 
    {
        var nomeCriador = crmForm.all.createdby.DataValue == null ? '' : crmForm.all.createdby.DataValue[0].name;
        var criadoPorSistema = (nomeCriador == 'TRIDEA CONSULTING' || nomeCriador == 'CRM ADMIN');
        if (crmForm.FormType == TypeUpdate && crmForm.all.new_assunto_tarefa.DataValue != null && criadoPorSistema)
        {
           if (!crmForm.UsuarioPertenceEquipe("CR_MAN_TAREFA"))
              crmForm.all.new_assunto_tarefa.Disabled = true;
        }
    }
    crmForm.EscondeCampoCliente = function() {
        crmForm.EscondeCampo("new_clienteid", true);
        crmForm.EscondeCampo("new_produtoid", true);
    }

    crmForm.EscondeCampoContato = function() {
        crmForm.EscondeCampo("new_contato_condominioid", true);
        crmForm.EscondeCampo("new_empreendimentoid", true);
    }
    crmForm.RegrasTarefaCondominio = function() {
        if ((crmForm.all.new_clienteid.DataValue == null || crmForm.all.new_clienteid.DataValue == "" || crmForm.all.new_clienteid.DataValue == undefined) &&
    (crmForm.all.new_contato_condominioid.DataValue == null || crmForm.all.new_contato_condominioid.DataValue == "" || crmForm.all.new_contato_condominioid.DataValue == undefined)) {
            alert("Cliente da ocorrência relacionada a tarefa não está preenchido, favor realizar o preenchimento.")
            return;
        }

        if (crmForm.all.new_clienteid.DataValue == null || crmForm.all.new_clienteid.DataValue == "" || crmForm.all.new_clienteid.DataValue == undefined) {
            crmForm.EscondeCampo("new_clienteid", true);
            crmForm.EscondeCampo("new_produtoid", true);
        }
        if (crmForm.all.new_contato_condominioid.DataValue == null || crmForm.all.new_contato_condominioid.DataValue == "" || crmForm.all.new_contato_condominioid.DataValue == undefined) {
            crmForm.EscondeCampo("new_contato_condominioid", true);
            crmForm.EscondeCampo("new_empreendimentoid", true);
        }

        
    }
    

    function Load() {
        if (window.opener != null && window.opener.crmForm && crmForm.FormType == TypeCreate && window.opener.crmForm.ObjectTypeCode == 112) {
            if (window.opener.crmForm) {
                if (window.opener.crmForm.subjectid.DataValue != null)
                    crmForm.AddValueLookup(crmForm.all.new_assuntodaocorrenciaid, window.opener.crmForm.subjectid.DataValue[0].id, window.opener.crmForm.subjectid.DataValue[0].type, window.opener.crmForm.subjectid.DataValue[0].name);

                if (window.opener.crmForm.new_assuntoprincipalid.DataValue != null)
                    crmForm.AddValueLookup(crmForm.all.new_assuntoprincipalid, window.opener.crmForm.new_assuntoprincipalid.DataValue[0].id, window.opener.crmForm.new_assuntoprincipalid.DataValue[0].type, window.opener.crmForm.new_assuntoprincipalid.DataValue[0].name);

                if (window.opener.crmForm.new_departamentoid.DataValue != null)
                    crmForm.AddValueLookup(crmForm.all.new_departamentoid, window.opener.crmForm.new_departamentoid.DataValue[0].id, window.opener.crmForm.new_departamentoid.DataValue[0].type, window.opener.crmForm.new_departamentoid.DataValue[0].name);

                if (window.opener.crmForm.customerid.DataValue != null) {
                    var type = window.opener.crmForm.customerid.DataValue[0].typename;
                    switch (type) {
                        case 'account':
                            crmForm.EscondeCampoContato();
                            crmForm.AddValueLookup(crmForm.all.new_clienteid, window.opener.crmForm.customerid.DataValue[0].id, window.opener.crmForm.customerid.DataValue[0].type, window.opener.crmForm.customerid.DataValue[0].name);
                            if (window.opener.crmForm.productid.DataValue != null)
                                crmForm.AddValueLookup(crmForm.all.new_produtoid, window.opener.crmForm.productid.DataValue[0].id, window.opener.crmForm.productid.DataValue[0].type, window.opener.crmForm.productid.DataValue[0].name);
                            break;
                        case 'contact':
                            crmForm.EscondeCampoCliente();
                            crmForm.AddValueLookup(crmForm.all.new_contato_condominioid, window.opener.crmForm.customerid.DataValue[0].id, window.opener.crmForm.customerid.DataValue[0].type, window.opener.crmForm.customerid.DataValue[0].name);
                            if (window.opener.crmForm.new_empreendimentoid.DataValue != null)
                                crmForm.AddValueLookup(crmForm.all.new_empreendimentoid, window.opener.crmForm.new_empreendimentoid.DataValue[0].id, window.opener.crmForm.new_empreendimentoid.DataValue[0].type, window.opener.crmForm.new_empreendimentoid.DataValue[0].name);
                            break;
                    }
                    
                    crmForm.RegrasTarefaCondominio();
                }
                

                if (window.opener.crmForm.new_regionalid.DataValue != null)
                    crmForm.AddValueLookup(crmForm.all.new_regionalid, window.opener.crmForm.new_regionalid.DataValue[0].id, window.opener.crmForm.new_regionalid.DataValue[0].type, window.opener.crmForm.new_regionalid.DataValue[0].name);

                if (window.opener.crmForm.casetypecode.DataValue != null)
                    crmForm.all.new_tipo_chamado.DataValue = window.opener.crmForm.casetypecode.DataValue;

                if (window.opener.crmForm.all.new_data_limite_resolucao.DataValue != null)
                    crmForm.all.new_data_limite_resolucao.DataValue = window.opener.crmForm.all.new_data_limite_resolucao.DataValue;

            }
            else {
                VerificaSeUsuarioTemPermissaoParaCriarAtividades();
                crmForm.EscondeCampo("new_clienteid");
            }
        }
        else
            VerificaSeUsuarioTemPermissaoParaCriarAtividades();


        if (crmForm.FormType == TypeDisabled || crmForm.FormType == TypeUpdate) {
            crmForm.onload = AfterLoad;

            function AfterLoad() {
                if (event.srcElement.readyState == "complete" || event.srcElement.readyState == "loaded") {
                    window.opener.localtion.reload();
                }
            }
        }

        // A descrição ficará habilitada somente no momento da criação
        if (crmForm.FormType != TypeCreate)
            crmForm.all.description.Disabled = true;

        //verifica se a tarefa é de uma assistência técnica e desabilita tudo
        if (crmForm.all.regardingobjectid.DataValue != null) {
            if (crmForm.all.regardingobjectid.DataValue[0].name.toLowerCase() == "assistência técnica") {
                crmForm.DesabilitarFormulario(true);
                RemoverBotoesPadroes();
                crmForm.all.ownerid.Disabled = true;
            }
        }

        if (crmForm.all.regardingobjectid.DataValue != null && crmForm.all.new_recebido_por.DataValue == null) {
            var cmd = new RemoteCommand('MrvService', 'ObterRecebidoPor', '/MRVCustomizations/');
            cmd.SetParameter("ocorrenciaId", crmForm.all.regardingobjectid.DataValue[0].id);
            var result = cmd.Execute();
            if (result.Success) {
                crmForm.all.new_recebido_por.DataValue = parseInt(result.ReturnValue.Mrv.RecebidoPor);
                crmForm.all.new_recebido_por.Disabled = true;
            } else {
                alert('Erro na comunicação com o MrvService. Contate o administrador do sistema.');
                crmForm.DesabilitarFormulario(true);
                RemoverBotoesPadroes();
            }
        }

        //Adicionar o botao note
        var objToolbar = new InlineToolbar("percentcomplete");
        objToolbar.AddButton("btnAbrir", "Nova", "15%", AddNote, "/_imgs/ico_16_5.gif");

        //se analise enviada ou não é tarefa de analise de credito
        if (crmForm.all.new_analise_enviada_credito.DataValue || !IsTarefaAnaliseCredito()) {
            crmForm.RemoveBotao("EnviarSolicitaodeAnlise");
            DesabilitaCessaDireito();
        }
        OcultarSecao_InfCessaoDireito();
        crmForm.SomenteProprietario("convertActivity;_MBSaveAsCompleted;_MIonActionMenuClickdelete4212;_MIchangeStatedeactivate42125;_MSsubconvertActivity");

        crmForm.AbrirNovoArvoreAssuntos();

        crmForm.EscondeCampo("new_recusado", true);
        if (crmForm.all.new_recusado.DataValue != null && crmForm.all.new_recusado.DataValue == true) {
            //Mostra status e razão do status
            var status = document.getElementById("EntityStatusText").innerText;
            document.getElementById("EntityStatusText").innerText = status + " - Recusado";
        }
        crmForm.EsconderAtributosCamposAdicionais(false);
        crmForm.ObterCamposAdicionaisPorAssunto();

        if (crmForm.all.new_fechamentodoprocesso.DataValue != null)
            motivoFechamentoProcesso = crmForm.all.new_fechamentodoprocesso.DataValue;

        if (crmForm.FormType == TypeCreate || crmForm.FormType == TypeUpdate)
            crmForm.ConfigurarMotivosFechamento();
        if (crmForm.FormType == TypeUpdate) {
            crmForm.CarregaMotivoFechamentoProcessoExistente();
        }
        if (crmForm.FormType != TypeUpdate || crmForm.all.new_agendamento_at.DataValue != true)
            crmForm.RemoveBotao('Agendamento');
        crmForm.BloqueioAssuntoTarefa();
        crmForm.RegrasTarefaCondominio();
    }

    function loadScript() {
        var oHead = document.getElementsByTagName('head')[0];
        var oScript = document.createElement('script');
        oScript.type = "text/javascript";
        oScript.src = "/_static/Base.js";

        oScript.onreadystatechange = function() {
            if (this.readyState == 'complete' || this.readyState == 'loaded') {
                Load();
            }
        }
        oHead.appendChild(oScript);
    }

    loadScript();

} catch (erro) {
    alert("Ocorreru um erro no formulário.\n" + erro.description);
}
