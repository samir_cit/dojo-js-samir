﻿formularioValido = true;
mensagemErro = "Ocorreu um erro no formulário. Impossível salvar.";
try {
    function loadScript() {
        var oScript = document.createElement('script');
        oScript.type = "text/javascript";
        oScript.src = "/_static/Base.js";

        var oHead = document.getElementsByTagName('head')[0];
        oHead.appendChild(oScript);

        oScript.onreadystatechange = function() {
            if (this.readyState == 'complete' || this.readyState == 'loaded')
                Load();
        }
    }
    loadScript();

    function Load() {
        ValidarGravacaoAbrangenciaNacional = function() {
            var cmd = new RemoteCommand("MrvService", "VerificaInclusaoTipoAbrangencia", "/MRVCustomizations/");
            cmd.SetParameter("promocaoId", crmForm.all.new_promooid.DataValue[0].id);
            var result = cmd.Execute();
            if (result.Success) {
                if (result.ReturnValue.Mrv.Error) {
                    formularioValido = false;
                    mensagemErro = result.ReturnValue.Mrv.Error;
                } else if (result.ReturnValue.Mrv.ErrorSoap) {
                    formularioValido = false;
                    mensagemErro = result.ReturnValue.Mrv.ErrorSoap;
                }
                else {
                    /// 1 - Inclusão permitida para abrangência nacional e NÃO nacional
                    /// 2 - Inclusao permitida somente para abrangência NÃO nacional
                    /// 3 - Inclusão de abrangência não permitida para a promoção informada
                    /// 4 - Permitida alteração do atributo abrangência nacional
                    var NivelPermissao = result.ReturnValue.Mrv.NivelPermissao;

                    if (NivelPermissao == "2" || (NivelPermissao == "4" && crmForm.FormType == TypeCreate)) {
                        crmForm.all.new_abrangencianacional.DataValue = false;
                        crmForm.DesabilitarCampos("new_abrangencianacional", true);
                    }
                    else if (NivelPermissao == "3" && crmForm.FormType == TypeCreate) {
                        formularioValido = false;
                        mensagemErro = "Inclusão de abrangência não permitida para a promoção informada";
                    }
                }
            }
        }

        ValidarAlteracaoAbrangencia = function() {
            var cmd = new RemoteCommand("MrvService", "RecuperaNumeroDeExcecoesDaAbrangencia", "/MRVCustomizations/");
            cmd.SetParameter("abrangenciaId", crmForm.ObjectId);
            var result = cmd.Execute();
            if (result.Success) {
                if (result.ReturnValue.Mrv.Error) {
                    formularioValido = false;
                    mensagemErro = result.ReturnValue.Mrv.Error;
                } else if (result.ReturnValue.Mrv.ErrorSoap) {
                    formularioValido = false;
                    mensagemErro = result.ReturnValue.Mrv.ErrorSoap;
                }
                else {
                    var TotalExecoes = result.ReturnValue.Mrv.TotalExecoes;
                    if (TotalExecoes != "0") {
                        formularioValido = false;
                        mensagemErro = "Esta abrangência não pode ser modificada, pois existem exceções cadastradas.";
                    }
                }
            }
        }

        CompletarDadosAbrangencia = function() {
            var produtoId = crmForm.all.new_produtoid.DataValue == null ? "" : crmForm.all.new_produtoid.DataValue[0].id;
            var blocoId = crmForm.all.new_blocoid.DataValue == null ? "" : crmForm.all.new_blocoid.DataValue[0].id;
            var empreendimentoId = crmForm.all.new_empreendimentoid.DataValue == null ? "" : crmForm.all.new_empreendimentoid.DataValue[0].id;
            var cidadeId = crmForm.all.new_cidadeid.DataValue == null ? "" : crmForm.all.new_cidadeid.DataValue[0].id;
            var regionalId = crmForm.all.new_regionalid.DataValue == null ? "" : crmForm.all.new_regionalid.DataValue[0].id;

            if (produtoId != "") {
                blocoId = ""; empreendimentoId = ""; cidadeId = ""; regionalId = "";
            }
            else if (blocoId != "") {
                empreendimentoId = ""; cidadeId = ""; regionalId = "";
            }
            else if (empreendimentoId != "") {
                cidadeId = ""; regionalId = "";
            }
            else if (cidadeId != "") {
                regionalId = "";
            }

            var cmd = new RemoteCommand("MrvService", "CompletarDadosAbrangencia", "/MRVCustomizations/");
            cmd.SetParameter("regionalId", regionalId);
            cmd.SetParameter("cidadeId", cidadeId);
            cmd.SetParameter("empreendimentoId", empreendimentoId);
            cmd.SetParameter("blocoId", blocoId);
            cmd.SetParameter("produtoId", produtoId);

            var result = cmd.Execute();
            if (result.Success) {
                if (result.ReturnValue.Mrv.Error) {
                    formularioValido = false;
                    mensagemErro = result.ReturnValue.Mrv.Error;
                } else if (result.ReturnValue.Mrv.ErrorSoap) {
                    formularioValido = false;
                    mensagemErro = result.ReturnValue.Mrv.ErrorSoap;
                }
                else {
                    if (typeof (result.ReturnValue.Mrv.RegionalId) != "object") {
                        var lookup = result.ReturnValue.Mrv.RegionalId.split(";");
                        if (lookup.length == 3) {
                            crmForm.AddValueLookup(crmForm.all.new_regionalid, lookup[0], 10141, lookup[1]);
                            AtribuirFilterLookup(crmForm.all.new_regionalid, crmForm.all.new_cidadeid, "new_name", "new_regionalid");
                        }
                    }

                    if (typeof (result.ReturnValue.Mrv.CidadeId) != "object") {
                        var lookup = result.ReturnValue.Mrv.CidadeId.split(";");
                        if (lookup.length == 3) {
                            crmForm.AddValueLookup(crmForm.all.new_cidadeid, lookup[0], 10031, lookup[1]);
                            AtribuirFilterLookup(crmForm.all.new_cidadeid, crmForm.all.new_empreendimentoid, "new_nome", "new_cidadeid");
                        }
                    }

                    if (typeof (result.ReturnValue.Mrv.EmpreendimentoId) != "object") {
                        var lookup = result.ReturnValue.Mrv.EmpreendimentoId.split(";");
                        if (lookup.length == 3) {
                            crmForm.AddValueLookup(crmForm.all.new_empreendimentoid, lookup[0], 10001, lookup[1]);
                            AtribuirFilterLookup(crmForm.all.new_empreendimentoid, crmForm.all.new_blocoid, "new_name", "new_blocoid");
                        }
                    }

                    if (typeof (result.ReturnValue.Mrv.BlocoId) != "object") {
                        var lookup = result.ReturnValue.Mrv.BlocoId.split(";");
                        if (lookup.length == 3) {
                            crmForm.AddValueLookup(crmForm.all.new_blocoid, lookup[0], 10020, lookup[1]);
                            AtribuirFilterLookup(crmForm.all.new_blocoid, crmForm.all.new_produtoid, "name", "new_blocosid");
                        }
                    }
                }
            }
        }
    
        DefinirNomeAbrangencia = function() {
            if (crmForm.all.new_produtoid.DataValue != null)
                return crmForm.all.new_produtoid.DataValue[0].name;

            if (crmForm.all.new_blocoid.DataValue != null)
                return "{0} - {1}".format(
                    crmForm.all.new_empreendimentoid.DataValue == null ? "" : crmForm.all.new_empreendimentoid.DataValue[0].name, 
                    crmForm.all.new_blocoid.DataValue[0].name);

            if (crmForm.all.new_empreendimentoid.DataValue != null)
                return crmForm.all.new_empreendimentoid.DataValue[0].name;

            if (crmForm.all.new_cidadeid.DataValue != null)
                return crmForm.all.new_cidadeid.DataValue[0].name;

            if (crmForm.all.new_regionalid.DataValue != null)
                return "Regional: {0}".format(crmForm.all.new_regionalid.DataValue[0].name);

            if (crmForm.all.new_abrangencianacional != null && crmForm.all.new_abrangencianacional.DataValue)
                return "ABRANGÊNCIA NACIONAL";
        }

        HabilitaOuDesabilitaCamposParaAbrangenciaNacional = function() {
            if (crmForm.all.new_abrangencianacional.DataValue != null) {
                var camposBloqueados = ["new_produtoid", "new_blocoid", "new_empreendimentoid", "new_cidadeid", "new_regionalid"];
                if (crmForm.all.new_abrangencianacional.DataValue == true)
                    crmForm.DesabilitarCampos(camposBloqueados.join(";"), true);
                else 
                    crmForm.DesabilitarCampos(camposBloqueados.join(";"), false);
            }
        }

        AtribuirFilterLookup = function(objectSrc, objectTo, attributeSearch, attributeFilter) {
            if (objectTo.Disabled == true) return;
            
            var filterValue = objectSrc.DataValue != null ? objectSrc.DataValue[0].id : "";
            filterValue = filterValue.substr(1, filterValue.length - 2);

            var typeCode = "";
            var returnCode = 0;
            switch (attributeFilter) {
                case "new_regionalid" : 
                    typeCode = filterValue != "" ? "10031" : "100312"; 
                    returnCode = 10031;
                    break;
                case "new_cidadeid" : 
                    typeCode = filterValue != "" ? "10001" : "100012"; 
                    returnCode = 10001;
                    break;
                case "new_blocoid" : 
                    typeCode = filterValue != "" ? "10020" : "100202"; 
                    returnCode = 10020;
                    break;
                case "new_blocosid" : 
                    typeCode = filterValue != "" ? "1024002" : "10240022"; 
                    returnCode = 1024;
                    break;
            }

            var oParam = "objectTypeCode=" + typeCode + "&filterDefault=false&attributesearch=" + attributeSearch + "&_" + attributeFilter + "=" + filterValue;
            crmForm.FilterLookup(objectTo, returnCode, "/MRVWeb/FilterLookup/FilterLookup.aspx", oParam);
        }
        
        DefinieLookupsPersonalizados = function() {
            AtribuirFilterLookup(crmForm.all.new_regionalid, crmForm.all.new_cidadeid, "new_name", "new_regionalid");
            AtribuirFilterLookup(crmForm.all.new_cidadeid, crmForm.all.new_empreendimentoid, "new_nome", "new_cidadeid");
            AtribuirFilterLookup(crmForm.all.new_empreendimentoid, crmForm.all.new_blocoid, "new_name", "new_blocoid");
            AtribuirFilterLookup(crmForm.all.new_blocoid, crmForm.all.new_produtoid, "name", "new_blocosid");
        }

        //Inicio
        if (crmForm.FormType == TypeCreate && crmForm.all.new_promooid.DataValue == null) {
            formularioValido = false;
            mensagemErro = "Para incluir abrangências deve-se utilizar o botão \"Novo Abrangência da Promoção\" do formulario de abrangência.";
            alert(mensagemErro);
            window.close();
        }
        else {
            ValidarGravacaoAbrangenciaNacional();
            if (crmForm.FormType == TypeUpdate) ValidarAlteracaoAbrangencia();

            if (formularioValido) {
                HabilitaOuDesabilitaCamposParaAbrangenciaNacional();
                DefinieLookupsPersonalizados();
            }
            else {
                var camposBloqueados = ["new_abrangencianacional", "new_produtoid", "new_blocoid", "new_empreendimentoid", "new_cidadeid", "new_regionalid"];
                crmForm.DesabilitarCampos(camposBloqueados.join(";"), true);
                alert(mensagemErro);
            }
        }
    }
}
catch (error) {
    formularioValido = false;
    alert("Ocorreu um erro no formulário.\n" + error.description);
}
