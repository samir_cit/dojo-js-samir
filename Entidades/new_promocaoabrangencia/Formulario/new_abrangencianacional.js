﻿if (crmForm.all.new_abrangencianacional.DataValue == true) {
    if (crmForm.all.new_produtoid.DataValue != null ||
        crmForm.all.new_blocoid.DataValue != null ||
        crmForm.all.new_empreendimentoid.DataValue != null ||
        crmForm.all.new_cidadeid.DataValue != null ||
        crmForm.all.new_regionalid.DataValue != null) {
        alert("Ao gravar esta abrangência os dados de Regional, Cidade, Empreendimento, Bloco e Produto serão apagados.");
    }
}

HabilitaOuDesabilitaCamposParaAbrangenciaNacional();