﻿try {
    if (crmForm.FormType == TypeUpdate &&
       (!crmForm.all.new_produtoid.disabled ||
        !crmForm.all.new_abrangencianacional.disabled)) {
        ValidarAlteracaoAbrangencia();
    }

    if (!formularioValido) {
        alert(mensagemErro);
        event.returnValue = false;
        return false;
    }
    
    if (crmForm.all.new_abrangencianacional.DataValue == true) {
        //Limpa os demais campos quando for selecionado abrangência nacional
        crmForm.all.new_produtoid.DataValue = null;
        crmForm.all.new_blocoid.DataValue = null;
        crmForm.all.new_empreendimentoid.DataValue = null;
        crmForm.all.new_cidadeid.DataValue = null;
        crmForm.all.new_regionalid.DataValue = null;
    }
    else 
    {
        if (crmForm.all.new_produtoid.DataValue == null &&
            crmForm.all.new_blocoid.DataValue == null &&
            crmForm.all.new_empreendimentoid.DataValue == null &&
            crmForm.all.new_cidadeid.DataValue == null &&
            crmForm.all.new_regionalid.DataValue == null) {
            alert("É obrigatório informar pelo menos um dos campos: Regional, Cidade, Empreendimento, Bloco ou Produto.");
            event.returnValue = false;
            return false;
        }

        //Garante a consistência dos dados        
        CompletarDadosAbrangencia();        
    }

    crmForm.all.new_produtoid.ForceSubmit = true;
    crmForm.all.new_blocoid.ForceSubmit = true;
    crmForm.all.new_empreendimentoid.ForceSubmit = true;
    crmForm.all.new_cidadeid.ForceSubmit = true;
    crmForm.all.new_regionalid.ForceSubmit = true;
    crmForm.all.new_abrangencianacional.ForceSubmit = true;

    //Atribui um nome para a abrangência
    crmForm.all.new_name.DataValue = DefinirNomeAbrangencia();
    crmForm.all.new_name.ForceSubmit = true;
} 
catch (error) {
    alert("Ocorreu um erro no formulário.\n" + error.description);
    event.returnValue = false;
    return false;
}
