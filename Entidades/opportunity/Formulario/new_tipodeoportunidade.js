﻿crmForm.ConfigurarPorTipoOportunidade();

crmForm.all.new_produtoid.DataValue = null;
crmForm.all.new_empreendimentodaoportunidadeid.DataValue = null;
crmForm.all.new_cidadeempreendimentooporunidadeid.DataValue = null;
crmForm.all.new_terrenoid.DataValue = null;

crmForm.all.new_produtoid.FireOnChange();
crmForm.all.new_produto_tipo_oportunidade.FireOnChange();

if (crmForm.all.new_tipodeoportunidade.DataValue == TipoDeOportunidade.SERVICO) 
{
    crmForm.SetFieldReqLevel("new_empreendimentodaoportunidadeid", 0);
    crmForm.EscondeCampo("new_produto_tipo_oportunidade", false);
}
else
    crmForm.EscondeCampo("new_produto_tipo_oportunidade", true);

crmForm.AjudarFormularioVendaPermuta();

if (crmForm.all.new_tipodeoportunidade.DataValue != null &&
    crmForm.all.new_tipodeoportunidade.DataValue == TipoDeOportunidade.PERMUTA)
    crmForm.all.customerid.DataValue = null;

if (crmForm.all.new_tipodeoportunidade.DataValue != TipoDeOportunidade.UNIDADE)
        crmForm.EscondeCampo("new_tipo_pne", false);
    else
        crmForm.EscondeCampo("new_tipo_pne", true);
