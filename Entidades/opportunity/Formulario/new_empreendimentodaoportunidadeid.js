﻿if (crmForm.all.new_produtoid.DataValue != null) {
    crmForm.all.new_produtoid.DataValue = null;
}

if (crmForm.all.new_tipodeplanodefinanciamentoopid.DataValue != null) {
    crmForm.all.new_tipodeplanodefinanciamentoopid.DataValue = null;
}

if (crmForm.all.new_tipodeoportunidade.DataValue != null &&
    crmForm.all.new_tipodeoportunidade.DataValue == TipoDeOportunidade.PERMUTA) {
    // Limpa o campo de cliente
    crmForm.all.customerid.DataValue = null;
}

/// Chama o onchange do produto para montar o filterlookup
crmForm.all.new_produtoid.FireOnChange();
crmForm.AjudarFormularioVendaPermuta();