﻿if (crmForm.all.new_tipodeoportunidade.DataValue == TipoDeOportunidade.UNIDADE) 
{
    var lookupData = new Array();
    var lookupItem = new Object();
    lookupItem.typename = 'PriceLevel';
    lookupItem.name = 'Lista Padrão';
    lookupData[0] = lookupItem;
    crmForm.all.pricelevelid.DataValue = lookupData;
    crmForm.all.pricelevelid.Disabled = true;

    crmForm.all.new_tipodeplanodefinanciamentoopid_c.style.visibility = "visible";
    crmForm.all.new_tipodeplanodefinanciamentoopid_d.style.visibility = "visible";
    crmForm.all.new_tipodeplanodefinanciamentoopid.setAttribute("req", 2);
    crmForm.all.new_tipodeplanodefinanciamentoopid_c.className = "req";
}
else 
{
    crmForm.all.pricelevelid.Disabled = false;
    crmForm.all.new_tipodeplanodefinanciamentoopid.DataValue = null;
    crmForm.all.new_tipodeplanodefinanciamentoopid.setAttribute("req", 0);
    crmForm.all.new_tipodeplanodefinanciamentoopid_c.className = "n";
    crmForm.all.new_tipodeplanodefinanciamentoopid_c.style.visibility = "hidden";
    crmForm.all.new_tipodeplanodefinanciamentoopid_d.style.visibility = "hidden";
}