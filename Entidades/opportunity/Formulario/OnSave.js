﻿try {
    if (!formularioValido) {
        alert("Ocorreu um erro no formulário. Impossível salvar.");
        event.returnValue = false;
        return false;
    }

    StatusCode =
    {
        EMANDAMENTO: "1",
        SUSPENSO: "2",
        GANHA: "3",
        CANCELADA: "4",
        DISTRATO: "5",
        QUEDAEMANDAMENTO: "200000",
        PREGANHA: "200001"
    }
    
    /// Perdendo a oportunidade e Cancelar as propostas desta oportunidade.
    if (event.Mode == 5) {
        /// Variável que simula o 'statuscode' da entidade
        novoStatus = document.getElementById("crNewStatus").value;

        if (novoStatus == StatusCode.GANHA) {
            if (crmForm.all.new_venda_garantida.DataValue && crmForm.all.statuscode.DataValue != StatusCode.PREGANHA)
            {
                alert("Oportunidade Venda Garantida. Favor realizar primeiro o processo do pré ganho.");
                event.returnValue = false;
                return false;
            }
            try {
                var tipoOportunidade = crmForm.all.new_tipodeoportunidade.DataValue;
                var cmd = new RemoteCommand("MrvService", "GanharOportunidade", "/MRVCustomizations/");
                cmd.SetParameter("opportunityId", crmForm.ObjectId);
                cmd.SetParameter("tipoOportunidade", tipoOportunidade);

                var result = cmd.Execute();

                if (result.Success) {
                    if (result.ReturnValue.Mrv.Error) {
                        if (result.ReturnValue.Mrv.Error.indexOf("NetTerm") == -1) {
                            alert("Envio de Contrato: " + result.ReturnValue.Mrv.Error);
                            event.returnValue = false;
                            return false;   
                        }
                    } else if (result.ReturnValue.Mrv.ErrorSoap) {
                        alert("Envio de Contrato: " + result.ReturnValue.Mrv.ErrorSoap);
                        event.returnValue = false;
                        return false;
                    } else {
                        var cmdTaxa = new RemoteCommand("MrvService", "CriarAditivoTaxa", "/MRVCustomizations/");
                        cmdTaxa.SetParameter("opportunityId", crmForm.ObjectId);
                        cmdTaxa.SetParameter("tipoOportunidade", tipoOportunidade);

                        var resultTaxa = cmdTaxa.Execute();
                        crmForm.TratarRetornoRemoteCommand(resultTaxa, "CriarAditivoTaxa");
                    }
                }
                else {
                    alert("Erro na comunicação: Envio de Contrato para SAP. Contate o administrador do sistema");
                    event.returnValue = false;
                    return false;                  
                }

                /// Colocar Data de Fechamento no estimatedclosedate (Data usada no relatório)
                crmForm.all.estimatedclosedate.DataValue = new Date();
            }
            catch (err) {
                alert(err.description);
                event.returnValue = false;
                return false;    
            }
        }
        else if (novoStatus != StatusCode.GANHA && crmForm.PerdePropostaExpiraContrato(crmForm.ObjectId)) {
            event.returnValue = false;
            return false;
        }
    }
    /// 22: Nova Proposta
    else if (event.Mode == 22) {
        if (crmForm.IsDirty) {
            alert("Salve antes de criar a proposta.");
            event.returnValue = false;
            return false;
        }

        if (crmForm.ValidarOportunidade()) {
            crmForm.all.new_flag_proposta.DataValue = true;

            /// Abre a Janela da Proposta.
            locAddRelatedToNonForm(1084, 3, crmForm.ObjectId, '');
        }
        event.returnValue = false;
        return false;
    }

    if (crmForm.FormType != TypeCreate) {
        crmForm.ValidarSeExisteMaisDeUmProduto();
    }

    /// Validando se já existe uma oportunidade com mesmo cliente e produto
    if (crmForm.FormType == TypeCreate) {
        if (crmForm.getUrlParam("_CreateFromType") == "1024") {
            crmForm.ValidarIlegalidade();
            event.returnValue = false;
            return false;
        }

        if (crmForm.all.new_tipodeoportunidade.DataValue == TipoDeOportunidade.UNIDADE ||
            crmForm.all.new_tipodeoportunidade.DataValue == TipoDeOportunidade.LOTE) {
            if (crmForm.ExisteDadosRepetidos("")) {
                alert("Já existe uma oportunidade igual a essa.");
                event.returnValue = false;
                return false;
            }
        }
        else if (crmForm.all.new_tipodeoportunidade.DataValue == TipoDeOportunidade.SERVICO) {
            if (crmForm.ExisteDadosRepetidosParaVendaServicos("")) {
                alert("Já existe uma oportunidade de venda de serviço igual a essa.");
                event.returnValue = false;
                return false;
            }
        }
    }
    else if (crmForm.FormType == TypeUpdate && event.Mode != 5) {
        if (crmForm.all.new_tipodeoportunidade.DataValue == TipoDeOportunidade.UNIDADE ||
            crmForm.all.new_tipodeoportunidade.DataValue == TipoDeOportunidade.LOTE) {
            if (crmForm.ExisteDadosRepetidos(crmForm.ObjectId)) {
                alert("Já existe uma oportunidade igual a essa.");
                event.returnValue = false;
                return false;
            }
        }
        else if (crmForm.all.new_tipodeoportunidade.DataValue == TipoDeOportunidade.SERVICO) {
            if (crmForm.ExisteDadosRepetidosParaVendaServicos(crmForm.ObjectId)) {
                alert("Já existe uma oportunidade de venda de serviço igual a essa.");
                event.returnValue = false;
                return false;
            }
        }
    }

    if (crmForm.all.new_tipodeoportunidade.DataValue != TipoDeOportunidade.KITACABEMNTO &&
        crmForm.all.new_tipodeoportunidade.DataValue != TipoDeOportunidade.SERVICO &&
        event.Mode != 6 &&
        event.Mode != 5) {
        if (crmForm.ValidarSeExisteOportunidadeGanhaParaEsteProduto()) {
            event.returnValue = false;
            return false;
        }
    }

    crmForm.all.new_incrementa_reserva.Disabled = false;
    crmForm.all.name.Disabled = false;
    crmForm.all.new_coddocontratosap.Disabled = false;
    crmForm.all.new_codigocontratosap6.Disabled = false;
    crmForm.all.pricelevelid.Disabled = false;
    crmForm.all.new_tipodeoportunidade.Disabled = false;
    crmForm.all.new_observacoesbacen.Disabled = false;
    crmForm.all.new_observacoesrendainformal.Disabled = false;
    crmForm.all.new_observacoesrendainformal2.Disabled = false;
    crmForm.all.new_observacocompromissosfinanceiros.Disabled = false;
    crmForm.all.new_opcao_adesao_confirmada.Disabled = false;
    crmForm.all.new_adesao_boleto_digital.Disabled = false;    

    crmForm.TransformaTodosCaracteresEmMaiusculo();
}
catch (erro) {
    alert("Ocorreu um erro no formulário." + erro.description);
    event.returnValue = false;
    return false;
}