﻿formularioValido = true;
try {
    function loadScript() {
        var oScript = document.createElement('script');
        oScript.type = "text/javascript";
        oScript.src = "/_static/Base.js";

        var oHead = document.getElementsByTagName('head')[0];
        oHead.appendChild(oScript);

        oScript.onreadystatechange = function() {
            if (this.readyState == 'complete' || this.readyState == 'loaded')
                Load();
        }
    }

    loadScript();

    function Load() {

        RazaoStatus =
        {
            EM_ANDAMENTO: 1,
            SUSPENSO: 2,
            QUEDA_EM_ANDAMENTO: 200000,
            PRE_GANHO: 200001
        }

        TipoVenda =
        {
            VENDA_UNIDADE: 1,
            VENDA_GARAGEM: 2,
            VENDA_BOX: 3,
            VENDA_KIT_ACABAMENTO: 4,
            VENDA_SERVICO: 5,
            VENDA_LOTE: 6,
            VENDA_PERMUTA: 7,
            VENDA_LOJA: 8
        }

        TipoAuditoria =
        {
            PROPOSTA: 1,
            OPORTUNIDADE_RENEGOCIACAO: 2,
            PROPOSTA_CSG: 3,
            EMPREENDIMENTO: 4,
            RETIFICACAO_RI: 5,
            OPORTUNIDADE: 6
        }

        Mensagem =
        {
            EMPREENDIMENTO_OBRIGATORIO: 'Empreendimento obrigatório.',
            CLIENTE_OBRIGATORIO: 'Cliente obrigatório.',
            GERANDO_CONTRATO: 'Gerando Contrato. Processo demorado. Aguarde.',
            SALVE_ANTES: 'Salve antes de gerar o contrato.',
            OCORREU_SEGUINTE_ERRO: 'Ocorreu o seguinte erro:\n',
            FALHA_ENVIO: 'Falha no envio das informações. Contate o administrador do sistema.',
            ERRO_FORMULARIO: "Ocorreu um erro no formulário.\n"
        }

        /// Validar se a Oportunidade possui alguma ilegalidade antes de ser salva.
        crmForm.ValidarIlegalidade = function() {
            var rCmd = new RemoteCommand("MrvService", "OportunidadeIlegal", "/MrvCustomizations/");
            rCmd.SetParameter("CorretorId", crmForm.all.ownerid.DataValue[0].id);
            rCmd.SetParameter("ProdutoId", crmForm.all.new_produtoid.DataValue[0].id);

            var retorno = rCmd.Execute();

            if (crmForm.TratarRetornoRemoteCommand(retorno, "OportunidadeIlegal"))
                alert(retorno.ReturnValue.Mrv.Mensagem);
        }

        /// Verifica se existe oportunidade ganha para este produto e não deixa salvar.
        crmForm.ValidarSeExisteOportunidadeGanhaParaEsteProduto = function() {
            var rCmd = new RemoteCommand("MrvService", "ValidarSeExisteOportunidadeFechadaParaEsteProduto", "/MRVCustomizations/");
            rCmd.SetParameter("produtoId", crmForm.all.new_produtoid.DataValue[0].id);
            if (crmForm.ObjectId != null)
                rCmd.SetParameter('opportunityId', crmForm.ObjectId);
            var retorno = rCmd.Execute();

            if (crmForm.TratarRetornoRemoteCommand(retorno, "ValidarSeExisteOportunidadeFechadaParaEsteProduto")) {
                if (retorno.ReturnValue.Mrv.Existe) {
                    alert("Uma oportunidade foi Ganha com o produto desta oportunidade.\nImpossível salvar!");
                    return true;
                } else
                    return false;
            } else
                return true;
        }

        /// Valida se existe mais de um produto na oportunidade com tipo unidade.
        crmForm.ValidarSeExisteMaisDeUmProduto = function() {
            var rCmd = new RemoteCommand('MrvService', 'ValidarSeExisteMaisDeUmProduto', '/MRVCustomizations/');
            rCmd.SetParameter('oportunidadeId', crmForm.ObjectId);

            var retorno = rCmd.Execute();

            if (crmForm.TratarRetornoRemoteCommand(retorno, "ValidarSeExisteMaisDeUmProduto")) {
                if (retorno.ReturnValue.Mrv.Existe) {
                    alert('Oportunidade tem dois produtos do tipo Unidade cadastrados.');
                    return true;
                } else
                    return false;
            } else
                return true;
        }

        /// Verifica se a Oportunidade está validada para ser salva.
        crmForm.ValidarOportunidade = function() {
            var rCmd = new RemoteCommand('MrvService', 'OportunidadeValida', '/MRVCustomizations/');
            rCmd.SetParameter('oportunidadeId', crmForm.ObjectId);

            var retorno = rCmd.Execute();

            return crmForm.TratarRetornoRemoteCommand(retorno, "OportunidadeValida");
        }

        /// Função que garante que existe apenas uma oportunidade com as mesmas informações.
        crmForm.ExisteDadosRepetidos = function(Identificador) {
            var rCmd = new RemoteCommand("MrvService", "ValidarDadosRepetidos", "/MRVCustomizations/");
            rCmd.SetParameter("produtoId", crmForm.all.new_produtoid.DataValue[0].id);
            rCmd.SetParameter("clienteId", crmForm.all.customerid.DataValue[0].id);
            rCmd.SetParameter("oportunidadeId", Identificador.toString().toUpperCase());

            var retorno = rCmd.Execute();

            if (crmForm.TratarRetornoRemoteCommand(retorno, "ValidarDadosRepetidos"))
                return retorno.ReturnValue.Mrv.Existe;
            else
                return false;
        }

        /// Função que garante que existe apenas uma oportunidade com as mesmas informações na venda de serviços.
        crmForm.ExisteDadosRepetidosParaVendaServicos = function(identificador) {
            var rCmd = new RemoteCommand("MrvService", "ValidarDadosRepetidosParaVendaServicos", "/MRVCustomizations/");
            rCmd.SetParameter("servicoProdutoId", crmForm.all.new_produto_tipo_oportunidade.DataValue[0].id);
            rCmd.SetParameter("clienteId", crmForm.all.customerid.DataValue[0].id);
            rCmd.SetParameter("produtoId", crmForm.all.new_produtoid.DataValue[0].id);
            rCmd.SetParameter("usuarioId", crmForm.all.ownerid.DataValue[0].id);
            rCmd.SetParameter("oportunidadeId", identificador);

            var retorno = rCmd.Execute();

            if (crmForm.TratarRetornoRemoteCommand(retorno, "ValidarDadosRepetidosParaVendaServicos"))
                return retorno.ReturnValue.Mrv.Existe;
            else
                return false;
        }

        crmForm.PerdePropostaExpiraContrato = function(oportunidadeId) {
            var rCmd = new RemoteCommand("MrvService", "PerdePropostaExpiraContrato", "/MRVCustomizations/");
            rCmd.SetParameter("oportunidadeId", oportunidadeId);
            var retorno = rCmd.Execute();

            return !crmForm.TratarRetornoRemoteCommand(retorno, "PerdePropostaExpiraContrato");
        }

        /// Configurar Título de acordo com o campo cliente
        crmForm.ConfigurarTitulo = function() {
            if (crmForm.all.customerid.DataValue != null) {
                if (crmForm.all.customerid.DataValue[0].typename == "account")
                    crmForm.all.name.DataValue = 'OPORTUNIDADE DE VENDA - ' + crmForm.all.customerid.DataValue[0].name;
                else {
                    alert("Tipo de cliente inválido.");
                    crmForm.all.customerid.DataValue = null;
                }
            } else
                crmForm.all.name.DataValue = "OPORTUNIDADE DE VENDA -";
        }

        crmForm.ConfigurarOnLoad = function() {
            crmForm.ConfigurarTitulo();

            /// Ocultar a Tab Kits.
            crmForm.all.tab5Tab.style.display = "none";

            crmForm.EscondeCampo("new_produto_tipo_oportunidade", true);
            crmForm.EscondeCampo("new_flag_proposta", true);
            crmForm.EscondeCampo("isrevenuesystemcalculated", true);

            crmForm.all.pricelevelid.SetFocus();

            crmForm.all.new_incrementa_reserva.Disabled = false;
            crmForm.all.name.Disabled = true;
            crmForm.all.new_coddocontratosap.Disabled = true;
            crmForm.all.new_possui_proposta.Disabled = true;

            crmForm.all.new_cidadeempreendimentooporunidadeid.FireOnChange();
            crmForm.all.new_produtoid.FireOnChange();
            crmForm.all.new_tipodeplanodefinanciamentoopid.FireOnChange();
            crmForm.all.customerid.FireOnChange();

            /// Ocultar a funcao de relacionamentos do botão mais ações.
            var relacionamentosCli = document.getElementById("_MIlocAddRelatedToNonForm45033crmFormSubmitcrmFormSubmitIdvalue");
            if (relacionamentosCli != null)
                relacionamentosCli.style.display = "none";

            /// Ocultar a funcao de relacionamentos da op devido a nova entidade ter sido criada
            relacionamentosCli = document.getElementById("navRelationship");
            if (relacionamentosCli != null)
                relacionamentosCli.style.display = "none";

            //DEF 486377 - RN 02 - Quando oportunidade não for do tipo de kit
            if (crmForm.all.new_tipodeoportunidade.DataValue == TipoDeOportunidade.KITACABEMNTO)
                crmForm.EscondeCampo("new_inicio_vigencia_kit", false);

            if (crmForm.all.new_tipodeoportunidade.DataValue != TipoDeOportunidade.UNIDADE)
                crmForm.EscondeCampo("new_tipo_pne", true);
        }

        crmForm.PreencheEmpreendimentoCidade = function() {
            if (crmForm.all.new_tipodeoportunidade.DataValue == TipoDeOportunidade.PERMUTA
                && crmForm.new_produtoid.DataValue != null
                && crmForm.all.new_empreendimentodaoportunidadeid.DataValue == null
                && crmForm.all.new_cidadeempreendimentooporunidadeid.DataValue == null) {

                var cmd = new RemoteCommand("MrvService", "ObterEmpreendimentoPorProduto", "/MRVCustomizations/");
                cmd.SetParameter("produtoId", crmForm.new_produtoid.DataValue[0].id);
                var result = cmd.Execute();
                if (crmForm.TratarRetornoRemoteCommand(result, "ObterEmpreendimentoPorProduto")) {
                    if (result.ReturnValue.Mrv.EmpreendimentoId != '') {
                        crmForm.AddValueLookup(crmForm.all.new_empreendimentodaoportunidadeid, '{' + result.ReturnValue.Mrv.EmpreendimentoId + '}', '10001', '');
                    }
                    if (result.ReturnValue.Mrv.CidadeId != '') {
                        crmForm.AddValueLookup(crmForm.all.new_cidadeempreendimentooporunidadeid, '{' + result.ReturnValue.Mrv.CidadeId + '}', '10031', '');
                    }
                } else
                    alert("Falha ao obter o Empreendimento e/ou a Cidade deste produto. Tente novamente ou contate o administrador!");
            }
        }
        /// Valida se o empreendimento do produto selecionado na oportunidade de kit participa do processo kit na obra
        crmForm.ValidarEmpreendimentoKitObra = function() {

            if (crmForm.all.new_tipodeoportunidade.DataValue == TipoDeOportunidade.KITACABEMNTO &&
                crmForm.all.new_produtoid.DataValue != null) {
                var cmd = new RemoteCommand("MrvService", "ValidarEmpreendimentoKitObra", "/MRVCustomizations/");
                cmd.SetParameter("produtoId", crmForm.new_produtoid.DataValue[0].id);
                var result = cmd.Execute();
                if (crmForm.TratarRetornoRemoteCommand(result, "ValidarEmpreendimentoKitObra")) {
                    if (result.ReturnValue.Mrv.Resultado) {
                        alert("O Empreendimento desta unidade participa do projeto Kit na Obra, e não podem ser comercializados Kits avulsos.");
                        crmForm.new_produtoid.DataValue = null;
                    }
                } else {
                    alert("Falha ao verificar se o empreendimento do produto selecionado participa do processo Kit na Obra. Tente novamente ou contate o administrador!");
                    crmForm.new_produtoid.DataValue = null;
                }
            }
        }

        crmForm.ConfigurarOnLoad();

        /// Função que preenche o preço do produto no campo Valor Imovel
        crmForm.PreenchePreco = function() {
            if (crmForm.all.new_tipodeoportunidade.DataValue == TipoDeOportunidade.UNIDADE &&
                crmForm.new_produtoid.DataValue != null) {
                /// Permitir alterar o status da unidade, apenas os membros da equipe administradores
                var cmd = new RemoteCommand("SimuladorVendaService", "ObtemPreco", "/MRVCustomizations/SimuladorVenda/");

                if (crmForm.ObjectId != null) {
                    cmd.SetParameter("idProduto", crmForm.new_produtoid.DataValue[0].id);
                    cmd.SetParameter("idLoja", GuidVazio);

                    var result = cmd.Execute();

                    if (!result.ReturnValue.Mrv.Error && !result.ReturnValue.Mrv.ErrorSoap) {
                        if (!result.ReturnValue.Mrv.NaoEncontrou)
                            crmForm.all.estimatedvalue.DataValue = parseFloat(result.ReturnValue.Mrv.ValorImovel);
                    } else
                        alert("Falha ao obter o preço deste produto.Tente novamente ou contate o administrador!");
                }
            }
            return;
        }

        /// Método que configura (oculta/mostra) campos relacionados ao tipo UNIDADE
        crmForm.ConfigurarCamposOportunidadeTipoUnidade = function(ocultarCampo) {
            crmForm.EscondeCampo("new_empreendimentodaoportunidadeid", ocultarCampo);
            crmForm.EscondeCampo("new_cidadeempreendimentooporunidadeid", ocultarCampo);
            crmForm.EscondeCampo("new_tipodeplanodefinanciamentoopid", ocultarCampo);
            crmForm.EscondeCampo("new_produto_tipo_oportunidade", true);
            crmForm.EscondeCampo("opportunityratingcode", ocultarCampo);
            crmForm.EscondeCampo("estimatedvalue", ocultarCampo);
            crmForm.EscondeCampo("new_valor_total_assessoria", ocultarCampo);
            crmForm.EscondeCampo("new_terrenoid", true);


            crmForm.SetFieldReqLevel('new_empreendimentodaoportunidadeid', !ocultarCampo);
            crmForm.SetFieldReqLevel('new_tipodeplanodefinanciamentoopid', !ocultarCampo);
            crmForm.SetFieldReqLevel('new_terrenoid', false);


            /// Ocultar a Tab Kits.
            crmForm.all.tab5Tab.style.display = "none";
        }
        crmForm.ConfigurarCamposOportunidadeTipoUnidade(true);

        crmForm.ConfigurarCamposOportunidadeTipoPermuta = function() {
            crmForm.EscondeCampo("new_empreendimentodaoportunidadeid", true);
            crmForm.EscondeCampo("new_cidadeempreendimentooporunidadeid", true);
            crmForm.EscondeCampo("new_tipodeplanodefinanciamentoopid", true);
            crmForm.EscondeCampo("new_produto_tipo_oportunidade", true);
            crmForm.EscondeCampo("opportunityratingcode", true);
            crmForm.EscondeCampo("estimatedvalue", true);
            crmForm.EscondeCampo("new_valor_total_assessoria", true);
            crmForm.EscondeCampo("new_terrenoid", false);

            crmForm.SetFieldReqLevel('new_empreendimentodaoportunidadeid', false);
            crmForm.SetFieldReqLevel('new_cidadeempreendimentooporunidadeid', false);
            crmForm.SetFieldReqLevel('new_tipodeplanodefinanciamentoopid', false);
            crmForm.SetFieldReqLevel('new_terrenoid', true);

            /// Ocultar a Tab Kits.
            crmForm.all.tab5Tab.style.display = "none";
        }

        crmForm.ConfigurarCamposOportunidadeTipoKit = function() {
            crmForm.EscondeCampo("new_empreendimentodaoportunidadeid", true);
            crmForm.EscondeCampo("new_cidadeempreendimentooporunidadeid", true);
            crmForm.EscondeCampo("new_tipodeplanodefinanciamentoopid", true);
            crmForm.EscondeCampo("new_produto_tipo_oportunidade", true);
            crmForm.EscondeCampo("opportunityratingcode", true);
            crmForm.EscondeCampo("estimatedvalue", true);
            crmForm.EscondeCampo("new_valor_total_assessoria", true);
            crmForm.EscondeCampo("new_terrenoid", true);

            crmForm.SetFieldReqLevel('new_empreendimentodaoportunidadeid', false);
            crmForm.SetFieldReqLevel('new_tipodeplanodefinanciamentoopid', false);
            crmForm.SetFieldReqLevel('new_terrenoid', false);

            /// Mostrar a Tab Kits.
            crmForm.all.tab5Tab.style.display = "block";
        }

        crmForm.ConfigurarCamposOportunidadeTipoServico = function() {
            crmForm.EscondeCampo("new_empreendimentodaoportunidadeid", true);
            crmForm.EscondeCampo("new_cidadeempreendimentooporunidadeid", true);
            crmForm.EscondeCampo("new_tipodeplanodefinanciamentoopid", true);
            crmForm.EscondeCampo("new_produto_tipo_oportunidade", false);
            crmForm.EscondeCampo("opportunityratingcode", true);
            crmForm.EscondeCampo("estimatedvalue", true);
            crmForm.EscondeCampo("new_valor_total_assessoria", true);
            crmForm.EscondeCampo("new_terrenoid", true);

            crmForm.SetFieldReqLevel("new_empreendimentodaoportunidadeid", false);
            crmForm.SetFieldReqLevel('new_tipodeplanodefinanciamentoopid', false);
            crmForm.SetFieldReqLevel('new_terrenoid', false);

            crmForm.all.new_produto_tipo_oportunidade.FireOnChange();

            /// Ocultar a Tab Kits.
            crmForm.all.tab5Tab.style.display = "none";
        }

        /// Configurar Formulário de acordo com o tipo de oportunidade
        crmForm.ConfigurarPorTipoOportunidade = function() {
            switch (crmForm.all.new_tipodeoportunidade.DataValue) {
                case TipoDeOportunidade.UNIDADE:
                case TipoDeOportunidade.BOX:
                case TipoDeOportunidade.GARAGEM:
                case TipoDeOportunidade.LOTE:
                case TipoDeOportunidade.LOJA:
                    crmForm.ConfigurarCamposOportunidadeTipoUnidade(false);
                    break;

                case TipoDeOportunidade.KITACABEMNTO:
                    crmForm.ConfigurarCamposOportunidadeTipoKit();
                    break;

                case TipoDeOportunidade.PERMUTA:
                    crmForm.ConfigurarCamposOportunidadeTipoPermuta();
                    break;

                case TipoDeOportunidade.SERVICO:
                    crmForm.ConfigurarCamposOportunidadeTipoServico();
                    break;

                default:
                    break;
            }
        }
        crmForm.ConfigurarPorTipoOportunidade();

        /// Configura os campos Observações
        crmForm.PreencherCamposObservacoes = function() {
            if (crmForm.FormType == TypeCreate) {
                crmForm.all.new_observacoesbacen.DataValue = "  Perguntar ao cliente se possui ou se já possuiu alguma dívida baixada em prejuízo.Será necessario informar o nome do Banco. ";
                crmForm.all.new_observacoesrendainformal.DataValue = "  Serão considerados os depósitos feitos no decorrer do mês. A movimentação deverá ser constante nos últimos 6 (seis) meses, sem a utilização do cheque especial. Não serão considerados depósitos que entram e saem da conta sem manter saldo em conta. ";
                crmForm.all.new_observacoesrendainformal2.DataValue = "  Serão considerados como renda os valores emitidos em notas fiscais, declaração de empresa e gastos com o cartão de crédito. ";
                crmForm.all.new_observacocompromissosfinanceiros.DataValue = "  O valor do aluguel não será utilizado para o comprometimento da renda.";
            }
        }
        crmForm.PreencherCamposObservacoes();

        /// Desabilitando campos na Guia Informações para a Análise
        crmForm.DesabilitarCamposGuiaInformacoesAnalise = function() {
            crmForm.all.new_possuiregistronocartrioderegistr.Disabled = true;
            crmForm.all.new_valordeclaradoimpostoderenda.readOnly = true;
            crmForm.all.new_numerodedependentesdeclarados.readOnly = true;
            crmForm.all.new_valortotal.readOnly = true;
            crmForm.all.new_crditoeducacionalparcelaematraso.Disabled = true;
            crmForm.all.new_numerodecheques.readOnly = true;
            crmForm.all.new_valortotaldecheques.readOnly = true;
            crmForm.all.new_situacao.Disabled = true;
            crmForm.all.new_financvalorprestacoes.readOnly = true;
            crmForm.all.new_totaldeprestaesavencer.readOnly = true;
            crmForm.all.new_qtdeparcelasavencerconsorcio.readOnly = true;
            crmForm.all.new_valormensalconsorcio.readOnly = true;
            crmForm.all.new_valormensalfinanciamento.readOnly = true;
            crmForm.all.new_qtdeparcelasavencerfinanciamento.readOnly = true;
            crmForm.all.new_valormensalemprestimobanco.readOnly = true;
            crmForm.all.new_qtdeparcelasavenceremprstimobanc.readOnly = true;
            crmForm.all.new_valormensalemprstimocontracheque.readOnly = true;
            crmForm.all.new_qtdeparcelasavenceremprstimocont.readOnly = true;
        }
        crmForm.DesabilitarCamposGuiaInformacoesAnalise();

        /// Usuário Adm
        Administrador = null;
        crmForm.UsuarioAdministrador = function() {
            if (Administrador == null)
                Administrador = crmForm.UsuarioPertenceEquipe("Administradores");

            return Administrador;
        }

        /// Adiciona Script para ser usado no click do botão
        var elm = document.createElement("script");
        elm.src = "/_static/_grid/cmds/util.js";
        document.appendChild(elm);

        /// Reabrir Oportunidade
        if (crmForm.FormType == TypeDisabled) {
            mnuReabrir = document.getElementById("_MIreactivate");

            if (mnuReabrir)
                mnuReabrir.style.display = "none";
        }

        if (crmForm.FormType == TypeCreate) {
            var lista = new Array();
            var item = new LookupControlItem("804307A6-3FCB-DC11-9ED6-001B7845B4A6", 1022, "Lista Padrão");
            lista[0] = item;
            crmForm.all.pricelevelid.DataValue = lista;
        }

        if (crmForm.FormType != TypeCreate)
            crmForm.all.new_tipodeoportunidade.Disabled = true;

        if (crmForm.FormType != TypeUpdate) {
            crmForm.RemoveBotao("GerarContrato");
            crmForm.RemoveBotao("AtivarContrato");
            crmForm.RemoveBotao("ValidarPrativaodoContrato");
            crmForm.RemoveBotao("AlterarDataBase");
        }

        //Remove o botao de reabrir proposta de kit se não é kit
        if (crmForm.FormType != TypeUpdate || crmForm.all.new_tipodeoportunidade.DataValue != TipoDeOportunidade.KITACABEMNTO)
            crmForm.RemoveBotao("ReabrirPropostadeKit");

        //Remove botão Queda Oportunidade se não é uma venda de unidade
        if (crmForm.FormType != TypeUpdate || crmForm.all.new_tipodeoportunidade.DataValue != TipoDeOportunidade.UNIDADE)
            crmForm.RemoveBotao("QuedaOportunidade");

        /// Flag para não acontecer o bug de quando apertar F5 criar uma nova proposta
        if (crmForm.all.new_flag_proposta.DataValue != null &&
            crmForm.all.new_flag_proposta.DataValue == true) {
            crmForm.all.new_flag_proposta.DataValue = false;
            crmForm.Save();
        }

        /// Verifica se já existe outra oportunidade fechada com o mesmo produto
        if (crmForm.FormType == TypeUpdate) {
            if (crmForm.all.new_tipodeoportunidade.DataValue != TipoDeOportunidade.KITACABEMNTO &&
                crmForm.all.new_tipodeoportunidade.DataValue != TipoDeOportunidade.SERVICO &&
                crmForm.all.new_produtoid.DataValue != null) {
                var cmd = new RemoteCommand("MrvService", "ValidarSeExisteOportunidadeFechadaParaEsteProduto", "/MRVCustomizations/");
                cmd.SetParameter("produtoId", crmForm.all.new_produtoid.DataValue[0].id);
                cmd.SetParameter("opportunityId", crmForm.ObjectId);
                var result = cmd.Execute();
                if (result.Success && result.ReturnValue.Mrv.Existe)
                    alert("Uma oportunidade foi Ganha com o produto desta oportunidade.\nA oportunidade atual não poderá ser mais ganha!");
            }

            /// Verifica se existe proposta criada para a oportunidade se existir bloqueia o campo produto/empreendimento/cidade
            ApenasUmaProposta = null;
            crmForm.ApenasUmaProposta = function() {
                if (ApenasUmaProposta != null)
                    return ApenasUmaProposta;

                var rCmd = new RemoteCommand("MrvService", "JustAQuote", "/MRVCustomizations/");
                rCmd.SetParameter("OpportunityId", crmForm.ObjectId);

                retorno = rCmd.Execute();

                if (retorno.Success)
                    ApenasUmaProposta = retorno.ReturnValue.Mrv.Exist;

                return ApenasUmaProposta;
            }

            if (crmForm.ApenasUmaProposta()) {
                crmForm.all.new_produtoid.Disabled = true;
                crmForm.all.new_empreendimentodaoportunidadeid.Disabled = true;
                crmForm.all.new_cidadeempreendimentooporunidadeid.Disabled = true;
                crmForm.all.new_tipodeplanodefinanciamentoopid.Disabled = true;
                crmForm.all.customerid.Disabled = true;
            } else {
                crmForm.RemoveBotao("AlterarDataBase");
            }
        }

        if (crmForm.all.new_codsapcliente.DataValue == null &&
            crmForm.all.new_codigoclientesap6.DataValue == null) {
            crmForm.all.tab4Tab.style.display = "none";
            crmForm.RemoveBotao("AtivarContrato");
            crmForm.RemoveBotao("ValidarPrativaodoContrato");
            crmForm.RemoveBotao("AtivarContratocomData");
        }
        
        crmForm.RemoverMenuValidarPreAtivacaoContrato = function() {
			if (crmForm.FormType == TypeUpdate)
			{
				var cmd = new RemoteCommand("MrvService", "ValidarContratoPreAtivoAtivo", "/MRVCustomizations/");
				cmd.SetParameter("oportunidadeId", crmForm.ObjectId);
				var resultado = cmd.Execute();
				if (crmForm.TratarRetornoRemoteCommand(resultado, "VerificaContratoPreAtivoAtivo")) {
					var retorno = resultado.ReturnValue.Mrv.ContratoPreAtivoAtivo;
					if (retorno) {
						crmForm.RemoveBotao("ValidarPrativaodoContrato");
					}
				}
			}
        }
        crmForm.RemoverMenuValidarPreAtivacaoContrato();

        /// Habilitar campos apenas para os membros da equipe Administradores
        if (crmForm.UsuarioAdministrador()) {
            var campos = "new_coddocontratosap;new_codsapcliente;new_deposito_identificado;new_codigocontratosap6;new_codigoclientesap6;new_deposito_identificado_sap6;new_codigopagadorsap6;new_aditivocasagarantia;new_aditivotaxadespachante;new_aditivosegurobras;new_aditivoreembclientecartorios;new_aditivoreembcliente;new_tipodeplanodefinanciamentoopid;new_produtoid;new_empreendimentodaoportunidadeid;new_cidadeempreendimentooporunidadeid;new_tipodeplanodefinanciamentoopid;customerid";
            crmForm.DesabilitarCampos(campos, false);
        } else
            crmForm.all.tab6Tab.style.display = "none";

        crmForm.getUrlParam = function(name) {
            name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
            var regexS = "[\\?&]" + name + "=([^&#]*)";
            var regex = new RegExp(regexS);
            var results = regex.exec(window.location.href);

            return results == null
                ? ""
                : results[1];
        }

        if (crmForm.getUrlParam("_CreateFromType") == "1024" &&
            crmForm.FormType == TypeCreate) {
            alert("Operação ilegal. Não prossiga com está oportunidade. \nA criação de oportunidades não deve ser feita pelo produto.");
            crmForm.all.ownerid.Disabled = true;
            window.close();
        }

        crmForm.BloquearCampoDataContratoBancario = function() {
            if (crmForm.FormType == TypeCreate || crmForm.FormType == TypeUpdate) {
                if (crmForm.UsuarioPertenceEquipe(Equipe.CR_AUDITORIACONTBANCO) && crmForm.all.new_venda_garantida.DataValue &&
                (crmForm.all.statuscode.DataValue == RazaoStatus.EM_ANDAMENTO || crmForm.all.statuscode.DataValue == RazaoStatus.QUEDA_EM_ANDAMENTO)) {
                    crmForm.all.new_data_contrato_bancario.Disabled = false;
                } else {
                    crmForm.all.new_data_contrato_bancario.Disabled = true;
                }
            }
        }

        crmForm.BloquearCampoDataContratoBancario();

        crmForm.EsconderMenuPreGanho = function() {
            if ((crmForm.all.new_venda_garantida.DataValue == null || crmForm.all.new_venda_garantida.DataValue == undefined ||
                !crmForm.all.new_venda_garantida.DataValue) || (crmForm.all.new_tipodeoportunidade.DataValue != TipoVenda.VENDA_UNIDADE || (crmForm.all.statuscode.DataValue != RazaoStatus.EM_ANDAMENTO && crmForm.all.statuscode.DataValue != RazaoStatus.QUEDA_EM_ANDAMENTO))) {
                crmForm.RemoveBotao("PrGanho");
            }
        }

        crmForm.EsconderMenuPreGanho();

        crmForm.EsconderMenuReverterQueda = function() {
            if (crmForm.all.statuscode.DataValue != RazaoStatus.QUEDA_EM_ANDAMENTO || !crmForm.UsuarioPertenceEquipe("CR_Reverter_Queda")) {
                crmForm.RemoveBotao("ReverterQueda");
            }
        }

        crmForm.EsconderMenuReverterQueda();

        crmForm.TipoConsultaAtributoCustomer = function() {
            if (crmForm.all.new_tipodeoportunidade.DataValue != null &&
                crmForm.all.new_tipodeoportunidade.DataValue == TipoDeOportunidade.PERMUTA) {
                if (crmForm.all.new_terrenoid.DataValue != null) {
                    oParam = "objectTypeCode=101771&filterDefault=false&attributesearch=name&_new_terrenoid=" + crmForm.all.new_terrenoid.DataValue[0].id;
                    crmForm.FilterLookup(crmForm.all.customerid, 1, "/MRVWeb/FilterLookup/FilterLookup.aspx", oParam);
                }
            } else
            /// Definir tipo padrão.
                crmForm.all.customerid.parentNode.childNodes[0].onclick = null;
        }

        crmForm.AjudarFormularioVendaPermuta = function() {
            if (crmForm.all.new_tipodeoportunidade.DataValue != null &&
                crmForm.all.new_tipodeoportunidade.DataValue == TipoDeOportunidade.PERMUTA) {
                /// Desabilitar o campo cliente enquanto não for escolhido o terreno
                crmForm.all.customerid.Disabled = crmForm.all.new_terrenoid.DataValue == null;
            } else
                crmForm.all.customerid.Disabled = false;

            /// Definir tipo de consulta que o cliente irá executar
            crmForm.TipoConsultaAtributoCustomer();
        }
        crmForm.AjudarFormularioVendaPermuta();

        /// Não exibe o menu de reabertura de oportunidade a quem não pertencer a equipe
        if (!crmForm.UsuarioPertenceEquipe("Reabertura de Oportunidades"))
            crmForm.RemoveBotao("ReabrirOportunidade");

        crmForm.ProcessaRegraBoletoDigital = function() {
            if (crmForm.all.new_tipodeoportunidade.DataValue == TipoDeOportunidade.UNIDADE &&
                crmForm.FormType == TypeUpdate &&
                crmForm.all.new_exibe_adesao_boleto_digital.DataValue &&
                crmForm.all.new_opcao_adesao_confirmada.DataValue != true) {
                crmForm.all.new_opcao_adesao_confirmada.Disabled = false;

            }
            if (crmForm.all.new_tipodeoportunidade.DataValue == TipoDeOportunidade.UNIDADE &&
                crmForm.FormType == TypeUpdate &&
                crmForm.all.new_exibe_adesao_boleto_digital.DataValue &&
                crmForm.all.new_opcao_adesao_confirmada.DataValue != true) {
                crmForm.all.new_opcao_adesao_confirmada.Disabled = false;

            }
            crmForm.all.new_adesao_boleto_digital.Disabled = (crmForm.all.new_opcao_adesao_confirmada.DataValue != true || crmForm.FormType != TypeUpdate);
        };
        crmForm.ProcessaRegraBoletoDigital();

        crmForm.BloquearFormularioPreGanho = function() {
            if (crmForm.all.statuscode.DataValue && crmForm.all.statuscode.DataValue == RazaoStatus.PRE_GANHO) {
                crmForm.DesabilitarFormulario(true);

                if (crmForm.UsuarioPertenceEquipe(Equipe.CR_AUDITORIACONTBANCO) && crmForm.all.new_venda_garantida.DataValue) {
                    crmForm.all.new_data_contrato_bancario.Disabled = false;
                }
            }
        }

        crmForm.BloquearFormularioPreGanho();
        crmForm.FuncGerarContrato = function() {

            /// Validações dos campos
            var isVendaKit = crmForm.all.new_tipodeoportunidade.DataValue == TipoVenda.VENDA_KIT_ACABAMENTO;
            var isVendaUnidade = crmForm.all.new_tipodeoportunidade.DataValue == TipoVenda.VENDA_UNIDADE;
            var isVendaServico = crmForm.all.new_tipodeoportunidade.DataValue == TipoVenda.VENDA_SERVICO;
            var isVendaGaragem = crmForm.all.new_tipodeoportunidade.DataValue == TipoVenda.VENDA_GARAGEM; /// Tratamento para não pedir empreendimento para kit
            if (crmForm.all.new_empreendimentodaoportunidadeid.DataValue == null && !isVendaKit && !isVendaServico) {
                crmForm.all.new_empreendimentodaoportunidadeid.SetFocus();
                alert(Mensagem.EMPREENDIMENTO_OBRIGATORIO);
            } else if (crmForm.all.customerid.DataValue == null) {
                crmForm.all.customerid.SetFocus();

                alert(Mensagem.CLIENTE_OBRIGATORIO);

            } else {
                if (isVendaUnidade || isVendaGaragem) {
                    var validarDadosBancarioCliente = new RemoteCommand('MrvService', 'ValidarDadosBancarioCliente', '/MRVCustomizations/');
                    validarDadosBancarioCliente.SetParameter('customerId', crmForm.all.customerid.DataValue[0].id.toString());
                    var retornoDadosBancario = validarDadosBancarioCliente.Execute();
                    if (crmForm.TratarRetornoRemoteCommand(retornoDadosBancario, 'ValidarDadosBancarioCliente')) {
                        if (retornoDadosBancario.ReturnValue.Mrv.Mensagem != undefined) {
                            alert(retornoDadosBancario.ReturnValue.Mrv.Mensagem);
                        }
                        crmForm.GerarContratoCRM();
                    }
                } else {
                    crmForm.GerarContratoCRM();
                }
            }
        }
        crmForm.GerarContrato = function() {
            if (!crmForm.IsDirty) {
                DisplayActionMsg(Mensagem.GERANDO_CONTRATO, 400, 150);
                setTimeout('crmForm.FuncGerarContrato()', 1000);
            } else
                alert(Mensagem.SALVE_ANTES);
        }
        crmForm.GerarContratoCRM = function() {
            var cmd = new RemoteCommand('MrvService', 'GerarContrato', '/MRVCustomizations/');
            cmd.SetParameter('opportunityId', crmForm.ObjectId);
            var retorno = cmd.Execute();
            if (retorno.Success) {
                if (crmForm.TratarRetornoRemoteCommand(retorno, 'GerarContrato')) {
                    if (retorno.ReturnValue.Mrv.Success != null && retorno.ReturnValue.Mrv.Success != undefined && !retorno.ReturnValue.Mrv.Success)
                        alert(Mensagem.OCORREU_SEGUINTE_ERRO + retorno.ReturnValue.Mrv.Mensagem);
                    else {
                        if (retorno.ReturnValue.Mrv.Mensagem) {
                            alert(retorno.ReturnValue.Mrv.Mensagem);
                        }
                        openObj(1010, retorno.ReturnValue.Mrv.contractId);
                        crmForm.SubmitCrmForm(1, true, true, false);
                    }
                }
            } else
                alert(Mensagem.FALHA_ENVIO);

        }

        AuditarFiador = function(userLogado) {
            var cmd = new RemoteCommand("MrvService", "Auditar", "/MrvCustomizations/");
            cmd.SetParameter("entidadeId", crmForm.ObjectId);
            cmd.SetParameter("tipoAuditoria", TipoAuditoria.OPORTUNIDADE);
            var resultado = cmd.Execute();
            if (crmForm.TratarRetornoRemoteCommand(resultado, "Auditar")) {
                document.getElementById("IFRAME_auditoria_fiador").src = document.getElementById("IFRAME_auditoria_fiador").src;
                return true;
            }
            return false;
        }

        ExibirMensagemNaoAuditar = function() {
            alert("Foi realizada a Queda da Oportunidade, devido a não auditoria do Fiador!");
            window.location.reload(false);
        }

        crmForm.ValidarDataContratoBancario = function() {
            var valorCampo = crmForm.all.new_data_contrato_bancario.DataValue != null ? crmForm.all.new_data_contrato_bancario.DataValue.toLocaleDateString() : "";
            var cmd = new RemoteCommand('MrvService', 'ValidarDataContratoBancario', '/MRVCustomizations/');
            cmd.SetParameter('oportunidadeId', crmForm.ObjectId);
            cmd.SetParameter('dataContratoBancario', valorCampo);
            var retorno = cmd.Execute();
            if (retorno.Success) {
                if (crmForm.TratarRetornoRemoteCommand(retorno, 'ValidarDataContratoBancario')) {
                    if (retorno.ReturnValue.Mrv.Mensagem != undefined && retorno.ReturnValue.Mrv.Mensagem != '') {
                        alert(retorno.ReturnValue.Mrv.Mensagem);
                        crmForm.all.new_data_contrato_bancario.DataValue = null;
                    }
                }
            } else {
                alert(Mensagem.FALHA_ENVIO);
            }
        }
    }

    crmForm.OpenChecklist = function() {
        if (crmForm.ValidarPermissaoAtivacaoContrato()) {
            var url = '/MrvWeb/Contrato/ChecklistAtivacaoContrato.aspx?oportunidadeId=' + crmForm.ObjectId;
            window.open(url, 'Janela', 'toolbar=no,location=no,directories=no,status=no,menubar=no,resizable=no,width=600px,height=600px');
        }
    }

    crmForm.ValidarPermissaoAtivacaoContrato = function() {
        var cmdValidarPermissaoAtivacaoContrato = new RemoteCommand('MrvService', 'ValidarPermissaoAtivacaoContrato', '/MRVCustomizations/');
        cmdValidarPermissaoAtivacaoContrato.SetParameter('oportunidadeId', crmForm.ObjectId);
        var retorno = cmdValidarPermissaoAtivacaoContrato.Execute();
        if (!crmForm.TratarRetornoRemoteCommand(retorno, "ValidarPermissaoAtivacaoContrato")) {
            alert(retorno.ReturnValue.Mrv.MensagemErro);
            return false;
        }
        if (retorno.ReturnValue.Mrv.MensagemErro != undefined && retorno.ReturnValue.Mrv.MensagemErro != '') {
            alert(retorno.ReturnValue.Mrv.MensagemErro);
            return false;
        }
        return true;
    }

    crmForm.ActiveContract = function() {
        if (crmForm.ValidarPermissaoAtivacaoContrato()) {
            var url = '/MrvWeb/Contrato/ChecklistAtivacaoContrato.aspx?oportunidadeId=' + crmForm.ObjectId;
            var cmdValidaPreAtivacaoCredito = new RemoteCommand('MrvService', 'ValidarAtivacaoContrato', '/MRVCustomizations/');
            cmdValidaPreAtivacaoCredito.SetParameter('oportunidadeId', crmForm.ObjectId);
            var retorno = cmdValidaPreAtivacaoCredito.Execute();
            if (!crmForm.TratarRetornoRemoteCommand(retorno, "ValidarAtivacaoContrato")) {
                if (confirm(retorno.ReturnValue.Mrv.MensagemErro)) {
                    window.open(url, 'Janela', 'toolbar=no,location=no,directories=no,status=no,menubar=no,resizable=no,width=600px,height=600px');
                }
                return;
            }
            if (retorno.ReturnValue.Mrv.MensagemErro != undefined && retorno.ReturnValue.Mrv.MensagemErro != '') {
                if (confirm(retorno.ReturnValue.Mrv.MensagemErro)) {
                    window.open(url, 'Janela', 'toolbar=no,location=no,directories=no,status=no,menubar=no,resizable=no,width=600px,height=600px');
                }
                return;
            }
            alert('Contrato ' + retorno.ReturnValue.Mrv.Mensagem + ' com sucesso.');
            
            crmForm.RemoveBotao("ValidarPrativaodoContrato");
            
        }
    }

    crmForm.ReverterQueda = function() {
        var url = '/MRVWeb/LogAlteracao/MotivoReverterQueda.aspx';
        var retorno = openStdDlg(url, null, 750, 440);

        if (retorno != null && retorno != undefined && retorno.Justificativa) {
            var cmdReverterQueda = new RemoteCommand('MrvService', 'ReverterQueda', '/MRVCustomizations/');
            cmdReverterQueda.SetParameter('oportunidadeId', crmForm.ObjectId);
            var retornoReverterQueda = cmdReverterQueda.Execute();
            if (crmForm.TratarRetornoRemoteCommand(retornoReverterQueda, "ReverterQueda")) {
                if (retornoReverterQueda.ReturnValue.Mrv.Mensagem != undefined) {
                    alert(retornoReverterQueda.ReturnValue.Mrv.Mensagem);
                    return;
                }
                var cmdCriarLog = new RemoteCommand('MrvService', 'CriarLogReverterQueda', '/MRVCustomizations/');
                cmdCriarLog.SetParameter('oportunidadeId', crmForm.ObjectId);
                cmdCriarLog.SetParameter('justificativa', retorno.Justificativa);
                var retornoLog = cmdCriarLog.Execute();
                if (crmForm.TratarRetornoRemoteCommand(retornoLog, "CriarLogReverterQueda")) {
                    if (!retornoLog.ReturnValue.Mrv.ExecutadoSucesso) {
                        alert('Não foi possível gravar o Log do Processo de Reversão de Queda');
                        return;
                    }
                }
            }
            alert('Processo executado com sucesso!');
        }
    }
}
catch (erro) {
    alert("Ocorreu um erro no formulário.\n" + erro.description);
    formularioValido = false;
}
