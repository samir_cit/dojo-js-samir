﻿if (crmForm.all.new_cidadeempreendimentooporunidadeid.IsDirty)
    crmForm.all.new_empreendimentodaoportunidadeid.DataValue = null;

var oCidade = crmForm.all.new_cidadeempreendimentooporunidadeid.DataValue != null ? crmForm.all.new_cidadeempreendimentooporunidadeid.DataValue[0].id : "";
oCidade = oCidade.substr(1, oCidade.length - 2);

var typeCode = "10001";
if (oCidade == "")
    typeCode = "100012";
var oParam = "objectTypeCode=" + typeCode + "&filterDefault=false&attributesearch=new_nome&_new_cidadeid=" + oCidade;

crmForm.FilterLookup(crmForm.all.new_empreendimentodaoportunidadeid, 10001, "/MRVWeb/FilterLookup/FilterLookup.aspx", oParam);