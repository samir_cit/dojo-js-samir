﻿if (crmForm.all.new_adesao_boleto_digital.DataValue != null) {
    var opcao = crmForm.all.new_adesao_boleto_digital.DataValue == 1 ? 'deseja optar' : 'não deseja optar';
    if (!window.confirm('Cliente ainda não aderiu ao Boleto Digital.\n A opção selecionada indica que o cliente ' + opcao + ' pela adesão? Confirma a resposta do cliente?')){
        crmForm.all.new_adesao_boleto_digital.DataValue = null;
        crmForm.SetFieldReqLevel("new_adesao_boleto_digital", true);
        crmForm.all.new_adesao_boleto_digital.SetFocus();
    }
}