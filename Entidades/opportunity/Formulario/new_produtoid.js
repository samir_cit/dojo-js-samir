﻿if (crmForm.new_produtoid.DataValue != null) {
    crmForm.PreencheEmpreendimentoCidade();
}
if (crmForm.all.new_tipodeoportunidade.DataValue != null) 
{
    var tipoOportunidadeVenda =
        crmForm.all.new_tipodeoportunidade.DataValue == TipoDeOportunidade.UNIDADE ||
        crmForm.all.new_tipodeoportunidade.DataValue == TipoDeOportunidade.GARAGEM ||
        crmForm.all.new_tipodeoportunidade.DataValue == TipoDeOportunidade.LOJA ||
        crmForm.all.new_tipodeoportunidade.DataValue == TipoDeOportunidade.LOTE;

    var tipoProduto = 0;

    switch (crmForm.all.new_tipodeoportunidade.DataValue) 
    {
        case TipoDeOportunidade.UNIDADE:
        case TipoDeOportunidade.GARAGEM:
        case TipoDeOportunidade.BOX:
            tipoProduto = crmForm.all.new_tipodeoportunidade.DataValue;
            break;
        case TipoDeOportunidade.LOTE:
            tipoProduto = 200001;
            break;
        case TipoDeOportunidade.LOJA:
            tipoProduto = 200002;
            break;
    }

    var permuta = crmForm.all.new_tipodeoportunidade.DataValue == TipoDeOportunidade.PERMUTA;

    // Customizando o FilterLookup de acordo com o tipo de oportunidade.
	var empreendimento
    var terreno;
    var customer;
    var oParam;
	
	if (crmForm.all.new_empreendimentodaoportunidadeid.DataValue != null)
        empreendimento = crmForm.all.new_empreendimentodaoportunidadeid.DataValue[0].id;
    else
        empreendimento = "";

    if (crmForm.all.new_terrenoid.DataValue != null)
        terreno = crmForm.all.new_terrenoid.DataValue[0].id;
    else
        terreno = "";

    if (crmForm.all.customerid.DataValue != null)
        customer = crmForm.all.customerid.DataValue[0].id;
    else
        customer = "";
    
    if (permuta)
        oParam = "objectTypeCode=10247&filterDefault=false&attributesearch=name&_new_terrenoid=" + terreno;
    else if (tipoOportunidadeVenda)
        oParam = "objectTypeCode=1024&filterDefault=false&attributesearch=name&_producttypecode=" + tipoProduto + "&_new_empreendimentoid=" + empreendimento;
    else
        oParam = "objectTypeCode=1026&filterDefault=false&attributesearch=name&_customerid=" + customer;

    crmForm.FilterLookup(crmForm.all.new_produtoid, 1024, "/MRVWeb/FilterLookup/FilterLookup.aspx", oParam);

    crmForm.ValidarEmpreendimentoKitObra();
    
    crmForm.all.new_tipodeplanodefinanciamentoopid.FireOnChange();
}