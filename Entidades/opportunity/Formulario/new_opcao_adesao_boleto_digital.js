﻿if (crmForm.all.new_opcao_adesao_confirmada.DataValue == true) {
    if (!window.confirm('Ao marcar este campo você está indicando que o cliente foi consultado se deseja fazer a adesão ao boleto digital.\nConfirma?')) {
        crmForm.all.new_opcao_adesao_confirmada.DataValue = false;
        crmForm.all.new_opcao_adesao_confirmada.SetFocus();
        crmForm.all.new_adesao_boleto_digital.Disabled = true;
        crmForm.SetFieldReqLevel("new_adesao_boleto_digital", false);
    }
    else {
        crmForm.all.new_adesao_boleto_digital.Disabled = false;
        crmForm.all.new_opcao_adesao_confirmada.Disabled = true;
        crmForm.SetFieldReqLevel("new_adesao_boleto_digital", true);
    }
}