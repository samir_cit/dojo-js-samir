﻿formularioValido = true;

try {

    function VerificarSeInformacaoFoiRepassada() {
        if (window.opener.opener && window.opener.crmForm.ObjectTypeCode == 112) {
            var informacaoRepassada = confirm("A informação foi passada para o cliente?");

            window.opener.crmForm.all.new_informacao_critica_repassada.DataValue = informacaoRepassada;
            var valor = crmForm.all.new_descricao.DataValue;
            if (valor != null && valor.length > 100)
                valor = valor.substr(0, 100);
            window.opener.crmForm.all.new_descricao_informacao_critica.DataValue = valor;
        }
    }

    window.attachEvent("onunload", VerificarSeInformacaoFoiRepassada);

    function guidGenerator() {
        var S4 = function() {
            return (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1);
        };
        return ("INFC-" + S4() + S4()).toUpperCase();
    }

    function Load() {

        StatusCode =
        {
            PENDENTE_DE_APROVACAO: "1",
            INATIVO: "2",
            APROVADA: "3",
            RECUSADA: "4"
        }

        if (crmForm.UsuarioPertenceEquipe("Editor SAC de Informações Críticas") || crmForm.UsuarioPertenceEquipe("Coordenador SAC")) {
            crmForm.all.statuscode.ForceSubmit = false;
            crmForm.all.new_replicar.ForceSubmit = false;
        }
        else {
            crmForm.EscondeCampo("new_assuntoid", true);
            crmForm.EscondeCampo("statuscode", true);
            crmForm.EscondeCampo("new_data_inicio", true);
            crmForm.EscondeCampo("new_data_fim", true);
            crmForm.EscondeCampo("new_replicar", true);

            crmForm.DesabilitarFormulario(true);
        }

        crmForm.EscondeCampo("ownerid", true);
        crmForm.EscondeCampo("new_justificativa", crmForm.all.statuscode.DataValue != StatusCode.RECUSADA);

        if (crmForm.FormType == TypeCreate) {
            /// Código gerado automático para controle interno.
            crmForm.all.new_name.DataValue = guidGenerator();

            crmForm.DesabilitarCampos("statuscode", true);
        }
    }

    function loadScript() {
        var oScript = document.createElement('script');
        oScript.type = "text/javascript";
        oScript.src = "/_static/Base.js";

        var oHead = document.getElementsByTagName('head')[0];
        oHead.appendChild(oScript);

        oScript.onreadystatechange = function() {
            if (this.readyState == 'complete' || this.readyState == 'loaded')
                Load();
        }
    }

    loadScript();

} catch (error) {
    formularioValido = false;
    alert("Ocorreu um erro no formulário.\n" + error.description);
}