﻿try {

    if (!formularioValido) {
        alert("Ocorreu um erro no formulário. Impossível salvar.");
        event.returnValue = false;
        return false;
    }
    
    crmForm.all.statuscode.ForceSubmit = true;
    crmForm.all.new_replicar.ForceSubmit = true;

} catch (error) {
    alert("Ocorreu um erro no formulário.\n" + error.description);
    event.returnValue = false;
    return false;
}