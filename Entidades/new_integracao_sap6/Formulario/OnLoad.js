﻿formularioValido = true;
try {

    crmForm.EsconderCampo = function(valor) {
        var campoC = eval("crmForm.all." + valor + "_c");
        var campoD = eval("crmForm.all." + valor + "_d");
        campoC.style.display = "none";
        campoD.style.display = "none";
    }

    //Campos para esconder na tela
    var campos = new Array("new_accountid", "new_contractid", "new_productid", "new_boletoid");
    for (var i = 0; i < campos.length; i++) {
        crmForm.EsconderCampo(campos[i]);
    }


} catch (error) {
    formularioValido = false;
    alert("Ocorreu um erro no formulário.\n" + error.description);
}