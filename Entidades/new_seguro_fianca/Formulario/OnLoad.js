﻿formularioValido = true;
try {
    //Tipos
    var SG = 2; //Seguro Garantia
    var CF = 1; //Carta Fiança

    function VisaoCartaFianca(mostrar) {
        var estilo = (mostrar) ? "" : "none";
        //esconde seção carta fiança e prorrogação
        document.getElementById("new_cf_data_emissao_d").parentNode.parentNode.style.display = estilo;
        document.getElementById("new_cf_data_inicio_vigencia_prorrogacao_d").parentNode.parentNode.style.display = estilo;

        //Habilitar ou Desabilitar campos
        crmForm.all.new_cf_data_emissao.Disabled = !mostrar;
        crmForm.all.new_cf_data_inicio_vigencia.Disabled = !mostrar;
        crmForm.all.new_bancosid.Disabled = !mostrar;
        crmForm.all.new_cf_data_final_vigencia.Disabled = !mostrar;
        crmForm.all.new_cf_valor.Disabled = !mostrar;
        crmForm.all.new_cf_data_baixa_documento.Disabled = !mostrar;
        crmForm.all.new_cf_descricao.Disabled = !mostrar;
        crmForm.all.new_cf_numero_fianca.Disabled = !mostrar;
        crmForm.all.new_cf_data_inicio_vigencia_prorrogacao.Disabled = !mostrar;
        crmForm.all.new_cf_data_prevista_solicitacao.Disabled = !mostrar;
        crmForm.all.new_cf_data_efetiva_solicitacao.Disabled = !mostrar;
        crmForm.all.new_cf_data_final_vigencia_prorrogada.Disabled = !mostrar;
        
        if (mostrar)
            DefinirNomeDoLabel("Beneficiários");
    }

    function DefinirNomeDoLabel(nome) {
        try {
            var html = document.getElementById("IFRAME_Clientes_d").parentNode.parentNode;
            for (var i = 0; i < html.childNodes.length; i++) {
                for (var indice = 0; indice < html.childNodes.item(i).childNodes.length; indice++) {
                    if (html.childNodes.item(i).childNodes.item(indice).className == "ms-crm-Form-Section ") {
                        html.childNodes.item(i).childNodes.item(indice).innerHTML = nome;
                        break;
                    }
                }
            }
        }
        catch (bla) {
            alert(bla.description);
        }
    }

    function VisaoSeguroGarantia(mostrar) {
        var estilo = (mostrar) ? "" : "none";
        //esconde seção carta fiança e prorrogação
        document.getElementById("new_sg_data_emissao_d").parentNode.parentNode.style.display = estilo;
        document.getElementById("new_sg_data_inicio_vigencia_prorrogada_d").parentNode.parentNode.style.display = estilo;

        //Habilitar ou Desabilitar campos
        crmForm.all.new_sg_data_emissao.Disabled = !mostrar;
        crmForm.all.new_sg_data_inicio_vigencia.Disabled = !mostrar;
        crmForm.all.new_seguradoraid.Disabled = !mostrar;
        crmForm.all.new_sg_data_final_vigencia.Disabled = !mostrar;
        crmForm.all.new_sg_valor.Disabled = !mostrar;
        crmForm.all.new_sg_data_baixa_documento.Disabled = !mostrar;
        crmForm.all.new_sg_descricao.Disabled = !mostrar;
        crmForm.all.new_sg_numero_apolice.Disabled = !mostrar;
        crmForm.all.new_sg_data_inicio_vigencia_prorrogada.Disabled = !mostrar;
        crmForm.all.new_sg_data_prevista_solicitacao.Disabled = !mostrar;
        crmForm.all.new_sg_data_efetiva_solicitacao.Disabled = !mostrar;
        crmForm.all.new_sg_data_final_vigencia_prorrogada.Disabled = !mostrar;

        if (mostrar)
            DefinirNomeDoLabel("Segurado");
    }

    function ObterTipo() {
        return (crmForm.all.new_tipo_garantia.DataValue != null) ? crmForm.all.new_tipo_garantia.DataValue : null;
    }

    crmForm.ConfiguraVisaoPorTipo = function() {
        var tipo = ObterTipo();
        if (tipo == null) {
            crmForm.all.new_name.DataValue = "Tipo de Garantia não definido.";
            VisaoCartaFianca(false);
            VisaoSeguroGarantia(false);
            DefinirNomeDoLabel("Tipo de Garantia não definido.");
        }

        if (tipo == CF) {
            crmForm.all.new_name.DataValue = "Carta Fiança";
            VisaoCartaFianca(true);
            VisaoSeguroGarantia(false);
        }
        if (tipo == SG) {
            //document.getElementById("IFRAME_Clientes_d").parentNode.parentNode.innerText = "Segurado";
            crmForm.all.new_name.DataValue = "Seguro Garantia";
            VisaoCartaFianca(false);
            VisaoSeguroGarantia(true);
        }
    }

    crmForm.ConfiguraVisaoPorTipo();

    //Esconder campo nome
    document.getElementById("new_name_d").parentNode.parentNode.style.display = "none";

    function ObterEnderecoIframe() {
        if (crmForm.ObjectId != null) {

            var oId = crmForm.ObjectId;
            var oType = crmForm.ObjectTypeCode;
            var security = crmFormSubmit.crmFormSubmitSecurity.value;

            return "/" + ORG_UNIQUE_NAME + "/userdefined/areas.aspx?oId=" + oId + "&oType=" + oType + "&security=" + security + "&roleOrd=1&tabSet=areanew_new_seguro_fianca_account";
        }
        else {
            return "about:blank";
        }
    }

    //Funcao para deixar a Pagina encaixada exatamente dentro do Iframe sem que sobre espacos deixando o Layout desalinhado.
    //Aguarda a janela até que o conteúdo do IFrame  estaja pronto
    function AjustandoIframe(iframeID) {
        if (document.getElementById(iframeID).contentWindow.document.readyState != "complete") {
            window.setTimeout(function() { AjustandoIframe(iframeID) }, 10);
        }
        else {
            // Alterar a cor do fundo
            document.getElementById(iframeID).contentWindow.document.body.style.backgroundColor = "#eef0f6";
            // Remova a borda esquerda
            document.getElementById(iframeID).contentWindow.document.body.all(0).style.borderLeftStyle = "none";
            // Remove padding
            document.getElementById(iframeID).contentWindow.document.body.all(0).all(0).all(0).all(0).style.padding = "0px";
            // Deixa a célula com toda a largura do IFRAME
            document.getElementById(iframeID).contentWindow.document.body.all(0).style.width = "100%"

            // mostra o IFrame
            document.getElementById(iframeID).style.display = "block";
        }
    }

    //Mostrar clientes relacionados quando não for uma criação.
    if (crmForm.FormType != 1) {
        crmForm.all.IFRAME_Clientes.src = ObterEnderecoIframe();
        AjustandoIframe("IFRAME_Clientes");
    }
    else {
        //document.getElementById("IFRAME_Clientes_d").parentNode.parentNode.innerText = "NADA";
        document.getElementById("IFRAME_Clientes_d").parentNode.parentNode.style.display = "none";
    }
} catch (error) {
    formularioValido = false;
    alert("Ocorreu um erro no formulário.\n" + error.description);
}