﻿formularioValido = true;

try {

    crmForm.all.new_dias_de_vencimento.onkeydown = function() { new_dias_de_vencimento_keydown() };
    
    function new_dias_de_vencimento_keydown() {

        var backspace = (event.keyCode == 8);
        var virgula = (event.keyCode == 188 || event.keyCode == 110);
        var setas = (event.keyCode == 37 || event.keyCode == 38 || event.keyCode == 39 || event.keyCode == 40);
        var deleta = (event.keyCode == 46);
        var tab = (event.keyCode == 9);
        var bloqueiaDigito = (event.keyCode < 48 || event.keyCode > 57) && (!virgula && !backspace && !setas && !deleta && !tab);
        bloqueiaDigito = (event.keyCode < 96 || event.keyCode > 105) && bloqueiaDigito
        
        if (bloqueiaDigito) {
            event.returnValue = false;
            event.keyCode = 0;
            return false;
        }
    }
   
    // Verifica se o usuário tem permissão na equipe
    crmForm.UsuarioPertenceEquipe = function(nomeEquipe) {
        var cmd = new RemoteCommand("MrvService", "UserContainsTeams", "/MRVCustomizations/");
        cmd.SetParameter("teams", nomeEquipe);
        var resultado = cmd.Execute();
        return resultado.Success ? resultado.ReturnValue.Mrv.Found : false;
    }

    // Desabilita campos no formulário
    crmForm.DesabilitarCampos = function(campos, tipo) {
        var tmp = campos.toString().split(";");

        for (var i = 0; i < tmp.length; i++) {
            var atributo = eval("crmForm.all." + tmp[i]);
            if (atributo != null)
                atributo.Disabled = tipo;
        }
    }

    crmForm.ValidacaoDataVencimento = function() {

        var diasVencimento = crmForm.all.new_dias_de_vencimento.DataValue;

        if (diasVencimento != null) {

            try {

                var dias = diasVencimento.toString().split(",");

                for (x = 0; x < dias.length; x++) {
                    if (dias[x] == "")
                        throw "Formato_Invalido";
                    else if (dias[x] > 31 || dias[x] == 0)
                        throw "Dia_Invalido";
                    else {
                        var repeticoes = 0;
                        //verifica duplicata de dias
                        for (b = 0; b < dias.length; b++) {
                            if (dias[x] == dias[b])
                                repeticoes++;
                        }
                        if (repeticoes > 1)
                            throw "Duplicatas_dias";
                    }
                }
            }
            catch (e) {
                if (e == "Formato_Invalido")
                    alert("Formato inválido informado no campo Dias de Vencimento.");
                else if (e == "Dia_Invalido")
                    alert("Existe(m) dia(s) inválido(s) no campo Dias de Vencimento.");
                else if (e == "Duplicatas_dias")
                    alert("Existem dias repetidos no campo Dias de Vencimento.");

                event.returnValue = false;
            }
        }
    }

    function Load() {
        //Verifica se usuário faz parte da equipe 'Lançamento Data de Vencimento', se não exibe alerta.
        if (!crmForm.UsuarioPertenceEquipe("Lançamento Data de Vencimento")) {
            alert('Você não tem permissão para acesso a esta tela.');
            self.close();
        }

        var Grupo = crmForm.all.new_grupo;
        var remover = "";

        for (index = 0; index < Grupo.Options.length; index++) {
            var valor = Grupo[index].value;
            //5-Mensal, 7-intermediária, 11-Serviço e 12-Kit
            if (valor != 5 && valor != 7 && valor != 11 && valor != 12 && Grupo[index].text != "")
                remover += index + ",";
        }

        for (x = 0; x < remover.split(",").length - 1; x++) {
            crmForm.all.new_grupo.DeleteOption(remover.split(",")[x]);
        }

        if (crmForm.FormType == TypeUpdate)
            crmForm.all.new_grupo.Disabled = true;
    }

    function loadScript() {
        var oHead = document.getElementsByTagName('head')[0];
        var oScript = document.createElement('script');
        oScript.type = "text/javascript";
        oScript.src = "/_static/Base.js";

        oScript.onreadystatechange = function() {
            if (this.readyState == 'complete' || this.readyState == 'loaded') {
                Load();
            }
        }
        oHead.appendChild(oScript);
    }

    loadScript();

}
catch (error) {
    formularioValido = false;
    alert("Ocorreu um erro no formulário.\n" + error.description);
}
