﻿//Metodo OnSave de Bloco
try {
    
    if (!crmForm.IsDirty)
        return false;

    if (!formularioValido) {
        alert("Ocorreu um erro no formulário. Impossível salvar.");
        event.returnValue = false;
        return false;
    }

    crmForm.ValidacaoDataVencimento();

    if (crmForm.FormType == TypeCreate){
        
        var cmd = new RemoteCommand("MrvService", "ValidaExistenciaDeGrupo", "/MRVCustomizations/");
        cmd.SetParameter("grupo", crmForm.all.new_grupo.DataValue);

        var result = cmd.Execute();
        if (result.Success) {
            if (result.ReturnValue.Mrv.ExisteGrupo){
                alert("Este Grupo já foi cadastrado.");
                crmForm.all.new_grupo.SetFocus();
                event.returnValue = false;
                return false;
            }
        }
    }
    
    //Transformar caracteres em maiusculos
    var elm = document.getElementsByTagName("input");
    for (i = 0; i < elm.length; i++) {
        if (elm[i].type == "text")
            if (elm[i].DataValue != null) {
            try {
                elm[i].DataValue = elm[i].DataValue.toUpperCase();
            }
            catch (e) { }
        }
    }

}
catch (error) {
    alert("Ocorreu um erro no formulário.\n" + error.description);
    event.returnValue = false;
    return false;
}