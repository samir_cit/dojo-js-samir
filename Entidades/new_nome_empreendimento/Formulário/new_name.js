﻿if (crmForm.all.new_name.DataValue != null) {
    var Nome = crmForm.all.new_name.DataValue;
    if (Nome.length > 20) {
        crmForm.all.new_nome_reduzido.Disabled = false;
        crmForm.SetFieldReqLevel("new_nome_reduzido", true);
    }
    else {
        crmForm.all.new_nome_reduzido.DataValue = Nome;
        crmForm.all.new_nome_reduzido.Disabled = true;
        crmForm.SetFieldReqLevel("new_nome_reduzido", false);
    }
}