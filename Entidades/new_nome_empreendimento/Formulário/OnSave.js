﻿try{
    if (!formularioValido) {
        alert("Ocorreu um erro no formulário. Impossível salvar.");
        event.returnValue = false;
        return false;
    }
    
     //Nome do epreendimento (completo e reduzido) não podem conter hífen "-"
    if((!(crmForm.all.new_nome_reduzido.Disabled)) || (!(crmForm.all.new_name.Disabled)))
    {
        var Nome_empreendimento = (crmForm.all.new_name.DataValue != null)?crmForm.all.new_name.DataValue:"";
        var Nome_empreendimento_reduzido = (crmForm.all.new_nome_reduzido.DataValue != null)?crmForm.all.new_nome_reduzido.DataValue:"";
        if((Nome_empreendimento_reduzido.indexOf("-") != -1) || (Nome_empreendimento.indexOf("-") != -1))
        {
            alert("O nome do empreendimento / nome reduzido não pode conter hífen.");
            crmForm.all.new_name.SetFocus();
            event.returnValue = false;
            return false;
        }
    }

    // Transformar caracteres em maiusculos------------------------------------//
    var elm = document.getElementsByTagName("input");
    for (i = 0; i< elm.length; i++){
        if (elm[i].type == "text"){
            if (elm[i].DataValue != null){
                try{
                    elm[i].DataValue = elm[i].DataValue.toUpperCase();
                }
                catch (e){}
            }
        }
    }
}
catch (error) {
    alert("Ocorreu um erro no formulário.\n" + error.description);
    event.returnValue = false;
    return false;
}