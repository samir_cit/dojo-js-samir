﻿formularioValido = true;
try {
	/*
	* Descrição:    Desabilita todos os campos do formulário
	*
	* Autor:        Francisco Avelino
	* Data:         24/02/2010
	* 
	*/
	DesabilitaCampos = function() {
		for (var index = 0; index < crmForm.all.length; index++)
			if (crmForm.all[index].Disabled != null)
				crmForm.all[index].Disabled = true;
	}

	// Verifica se o formulário foi aberto para edição e se o Nome já foi enviado
	if (crmForm.ObjectId != null ){//&& crmForm.all.statuscode.DataValue == 3) {
	    // Bloqueia todos os atributos do formulário
	    DesabilitaCampos();
	}
} catch (error) {
    formularioValido = false;
    alert("Ocorreu um erro no formulário.\n" + error.description);
}