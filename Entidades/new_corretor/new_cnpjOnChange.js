﻿var cnpj =  crmForm.all.new_cnpj.DataValue;
if (cnpj != null) 
{
   var valido = true;
   var strNum = cnpj.replace(/[^0-9]/g, "");
   var i = 0; var l = 0;  var strMul = "6543298765432";
   var character = ""; var iSoma = 0; var strNum_base = ""; var iLenNum_base = 0;
   var iLenMul = 0; var iSoma = 0; var strNum_base = 0; var iLenNum_base = 0;
           
   if (strNum.length != 14)
      valido = false;
   else {
        strNum_base = strNum.substring(0, 12);
        iLenNum_base = strNum_base.length - 1;
        iLenMul = strMul.length - 1;
        for (i = 0; i < 12; i++)
            iSoma = iSoma + parseInt(strNum_base.substring((iLenNum_base - i), (iLenNum_base - i) + 1), 10) * parseInt(strMul.substring((iLenMul - i), (iLenMul - i) + 1), 10);
        iSoma = 11 - (iSoma - Math.floor(iSoma / 11) * 11);
        if (iSoma == 11 || iSoma == 10)
            iSoma = 0;
        strNum_base = strNum_base + iSoma;
        iSoma = 0;
        iLenNum_base = strNum_base.length - 1
        for (i = 0; i < 13; i++)
            iSoma = iSoma + parseInt(strNum_base.substring((iLenNum_base - i), (iLenNum_base - i) + 1), 10) * parseInt(strMul.substring((iLenMul - i), (iLenMul - i) + 1), 10)
        iSoma = 11 - (iSoma - Math.floor(iSoma / 11) * 11);
        if (iSoma == 11 || iSoma == 10)
            iSoma = 0;
        strNum_base = strNum_base + iSoma;
        if (strNum != strNum_base)
            valido = false;
 }
 if (!valido) {
     alert('Digite um CNPJ válido.');
     crmForm.all.new_cnpj.DataValue = null;
 }
 else
     crmForm.all.new_cnpj.DataValue = strNum.substr(0, 2) + "." + strNum.substr(2, 3) + "." + strNum.substr(5, 3) + "/" + strNum.substr(8, 4) + "-" + strNum.substr(12, 2);
}