﻿formularioValido = true;
try {

	var guid = (function () {
		function s4() {
			return Math.floor((1 + Math.random()) * 0x10000)
			.toString(16)
			.substring(1);
		}
		return function () {
			return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
			s4() + '-' + s4() + s4() + s4();
		};
	})();

	function loadScript() {
		var oScript = document.createElement('script');
		oScript.type = "text/javascript";
		oScript.src = "/_static/Base.js?guid=" + guid();
		var oHead = document.getElementsByTagName('head')[0];
		oHead.appendChild(oScript);

		oScript.onreadystatechange = function () {
			if (this.readyState == 'complete' || this.readyState == 'loaded')
				Load();
		}
	}

	loadScript();
	
	function Load() {
	
	}
} catch (error) {
	formularioValido = false;
	alert("Ocorreu um erro no formulário.\n" + error.description);
}