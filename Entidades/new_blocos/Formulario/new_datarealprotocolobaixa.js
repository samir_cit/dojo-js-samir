﻿// Se não informou Data Real Protocolo, desabilita Número do Protocolo 

if (crmForm.all.new_datarealprotocolobaixa.DataValue == null)
{
    crmForm.all.new_num_protocolo_habitese_c.style.visibility = 'hidden';
    crmForm.all.new_num_protocolo_habitese_d.style.visibility = 'hidden';
    crmForm.all.new_num_protocolo_habitese.DataValue = null;
    crmForm.SetFieldReqLevel('new_num_protocolo_habitese', false);    
}
else 
{
    crmForm.all.new_num_protocolo_habitese_c.style.visibility = 'visible';
    crmForm.all.new_num_protocolo_habitese_d.style.visibility = 'visible';
    crmForm.SetFieldReqLevel('new_num_protocolo_habitese', true);
}
