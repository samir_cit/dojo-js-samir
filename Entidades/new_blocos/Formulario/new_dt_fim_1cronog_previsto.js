﻿//Preencher o campo de data prevista de inicio caso ela esteja vazia--------------------------------//
if(crmForm.all.new_datafimcronog.DataValue == null)
{
    crmForm.all.new_datafimcronog.DataValue = crmForm.all.new_dt_fim_1cronog_previsto.DataValue;
}

//calcular a duracao de obra da data prevista----------------------------------//
crmForm.all.new_dataprimeirocontratocliente.FireOnChange();


//Confirma a alteração
if(crmForm.all.new_dt_fim_1cronog_previsto.DataValue != null && crmForm.all.new_data_fim_primeiro_cronog.DataValue != null)
{
    if(!window.confirm("Deseja realmente trocar a data?"))
    {
         crmForm.all.new_dt_fim_1cronog_previsto.DataValue = crmForm.all.new_data_fim_primeiro_cronog.DataValue;
         crmForm.all.new_duracao_obra_1cronog.FireOnChange();
    }
    else
    {
         crmForm.all.new_duracao_obra_1cronog.FireOnChange();
    }
}
else
{
     crmForm.all.new_duracao_obra_1cronog.FireOnChange();
}

/* 
Atualiza a Data Prevista do Contrato do Cliente
Re-Calcula Datas Engenharia
*/
crmForm.AtualizaDataTerminoObra();
crmForm.all.new_atua_dt_termino_obra.FireOnChange();
