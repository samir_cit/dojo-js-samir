﻿if (crmForm.all.new_datadeterminodeobra.DataValue == null) 
{
    alert("Você deve fornecer um valor para Data Prevista do Contrato Cliente.");
    crmForm.all.new_datadeterminodeobra.DataValue = DataTerminoObra;
    crmForm.all.new_datadeterminodeobra.SetFocus();
}
else 
{
    DataTerminoObra = crmForm.all.new_datadeterminodeobra.DataValue;
    crmForm.CalculaDatasEngenharia();
}
