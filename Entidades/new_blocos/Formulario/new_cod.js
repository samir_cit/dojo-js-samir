﻿function TrataCampo(oField) {
    if (typeof (oField) != "undefined" && oField != null) {
        if (oField.DataValue != null) {
            var sTmp = oField.DataValue.replace(/[^0-9]/g, "");

            if (sTmp.length != 19) {
                oField.DataValue = null;
                alert("Centro de custo fora da faixa 19 dígitos sequenciais! Ex: 004 001 06 30151 001 001");
            }
            else {
                oField.DataValue = sTmp.substr(0, 3) + "." + sTmp.substr(3, 3) + "." + sTmp.substr(6, 2) + "." + sTmp.substr(8, 5) + "." + sTmp.substr(13, 3) + "." + sTmp.substr(16, 3);
            }
        }
    }

}

//Criar picklist de listas.
var xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
var codSel = (crmForm.all.new_cod != null) ? crmForm.all.new_cod.DataValue : "";
crmForm.all.new_cod.parentElement.innerHTML = "<span id='new_cod' ><b>Aguarde...</b></span>";

var url = "/MrvWeb/RetornaCentroCusto/Default.aspx?blocoid=" + crmForm.ObjectId + "&codselecionado=" + codSel + " ";
xmlHttp.open("GET", url, true);
xmlHttp.onreadystatechange = function() {
    if (xmlHttp.readyState == 4) {
        if (xmlHttp.status == 200) {
            var strRetorno = xmlHttp.responseText;
            if (strRetorno.toString().indexOf("*N*", 0) >= 0) {
                crmForm.all.new_cod.parentElement.innerHTML = "<span id='new_cod'><b>" + strRetorno.toString().replace("*N*", "") + "</b></span>";
            }
            else {
                var aRetorno = strRetorno.split("------|*|------");
                crmForm.all.new_cod.parentElement.innerHTML = aRetorno[0];

                if (aRetorno[0] != null && aRetorno[0] != "") {
                    crmForm.all.new_cod.DataValue = aRetorno[1];
                    crmForm.all.new_cod.DefaultValue = aRetorno[1];
                    TrataCampo(crmForm.all.new_cod);
                }
            }
        }
    }
}

xmlHttp.send(null);
