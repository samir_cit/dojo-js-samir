﻿/*
Verifica se terá Re-Calculo na Data Duração da Obra
Verifica se terá Re-Calculo na Data Atraso da Obra 
Atualiza a Data Prevista do Contrato do Cliente
Re-Calcula Datas Engenharia
Manipula o Motivo de Alteração do Cronograma Obra - Atual para os campos previstos do cronograma
*/
crmForm.VerificaDuracaoObra();
crmForm.VerificaAtrasoObra();
crmForm.AtualizaDataTerminoObra();
crmForm.CalculaDatasEngenharia();
crmForm.TrataDatasPrevistasDoCronograma();