﻿if (crmForm.all.new_atua_dt_atualizada_habitese.DataValue) 
{
    // Atualização Automática
    crmForm.all.new_data_prev_protocolo_atu_hab.Disabled = true;
    if (AtualizaDatas) 
        crmForm.CalculaDatasEngenharia();
}
else 
{
    // Atualização Manual
    crmForm.all.new_data_prev_protocolo_atu_hab.Disabled = false
}