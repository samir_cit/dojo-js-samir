﻿crmForm.all.new_total.DataValue = crmForm.all.new_reaaregularizarm.DataValue * crmForm.all.new_valorpm.DataValue;

if (crmForm.all.new_issdomunicpio.DataValue != null) {
    crmForm.all.new_porcentagem5.DataValue = (crmForm.all.new_issdomunicpio.DataValue/100) * crmForm.all.new_totalnfservios.DataValue; 
    
    if ((crmForm.all.new_total.DataValue != null) && (crmForm.all.new_total.DataValue != 0)) {
        crmForm.all.new_issdomunicpio01.DataValue = (crmForm.all.new_total.DataValue*crmForm.all.new_issdomunicpio.DataValue)/100;
        crmForm.all.new_total2.DataValue = crmForm.all.new_issdomunicpio01.DataValue - crmForm.all.new_porcentagem5.DataValue;
        crmForm.all.new_totalapagar.DataValue = crmForm.all.new_total2.DataValue - (crmForm.all.new_modeobraprpriafolha.DataValue + crmForm.all.new_estagiariosfolha.DataValue);
    }
    else {
        crmForm.all.new_issdomunicpio01.DataValue = null;
        crmForm.all.new_total2.DataValue = null;
        crmForm.all.new_totalapagar.DataValue = null;
    }  
}
else {
    crmForm.all.new_porcentagem5.DataValue = null;
    crmForm.all.new_issdomunicpio01.DataValue = null;
    crmForm.all.new_total2.DataValue = null;
    crmForm.all.new_totalapagar.DataValue = null;
}
