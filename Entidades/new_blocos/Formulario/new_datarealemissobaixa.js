﻿// Se não informou Data Real Emissão, desabilita o automático/manual das datas atualizadas da Averbação

if (crmForm.all.new_datarealemissobaixa.DataValue == null)
{
    crmForm.all.new_atua_dt_protocolo_atualizada_averb.Disabled = true;
    crmForm.all.new_atua_dt_protocolo_atualizada_averb.DataValue = true;
    crmForm.all.new_data_prev_protocolo_atu_averb.Disabled = true;
    
    crmForm.all.new_atua_dt_averbacao_atualizada.Disabled = true;
    crmForm.all.new_atua_dt_averbacao_atualizada.DataValue = true;
    crmForm.all.new_data_prev_averbacao_atu.Disabled = true;
}
else 
{
    new_atua_dt_averbacao_atualizada.Disabled = false;
    new_atua_dt_protocolo_atualizada_averb.Disabled = false;
}

if (AtualizaDatas)
    crmForm.CalculaDatasEngenharia();
