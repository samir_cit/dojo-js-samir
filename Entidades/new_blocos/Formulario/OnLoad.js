﻿formularioValido = true;
changedAlteracao = false;
nomeUsuarioLogado = "";
idUsuarioLogado = "";

TipoEntidade = {
    BLOCO: 1,
    MODULO_VILLAGE: 2
}
TipoLogCampos = {
    ELEGIBILIDADE: 1,
    DATA_PREVISTA_ENTREGA_CHAVES: 2
}

try {
    Equipe = {
        ENG_KIT_ACABAMENTO: "ENG_Kit_Acabamento",
        ADMINISTRADORES: "Administradores",
        NUCLEO_NOTIFICACAO: "Núcleo - Notificação",
        NUCLEO_NIAC: "Núcleo - NIAC",
        NUCLEO_ENCANTE: "Núcleo - Encante",
        NUCLEO_CONTROLE_ESTRATEGICO: "Núcleo - Controle Estratégico",
        DESENV_IMOBILIARIO: "Desenv.Imobiliário",
        ENGENHARIA_PLANEJAMENTO: "Engenharia - Planejamento",
        COORDENAÇÃO_KIT_ACABAMENTO: "Coordenação - Kit Acabamento",
        ENGENHARIA_PRAZOS: "Engenharia - Prazos",
        BLOCO_VALOR_GLOBAL_DE_VENDAS: "Bloco - Valor Global de Vendas",
        AT_PRAZOS: "AT_Prazos",
        AT_PRAZOS_GERENCIAL: "AT_Prazos_Gerencial",
        ENTREGA_CHAVES_BLOCO: "Entrega de chave bloco"

    }

    var atributosDataLog = new Array("new_data_1_alteracao_log", "new_data_2_alteracao_log", "new_data_3_alteracao_log", "new_data_4_alteracao_log", "new_data_5_alteracao_log", "new_data_6_alteracao_log", "new_data_7_alteracao_log", "new_data_8_alteracao_log", "new_data_9_alteracao_log", "new_data_10_alteracao_log");
    var atributosDatasAlteracoes = new Array("new_1alterao", "new_2alterao", "new_3alterao", "new_4alterao", "new_5alterao", "new_6alterao", "new_7alterao", "new_8alterao", "new_9alterao", "new_10alterao");
    crmForm.Log = function(atributo) {
        //achar posição no array
        var posicao = -1;
        for (var index = 0; index < atributosDatasAlteracoes.length; index++) {
            if (atributo == atributosDatasAlteracoes[index]) {
                posicao = index;
                break;
            }
        }

        if (eval("crmForm.all." + atributosDatasAlteracoes[posicao]).IsDirty) {
            eval("crmForm.all." + atributosDataLog[posicao]).DataValue = new Date();
            eval("crmForm.all." + atributosDataLog[posicao]).ForceSubmit = true;
        }
    }

    crmForm.EscondeCampo = function(nomeCampo, esconde) {
        var tipo = "";
        if (esconde)
            tipo = "none";

        var atributo_c = eval("crmForm.all." + nomeCampo + "_c");
        var atributo_d = eval("crmForm.all." + nomeCampo + "_d");
        atributo_c.style.display = tipo;
        atributo_d.style.display = tipo;
    }

    crmForm.DataModificacao = function(campo, atributos) {
        var tmp = atributos.split(";");
        var tipo = "";

        if (eval(campo).DataValue == true)
            tipo = "";
        else
            tipo = "none";

        for (var index = 0; index < tmp.length; index++) {
            document.getElementById(tmp[index] + "_c").style.display = tipo;
            document.getElementById(tmp[index] + "_d").style.display = tipo;
        }
    }
    //grava o usuário no respectivo numero do campo de alteração
    crmForm.GravaUsuario = function(campoAlteracao, campoUsuario) {

        if (campoAlteracao.DataValue && changedAlteracao)
            crmForm.GravarUsuarioLogado(campoUsuario);
    }

    crmForm.CalculaMeses = function(dtInicio, dtFim) {
        dif = dtFim.getTime() - dtInicio.getTime();
        dia = 1000 * 60 * 60 * 24;
        diaDif = dif / dia;
        return Math.round(Math.round(diaDif) / 30);
    }

    crmForm.GuardaHistorico = function(campoHistorico) {
        if (crmForm.all.new_entrega_chaves_pos_assinatura.DataValue != null)
            eval("crmForm.all." + campoHistorico).DataValue = crmForm.all.new_entrega_chaves_pos_assinatura.DataValue;
    }

    crmForm.VerificaDuracaoObra = function() {
        if ((crmForm.all.new_data_aceite_at.DataValue != null) &&
			(crmForm.all.new_datainiciocronog.DataValue != null)) {
            crmForm.all.new_duracaodaobra.DataValue = crmForm.CalculaMeses(crmForm.all.new_datainiciocronog.DataValue, crmForm.all.new_data_aceite_at.DataValue);
        } else
            crmForm.all.new_duracaodaobra.DataValue = null;
    }

    crmForm.VerificaAtrasoObra = function() {
        if ((crmForm.all.new_datafimcronog.DataValue != null) &&
			(crmForm.all.new_dataprimeirocontratocliente.DataValue != null) &&
			(crmForm.all.new_datafimcronog.DataValue > crmForm.all.new_dataprimeirocontratocliente.DataValue)) {
            crmForm.all.new_atraso.DataValue = crmForm.CalculaMeses(crmForm.all.new_dataprimeirocontratocliente.DataValue, crmForm.all.new_datafimcronog.DataValue);
            crmForm.all.new_atraso_c.style.visibility = 'visible';
            crmForm.all.new_atraso_d.style.visibility = 'visible';
        } else {
            crmForm.all.new_atraso.DataValue = null;
            crmForm.all.new_atraso_c.style.visibility = 'hidden';
            crmForm.all.new_atraso_d.style.visibility = 'hidden';
        }
    }

    // Se for atualização automática, atualiza a Data Prevista de Término da Obra
    crmForm.AtualizaDataTerminoObra = function() {
        if (!crmForm.all.new_atua_dt_termino_obra.DataValue)
            return;

        var Data = crmForm.all.new_10alterao.DataValue != null
			 ? crmForm.all.new_10alterao.DataValue
			 : crmForm.all.new_9alterao.DataValue != null
			 ? crmForm.all.new_9alterao.DataValue
			 : crmForm.all.new_8alterao.DataValue != null
			 ? crmForm.all.new_8alterao.DataValue
			 : crmForm.all.new_7alterao.DataValue != null
			 ? crmForm.all.new_7alterao.DataValue
			 : crmForm.all.new_6alterao.DataValue != null
			 ? crmForm.all.new_6alterao.DataValue
			 : crmForm.all.new_5alterao.DataValue != null
			 ? crmForm.all.new_5alterao.DataValue
			 : crmForm.all.new_4alterao.DataValue != null
			 ? crmForm.all.new_4alterao.DataValue
			 : crmForm.all.new_3alterao.DataValue != null
			 ? crmForm.all.new_3alterao.DataValue
			 : crmForm.all.new_2alterao.DataValue != null
			 ? crmForm.all.new_2alterao.DataValue
			 : crmForm.all.new_1alterao.DataValue != null
			 ? crmForm.all.new_1alterao.DataValue
			 : crmForm.all.new_dataprimeirocontratocliente.DataValue;

        DataTerminoObra
			 = crmForm.all.new_datadeterminodeobra.DataValue
			 = Data;
    }

    function AlteraStatusBloco() {
        //Data atual
        var dataFutura = new Date();
        //Verificação do gênero do bloco
        var isBlocoTorre = ((crmForm.all.new_tipo.DataValue == 1) || (crmForm.all.new_tipo.DataValue == 2));

        if (isBlocoTorre) //É bloco ou torre
        {
            //Soma-se 6 mêses a data atual.
            dataFutura.setMonth(dataFutura.getMonth() + 6)
        } else //É casa
        {
            //Soma-se 4 mêses a data atual.
            dataFutura.setMonth(dataFutura.getMonth() + 4)
        }

        //Casa, torre ou bloco está pronto
        if (crmForm.all.new_data_previsao_entrega.DataValue != null && crmForm.all.new_data_previsao_entrega.DataValue <= Date()) {
            //Ajustar data de início e fim.

            //Status de pronto
            crmForm.all.new_statusdobloco.DataValue = 1;
        }
        //Casa, torre ou bloco está quase pronto(data de entrega é maior que data atual mas é menor se contar 6 mêses)
        else if (crmForm.all.new_data_previsao_entrega.DataValue != null && crmForm.all.new_data_previsao_entrega.DataValue > Date()) {
            if (crmForm.all.new_data_previsao_entrega.DataValue < dataFutura) {
                //Ajustar data de início e fim.

                //Status de semi-pronto
                crmForm.all.new_statusdobloco.DataValue = 2;
            }
        }
        //Casa, torre ou bloco está quase pronto(data de entrega levará mais que 6 mêses a contar da data atual)
        else if (crmForm.all.new_data_previsao_entrega.DataValue != null && crmForm.all.new_data_previsao_entrega.DataValue > dataFutura) {
            //Ajustar data de início e fim.

            //Status em construção
            crmForm.all.new_statusdobloco.DataValue = 3;
        }
    }

    if (crmForm.FormType == 1)
        crmForm.all.new_statusdobloco.DataValue = '3';

    if (crmForm.FormType == 2) {
        crmForm.all.new_existealterao.FireOnChange();
        AlteraStatusBloco();
    }

    //Desabilita campo nome do modulo caso contenha Histórico de cargas
    crmForm.all.new_nomedobloco.Disabled = (crmForm.all.new_nomedobloco.DataValue != null && crmForm.all.new_nomedobloco.DataValue.indexOf("Histórico de Carga") >= 0);

    //Adiciona Script para ser usado no click do botão
    var elm = document.createElement("script");
    elm.src = "/_static/_grid/cmds/util.js";
    document.appendChild(elm);

    crmForm.EscondeCampo("new_name", true);
    crmForm.all.new_datadetrminodaobra.Disabled = true;
    crmForm.EscondeCampo("new_data_previsao_entrega", true);

    changedAlteracao = false;

    crmForm.all.new_existealterao.FireOnChange();
    crmForm.all.new_2alteracao.FireOnChange();
    crmForm.all.new_3alteracao.FireOnChange();
    crmForm.all.new_4alteracao.FireOnChange();
    crmForm.all.new_5alteracao.FireOnChange();
    crmForm.all.new_6alteracao.FireOnChange();
    crmForm.all.new_7alteracao.FireOnChange();
    crmForm.all.new_8alteracao.FireOnChange();
    crmForm.all.new_9alteracao.FireOnChange();
    crmForm.all.new_10alteracao.FireOnChange();
    crmForm.all.new_possuiiss.FireOnChange();

    changedAlteracao = true;

    if (crmForm.all.new_pkl_elevador.DataValue != null && crmForm.all.new_pkl_elevador.DataValue == 0) {
        document.getElementById("new_laudotecnicodeinstalaoeletrica_d").parentNode.parentNode.style.display = "none";
        document.getElementById("new_obselevador_d").parentNode.parentNode.style.display = "none";
    } else {
        document.getElementById("new_laudotecnicodeinstalaoeletrica_d").parentNode.parentNode.style.display = "inline";
        document.getElementById("new_obselevador_d").parentNode.parentNode.style.display = "inline";
    }

    //Desabiltar seção -----STATUS DO HABITE-SE-------------------------------------------
    if (crmForm.all.new_statushabitese.DataValue != null && crmForm.all.new_statushabitese.DataValue == 2)
        document.getElementById("new_novadatadevistoria_d").parentNode.parentNode.style.display = "inline";
    else
        document.getElementById("new_novadatadevistoria_d").parentNode.parentNode.style.display = "none";

    // Atualiza o campo 'Atualização Automática'
    if (crmForm.all.new_atua_dt_termino_obra.DataValue == null)
        crmForm.all.new_atua_dt_termino_obra.DataValue = true;

    if (crmForm.all.new_atua_dt_atualizada_habitese.DataValue == null)
        crmForm.all.new_atua_dt_atualizada_habitese.DataValue = true;

    if (crmForm.all.new_atua_dt_emis_atualizada_hab.DataValue == null)
        crmForm.all.new_atua_dt_emis_atualizada_hab.DataValue = true;

    if (crmForm.all.new_atua_dt_protocolo_atualizada_averb.DataValue == null)
        crmForm.all.new_atua_dt_protocolo_atualizada_averb.DataValue = true;

    if (crmForm.all.new_atua_dt_averbacao_atualizada.DataValue == null)
        crmForm.all.new_atua_dt_averbacao_atualizada.DataValue = true;

    if (crmForm.all.new_atua_dt_emis_atualizada_cnd.DataValue == null)
        crmForm.all.new_atua_dt_emis_atualizada_cnd.DataValue = true;

    if (crmForm.all.new_atua_dt_protocolo_atualizada_cnd.DataValue == null)
        crmForm.all.new_atua_dt_protocolo_atualizada_cnd.DataValue = true;

    AtualizaDatas = false;
    crmForm.all.new_datarealemissobaixa.FireOnChange();
    crmForm.all.new_atua_dt_termino_obra.FireOnChange();
    crmForm.all.new_atua_dt_atualizada_habitese.FireOnChange();
    crmForm.all.new_atua_dt_emis_atualizada_hab.FireOnChange();
    crmForm.all.new_atua_dt_protocolo_atualizada_averb.FireOnChange();
    crmForm.all.new_atua_dt_averbacao_atualizada.FireOnChange();
    crmForm.all.new_atua_dt_emis_atualizada_cnd.FireOnChange();
    crmForm.all.new_atua_dt_protocolo_atualizada_cnd.FireOnChange();
    AtualizaDatas = true;

    //script para aparecer a data de entrega d material, cas a empresa esteja contratada----
    crmForm.all.new_empresacontratada.FireOnChange();

    //script para mostrar status do -- Doc Elevador------------------------------------
    crmForm.all.new_laudotecnicodeinstalaoeletrica.FireOnChange();

    //script para mostrar status do doc INSS------------------------------------
    if ((crmForm.all.new_copiaautenticadadohabitese.DataValue == false) ||
		(crmForm.all.new_guiaautenticadagps.DataValue == false) ||
		(crmForm.all.new_copiadocei.DataValue == false) ||
		(crmForm.all.new_pesquisaderestries.DataValue == false) ||
		(crmForm.all.new_projetoaprovado.DataValue == false) ||
		(crmForm.all.new_laudodoengenheiro.DataValue == false) ||
		(crmForm.all.new_copiaprojetoaprovado.DataValue == false) ||
		(crmForm.all.new_statusdapesquisa.DataValue == 1)) {
        crmForm.all.new_statusdocinss.DataValue = 1;
    } else {
        crmForm.all.new_statusdocinss.DataValue = 2;
    }

    // GUIA02 - DURAÇÃO DE OBRA-o--------------------
    crmForm.all.new_duracaodaobra.readOnly = true;

    //GUIA 03 - ISS------------------------------------------
    crmForm.all.new_reamdiadasunidadesm.readOnly = true;
    crmForm.all.new_reaaregularizarm.readOnly = true;
    crmForm.all.new_total.readOnly = true;
    crmForm.all.new_issdomunicpio01.readOnly = true;
    crmForm.all.new_porcentagem5.readOnly = true;
    crmForm.all.new_total2.readOnly = true;
    crmForm.all.new_totalapagar.readOnly = true;

    //GUIA 03 - BOMBEIRO--------------------------------
    crmForm.all.new_areadobloco.readOnly = true;

    //GUIA 03 - STATUS
    crmForm.all.new_statusdoc.Disabled = true;
    crmForm.all.new_statusart.Disabled = true;
    crmForm.all.new_statusdocinss.Disabled = true;

    //----Ocultar Guias e campos-------------------------------------------//
    if (crmForm.all.new_tipo.DataValue == 3) {
        crmForm.all.tab1Tab.style.display = "none";
        crmForm.all.tab2Tab.style.display = "none";
        crmForm.all.tab3Tab.style.display = "none";
        crmForm.EscondeCampo("new_nmerodepavimentos", true);
        crmForm.EscondeCampo("new_nmerodeunidadesporpavimento", true);
        crmForm.EscondeCampo("new_tipo_acabamento", true);
        crmForm.EscondeCampo("new_exibir_habitese", true);
        crmForm.all.new_ntotal_unidades_bloco.Disabled = false;
    } else {
        crmForm.all.tab1Tab.style.display = "inline";
        crmForm.all.tab2Tab.style.display = "inline";
        crmForm.all.tab3Tab.style.display = "inline";
        crmForm.EscondeCampo("new_nmerodepavimentos", false);
        crmForm.EscondeCampo("new_nmerodeunidadesporpavimento", false);
        crmForm.EscondeCampo("new_tipo_acabamento", false);
        crmForm.EscondeCampo("new_exibir_habitese", false);
    }

    //apagar o campo de cálculo de atraso na entrega de obra(ebgenharia), caso exista------------------//
    crmForm.EscondeCampo("new_atraso", crmForm.all.new_atraso.DataValue == null);

    //Ocultar sessão de mapeamento-------------------------------------------------------------------//
    document.getElementById("new_mapeamento_tipologia_d").parentNode.parentNode.style.display = "none";
    crmForm.all.new_tipo.FireOnChange();

    //função para desbloquear alteração do tipo do bloco-------------------------------------------------//
    crmForm.all.new_tipo.Disabled = (crmForm.FormType != 1 && crmForm.all.new_tipo.DataValue != null);

    //Guarda a data de início 1 cronograma
    if (crmForm.all.new_dt_inicio_1cronog_previsto.DataValue != null)
        crmForm.all.new_data_inicio_primeiro_cronog.DataValue = crmForm.all.new_dt_inicio_1cronog_previsto.DataValue;

    if (crmForm.all.new_dt_fim_1cronog_previsto.DataValue != null)
        crmForm.all.new_data_fim_primeiro_cronog.DataValue = crmForm.all.new_dt_fim_1cronog_previsto.DataValue;

    //Esconde campo--------------------------------------------------------------------//
    crmForm.EscondeCampo("new_data_inicio_primeiro_cronog", true);
    crmForm.EscondeCampo("new_data_fim_primeiro_cronog", true);

    //Inicializar campos zerados caso estejam nulos-------------------------------------------------//
    if (crmForm.all.new_ntotal_unidades_bloco.DataValue == null)
        crmForm.all.new_ntotal_unidades_bloco.DataValue = 0;

    if (crmForm.all.new_totaldeunidadesgeradas.DataValue == null)
        crmForm.all.new_totaldeunidadesgeradas.DataValue = 0;

    //Chama o evento onchange do campo new_cod para construir o picklist
    crmForm.all.new_cod.FireOnChange();

    //Desabilitando campos-----------------------------------------------------------------------------//

    crmForm.all.new_dataprevistadoprotocolo.Disabled = true;
    crmForm.all.new_dataprevistadeemissoiss.Disabled = true;

    crmForm.all.new_dataprevistaprotocolocb.Disabled = true;
    crmForm.all.new_dataprevistadeemissocb.Disabled = true;

    crmForm.all.new_dataprevistaprotocolohab.Disabled = true;
    crmForm.all.new_dataprevistadeemissohab.Disabled = true;

    crmForm.all.new_dataprevistaprotocolocnd.Disabled = true;
    crmForm.all.new_dataprevistadeemissocnd.Disabled = true;

    crmForm.all.new_dt_prev_entrega_material.Disabled = true;

    crmForm.all.new_dataprevistaentradaprotocolo.Disabled = true;
    crmForm.all.new_dataprevistadeaverbao.Disabled = true;

    crmForm.all.new_dataprevistainicio.Disabled = true;
    crmForm.all.new_dataprevistadeconcluso.Disabled = true;
    crmForm.all.new_data_prev_inicio_proc_atu.Disabled = true;
    crmForm.all.new_data_prev_conclusao_proc_atu.Disabled = true;

    crmForm.UsuarioPertenceEquipe = function(nomeEquipe) {
        var cmd = new RemoteCommand("MrvService", "UserContainsTeams", "/MRVCustomizations/");

        cmd.SetParameter("teams", nomeEquipe);

        var resultado = cmd.Execute();

        return resultado.Success ? resultado.ReturnValue.Mrv.Found : false;
    }

    crmForm.GravarUsuarioLogado = function(objeto) {
        if (nomeUsuarioLogado == "" && idUsuarioLogado == "") {
            var cmd = new RemoteCommand("MrvService", "ObterDadosUsuario", "/MRVCustomizations/");

            var resultado = cmd.Execute();
            if (resultado.Success) {
                nomeUsuarioLogado = resultado.ReturnValue.Mrv.Nome;
                idUsuarioLogado = resultado.ReturnValue.Mrv.Id;

            }
        }
        objeto.DataValue = nomeUsuarioLogado;
    }

    //Desabilitar o campo da tabela de prazo para quem nao pertence a equipe de Engenharia-Planejamneto
    if (!crmForm.UsuarioPertenceEquipe(Equipe.ENGENHARIA_PLANEJAMENTO)) {
        crmForm.all.new_atua_dt_termino_obra.Disabled = true;
        crmForm.all.new_statusgeral.Disabled = true;
        crmForm.all.new_entrega_chaves_pos_assinatura.Disabled = true; //xxxxxxx
        crmForm.all.new_dt_inicio_1cronog_previsto.Disabled = true;
        crmForm.all.new_dt_fim_1cronog_previsto.Disabled = true;
        crmForm.all.new_duracao_obra_1cronog.Disabled = true;
        crmForm.all.new_datainiciocronog.Disabled = true;
        crmForm.all.new_datafimcronog.Disabled = true;
        crmForm.all.new_duracaodaobra.Disabled = true;
        crmForm.all.new_datafimcronogreal.Disabled = true;
        crmForm.all.new_duracaodaobrareal.Disabled = true;
        crmForm.all.new_observaodaobra.Disabled = true;
        crmForm.all.new_dataprimeirocontratocliente.Disabled = true;
        crmForm.all.new_atraso.Disabled = true;
        crmForm.all.new_existealterao.Disabled = true;
        crmForm.all.new_1alterao.Disabled = true;
        crmForm.all.new_1observao.Disabled = true;
        crmForm.all.new_2alteracao.Disabled = true;
        crmForm.all.new_2alterao.Disabled = true;
        crmForm.all.new_2observao.Disabled = true;
        crmForm.all.new_3alteracao.Disabled = true;
        crmForm.all.new_3alterao.Disabled = true;
        crmForm.all.new_3observao.Disabled = true;
        crmForm.all.new_4alteracao.Disabled = true;
        crmForm.all.new_4alterao.Disabled = true;
        crmForm.all.new_4observao.Disabled = true;
        crmForm.all.new_5alteracao.Disabled = true;
        crmForm.all.new_5alterao.Disabled = true;
        crmForm.all.new_5observao.Disabled = true;
        crmForm.all.new_6alteracao.Disabled = true;
        crmForm.all.new_6alterao.Disabled = true;
        crmForm.all.new_6observao.Disabled = true;
        crmForm.all.new_7alteracao.Disabled = true;
        crmForm.all.new_7alterao.Disabled = true;
        crmForm.all.new_7observao.Disabled = true;
        crmForm.all.new_8alteracao.Disabled = true;
        crmForm.all.new_8alterao.Disabled = true;
        crmForm.all.new_8observao.Disabled = true;
        crmForm.all.new_9alteracao.Disabled = true;
        crmForm.all.new_9alterao.Disabled = true;
        crmForm.all.new_9observao.Disabled = true;
        crmForm.all.new_10alteracao.Disabled = true;
        crmForm.all.new_10alterao.Disabled = true;
        crmForm.all.new_10observao.Disabled = true;

        //guia Doc.Encerramento de Obra-----------//
        crmForm.all.new_datadeterminodeobra.Disabled = true;

        //Guia Desmembramento IPTU--------------//
        crmForm.all.new_dataprevistainicio.Disabled = true;
    }

    if (crmForm.UsuarioPertenceEquipe(Equipe.ENG_KIT_ACABAMENTO)) {
        crmForm.all.new_data_limite_aquisicao_kit_acabamento.Disabled = false;
    } else {
        crmForm.all.new_data_limite_aquisicao_kit_acabamento.Disabled = true;
    }

    if (crmForm.UsuarioPertenceEquipe(Equipe.DESENV_IMOBILIARIO)) {
        crmForm.all.new_averbacao_retida_credito.Disabled = false;
        crmForm.all.new_data_liberacao_credito.Disabled = false;
    } else {
        crmForm.all.new_data_liberacao_credito.Disabled = true;
        crmForm.all.new_averbacao_retida_credito.Disabled = true;
    }

    //Desabilitar o campo fora de venda e cancelar lançamento
    crmForm.all.new_fora_venda.Disabled = true;
    crmForm.all.new_cancela_lancamento.Disabled = true;

    // Salva a data de termino da obra, a data prevista protocolo atualizada e a data prevista emissão atualizada
    DataPrevEmissaoAtuHab = crmForm.all.new_data_prev_emissao_atu_hab.DataValue == null ? null : crmForm.all.new_data_prev_emissao_atu_hab.DataValue;
    DataPrevProtocoloAtuHab = crmForm.all.new_data_prev_protocolo_atu_hab.DataValue == null ? null : crmForm.all.new_data_prev_protocolo_atu_hab.DataValue;
    DataTerminoObra = crmForm.all.new_datadeterminodeobra.DataValue == null ? null : crmForm.all.new_datadeterminodeobra.DataValue;

    // Verifica se a diferença entre a data prevista protocolo atualizada e a data prevista emissão atualizada
    // é maior que 30 dias
    crmForm.DifDtProtocoloDtEmissao = function() {
        if (crmForm.all.new_data_prev_emissao_atu_hab.DataValue != null && crmForm.all.new_data_prev_protocolo_atu_hab.DataValue != null) {
            var Data1 = crmForm.all.new_data_prev_emissao_atu_hab.DataValue;
            var Data2 = crmForm.all.new_data_prev_protocolo_atu_hab.DataValue;
            var Dif = Date.UTC(Data1.getYear(), Data1.getMonth(), Data1.getDate(), 0, 0, 0) -
				Date.UTC(Data2.getYear(), Data2.getMonth(), Data2.getDate(), 0, 0, 0);
            Dif = (Dif / 1000 / 60 / 60 / 24);

            if (Dif < 30)
                alert("A diferença entre a Data Prevista de Protocolo Atualizada e a Data Prevista de Emissão Atualizada \ndeve ser maior ou igual a 30 dias!");

            return (Dif >= 30);
        }

        return true;
    }

    crmForm.ConfiguraPerfilAtPrazos = function() {

        crmForm.all.new_data_entrega_chaves.Disabled = true;
        crmForm.all.new_data_aceite_at.Disabled = true;
        crmForm.all.new_datafimcronogreal.Disabled = true;

        if (crmForm.UsuarioPertenceEquipe(Equipe.AT_PRAZOS_GERENCIAL)) {
            crmForm.all.new_datafimcronogreal.Disabled = false;
            //Desabilita campo Entrega de chaves caso o campo Data Aceite At estiver vazio e Desabilita campo Data Aceite caso o campo Data Fim Cronog.Real estiver vazio
            crmForm.all.new_data_aceite_at.Disabled = (crmForm.all.new_datafimcronogreal.DataValue == undefined);
            crmForm.all.new_data_entrega_chaves.Disabled = (crmForm.all.new_data_aceite_at.DataValue == undefined);
        } else {
            if (crmForm.UsuarioPertenceEquipe(Equipe.AT_PRAZOS)) {
                crmForm.all.new_datafimcronogreal.Disabled = (crmForm.all.new_datafimcronogreal.DataValue != undefined);

                if (crmForm.all.new_data_aceite_at.DataValue == undefined) {
                    crmForm.all.new_data_aceite_at.Disabled = (crmForm.all.new_datafimcronogreal.DataValue == undefined);
                } else {
                    crmForm.all.new_data_aceite_at.Disabled = true;
                    if (crmForm.all.new_data_entrega_chaves.DataValue == undefined) {
                        crmForm.all.new_data_entrega_chaves.Disabled = (crmForm.all.new_data_aceite_at.DataValue == undefined);
                    } else {
                        crmForm.all.new_data_entrega_chaves.Disabled = true;
                    }
                }
            } else {
                if (crmForm.UsuarioPertenceEquipe(Equipe.ENGENHARIA_PRAZOS)) {
                    crmForm.all.new_datafimcronogreal.Disabled = (crmForm.all.new_datafimcronogreal.DataValue != undefined);
                }
            }
        }
        if (crmForm.UsuarioPertenceEquipe(Equipe.ENTREGA_CHAVES_BLOCO)) {
            crmForm.all.new_data_entrega_chaves.Disabled = (crmForm.all.new_data_aceite_at.DataValue == undefined);
        }
    }
    // Soma dias a uma data
    crmForm.AddDays = function(Data, NumDias) {
        if (Data == null)
            return null;

        var Dt = new Date(Data.toString());
        return Dt.setDate(Dt.getDate() + NumDias)
    }

    crmForm.CompararMeses = function(Data) {
        if (Data == null)
            return true;
        var year = Data.getFullYear();
        var month = Data.getMonth();
        var dia = Data.getDate();
        var today = new Date();
        if (month == today.getMonth() && year == today.getFullYear() && dia <= today.getDate()) {
            return true;
        } else {
            return false;
        }
    }

    /// Aba:    Doc. Encerramento de Obra
    /// Sessão: Pedido de Baixa de Obra - Habite-se
    crmForm.CalculaBaixaDeObraHabitese = function(dataContratoCliente) {
        if (crmForm.all.new_dataprevistaprotocolohab.DataValue == null)
            crmForm.all.new_dataprevistaprotocolohab.DataValue = crmForm.AddDays(dataContratoCliente, 30);

        if (crmForm.all.new_atua_dt_atualizada_habitese.DataValue)
            crmForm.all.new_data_prev_protocolo_atu_hab.DataValue = crmForm.AddDays(dataContratoCliente, 30);

        if (crmForm.all.new_dataprevistadeemissohab.DataValue == null)
            crmForm.all.new_dataprevistadeemissohab.DataValue = crmForm.AddDays(dataContratoCliente, 90);

        if (crmForm.all.new_atua_dt_emis_atualizada_hab.DataValue)
            crmForm.all.new_data_prev_emissao_atu_hab.DataValue = crmForm.AddDays(dataContratoCliente, 90);
    }

    /// Aba:    Doc. Encerramento de Obra
    /// Sessão: Averbação do Habite-se
    crmForm.CalculaAverbacaoHabitese = function() {
        var DataPrevistaProtocoloAverb = null;
        var DataPrevistaAverbacao = null;

        if (crmForm.all.new_atua_dt_protocolo_atualizada_averb.DataValue) {
            if (crmForm.all.new_datarealemissocnd.DataValue != null) {
                DataPrevistaProtocoloAverb = crmForm.AddDays(crmForm.all.new_datarealemissocnd.DataValue, 15);
            } else if (crmForm.all.new_data_prev_emissao_atu_cnd.DataValue != null) {
                DataPrevistaProtocoloAverb = crmForm.AddDays(crmForm.all.new_data_prev_emissao_atu_cnd.DataValue, 15);
            }
        } else {
            DataPrevistaProtocoloAverb = crmForm.all.new_data_prev_protocolo_atu_averb.DataValue;
        }

        if (crmForm.all.new_atua_dt_averbacao_atualizada.DataValue && DataPrevistaProtocoloAverb != null) {
            DataPrevistaAverbacao = crmForm.AddDays(new Date(DataPrevistaProtocoloAverb), 75);
        }

        if (crmForm.all.new_dataprevistaentradaprotocolo.DataValue == null) {
            crmForm.all.new_dataprevistaentradaprotocolo.DataValue = DataPrevistaProtocoloAverb;
            crmForm.all.new_dataprevistaentradaprotocolo.ForceSubmit = true;
        }

        if (crmForm.all.new_atua_dt_protocolo_atualizada_averb.DataValue) {
            crmForm.all.new_data_prev_protocolo_atu_averb.DataValue = DataPrevistaProtocoloAverb;
            crmForm.all.new_data_prev_protocolo_atu_averb.ForceSubmit = true;
        }
        if (crmForm.all.new_dataprevistadeaverbao.DataValue == null) {
            crmForm.all.new_dataprevistadeaverbao.DataValue = DataPrevistaAverbacao;
            crmForm.all.new_dataprevistadeaverbao.ForceSubmit = true;
        }
        if (crmForm.all.new_atua_dt_averbacao_atualizada.DataValue) {
            crmForm.all.new_data_prev_averbacao_atu.DataValue = DataPrevistaAverbacao;
            crmForm.all.new_data_prev_averbacao_atu.ForceSubmit = true;
        }
    }

    /// Calcula as datas Habite-se/Averbação/Certidão/Laudo/etc.
    crmForm.CalculaDatasEngenharia = function() {
        var DataTerminoObra = crmForm.all.new_datadeterminodeobra.DataValue;

        //protocolo ISS, emissao ISS----------------------------//
        crmForm.all.new_dataprevistadoprotocolo.DataValue = crmForm.AddDays(DataTerminoObra, -30);
        crmForm.all.new_dataprevistadeemissoiss.DataValue = crmForm.all.new_datadeterminodeobra.DataValue;

        //protocolo Bombeiro, emissao Bombeiro--------------//
        crmForm.all.new_dataprevistaprotocolocb.DataValue = crmForm.AddDays(DataTerminoObra, -15);
        crmForm.all.new_dataprevistadeemissocb.DataValue = crmForm.AddDays(DataTerminoObra, 30);

        crmForm.CalculaBaixaDeObraHabitese(DataTerminoObra);

        //salva a data prevista protocolo atualizada e a data prevista emissão atualizada
        DataPrevEmissaoAtuHab = crmForm.all.new_data_prev_emissao_atu_hab.DataValue == null ? null : crmForm.all.new_data_prev_emissao_atu_hab.DataValue;
        DataPrevProtocoloAtuHab = crmForm.all.new_data_prev_protocolo_atu_hab.DataValue == null ? null : crmForm.all.new_data_prev_protocolo_atu_hab.DataValue;
        crmForm.DifDtProtocoloDtEmissao();

        //protocolo CND INSS, emissao CND INSS------------//
        var DataPrevistaProtocoloCND = null;
        var DataPrevistaEmissaoCND = null;

        if (crmForm.all.new_datarealemissobaixa.DataValue != null) {
            DataPrevistaProtocoloCND = crmForm.AddDays(crmForm.all.new_datarealemissobaixa.DataValue, 10);
            DataPrevistaEmissaoCND = crmForm.AddDays(crmForm.all.new_datarealemissobaixa.DataValue, 25);
        } else if (crmForm.all.new_data_prev_emissao_atu_hab.DataValue != null) {
            DataPrevistaProtocoloCND = crmForm.AddDays(crmForm.all.new_data_prev_emissao_atu_hab.DataValue, 10);
            DataPrevistaEmissaoCND = crmForm.AddDays(crmForm.all.new_data_prev_emissao_atu_hab.DataValue, 25);
        }

        if (crmForm.all.new_dataprevistaprotocolocnd.DataValue == null)
            crmForm.all.new_dataprevistaprotocolocnd.DataValue = DataPrevistaProtocoloCND;
        if (crmForm.all.new_dataprevistadeemissocnd.DataValue == null)
            crmForm.all.new_dataprevistadeemissocnd.DataValue = DataPrevistaEmissaoCND;

        if (crmForm.all.new_atua_dt_protocolo_atualizada_cnd.DataValue == true) // atualização automática
            crmForm.all.new_data_prev_protocolo_atu_cnd.DataValue = DataPrevistaProtocoloCND;
        if (crmForm.all.new_atua_dt_emis_atualizada_cnd.DataValue) // atualização automática
            crmForm.all.new_data_prev_emissao_atu_cnd.DataValue = DataPrevistaEmissaoCND;

        //Elaboração material instituição-----------------------//
        var DtPrevEntregaMaterial = null;

        if (crmForm.all.new_datarealprotocolocnd.DataValue != null)
            DtPrevEntregaMaterial = crmForm.AddDays(crmForm.all.new_datarealprotocolocnd.DataValue, 10);
        else if (crmForm.all.new_data_prev_protocolo_atu_cnd.DataValue != null)
            DtPrevEntregaMaterial = crmForm.AddDays(crmForm.all.new_data_prev_protocolo_atu_cnd.DataValue, 10);

        crmForm.all.new_dt_prev_entrega_material.DataValue = DtPrevEntregaMaterial;

        crmForm.CalculaAverbacaoHabitese();

        // Desmembramento IPTU-------------------------------------------//
        var DataPrevistaInicio = null;
        var DataPrevistaConclusao = null;

        if (crmForm.all.new_datarealemissobaixa.DataValue != null) {
            DataPrevistaInicio = crmForm.AddDays(crmForm.all.new_datarealemissobaixa.DataValue, 5);
            DataPrevistaConclusao = crmForm.AddDays(crmForm.all.new_datarealemissobaixa.DataValue, 35);
        } else if (crmForm.all.new_data_prev_emissao_atu_hab.DataValue != null) {
            DataPrevistaInicio = crmForm.AddDays(crmForm.all.new_data_prev_emissao_atu_hab.DataValue, 5);
            DataPrevistaConclusao = crmForm.AddDays(crmForm.all.new_data_prev_emissao_atu_hab.DataValue, 35);
        }

        if (crmForm.all.new_dataprevistainicio.DataValue == null)
            crmForm.all.new_dataprevistainicio.DataValue = DataPrevistaInicio;
        if (crmForm.all.new_dataprevistadeconcluso.DataValue == null)
            crmForm.all.new_dataprevistadeconcluso.DataValue = DataPrevistaConclusao;

        crmForm.all.new_data_prev_inicio_proc_atu.DataValue = DataPrevistaInicio;
        crmForm.all.new_data_prev_conclusao_proc_atu.DataValue = DataPrevistaConclusao;
    }

    crmForm.RemoveBotao = function(nome) {
        var lista = document.getElementsByTagName("LI");
        for (i = 0; i < lista.length; i++) {
            if (lista[i].id.indexOf(nome) >= 0) {
                var o = lista[i].parentElement;
                o.removeChild(lista[i]);
                break;
            }
        }
    }

    //Variável global
    UsuarioAdministrador = null;

    if (UsuarioAdministrador == null)
        UsuarioAdministrador = crmForm.UsuarioPertenceEquipe(Equipe.ADMINISTRADORES);

    if (!UsuarioAdministrador) {
        //Esconde botões
        crmForm.RemoveBotao("EnviarparaoSAP");
    }

    crmForm.DesabilitarFormulario = function(valor) {
        var todosAtributos = document.getElementsByTagName("label");
        for (var i = 0; i < todosAtributos.length; i++) {
            if (todosAtributos[i].htmlFor != "") {
                var atributo = eval("crmForm.all." + todosAtributos[i].htmlFor.replace("_ledit", ""));
                if (atributo != undefined && atributo != null)
                    atributo.Disabled = valor;
            }
        }
    }

    if (crmForm.UsuarioPertenceEquipe(Equipe.COORDENAÇÃO_KIT_ACABAMENTO)) {
        crmForm.DesabilitarFormulario(true);
    }

    /// Desabilita campo no formulário
    crmForm.DesabilitarCampos = function(campos, tipo) {
        var tmp = campos.toString().split(";");

        for (var i = 0; i < tmp.length; i++) {
            var atributo = eval("crmForm.all." + tmp[i]);
            if (atributo != null)
                atributo.Disabled = tipo;
        }
    }

    crmForm.ConfiguraPerfilEngenhariaPrazos = function() {

        if (crmForm.all.new_tipo.DataValue == 3) //Se o bloco for do tipo casa, não configura nada pois os campos não serão visíveis
            return;

        var Campos_TabelaPrazos_CronogramaAtual = "" +
			"new_statusgeral;new_data_previsao_entrega;" +
			"new_dt_inicio_1cronog_previsto;new_dt_fim_1cronog_previsto;new_duracao_obra_1cronog;" +
			"new_datainiciocronog;new_datafimcronog;new_duracaodaobra;" +
			"new_datafimcronogreal;new_duracaodaobrareal;" +
			"new_observaodaobra;new_motivo_alteracao";

        var Campos_TabelaPrazos_PrevisaoTerminoObraAtualizado = "" +
			"new_dataprimeirocontratocliente;new_atraso;" +
			"new_existealterao;new_1alterao;new_1observao;" +
			"new_2alteracao;new_2alterao;new_2observao;" +
			"new_3alteracao;new_3alterao;new_3observao;" +
			"new_4alteracao;new_4alterao;new_4observao;" +
			"new_5alteracao;new_5alterao;new_5observao;" +
			"new_6alteracao;new_6alterao;new_6observao;" +
			"new_7alteracao;new_7alterao;new_7observao;" +
			"new_8alteracao;new_8alterao;new_8observao;" +
			"new_9alteracao;new_9alterao;new_9observao;" +
			"new_10alteracao;new_10alterao;new_10observao";

        var Campos_DocEncerramentoObra_PedidoHabiteSe = "new_data_prev_emissao_atu_hab;new_atua_dt_emis_atualizada_hab;new_datarealemissobaixa;new_possuicertidobaixaehabitese;new_datadeterminodeobra;new_atua_dt_termino_obra;new_dataprevistaprotocolohab;new_data_prev_protocolo_atu_hab;new_atua_dt_atualizada_habitese;new_datarealprotocolobaixa;new_num_protocolo_habitese;new_obs_habitese";

        var Campos_Contrato_Cliente = "new_observacao_alteracao"

        //Verifica se usuário logado pertence a equipe 'Engenharia - Prazos'
        var perfilEngenhariaPrazos = crmForm.UsuarioPertenceEquipe(Equipe.ENGENHARIA_PRAZOS);
        var perfilEngenhariaPrazosAtualizaPrevistaAverbacao = crmForm.UsuarioPertenceEquipe("Altera data Prevista de Averbação");

        crmForm.DesabilitarCampos(Campos_TabelaPrazos_CronogramaAtual, !perfilEngenhariaPrazos);
        crmForm.DesabilitarCampos(Campos_TabelaPrazos_PrevisaoTerminoObraAtualizado, !perfilEngenhariaPrazos);
        crmForm.DesabilitarCampos(Campos_DocEncerramentoObra_PedidoHabiteSe, !perfilEngenhariaPrazos);
        crmForm.DesabilitarCampos(Campos_Contrato_Cliente, !perfilEngenhariaPrazos);

        if (perfilEngenhariaPrazos) { //se for automático a data prevista de emissão atualizada fica desabilitada
            crmForm.all.new_atua_dt_emis_atualizada_hab.FireOnChange();
            crmForm.all.new_atua_dt_termino_obra.FireOnChange();
            crmForm.all.new_atua_dt_atualizada_habitese.FireOnChange();
        }

        if (perfilEngenhariaPrazosAtualizaPrevistaAverbacao) {
            if (crmForm.all.new_datarealemissobaixa.DataValue != null) {
                crmForm.all.new_atua_dt_averbacao_atualizada.Disabled = false;
            }
        }
    }

    crmForm.ConfiguraPerfilValorGlobalDeVendas = function(campo) {
        var fazParteDoperfilValorGlobalDeVendas = crmForm.UsuarioPertenceEquipe(Equipe.BLOCO_VALOR_GLOBAL_DE_VENDAS)
        var Campos_ValorGlobalDeVendas = "new_valor_geral_venda;new_dataprevistadevgv;new_datarealdevgv;new_dataprevisodelanamento;new_datalanamento;new_dt_prev_lancamento_atualiz;new_curvadevendasid";
        crmForm.DesabilitarCampos(Campos_ValorGlobalDeVendas, !fazParteDoperfilValorGlobalDeVendas);
    }

    var motivoAlteracaoInicial = "";
    var dataInicioCronogramaPrevistoInicial = "";
    var dataFimCronogramaPrevistoInicial = "";
    var duracaoObraCronogramaPrevistoInicial = "";
    var teveAlteracaoDeDatas = false;

    crmForm.TrataDatasPrevistasDoCronograma = function() {

        teveAlteracaoDeDatas = true;

        if (crmForm.HouveAlteracaoDasDataPrevistasDoCronograma(false)) {
            //Limpa o motivo atual
            crmForm.all.new_motivo_alteracao.DataValue = "";
            //Obriga informar um novo motivo da alteração
            crmForm.SetFieldReqLevel('new_motivo_alteracao', true);
        } else
            crmForm.SetFieldReqLevel('new_motivo_alteracao', false);
    }

    crmForm.GuardaValoresIniciaisDasDataPrevistasDoCronograma = function() {
        var dataInicioCronogramaPrevista = crmForm.all.new_datainiciocronog.DataValue;
        var dataFimCronogramaPrevista = crmForm.all.new_datafimcronog.DataValue;
        var duracaoDaObraPrevista = crmForm.all.new_duracaodaobra.DataValue;
        var motivoAlteracao = crmForm.all.new_motivo_alteracao.DataValue

        dataInicioCronogramaPrevistoInicial = (dataInicioCronogramaPrevista != null ? dataInicioCronogramaPrevista : "");
        dataFimCronogramaPrevistoInicial = (dataFimCronogramaPrevista != null ? dataFimCronogramaPrevista : "");
        duracaoObraCronogramaPrevistoInicial = (duracaoDaObraPrevista != null ? duracaoDaObraPrevista : "");
        motivoAlteracaoInicial = (motivoAlteracao != null ? motivoAlteracao : "");
    }

    crmForm.HouveAlteracaoDasDataPrevistasDoCronograma = function(registroSendoSalvo) {

        var retorno = false;

        if (dataInicioCronogramaPrevistoInicial.toString() == crmForm.all.new_datainiciocronog.DataValue &&
			dataFimCronogramaPrevistoInicial.toString() == crmForm.all.new_datafimcronog.DataValue &&
			duracaoObraCronogramaPrevistoInicial.toString() == crmForm.all.new_duracaodaobra.DataValue &&
			teveAlteracaoDeDatas) {

            if (registroSendoSalvo) {
                retorno = confirm("Não houve nenhum alteração de data, más deseja manter o Motivo Alteração atual? ");
                event.returnValue = retorno;
            } else {
                crmForm.all.new_motivo_alteracao.selectedIndex = motivoAlteracaoInicial;
                retorno = false;
            }
        } else
            retorno = true;

        return retorno;
    }

    crmForm.ConfiguraLoockupCurvaVendas = function() {
        var oParam = "objectTypeCode=10257&filterDefault=false&attributesearch=new_name&_tipo=2";
        var typeCode = "10257";
        if (ORG_UNIQUE_NAME == "QAS")
            typeCode = "10264";
        crmForm.FilterLookup(crmForm.all.new_curvadevendasid, typeCode, "/MRVWeb/FilterLookup/FilterLookup.aspx", oParam);
    }

    //Valida data aceite, datafimcronog real e data de liberação de entrega de chaves.
    //data1 = data fim
    //data2 = dataaceite
    //data3 = liberação de entrega de chaves

    crmForm.CompararDataAt = function(data1, data2, data3, mensagem, mensagem2) {
        if (data1 == null) {
            data2.DataValue = null;
            data3.DataValue = null;
            data1.SetFocus();
            event.returnValue = false;
            return;
        }
        if ((data2.DataValue != null) && (data2.DataValue < data1.DataValue)) {
            alert(mensagem);
            data2.DataValue = null;
            data3.DataValue = null;
            data2.SetFocus();
            event.returnValue = false;
            return;
        }
        if ((data3.DataValue != null) && (data3.DataValue < data2.DataValue)) {
            alert(mensagem2);
            data3.DataValue = null;
            data3.SetFocus();
            event.returnValue = false;
        }

        if (((data1.DataValue == null) && (data2.DataValue != null)) || ((data2.DataValue == null) && (data3.DataValue != null))) {
            alert(mensagem);
            data2.DataValue = null;
            data3.DataValue = null;
            data2.SetFocus();
            event.returnValue = false;
        }
    }

    crmForm.ConfiguraPerfilValorGlobalDeVendas();
    crmForm.ConfiguraPerfilEngenhariaPrazos();
    crmForm.ConfiguraPerfilAtPrazos();

    function Load() {
        dataAtualEntregaDeChaves = crmForm.all.new_data_prevista_entrega.DataValue;
        if (!crmForm.UsuarioPertenceEquipe(Equipe.NUCLEO_NOTIFICACAO + ";" + Equipe.NUCLEO_NIAC + ";" + Equipe.NUCLEO_ENCANTE + ";" + Equipe.NUCLEO_CONTROLE_ESTRATEGICO)) {
            crmForm.all.new_data_da_visita.Disabled = true;
            crmForm.all.new_visita_marcada.Disabled = true;
            crmForm.all.new_visita_realizada.Disabled = true;
        }

        if (dataInicioCronogramaPrevistoInicial == "" && dataFimCronogramaPrevistoInicial == "" && duracaoObraCronogramaPrevistoInicial == "")
            crmForm.GuardaValoresIniciaisDasDataPrevistasDoCronograma(); //Guarda valores das datas previstas do cronograma
        crmForm.ConfiguraLoockupCurvaVendas();

        crmForm.ExisteUnidadeLancada = function() {
            var cmd = new RemoteCommand("MrvService", "ValidarUnidadeLancadaPorEmpreendimento", "/MRVCustomizations/");
            cmd.SetParameter("empreendimentoId", crmForm.all.new_blocoid.DataValue[0].id);
            var resultado = cmd.Execute();
            if (crmForm.TratarRetornoRemoteCommand(resultado, "ValidarUnidadeLancadaPorEmpreendimento")) {
                return resultado.ReturnValue.Mrv.UnidadeLancada;
            }
            return false;
        }

        crmForm.ValidarPermissaoKitNaObra = function() {
            if (crmForm.ExisteUnidadeLancada()) {
                crmForm.DesabilitarCampos("new_tipo_acabamento", true);
            } else {
                var UsuarioAcabamentoBloco = crmForm.UsuarioPertenceEquipe(Equipe.CO_ACABAMENTO_BLOCO);
                var UsuarioAdministrador = crmForm.UsuarioPertenceEquipe(Equipe.ADMINISTRADORES);

                // Se equipe do usuário for diferente de CO_ACABAMENTO_BLOCO e Administradores o campo será bloqueado
                if (!UsuarioAcabamentoBloco && !UsuarioAdministrador)
                    crmForm.DesabilitarCampos("new_tipo_acabamento", true);
            }
        }
        crmForm.DesabilitaVendaGarantida = function() {
            if (crmForm.FormType == TypeCreate || crmForm.FormType == TypeUpdate) {
                if (crmForm.UsuarioPertenceEquipe(Equipe.COM_VENDA_GARANTIDA)) {
                    crmForm.all.new_venda_garantida.Disabled = false;
                } else {
                    crmForm.all.new_venda_garantida.Disabled = true;
                }
            }
        }
        crmForm.DesabilitaVendaGarantida();
        crmForm.ValidarPermissaoKitNaObra();

        if (crmForm.UsuarioPertenceEquipe(Equipe.ENGENHARIA_PREVISAO_ENTREGA)) {
            crmForm.all.new_data_prevista_entrega.Disabled = false;
        } else {
            crmForm.all.new_data_prevista_entrega.Disabled = true;
        }
        if (crmForm.UsuarioPertenceEquipe(Equipe.ADMINISTRADORES)) {
            crmForm.DesabilitarCampos("new_exibir_habitese", false);
        } else {
            crmForm.DesabilitarCampos("new_exibir_habitese", true);
        }
    }

    function loadScript() {
        var oHead = document.getElementsByTagName('head')[0];
        var oScript = document.createElement('script');
        oScript.type = "text/javascript";
        oScript.src = "/_static/Base.js";

        oScript.onreadystatechange = function() {
            if (this.readyState == 'complete' || this.readyState == 'loaded') {
                Load();
            }
        }
        oHead.appendChild(oScript);
    }

    loadScript();

    crmForm.AtualizarDataPrevistaEntregaChaves = function() {
        try {

            var dataInserida;

            if (dataAtualEntregaDeChaves == null) {
                dataAtualEntregaDeChaves = "";
            }

            if (crmForm.all.new_data_prevista_entrega.DataValue == null) {
                dataInserida = "";
            } else {
                dataInserida = crmForm.all.new_data_prevista_entrega.DataValue;
            }

            if (dataInserida.toString() != dataAtualEntregaDeChaves.toString()) {
                var nomeCampo = crmForm.all.new_data_prevista_entrega.id;
                var valorCampo = crmForm.all.new_data_prevista_entrega.DataValue != null ? crmForm.all.new_data_prevista_entrega.DataValue.toLocaleDateString() : "";
                var valorCampoAnterior = dataAtualEntregaDeChaves != "" ? dataAtualEntregaDeChaves.toLocaleDateString() : dataAtualEntregaDeChaves;
                var idRegistro = crmForm.ObjectId;
                var nomeTecnicoEntidade = crmForm.ObjectTypeName;

                var oParam = "nomeCampo=" + nomeCampo + "&nomeTecnicoEntidade=" + nomeTecnicoEntidade + "&tipoLog=" + TipoLogCampos.DATA_PREVISTA_ENTREGA_CHAVES;

                var url = '/MRVWeb/LogAlteracao/MotivoAlteracaoCampo.aspx?' + oParam;

                var retorno = openStdDlg(url, null, 750, 440);

                if (retorno != null && retorno != undefined) {
                    var cmd = new RemoteCommand("MrvService", "AtualizarDataPrevistaEntregaChavesUnico", "/MRVCustomizations/");
                    cmd.SetParameter("idModuloBloco", idRegistro);
                    cmd.SetParameter("tipo", TipoEntidade.BLOCO);
                    cmd.SetParameter("dataEntregaAtualizada", valorCampo);
                    cmd.SetParameter("justificativaAlteracao", retorno.Justivicativa);
                    cmd.SetParameter("dataEntregaAnterior", valorCampoAnterior);

                    var resultado = cmd.Execute();
                    if (crmForm.TratarRetornoRemoteCommand(resultado, "AtualizarDataPrevistaEntregaChavesUnico")) {
                        if (resultado.ReturnValue.Mrv.dataAtualizada == true) {
                            if (typeof (resultado.ReturnValue.Mrv.mensagem) != "object") {
                                alert(resultado.ReturnValue.Mrv.mensagem);
                            }
                            if (typeof (resultado.ReturnValue.Mrv.mensagemAviso) != "object") {
                                alert(resultado.ReturnValue.Mrv.mensagemAviso);
                            }
                            return true;
                        } else {
                            if (dataAtualEntregaDeChaves == "") {
                                dataAtualEntregaDeChaves = null;
                            }
                            crmForm.all.new_data_prevista_entrega.DataValue = dataAtualEntregaDeChaves;
                            crmForm.all.new_data_prevista_entrega.SetFocus();
                            alert(resultado.ReturnValue.Mrv.mensagem);
                        }
                    } else {
                        if (dataAtualEntregaDeChaves == "") {
                            dataAtualEntregaDeChaves = null;
                        }
                        crmForm.all.new_data_prevista_entrega.DataValue = dataAtualEntregaDeChaves;
                        crmForm.all.new_data_prevista_entrega.SetFocus();
                        alert("Operação cancelada.");
                        event.returnValue = false;
                        return false;
                    }
                } else {
                    alert("Operação cancelada.");
                    event.returnValue = false;
                    return false;
                }
            } else {
                return true;
            }
        } catch (error) {
            alert("Ocorreu um erro no formulário.\n" + error.description);
            event.returnValue = false;
            return false;
        }
    }

} catch (error) {
    formularioValido = false;
    alert("Ocorreu um erro no formulário.\n" + error.description);
}