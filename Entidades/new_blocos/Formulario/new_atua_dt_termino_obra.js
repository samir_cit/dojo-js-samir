﻿if (crmForm.all.new_atua_dt_termino_obra.DataValue != null && 
    crmForm.all.new_atua_dt_termino_obra.DataValue) 
{
    crmForm.AtualizaDataTerminoObra();
    
    if (AtualizaDatas) 
        crmForm.CalculaDatasEngenharia();
}

/* Desabilita se tiver valor o atributo e for true (automático) */
crmForm.all.new_datadeterminodeobra.Disabled =
    (crmForm.all.new_atua_dt_termino_obra.DataValue != null && crmForm.all.new_atua_dt_termino_obra.DataValue);