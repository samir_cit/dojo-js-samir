﻿//Metodo OnSave de Bloco
try {
    if (!crmForm.IsDirty)
    return false;

        if (!formularioValido) {
        alert("Ocorreu um erro no formulário. Impossível salvar.");
        event.returnValue = false;
        return false;
    }
    
    crmForm.HouveAlteracaoDasDataPrevistasDoCronograma(true);

    if (crmForm.all.new_nomedobloco.IsDirty || crmForm.all.new_tipo.IsDirty) {
        //Limita o nome do bloco a 30 caracteres
        var tamNomeBloco = crmForm.all.new_nomedobloco.DataValue.length
        if (tamNomeBloco > 30) {
            alert("O nome do bloco não pode ultrapassar 30 caracteres.\nTamanho atual: " + tamNomeBloco + " caracteres");
            crmForm.all.new_nomedobloco.SetFocus();
            event.returnValue = false;
        }

        //Força o usuário a usar bloco no nome do bloco
        var nomeBloco = crmForm.all.new_nomedobloco.DataValue.toUpperCase();
        var tipo = parseFloat(crmForm.all.new_tipo.DataValue);
        var palavra = "BLOCO";
        switch(tipo)
        {
          case 4: palavra = "QUADRA";break;
          default: palavra = "BLOCO";break;
        }
        if (!(nomeBloco.match(palavra))) {
            alert("O nome do bloco deve conter a palavra " + palavra);
            crmForm.all.new_nomedobloco.SetFocus();
            event.returnValue = false;
        }
    }


    //Alteração de materiais SAP 6.0 (interface INTMM010)---------------------------------//

    if (crmForm.all.new_statusdobloco.IsDirty) {
        var cmd = new RemoteCommand("MaterialService", "AlterarMateriais", "/MRVCustomizations/");
        cmd.SetParameter("strBlocoId", crmForm.ObjectId);
        cmd.SetParameter("strCodStatusBloco", crmForm.all.new_statusdobloco.DataValue);

        var result = cmd.Execute();
        if (result.Succes) {
            if (result.ReturnValue.Mrv.Sucesso)
                alert(result.ReturnValue.Mrv.Resultado);
            else
                alert(result.ReturnValue.Mrv.Mensagem);
        }
    }

    //------------------------------------------------------------------------------------//

    //Verifica se o código do SAP informado já existe em outro Bloco---------------------------------//
    if (crmForm.all.new_codsap.DataValue && crmForm.all.new_codsap.IsDirty) {
        var cmd = new RemoteCommand("MrvService", "ExistCodSAP", "/MRVCustomizations/");
        cmd.SetParameter("entity", "new_blocos");
        cmd.SetParameter("primaryField", "new_blocosid");
        cmd.SetParameter("fieldContainsCodSAP", "new_codsap");
        cmd.SetParameter("codSAP", crmForm.all.new_codsap.DataValue);
        var entityId = crmForm.ObjectId;
        if (!entityId)
            entityId = "";

        cmd.SetParameter("entityId", entityId);

        var result = cmd.Execute();
        if (result.Success) {
            if (result.ReturnValue.Mrv.Found) {
                alert("Código do SAP já está associado a outro bloco.\nAltere o código do SAP e tente salvar novamente!");
                event.returnValue = false;
                return false;
            }
        }
    }

    //Script para atribuir nome ao formulario - campo nome-------------------------------------
    if (crmForm.all.new_blocoid.DataValue != null) {
        crmForm.all.new_name.DataValue = crmForm.all.new_blocoid.DataValue[0].name + " - " + crmForm.all.new_nomedobloco.DataValue;

    }

    //Verifica se o nome do empreendimento é igual ao do Terreno.---------------------------------//
    //Se igual bloqueia o Save.-----------------------------------------------------------------------------//
    if (crmForm.FormType == 1) {
        if (crmForm.all.new_blocoid.DataValue != null) {
            var cmd = new RemoteCommand("MrvService", "IsEqualName", "/MRVCustomizations/");
            cmd.SetParameter("EmpreendimentoId", crmForm.all.new_blocoid.DataValue[0].id);
            cmd.SetParameter("EmpreendimentoName", crmForm.all.new_blocoid.DataValue[0].name);

            var result = cmd.Execute();
            if (result.Success) {
                if (result.ReturnValue.Mrv.Mensagem) {
                    alert(result.ReturnValue.Mrv.Mensagem);
                    event.returnValue = false;
                    return false;
                }
            }
        }
        else {
            alert("Bloco sem Empreendimento relacionado.\nVerifique.");
            event.returnValue = false;
            return false;
        }
    }
    // Transformar caracteres em maiusculos------------------------------------//
    var elm = document.getElementsByTagName("input");
    for (i = 0; i < elm.length; i++) {
        if (elm[i].type == "text")
            if (elm[i].DataValue != null) {
            try {
                elm[i].DataValue = elm[i].DataValue.toUpperCase();
            }
            catch (e) { }
        }
    }
    if (crmForm.UsuarioPertenceEquipe(Equipe.AT_PRAZOS_GERENCIAL)) {
    crmForm.all.new_datafimcronogreal.Disabled = false;
    crmForm.all.new_data_aceite_at.Disabled = false;
    crmForm.all.new_data_entrega_chaves.Disabled = false;
} else {
    if (crmForm.all.new_datafimcronogreal.IsDirty) {
        crmForm.all.new_datafimcronogreal.Disabled = false;
    }
    if (crmForm.all.new_data_aceite_at.IsDirty) {
        crmForm.all.new_data_aceite_at.Disabled = false;
    }
    if (crmForm.all.new_data_entrega_chaves.IsDirty) {
        crmForm.all.new_data_entrega_chaves.Disabled = false;
    }
}

    //Habilitar campos
    //Para desabilitar novos campos, basta inserir o nome do campo no array abaixo.
    function HabilitarCamposSalvar() {
        var atributos = new Array(
                              "new_usuario_1_alteracao", "new_usuario_2_alteracao", "new_usuario_3_alteracao", 
                              "new_usuario_4_alteracao", "new_usuario_5_alteracao", "new_usuario_6_alteracao", 
                              "new_usuario_7_alteracao", "new_usuario_8_alteracao", "new_usuario_9_alteracao", "new_usuario_10_alteracao",
                              "new_dataprevistadoprotocolo", "new_dataprevistadeemissoiss", "new_dataprevistaprotocolocb",
                              "new_dataprevistadeemissocb", "new_dataprevistaprotocolohab", "new_dataprevistadeemissohab",
                              "new_datadeterminodeobra", "new_data_prev_protocolo_atu_hab", "new_data_prev_emissao_atu_hab", "new_dataprevistadeconcluso",
                              "new_dataprevistaprotocolocnd", "new_dataprevistadeemissocnd", "new_dt_prev_entrega_material",
                              "new_dataprevistaentradaprotocolo", "new_dataprevistadeaverbao", "new_dataprevistainicio", "new_fora_venda","new_cancela_lancamento",
                              "new_data_prev_protocolo_atu_cnd", "new_data_prev_emissao_atu_cnd",
                              "new_data_prev_inicio_proc_atu", "new_data_prev_conclusao_proc_atu",
                              "new_data_limite_aquisicao_kit_acabamento"
                              );
        for (var index = 0; index < atributos.length; index++) {
            eval("crmForm.all." + atributos[index]).Disabled = false;
            eval("crmForm.all." + atributos[index]).ForceSubmit = true;
        }
    }


    HabilitarCamposSalvar();
    crmForm.all.new_datadeterminodeobra.Disabled = false;
    crmForm.all.new_datadeterminodeobra.ForceSubmit = true;
    
    if(!crmForm.AtualizarDataPrevistaEntregaChaves())
	{
	    event.returnValue = false;
		return false;
	}

}
catch (error) {
    alert("Ocorreu um erro no formulário.\n" + error.description);
    event.returnValue = false;
    return false;
}