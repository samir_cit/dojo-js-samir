﻿if (!crmForm.UsuarioPertenceEquipe(Equipe.AT_PRAZOS_GERENCIAL) && !crmForm.CompararMeses(crmForm.all.new_data_aceite_at.DataValue)) {
    alert("O Campo Data Aceite da AT deve ser preenchido com data do mês corrente e menor ou igual que data atual, favor corrigir a informação.")
    crmForm.all.new_data_aceite_at.DataValue = null;
    crmForm.all.new_data_entrega_chaves.DataValue = null;
}

//Valida se data aceite está maior ou igual Data Fim cronograma real
crmForm.CompararDataAt(crmForm.all.new_datafimcronogreal,crmForm.all.new_data_aceite_at, crmForm.all.new_data_entrega_chaves, 
"A data informada no campo Data de Aceite da AT deve ser maior ou igual a  data do campo Data Real do Fim do Cronograma",
"A data informada no campo Lib. Entrega de chaves AT deve ser maior ou igual a  data do campo Data de Aceite da AT");

// Se data aceite contem dados habilita campo Data entrega de chaves.
if (crmForm.all.new_data_aceite_at.DataValue != null) {
	crmForm.all.new_data_entrega_chaves.Disabled = false;
} else {
	crmForm.all.new_data_entrega_chaves.Disabled = true;
}

if ((crmForm.all.new_data_aceite_at.DataValue != null) &&
    (crmForm.all.new_datainiciocronogreal.DataValue != null)) {
    crmForm.all.new_duracaodaobrareal.DataValue = crmForm.CalculaMeses(crmForm.all.new_datainiciocronogreal.DataValue, crmForm.all.new_data_aceite_at.DataValue);
}
else {
    crmForm.all.new_duracaodaobrareal.DataValue = null;
}