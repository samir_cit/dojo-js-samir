﻿if(crmForm.all.new_datalanamento.DataValue != null) {
    var cmd = new RemoteCommand("MrvService", "GetEntity", "/MrvCustomizations/");
    cmd.SetParameter("entity", "new_empreendimento");
    cmd.SetParameter("primaryField", "new_empreendimentoid");
    cmd.SetParameter("entityId", crmForm.all.new_blocoid.DataValue[0].id);
    cmd.SetParameter("fieldList", "new_empresa_vendedoraid");

    var Resultado = cmd.Execute();
    if(Resultado.Success) {
        if(typeof Resultado.ReturnValue.Mrv.new_empresa_vendedoraid == "undefined")
        {
            alert("Empreendimento sem empresa promitente");
            crmForm.all.new_datalanamento.DataValue = null;
        }
    }
}

if(crmForm.all.new_datalanamento.DataValue == null)
    crmForm.all.new_cancela_lancamento.DataValue = true;
else    
    crmForm.all.new_cancela_lancamento.DataValue = false;
