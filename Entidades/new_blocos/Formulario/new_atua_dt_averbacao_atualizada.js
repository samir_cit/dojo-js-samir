﻿if (crmForm.all.new_atua_dt_averbacao_atualizada.DataValue) 
{
    // Atualização Automática
    crmForm.all.new_data_prev_averbacao_atu.Disabled = true;
    if (AtualizaDatas) 
        crmForm.CalculaDatasEngenharia();
}
else 
{
    // Atualização Manual
    crmForm.all.new_data_prev_averbacao_atu.Disabled = false
}