﻿//----Ocultar Guias e campos-------------------------------------------//
if (crmForm.all.new_tipo.DataValue == 3) {
    crmForm.all.tab1Tab.style.display = "none";
    crmForm.all.tab2Tab.style.display = "none";
    crmForm.all.tab3Tab.style.display = "none";
    crmForm.EscondeCampo("new_nmerodepavimentos", true);
    crmForm.EscondeCampo("new_nmerodeunidadesporpavimento", true);
    crmForm.EscondeCampo("new_tipo_acabamento", true);

    crmForm.all.new_mapeamento_tipologia.DataValue = 2;
}
else {
    crmForm.all.tab1Tab.style.display = "inline";
    crmForm.all.tab2Tab.style.display = "inline";
    crmForm.all.tab3Tab.style.display = "inline";
    crmForm.EscondeCampo("new_nmerodepavimentos", false);
    crmForm.EscondeCampo("new_nmerodeunidadesporpavimento", false);
    crmForm.EscondeCampo("new_tipo_acabamento", false);

    crmForm.all.new_mapeamento_tipologia.DataValue = 1;
}