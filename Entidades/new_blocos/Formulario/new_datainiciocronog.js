﻿try 
{

    crmForm.all.new_datafimcronog.FireOnChange();
    if (crmForm.all.new_entrega_chaves_pos_assinatura.DataValue == null) {
        //faz o cálculo dos meses para entrega das chaves apenas se o campo for diferente de null
        if (crmForm.all.new_datainiciocronog.DataValue != null) {
            var arrayAtributosBoolean = new Array("new_10alteracao", "new_9alteracao", "new_8alteracao", "new_7alteracao", "new_6alteracao", "new_5alteracao", "new_4alteracao", "new_3alteracao", "new_2alteracao", "new_existealterao");
            var arrayDatas = new Array("new_10alterao", "new_9alterao", "new_8alterao", "new_7alterao", "new_6alterao", "new_5alterao", "new_4alterao", "new_3alterao", "new_2alterao", "new_1alterao");
            var arrayLabelCampo = new Array("10º Alteração", "9º Alteração", "8º Alteração", "7º Alteração", "6º Alteração", "5º Alteração", "4º Alteração", "3º Alteração", "2º Alteração", "1º Alteração");
            var arrayHistoricoMeses = new Array("new_historico_entrega_chaves_10_alteracao", "new_historico_entrega_chaves_9_alteracao", "new_historico_entrega_chaves_8_alteracao", "new_historico_entrega_chaves_7_alteracao", "new_historico_entrega_chaves_6_alteracao", "new_historico_entrega_chaves_5_alteracao", "new_historico_entrega_chaves_4_alteracao", "new_historico_entrega_chaves_3_alteracao");

            var posicao = -1;
            for (var index = 0; index < arrayAtributosBoolean.length; index++) {
                if (eval("crmForm.all." + arrayAtributosBoolean[index]).DataValue == true) {
                    posicao = index;
                    break;
                }
            }

            var dataAlteracao = null;
            //Se posição maior que ou igual a 0 então existe data de alteração. Se não usa a data de primeiro contrato.
            if (posicao >= 0) {
                if (eval("crmForm.all." + arrayDatas[posicao]).DataValue != null) {
                    dataAlteracao = eval("crmForm.all." + arrayDatas[posicao]).DataValue;
                }
                else {
                    alert("O campo " + arrayLabelCampo[posicao] + " está marcado com sim, mas não tem data previsão de término.");
                }
            } else {
                if (crmForm.all.new_dataprimeirocontratocliente.DataValue != null) {
                    dataAlteracao = crmForm.all.new_dataprimeirocontratocliente.DataValue;
                }
                else {
                    alert("Data do primeiro contrato em branco.\nCálculo dos meses para entrega da chave não iniciado.");
                }
            }

            if (dataAlteracao != null) {
                crmForm.all.new_entrega_chaves_pos_assinatura.DataValue = crmForm.CalculaMeses(crmForm.all.new_datainiciocronog.DataValue, dataAlteracao);
            }
        }

        crmForm.TrataDatasPrevistasDoCronograma();
    }   
}
catch (error) 
{
    alert(error.description);
}