﻿try {
    if (!formularioValido) {
        alert("Ocorreu um erro no formulário. Impossível salvar.");
        event.returnValue = false;
        return false;
    }
    
    // Verifica se já existe SLA para este assunto
    if ((crmForm.FormType == 1) && (crmForm.all.new_assuntoid.DataValue != null))
    {
        var cmd = new RemoteCommand('MrvService', 'ObterSLAPorAssunto', '/MRVCustomizations/');
        cmd.SetParameter('AssuntoId', crmForm.all.new_assuntoid.DataValue[0].id);
        var result = cmd.Execute();
        if (result.Success) {
            if (result.ReturnValue.Mrv.Erro) 
            {
                alert(result.ReturnValue.Mrv.Result);
                event.returnValue = false;
                return false;
            }
            else 
            {
                if (result.ReturnValue.Mrv.Existe) 
                {
                    alert("Já existe uma SLA cadastrada para este assunto.");
                    event.returnValue = false;
                    return false;
                }
            }
        }
        else
            alert('Erro na comunicação com o MrvService. Contate o administrador do sistema.');
    }    

} catch (error) 
{
    alert("Ocorreu um erro no formulário.\n" + error.description);
    event.returnValue = false;
    return false;
}