﻿if (crmForm.all.new_assuntoid.DataValue != null) {
    var subjectid = crmForm.all.new_assuntoid.DataValue[0].id;

    var cmd = new RemoteCommand("MrvService", "ObterDadosOcorrencia", "/MRVCustomizations/");
    cmd.SetParameter("assuntoId", subjectid);
    var result = cmd.Execute();
    if (result.Success) {
        if (result.ReturnValue.Mrv.ErrorSoap) {
            alert(result.ReturnValue.Mrv.ErrorSoap);
        }
        else if (result.ReturnValue.Mrv.Error) {
            alert(result.ReturnValue.Mrv.Error);
        }
        else if (result.ReturnValue.Mrv.Mensagem) {
            alert(result.ReturnValue.Mrv.Mensagem);
            crmForm.all.new_assuntoid.DataValue = null;
        }
    }
    else {
        alert("Erro na consulta do WebService. Método: ObterDadosOcorrencia");
    }
}
