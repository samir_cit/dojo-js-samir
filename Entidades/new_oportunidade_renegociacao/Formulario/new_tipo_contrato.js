﻿if (crmForm.all.new_tipo_contrato.DataValue != null &&
    crmForm.all.new_tipo_contrato.DataValue != TipoDeContrato.UNIDADE && crmForm.all.new_tipo_contrato.DataValue != TipoDeContrato.PERMUTA) {
    crmForm.EscondeCampo("new_aditivoid", false);
    crmForm.all.new_aditivoid.DataValue = null;
    crmForm.all.new_aditivoid.FireOnChange();
}
else {
    crmForm.DesabilitarCampos("new_aditivoid", true);
}

crmForm.all.new_accountid.DataValue = null;
crmForm.all.new_contratoid.DataValue = null;
crmForm.all.new_contratoid.FireOnChange();