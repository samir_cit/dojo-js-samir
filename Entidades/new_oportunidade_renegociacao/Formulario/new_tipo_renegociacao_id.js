﻿var oTypeCode = 10132;
var tipoNenegociacaoId = crmForm.all.new_grupo_renegociacao_id.DataValue != null ? crmForm.all.new_grupo_renegociacao_id.DataValue[0].id : "";

/* Se usuário pertencer a Equipe Aditivos e Alterações Contratuais e selecionar o grupo Somente CRM, um parâmetro como true deverá ser passado para o lookup. */
var somenteCRMeEquipeAditivosAlteracaoContratual = crmForm.UsuarioPertenceEquipe("Aditivos e Alterações Contratuais");

var oParam = "objectTypeCode=10132&filterDefault=false&attributesearch=new_name&_new_equipe_aditivos_alteracoes_contratuais=" + somenteCRMeEquipeAditivosAlteracaoContratual + "&_new_grupo_renegociacaoid=" + tipoNenegociacaoId;

crmForm.FilterLookup(crmForm.all.new_tipo_renegociacao_id, oTypeCode, "/MRVWeb/FilterLookup/FilterLookup.aspx", oParam);

crmForm.ConfiguraVisaoFormularioPorTipoDeRenegociacao();

if (crmForm.all.new_tipo_renegociacao_id.DataValue != null) 
{
    crmForm.ConfiguraVisibilidadeCamposPorTipoRenegociacao(crmForm.all.new_tipo_renegociacao_id.DataValue[0].id);

    var mostrarCampoDataEntregaChaves = crmForm.all.new_tipo_renegociacao_id.DataValue[0].name.toUpperCase().indexOf("COM CHAVES") > 0;
    crmForm.EscondeCampo("new_data_entrega_chaves", !mostrarCampoDataEntregaChaves);
}
else 
{
    crmForm.EscondeCampo("new_data_renegociacao_cessao_direito", true);
    crmForm.EscondeCampo("new_nova_previsao_entrega_chaves", true);
}

if (crmForm.Carregado &&
    crmForm.all.new_accountid.DataValue != null &&
    crmForm.all.new_novoclienteid.DataValue != null && 
    crmForm.all.new_accountid.DataValue[0].id != crmForm.all.new_novoclienteid.DataValue[0].id)
{
    crmForm.all.new_novoclienteid.DataValue = null;
}


AtualizarURL();

crmForm.all.new_novoclienteid.FireOnChange();
crmForm.LoadAditivoLookup();

