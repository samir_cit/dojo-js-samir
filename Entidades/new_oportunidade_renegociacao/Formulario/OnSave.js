﻿try {
    
    if (!formularioValido) {
        alert("Ocorreu um erro no formulário. Impossível salvar.");
        event.returnValue = false;
        return false;
    }
    
    /// Método que valida a criação de uma oportunidade para o tipo de renegociação: Aditivo contratual fiador
    function ValidarAditivoContratualFiador(tipoContrato) {
        if (tipoContrato = ! TipoDeContrato.UNIDADE) {
            alert("Para este tipo de renegociação o tipo de contrato deve ser Venda de Unidade.");
            event.returnValue = false;
            return false;
        }
    }
    
    if (crmForm.all.new_grupo_renegociacao_id.DataValue == GrupoRenegociacao.SOMENTE_CRM)
        ValidarAditivoContratualFiador(crmForm.all.new_tipo_contrato.DataValue);
    
    crmForm.all.new_name.Disabled = false;
    crmForm.all.new_tipo_contrato.Disabled = false;
} 
catch (error) {
    alert("Ocorreu um erro no formulário.\n" + error.description);
    event.returnValue = false;
    return false;
}