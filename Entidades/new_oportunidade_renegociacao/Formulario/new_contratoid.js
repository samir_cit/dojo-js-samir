﻿if (crmForm.all.new_contratoid.DataValue != null) {
    var tipo = crmForm.all.new_tipo_contrato.DataValue;
    if (tipo != TipoDeContrato.KITACABEMNTO && tipo != TipoDeContrato.SERVICO) {
        var retorno = crmForm.ObterInformacoesDoContrato(crmForm.all.new_contratoid.DataValue);
        if (retorno != null) {
            crmForm.all.new_possui_kit.DataValue = retorno.ReturnValue.Mrv.PossuiKits;
            crmForm.all.new_possui_box.DataValue = retorno.ReturnValue.Mrv.PossuiBoxes;
            crmForm.all.new_possui_garagem.DataValue = retorno.ReturnValue.Mrv.PossuiGaragens;
        }
    }
}

var tipocontrato = TipoDeContrato.UNIDADE;

if (crmForm.all.new_tipo_contrato.DataValue != null && crmForm.all.new_tipo_contrato.DataValue == TipoDeContrato.PERMUTA) {
    tipocontrato = crmForm.all.new_tipo_contrato.DataValue;
}

var oParam = "objectTypeCode=1011&filterDefault=false&attributesearch=title&_customerid=" + (crmForm.all.new_accountid.DataValue != null ? crmForm.all.new_accountid.DataValue[0].id : "") + "&_tipocontrato=" + tipocontrato;
crmForm.FilterLookup(crmForm.all.new_contratoid, 1010, "/MRVWeb/FilterLookup/FilterLookup.aspx", oParam);

crmForm.ObterAditivoPorContrato();

