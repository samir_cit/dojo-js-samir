﻿crmForm.all.new_tipo_renegociacao_id.DataValue = null;
crmForm.all.new_tipo_renegociacao_id.FireOnChange();

if (crmForm.FormType == TypeUpdate) {
        crmForm.ApresentaTabRelacionamento();
}

if (crmForm.all.new_grupo_renegociacao_id.DataValue != null) 
{
    crmForm.ConfiguraVisaoFormularioPorTipoDeRenegociacao();

    if (grupoRenegociacao.new_codigo_grupo == GrupoRenegociacao.RENEGOCIACAO_KITACABAMENTO) {
        crmForm.all.new_tipo_contrato.DataValue = TipoDeContrato.KITACABEMNTO;
        crmForm.all.new_tipo_contrato.Disabled = true;
    }
    if (crmForm.all.new_grupo_renegociacao_id.DataValue[0].name.toString().indexOf("CRM") >= 0) 
    {
        crmForm.all.new_accountid.Disabled = true;
        crmForm.all.new_contratoid.Disabled = true;
    }
    else if (crmForm.FormType == TypeUpdate && crmForm.ExisteProposta(crmForm.ObjectId)) 
    {
        crmForm.all.new_accountid.Disabled = true;
        crmForm.all.new_contratoid.Disabled = true;
    }
    else 
    {
        crmForm.all.new_accountid.Disabled = false;
        crmForm.all.new_contratoid.Disabled = false;
    }

    /// Cria uma copia do cliente no novo cliente
    crmForm.DefinirMesmoCliente();
}

crmForm.LoadAditivoLookup();