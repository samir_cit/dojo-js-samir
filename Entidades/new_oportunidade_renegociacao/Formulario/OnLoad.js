﻿formularioValido = true;

try {
    StatusCode =
    {
        RASCUNHO: "1",
        GANHA: "3",
        PERDIDA: "4"
    }

    TipoDeContrato =
    {
        UNIDADE: "1",
        GARAGEM: "2",
        BOX: "3",
        KITACABEMNTO: "4",
        SERVICO: "5",
        PERMUTA: "7"
    }

    GrupoRenegociacao =
    {
        RENEGOCIACAO_SALDO_DEVEDOR: "1",
        FINANCIAMENTO_BANCARIO: "2",
        CESSAO_DIREITO_TOTAL: "3",
        CESSAO_DIREITO_PARCIAL: "4",
        SOMENTE_CRM: "5",
        DISTRATO: "6",
        RENEGOCIACAO_TAXAS: "7",
        RENEGOCIACAO_KITACABAMENTO: "8"
    }

    TipoAuditoria =
    {
        PROPOSTA: 1,
        OPORTUNIDADE_RENEGOCIACAO: 2,
        PROPOSTA_CSG: 3
    }

    crmForm.ApresentaCampoDataSap = function() {
        var mostrarCampoDataEntregaChaves =
            crmForm.all.new_tipo_renegociacao_id.DataValue != null &&
            crmForm.all.new_tipo_renegociacao_id.DataValue[0].name.toUpperCase().indexOf("COM CHAVES") > 0;

        crmForm.EscondeCampo("new_data_entrega_chaves", !mostrarCampoDataEntregaChaves);
    }

    crmForm.ConfigurarTitulo = function() {
        crmForm.all.new_name.DataValue = "RENEGOCIAÇÃO";

        if (crmForm.all.new_novoclienteid.DataValue)
            crmForm.all.new_name.DataValue = crmForm.all.new_name.DataValue.concat(" - ", crmForm.all.new_novoclienteid.DataValue[0].name);

        else if (crmForm.all.new_accountid.DataValue)
            crmForm.all.new_name.DataValue = crmForm.all.new_name.DataValue.concat(" - ", crmForm.all.new_accountid.DataValue[0].name);
    }

    crmForm.ConfiguraVisaoFormularioPorTipoDeRenegociacao = function() {
        var codigoGrupo = '';
        var codigoTipoRenegociacao = 0;

        grupoRenegociacao = crmForm.ObterGrupoRenegociacao(crmForm.all.new_grupo_renegociacao_id.DataValue);

        tipoRenegociacao = crmForm.ObterTipoRenegociacao(crmForm.all.new_tipo_renegociacao_id.DataValue);

        if (null != grupoRenegociacao)
            codigoGrupo = grupoRenegociacao.new_codigo_grupo;

        if (null != tipoRenegociacao)
            codigoTipoRenegociacao = parseInt(tipoRenegociacao.new_codigo_tipo_renegociacao);

        var apresentaVisaoParaTrocaDeTitular = (codigoGrupo == GrupoRenegociacao.CESSAO_DIREITO_TOTAL) || (codigoGrupo == GrupoRenegociacao.CESSAO_DIREITO_PARCIAL) || (codigoGrupo == GrupoRenegociacao.RENEGOCIACAO_SALDO_DEVEDOR && (codigoTipoRenegociacao == 3));
        var apresentaVisaoParaEscolhaDeAditivo = crmForm.all.new_tipo_contrato.DataValue == null || crmForm.all.new_tipo_contrato.DataValue == TipoDeContrato.UNIDADE || crmForm.all.new_tipo_contrato.DataValue == TipoDeContrato.PERMUTA;

        crmForm.EscondeCampo("new_novoclienteid", !apresentaVisaoParaTrocaDeTitular);
        crmForm.DesabilitarCampos("new_aditivoid", apresentaVisaoParaEscolhaDeAditivo);

        crmForm.SetFieldReqLevel('new_aditivoid', !apresentaVisaoParaEscolhaDeAditivo);
        crmForm.SetFieldReqLevel('new_novoclienteid', apresentaVisaoParaTrocaDeTitular);
        crmForm.SetFieldReqLevel('new_accountid', true);
        crmForm.SetFieldReqLevel('new_contratoid', true);
        crmForm.SetFieldReqLevel('new_tipo_renegociacao_id', true);
        crmForm.SetFieldReqLevel('new_tipo_contrato', true);
        crmForm.SetFieldReqLevel('new_grupo_renegociacao_id', true);

        crmForm.all.new_name.Disabled = true;

        crmForm.all.tab3Tab.style.display = 'none';

        crmForm.all.tab5Tab.style.display = "none";

        if (crmForm.FormType == TypeUpdate) {
            /// Apresenta campo data sap caso o tipo renegociação for com chaves.
            crmForm.ApresentaCampoDataSap();

            /// tab Kits comentada no projeto melhorias kit
            crmForm.all.tab3Tab.style.display = "none";

            crmForm.ApresentaTabRelacionamento();

            if (crmForm.all.statuscode.DataValue != StatusCode.RASCUNHO) {
                crmForm.DesabilitarFormulario(true);
                crmForm.all.tab2Tab.style.display = 'none';
            }

            if (grupoRenegociacao && grupoRenegociacao.new_codigo_grupo == GrupoRenegociacao.RENEGOCIACAO_KITACABAMENTO) {
                //if (null != contrato)
                //    document.getElementById("IFRAME_Kits_Renegociacao").src = "/mrvweb/kit/listkitsofopportunity.aspx?Id=" + crmForm.ObjectId + "&tipo=";

                crmForm.all.new_tipo_contrato.DataValue = TipoDeContrato.KITACABEMNTO;
                crmForm.all.new_tipo_contrato.Disabled = true;
            }
        }
    }

    crmForm.DefinirMesmoCliente = function() {
        if (crmForm.all.new_accountid.DataValue != null) {
            if (grupoRenegociacao != null) {
                if (grupoRenegociacao.new_codigo_grupo == '3' || grupoRenegociacao.new_codigo_grupo == '4')
                    crmForm.all.new_novoclienteid.DataValue = crmForm.all.new_accountid.DataValue;
            }
        }
    }

    crmForm.ApresentaTabRelacionamento = function() {

        if (null != grupoRenegociacao) {
            /// Tab Relacionamento Cliente
            crmForm.all.tab5Tab.style.display = "block";

            var url = "";

            if (grupoRenegociacao.new_codigo_grupo == GrupoRenegociacao.CESSAO_DIREITO_TOTAL) {
                var oTypeCode = "10116";
                var oSecurity = "65591";
                url = "areas.aspx?oId=%7b" + crmForm.ObjectId.replace('{', '').replace('}', '') + "%7d&oType=" + oTypeCode + "&security=" + oSecurity + "&tabSet=new_new_opor_reneg_relac_oport_reneg";
            }
            else {
                if (crmForm.all.new_contratoid.DataValue) {
                    contrato = crmForm.ObterContratoQueEstaSendoRenegociado(crmForm.all.new_contratoid.DataValue[0].id);
                    url = "/sfa/opps/areas.aspx?oId=%7b" + contrato.new_opportunityid.replace('{', '').replace('}', '') + "%7d&oType=3&security=852023&tabSet=new_opportunity_new_relacoes_do_cliente";
                }
                else
                    alert('Por favor, preencha o contrato que será regenegociado.');
            }

            document.getElementById("IFRAME_relacionamentos").src = url;
        }
    }

    crmForm.ExisteProposta = function(oportunidadeId) {
        if (existeProposta != null)
            return existeProposta;

        var rCmd = new RemoteCommand('DistratoRenegociacaoService', 'ExisteProposta', '/MRVCustomizations/DistratoRenegociacao/');
        rCmd.SetParameter('oportunidadeId', oportunidadeId);
        var retorno = rCmd.Execute();

        if (crmForm.TratarRetornoRemoteCommand(retorno, "ExisteProposta"))
            existeProposta = retorno.ReturnValue.Mrv.ExisteProposta;

        return existeProposta;
    }

    crmForm.ObterPropostaPorOportunidade = function(oportunidadeId) {
        var rCmd = new RemoteCommand('DistratoRenegociacaoService', 'ObterPropostaPorOportunidade', '/MRVCustomizations/DistratoRenegociacao/');
        rCmd.SetParameter('oportunidadeId', oportunidadeId);
        var retorno = rCmd.Execute();

        if (crmForm.TratarRetornoRemoteCommand(retorno, "ObterPropostaPorOportunidade")) {
            if (retorno.ReturnValue.Mrv.Encontrou)
                return retorno.ReturnValue.Mrv;
        }
        return null;
    }

    crmForm.ObterContratoQueEstaSendoRenegociado = function(contratoId) {
        var rCmd = new RemoteCommand('DistratoRenegociacaoService', 'ObterContratoQueEstaSendoRenegociado', '/MRVCustomizations/DistratoRenegociacao/');
        rCmd.SetParameter('contratoId', contratoId);
        var retorno = rCmd.Execute();

        if (crmForm.TratarRetornoRemoteCommand(retorno, "ObterContratoQueEstaSendoRenegociado")) {
            if (retorno.ReturnValue.Mrv.Encontrou)
                return retorno.ReturnValue.Mrv;
        }
        return null;
    }

    crmForm.UsuarioPodeFecharRenegociacao = function() {
        return crmForm.UsuarioPertenceEquipe('Contrato-Gerência');
    }

    crmForm.ObterGrupoRenegociacao = function(grupoRenegociacaoId) {

        if (null == grupoRenegociacaoId)
            return null;

        if (null != grupoRenegociacao && (grupoRenegociacao.new_grupo_renegociacaoid == grupoRenegociacaoId[0].id.toString()))
            return grupoRenegociacao;

        var rCmd = new RemoteCommand('DistratoRenegociacaoService', 'ObterGrupoRenegociacao', '/MRVCustomizations/DistratoRenegociacao/');
        rCmd.SetParameter('grupoRenegociacaoId', grupoRenegociacaoId[0].id);
        var retorno = rCmd.Execute();

        if (crmForm.TratarRetornoRemoteCommand(retorno, "ObterGrupoRenegociacao")) {
            if (retorno.ReturnValue.Mrv.new_grupo_renegociacaoid != null)
                grupoRenegociacao = retorno.ReturnValue.Mrv;
        }
        return grupoRenegociacao;
    }

    crmForm.ObterTipoRenegociacao = function(tipoRenegociacaoId) {
        if (null == tipoRenegociacaoId)
            return null;

        if (null != tipoRenegociacao && (tipoRenegociacao.new_tipo_renegociacaoid == tipoRenegociacaoId[0].id.toString()))
            return tipoRenegociacao;

        var rCmd = new RemoteCommand('DistratoRenegociacaoService', 'ObterTipoRenegociacao', '/MRVCustomizations/DistratoRenegociacao/');
        rCmd.SetParameter('tipoRenegociacaoId', tipoRenegociacaoId[0].id);
        var retorno = rCmd.Execute();

        if (crmForm.TratarRetornoRemoteCommand(retorno, "ObterTipoRenegociacao")) {
            if (retorno.ReturnValue.Mrv.new_tipo_renegociacaoid != null)
                return retorno.ReturnValue.Mrv;
        }
        return tipoRenegociacao;
    }

    /*
    * Verifica se um cliente foi cadastrado como "segundo cliente" de uma oportunidade de venda
    * Este método é executado de dentro do arquivo isv.config através do botão "Gerar Contrato da Renegociação"
    */
    crmForm.FoiCadastradoComoSegundoCliente = function(contratoId, clienteId) {

        var rCmd = new RemoteCommand('DistratoRenegociacaoService', 'JaFoiCadastradoComoSegundoCliente', '/MRVCustomizations/DistratoRenegociacao/');
        rCmd.SetParameter('contratoId', contratoId);
        rCmd.SetParameter('clienteId', clienteId);
        var retorno = rCmd.Execute();

        if (crmForm.TratarRetornoRemoteCommand(retorno, "JaFoiCadastradoComoSegundoCliente"))
            return retorno.ReturnValue.Mrv.SegundoCliente;

        return false;
    }

    crmForm.ValidarDataAssinaturaCliente = function() {
        var stringDataAssinatura;
        var dataAssinatura = crmForm.all.new_data_assinatura_cliente.DataValue;

        if (dataAssinatura == null)
            return;

        if (crmForm.all.new_contratoid.DataValue == null) {
            alert('Antes de Definir a Data de Assinatura Cliente, você deve selecionar um Contrato.');
            crmForm.all.new_data_assinatura_cliente.DataValue = null;
        }

        stringDataAssinatura = dataAssinatura.getDate() + '/' + (dataAssinatura.getMonth() + 1) + '/' + dataAssinatura.getYear();

        var rCmd = new RemoteCommand('DistratoRenegociacaoService', 'DataAssinaturaRenegociacaoValida', '/MRVCustomizations/DistratoRenegociacao/');
        rCmd.SetParameter('contratoId', crmForm.all.new_contratoid.DataValue[0].id);
        rCmd.SetParameter('dataAssinatura', stringDataAssinatura);
        var retorno = rCmd.Execute();

        if (crmForm.TratarRetornoRemoteCommand(retorno, "DataAssinaturaRenegociacaoValida")) {
            if (retorno.ReturnValue != null && !retorno.ReturnValue.Mrv.Valida) {
                alert('A Data de Assinatura do Cliente deve ser maior ou igual à Data Base do Contrato.');
                crmForm.all.new_data_assinatura_cliente.SetFocus();
            }
        }
        else
            alert('Houve um erro ao validar a Data de Assinatura do Cliente, porém observe que a Data de Assinatura do Cliente deve ser maior que a Data Base do Contrato.');
    }

    /* Configura a visibilidade dos camops (Data de Renegociação Cessão Direito e Previsão Entrega de Chaves) de acordo com o Tipo Renegociação selecionado */
    crmForm.ConfiguraVisibilidadeCamposPorTipoRenegociacao = function(tipoRenegociacaoId) {
        var rCmd = new RemoteCommand('MrvService', 'VisibilidadeCamposPorTipoRenegociaca', '/MRVCustomizations/');
        rCmd.SetParameter('tipoRenegociacaoId', tipoRenegociacaoId);
        var retorno = rCmd.Execute();

        if (crmForm.TratarRetornoRemoteCommand(retorno, "VisibilidadeCamposPorTipoRenegociaca")) {
            crmForm.EscondeCampo("new_data_renegociacao_cessao_direito", retorno.ReturnValue.Mrv.DataRenegociacaoCessaoDireito == false);
            crmForm.EscondeCampo("new_nova_previsao_entrega_chaves", retorno.ReturnValue.Mrv.NovaPrevisaoEntregaChaves == false);
        }
    }

    crmForm.ObterInformacoesDoContrato = function(contratoId) {
        var rCmd = new RemoteCommand("DistratoService", "ObterInformacoesContratoParaDistrato", "/MRVCustomizations/DistratoRenegociacao/");
        rCmd.SetParameter("contratoId", contratoId);
        retorno = rCmd.Execute();

        if (crmForm.TratarRetornoRemoteCommand(retorno, "ObterInformacoesContratoParaDistrato"))
            return retorno;
        else
            return null;
    }

    Administrador = null;
    crmForm.UsuarioAdministrador = function() {
        if (Administrador == null) {
            var rCmd = new RemoteCommand('MrvService', 'UserContainsTeams', '/MRVCustomizations/');
            rCmd.SetParameter('teams', 'Administradores');
            var retorno = rCmd.Execute();

            if (retorno.Success)
                Administrador = retorno.ReturnValue.Mrv.Found;
        }

        return Administrador;
    }

    crmForm.ConfiguraFrameSaldo = function() {
        if (crmForm.all.new_aditivoid.DataValue != null)
            crmForm.all.IFRAME_Saldo.src = "/mrvweb/saldo/saldo.aspx?CodigoAditivo=" + crmForm.all.new_aditivoid.DataValue[0].id;
        else
            crmForm.all.IFRAME_Saldo.src = "/mrvweb/saldo/saldo.aspx?id=" + crmForm.ObjectId;
    }

    crmForm.ObterAditivoPorContrato = function() {
        if (crmForm.all.new_contratoid.DataValue && (crmForm.all.new_tipo_contrato.DataValue &&
                (crmForm.all.new_tipo_contrato.DataValue == TipoDeContrato.UNIDADE || crmForm.all.new_tipo_contrato.DataValue == TipoDeContrato.PERMUTA))) {

            //Aditivo
            var cmd = new RemoteCommand('DistratoRenegociacaoService', 'ObterDadosAditivoPorContrato', '/MRVCustomizations/DistratoRenegociacao/');
            cmd.SetParameter('contratoId', crmForm.all.new_contratoid.DataValue[0].id);
            cmd.SetParameter('tipoVenda', crmForm.all.new_tipo_contrato.DataValue);
            var resultado = cmd.Execute();

            if (crmForm.TratarRetornoRemoteCommand(resultado, "ObterDadosAditivoPorContrato")) {
                if (resultado.ReturnValue.Mrv.aditivoId && resultado.ReturnValue.Mrv.aditivoNome) {
                    crmForm.AddValueLookup(crmForm.all.new_aditivoid, resultado.ReturnValue.Mrv.aditivoId, '10103', resultado.ReturnValue.Mrv.aditivoNome);
                    crmForm.all.new_aditivoid.ForceSubmit = true;
                    crmForm.LoadAditivoLookup();
                    crmForm.all.new_aditivoid.FireOnChange();
                    return;
                }
            }
        }

        crmForm.all.new_aditivoid.DataValue = null;
        crmForm.LoadAditivoLookup();
    }
    
    crmForm.LoadAditivoLookup = function(){
        var oTypeCode = "10103";    
        var oParam = "objectTypeCode=" + oTypeCode + "&filterDefault=false&attributesearch=new_name&_contractid=" + (crmForm.all.new_contratoid.DataValue != null ? crmForm.all.new_contratoid.DataValue[0].id : "") + '&_tipoaditivo=' + (crmForm.all.new_tipo_contrato.DataValue != null ? crmForm.all.new_tipo_contrato.DataValue : "0");
        crmForm.FilterLookup(crmForm.all.new_aditivoid, oTypeCode, "/MRVWeb/FilterLookup/FilterLookup.aspx", oParam);
    }

    AtualizarURL = function() {
        if (crmForm.all.IFRAME_auditoria_Oportunidade_Reneg != null && crmForm.all.new_tipo_renegociacao_id.DataValue != null) {
            var params = crmForm.all.IFRAME_auditoria_Oportunidade_Reneg.url;
            params = params.substr(params.indexOf('?'));
            var idTipoReneg = "&idTipoReneg=" + crmForm.all.new_tipo_renegociacao_id.DataValue[0].id;
            var newTarget = "/mrvweb/OportunidadeReneg/AuditoriaOportunidadeRenegociacao.aspx";
            newTarget = newTarget + params + idTipoReneg;
            crmForm.all.IFRAME_auditoria_Oportunidade_Reneg.src = newTarget;
        }

    }

    function loadScript() {
        var oScript = document.createElement('script');
        oScript.type = "text/javascript";
        oScript.src = "/_static/Base.js";

        var oHead = document.getElementsByTagName('head')[0];
        oHead.appendChild(oScript);

        oScript.onreadystatechange = function() {
            if (this.readyState == 'complete' || this.readyState == 'loaded')
                Load();
        }
    }

    loadScript();

    function Load() {

        //Se a entidade distrato for aberta através da entidade incident
        if (window.opener != null && window.opener.crmForm != null && crmForm.FormType == TypeCreate && window.opener.crmForm.ObjectTypeCode == 112 && window.opener.crmForm) {

            //Busca cliente da ocorrência
            if (window.opener.crmForm.customerid.DataValue != null) {
                var idCliente = window.opener.crmForm.customerid.DataValue[0].id;
                var nomeCliente = window.opener.crmForm.customerid.DataValue[0].name;
                var typeCode = window.opener.crmForm.customerid.DataValue[0].type;
                crmForm.AddValueLookup(crmForm.all.new_accountid, idCliente, typeCode, nomeCliente);
            }
            crmForm.all.new_accountid.FireOnChange();

            //Tipo de Contrato
            var cmd = new RemoteCommand("MrvService", "ObterTipoDoContratoDaOportunidade", "/MRVCustomizations/");
            cmd.SetParameter("oportunidadeId", window.opener.crmForm.all.new_opportunityid.DataValue[0].id);
            var resultado = cmd.Execute();

            if (crmForm.TratarRetornoRemoteCommand(resultado, "ObterIdContratoPorOportunidade")) {
                if (resultado.ReturnValue.Mrv.TipoContrato != undefined) {
                    crmForm.all.new_tipo_contrato.DataValue = resultado.ReturnValue.Mrv.TipoContrato;
                }
            }

            //Busca contrato e tipo de contrato da oportunidade da Ocorrência
            if (window.opener.crmForm.all.new_opportunityid.DataValue != null) {

                //Contrato
                var cmd = new RemoteCommand("MrvService", "ObterIdContratoPorOportunidade", "/MRVCustomizations/");
                cmd.SetParameter("oportunidadeId", window.opener.crmForm.all.new_opportunityid.DataValue[0].id);
                var resultado = cmd.Execute();

                if (crmForm.TratarRetornoRemoteCommand(resultado, "ObterIdContratoPorOportunidade"))
                    if (resultado.ReturnValue.Mrv.contractId != '' && resultado.ReturnValue.Mrv.contractName != '') {
                    crmForm.AddValueLookup(crmForm.all.new_contratoid, resultado.ReturnValue.Mrv.contractId, '1010', resultado.ReturnValue.Mrv.contractName);

                    crmForm.all.new_contratoid.FireOnChange();
                }
            }
        }

        var oParam = "objectTypeCode=10108&filterDefault=false&attributesearch=new_name";

        var oTypeCode = 10108;
        crmForm.FilterLookup(crmForm.all.new_grupo_renegociacao_id, oTypeCode, "/MRVWeb/FilterLookup/FilterLookup.aspx", oParam);

        var oParam = "objectTypeCode=1011&filterDefault=false&attributesearch=title&_customerid=" + (crmForm.all.new_accountid.DataValue != null ? crmForm.all.new_accountid.DataValue[0].id : "");
        crmForm.FilterLookup(crmForm.all.new_contratoid, 1010, "/MRVWeb/FilterLookup/FilterLookup.aspx", oParam);

        /* Variável que armazena o tipo de renegociação selecionado pelo usuário.
        Esta variável é utilizada nos eventos OnChange, Onsave e também no arquivo ISV.config. */
        tipoRenegociacao = null;
        grupoRenegociacao = null;
        contrato = null;
        existeProposta = null;

        crmForm.LoadAditivoLookup();
        crmForm.all.new_aditivoid.FireOnChange();
        crmForm.all.new_novoclienteid.FireOnChange();
        crmForm.all.new_tipo_renegociacao_id.FireOnChange();

        if (crmForm.FormType == TypeUpdate && crmForm.ExisteProposta(crmForm.ObjectId)) {
            crmForm.all.new_accountid.Disabled = true;
            crmForm.all.new_contratoid.Disabled = true;
        }

        if (crmForm.UsuarioAdministrador() == null || !crmForm.UsuarioAdministrador())
            crmForm.all.tab7Tab.style.display = "none";

        crmForm.ConfiguraFrameSaldo();

        Auditar = function(userLogado) {
            var cmd = new RemoteCommand("MrvService", "Auditar", "/MrvCustomizations/");
            cmd.SetParameter("entidadeId", crmForm.ObjectId);
            cmd.SetParameter("tipoAuditoria", TipoAuditoria.OPORTUNIDADE_RENEGOCIACAO);
            var resultado = cmd.Execute();

            if (resultado.Success) {
                if (resultado.ReturnValue.Mrv.Error) {
                    alert(resultado.ReturnValue.Mrv.Error);
                    return false;
                }
                else if (resultado.ReturnValue.Mrv.ErrorSoap) {
                    alert(resultado.ReturnValue.Mrv.ErrorSoap);
                    return false;
                }
                else {
                    document.getElementById("IFRAME_auditoria_Oportunidade_Reneg").src = document.getElementById("IFRAME_auditoria_Oportunidade_Reneg").src;
                    return true;
                }
            }
            return false;
        }
        AbrirRegistro = function(typecode, id) {
            openObj(typecode, id);
        }
        AtualizarURL();

        crmForm.CancelarOportunidadeRenegociacao = function() {
            if (!crmForm.all.statuscode.DataValue || crmForm.all.statuscode.DataValue != StatusCode.GANHA) {
                alert("A Oportunidade de Renegociação não pode ser cancelada, pois a mesma não está ganha");
                return;
            }
            if (!crmForm.UsuarioPertenceEquipe(Equipe.CR_Cancelar_Renegociacao)) {
                alert("Não possui permissão para realizar esse processo");
                return;
            }

            var codigoGrupo = '';
            var codigoTipoRenegociacao = 0;

            grupoRenegociacao = crmForm.ObterGrupoRenegociacao(crmForm.all.new_grupo_renegociacao_id.DataValue);

            tipoRenegociacao = crmForm.ObterTipoRenegociacao(crmForm.all.new_tipo_renegociacao_id.DataValue);

            if (grupoRenegociacao) {
                codigoGrupo = grupoRenegociacao.new_codigo_grupo;
            }

            if (tipoRenegociacao) {
                codigoTipoRenegociacao = parseInt(tipoRenegociacao.new_codigo_tipo_renegociacao);
            }

            var trocaDeTitularidade = (codigoGrupo == GrupoRenegociacao.CESSAO_DIREITO_TOTAL) || (codigoGrupo == GrupoRenegociacao.CESSAO_DIREITO_PARCIAL) || (codigoGrupo == GrupoRenegociacao.RENEGOCIACAO_SALDO_DEVEDOR && (codigoTipoRenegociacao == 3));

            if (trocaDeTitularidade) {
                alert("Não é possível Cancelar Oportunidade de Renegociação para Oportunidades do Tipo de Troca de Titularidade");
                return;
            }

            var rCmd = new RemoteCommand('DistratoRenegociacaoService', 'CancelarOportunidadeRenegociacao', '/MRVCustomizations/DistratoRenegociacao/');
            rCmd.SetParameter('oportunidadeRenegociacaoId', crmForm.ObjectId);
            var retorno = rCmd.Execute();

            if (crmForm.TratarRetornoRemoteCommand(retorno, "CancelarOportunidadeRenegociacao")) {
                if (retorno.ReturnValue != null && retorno.ReturnValue.Mrv.RenegociacaoCancelada) {
                    alert('Oportunidade Cancelada com Sucesso!');
                    window.location.reload(true);
                }
                if (!retorno.ReturnValue.Mrv.Success) {
                    if (retorno.ReturnValue.Mrv.Mensagem) {
                        alert(retorno.ReturnValue.Mrv.Mensagem);
                    }
                }
            }
            else {
                alert('Não foi possível Cancelar a Oportunidade de Renegociação');
            }
        }
    }
}
catch (error) {
    formularioValido = false;
    alert("Ocorreu um erro no formulário.\n" + error.description);
}