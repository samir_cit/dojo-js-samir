﻿formularioValido = true;
inicioVigencia = null;
fimVigencia = null;
var ItensPromocao = '';
var statusAguardandoAuditoria = 3;
var statusAuditoriaAprovada = 1;
var statusAuditoriaReprovada = 2;
var equipeAuditoria = 'MKT-AUDITORIA PROMOCAO';
var equipeVendasCadastroPromocao = 'CV_PROMOCAO';
tipoPremioProduto = 1;
tipoPremioServico = 2;
var auditoriaAprovada = 1;
classificacaoParcelamento = 2;
classificacaoProduto = 3;
var auditoriaNaoAprovada = 2;
var naoAuditada = 3;
var dadosUsuario = null;
var TypeCreate = 1;
var TypeUpdate = 2;
try {
    function loadScript() {
        
        var oScript = document.createElement('script');
        oScript.type = "text/javascript";
        oScript.src = "/_static/Base.js";

        var oHead = document.getElementsByTagName('head')[0];
        oHead.appendChild(oScript);

        oScript.onreadystatechange = function() {
            if (this.readyState == 'complete' || this.readyState == 'loaded')
                Load();
        }
    }
    loadScript();

    function Load() {
        //Armazena o período da vigência para checar se houve alteração no evento OnSave
        inicioVigencia = crmForm.all.new_iniciovigencia.DataValue;
        fimVigencia = crmForm.all.new_fimvigencia.DataValue;
        document.frames("IFRAME_Premios").frameElement.attachEvent("onreadystatechange", OnReadyStateChange);
        //Carrega permissoes        
        dadosUsuario = new InformacoesDoUsuario();
        //habilita campo definição premio somente no create
        //se usuario da equipe equipeVendasCadastroPromocao o tipo é sempre serviço
        if (crmForm.FormType == TypeCreate) {
            if (dadosUsuario.PertenceA(equipeVendasCadastroPromocao)) {
                crmForm.all.statuscode.DataValue = statusAguardandoAuditoria;
                crmForm.all.new_tipopremio.DataValue = tipoPremioServico;               
            }
            else
                crmForm.all.new_tipopremio.Disabled = false;
            crmForm.all.new_tipopremio.ForceSubmit = true;
        }
        crmForm.ConfiguraAuditoria();
        crmForm.ClassificacaoChange();
    }
	
	function OnReadyStateChange() {
		var IframeMaster = "IFRAME_Premios";
		if( document.frames(IframeMaster).frameElement.readyState == 'complete' ) {
		    if ((crmForm.GetElementsByClassName("ms-crm-MenuBar-Left", "ul", document.frames(IframeMaster).document) != null) &&
			    (crmForm.GetElementsByClassName("ms-crm-MenuBar-Left", "ul", document.frames("IFRAME_Premios").document)).length != 0) {
				crmForm.GetElementsByClassName("ms-crm-MenuBar-Left", "ul", document.frames(IframeMaster).document)[0].children[0].attachEvent("onclick", OnclickChange);
				var action = crmForm.GetElementsByClassName("ms-crm-MenuBar-Left", "ul", document.frames(IframeMaster).document)[0].children[0].action;
				crmForm.GetElementsByClassName("ms-crm-MenuBar-Left", "ul", document.frames(IframeMaster).document)[0].children[0].action = "parent.crmForm.CarregarItensPromocao(); " + action + " parent.crmForm.AtualizarInclusaoRegistros();";
			}
		}
	}
	
	function OnclickChange() {
		var IframeMaster = "IFRAME_Premios";
		if( document.frames(IframeMaster).frameElement.readyState == 'complete' ) {      
			crmForm.all.IFRAME_Premios.src = ObterEnderecoIframe();
			AjustandoIframe("IFRAME_Premios");
		}	
    }
    crmForm.ConfiguraAuditoria = function() {
        if (crmForm.FormType == TypeUpdate && crmForm.all.statuscode.DataValue == statusAguardandoAuditoria) {
            dadosUsuario = new InformacoesDoUsuario();
            if (dadosUsuario.PertenceA(equipeAuditoria)) {
                crmForm.all.new_auditoria.Disabled = false;
                crmForm.PromocaoAprovadaChange();
            }
            else
                crmForm.all.new_auditoria.Disabled = true;
        }
        var equipeComercial = dadosUsuario.PertenceA(equipeVendasCadastroPromocao);
        if (crmForm.FormType == TypeUpdate && crmForm.all.statuscode.DataValue == statusAuditoriaAprovada && equipeComercial) {
            crmForm.all.new_tipopremio.Disabled = true;
            crmForm.all.new_iniciovigencia.Disabled = true;
            crmForm.all.new_fimvigencia.Disabled = true;
            crmForm.all.new_tipocumulativa.Disabled = true;
            crmForm.all.new_classificacao.Disabled = true;
            crmForm.all.new_quantidade_parcelas.Disabled = true;
        }
    }

    crmForm.PromocaoAprovadaChange = function() {
        //Se aprovada ou nao aprovada        
        if (crmForm.all.new_auditoria.DataValue != naoAuditada) {
            if (dadosUsuario == null)
                dadosUsuario = new InformacoesDoUsuario();
            //Informa usuario auditor e liberada motivo para nao aprovada. 
            //WF 'PROMOÇÃO - PROCESSO AUDITORIA' atualizara status conforme aprovacao/nao aprovacao
            crmForm.AddValueLookup(crmForm.all.new_auditadoporid, dadosUsuario.Id, '8', dadosUsuario.Fullname);            
            if (crmForm.all.new_auditoria.DataValue == auditoriaAprovada) {
                crmForm.all.new_motivo_auditoria.Disabled = false;
                crmForm.all.new_motivo_auditoria.DataValue = null;
                crmForm.SetFieldReqLevel("new_motivo_auditoria", 0);
            }
            else {
                crmForm.all.new_motivo_auditoria.Disabled = false;
                crmForm.SetFieldReqLevel("new_motivo_auditoria", 1);
            }
        }
        else {
            crmForm.SetFieldReqLevel("new_motivo_auditoria", 0);
            crmForm.all.new_auditadoporid.DataValue = null;
            crmForm.all.new_motivo_auditoria.DataValue = null;
        }
        crmForm.all.new_auditadoporid.ForceSubmit = true;
        crmForm.all.new_motivo_auditoria.ForceSubmit = true;
    }

    crmForm.TipoPremioChange = function() {
        if (crmForm.all.new_tipopremio.DataValue == tipoPremioServico) {
            crmForm.all.statuscode.DataValue = statusAguardandoAuditoria;
            alert("Atenção: Promoção Serviço aceitará somente prêmios tipo ITBI e Registro e deverá passar por auditoria!");
        }
        else {
            crmForm.all.statuscode.DataValue = statusAuditoriaAprovada;
            crmForm.all.new_classificacao.DataValue = classificacaoProduto;
        }
    }

    crmForm.ClassificacaoChange = function() {
        if (crmForm.FormType != TypeCreate && crmForm.FormType != TypeUpdate && crmForm.all.new_classificacao.Disabled == false) {
            crmForm.all.new_quantidade_parcelas.Disabled = true;
            return;
        }
        if (crmForm.all.new_classificacao.DataValue == classificacaoParcelamento) {
            crmForm.all.new_quantidade_parcelas.Disabled = false;
            crmForm.SetFieldReqLevel("new_quantidade_parcelas", 1);
        }
        else {
            crmForm.all.new_quantidade_parcelas.Disabled = true;
            crmForm.SetFieldReqLevel("new_quantidade_parcelas", 0);
            crmForm.all.new_quantidade_parcelas.DataValue = null;
        }
        crmForm.all.new_quantidade_parcelas.ForceSubmit = true;
    }

    crmForm.QuantidadeParcelasChange = function() {
        if (crmForm.all.new_name.DataValue == null) {
            alert('Informe primeiro o campo nome!');
            crmForm.all.new_quantidade_parcelas.DataValue = null;
            event.returnValue = false;
            return false;
        }
        if (crmForm.all.new_quantidade_parcelas.DataValue != null) {
            var nome = crmForm.all.new_name.DataValue;
            var n = nome.search(" - ");
            if (n > 0)
                nome = nome.substr(0, n);
            crmForm.all.new_name.DataValue = nome + ' - ' + crmForm.all.new_quantidade_parcelas.DataValue + ' Vezes';
        }
    }
    crmForm.AtualizarInclusaoRegistros = function() {
		var cmd = new RemoteCommand("MrvService", "ValidarItensPromocao", "/MRVCustomizations/");
        cmd.SetParameter("promocaoId", crmForm.ObjectId);
		cmd.SetParameter("itensPromocaoExistentes", ItensPromocao);
        var resultado = cmd.Execute();
        crmForm.TratarRetornoRemoteCommand(resultado, "ValidarItensPromocao");
    }

    crmForm.ObterItensPromocao = function() {
        var cmd = new RemoteCommand("MrvService", "ObterItensPromocao", "/MRVCustomizations/");
        cmd.SetParameter("promocaoId", crmForm.ObjectId);
        var resultado = cmd.Execute();
		if (crmForm.TratarRetornoRemoteCommand(resultado, "ObterItensPromocao"))
            return resultado.ReturnValue.Mrv.itensPromocao;
        else
            return '';
    }
	
	crmForm.CarregarItensPromocao = function() {
		ItensPromocao = crmForm.ObterItensPromocao();
	}

    function ObterEnderecoIframe() {
        if (crmForm.ObjectId != null) {

            var oId = crmForm.ObjectId;
            var oType = crmForm.ObjectTypeCode;
            var security = crmFormSubmit.crmFormSubmitSecurity.value;

            return "/" + ORG_UNIQUE_NAME + "/userdefined/areas.aspx?oId=" + oId + "&oType=" + oType + "&security=" + security + "&roleOrd=1&tabSet=areanew_new_promocao_new_itens_promocao";
        }
        else {
            return "about:blank";
        }
    }

    //Funcao para deixar a Pagina encaixada exatamente dentro do Iframe sem que sobre espacos deixando o Layout desalinhado.
    //Aguarda a janela até que o conteúdo do IFrame  estaja pronto
    function AjustandoIframe(iframeID) {
        if (document.getElementById(iframeID).contentWindow.document.readyState != "complete") {
            window.setTimeout(function() { AjustandoIframe(iframeID) }, 10);
        }
        else {
            // Alterar a cor do fundo
            document.getElementById(iframeID).contentWindow.document.body.style.backgroundColor = "#eef0f6";
            // Remova a borda esquerda
            document.getElementById(iframeID).contentWindow.document.body.all(0).style.borderLeftStyle = "none";
            // Remove padding
            document.getElementById(iframeID).contentWindow.document.body.all(0).all(0).all(0).all(0).style.padding = "0px";
            // Deixa a célula com toda a largura do IFRAME
            document.getElementById(iframeID).contentWindow.document.body.all(0).style.width = "100%"

            // mostra o IFrame
            document.getElementById(iframeID).style.display = "block";
        }
    }

    if (crmForm.FormType != TypeCreate) {
        crmForm.all.IFRAME_Premios.src = ObterEnderecoIframe();
        AjustandoIframe("IFRAME_Premios");
    }
    else {

        document.getElementById("IFRAME_Premios_d").parentNode.parentNode.style.display = "none";
    } if (crmForm.FormType != TypeCreate) {
        crmForm.all.IFRAME_Premios.src = ObterEnderecoIframe();
        AjustandoIframe("IFRAME_Premios");
    }
    else {

        document.getElementById("IFRAME_Premios_d").parentNode.parentNode.style.display = "none";
    }
}
catch (error) {
    formularioValido = false;
    alert("Ocorreu um erro no formulário.\n" + error.description);
}
