﻿try {
    if (!formularioValido) {
        alert("Ocorreu um erro no formulário. Impossível salvar.");
        event.returnValue = false;
        return false;
    }
    
    if (crmForm.all.new_iniciovigencia.DataValue > crmForm.all.new_fimvigencia.DataValue){
        alert("A data de início da vigência não pode ser maior que a data de fim de vigência.");
        crmForm.all.new_iniciovigencia.SetFocus();
        event.returnValue = false;
        return false;
    }
    if (crmForm.all.new_tipopremio.DataValue == tipoPremioServico && crmForm.all.new_classificacao.DataValue == classificacaoProduto)
    {
        alert("Para cadastro de promoção tipo Serviço selecione a classificação Isenção ou Parcelamento.");
        event.returnValue = false;
        return false;
    }
    if (crmForm.all.new_tipopremio.DataValue == tipoPremioProduto && crmForm.all.new_classificacao.DataValue != classificacaoProduto)
    {
        alert("Para cadastro de promoção tipo Produto selecione a classificação Produto.");
        event.returnValue = false;
        return false;
    }

    crmForm.TransformaTodosCaracteresEmMaiusculo();
    //Testa se houve alteração no período de vigência    
    if (crmForm.FormType == TypeUpdate)
    {
        if (inicioVigencia.toString() != crmForm.all.new_iniciovigencia.DataValue.toString() || 
            fimVigencia.toString() != crmForm.all.new_fimvigencia.DataValue.toString()) {
            Informacoes = new InformacoesDoUsuario();
            
            var today = new Date();
            var strDataAtual = "{0}/{1}/{2}".format(today.getDate(), today.getMonth()+1, today.getFullYear());
            var strDataInicioAnt = "{0}/{1}/{2}".format(inicioVigencia.getDate(), inicioVigencia.getMonth()+1, inicioVigencia.getFullYear());
            var strDataFimAnt = "{0}/{1}/{2}".format(fimVigencia.getDate(), fimVigencia.getMonth()+1, fimVigencia.getFullYear());
            var strDataInicioAtu = "{0}/{1}/{2}".format(crmForm.all.new_iniciovigencia.DataValue.getDate(), 
                        crmForm.all.new_iniciovigencia.DataValue.getMonth()+1, 
                        crmForm.all.new_iniciovigencia.DataValue.getFullYear());
            var strDataFimAtu = "{0}/{1}/{2}".format(crmForm.all.new_fimvigencia.DataValue.getDate(), 
                        crmForm.all.new_fimvigencia.DataValue.getMonth()+1, 
                        crmForm.all.new_fimvigencia.DataValue.getFullYear());
        
            var texto = crmForm.all.new_historicoprorrogacao.DataValue == null ? "" : crmForm.all.new_historicoprorrogacao.DataValue;
            texto += texto == "" ? "" : "\r\n===============================\r\n";
            texto += "Vigencia anterior: {0} a {1}\r\n".format(strDataInicioAnt, strDataFimAnt);
            texto += "Vigencia nova: {0} a {1}\r\n".format(strDataInicioAtu, strDataFimAtu);
            texto += "Alterado por: {0}\r\n".format(Informacoes.Fullname);
            texto += "Em: {0}\r\n".format(strDataAtual);
            crmForm.all.new_historicoprorrogacao.DataValue = texto;
            crmForm.all.new_historicoprorrogacao.ForceSubmit = true;
        }
    }
} 
catch (error) {
    alert("Ocorreu um erro no formulário.\n" + error.description);
    event.returnValue = false;
    return false;
}
