﻿formularioValido = true;

Pais = {
    BRASIL: 10
}

Mensagem = {
    PAIS_ESTRANGEIRO_NAO_SELECIONADO: "Para endereço estrangeiro não é possível selecionar o país BRASIL.",
    MENSAGEM_CEP_FORA_FAIXA: "Cep fora da faixa 8 dígitos sequenciais! Ex: 31080140",
    MENSAGEM_CEP_NAO_PREENCHIDO: "Cep não preenchido! Para Endereço Nacional é obrigatório o preenchido do CEP!",
    MENSAGEM_IMPOSSIVEL_ATUALIZAR_CIDADE: "Não é possível atualizar a cidade quando o Endereço Confere",
    CONSULTA_CEP_NAO_DISPONIVEL: "Consulta de CEP não está disponível. Favor preencher o endereço.",
    CEP_NAO_CADASTRADO: "CEP não cadastrado, preencha o endereço!"
}

// Códigos de filter lookup de cidades por estados
CodigoFiltrerLookup = {
    CIDADES_SP: "10031003",
    CIDADES_RJ_SP: "10031004",
    CIDADES_MG: "10031005",
    CIDADES_BA_SE: "10031006",
    CIDADES_AL_PB_PE_RN: "10031007",
    CIDADES_AC_AM_AP_CE_MA_PA_PI_RR: "10031008",
    CIDADES_DF_GO_MS_MT_RO_TO: "10031009",
    CIDADES_PR_SC: "10031010",
    CIDADES_RS: "10031011"
}

try {

    function Load() {

        CEP_LENGTH = 8;
        
        //Montar nome do registro
        crmForm.MontaNome = function() {
            crmForm.all.name.DataValue = crmForm.all.line1.DataValue;
        }

        crmForm.AdicionaBotaoLookup = function(campo, padrao) {
            //Passar o nome do atributo como parâmetro
            var actnButton = new BotaoAjuda(campo);
            //Mensagem
            actnButton.Image.Title = "Clique para selecionar um valor para cidade.";
            //Tamanho e Altura
            actnButton.Image.Width = 21;
            actnButton.Image.Height = 18;
            //Cursor
            actnButton.Image.Cursor = "hand";
            //Imagens Over e Out
            actnButton.Image.MouseOver = "/_imgs/lookupOn.gif";
            actnButton.Image.MouseOut = "/_imgs/lookupOff.gif";
            //Função que o clique deve chamar
            actnButton.Click = function() { crmForm.AbreLookupCidade(padrao) };
            //Adiciona actnButton imagem ao lado do campo
            actnButton.Paint();
        }

        crmForm.SetCidade = function(id, nome) {
            crmForm.all.city.DataValue = nome;
            crmForm.all.new_cidadeid.DataValue = id;

            var cmdDadosCidade = new RemoteCommand("MrvService", "retornarEstadoEDDD", "/MRVCustomizations/");
            cmdDadosCidade.SetParameter("cidadeId", id);
            var oResultado = cmdDadosCidade.Execute();
            if (oResultado.Success) {
                // Atribui o BRASIL como item selecionado no campo Pais
                crmForm.all.new_pais.DataValue = Pais.BRASIL;
                crmForm.all.new_pais.Disabled = true;

                switch (oResultado.ReturnValue.Mrv.estado) {
                    case "AC":
                        crmForm.all.new_estado.SelectedIndex = EstadoPicklist.AC;
                        break;
                    case "AL":
                        crmForm.all.new_estado.SelectedIndex = EstadoPicklist.AL;
                        break;
                    case "AM":
                        crmForm.all.new_estado.SelectedIndex = EstadoPicklist.AM;
                        break;
                    case "AP":
                        crmForm.all.new_estado.SelectedIndex = EstadoPicklist.AP;
                        break;
                    case "BA":
                        crmForm.all.new_estado.SelectedIndex = EstadoPicklist.BA;
                        break;
                    case "CE":
                        crmForm.all.new_estado.SelectedIndex = EstadoPicklist.CE;
                        break;
                    case "DF":
                        crmForm.all.new_estado.SelectedIndex = EstadoPicklist.DF;
                        break;
                    case "ES":
                        crmForm.all.new_estado.SelectedIndex = EstadoPicklist.ES;
                        break;
                    case "GO":
                        crmForm.all.new_estado.SelectedIndex = EstadoPicklist.GO;
                        break;
                    case "MA":
                        crmForm.all.new_estado.SelectedIndex = EstadoPicklist.MA;
                        break;
                    case "MG":
                        crmForm.all.new_estado.SelectedIndex = EstadoPicklist.MG;
                        break;
                    case "MS":
                        crmForm.all.new_estado.SelectedIndex = EstadoPicklist.MS;
                        break;
                    case "MT":
                        crmForm.all.new_estado.SelectedIndex = EstadoPicklist.MT;
                        break;
                    case "PA":
                        crmForm.all.new_estado.SelectedIndex = EstadoPicklist.PA;
                        break;
                    case "PB":
                        crmForm.all.new_estado.SelectedIndex = EstadoPicklist.PB;
                        break;
                    case "PE":
                        crmForm.all.new_estado.SelectedIndex = EstadoPicklist.PE;
                        break;
                    case "PI":
                        crmForm.all.new_estado.SelectedIndex = EstadoPicklist.PI;
                        break;
                    case "PR":
                        crmForm.all.new_estado.SelectedIndex = EstadoPicklist.PR;
                        break;
                    case "RJ":
                        crmForm.all.new_estado.SelectedIndex = EstadoPicklist.RJ;
                        break;
                    case "RN":
                        crmForm.all.new_estado.SelectedIndex = EstadoPicklist.RN;
                        break;
                    case "RO":
                        crmForm.all.new_estado.SelectedIndex = EstadoPicklist.RO;
                        break;
                    case "RR":
                        crmForm.all.new_estado.SelectedIndex = EstadoPicklist.RR;
                        break;
                    case "RS":
                        crmForm.all.new_estado.SelectedIndex = EstadoPicklist.RS;
                        break;
                    case "SC":
                        crmForm.all.new_estado.SelectedIndex = EstadoPicklist.SC;
                        break;
                    case "SE":
                        crmForm.all.new_estado.SelectedIndex = EstadoPicklist.SE;
                        break;
                    case "SP":
                        crmForm.all.new_estado.SelectedIndex = EstadoPicklist.SP;
                        break;
                    case "TO":
                        crmForm.all.new_estado.SelectedIndex = EstadoPicklist.TO;
                        break;
                    default:
                        crmForm.all.new_estado.SelectedIndex = 0;
                        break;
                }
                crmForm.all.new_estado.Disabled = true;
            }
        }

        crmForm.AbreLookupCidade = function(padrao) {
            if (crmForm.all.new_endereco_confere.DataValue) {
                alert(Mensagem.MENSAGEM_IMPOSSIVEL_ATUALIZAR_CIDADE);
                return false;
            }

            var url = "";
            if (padrao) {
                url = "/_controls/lookup/lookupsingle.aspx?class=null&objecttypes=10031&browse=0&ShowNewButton=1&ShowPropButton=1&DefaultType=0";
            }
            else {
                // Recupera o primeiro digito do cep
                var primeiroDigitoCep = crmForm.all.postalcode.DataValue.toString().substring(0, 1);
                var codigoLookup = "";
                switch (primeiroDigitoCep) {
                    case "0":
                    case "1":
                        codigoLookup = CodigoFiltrerLookup.CIDADES_SP;
                        break;
                    case "2":
                        codigoLookup = CodigoFiltrerLookup.CIDADES_RJ_SP;
                        break;
                    case "3":
                        codigoLookup = CodigoFiltrerLookup.CIDADES_MG;
                        break;
                    case "4":
                        codigoLookup = CodigoFiltrerLookup.CIDADES_BA_SE;
                        break;
                    case "5":
                        codigoLookup = CodigoFiltrerLookup.CIDADES_AL_PB_PE_RN;
                        break;
                    case "6":
                        codigoLookup = CodigoFiltrerLookup.CIDADES_AC_AM_AP_CE_MA_PA_PI_RR;
                        break;
                    case "7":
                        codigoLookup = CodigoFiltrerLookup.CIDADES_DF_GO_MS_MT_RO_TO;
                        break;
                    case "8":
                        codigoLookup = CodigoFiltrerLookup.CIDADES_PR_SC;
                        break;
                    case "9":
                        codigoLookup = CodigoFiltrerLookup.CIDADES_RS;
                        break;
                }

                var oParam = "objectTypeCode=" + codigoLookup + "&filterDefault=false&attributesearch=new_name";
                url = "/MRVWeb/FilterLookup/FilterLookup.aspx?" + oParam;
            }

            var retorno = openStdDlg(url, null, 650, 450);
            var id = "";
            var nome = "";
            if (retorno != undefined) {
                if (padrao) {
                    id = retorno.items[0].id;
                    nome = retorno.items[0].name;
                }
                else {
                    strValues = retorno.split('*|*');
                    id = strValues[1];
                    nome = strValues[0];
                }
                crmForm.SetCidade(id, nome);
            }
        }

        AdicionaNotificacao = function() {
            var objeto = this;

            this.ERRO = "/_imgs/error/notif_icn_crit16.png";
            this.ALERTA = "/_imgs/error/notif_icn_warn16.png";
            this.SUCESSO = "/_imgs/ico/16_L_check.gif";

            objeto.Mostra = function(messagem, imagem) {
                var notificationHTML = '<DIV class="Notification"><TABLE cellSpacing="0" cellPadding="0"><TBODY><TR><TD vAlign="top"><IMG class="ms-crm-Lookup-Item" alt="" src="' + imagem + '" /></TD><TD><SPAN>' + messagem + '</SPAN></TD></TR></TBODY></TABLE></DIV>';
                var notificationsArea = document.getElementById('Notifications');
                if (notificationsArea == null)
                    return;
                notificationsArea.innerHTML = notificationHTML;
                notificationsArea.style.display = 'block';
            }

            objeto.Oculta = function() {
                var notificationsArea = document.getElementById('Notifications');
                if (notificationsArea == null)
                    return;
                notificationsArea.innerHTML = '';
                notificationsArea.style.display = 'inline';
            }
        }

        Notificacao = new AdicionaNotificacao();

        crmForm.ValidaEndereco = function() {
            var resumido = crmForm.all.new_endereco_resumido.DataValue != null ? crmForm.all.new_endereco_resumido.DataValue : "";
            var endereco = crmForm.all.line1.DataValue != null ? crmForm.all.line1.DataValue : "";
            var numero = crmForm.all.new_numero.DataValue != null ? crmForm.all.new_numero.DataValue : "";
            var complemento = crmForm.all.new_complemento.DataValue != null ? crmForm.all.new_complemento.DataValue : "";
            var enderecoNacional = crmForm.all.new_endereco_nacional.DataValue;
            
            var retorno = "";
            if (resumido != "")
                retorno = resumido + "-" + numero;
            else
                retorno = endereco + "-" + numero;

            if (complemento != "")
                retorno += " " + complemento;

            if (retorno.length > 60) {
                Notificacao.Mostra("Endereço maior que 60 caracteres. Tamanho " + retorno.length + " caracteres.", Notificacao.ERRO);
                return true;
            }

            if (enderecoNacional) {

                if (crmForm.all.postalcode.DataValue == null) {
                    alert(Mensagem.MENSAGEM_CEP_NAO_PREENCHIDO);
                    return false;
                }
                
                var cep = crmForm.all.postalcode.DataValue.replace(/[^0-9]/g, "");

                if (cep.length != CEP_LENGTH) {
                    Notificacao.Mostra(Mensagem.MENSAGEM_CEP_FORA_FAIXA, Notificacao.ERRO);
                    return true;
                }
            }

            return false;
        }

        var lookupPadrao = true;

        //simula botão lookup da cidade
        crmForm.AdicionaBotaoLookup("city", lookupPadrao);

        //Desabilitar campos
        crmForm.all.city.readOnly = true;

        //Esconder campos da seção administrativa
        crmForm.EscondeCampo("name", true);
        crmForm.EscondeCampo("new_cidadeid", true);
        crmForm.EscondeCampo("addressnumber", true);

        //forca a atualização dos campos
        crmForm.all.city.ForceSubmit = true;
        crmForm.all.new_estado.ForceSubmit = true;

        Informacoes = new InformacoesDoUsuario();

        crmForm.ExibirCamposEstrangeiro = function(visivel) {

            // Definine a visibilidade dos campos de informações estrangeiras
            crmForm.EscondeCampo("new_cidadeestrangeira", !visivel);
            crmForm.EscondeCampo("new_estadoestrangeiro", !visivel);
        }

        crmForm.TrataExibicaoCampoEnderecoConfere = function() {
            var temPerfilDeConfereEndereco = Informacoes.PertenceA("Confere Endereço");
            crmForm.EscondeCampo("new_endereco_confere", !temPerfilDeConfereEndereco);

            if (crmForm.all.new_endereco_confere.DataValue == null)
                crmForm.all.new_endereco_confere.DataValue = true;
            else {
                if (temPerfilDeConfereEndereco)
                    crmForm.TrataEnderecoConfere();
            }
        }

        crmForm.TrataEnderecoConfere = function() {
            if (crmForm.all.new_endereco_nacional.DataValue) {
                var habilitaCampoEndereco = crmForm.all.new_endereco_confere.DataValue;
                crmForm.DesabilitarCampos("city;new_estado;new_pais", true);
                crmForm.DesabilitarCampos("new_complemento;new_numero", false);
                crmForm.DesabilitarCampos("line1;new_tipo_logradouro;new_bairro", habilitaCampoEndereco);
                crmForm.DesabilitarCampos("new_endereco_confere", !habilitaCampoEndereco);

                if (!habilitaCampoEndereco && (crmForm.FormType == TypeCreate || crmForm.FormType == TypeUpdate)) {
                    crmForm.all.new_endereco_confere.ForceSubmit = true;
                }

                if (crmForm.all.new_pais.DataValue == null) {
                    crmForm.all.new_pais.DataValue = Pais.BRASIL;
                }

                if (crmForm.all.new_pais.DataValue != Pais.BRASIL) {
                    crmForm.TrataPais();
                }
            }
        }

        crmForm.TrataPais = function() {
            var enderecoNacional = crmForm.all.new_endereco_nacional.DataValue;
            if (enderecoNacional && crmForm.all.new_pais.DataValue == Pais.BRASIL) {
                crmForm.all.new_pais.Disabled = true;
            }
            else if (!enderecoNacional && crmForm.all.new_pais.SelectedIndex > 0 && crmForm.all.new_pais.DataValue == Pais.BRASIL) {
                alert(Mensagem.PAIS_ESTRANGEIRO_NAO_SELECIONADO);
                crmForm.all.new_pais.SelectedIndex = 0;
            }
            else {
                crmForm.all.new_pais.Disabled = false;
            }
        }

        crmForm.TratarExibicaoEnderecoNacional = function() {
            if (crmForm.all.new_endereco_nacional.DataValue == null) {
                crmForm.all.new_endereco_nacional.DataValue = true;
            }
        }

        crmForm.TratarEnderecoNacional = function() {
            var enderecoNacional = crmForm.all.new_endereco_nacional.DataValue;
            if (enderecoNacional) {
                crmForm.EscondeCampo("new_endereco_confere", false);
                
                crmForm.TrataEnderecoConfere();
                
                if (crmForm.all.new_pais.DataValue != Pais.BRASIL) {
                    crmForm.all.new_pais.DataValue = Pais.BRASIL;
                    crmForm.all.new_pais.Disabled = true;
                }
                crmForm.ExibirCamposEstrangeiro(false);
                crmForm.all.new_cidadeestrangeira.DataValue = null;
                crmForm.all.new_estadoestrangeiro.DataValue = null;
            }
            else {
                crmForm.all.new_estado.Disabled = true;
                crmForm.all.city.Disabled = true;
                crmForm.all.city.DataValue = null;
                //Força que a imagem do lookup seja desabilitada
                for (i = 0; i < crmForm.all.city_d.childNodes.length; i++) {
                    if (crmForm.all.city_d.childNodes[i].nodeName.toUpperCase() == "IMG") {
                        crmForm.all.city_d.childNodes[i].disabled = true;
                    }
                }
                crmForm.all.new_cidadeid.DataValue = null;
                crmForm.all.new_estado.SelectedIndex = 0;
                crmForm.all.new_estado.DataValue = null;
                if (crmForm.all.new_pais.DataValue == Pais.BRASIL) {
                    crmForm.all.new_pais.SelectedIndex = 0;
                }
                crmForm.all.new_pais.Disabled = false;
                crmForm.EscondeCampo("new_endereco_confere", true);
                crmForm.DesabilitarCampos("line1;new_tipo_logradouro;new_bairro", false);
                crmForm.ExibirCamposEstrangeiro(true);
            }
        }

        crmForm.TrataExibicaoCampoEnderecoConfere();
        crmForm.TratarExibicaoEnderecoNacional();
        crmForm.TratarEnderecoNacional();

        crmForm.LimpaCampos = function() {
            crmForm.all.new_bairro.DataValue = null;
            crmForm.all.new_complemento.DataValue = null;
            crmForm.all.line1.DataValue = null;
            crmForm.all.name.DataValue = null;
            crmForm.all.new_tipo_logradouro.DataValue = null;
            crmForm.all.new_estado.SelectedIndex = 0;
            crmForm.all.city.DataValue = null;
            crmForm.all.new_pais.SelectedIndex = 0;
            crmForm.all.new_numero.DataValue = null;
            crmForm.all.new_endereco_resumido.DataValue = null;
        }

        crmForm.SetUF = function(nome, picklistField) {
            for (fieldIndex = 0; fieldIndex < picklistField.Options.length; fieldIndex++) {
                if (typeof (nome) == "string") {
                    if (picklistField.Options[fieldIndex].Text.toUpperCase() == nome.toUpperCase())
                        picklistField.SelectedIndex = fieldIndex;
                }
            }
        }
        
        crmForm.SetTipoLogradouro = function(nome, picklistField) {
            for (fieldIndex = 0; fieldIndex < picklistField.Options.length; fieldIndex++) {
                if (typeof (nome) == "string") {
                    if (picklistField.Options[fieldIndex].Text.toUpperCase() == nome.toUpperCase()) {
                        picklistField.SelectedIndex = fieldIndex;
                        return;
                    }
                }
            }
            picklistField.SelectedIndex = 41;
            return "";
        }

        crmForm.ObterEndereco = function(cep) {
            if (cep != null) {
                cep = cep.replace(".", "");
                cep = cep.replace("-", "");

                var cmd = new RemoteCommand("MrvService", "GetAddressByCep", "/MRVCustomizations/");
                cmd.SetParameter("cep", cep);
                var result = cmd.Execute();
                if (result.Success) {

                    if (!result.ReturnValue.Mrv.IsNull && !result.ReturnValue.Mrv.Error) {

                        if (typeof (result.ReturnValue.Mrv.TIPORUA) != "object" && typeof (result.ReturnValue.Mrv.NOMERUA) != "object") {
                            crmForm.all.line1.DataValue = result.ReturnValue.Mrv.TIPORUA + " " + result.ReturnValue.Mrv.NOMERUA;
                            crmForm.all.name.DataValue = result.ReturnValue.Mrv.TIPORUA + " " + result.ReturnValue.Mrv.NOMERUA;
                        }
                        else if (typeof (result.ReturnValue.Mrv.NOMERUA) != "object") {
                            crmForm.all.line1.DataValue = result.ReturnValue.Mrv.NOMERUA;
                            crmForm.all.name.DataValue = result.ReturnValue.Mrv.NOMERUA;
                        }
                        else {
                            crmForm.all.line1.DataValue = null;
                            crmForm.all.name.DataValue = null;
                        }

                        if (crmForm.all.line1.DataValue == "" || crmForm.all.line1.DataValue == null)
                            crmForm.all.line1.Disabled = false;
                        else
                            crmForm.all.line1.Disabled = true;

                        if (typeof (result.ReturnValue.Mrv.TIPORUA) != "object") {
                            crmForm.SetTipoLogradouro(result.ReturnValue.Mrv.TIPORUA, crmForm.all.new_tipo_logradouro);
                            crmForm.all.new_tipo_logradouro.Disabled = true;
                        } else {
                            crmForm.all.new_tipo_logradouro.Disabled = false;
                        }

                        if (typeof (result.ReturnValue.Mrv.COMPLEMENTO) != "object") {
                            if (result.ReturnValue.Mrv.COMPLEMENTO.length <= 10)
                                crmForm.all.new_complemento.DataValue = result.ReturnValue.Mrv.COMPLEMENTO;
                            else
                                crmForm.all.new_complemento.DataValue = result.ReturnValue.Mrv.COMPLEMENTO.substr(0, 10);
                        } else
                            crmForm.all.new_complemento.DataValue = null;

                        if (typeof (result.ReturnValue.Mrv.NOMERUAREDUZIDO) != "object")
                            crmForm.all.new_endereco_resumido.DataValue = result.ReturnValue.Mrv.NOMERUAREDUZIDO;

                        if (typeof (result.ReturnValue.Mrv.UF) != "object") {
                            crmForm.SetUF(result.ReturnValue.Mrv.UF, crmForm.all.new_estado);
                            crmForm.all.new_pais.SelectedIndex = 10;
                            crmForm.all.new_estado.Disabled = true;
                        }
                        else
                            crmForm.all.new_estado.Disabled = false;

                        if (typeof (result.ReturnValue.Mrv.BAIRRO) != "object") {
                            if (result.ReturnValue.Mrv.BAIRRO.length <= 15)
                                crmForm.all.new_bairro.DataValue = result.ReturnValue.Mrv.BAIRRO;
                            else
                                crmForm.all.new_bairro.DataValue = result.ReturnValue.Mrv.BAIRRO.substr(0, 15);

                            crmForm.all.new_bairro.Disabled = true;
                        } else {
                            crmForm.all.new_bairro.DataValue = null;
                            crmForm.all.new_bairro.Disabled = false;
                        }

                        if (typeof (result.ReturnValue.Mrv.CIDADE) != "object" && typeof (result.ReturnValue.Mrv.UF) != "object") {
                            var cmdCidade = new RemoteCommand("MrvService", "GetCityByNameUf", "/MRVCustomizations/");
                            cmdCidade.SetParameter("NameCidade", result.ReturnValue.Mrv.CIDADE);
                            cmdCidade.SetParameter("UF", result.ReturnValue.Mrv.UF);
                            var oResultado = cmdCidade.Execute();
                            if (oResultado.Success) {
                                if (oResultado.ReturnValue.Mrv.Cidade != false) {
                                    crmForm.all.new_cidadeid.DataValue = oResultado.ReturnValue.Mrv.Cidade;
                                    crmForm.all.city.DataValue = result.ReturnValue.Mrv.CIDADE;
                                }
                            }
                        }

                        crmForm.all.city.Disabled = true;
                        //Força que a imagem do lookup seja desabilitada
                        for (i = 0; i < crmForm.all.city_d.childNodes.length; i++) {
                            if (crmForm.all.city_d.childNodes[i].nodeName.toUpperCase() == "IMG") {
                                crmForm.all.city_d.childNodes[i].disabled = true;
                            }
                        }
                        crmForm.all.new_pais.Disabled = true;

                        var enderecoConfere = (crmForm.all.line1.DataValue != null && crmForm.all.new_tipo_logradouro.DataValue != null &&
                                                crmForm.all.city.DataValue != null && crmForm.all.new_bairro.DataValue != null);

                        crmForm.all.new_endereco_confere.DataValue = enderecoConfere;
                        crmForm.DesabilitarCampos("new_endereco_confere", !enderecoConfere);

                        if (crmForm.FormType == TypeCreate || crmForm.FormType == TypeUpdate) {
                            crmForm.all.new_endereco_confere.ForceSubmit = true;
                        }
                    }
                    else {
                        if (result.ReturnValue.Mrv.Error) {
                            alert(Mensagem.CONSULTA_CEP_NAO_DISPONIVEL);
                        }
                        else {
                            alert(Mensagem.CEP_NAO_CADASTRADO);

                            var lookupPadrao = false;

                            //simula botão lookup da cidade
                            crmForm.AdicionaBotaoLookup("city", lookupPadrao);
                        }
                        crmForm.LimpaCampos();

                        crmForm.all.new_endereco_confere.DataValue = false;
                        crmForm.DesabilitarCampos("new_endereco_confere", true);

                        if (crmForm.FormType == TypeCreate || crmForm.FormType == TypeUpdate) {
                            crmForm.all.new_endereco_confere.ForceSubmit = true;
                        }

                        crmForm.all.new_tipo_logradouro.Disabled = false;
                        crmForm.all.city.Disabled = false;
                        //Força que a imagem do lookup seja desabilitada
                        for (i = 0; i < crmForm.all.city_d.childNodes.length; i++) {
                            if (crmForm.all.city_d.childNodes[i].nodeName.toUpperCase() == "IMG") {
                                crmForm.all.city_d.childNodes[i].disabled = false;
                            }
                        }
                        crmForm.all.city_d.Disabled = false;
                        crmForm.all.new_estado.Disabled = true;
                        crmForm.all.new_pais.Disabled = true;
                        crmForm.all.line1.Disabled = false;
                        crmForm.all.new_bairro.Disabled = false;
                    }
                }
            }
        }
        
        crmForm.ValidarCEP = function(campo) {
            crmForm.LimpaCampos();
            if (typeof (campo) != "undefined" && campo != null) {
                if (campo.DataValue != null) {
                    // Valida se é um endereço nacional
                    if (crmForm.all.new_endereco_nacional.DataValue) {
                        var sTmp = campo.DataValue.replace(/[^0-9]/g, "");

                        if (sTmp.length != 8) {
                            campo.DataValue = null;
                            alert(Mensagem.MENSAGEM_CEP_FORA_FAIXA);
                        }
                        else {
                            crmForm.ObterEndereco(campo.DataValue);
                            campo.DataValue = sTmp.substr(0, 2) + "." + sTmp.substr(2, 3) + "-" + sTmp.substr(5, 3);
                        }
                    }
                }
                else { crmForm.ObterEndereco(campo.DataValue); }
            }
        }

        crmForm.ResumirEndereco = function() {
            if (crmForm.all.line1.DataValue != null && crmForm.all.new_tipo_logradouro.SelectedText != "") {
                var endereco = crmForm.all.line1.DataValue.split(' ');
                var enderecoresumido = "";
                var ultimoEndereco = endereco.length - 1;

                try {
                    var tipologradouro = crmForm.all.new_tipo_logradouro.SelectedText.toLowerCase();
                    tipologradouro = crmForm.RemoverAcento(tipologradouro);
                    tipologradouro = TIPOLOGRADOURO[tipologradouro].code;
                }
                catch (err) {
                    tipologradouro = tipologradouro.substring(0, 1).toUpperCase();
                }
                
                enderecoresumido += tipologradouro + '. ';
                
                for (var i = 0; i < endereco.length; i++) {
                    var item = endereco[i];
                    if (i == 0 || i == ultimoEndereco) {
                        enderecoresumido += item.substring(0, item.length);
                    }
                    else {
                        enderecoresumido += item.substring(0, 1) + '.';
                    }

                    if (i != ultimoEndereco)
                        enderecoresumido += " ";
                }

                crmForm.all.new_endereco_resumido.DataValue = enderecoresumido;
            }
        }

        crmForm.RemoverAcento = function(texto) {
            texto = texto.replace(new RegExp('[ÁÀÂÃ]', 'gi'), 'a');
            texto = texto.replace(new RegExp('[ÉÈÊ]', 'gi'), 'e');
            texto = texto.replace(new RegExp('[ÍÌÎ]', 'gi'), 'i');
            texto = texto.replace(new RegExp('[ÓÒÔÕ]', 'gi'), 'o');
            texto = texto.replace(new RegExp('[ÚÙÛ]', 'gi'), 'u');
            texto = texto.replace(new RegExp('[Ç]', 'gi'), 'c');
            return texto;
        }

        // Desabilita Campo Endereço Resumido
        crmForm.all.new_endereco_resumido.Disabled = true;

    }

    function loadScript() {
        var oHead = document.getElementsByTagName('head')[0];
        var oScript = document.createElement('script');
        oScript.type = "text/javascript";
        oScript.src = "/_static/Base.js";

        oScript.onreadystatechange = function() {
            if (this.readyState == 'complete' || this.readyState == 'loaded') {
                Load();
            }
        }
        oHead.appendChild(oScript);
    }

    loadScript();

} catch (error) {
    formularioValido = false;
    alert("Ocorreu um erro no formulário.\n" + error.description);
}