﻿try {
    function ErroCidade() {
        crmForm.SetCidade("", "");
        alert("Erro ao selecionar a cidade. Tente novamente.");
        event.returnValue = false;
        return false;
    }

    if (!formularioValido) {
        alert("Ocorreu um erro no formulário. Impossível salvar.");
        event.returnValue = false;
        return false;
    }
    if (crmForm.all.name.DataValue == null){
        crmForm.MontaNome();
    }

    var endereco = crmForm.all.line1.DataValue.toString();
    var enderecoResumido = crmForm.all.new_endereco_resumido.DataValue;
    var complemento = "";
    var bairro = crmForm.all.new_bairro.DataValue;

    if(crmForm.all.new_complemento.DataValue != null)
     complemento = crmForm.all.new_complemento.DataValue.toString();

    if(endereco.indexOf(";") !== -1 || enderecoResumido.indexOf(";") !== -1 || complemento.indexOf(";") !== -1 || bairro.indexOf(";") !== -1)
    {
        alert('Não é possível salvar com ";" em nenhum dos campos do endereço.');
        event.returnValue = false;
        return false;
    }

    if (crmForm.ValidaEndereco()) {
        event.returnValue = false;
        return false;
    }
    if(crmForm.all.new_endereco_nacional.DataValue){
        if (crmForm.all.new_cidadeid.DataValue != null) {
            var guid = crmForm.all.new_cidadeid.DataValue.replace("{", "").replace("}", "");
            if (guid.length != 36){
                ErroCidade();
            }

        } else{
            ErroCidade();
        }
    }

    //Habilitar campos.
    crmForm.all.city.readOnly = false;
    
    if (crmForm.FormType == TypeCreate || crmForm.FormType == TypeUpdate) {
        crmForm.all.new_endereco_resumido.ForceSubmit = true;
        crmForm.all.new_tipo_logradouro.ForceSubmit = true;
        crmForm.all.line1.ForceSubmit = true;
        crmForm.all.new_bairro.ForceSubmit = true;
        crmForm.all.new_estado.ForceSubmit = true;
        crmForm.all.new_pais.ForceSubmit = true;       
    }

    // Transformar caracteres em maiusculos------------------------------------//
    var elm = document.getElementsByTagName("input");
    for (i = 0; i < elm.length; i++) {
        if (elm[i].type == "text")
            if (elm[i].DataValue != null) {
            try {
                elm[i].DataValue = elm[i].DataValue.toUpperCase();
            }
            catch (e) { }
        }
    }

} catch (error) {
    alert("Ocorreu um erro no formulário.\n" + error.description);
    event.returnValue = false;
    return false;
}