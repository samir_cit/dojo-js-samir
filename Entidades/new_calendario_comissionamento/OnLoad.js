﻿formularioValido = true;
try 
{

    crmForm.all.new_motivo_alteracao.Disabled = crmForm.FormType != 2;
    crmForm.ConfigurarNome = function() {
        if (crmForm.IsDirty) {
            var mes = crmForm.all.new_mes_competencia.DataValue;
            var ano = crmForm.all.new_ano_competencia.DataValue;
            crmForm.all.new_name.DataValue = 'CALENDARIO COMISSIONAMENTO - ' + mes + '/' + ano;
        }
    }
} 
catch (error) 
{
    formularioValido = false;
    alert("Ocorreu um erro no formulário.\n" + error.description);
}