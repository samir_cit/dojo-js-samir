﻿try {
    if (!formularioValido) {
        alert("Ocorreu um erro no formulário. Impossível salvar.");
        event.returnValue = false;
        return false;
    }
    
    //Validar dias
    function DiasArray(n) 
    {
	    for (var i = 1; i <= n; i++) {
		  this[i] = 31
		  if (i==4 || i==6 || i==9 || i==11) {this[i] = 30}
		  if (i==2) {this[i] = 29}
         } 
         return this;
     }
     function DiasFevereiro(year){
	    // February has 29 days in any year evenly divisible by four,
        // EXCEPT for centurial years which are not also divisible by 400.
        return (((year % 4 == 0) && ( (!(year % 100 == 0)) || (year % 400 == 0))) ? 29 : 28 );
     }

     ValidarDataComissao = function(strDay, msg) {
         var year = crmForm.all.new_ano_competencia.DataValue;
         var month = crmForm.all.new_mes_competencia.DataValue;
         var day = parseInt(strDay);

         var daysInMonth = DiasArray(12)
         if (day < 1 || day > 31 || (month == 2 && day > DiasFevereiro(year)) || day > daysInMonth[month]) {
             alert("Informe um dia válido no campo " + msg + " para o mês selecionado.")
             return false
         }
         return true;
     }
     var digitalizacaoRPA =  ValidarDataComissao(crmForm.all.new_dia_limite_digitalizacao_rpa.DataValue, "Dia Limite Digitalização RPA");
     if (!digitalizacaoRPA) {event.returnValue = false;return false;}
     var diaVencPedido =  ValidarDataComissao(crmForm.all.new_dia_venc_pedido.DataValue, "Dia Limite Vencimento Pedido");
     if (!diaVencPedido) {event.returnValue = false;return false;}
     var digitalizacaoNF =  ValidarDataComissao(crmForm.all.new_dia_limite_digitalizacao_nf.DataValue, "Dia Limite Digitalização NF");
     if (!digitalizacaoNF) {event.returnValue = false;return false;}
    
    // Transformar caracteres em maiusculos//
    var elm = document.getElementsByTagName("input");
    for (i = 0; i < elm.length; i++) {
        if (elm[i].type == "text")
            if (elm[i].DataValue != null) {
            try {
                elm[i].DataValue = elm[i].DataValue.toUpperCase();
            }
            catch (e) { }
        }
    }
    crmForm.all.new_name.Disabled = false;
} 
catch (error) 
{
    alert("Ocorreu um erro no formulário.\n" + error.description);
    event.returnValue = false;
    return false;
}