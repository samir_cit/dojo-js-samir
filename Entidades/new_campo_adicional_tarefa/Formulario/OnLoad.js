﻿formularioValido = true;
try {
    crmForm.ObterItemSelecionado = function() {
        var item = new Object();
        item.valor = "";
        item.nome = "";
        for (var index = 0; index < crmForm.all.new_atributo.length; index++) {
            if (crmForm.all.new_atributo[index].selected) {
                item.valor = crmForm.all.new_atributo[index].value;
                item.nome = crmForm.all.new_atributo[index].text;
                break;
            }
        }

        if (item.valor == "") {
            alert("Selecione um atributo.");
            return false;
        }
        return item;
    }

    crmForm.SelecionarItem = function(valor) {
        for (var index = 0; index < crmForm.all.new_atributo.length; index++) {
            if (crmForm.all.new_atributo[index].value == valor) {
                crmForm.all.new_atributo[index].selected = true;
                break;
            }
        }
    }

    var cmd = new RemoteCommand("MrvService", "ObterAtributosParaCamposAdicionaisTarefa", "/MRVCustomizations/");
    var result = cmd.Execute();
    if (result.Success) {
        if (result.ReturnValue.Mrv.Error)
            alert(result.ReturnValue.Mrv.Error);
        else if (result.ReturnValue.Mrv.ErrorSoap)
            alert(result.ReturnValue.Mrv.ErrorSoap)
        else {
            var valor = (crmForm.all.new_atributo.DataValue != null) ? crmForm.all.new_atributo.DataValue : "";

            //crmForm.all.new_atributo.parentElement.innerHTML = "<select req=\"0\" id=\"new_atributo\" name=\"new_atributo\" class=\"selectBox\" tabindex=\"1110\"><option value=''></option><option value='1'>Item 1</option><option value='2'>Item 2</option><option value='3'>Item 3</option></select>";
            crmForm.all.new_atributo.parentElement.innerHTML = result.ReturnValue.Mrv.Atributos.toString();
            crmForm.SelecionarItem(valor);
        }
    }

    if (crmForm.all.new_name.DataValue == null)
        crmForm.all.new_name.DataValue = "Valor Indefinido.";

    crmForm.all.new_name_c.style.display = "none";
    crmForm.all.new_name_d.style.display = "none";

} catch (error) {
    formularioValido = false;
    alert("Ocorreu um erro no formulário.\n" + error.description);
}