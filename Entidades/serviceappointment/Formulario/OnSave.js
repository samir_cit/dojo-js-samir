﻿try {
    if (!formularioValido) {
        alert("Ocorreu um erro no formulário. Impossível salvar.");
        event.returnValue = false;
        return false;
    }
    
    // Transformar caracteres em maiusculos------------------------------------//
    var elm = document.getElementsByTagName("input");
    for (i = 0; i< elm.length; i++)
    {
        if (elm[i].type == "text")
            if (elm[i].DataValue != null)
            { 
                 try{
                  elm[i].DataValue = elm[i].DataValue.toUpperCase(); }
                  catch (e) {}
             }
    }

    
} catch (error) {
    alert("Ocorreu um erro no formulário.\n" + error.description);
    event.returnValue = false;
    return false;
}