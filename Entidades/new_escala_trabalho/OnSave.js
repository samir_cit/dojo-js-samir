﻿formularioValido = true;
crmForm.ValidaEscala = function(){
    var diaPreenchido = false;
    var mensagem = '';
    diaPreenchido = crmForm.all.new_inicio_dia1.DataValue || crmForm.all.new_fim_dia1.DataValue;    
    if (diaPreenchido && (crmForm.all.new_inicio_dia1.DataValue == null || crmForm.all.new_fim_dia1.DataValue == null) ) 
        mensagem = 'Escala do dia 1 inválida.\n';
    
    diaPreenchido = crmForm.all.new_inicio_dia2.DataValue || crmForm.all.new_fim_dia2.DataValue;
    if (diaPreenchido && (crmForm.all.new_inicio_dia2.DataValue == null || crmForm.all.new_fim_dia2.DataValue == null) )
        mensagem += 'Escala do dia 2 inválida.\n';
    
    diaPreenchido = crmForm.all.new_inicio_dia3.DataValue || crmForm.all.new_fim_dia3.DataValue;            
    if (diaPreenchido && (crmForm.all.new_inicio_dia3.DataValue == null || crmForm.all.new_fim_dia3.DataValue == null) )
        mensagem += 'Escala do dia 3 inválida.\n';
    
    diaPreenchido = crmForm.all.new_inicio_dia4.DataValue || crmForm.all.new_fim_dia4.DataValue;
    if (diaPreenchido && ( crmForm.all.new_inicio_dia4.DataValue == null || crmForm.all.new_fim_dia4.DataValue == null) )
        mensagem += 'Escala do dia 4 inválida.\n';
    
    diaPreenchido = crmForm.all.new_inicio_dia5.DataValue || crmForm.all.new_fim_dia5.DataValue;
    if (diaPreenchido && ( crmForm.all.new_inicio_dia5.DataValue == null || crmForm.all.new_fim_dia5.DataValue == null) )
        mensagem += 'Escala do dia 5 inválida.\n';
    
    diaPreenchido = crmForm.all.new_inicio_dia6.DataValue || crmForm.all.new_fim_dia6.DataValue;
    if (diaPreenchido && (crmForm.all.new_inicio_dia6.DataValue == null || crmForm.all.new_fim_dia6.DataValue == null) )
        mensagem += 'Escala do dia 6 inválida.\n';
    if(crmForm.ValidaRepeticao())
        mensagem += 'Escala inválida. Possui dias repetidos.\n';
    return mensagem;
}

crmForm.ValidaRepeticao = function(){
     var dias = []; 
     var repetiu = false;
     dias[0] = crmForm.all.new_tipo_dia1.DataValue;
     dias[1] = crmForm.all.new_tipo_dia2.DataValue;
     dias[2] = crmForm.all.new_tipo_dia3.DataValue;
     dias[3] = crmForm.all.new_tipo_dia4.DataValue;
     dias[4] = crmForm.all.new_tipo_dia5.DataValue;
     dias[5] = crmForm.all.new_tipo_dia6.DataValue;
    for	(i = 0; i <= 5; i++) {
        for	(j = 0; j <= 5; j++) {
         repetiu = (dias[i] == dias[j] && i != j);
         if(repetiu)break;
        }
        if(repetiu)break;
    }
    return repetiu;
}

crmForm.ValidaHorario = function(){
     var horasInicio = []; 
     var horasFim = [];    
     var horaIni;
     var horaFim;  
     horasInicio[0] = crmForm.all.new_inicio_dia1.DataValue;
     horasInicio[1] = crmForm.all.new_inicio_dia2.DataValue;
     horasInicio[2] = crmForm.all.new_inicio_dia3.DataValue;
     horasInicio[3] = crmForm.all.new_inicio_dia4.DataValue;
     horasInicio[4] = crmForm.all.new_inicio_dia5.DataValue;
     horasInicio[5] = crmForm.all.new_inicio_dia6.DataValue;
     horasFim[0] = crmForm.all.new_fim_dia1.DataValue;
     horasFim[1] = crmForm.all.new_fim_dia2.DataValue;
     horasFim[2] = crmForm.all.new_fim_dia3.DataValue;
     horasFim[3] = crmForm.all.new_fim_dia4.DataValue;
     horasFim[4] = crmForm.all.new_fim_dia5.DataValue;
     horasFim[5] = crmForm.all.new_fim_dia6.DataValue;
    for	(i = 0; i <= 5; i++) {
        horaIni = horasInicio[i].split(':');
        horaFim = horasFim[i].split(':');
        if(parseInt(horaFim[0], 10) < parseInt(horaIni[0], 10) )
        {
            return "Escala possui hora fim menor que hora início na linha " + (i + 1).toString() + ".";
        }        
    }
    return '';
}

crmForm.SomaHoras = function(){
   var horasTotal = 0;
   var minutosTotal = 0;
   var horaIni;
   var horaFim;
   if( crmForm.all.new_inicio_dia1.DataValue && crmForm.all.new_fim_dia1.DataValue)
   {
     horaIni = crmForm.all.new_inicio_dia1.DataValue.split(':');
     horaFim = crmForm.all.new_fim_dia1.DataValue.split(':');
     horasTotal = parseInt(horaFim[0], 10) - parseInt(horaIni[0], 10);
     minutosTotal = parseInt(horaFim[1], 10) - parseInt(horaIni[1], 10);
     if(minutosTotal < 0)
     {
       minutosTotal = 60 + minutosTotal;
       horasTotal -= 1;
     }
   }
   if( crmForm.all.new_inicio_dia2.DataValue && crmForm.all.new_fim_dia2.DataValue)
   {
     horaIni = crmForm.all.new_inicio_dia2.DataValue.split(':');
     horaFim = crmForm.all.new_fim_dia2.DataValue.split(':');
     horasTotal += parseInt(horaFim[0], 10) - parseInt(horaIni[0], 10);
     minutosTotal += parseInt(horaFim[1], 10) - parseInt(horaIni[1], 10);
     if(minutosTotal < 0)
     {
       minutosTotal = 60 + minutosTotal;
       horasTotal -= 1;
     }
   }
   if( crmForm.all.new_inicio_dia3.DataValue && crmForm.all.new_fim_dia3.DataValue)
   {
     horaIni = crmForm.all.new_inicio_dia3.DataValue.split(':');
     horaFim = crmForm.all.new_fim_dia3.DataValue.split(':');
     horasTotal += parseInt(horaFim[0], 10) - parseInt(horaIni[0], 10);
     minutosTotal += parseInt(horaFim[1], 10) - parseInt(horaIni[1], 10);
     if(minutosTotal < 0)
     {
       minutosTotal = 60 + minutosTotal;
       horasTotal -= 1;
     }
   }
   if( crmForm.all.new_inicio_dia4.DataValue && crmForm.all.new_fim_dia4.DataValue)
   {
     horaIni = crmForm.all.new_inicio_dia4.DataValue.split(':');
     horaFim = crmForm.all.new_fim_dia4.DataValue.split(':');
     horasTotal += parseInt(horaFim[0], 10) - parseInt(horaIni[0], 10);
     minutosTotal += parseInt(horaFim[1], 10) - parseInt(horaIni[1], 10);
     if(minutosTotal < 0)
     {
       minutosTotal = 60 + minutosTotal;
       horasTotal -= 1;
     }
   }
   if( crmForm.all.new_inicio_dia5.DataValue && crmForm.all.new_fim_dia5.DataValue)
   {
     horaIni = crmForm.all.new_inicio_dia5.DataValue.split(':');
     horaFim = crmForm.all.new_fim_dia5.DataValue.split(':');
     horasTotal += parseInt(horaFim[0], 10) - parseInt(horaIni[0], 10);
     minutosTotal += parseInt(horaFim[1], 10) - parseInt(horaIni[1], 10);
     if(minutosTotal < 0)
     {
       minutosTotal = 60 + minutosTotal;
       horasTotal -= 1;
     }
   }
   if( crmForm.all.new_inicio_dia6.DataValue && crmForm.all.new_fim_dia6.DataValue)
   {
     horaIni = crmForm.all.new_inicio_dia6.DataValue.split(':');
     horaFim = crmForm.all.new_fim_dia6.DataValue.split(':');
     horasTotal += parseInt(horaFim[0], 10) - parseInt(horaIni[0], 10);
     minutosTotal += parseInt(horaFim[1], 10) - parseInt(horaIni[1], 10);
     if(minutosTotal < 0)
     {
       minutosTotal = 60 + minutosTotal;
       horasTotal -= 1;
     }
   }

   while(minutosTotal > 60)
   {
    horasTotal += 1;
    minutosTotal = minutosTotal - 60;
   }
   crmForm.all.new_total_horas.DataValue = horasTotal.toString() + ':' + minutosTotal.toString();
   crmForm.all.new_total_horas.ForceSubmit = true;
}

try {
    if (crmForm.all.new_name.IsDirty && crmForm.all.new_name.DataValue) {
        crmForm.all.new_name.DataValue = crmForm.all.new_name.DataValue.toUpperCase();
    }
    
    var mensagem = crmForm.ValidaEscala();
    if(mensagem == '')
        mensagem = crmForm.ValidaHorario();
    if(mensagem != '')
    {
        alert("A escala contém erro de preenchimento:\n" + mensagem);
        event.returnValue = false;
        return false;
    }
    crmForm.SomaHoras();
    //Limpa datas de controle
    crmForm.all.new_data_ultimo_desbloqueio.DataValue = null;
    crmForm.all.new_data_ultimo_bloqueio.DataValue = null;
    
} catch (error) {
    formularioValido = false;
    alert("Ocorreu um erro no formulário.\n" + error.description);
}