﻿formularioValido = true;
crmForm.FormataHora = function(oField) {
    try {
       if (typeof (oField) == "undefined" || oField.DataValue == null) return;
        var sTmp = oField.DataValue.replace(/[^0-9]/g, "");
        if (sTmp.length == 4) {
            oField.DataValue = sTmp.substr(0, 2) + ":" + sTmp.substr(2, 2);
        }
        else {
            oField.DataValue = null;
            oField.SetFocus();
            alert("Campo fora da faixa. Informe hora no padrão HH:MM e faixa de 00:00 as 24:00!");
        }
    }
    catch (error) {
        alert("Ocorreu um erro no formulário.\n" + error.description);
    }
}

try 
{

} catch (error) {
    formularioValido = false;
    alert("Ocorreu um erro no formulário.\n" + error.description);
}