﻿formularioValido = true;
try {

    crmForm.SelecionarItem = function(atributoCorrente, valor) {
        var atributo = eval("crmForm.all." + atributoCorrente);
        for (var index = 0; index < atributo.length; index++) {
            if (atributo[index].value == valor) {
                atributo[index].selected = true;
                break;
            }
        }
    }

    crmForm.CarregarPicklist = function(atributo, atributoCorrente, entidade) {
        var objAtt = eval("crmForm.all." + atributoCorrente);
        //Verificar se existe valor nos campos que simulam picklist
        var valorAtt = null;
        if (objAtt.DataValue != null)
            valorAtt = objAtt.DataValue;

        var cmd = new RemoteCommand("MrvService", "ObterHTMLPicklistDinamico", "/MRVCustomizations/");
        cmd.SetParameter("entidade", entidade);
        cmd.SetParameter("atributo ", atributo);
        cmd.SetParameter("atributoCorrente", atributoCorrente);
        var result = cmd.Execute();
        if (result.Success) {
            if (result.ReturnValue.Mrv.Error) {
                alert(result.ReturnValue.Mrv.Error);
                return "";
            }
            if (result.ReturnValue.Mrv.ErrorSoap) {
                alert(result.ReturnValue.Mrv.ErrorSoap);
                return "";
            }
            else {
                objAtt.parentElement.innerHTML = result.ReturnValue.Mrv.Atributos.toString();
                if (valorAtt != null)
                    crmForm.SelecionarItem(atributoCorrente, valorAtt);
            }
        } else {
            alert("Erro na consulta do WebMethod ObterHTMLPicklistDinamico.");
        }
    }
        
    crmForm.all.new_status_proposta.FireOnChange();
    crmForm.all.new_status_contrato.FireOnChange();
    crmForm.all.new_status_proposta.Disabled = true;
    crmForm.all.new_status_contrato.Disabled = true;
    
} catch (error) {
    formularioValido = false;
    alert("Ocorreu um erro no formulário.\n" + error.description);
}