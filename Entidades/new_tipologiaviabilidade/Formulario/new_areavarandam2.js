﻿if (crmForm.all.new_areavarandam2.DataValue != null) {
    crmForm.all.new_areaequivalentem2.DataValue = crmForm.CalcularAreaEquivalenteEstimada(
            crmForm.all.new_areaaptom2.DataValue,
            crmForm.all.new_areavarandam2.DataValue,
            crmForm.all.new_areaterracocom2.DataValue,
            crmForm.all.new_indicevarandaequiv.DataValue,
            crmForm.all.new_indiceterracoequiv.DataValue,
            crmForm.all.new_coeficienteareaequiv.DataValue
        );
        
     crmForm.all.new_area_equivalente_tabelao_m2.DataValue = crmForm.CalcularAreaEquivalenteEstimada(
            crmForm.all.new_areaaptom2.DataValue,
            crmForm.all.new_areavarandam2.DataValue,
            crmForm.all.new_areaterracocom2.DataValue,
            crmForm.all.new_indicevarandaequiv.DataValue,
            crmForm.all.new_indiceterracoequiv.DataValue,
            crmForm.all.new_coeficiente_area_equiv_tabelao.DataValue
        );

    var nome = "";

    if (crmForm.all.new_nomedatipologia.DataValue != null)
        nome = crmForm.all.new_nomedatipologia.SelectedText;

    if (nome.indexOf("COB") == -1) {
        crmForm.all.new_precovenda.DataValue = crmForm.CalcularPrecoVendaEstimadoPorUnidadeNormal(
                crmForm.all.new_areaaptom2.DataValue,
                crmForm.all.new_areavarandam2.DataValue,
                crmForm.all.new_vendam2.DataValue
            );
    }