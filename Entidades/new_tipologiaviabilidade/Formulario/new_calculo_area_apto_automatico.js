﻿var calculoAutomatico = crmForm.all.new_calculo_area_apto_automatico.DataValue;

if (calculoAutomatico) {
    //Se não há tipologia base
    if (!crmForm.RetornaTipologiaBase())
        crmForm.all.new_calculo_area_apto_automatico.DataValue = false;
}

//Habilita os campos de área em função do valor selecionado no campo de Cálculo automático
crmForm.all.new_areaaptom2.Disabled = calculoAutomatico;
crmForm.all.new_areavarandam2.Disabled = calculoAutomatico;
crmForm.all.new_area_equivalente_tabelao_m2.Disabled = calculoAutomatico;
crmForm.all.new_areaterracocom2.Disabled = calculoAutomatico;
crmForm.all.new_precovenda.Disabled = calculoAutomatico;

