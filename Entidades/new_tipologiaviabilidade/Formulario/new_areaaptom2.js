﻿//SEM COBERTURA
if (crmForm.all.new_areaaptom2.DataValue != null) {
    //Quando há alteração de valor no campo na área do apto deve-se recalcular a área equivalente
    crmForm.all.new_areaequivalentem2.DataValue = crmForm.CalcularAreaEquivalenteEstimada(
            crmForm.all.new_areaaptom2.DataValue,
            crmForm.all.new_areavarandam2.DataValue,
            crmForm.all.new_areaterracocom2.DataValue,
            crmForm.all.new_indicevarandaequiv.DataValue,
            crmForm.all.new_indiceterracoequiv.DataValue,
            crmForm.all.new_coeficienteareaequiv.DataValue
        );

    //Quando há alteração de valor no campo na área do apto deve-se recalcular a área equivalente tabelão
    crmForm.all.new_area_equivalente_tabelao_m2.DataValue = crmForm.CalcularAreaEquivalenteEstimada(
            crmForm.all.new_areaaptom2.DataValue,
            crmForm.all.new_areavarandam2.DataValue,
            crmForm.all.new_areaterracocom2.DataValue,
            crmForm.all.new_indicevarandaequiv.DataValue,
            crmForm.all.new_indiceterracoequiv.DataValue,
            crmForm.all.new_coeficiente_area_equiv_tabelao.DataValue
        );
    
    var nome = "";
    if (crmForm.all.new_nomedatipologia.DataValue != null)
        nome = crmForm.all.new_nomedatipologia.SelectedText;
    //Caso seja uma tipologia tipo COB, chama-se a função RetornaTipologiaBase
    if (nome.indexOf("COB") >= 0) {
        crmForm.RetornaTipologiaBase(false, true);
    }//No caso de uma tipologia normal (base), calcula-se o valor do preço de venda estimado da unidade
    else {
        crmForm.all.new_precovenda.DataValue = 
        (crmForm.all.new_areaaptom2.DataValue + crmForm.all.new_areavarandam2.DataValue) * crmForm.all.new_vendam2.DataValue;
    }
}
if (crmForm.all.new_areaequivalenterealm2.DataValue == null)
    crmForm.all.new_areaequivalenterealm2.DataValue = crmForm.all.new_areaequivalentem2.DataValue;

