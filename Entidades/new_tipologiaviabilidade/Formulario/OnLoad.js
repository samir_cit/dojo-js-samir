﻿formularioValido = true;

try {
    if (crmForm.FormType == 1)
        crmForm.all.new_numeroapto.DataValue = 1;

    // Salva os valores do nome e do tamanho da tipologia
    TamanhoTipologia = crmForm.all.new_tamanho.DataValue;
    NomeTipologia = crmForm.all.new_nomedatipologia.DataValue != null ? crmForm.all.new_nomedatipologia.SelectedText : "";

    /* Funções Públicas de Cálculos */
    crmForm.CalcularAreaAptoCOB = function(areaApto, areaVaranda, indiceAptoCOB) {
        return (areaApto + areaVaranda) * indiceAptoCOB;
    }
    crmForm.CalcularAreaDoTerracoCOB = function(areaApto, areaVaranda, indiceTerracoCOB, areaAptoCOB) {
        return ((areaApto + areaVaranda) * indiceTerracoCOB) - areaAptoCOB;
    }
    crmForm.CalcularAreaEquivalenteEstimada = function(areaApto, areaVaranda, areaTerraco, indiceVarandaEquivalente, indiceTerracoEquivalente, coeficienteAreaEquivalente) {
        return ((areaApto + (areaVaranda * indiceVarandaEquivalente) + (areaTerraco * indiceTerracoEquivalente)) * coeficienteAreaEquivalente);
    }
    crmForm.CalcularAreaEquivalenteEstimadaEngenharia = function(areaAptoEng, areaTerracoCob, indiceAreaTerracoCobEng, areaVaranda, indiceAreaVarandaEng, areaTerracoDescoberta, indiceAreaTerracoDescobertaEng, areaPrivativaDescoberta, indiceAreaPrivativaDescobertaEng, indiceCoeficienteAreaEquivEng) {
    return (areaAptoEng + (areaTerracoCob * indiceAreaTerracoCobEng) + (areaVaranda * indiceAreaVarandaEng) + (areaTerracoDescoberta * indiceAreaTerracoDescobertaEng) + (areaPrivativaDescoberta * indiceAreaPrivativaDescobertaEng)) * indiceCoeficienteAreaEquivEng;
    }
    crmForm.CalcularPrecoVendaEstimadoPorUnidadeCOB = function(precoVenda, indicePrecoVendaCOB) {
        return precoVenda * indicePrecoVendaCOB;
    }
    crmForm.CalcularPrecoVendaEstimadoPorUnidadeNormal = function(areaApto, areaVaranda, vendaM2) {
        return (areaApto + areaVaranda) * vendaM2;
    }
    /**/

    if (crmForm.all.new_areaaptom2.DataValue == null)
        crmForm.all.new_areaaptom2.DataValue = 0.00;
    if (crmForm.all.new_areavarandam2.DataValue == null)
        crmForm.all.new_areavarandam2.DataValue = 0.00;
    if (crmForm.all.new_areaterracocom2.DataValue == null)
        crmForm.all.new_areaterracocom2.DataValue = 0.00;
    if (crmForm.all.new_areaequivalentem2.DataValue == null)
        crmForm.all.new_areaequivalentem2.DataValue = 0.00;
    if (crmForm.all.new_area_equivalente_tabelao_m2.DataValue == null)
        crmForm.all.new_area_equivalente_tabelao_m2.DataValue = 0.00;
    if (crmForm.all.new_precovenda.DataValue == null)
        crmForm.all.new_precovenda.DataValue = 0.00;
    //**//
    if (crmForm.all.new_area_apto_engenharia.DataValue == null)
        crmForm.all.new_area_apto_engenharia.DataValue = 0.00;
    if (crmForm.all.new_area_terraco_coberta.DataValue == null)
        crmForm.all.new_area_terraco_coberta.DataValue = 0.00;
    if (crmForm.all.new_area_terraco_descoberta.DataValue == null)
        crmForm.all.new_area_terraco_descoberta.DataValue = 0.00;
    if (crmForm.all.new_area_varanda.DataValue == null)
        crmForm.all.new_area_varanda.DataValue = 0.00;
    if (crmForm.all.new_area_privativa_descoberta.DataValue == null)
        crmForm.all.new_area_privativa_descoberta.DataValue = 0.00;
    if (crmForm.all.new_coeficiente_area_equiv_eng.DataValue == null)
        crmForm.all.new_coeficiente_area_equiv_eng.DataValue = 0.00;
    if (crmForm.all.new_areaequivalenterealm2.DataValue == null)
        crmForm.all.new_areaequivalenterealm2.DataValue = 0.00;
    //**//

    // Habilita/Desabilita área do apto, área do terraço e cálculo automático das áreas
    if (NomeTipologia.indexOf("COB") >= 0) {
        crmForm.all.new_calculo_area_apto_automatico_c.style.visibility = 'visible';
        crmForm.all.new_calculo_area_apto_automatico_d.style.visibility = 'visible';

        if (crmForm.all.new_calculo_area_apto_automatico.DataValue) {
            crmForm.all.new_areaaptom2.Disabled = true;
            crmForm.all.new_areavarandam2.Disabled = true;
            crmForm.all.new_areaterracocom2.Disabled = true;
        }
        else {
            crmForm.all.new_areaaptom2.Disabled = false;
            crmForm.all.new_areavarandam2.Disabled = false;
            crmForm.all.new_areaterracocom2.Disabled = false;
        }
    }
    else {
        crmForm.all.new_calculo_area_apto_automatico_c.style.visibility = 'hidden';
        crmForm.all.new_calculo_area_apto_automatico_d.style.visibility = 'hidden';
    }

    // Funçao para verificar se existe a tipologia base de uma cobertura (COB).
    // Se existir, calcula a área do apto e do terraço a partir desta tipologia base
    crmForm.RetornaTipologiaBase = function() {
        //**Procura a tipologia base da tipologia COB selecionada na lista**//
        if (NomeTipologia.indexOf("COB") >= 0) {
            var codigoBase = 0;
            var tipoBase = NomeTipologia.replace(" COB", "");
            var combo = crmForm.all.new_nomedatipologia;
            for (index = 0; index < combo.length; index++) {
                if (combo.options[index].text == tipoBase) {
                    codigoBase = combo.options[index].value;
                    break;
                }
            }

            if (codigoBase == 0) {
                alert("Não existe a tipologia " + tipoBase + "!")
                return false;
            }
            //**Fim**//

            //**Busca os dados da tipologia base**//
            if (crmForm.all.new_blocoviabilidadeid.DataValue && crmForm.all.new_tamanho.DataValue != null) {
                var cmd = new RemoteCommand("MrvService", "RetornaTipologiaBase", "/MRVCustomizations/");
                cmd.SetParameter("blocoId", crmForm.all.new_blocoviabilidadeid.DataValue[0].id);
                cmd.SetParameter("codigoTipologia", codigoBase);
                cmd.SetParameter("tamanhoTipologia", crmForm.all.new_tamanho.DataValue);
                var result = cmd.Execute();

                if (result.Success) {
                    if (result.ReturnValue.Mrv.Error) {
                        alert(result.ReturnValue.Mrv.Error);
                        return false;
                    }
                    else if (result.ReturnValue.Mrv.ErrorSoap) {
                        alert(result.ReturnValue.Mrv.ErrorSoap);
                        return false;
                    }
                    //**Verifica se existe a tipologia base cadastrada no CRM
                    if (result.ReturnValue.Mrv.ExisteTipologiaBase) {
                        // Carrega variáveis para cálculo
                        var area_apto = result.ReturnValue.Mrv.area_apto.toString().indexOf(",") >= 0 ? parseFloat(result.ReturnValue.Mrv.area_apto.replace(',', '.')) : result.ReturnValue.Mrv.area_apto;
                        var area_varanda = result.ReturnValue.Mrv.area_varanda.toString().indexOf(",") >= 0 ? parseFloat(result.ReturnValue.Mrv.area_varanda.replace(',', '.')) : result.ReturnValue.Mrv.area_varanda;
                        var area_terraco = result.ReturnValue.Mrv.area_terraco.toString().indexOf(",") >= 0 ? parseFloat(result.ReturnValue.Mrv.area_terraco.replace(',', '.')) : result.ReturnValue.Mrv.area_terraco;
                        var preco_venda = result.ReturnValue.Mrv.preco_venda.toString().indexOf(",") >= 0 ? parseFloat(result.ReturnValue.Mrv.preco_venda.replace(',', '.')) : result.ReturnValue.Mrv.preco_venda;
                        //**//

                        //**Se cálculo automático**//
                        if (crmForm.all.new_calculo_area_apto_automatico.DataValue) {
                            crmForm.all.new_areaaptom2.DataValue = crmForm.CalcularAreaAptoCOB(area_apto, area_varanda, crmForm.all.new_indiceaptocob.DataValue);
                            crmForm.all.new_areaterracocom2.DataValue = crmForm.CalcularAreaDoTerracoCOB(area_apto, area_varanda, crmForm.all.new_indiceterracocob.DataValue, crmForm.all.new_areaaptom2.DataValue);
                        }
                        //**//
                        crmForm.all.new_precovenda.DataValue = crmForm.CalcularPrecoVendaEstimadoPorUnidadeCOB(preco_venda, crmForm.all.new_indiceprecovendacob.DataValue);
                        crmForm.all.new_areaequivalentem2.DataValue = crmForm.CalcularAreaEquivalenteEstimada(crmForm.all.new_areaaptom2.DataValue, crmForm.all.new_areavarandam2.DataValue, crmForm.all.new_areaterracocom2.DataValue, crmForm.all.new_indicevarandaequiv.DataValue, crmForm.all.new_indiceterracoequiv.DataValue, crmForm.all.new_coeficienteareaequiv.DataValue);
                        crmForm.all.new_area_equivalente_tabelao_m2.DataValue = crmForm.CalcularAreaEquivalenteEstimada(crmForm.all.new_areaaptom2.DataValue, crmForm.all.new_areavarandam2.DataValue, crmForm.all.new_areaterracocom2.DataValue, crmForm.all.new_indicevarandaequiv.DataValue, crmForm.all.new_indiceterracoequiv.DataValue, crmForm.all.new_coeficiente_area_equiv_tabelao.DataValue);
                        crmForm.all.new_areaequivalenterealm2.DataValue = crmForm.CalcularAreaEquivalenteEstimadaEngenharia(crmForm.all.new_area_apto_engenharia.DataValue, crmForm.all.new_area_terraco_coberta.DataValue, crmForm.all.new_indice_area_terraco_coberto_engenharia.DataValue, crmForm.all.new_area_varanda.DataValue, crmForm.all.new_indice_area_varanda_engenharia.DataValue, crmForm.all.new_area_terraco_descoberta.DataValue, crmForm.all.new_indice_area_terraco_descoberto_engenharia.DataValue, crmForm.all.new_area_privativa_descoberta.DataValue, crmForm.all.new_indice_area_priv_descoberta_engenharia.DataValue, crmForm.all.new_coeficiente_area_equiv_eng.DataValue);
                        //**//
                    }
                    else {
                        alert("Não existe a tipologia " + tipoBase + " com o tamanho " + crmForm.all.new_tamanho.SelectedText + "!")
                        return false;
                    }
                }
                else {
                    alert('Erro na comunicação com o MrvService. Contate o administrador do sistema.');
                    return false;
                }
            }
        }
        else {
            crmForm.all.new_calculo_area_apto_automatico_c.style.visibility = 'hidden';
            crmForm.all.new_calculo_area_apto_automatico_d.style.visibility = 'hidden';
            //**//
            crmForm.all.new_areaequivalenterealm2.DataValue = crmForm.CalcularAreaEquivalenteEstimadaEngenharia(crmForm.all.new_area_apto_engenharia.DataValue, crmForm.all.new_area_terraco_coberta.DataValue, crmForm.all.new_indice_area_terraco_coberto_engenharia.DataValue, crmForm.all.new_area_varanda.DataValue, crmForm.all.new_indice_area_varanda_engenharia.DataValue, crmForm.all.new_area_terraco_descoberta.DataValue, crmForm.all.new_indice_area_terraco_descoberto_engenharia.DataValue, crmForm.all.new_area_privativa_descoberta.DataValue, crmForm.all.new_indice_area_priv_descoberta_engenharia.DataValue, crmForm.all.new_coeficiente_area_equiv_eng.DataValue);
            //**//
        }

        return true;
    }

    //Função para desabilitar campos do formulário
    //Autor: Alexandre Negrão
    //Data: 9/3/2010
    function DesabilitarCampos(campos) {
        var tmp = campos.split(";");
        for (index = 0; index < tmp.length; index++) {
            if (tmp[index] != "") {
                var object = eval("crmForm.all." + tmp[index]);
                object.Disabled = true;
            }
        }
    }

    crmForm.DesabilitaCamposSeViabilidadeGerada = new function() {
        if ((crmForm.FormType == 2) && (crmForm.new_blocoviabilidadeid.DataValue != null)) {
            var attributes = "new_viabilidadeid";
            var cmd = new RemoteCommand("MrvService", "GetEntity", "/MRVCustomizations/");

            cmd.SetParameter("entity", "new_blocoviabilidade");
            cmd.SetParameter("primaryField", "new_blocoviabilidadeid");
            cmd.SetParameter("entityId", crmForm.new_blocoviabilidadeid.DataValue[0].id);
            cmd.SetParameter("fieldList", attributes);

            var oResult = cmd.Execute();
            if (oResult.Success) {
                if (oResult.ReturnValue.Mrv.new_viabilidadeid != null) {
                    attributes = "new_viabilidadegerada";
                    cmd = new RemoteCommand("MrvService", "GetEntity", "/MRVCustomizations/");

                    cmd.SetParameter("entity", "new_viabilidade");
                    cmd.SetParameter("primaryField", "new_viabilidadeid");
                    cmd.SetParameter("entityId", oResult.ReturnValue.Mrv.new_viabilidadeid);
                    cmd.SetParameter("fieldList", attributes);

                    oResult = cmd.Execute();
                    if (oResult.Success) 
                    {
                        if (oResult.ReturnValue.Mrv.new_viabilidadegerada != null) 
                        {
                            if (oResult.ReturnValue.Mrv.new_viabilidadegerada == 1) 
                            {
                                var campos =
                                [
                                    "new_nomedatipologia",
                                    "new_tamanho",
                                    "new_numeroapto",
                                    "new_coeficienteareaequiv",
                                    "new_vendam2",
                                    "new_calculo_area_apto_automatico",
                                    "new_precovenda",
                                    "new_blocoviabilidadeid",
                                    "new_box",
                                    "new_garagem",
                                    "new_qtdegaragem",
                                    "new_garagemopcao2",
                                    "new_qtdegaragem2"
                                ];

                                for (var i = 1; i <= 15; i++) 
                                {
                                    campos.push("new_numeracaoinicialcoluna" + i);
                                    campos.push("new_qtdunidadescoluna" + i);
                                    campos.push("new_posicaohorizontalcoluna" + i);
                                    campos.push("new_posicaoverticalcoluna" + i);
                                }

                                DesabilitarCampos(campos.join(";"));
                            }
                        }
                    }
                }
            }
        }
    }

    //Função para esconder seções,e seus controles, no formulário
    //Autor: Alexandre Negrão
    //Data: 9/3/2010
    function EscondeSecao(camposParent, esconde) {
        var tipo = "inline";

        if (esconde)
            tipo = "none";

        var tmp = camposParent.split(";");

        for (index = 0; index < tmp.length; index++)
            document.getElementById(tmp[index] + "_d").parentNode.parentNode.style.display = tipo;
    }

    crmForm.VerificaFuncao = function(nomeFuncao) {
        var cmd = new RemoteCommand("MrvService", "UserContainsTeams", "/MrvCustomizations/");
        cmd.SetParameter("teams", nomeFuncao);
        var resultado = cmd.Execute();
        if (resultado.Success) {
            return resultado.ReturnValue.Mrv.Found;
        } else {
            alert("Erro na consulta do método UserContainsTeams");
            formularioValido = false;
            return false;
        }
    }

    function DisabledField(Fields) {
        var tmp = Fields.split(";");
        for (index = 0; index < tmp.length; index++) {
            var object = eval("crmForm.all." + tmp[index])
            if (object != null) {
                object.Disabled = true;
            }
        }
    }

    function HabilitarParaEngenharia(habilitar) {
        crmForm.all.new_permuta_engenharia.Disabled = habilitar;
        crmForm.all.new_preco_venda_tabelao_unidade.Disabled = habilitar;
        crmForm.all.new_pereco_venda_engenharia.Disabled = habilitar;
        crmForm.all.new_area_apto_engenharia.Disabled = habilitar;
        crmForm.all.new_area_privativa_descoberta.Disabled = habilitar;
        crmForm.all.new_area_terraco_coberta.Disabled = habilitar;
        crmForm.all.new_coeficiente_area_equiv_eng.Disabled = habilitar;
        crmForm.all.new_area_terraco_descoberta.Disabled = habilitar;
        crmForm.all.new_area_varanda.Disabled = habilitar;
        crmForm.all.new_area_casa.Disabled = habilitar;
        crmForm.all.new_area_varanda_coberta_fundos.Disabled = habilitar;
        crmForm.all.new_area_pergolado.Disabled = habilitar;
        crmForm.all.new_area_varanda_coberta_frente.Disabled = habilitar;
        crmForm.all.new_area_sob_telhado.Disabled = habilitar;
        crmForm.all.new_coef_area_equiv_tab_real_casa_engenharia.Disabled = habilitar;
        crmForm.all.new_area_sacada_real.Disabled = habilitar;
        crmForm.all.new_area_equiv_tab_real_casa_eng.Disabled = habilitar;
        crmForm.all.new_area_reserva1.Disabled = habilitar;
        crmForm.all.new_area_reserva2.Disabled = habilitar;
        crmForm.all.new_observacao_area_reserva1.Disabled = habilitar;
        crmForm.all.new_observacao_area_reserva2.Disabled = habilitar;
    }

    //Caso o usuário pertença a equipe Viabilidade engenharia, ele não poderá alterar nenhum campo da viabilidade
    if (crmForm.VerificaFuncao("Viabilidade Engenharia")) {
        if (crmForm.FormType == 2) {
            var todosAtributos = document.getElementsByTagName("label");
            for (var i = 0; i < todosAtributos.length; i++) {
                if (todosAtributos[i].htmlFor != "") {
                    DisabledField(todosAtributos[i].htmlFor.replace("_ledit", ""));
                    HabilitarParaEngenharia(false);
                }
            }
        }
    }

    //Função para esconder as seções de área de casa ou área de aptos na aba da engenharia
    //dependendo do tipo de unidade a ser gerada definida no bloco viabilidade.
    //Autor: Alexandre Negrão
    //Data: 9/3/2010
    crmForm.EscondeSecoesEngenhariaPorTipoDeUnidadeAserGerada = function() {
        if (crmForm.new_blocoviabilidadeid.DataValue != null) {
            var attributes = "new_tipodeunidade";
            var cmd = new RemoteCommand("MrvService", "GetEntity", "/MRVCustomizations/");

            cmd.SetParameter("entity", "new_blocoviabilidade");
            cmd.SetParameter("primaryField", "new_blocoviabilidadeid");
            cmd.SetParameter("entityId", crmForm.new_blocoviabilidadeid.DataValue[0].id);
            cmd.SetParameter("fieldList", attributes);

            var oResult = cmd.Execute();
            if (oResult.Success) {
                if (oResult.ReturnValue.Mrv.new_tipodeunidade == 1) {
                    //Bloco de apartamentos, esconder seção das áreas de casas na aba engenharia
                    EscondeSecao("new_area_casa", true);
                }
                else if (oResult.ReturnValue.Mrv.new_tipodeunidade == 2) {
                    //Bloco de casas, esconder seção de áreas dos aptos na aba engenharia
                    EscondeSecao("new_area_apto_engenharia", true);
                }
            }
        }
    }

    //Função também chamada no onchange de new_blocoviabilidadeid
    crmForm.EscondeSecoesEngenhariaPorTipoDeUnidadeAserGerada();
}
catch (error) {
    formularioValido = false;
    alert("Ocorreu um erro no formulário.\n" + error.description);
}
