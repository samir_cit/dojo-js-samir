﻿try {
    if (!formularioValido) {
        alert("Ocorreu um erro no formulário. Impossível salvar.");
        event.returnValue = false;
        return false;
    }

// Transformar caracteres em maiusculos------------------------------------//
var elm = document.getElementsByTagName("input");
for (i = 0; i< elm.length; i++)
{
    if (elm[i].type == "text")
        if (elm[i].DataValue != null)
        { 
             try{
              elm[i].DataValue = elm[i].DataValue.toUpperCase(); }
              catch (e) {}
         }
}

    crmForm.all.new_precovenda.Disabled = false;
    crmForm.all.new_areaaptom2.Disabled = false;
    crmForm.all.new_areaterracocom2.Disabled = false;
    crmForm.all.new_areaequivalentem2.Disabled = false;
    crmForm.all.new_area_equivalente_tabelao_m2.DataValue = false;

    crmForm.AtualizarCOBApartirDaBASE = function() 
    {
        var cmd = new RemoteCommand("MrvService", "AtualizarTipologiaViabilidadeCOBApartirDaBASE", "/MRVCustomizations/");
        
        cmd.SetParameter("idBlocoViabilidade", crmForm.all.new_blocoviabilidadeid.DataValue[0].id);
        cmd.SetParameter("nome", crmForm.all.new_nomedatipologia.SelectedText);
        cmd.SetParameter("tamanho", crmForm.all.new_tamanho.SelectedText);
        cmd.SetParameter("areaApto", crmForm.all.new_areaaptom2.DataValue);
        cmd.SetParameter("areaVaranda", crmForm.all.new_areavarandam2.DataValue);
        cmd.SetParameter("precoVenda", crmForm.all.new_precovenda.DataValue);
        
        var result = cmd.Execute();
        
        if (result.Success) 
        {
            if (result.ReturnValue.Mrv.Error)
                crmForm.DesabilitarFormularioErro(result.ReturnValue.Mrv.Error);
            else if (result.ReturnValue.Mrv.ErrorSoap)
                crmForm.DesabilitarFormularioErro(result.ReturnValue.Mrv.ErrorSoap);
        }
        else
            crmForm.DesabilitarFormularioErro("Erro na consulta das informações do usuário.");
    }
    
    /* Atualizar a Tipologia COB se a BASE for alterada. */
    if (crmForm.all.new_nomedatipologia.SelectedText != "" && 
        crmForm.all.new_nomedatipologia.SelectedText.indexOf("COB") == -1)
        crmForm.AtualizarCOBApartirDaBASE();
        
        crmForm.all.new_areaequivalenterealm2.ForceSubmit = true;
} 
catch (error) 
{
    alert("Ocorreu um erro no formulário.\n" + error.description);
    event.returnValue = false;
    return false;
}
