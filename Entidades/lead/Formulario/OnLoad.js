﻿function loadScript() {
    var oScript = document.createElement('script');
    oScript.type = "text/javascript";
    oScript.src = "/_static/Base.js";

    var oHead = document.getElementsByTagName('head')[0];
    oHead.appendChild(oScript);

    oScript.onreadystatechange = function() {
        if (this.readyState == 'complete' || this.readyState == 'loaded')
            Load();
    }
}
loadScript();

function Load() {
	StatusCode =
	{
		QUALIFICADO: "3"
	}

	NotificacaoIndicacao =
	{
		EMAIL_ENVIADO: "1",
		EMAIL_NAO_ENVIADO: "2",
		LEGADO: "3"
	}
	var desabilitarReenvioEmail = true;
	if ((crmForm.all.new_notificacao_indicacao.DataValue == NotificacaoIndicacao.EMAIL_NAO_ENVIADO || crmForm.all.new_notificacao_indicacao.DataValue == NotificacaoIndicacao.LEGADO
	|| crmForm.all.new_notificacao_indicacao_premiada_2.DataValue == NotificacaoIndicacao.EMAIL_NAO_ENVIADO || crmForm.all.new_notificacao_indicacao_premiada_2.DataValue == NotificacaoIndicacao.LEGADO) 
	&& crmForm.all.statuscode.DataValue == StatusCode.QUALIFICADO) {
		desabilitarReenvioEmail = false;
	}
	if (desabilitarReenvioEmail) {
		crmForm.RemoveBotao("HabilitarReenviodeEmail");	
		crmForm.RemoveBotao("MaisAes");
	}
}