﻿function CallCRM(command, soapAction) {
    var soapXML = "<?xml version='1.0' encoding='utf-8'?><soap:Envelope xmlns:soap='http://schemas.xmlsoap.org/soap/envelope/' xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema'>";
    soapXML += GenerateAuthenticationHeader();
    soapXML += command;
    soapXML += "</soap:Envelope>";

    var xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
    xmlhttp.open("POST", "/mscrmservices/2007/crmservice.asmx", false);
    xmlhttp.setRequestHeader("Content-Type", "text/xml; charset=utf-8");
    xmlhttp.setRequestHeader("SOAPAction", soapAction);
    xmlhttp.setRequestHeader("Content-Length", soapXML.length);
    xmlhttp.send(soapXML);
    return xmlhttp;
}

function GetUserId() {
    var body = "<soap:Body><Execute xmlns='http://schemas.microsoft.com/crm/2007/WebServices'><Request xsi:type='WhoAmIRequest' /></Execute></soap:Body>";
    var response = CallCRM(body, "http://schemas.microsoft.com/crm/2007/WebServices/Execute");

    var xmlDoc = new ActiveXObject("Microsoft.XMLDOM");
    xmlDoc.async = false;
    xmlDoc.loadXML(response.responseXML.xml);

    var userid = xmlDoc.getElementsByTagName("UserId")[0].childNodes[0].nodeValue;
    return userid;
}

function GetBusinessUnitByUser() {
    var retorno = new Object();
    retorno.BU = "";
    retorno.NOME = "";
    retorno.ERRO = false;
    retorno.Mensagem = "";
    var body = "<soap:Body><RetrieveMultiple xmlns='http://schemas.microsoft.com/crm/2007/WebServices'>" +
                "<query xmlns:q1='http://schemas.microsoft.com/crm/2006/Query' xsi:type='q1:QueryExpression'>" +
                    "<q1:EntityName>systemuser</q1:EntityName>" +
                    "<q1:ColumnSet xsi:type='q1:ColumnSet'>" +
                        "<q1:Attributes>" +
                            "<q1:Attribute>fullname</q1:Attribute>" +
                            "<q1:Attribute>businessunitid</q1:Attribute>" +
                        "</q1:Attributes>" +
                    "</q1:ColumnSet>" +
                    "<q1:Distinct>false</q1:Distinct>" +
                    "<q1:Criteria>" +
                        "<q1:FilterOperator>And</q1:FilterOperator>" +
                        "<q1:Conditions>" +
                            "<q1:Condition>" +
                                "<q1:AttributeName>systemuserid</q1:AttributeName>" +
                                 "<q1:Operator>Like</q1:Operator>" +
                                 "<q1:Values>" +
                                    "<q1:Value xsi:type='xsd:string'>" + GetUserId() + "</q1:Value>" +
                                 "</q1:Values>" +
                            "</q1:Condition>" +
                        "</q1:Conditions>" +
                    "</q1:Criteria>" +
                "</query>" +
                "</RetrieveMultiple></soap:Body>";
    var response = CallCRM(body, "http://schemas.microsoft.com/crm/2007/WebServices/RetrieveMultiple");
    var resultXml = response.responseXML;
    // Verificando erros.
    var errorCount = resultXml.selectNodes('//error').length;
    if (errorCount != 0) {
        retorno.ERRO = true;
        retorno.Mensagem = resultXml.selectSingleNode('//description').nodeTypedValue;
    }
    else {
        var results = resultXml.getElementsByTagName('BusinessEntity');
        if (results.length == 0) {
            retorno.ERRO = true;
            retorno.Mensagem = "Sem retorno.";
        }
        else {
            retorno.BU = results[0].selectSingleNode('./q1:businessunitid').attributes[0].nodeValue;
            retorno.NOME = results[0].selectSingleNode('./q1:fullname').nodeTypedValue;
        }
    }

    return retorno;
}

var inf = GetBusinessUnitByUser();
if (inf.ERRO) {
    alert(inf.Mensagem);
}
else {
    alert(inf.BU);
}